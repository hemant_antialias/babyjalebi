var admincontroller = {
    adminuserlist : '/admin/adminuserlist',
    vendorlist : '/admin/vendorlist',
    productlist : '/admin/productlist',
    inventorylist : '/admin/inventorylist',
    adminupdate : '/admin/adminupdate',
    orderlist : '/admin/orderlist',
    couponlist : '/admin/couponlist',


    admin_userlist_grid : function(){
        var self = this;

        $(document).ready(function () {
            var rawurl= self.adminuserlist;
            var fields=new Array('id','phone','name','email');
            var jqxgridid='#jqxgrid';
           
            var source =
            {
                datatype: "json",
                cache: false,
                datafields: [
                    { name: 'id',type: 'number' },
                    { name: 'name'},
                    { name: 'email'},
                    { name: 'contact' },
                     { name: 'dail_code' },
                    { name: 'is_active' },
                    { name: 'is_supper' },
                    { name: 'is_vendor'},
                    { name: 'created_at' }, 
                    { name: 'updated_at'} ,
                    
                ],
                id: 'id',
                url: rawurl,
                root: 'Rows',
                beforeprocessing: function(data)
                {       
                    source.totalrecords = data[0].TotalRows;
                    
                },

                sort: function () {
                // update the grid and send a request to the server.
                $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
                },

                filter: function (rowid, rowdata) {
                    // update the grid and send a request to the server.
                    $(jqxgridid).jqxGrid('updatebounddata', 'filter');
                },
                // addrow: function (rowid, rowdata, position, commit) {
                //     // synchronize with the server - send insert command
                //     var data = "insert=true&" + $.param(rowdata);
                //     ajax_grid(data,commit,rawurl);
                // },
                // deleterow: function (rowid, commit) {
                //     // synchronize with the server - send delete command
                //     var data = "delete=true&" + $.param({ id: rowid });
                //     ajax_grid(data,commit,rawurl);
                // },
                updaterow: function (rowid, rowdata, commit) {
                    //synchronize with the server - send update command
                    var data = "update=true&" + $.param(rowdata);
                    
                   // ajax_grid(data,commit,rawurl);
                  
                    var id = rowdata.id;
                    var is_supper = rowdata.is_supper;
                    var is_active = rowdata.is_active;
                   
                    $.ajax({
                    url: self.adminupdate,
                    type: "get",
                    data: {'id' : id,'is_active' : is_active,'is_supper' : is_supper},
                    success: function (data, textStatus, jqXHR) {
                        $("#jqxgrid").jqxGrid('updatebounddata');

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    // error check
                    }
                    }); // end ajax 

                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source);

            // initialize jqxGrid
            $(jqxgridid).jqxGrid(
            {
                width: '100%',
                height: 450,
                source: dataAdapter,
                theme: theme,
                editable: true,
                showfilterrow: true,
                filterable: true,
                sortable: true,
                pageable: true,
                pagesize: 50,
                pagesizeoptions: ['50', '100', '500'],
                virtualmode: true,
                //selectionmode: 'checkbox',
                rendergridrows: function()
                {
                    
                      return dataAdapter.records;     
                },
                columns: [
                    { text: 'Id', datafield: 'id', width: 70 ,filtercondition : 'EQUAL', editable: false},
                    { text: 'Name', datafield: 'name', width: 130 , editable: false},
                    { text: 'Email', datafield: 'email', width: 140, editable: false},
                    { text: 'Dial Code', datafield: 'dail_code', width: 130,editable: false },
                    { text: 'Contact', datafield: 'contact', width: 130,editable: false },
                    { text: 'Active', datafield: 'is_active',columntype:'checkbox', width: 100,editable: true },
                    { text: 'Supper', datafield: 'is_supper',columntype:'checkbox', width: 100,editable: true },
                    { text: 'Vendor', datafield: 'is_vendor',columntype:'checkbox', width: 100,editable: true },
                   // { text: 'Contact', datafield: 'is_vendor', width: 130,editable: false },
                    { text: 'Created At', datafield: 'created_at', width: 130, filtercondition : 'CONTAINS', editable: false},
                    { text: 'Updated At', datafield: 'updated_at', width: 130,  filtercondition : 'CONTAINS',editable: false},
          
                ]
            });


//trigger filter on notification button click
$('body').on('click','.notif-filter',function(){
    set_filter($(this).data('filtercol'),$(this).data('filterval'));
});

$('body').on('click','.reset-filter',function(){
    $(jqxgridid).jqxGrid('clearfilters');
});

function set_filter(colname,val){
        var searchText = val;
        // $("#jqxgrid").jqxGrid('removefilter', 'firstname');
        var filtergroup = new $.jqx.filter();
        var filtervalue = searchText;
        var filtercondition = 'contains';
        var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
        // used when there are multiple filters on a grid column:
        var filter_or_operator = 1;
        // used when there are multiple filters on the grid:
        filtergroup.operator = 'or';
        filtergroup.addfilter(filter_or_operator, filter);
        //remove other filters
        $(jqxgridid).jqxGrid('addfilter', colname, filtergroup);
        $(jqxgridid).jqxGrid('applyfilters');     
}            
//trigger filter on notification button click
        }); // end document.ready func

    }, // end users br_grid func

vendor_list_grid : function(){
        var self = this;

        $(document).ready(function () {
            var rawurl= self.vendorlist;
            var fields=new Array('id','phone','name','email');
            var jqxgridid='#jqxgrid';
           
            var source =
            {
                datatype: "json",
                cache: false,
                datafields: [
                    { name: 'admin_id',type: 'number' },
                    { name: 'name'},
                    { name: 'customer_email'},
                    { name: 'address' },
                     { name: 'store_name' },
                    { name: 'country' },
                    { name: 'state' },
                    { name: 'city'},
                    { name: 'created_at' }, 
                    { name: 'updated_at'} ,
                    
                ],
                id: 'admin_id',
                url: rawurl,
                root: 'Rows',
                beforeprocessing: function(data)
                {       
                    source.totalrecords = data[0].TotalRows;
                    
                },

                sort: function () {
                // update the grid and send a request to the server.
                $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
                },

                filter: function (rowid, rowdata) {
                    // update the grid and send a request to the server.
                    $(jqxgridid).jqxGrid('updatebounddata', 'filter');
                },
                // addrow: function (rowid, rowdata, position, commit) {
                //     // synchronize with the server - send insert command
                //     var data = "insert=true&" + $.param(rowdata);
                //     ajax_grid(data,commit,rawurl);
                // },
                // deleterow: function (rowid, commit) {
                //     // synchronize with the server - send delete command
                //     var data = "delete=true&" + $.param({ id: rowid });
                //     ajax_grid(data,commit,rawurl);
                // },
                updaterow: function (rowid, rowdata, commit) {
                    //synchronize with the server - send update command
                    var data = "update=true&" + $.param(rowdata);
                    
                   // ajax_grid(data,commit,rawurl);
                  
                    var id = rowdata.id;
                    var is_supper = rowdata.is_supper;
                    var is_active = rowdata.is_active;
                   
                    $.ajax({
                    url: self.adminupdate,
                    type: "get",
                    data: {'id' : id,'is_active' : is_active,'is_supper' : is_supper},
                    success: function (data, textStatus, jqXHR) {

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    // error check
                    }
                    }); // end ajax 

                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source);

            // initialize jqxGrid
            $(jqxgridid).jqxGrid(
            {
                width: '100%',
                height: 450,
                source: dataAdapter,
                theme: theme,
                editable: true,
                showfilterrow: true,
                filterable: true,
                sortable: true,
                pageable: true,
                pagesize: 50,
                pagesizeoptions: ['50', '100', '500'],
                virtualmode: true,
                //selectionmode: 'checkbox',
                rendergridrows: function()
                {
                    
                      return dataAdapter.records;     
                },
                columns: [
                    { text: 'Id', datafield: 'admin_id', width: 70 ,filtercondition : 'EQUAL', editable: false},
                    { text: 'Name', datafield: 'name', width: 130 , editable: false},
                    { text: 'Customer Email', datafield: 'customer_email', width: 140, editable: false},
                    { text: 'Country', datafield: 'country', width: 130,editable: false },
                    { text: 'State', datafield: 'state', width: 130,editable: false },
                    { text: 'City', datafield: 'city', width: 100,editable: true },
                    { text: 'Address', datafield: 'address', width: 200,editable: true },
                    { text: 'Location', datafield: 'location', width: 100,editable: true },
                    { text: 'Store Name', datafield: 'store_name', width: 130,editable: false },
                    { text: 'Created At', datafield: 'created_at', width: 130, filtercondition : 'CONTAINS', editable: false},
                    { text: 'Updated At', datafield: 'updated_at', width: 130,  filtercondition : 'CONTAINS',editable: false},
          
                ]
            });


//trigger filter on notification button click
$('body').on('click','.notif-filter',function(){
    set_filter($(this).data('filtercol'),$(this).data('filterval'));
});

$('body').on('click','.reset-filter',function(){
    $(jqxgridid).jqxGrid('clearfilters');
});

function set_filter(colname,val){
        var searchText = val;
        // $("#jqxgrid").jqxGrid('removefilter', 'firstname');
        var filtergroup = new $.jqx.filter();
        var filtervalue = searchText;
        var filtercondition = 'contains';
        var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
        // used when there are multiple filters on a grid column:
        var filter_or_operator = 1;
        // used when there are multiple filters on the grid:
        filtergroup.operator = 'or';
        filtergroup.addfilter(filter_or_operator, filter);
        //remove other filters
        $(jqxgridid).jqxGrid('addfilter', colname, filtergroup);
        $(jqxgridid).jqxGrid('applyfilters');     
}            
//trigger filter on notification button click
        }); // end document.ready func

    }, // end users br_grid func
product_list_grid : function(){
        var self = this;

        $(document).ready(function () {
            var rawurl= self.productlist;
            var fields=new Array('id','phone','name','email');
            var jqxgridid='#jqxgrid';
           
            var source =
            {
                datatype: "json",
                cache: false,
                datafields: [
                    { name: 'id',type: 'number' },
                    { name: 'product_images'},
                    { name: 'product_title'},
                    { name: 'product_description' },
                    { name: 'vendor_name' },
                    { name: 'price' },
                    { name: 'compare_price'},
                    { name: 'weight'},
                    { name: 'unit'},
                    { name: 'product_variants'},
                    { name: 'created_at' }, 
                    { name: 'updated_at'} ,
                    
                ],
                id: 'id',
                url: rawurl,
                root: 'Rows',
                beforeprocessing: function(data)
                {       
                    source.totalrecords = data[0].TotalRows;
                    
                },

                sort: function () {
                // update the grid and send a request to the server.
                $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
                },

                filter: function (rowid, rowdata) {
                    // update the grid and send a request to the server.
                    $(jqxgridid).jqxGrid('updatebounddata', 'filter');
                },
                // addrow: function (rowid, rowdata, position, commit) {
                //     // synchronize with the server - send insert command
                //     var data = "insert=true&" + $.param(rowdata);
                //     ajax_grid(data,commit,rawurl);
                // },
                // deleterow: function (rowid, commit) {
                //     // synchronize with the server - send delete command
                //     var data = "delete=true&" + $.param({ id: rowid });
                //     ajax_grid(data,commit,rawurl);
                // },
                updaterow: function (rowid, rowdata, commit) {
                    //synchronize with the server - send update command
                    var data = "update=true&" + $.param(rowdata);
                    
                   // ajax_grid(data,commit,rawurl);
                  
                    // var id = rowdata.id;
                    // var is_supper = rowdata.is_supper;
                    // var is_active = rowdata.is_active;
                   
                    // $.ajax({
                    // url: self.adminupdate,
                    // type: "get",
                    // data: {'id' : id,'is_active' : is_active,'is_supper' : is_supper},
                    // success: function (data, textStatus, jqXHR) {

                    // },
                    // error: function (jqXHR, textStatus, errorThrown) {
                    // // error check
                    // }
                    // }); // end ajax 

                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source);

            // initialize jqxGrid
            $(jqxgridid).jqxGrid(
            {
                width: '100%',
                height: 450,
                source: dataAdapter,
                theme: theme,
                editable: true,
                showfilterrow: true,
                filterable: true,
                sortable: true,
                pageable: true,
                pagesize: 50,
                pagesizeoptions: ['50', '100', '500'],
                virtualmode: true,
                //selectionmode: 'checkbox',
                rendergridrows: function()
                {
                    
                      return dataAdapter.records;     
                },
                
                columns: [
                    { text: 'Id', datafield: 'id', width: 70 ,filtercondition : 'EQUAL', editable: false},
                    { text: 'Image', datafield: 'product_images', width: 130 , editable: false},
                    { text: 'Title', datafield: 'product_title', width: 140, editable: false},
                    { text: 'Description', datafield: 'product_description', width: 130,editable: false },
                    { text: 'Vendor', datafield: 'vendor_name', width: 130,editable: false },
                    { text: 'Price', datafield: 'price', width: 100,editable: true },
                    { text: 'Compare Price', datafield: 'compare_price', width: 200,editable: false },
                    { text: 'Weight', datafield: 'weight', width: 100,editable: false },
                    { text: 'Unit', datafield: 'unit', width: 130,editable: false },
                    { text: 'variant', datafield: 'product_variants', width: 130,editable: false },
                    { text: 'Created At', datafield: 'created_at', width: 130, filtercondition : 'CONTAINS', editable: false},
                    { text: 'Updated At', datafield: 'updated_at', width: 130,  filtercondition : 'CONTAINS',editable: false},
          
                ]
            });


//trigger filter on notification button click
$('body').on('click','.notif-filter',function(){
    set_filter($(this).data('filtercol'),$(this).data('filterval'));
});

$('body').on('click','.reset-filter',function(){
    $(jqxgridid).jqxGrid('clearfilters');
});

function set_filter(colname,val){
        var searchText = val;
        // $("#jqxgrid").jqxGrid('removefilter', 'firstname');
        var filtergroup = new $.jqx.filter();
        var filtervalue = searchText;
        var filtercondition = 'contains';
        var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
        // used when there are multiple filters on a grid column:
        var filter_or_operator = 1;
        // used when there are multiple filters on the grid:
        filtergroup.operator = 'or';
        filtergroup.addfilter(filter_or_operator, filter);
        //remove other filters
        $(jqxgridid).jqxGrid('addfilter', colname, filtergroup);
        $(jqxgridid).jqxGrid('applyfilters');     
}            
//trigger filter on notification button click
        }); // end document.ready func

    }, // end users br_grid func
    order_list_grid : function(){
        var self = this;

        $(document).ready(function () {
            var rawurl= self.orderlist;
            var fields=new Array('id','phone','name','email');
            var jqxgridid='#jqxgrid';
           
            var source =
            {
                datatype: "json",
                cache: false,
                datafields: [
                    { name: 'id',type: 'number' },
                    { name: 'first_name'},
                    { name: 'product_title'},
                    { name: 'status_title' },
                    { name: 'order_code' },
                    { name: 'amount' },
                    { name: 'payble_amount'},
                    { name: 'discount'},
                    { name: 'coupon_code'},
                    { name: 'location'},
                    { name: 'created_at' }, 
                    { name: 'updated_at'} ,
                    { name: 'order_currency'},
                    
                ],
                id: 'id',
                url: rawurl,
                root: 'Rows',
                beforeprocessing: function(data)
                {       
                    source.totalrecords = data[0].TotalRows;
                    
                },

                sort: function () {
                // update the grid and send a request to the server.
                $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
                },

                filter: function (rowid, rowdata) {
                    // update the grid and send a request to the server.
                    $(jqxgridid).jqxGrid('updatebounddata', 'filter');
                },
                // addrow: function (rowid, rowdata, position, commit) {
                //     // synchronize with the server - send insert command
                //     var data = "insert=true&" + $.param(rowdata);
                //     ajax_grid(data,commit,rawurl);
                // },
                // deleterow: function (rowid, commit) {
                //     // synchronize with the server - send delete command
                //     var data = "delete=true&" + $.param({ id: rowid });
                //     ajax_grid(data,commit,rawurl);
                // },
                updaterow: function (rowid, rowdata, commit) {
                    //synchronize with the server - send update command
                    var data = "update=true&" + $.param(rowdata);
                    
                   // ajax_grid(data,commit,rawurl);
                  
                    // var id = rowdata.id;
                    // var is_supper = rowdata.is_supper;
                    // var is_active = rowdata.is_active;
                   
                    // $.ajax({
                    // url: self.adminupdate,
                    // type: "get",
                    // data: {'id' : id,'is_active' : is_active,'is_supper' : is_supper},
                    // success: function (data, textStatus, jqXHR) {

                    // },
                    // error: function (jqXHR, textStatus, errorThrown) {
                    // // error check
                    // }
                    // }); // end ajax 

                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source);

            // initialize jqxGrid
            $(jqxgridid).jqxGrid(
            {
                width: '100%',
                height: 450,
                source: dataAdapter,
                theme: theme,
                editable: true,
                showfilterrow: true,
                filterable: true,
                sortable: true,
                pageable: true,
                pagesize: 50,
                pagesizeoptions: ['50', '100', '500'],
                virtualmode: true,
                //selectionmode: 'checkbox',
                rendergridrows: function()
                {
                    
                      return dataAdapter.records;     
                },
                
                columns: [
                    { text: 'Id', datafield: 'id', width: 70 ,filtercondition : 'EQUAL', editable: false},
                    { text: 'Custome Name', datafield: 'first_name', width: 130 , editable: false},
                    { text: 'Order Code', datafield: 'order_code', width: 140, editable: false},
                    { text: 'Poduct', datafield: 'product_title', width: 130,editable: false },
                    { text: 'Status', datafield: 'status_title', width: 130,editable: false },
                    { text: 'Currency', datafield: 'order_currency', width: 100,editable: true },
                    { text: 'Amount', datafield: 'amount', width: 200,editable: false },
                    { text: 'Payble Amount', datafield: 'payble_amount', width: 100,editable: false },
                    { text: 'Coupon Code', datafield: 'coupon_code', width: 130,editable: false },
                     { text: 'Discount', datafield: 'discount', width: 130,editable: false },
                    { text: 'Location', datafield: 'location', width: 130,editable: false },
                    { text: 'Created At', datafield: 'created_at', width: 130, filtercondition : 'CONTAINS', editable: false},
                    { text: 'Updated At', datafield: 'updated_at', width: 130,  filtercondition : 'CONTAINS',editable: false},
          
                ]
            });


//trigger filter on notification button click
$('body').on('click','.notif-filter',function(){
    set_filter($(this).data('filtercol'),$(this).data('filterval'));
});

$('body').on('click','.reset-filter',function(){
    $(jqxgridid).jqxGrid('clearfilters');
});

function set_filter(colname,val){
        var searchText = val;
        // $("#jqxgrid").jqxGrid('removefilter', 'firstname');
        var filtergroup = new $.jqx.filter();
        var filtervalue = searchText;
        var filtercondition = 'contains';
        var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
        // used when there are multiple filters on a grid column:
        var filter_or_operator = 1;
        // used when there are multiple filters on the grid:
        filtergroup.operator = 'or';
        filtergroup.addfilter(filter_or_operator, filter);
        //remove other filters
        $(jqxgridid).jqxGrid('addfilter', colname, filtergroup);
        $(jqxgridid).jqxGrid('applyfilters');     
}            
//trigger filter on notification button click
        }); // end document.ready func

    }, // end users br_grid func


    coupon_list_grid : function(){
        var self = this;

        $(document).ready(function () {
            var rawurl= self.orderlist;
            var fields=new Array('id','phone','name','email');
            var jqxgridid='#jqxgrid';
           
            var source =
            {
                datatype: "json",
                cache: false,
                datafields: [
                    { name: 'id',type: 'number' },
                    { name: 'first_name'},
                    { name: 'product_title'},
                    { name: 'status_title' },
                    { name: 'order_code' },
                    { name: 'amount' },
                    { name: 'payble_amount'},
                    { name: 'discount'},
                    { name: 'coupon_code'},
                    { name: 'location'},
                    { name: 'created_at' }, 
                    { name: 'updated_at'} ,
                    { name: 'order_currency'},
                    
                ],
                id: 'id',
                url: rawurl,
                root: 'Rows',
                beforeprocessing: function(data)
                {       
                    source.totalrecords = data[0].TotalRows;
                    
                },

                sort: function () {
                // update the grid and send a request to the server.
                $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
                },

                filter: function (rowid, rowdata) {
                    // update the grid and send a request to the server.
                    $(jqxgridid).jqxGrid('updatebounddata', 'filter');
                },
                // addrow: function (rowid, rowdata, position, commit) {
                //     // synchronize with the server - send insert command
                //     var data = "insert=true&" + $.param(rowdata);
                //     ajax_grid(data,commit,rawurl);
                // },
                // deleterow: function (rowid, commit) {
                //     // synchronize with the server - send delete command
                //     var data = "delete=true&" + $.param({ id: rowid });
                //     ajax_grid(data,commit,rawurl);
                // },
                updaterow: function (rowid, rowdata, commit) {
                    //synchronize with the server - send update command
                    var data = "update=true&" + $.param(rowdata);
                    
                   // ajax_grid(data,commit,rawurl);
                  
                    // var id = rowdata.id;
                    // var is_supper = rowdata.is_supper;
                    // var is_active = rowdata.is_active;
                   
                    // $.ajax({
                    // url: self.adminupdate,
                    // type: "get",
                    // data: {'id' : id,'is_active' : is_active,'is_supper' : is_supper},
                    // success: function (data, textStatus, jqXHR) {

                    // },
                    // error: function (jqXHR, textStatus, errorThrown) {
                    // // error check
                    // }
                    // }); // end ajax 

                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source);

            // initialize jqxGrid
            $(jqxgridid).jqxGrid(
            {
                width: '100%',
                height: 450,
                source: dataAdapter,
                theme: theme,
                editable: true,
                showfilterrow: true,
                filterable: true,
                sortable: true,
                pageable: true,
                pagesize: 50,
                pagesizeoptions: ['50', '100', '500'],
                virtualmode: true,
                //selectionmode: 'checkbox',
                rendergridrows: function()
                {
                    
                      return dataAdapter.records;     
                },
                
                columns: [
                    { text: 'Id', datafield: 'id', width: 70 ,filtercondition : 'EQUAL', editable: false},
                    { text: 'Custome Name', datafield: 'first_name', width: 130 , editable: false},
                    { text: 'Order Code', datafield: 'order_code', width: 140, editable: false},
                    { text: 'Poduct', datafield: 'product_title', width: 130,editable: false },
                    { text: 'Status', datafield: 'status_title', width: 130,editable: false },
                    { text: 'Currency', datafield: 'order_currency', width: 100,editable: true },
                    { text: 'Amount', datafield: 'amount', width: 200,editable: false },
                    { text: 'Payble Amount', datafield: 'payble_amount', width: 100,editable: false },
                    { text: 'Coupon Code', datafield: 'coupon_code', width: 130,editable: false },
                     { text: 'Discount', datafield: 'discount', width: 130,editable: false },
                    { text: 'Location', datafield: 'location', width: 130,editable: false },
                    { text: 'Created At', datafield: 'created_at', width: 130, filtercondition : 'CONTAINS', editable: false},
                    { text: 'Updated At', datafield: 'updated_at', width: 130,  filtercondition : 'CONTAINS',editable: false},
          
                ]
            });


//trigger filter on notification button click
$('body').on('click','.notif-filter',function(){
    set_filter($(this).data('filtercol'),$(this).data('filterval'));
});

$('body').on('click','.reset-filter',function(){
    $(jqxgridid).jqxGrid('clearfilters');
});

function set_filter(colname,val){
        var searchText = val;
        // $("#jqxgrid").jqxGrid('removefilter', 'firstname');
        var filtergroup = new $.jqx.filter();
        var filtervalue = searchText;
        var filtercondition = 'contains';
        var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
        // used when there are multiple filters on a grid column:
        var filter_or_operator = 1;
        // used when there are multiple filters on the grid:
        filtergroup.operator = 'or';
        filtergroup.addfilter(filter_or_operator, filter);
        //remove other filters
        $(jqxgridid).jqxGrid('addfilter', colname, filtergroup);
        $(jqxgridid).jqxGrid('applyfilters');     
}            
//trigger filter on notification button click
        }); // end document.ready func

    }, // end users br_grid func

}

