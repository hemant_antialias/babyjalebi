<!-- Display Popup Button -->
<!-- <h1>Click Button To Popup Form Using Javascript</h1>
<button id="popup" onclick="div_show()">Popup</button> -->
<script>
$(function () {
 
  $("#rateYo").rateYo({
    rating: 5,
    fullStar: true,
 
    onSet: function (rating, rateYoInstance) {


        $('.hiddenrating').val(rating);
 
      // alert("Rating is set to: " + rating);
    }
  });
});
</script>
<script>
 $('.submitreview').click(function(e){
    e.preventDefault();
    
    var name = document.getElementById("name").value;
    var title = document.getElementById("title").value;
    var review = document.getElementById("msg").value;
if (name == '' || title == '' || review == '') {
// alert("Fill All Fields");
$("#error").css("display","block");
$("#error").css("color","red");
}
else
{
// var msgval = $("#msg").val();
    var base_url = "{{URL::to('/')}}";
       $.ajax({
        url:base_url+'/review',
          type:'POST',
          data:$("#reviewForm").serialize(),
          success:function(data){
            if (data.status==1) {
            // $("#npopupContact").modal('hide');
            div_hide();
             // window.location.reload();
             // alert(data.message);
             $('.success-review').html(data.message);
             $('.success-review').addClass('succcolor');


              // toastr.success(data.message, {timeOut: 100000});
              // return;
            }
            // $('.user-detail').html(data);
            // toastr.success('Customer has been added successfully.', {timeOut: 100000})
            // $('.email-invoice').prop("disabled", true);
          }
      });
   }
    });
</script>

<script>
    function check_empty() {
if (document.getElementById('name').value == "" || document.getElementById('email').value == "" || document.getElementById('msg').value == "") {
alert("Fill All Fields !");
} else {
document.getElementById('form').submit();
alert("Form Submitted Successfully...");
}
}
//Function To Display Popup
function div_show() {
$('#reviewForm')[0].reset();
document.getElementById('abc').style.display = "block";
}
//Function to Hide Popup
function div_hide(){
document.getElementById('abc').style.display = "none";
}
</script>
<!-- The Modal -->
<div id="myModal" class="modal vertically-centered">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <input type="email" name="Email_available" placeholder="email" required="">
    <button class="btn-danger" style="background:red;">Submit</button>
  </div>

</div>
 <script>
// Get the modal
// var modal = document.getElementById('myModal');

// // Get the button that opens the modal
// var btn = document.getElementsByClassName("myn");


// // Get the <span> element that closes the modal
 

// // When the user clicks the button, open the modal 
// btn.onclick = function() {
//     modal.style.display = "block";
// }
$('.myn').click( function(){
   $('#myModal').show();
});
$('.close').click(function(){
    $('#myModal').hide();
});


</script>

<!-- <script type="text/javascript">
     $(
 
     function () {
           var rules = {
            name: {
                required: true
            }
        };
        var messages = {
            txtName: {
                required: "Please enter name"
            }
        };
       $("reviewForm").validate({rules:rules, messages:messages});
          
         
          $(".myn").click(
              function () {

                  $("#dlg").dialog(
                      {
                          modal: true,
                          buttons: {
                              "Save": function () {
                                alert(  $("#frm").valid());
                              },
                              "Cancel": function () {
                                  $("#dlg").dialog("close");
              }
                          }

                      }
                      );
              }
              );

      });
</script> -->