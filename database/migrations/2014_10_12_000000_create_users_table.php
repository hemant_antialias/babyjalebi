<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('user_image')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('is_accept_marketing')->default(0);
            $table->tinyInteger('is_tax_exempt')->default(0);
            $table->string('contact')->nullable();
            $table->string('dial_code')->nullable();
            $table->tinyInteger('mobile_verify')->default(0);
            $table->tinyInteger('email_verify')->default(0);
            $table->string('google_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('company')->nullable();
            $table->text('address')->nullable();
            $table->text('notes')->nullable();
            $table->text('tags')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('location')->nullable();
            $table->integer('zip')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
