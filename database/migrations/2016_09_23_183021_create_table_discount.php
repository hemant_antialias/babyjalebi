<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDiscount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('discount_coupon_code');
            $table->string('discount_value');
            $table->tinyInteger('coupon_type')->default(0);
            $table->string('discount_type', 100)->nullable();
            $table->string('min_order_amount', 100)->nullable();
            $table->integer('collection')->nullable();
            $table->string('product_varient')->nullable();
            $table->string('customer_type')->nullable();
            $table->date('start_date')->nullable();
            $table->date('valid_till')->nullable();
            $table->integer('product_id');
            $table->integer('store_id');
            $table->integer('city');
            $table->string('used')->nullable();
            $table->string('coupon_quantity')->nullable();
            $table->tinyInteger('limit_per_user')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_coupon');
    }
}
