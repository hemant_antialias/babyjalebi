<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->tinyInteger('is_supper')->default(0);
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('is_vendor')->default(0);
            $table->string('contact')->nullable();
            $table->string('dail_code')->nullable();
            $table->string('admin_role')->nullable();
            $table->string('admin_permission')->nullable();
            $table->tinyInteger('mobile_verify')->default(0);
            $table->tinyInteger('email_verify')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
