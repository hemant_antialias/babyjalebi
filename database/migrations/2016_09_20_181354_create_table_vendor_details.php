<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendorDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->string('store_name');
            $table->string('customer_email')->nullable();
            $table->string('image')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('location')->nullable();
            $table->text('address')->nullable();
            $table->string('pin_code')->nullable();
            $table->text('store_details')->nullable();
            $table->text('near_by')->nullable();
            $table->string('lat')->nullable();
            $table->string('lag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors_details');
    }
}
