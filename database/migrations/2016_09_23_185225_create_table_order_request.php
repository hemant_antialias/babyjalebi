<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_request', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_code');
            $table->integer('user_id');
            $table->integer('store_id');
            $table->string('order_currency')->nullable();
            $table->integer('order_status')->nullable();
            $table->float('amount')->default(0);
            $table->float('payble_amount')->default(0);
            $table->float('discount')->default(0);;
            $table->string('coupon_code')->nullable();
            $table->string('location')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_request');
    }
}
