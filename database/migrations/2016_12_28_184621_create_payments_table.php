<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('payment_gateway_id');
            $table->string('payment_gateway_name',32);

            $table->string('mihpayid',64);
            $table->string('mode',6);
            $table->string('status',12);
            $table->string('txnid',64);
            $table->string('currency',6);           
            $table->float('paid_amount');
            $table->string('bank_ref_num',32);
            $table->string('addedon',32);
            $table->string('firstname',124);
            $table->string('phone',24);
            $table->text('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
