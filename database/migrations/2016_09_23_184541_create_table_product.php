<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_title');
            $table->text('product_description')->nullable();
            //$table->text('product_description');
            $table->text('product_images')->nullable();
            $table->integer('product_category')->nullable();
            $table->integer('meat_type')->nullable();
            $table->integer('brand_type')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->string('price')->default(0);
            $table->string('quantity')->default(0);
            $table->string('status')->default(0);
            $table->string('incoming')->nullable();
            $table->string('compare_price')->nullable();
            $table->tinyInteger('is_text')->default(0);
            $table->string('i_sku')->nullable();
            $table->string('i_barcode')->nullable();
            $table->string('inventory_policy')->nullable();
            $table->string('weight')->nullable();
            $table->string('unit')->nullable();
            $table->string('fullfillment_service')->nullable();
            $table->string('is_shiping')->nullable();
            $table->text('product_variants')->nullable();
            $table->string('slug',500)->nullable();
            $table->text('product_page_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('url')->nullable();
            $table->text('product_collection')->nullable();
            //$table->text('product_collection');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
