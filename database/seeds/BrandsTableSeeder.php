<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert(
            array(
                    array('brand_name' => 'LA CARNE CUTS', 'slug' => 'la-carne-cuts', 'status' => 1),
                    array('brand_name' => 'HOWDYJI', 'slug' => 'howdyji', 'status' => 1),
                    array('brand_name' => 'LIONFRESH', 'slug' => 'lionfresh', 'status' => 1),
                )
           );
    }
}
