<?php

use Illuminate\Database\Seeder;

class FulfillmentStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('fulfillment_status')->insert(
            array(
                    array('fulfillment_status_name' => 'fulfilled'),
                    array('fulfillment_status_name' => 'not fulfilled'),
                    array('fulfillment_status_name' => 'partially fulfilled'),
                    array('fulfillment_status_name' => 'unfulfilled'),
                )
           );
    }
}
