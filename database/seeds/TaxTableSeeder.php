<?php

use Illuminate\Database\Seeder;

class TaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxes')->insert(
            array(
                    array('tax' => '5'),
                    array('tax' => '12.5'),
                )
           );
    }
}
