<?php

use Illuminate\Database\Seeder;

class CollectionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('collection_types')->insert(
            array(
                    array('name' => 'Category'),
                    array('name' => 'Meat'),
                    array('name' => 'Brand'),
                )
           );
    }
}
