<?php

use Illuminate\Database\Seeder;

class MeatTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meat_types')->insert(
            array(
                    array('meat_type' => 'Chicken', 'slug' => 'chicken'),
                    array('meat_type' => 'Lamb', 'slug' => 'lamb'),
                    array('meat_type' => 'Pork', 'slug' => 'pork'),
                    array('meat_type' => 'Sea Food', 'slug' => 'sea-food'),
                    array('meat_type' => 'Turkey', 'slug' => 'turkey'),
                )
           );
    }
}
