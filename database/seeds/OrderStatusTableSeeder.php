<?php

use Illuminate\Database\Seeder;
use App\Admin;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('order_status')->insert(
            array(
                    array('status_title' => 'New'),
                    array('status_title' => 'Approved'),
                    array('status_title' => 'Out of Delivery'),
                    array('status_title' => 'Complete'),
                    array('status_title' => 'Cancelled'),
                    array('status_title' => 'Any'),
                )
           );
    }
}
