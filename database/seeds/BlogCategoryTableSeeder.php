<?php

use Illuminate\Database\Seeder;

class BlogCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog_category')->insert(
            array(
                    array('category' => 'Meat Manual 1', 'slug' => 'meat-manual-1', 'status' => 1),
                    array('category' => 'Recipe', 'slug' => 'recipe', 'status' => 1),
                )
           );
    }
}
