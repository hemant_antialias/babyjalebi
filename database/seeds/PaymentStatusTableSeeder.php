<?php

use Illuminate\Database\Seeder;

class PaymentStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_status')->insert(
            array(
                    array('payment_status_name' => 'authorized'),
                    array('payment_status_name' => 'paid'),
                    array('payment_status_name' => 'partially refunded'),
                    array('payment_status_name' => 'partially paid'),
                    array('payment_status_name' => 'pending'),
                    array('payment_status_name' => 'refunded'),
                    array('payment_status_name' => 'unpaid'),
                    array('payment_status_name' => 'voided'),
                    array('payment_status_name' => 'any'),
                )
           );
    }
}
