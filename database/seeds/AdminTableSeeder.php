<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Admin::truncate();
        Admin::create(array(
        	'id' 		 => '1',
            'name'		 => 'Lionfresh Super Admin',
			'email'   	 => 'admin@lionfresh.com',
			'password'	 =>  Hash::make('123456'),
			'is_supper'    	 => '1',
			'is_active'    	 => '1',
			'is_vendor'    	 => '0',
		));
    }
}
