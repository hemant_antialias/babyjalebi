<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->truncate();
    	
        $this->call(UserTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(OrderStatusTableSeeder::class);
        $this->call(PaymentStatusTableSeeder::class);
        $this->call(FulfillmentStatusTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(MeatTypesTableSeeder::class);
        $this->call(BlogCategoryTableSeeder::class);
        $this->call(TaxTableSeeder::class);
        $this->call(CollectionTypesTableSeeder::class);
    }
}
