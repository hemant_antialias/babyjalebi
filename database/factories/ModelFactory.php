<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'contact' => $faker->tollFreePhoneNumber,
        'user_image' => $faker->imageUrl($width=640, $height=480),
        'address' => $faker->address,
        'country' => $faker->country,
        'state' => $faker->state,
        'city' => $faker->city,
        'location' => $faker->streetName,
        'notes' => $faker->realText($maxNbChars = 200, $indexSize = 3)
    ];
});