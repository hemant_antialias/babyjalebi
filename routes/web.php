<?php
// use PDF;
use Illuminate\Support\Facades\Hash as Hash;
use Spatie\Analytics\Period;
use App\OrderRequest;
use App\User;
use App\DiscountCoupon;
use App\Product;
use App\Address;
use App\OrderStatus;
use App\Tracker;
use App\Vendor;
use \Illuminate\Support\Facades\DB as DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
define('STATIC_BASE_URL',  URL::to('/').'/');

//coupon types
define('MONEY_COUPON', '1');
define('PERCENTAGE_COUPON', '2');
define('FREE_SHIPPING', '3');
define('PDF_TMP_DIR', public_path().'/pdf_tmp/');

// review 
define('REVIEW_REJECT', '0');
define('REVIEW_PENDING', '1');
define('REVIEW_APPROVED', '2');

// Tags
define('PRODUCT_TAGS', '1');
define('ORDER_TAGS', '2');
define('CUSTOMER_TAGS', '3');
define('DISCOUNT_TAGS', '4');

// Payment Modes
define('COD', '0');
define('ONLINE', '1');


Route::get('/data', function () {
$response = 'ga:pageviews';
            
$ab = ['dimensions' => 'ga:pagePath,ga:pageTitle','sort' => '-ga:pageviews','max-results' => 5];
    $analyticsData = Analytics::performQuery(Period::days(0), $response, $ab);
    dd($analyticsData);
    // echo $analyticsData[0]['visitors'];
    
}); 


Route::get('/inseruser', function(){

$userid = DB::table('users')->select('id')->first();
dd($userid);

    $userdetail = DB::table('order_request')
        ->join('users','order_request.user_id','=','users.id','left')
        ->select('users.*',DB::raw('SUM(order_request.payble_amount) as totalSales'))
        ->where('order_request.user_id','=',$userid->id)
        ->first();


    $order = User::find($userdetail->id);
            $order->total_spent = $userdetail->totalSales;
            $order->save();

        });

// Route::get('/', function () {

//     return view('web-files.index', compact('cities'));
// });
define('PRODUCT_IMAGE_BASE_PATH_TMP',URL::to('/').'/tmp');

// for website routs
Route::get('/', 'HomeController@index');

// for product tab

Route::get('/products/{slug}', 'HomeController@productDetail');
Route::get('/about-us', 'StaticController@about');
Route::get('/404', 'StaticController@pageNotFound');
Route::get('/t&c', 'StaticController@tandc');
Route::get('/contact-us', 'StaticController@contactus');
Route::post('/sendcontact', 'HomeController@sendContact');
Route::get('/privacy-policy', 'StaticController@privacyp');
Route::get('/cancellation&refund', 'StaticController@cancellation');
Route::get('/shipping-returns','StaticController@shippingreturns');
Route::get('/press','StaticController@press');
Route::get('/faqs','StaticController@faqs');
Route::get('/personalization-services', 'StaticController@ps');


Route::get('admin/login', 'AdminController@login');
Route::post('admin/login', 'AdminController@postLogin');
Route::post('admin/dashboard/change-store', 'AdminController@changeStore');

Route::get('admin/logout', 'AdminController@logout');
Route::get('admin/edit-profile', 'AdminController@profile');
Route::post('admin/edit-profile', 'AdminController@editProfile');

Route::get('admin/settings', 'AdminController@settings');
Route::get('admin/add-banner', 'AdminController@banner');
Route::post('admin/add-banner', 'AdminController@addBanner');
Route::get('admin/bannerlist', 'AdminController@bannerList');


Route::post('admin/change-password', 'AdminController@postChangepassword');
Route::post('admin/change-email', 'AdminController@postChangeEmail');
Route::post('admin/deactivate', 'AdminController@postDeactivateAccount');


Route::get('product_tags', 'ProductController@allTags');



Route::get('admin/dashboard', 'AdminController@dashboard');
Route::get('admin/menu', 'AdminController@menu');
Route::get('admin/product', 'AdminController@product');

// Staff 
Route::get('admin/users', 'AdminController@adminUserList');
Route::get('admin/user/new', 'AdminController@addUser');
Route::post('admin/user/new', 'AdminController@postAddUser');


Route::get('admin/adminuserlist', 'AdminController@adminuserlist');
Route::post('admin/adminuserupd', 'AdminController@adminuserupd');

// Role and Permission
Route::get('admin/add-role', 'RolePermissionController@addRole');
Route::post('admin/add-role', 'RolePermissionController@postAddRole');
Route::get('admin/add-permission', 'RolePermissionController@addPermission');
Route::post('admin/add-permission', 'RolePermissionController@postAddPermission');




// Customer
Route::get('admin/customers/new', 'Admin\CustomerController@newCustomer');
Route::get('admin/customers', 'Admin\CustomerController@customers');
Route::get('admin/customers/edit/{id}', 'Admin\CustomerController@editCustomer');
Route::post('admin/customers/new', 'Admin\CustomerController@addNewCustomer');
Route::post('admin/customers/edit/{id}', 'Admin\CustomerController@addNewCustomer');
Route::post('admin/ajax/customer', 'Admin\CustomerController@getCustomerById');
Route::get('admin/customerlist', 'Admin\CustomerController@customerlist');
Route::get('admin/customers/{customerid}', 'Admin\CustomerController@customerDetail');
Route::post('admin/add-customer', 'Admin\CustomerController@addCustomer');
Route::post('admin/add-customer1', 'Admin\CustomerController@editCustomerinorderpage');
Route::post('admin/change-customer-email', 'Admin\CustomerController@changeCustomerEmail');
Route::post('admin/customer/add-tags-to-customer', 'Admin\CustomerController@addTagsToCustomer');
Route::post('admin/ajax/remove-customer', 'Admin\CustomerController@removeCustomer');
Route::post('admin/update-billing-address', 'Admin\CustomerController@updateBillingAddress');
Route::post('admin/update-billing-address1', 'Admin\CustomerController@updateBillingAddressOrderpage');
Route::get('admin/customers/all-orders/{id}', 'Admin\CustomerController@customerAllOrder');
Route::post('admin/customer/add-notes', 'Admin\CustomerController@addNotes');

// b2b users routes
Route::get('admin/business/customers', 'Admin\CustomerController@businessCustomers');
Route::post('admin/business/user/update-credits', 'Admin\CustomerController@updateBusinessUserCredits');


//  customer list  export
Route::get('/all-customer-csv', function(){

    $table = User::all();
    $filename = "customerlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('First Name', 'Last Name', 'Email', 'Notes', 'Contact', 'Address', 'Address2', 'Country', 'State', 'City', 'Location', 'Zip'));

    foreach($table as $row) {
         
        fputcsv($handle, array($row['first_name'], $row['last_name'], $row['email'], $row['notes'], $row['contact'], $row['address'], $row['address2'], $row['country'], $row['state'], $row['city'], $row['location'], $row['zip']));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'Customer_list.csv', $headers);
});

// reports 

Route::get('admin/report/reportlist', 'ReportsController@reportList');
Route::get('admin/report/report', 'ReportsController@reportsByMonth');
Route::get('admin/report/{slug}', 'ReportsController@reportDetail');
Route::get('admin/report/{slug}', 'ReportsController@reportDetail');
Route::post('admin/report-bydate', 'ReportsController@reportBydate');
// export data by date
Route::post('admin/discountreport-bydate', 'ReportsController@discountReportBydate');
Route::post('admin/daywisereport-bydate', 'ReportsController@daywiseReportBydate');
Route::get('admin/product-alldata', 'ReportsController@productReportAll');
Route::post('admin/product-bydate', 'ReportsController@salesbyproductReportBydate');
//  Export all data in sales by day section

Route::get('/daywise-records-csv', function(){

    $table = DB::select("select DISTINCT DAY(created_at) as day, MONTH(created_at) as mnth,order_status  from order_request where order_status = '4' OR order_status = '5' GROUP BY DAY(created_at),  MONTH(created_at) ORDER BY created_at DESC" );
                        // dd($table);
    $filename = "discountedorderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Date','Total Order', 'Gross sales', 'Discount', 'Returns', 'Net Sales', 'Shipping', 'Tax', 'Total Sales'));

    foreach($table as $row) {
        
$month = $row->day;
                              $month1 = $row->mnth;
                              $newDateTime = date("M", strtotime($month1));
                            // dd($month1);
                            $year = date("y",strtotime($row->day));
                           // $year = date("Y"); 

                            // echo $month;
        $all_compeletd_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as gross_amount, SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                   ->whereRaw('extract(day from created_at) = ?', [$month])
                   ->whereRaw('extract(month from created_at) = ?', [$month1]);
          if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_compeletd_records_qeury->where('vendor_id', $vendor->id);

           }
            $all_compeletd_records = $all_compeletd_records_qeury->where('order_status','=', 4)->distinct()->distinct()->first();
                    
        $all_return_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount , SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                   ->whereRaw('extract(day from created_at) = ?', [$month])
                   ->whereRaw('extract(month from created_at) = ?', [$month1]);
        if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_return_records_qeury->where('vendor_id', $vendor->id);

           }
          $all_return_records = $all_return_records_qeury->where('order_status','=',5)
                    ->distinct()->first();

        $all_records= $all_compeletd_records->gross_amount + $all_return_records->return_amount;
            
        $totalorder = $all_compeletd_records->total_order + $all_return_records->total_order;

                    // dd($all_records->total_order);
       // $return_amount = DB::table('order_request')
       //              ->select('order_request.*', DB::raw('SUM(amount) as return_amount'))
       //              ->whereRaw('extract(day from created_at) = ?', [$month])
       //              ->whereRaw('extract(month from created_at) = ?', [$month1])
       //              ->where('order_status','=', 5)
       //              ->distinct()->first();
        $netsale= $all_records - $all_compeletd_records->discount - $all_return_records->return_amount;
        
        $totalsale = $netsale + $all_compeletd_records->shippingrate + $all_compeletd_records->tax;

         $monthNum = $month1;
         $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
        $monthName;
        $datevalue = $month .$monthName ."2017";
       

         if($all_compeletd_records->discount == '0' || $all_compeletd_records->discount == NULL )
         {
            $discount = "0.00";
         }
        else
        {
            $discount = $all_compeletd_records->discount;
        }
        if($all_return_records->return_amount == '0' || $all_return_records->return_amount == NULL )
        {
            $returnvalue = "0.00";
        }
        else
        {
            $returnvalue = $all_return_records->return_amount;
        }
        
        $netsale1 = round($netsale, 2);
        if($all_compeletd_records->shippingrate == '0' || $all_compeletd_records->shippingrate == NULL )
        {
            $shippingrate = "0.00";
        }
        else
        {
            $shippingrate = $all_compeletd_records->shippingrate;
        }
        fputcsv($handle, array($datevalue,$totalorder,$all_records,$discount,$returnvalue,$netsale1,$shippingrate,round($all_compeletd_records->tax, 2),round($totalsale, 2)));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'salesbyday_Order_list.csv', $headers);
});

// end


// Product
Route::get('admin/product/new', 'Admin\ProductController@newProduct');
Route::get('admin/product/edit/{id}', 'Admin\ProductController@editProduct');
Route::post('admin/product/new', 'Admin\ProductController@addNewProduct');
Route::post('admin/product/upload', 'Admin\ProductController@imageupload');
Route::post('admin/product/edit/{id}', 'Admin\ProductController@addNewProduct');
Route::get('admin/products/inventory', 'Admin\ProductController@inventory');
Route::get('admin/inventorylist', 'Admin\ProductController@inventorylist');
// Route::get('admin/inventory/filter', 'Admin\ProductController@filterInventory');
Route::post('admin/add-variation-image', 'Admin\ProductController@addVariationImage');

// inventory list export
Route::get('/inventorylist', function(){
    
// dd($isStore()->id);
//     if (isStore())
//    {
//     $vendor = Vendor::where('admin_id', isStore()->id)->select('store_name')->first();
// }
    if(isStore())
    {
    $vendor = Vendor::where('admin_id', isStore()->id)->select('id','store_name')->first();
    }
$inventorylist = DB::table('inventory')
            ->join('product', 'inventory.product_id', '=', 'product.id', 'left')
            ->select('product.product_title', 'inventory.quantity','inventory.product_id')
            ->where('store_id',$vendor->id)
            ->get()
            ->toArray();


    $filename = $vendor->store_name."inventorylist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Title','Inventory'));

foreach($inventorylist as $row) {
    // $discription = strip_tags($row['product_description']);
  
        fputcsv($handle, array($row->product_title,$row->quantity));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, $vendor->store_name.'_Inventory_list.csv', $headers);
});

Route::post('admin/quantity/update', 'Admin\ProductController@updateProductQuantity');

Route::post('admin/add-products-to-category', 'Admin\ProductController@addProductsToCategory');
Route::post('admin/product/change-status', 'Admin\ProductController@changeProductStatus');
Route::post('admin/category/change-status', 'Admin\ProductController@changeStatus');
Route::post('admin/menu/change-status', 'Admin\ProductController@menuStatus');
Route::post('admin/product/add-product-type', 'Admin\ProductController@addNewProductType');
Route::post('admin/product/delete', 'Admin\ProductController@deleteProduct');
Route::post('admin/categoryslider/delete', 'Admin\ProductController@deleteCategoryslider');
Route::post('delete-tag', 'Admin\ProductController@deleteTag');

Route::get('admin/change-store', 'Admin\ProductController@changeStore');



Route::get('allproducts', 'Admin\ProductController@allProducts');
Route::get('allcategories', 'Admin\ProductController@allCategory');
Route::get('allvendors', 'Admin\ProductController@allVendor');
Route::get('alltags', 'Admin\ProductController@allTags');
Route::get('allproductsbycategory', 'Admin\ProductController@allProductsByCategory');
Route::get('allproductsbyvendor', 'Admin\ProductController@allProductsByVendor');
Route::get('allproductsbytags', 'Admin\ProductController@allProductsByTags');
Route::post('all-products-by-stores', 'Admin\ProductController@allProductsByStores');
Route::post('all-exclude-products-by-stores', 'Admin\ProductController@allExcludeProductsByStores');

Route::post('all-category-by-stores', 'Admin\ProductController@allCategoryByStores');
Route::post('all-exclude-category-by-stores', 'Admin\ProductController@allExcludeCategoryByStores');

Route::post('all-products-by-stores-category', 'Admin\ProductController@allProductsByStoresCategory');


// / Product Attribute CRUD
Route::get('admin/attribute', 'Admin\ProductAttributeController@index');
Route::get('admin/attribute/add', 'Admin\ProductAttributeController@add');
Route::post('admin/attribute/postadd', 'Admin\ProductAttributeController@postadd');
Route::get('admin/attribute/edit/{id}', 'Admin\ProductAttributeController@edit');
Route::post('admin/attribute/postedit/{id}', 'Admin\ProductAttributeController@postedit');
Route::get('admin/attribute/delete/{id}', 'Admin\ProductAttributeController@delete');



// Product Category
Route::get('admin/product/category/new', 'Admin\ProductController@newProductCategory');
Route::get('admin/product/category/edit/{id}', 'Admin\ProductController@editProductCategory');
Route::post('admin/product/category/new', 'Admin\ProductController@addNewProductCategory');
Route::post('admin/product/category/edit/{id}', 'Admin\ProductController@addNewProductCategory');
Route::get('admin/product', 'Admin\ProductController@product');
Route::get('admin/productlist', 'Admin\ProductController@productlist');
Route::get('admin/categorylist', 'Admin\ProductController@categoryList');
Route::get('admin/add-product-category', 'Admin\ProductController@addProductCategory');
Route::post('admin/product/delete-product-session', 'Admin\ProductController@deleteProductFromSession');
Route::post('/admin/product/restock', 'Admin\ProductController@restock');



// reviews
Route::get('admin/reviewlist', 'Admin\ProductController@reviewlist');
Route::post('admin/review/change-status', 'Admin\ProductController@changeReviewStatus');

// Vendors
Route::get('admin/vendor/new', 'AdminController@newVendor');
Route::get('admin/vendor/edit/{id}', 'AdminController@editVendor');
Route::post('admin/vendor/new', 'AdminController@addNewVendor');
Route::get('admin/vendor', 'AdminController@vendor');
Route::get('admin/vendorlist', 'AdminController@vendorlist');
Route::post('admin/vendor/edit/{id}', 'AdminController@addNewVendor');


// Discount Coupon
Route::get('admin/coupon/new', 'Admin\CouponController@newCoupon');
Route::get('admin/coupon/edit/{id}', 'Admin\CouponController@editCoupon');
Route::post('admin/coupon/new', 'Admin\CouponController@addNewCoupon');
Route::post('admin/coupon/edit/{id}', 'Admin\CouponController@addNewCoupon');
Route::get('admin/couponlist', 'Admin\CouponController@couponList');
// coupon list export
Route::get('/exportcoupon', function(){

    $table = DiscountCoupon::all();
    $filename = "Couponlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Discount coupon code', 'Discount Value', 'Coupon Type', 'Discount Type', 'Minimum order amount', 'Collection', 'Product Varient', 'Customer Type', 'Start Date', 'End Date', 'Product ID', 'Store ID', 'City', 'Used', 'Coupon Quantity', 'Limited Per User', 'Active', 'created at'));
foreach($table as $row) {
  
        fputcsv($handle, array($row['discount_coupon_code'], $row['discount_value'], $row['coupon_type'], $row['discount_type'], $row['min_order_amount'], $row['collection'], $row['product_varient'], $row['customer_type'], $row['start_date'], $row['valid_till'], $row['product_id'], $row['store_id'], $row['city'], $row['used'], $row['coupon_quantity'],$row['limit_per_user'],$row['is_active'],$row['created_at']));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'Coupon_list.csv', $headers);
});


// Order
Route::get('admin/order/new', 'Admin\OrderController@newOrder');
Route::post('admin/add-to-cart', 'Admin\OrderController@addToCart');
Route::post('admin/order/new', 'Admin\OrderController@createOrder');
Route::get('admin/order', 'Admin\OrderController@order');
Route::get('admin/orderlist', 'Admin\OrderController@orderlist');
Route::post('admin/add-to-order', 'Admin\OrderController@addToOrder');
Route::post('admin/add-product-to-session', 'Admin\OrderController@addProductToSession');
Route::post('admin/delete-product-to-session', 'Admin\OrderController@removeProductToSession');


Route::post('admin/bookorder', 'Admin\OrderController@bookOrder');
Route::get('admin/orderlist', 'Admin\OrderController@orderlist');
Route::get('admin/order/{ordercode}', 'Admin\OrderController@orderDetail');
Route::post('admin/ajax/show_product', 'Admin\OrderController@showProductByKeyword');
Route::post('admin/order/change-order-status', 'Admin\OrderController@changeOrderStatus');
Route::post('admin/order/change-payment-status', 'Admin\OrderController@changePaymentStatus');
Route::get('admin/ajax/discount-by-coupon', 'Admin\OrderController@discountByCoupon');
Route::post('admin/order/send-invoice', 'Admin\OrderController@sendInvoice');
Route::post('admin/order/delete-product-from-session', 'Admin\OrderController@deleteProductFromSession');
Route::post('/admin/order/post-comment', 'Admin\OrderController@postOrderComment');
Route::post('/mail/order/invoice', 'Admin\OrderController@printInvoice');
Route::post('order/export', 'Admin\OrderController@exportList');
Route::get('/admin/abondand', 'Admin\OrderController@abondand');
Route::post('admin/order/add-notes', 'Admin\OrderController@addNotes');

Route::get('/all-records-csv', function(){

    $table = DB::table('order_request')
                        ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                        ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                        ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                        ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                        ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                        ->select('order_request.*','order_status.status_title','payment_status.payment_status_name', 'users.id as userid','users.first_name', 'users.last_name', 'users.email', 'users.contact','users.notes', 'address.address', 'address.phone', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.phone as bphone', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
                        ->get();
                        // dd($table);
    $filename = "orderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Order Code', 'Email', 'Financial Status', 'Paid at', 'Subtotal', 'Shipping', 'Taxes', 'Total', 'Discount Code', 'Discount Amount', 'Created at', 'Status', 'Billing Name','Billing Last Name', 'Billing Street','Billing City','Billing Zip','Billing Phone','Shipping Name','Notes','Payment Method'));

    foreach($table as $row) {
        
$address = Address::where('user_id', $row->user_id)->first();
$status = OrderStatus::where('id',$row->order_status)->first();
                // dd($status->status_title);
// echo $status['status_title']; 
// $string = implode(';', $status);
// $status = OrderStatus::where('id', $row->status)->first();
         if ($row->order_from == 0)
    {
         $orderfrom = "Offline";
    }
    else
    {
        $orderfrom = "Online";
    }
    if($row->vendor_id == 1)
    {
        $orderid= "D".$row->id;
    }
      
    else
    {

    $orderid= "G".$row->id;

    }
    if($row->contact == 'NULL')
    {
        $contact = $row->bphone;
    }
    else
    {
        $contact = $row->contact;
    }
        
        fputcsv($handle, array($orderid,$row->email,$row->payment_status_name,$row->created_at,$row->amount,$row->custom_rate_value,$row->tax_rate_value,$row->payble_amount,$row->coupon_code,$row->discount,$row->created_at,$status['status_title'],$row->first_name,$row->last_name,$row->address,$row->city,$row->zip,$contact,$row->first_name,$row->notes));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'Order_list.csv', $headers);
});

//  for discounted order list export
Route::get('/discounted-records-csv', function(){

    $table = DB::table('order_request')
                             ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                             ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                             ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                             ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                             ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                             ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                             ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name','users.email','users.contact','users.notes', 'order_status.status_title','order_status.id as order_id', 'payment_status.payment_status_name', 'payment_status.id as payment_id','address.address', 'address.phone', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.phone as bphone', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
                             ->where('order_request.order_status','=', 4)
                             ->where('order_request.coupon_code', '<>' , NULL)
                             ->orderBy('order_request.created_at', 'desc')
                             ->get();
                        // dd($table);
    $filename = "discountedorderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Order Code', 'Email', 'Financial Status', 'Paid at', 'Subtotal', 'Shipping', 'Taxes', 'Total', 'Discount Code', 'Discount Amount', 'Created at', 'Status', 'Billing Name','Billing Last Name', 'Billing Street','Billing City','Billing Zip','Billing Phone','Shipping Name','Notes','Payment Method'));

    foreach($table as $row) {
        
$address = Address::where('user_id', $row->user_id)->first();
$status = OrderStatus::where('id',$row->order_status)->first();
                // dd($status->status_title);
// echo $status['status_title']; 
// $string = implode(';', $status);
// $status = OrderStatus::where('id', $row->status)->first();
         if ($row->order_from == 0)
    {
         $orderfrom = "Offline";
    }
    else
    {
        $orderfrom = "Online";
    }
    if($row->vendor_id == 1)
    {
        $orderid= "D".$row->id;
    }
      
    else
    {

    $orderid= "G".$row->id;

    }
    if($row->contact == 'NULL')
    {
        $contact = $row->bphone;
    }
    else
    {
        $contact = $row->contact;
    }
        
        fputcsv($handle, array($orderid,$row->email,$row->payment_status_name,$row->created_at,$row->amount,$row->custom_rate_value,$row->tax_rate,$row->payble_amount,$row->coupon_code,$row->discount,$row->created_at,$status['status_title'],$row->first_name,$row->last_name,$row->address,$row->city,$row->zip,$contact,$row->first_name,$row->notes));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'discounted_Order_list.csv', $headers);
});
Route::get('cron', 'CronController@abondandCart');
Route::post('admin/get-sales', 'Admin\OrderController@totalSales');
Route::post('admin/export-by-day', 'Admin\OrderController@exportByday');


Route::post('/check-pincode', 'CartController@checkPincode');


Route::post('admin/calc-total-qty', 'Admin\OrderController@calcTotalQty');
Route::post('admin/add-discounts', 'Admin\OrderController@addDiscounts');
Route::post('admin/remove-discount', 'Admin\OrderController@removeDiscounts');
Route::post('admin/add-shipping', 'Admin\OrderController@addShipping');

// for detail page 

Route::get('/bed-in-a-bag/{productslug}', 'HomeController@productDetailstatic');
Route::get('/gifts/{productslug}', 'HomeController@productDetailstatic');
Route::get('/diaper-bags/{productslug}', 'HomeController@productDetailstatic');
Route::post('/ajax/variationsimges', 'HomeController@variationsimges');



// Paytm payment url
Route::get("payment", "PaymentController@payment");
Route::post("payment/callback", "PaymentController@callback");


// Citrus payment url
Route::get("cpayment", "PaymentController@citruspay");
Route::post("indipay/response", "PaymentController@response");


Route::get("fpayment", "PaymentController@freechargePayment");

// Freecharge payment routes
Route::get('freecharge', 'PaymentController@freechargePayment');
Route::post("spayment/callback", "PaymentController@freechargeSuccessResponse");
Route::post("fpayment/callback", "PaymentController@freechargeFaiureResponse");


// Blogs
Route::get('admin/blog', 'Admin\BlogController@blogListing');
Route::get('admin/blog/new', 'Admin\BlogController@newBlog');
Route::post('admin/blog/new', 'Admin\BlogController@addNewBlog');
Route::get('admin/blog/edit/{id}', 'Admin\BlogController@editBlog');
Route::post('admin/blog/edit/{id}', 'Admin\BlogController@addNewBlog');



// Website Routes
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Auth::routes();
Route::get('ajaxdata', 'HomeController@autosearch');
Route::post('all-stores-by-city', 'HomeController@allStoresByCity');


// Route::get('/collections/{collection}/products/{slug}', 'HomeController@productDetail');
Route::get('/collection', 'HomeController@chicken');
Route::post('delete-variation', 'Admin\ProductController@deleteProductvariation');
Route::post('delete-related-product', 'Admin\ProductController@deleteRelatedProduct');
Route::get('/collections/{category}', 'HomeController@shopByCollections');
Route::get('/ajax/product-list', 'HomeController@ajaxProductList');
Route::post('/admin/ajax/delete-var-img', 'Admin\ProductController@delvarimg');

// coupon list export
Route::get('/productlist', function(){

    $table = Product::all();
    $filename = "Productlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('SKU', 'Title', 'Discription', 'Price', 'Compare Price', 'Tax', 'Quantity', 'Weight', 'Created at'));

foreach($table as $row) {
    $discription = strip_tags($row['product_description']);
  
        fputcsv($handle, array($row['i_sku'], $row['product_title'], $discription, $row['price'], $row['compare_price'], $row['tax'], $row['quantity'], $row['weight'],$row['created_at']));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'Product_list.csv', $headers);
});
Route::get('/checkout/customer-info/{ordercode}', 'AccountController@customerInfo');




Route::get('/home', 'HomeController@cityByHomepage');
Route::post('/add-subscriber', 'HomeController@addSubscriber');
Route::post('/email-when-available', 'HomeController@emailWhenAvailable');


Route::get('/home/{cityname}', 'HomeController@index');
Route::get('/blog/{bcategory}', 'HomeController@blogList');
Route::get('/blog/{bcategory}/{slug}', 'HomeController@blogDetail');


Route::get('/cart/asynccontent', 'CartController@asyncContent');
Route::get('/checkout', 'CartController@checkout');
Route::post('/add-to-cart', 'CartController@addToCart');
Route::post('/check-discount', 'CartController@checkDiscount');
Route::post('/remove-discount', 'CartController@removeDiscount');

Route::post('/delete-product', 'CartController@deleteProduct');
Route::post('/update-cart', 'CartController@updateOnQuantity');
Route::post('/order/new', 'CartController@newOrder');
Route::post('/guestorder/new', 'CartController@guestnewOrder');

Route::get('/query', 'HomeController@query');
Route::get('/product/search/{productslug}', 'HomeController@productserachDetail');
Route::get('/user/address', 'AccountController@userAddress');
Route::get('/user/add-address', 'AccountController@addAddress');
Route::get('/user/edit-address/{id}', 'AccountController@editAddress');
Route::post('/user/add-user-address', 'AccountController@addUserAddress');
Route::post('/user/add-address', 'AccountController@createAddress');
Route::post('/user/edit-address/{id}', 'AccountController@createAddress');
Route::get('/user/edit-profile', 'AccountController@editProfile');
Route::post('/user/edit-profile', 'AccountController@postEditProfile');
Route::get('/user/settings', 'AccountController@settings');
Route::post('/user/change-password', 'AccountController@postChangepassword');
// Route::get('/allproducts', 'HomeController@allProducts');

// for review
Route::post('/review', 'HomeController@writeReviews');
// for thank you page
Route::get('/checkout/thankyou/{ordercode}', 'AccountController@thankyou');

Route::get('/checkout/shipping-method/{ordercode}', 'AccountController@shippingMethod');
Route::get('/checkout/payment-method/{ordercode}', 'AccountController@paymentMethod');
Route::post('/checkout/add-address', 'AccountController@addCheckoutAddress');
Route::post('/checkout/add-shipping', 'AccountController@addCheckoutShipping');
Route::post('/checkout/order', 'AccountController@completeOrder');
// Route::get('/user/account', 'AccountController@getAccount');
Route::get('/user/account', 'AccountController@orderListbyusrid');

Route::get('/email', function () {
    
    // $cities = DB::table('city')->select('city_id', 'city')->where('status', '=', 1)->get();
    Mail::send('mail.order.order_success', [], function($message){
        $message->from('abhishekgautam76@gmail.com');
        $message->to('hemant@theantialias.com', "Lionfres Support")->subject("Welcome To Lionfresh");
    });
    dd('sent');
    return view('mail.order.order_success');
});

// for instagram
// Route::get('/', 'InstagramController@instagramFeed');


Route::get('/cart', 'CartController@cart');
Route::get('/{category}', 'HomeController@productIndex'); //product list by category
Route::get('/{category}/{subcategory}', 'HomeController@withSubcategory'); // subcategory list by category
Route::get('/{category}/{subcategory}/{product}', 'HomeController@productDetail'); // product detail page



