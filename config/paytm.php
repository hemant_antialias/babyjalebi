<?php
    return array(
        /*
        |--------------------------------------------------------------------------
        | Paytm Default Gateway
        |--------------------------------------------------------------------------
        | "sandbox" is for testing purpose
        | "live" is for production 
        |
        */
        'default'       => 'live',

        /*
        |--------------------------------------------------------------------------
        | Paytm Aditional settings
        |--------------------------------------------------------------------------
        |
        */
        'industry_type' => 'Retail109',
        'channel'       => 'WEB',
        'order_prefix'  => 'LION',
        'website'       => 'PrimoWEB',

        /*
        |--------------------------------------------------------------------------
        | Paytm Connection Credentials
        |--------------------------------------------------------------------------
        | sandbox,live
        |
        */
        'connections' => [

            'sandbox' => [
                'merchant_key'  => 'I1u&td1K7tAk#@t0',
                'merchant_mid'  => 'your-merchant-id',
            ],

            'live' => [
                'merchant_key'  => 'I1u&td1K7tAk#@t0',
                'merchant_mid'  => 'PrimoF37674321630113',
            ]

        ],
    );
