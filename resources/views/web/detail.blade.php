@extends('web.web_layout')

@section('content')
<?php  
use App\ProductImages;
use App\ProductReviews;
?>
<link rel="stylesheet" type="text/css" href="{{URL::to('web-assets/css/rating.css')}}">
        <script src="{{URL::to('web-assets/js/rating.js')}}"></script> 

<link rel="stylesheet" type="text/css" href="{{URL::to('css/detail.css')}} ">
<style>
     @media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px)  { /* IPAD STYLES GO HERE */
 html {
    -webkit-text-size-adjust:none;
    margin-left: 0%;
    margin-right: -5%;
}  
    .collection-listing .product-block {
    /* float: left; */
    width: 249px;
}
}
  
    
    }
/* The Modal (background) */
.collection-listing .product-block {
    height: auto;
    width: 20% !important;
    padding-left: 2% !important;
    padding-right: 2% !important;
    min-width: 0;
}
.out-of-stock {
    z-index: 1 !important;
    background: black !important;
    color: #fff !important;
    display: block !important;
    /* border-bottom-right-radius: 15px !important; */
    position: absolute;
    z-index: 999;
    text-align: center;
    /* border-bottom-left-radius: 15px !important; */
    width: 41% !important;
    margin-left: 58% !important;
    margin-right: auto !important;
    margin-top: 5%;
    padding-top: 4px;
    padding-bottom: 4px !important;
    font-weight: 500;
     font-size: 13px !important; 
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 10px;
    border: 1px solid #888;
    width: 25%;
    margin-top: 25%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
 .emailwhenavailbtncss {
  
    width: 86% !important;
                            }
@media only screen and ( max-width: 700px){
.collection-listing .product-block {
    height: auto;
    width: 46% !important;
    padding-left: 2% !important;
    padding-right: 2% !important;
    min-width: 0;
}
    .emailwhenavailbtncss {
    background: black !important;
    /* border-color: #839683 !important; */
    letter-spacing: 0.9px;
    font-size: 12px !important;
    margin-left: 0px !important;
    margin-bottom: -40% !important;
    padding-top: 2px !important;
    padding-bottom: 2px !important;
    width: 70% !important;
                            }
                            .cartmobicon {
    display: block !important;
    background: black;
    color: white;
    padding-top: 3px;
    padding-bottom: 3px;
    width: 20%;
    float: right;
    margin-right: 5.5%;
    margin-top: 6% !important;
                                        }
 .out-of-stock {
    font-size: 11px !important;
 }
}
@media only screen and ( width: 414px){
.cartmobicon {
    margin-top: 6% !important;
}

}
.soldout{ font-size: 10px!important; }
.leftqnty{
  font-size: 12px !important;
}
@media only screen and (max-width: 768px){
     .meatoutofstock{
           width: 52% !important;
    margin-left: 46% !important;
    }
    .leftqnty {
    font-size: 12px !important;
}
}
@media only screen and (width: 360px)
{
.cartmobicon {
    margin-top: 7% !important;
    margin-right: 0% !important;
}
.out-of-stock {
  margin-left:57% !important;
}
}
@media only screen and (width: 375px)
{
.cartmobicon {
    margin-top: 7% !important;
    margin-right: 1.5% !important;
}
.out-of-stock {
  margin-left:55% !important;
}
.meatoutofstock {
    width: 52% !important;
    margin-left: 43.5% !important;
}
}
@media only screen and (width: 320px)
{
.cartmobicon {
    margin-top: 3% !important;
    margin-right: -2.5% !important;
	width:26% !important;so
}

}
@media only screen and (width : 412px )
{
 .meatoutofstock {
    width: 52% !important;
    margin-left: 40% !important;
}
.soldexception{
margin-left : 50% !important;

}
@media only screen and (width : 414px)
{
.meatoutofstock {
    width: 52% !important;
    margin-left: 40% !important;
}
.out-of-stock {
  margin-left:51% !important;
}
</style>
<div id="abc">
<!-- Popup Div Starts Here -->
<div id="popupContact">
<!-- Contact Us Form -->
<form action="#" id="reviewForm" method="post" class="form" name="form">
<!-- <img  src="images/3.png" > -->
<a id="close" onclick ="div_hide()">×</a>
<h6 style="text-align: center;">  WRITE A REVIEW</h6>
<hr>
<div>
<div id="rateYo"></div>
<br>
<div id="error" style="display: none;"> All fields are Requierd</div>
<input  name="rating" placeholder="Title" class="hiddenrating" value="5" type="hidden">
<input  name="product_id" placeholder="Title" class="hiddenrating1" value="{{$product->id}}" type="hidden">
<input class="reviewinput" name="name" id="name" placeholder="Name" type="text" required="required" /><br><br>
<input class="reviewinput" name="title" id="title" placeholder="Title" type="text" required="required" /><br>
<textarea id="msg" name="message" id="msg" placeholder="Review" required="required" /></textarea>
<input type="submit" name="" id="submit" class="submitreview">
<!-- <a href="javascript:%20check_empty()" >Send</a> -->
</div>
</form>
</div> 
<!-- Popup Div Ends Here -->
</div>

<!-- We will use PrefixFree - a script that takes care of CSS3 vendor prefixes
You can download it from http://leaverou.github.com/prefixfree/ -->
<!-- <script src="https://thecodeplayer.com/uploads/js/prefixfree.js" type="text/javascript"></script> -->



<!-- code by aj for flexslider ends here  -->
    <div id="content">
        <div itemscope itemtype="http://schema.org/Product">
            <meta itemprop="name" content="Chicken Wings - Skinless (250g)" />
            <meta itemprop="url" content="../../../products/chicken-wings-fresh.html" />
            <meta itemprop="image" content="products/shutterstock_61008112_grandeb776.jpg?v=1462589324" />
            <meta itemprop="brand" content="LionFresh"/>
            <meta itemprop="logo" content="products/shutterstock_61008112_grandeb776.jpg?v=1462589324"/>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <meta itemprop="price" content="120.00" />
                <meta itemprop="priceCurrency" content="INR" />
                <link itemprop="availability" href="http://schema.org/InStock" />
            </div>
            <div id="shopify-product-reviews" data-id="5435085315">
            </div>
            <div class="container">
            
                <div class="page-header cf">
                    <div class="filters">
              <!-- <span class="see-more">See more:
                <a href="#" title="">
                </a>
              </span> -->
                    </div>
                    <div class="social-area">
              <!-- <span class="nextprev">
                <a class="control-prev" href="whole-chicken.html">Previous
                </a>
              </span> -->
                    </div>
                </div>
            </div>

       
            
            <div id="main-product-detail" class="product-detail spaced-row container cf">
                <div class="gallery layout-column-half-left has-thumbnails">
                <div class="main-image">
                     <a class="shows-lightbox" href="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" title="{{$product->product_title}} - Lion Fresh">
                        <img src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}} - Lion Fresh">
                    </a>
                    
                </div>
                
               <div class="thumbnails">
               <a class="active" title="{{$product->product_title}} - Lion Fresh" href="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" data-full-size-url="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}">
                        
                        <img src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}} - Lion Fresh">
                        
                    </a>
                    
                @foreach($product_images as $images)
                    
                     <a title="{{$product->product_title}} - Lion Fresh" href="{{URL::to('/product_images/'.$images->product_id.'/'.$images->images)}}" data-full-size-url="{{URL::to('/product_images/'.$images->product_id.'/'.$images->images)}}">
                        
                           <img src="{{URL::to('/product_images/'.$images->product_id.'/'.$images->images)}}" alt="{{$product->product_title}} - Lion Fresh">
                        
                     </a>
                    
                        
                        @endforeach
                    
                </div>
                
             </div>
                <div class="detail layout-column-half-right">
                    <h4 class="title">
                        {{$product->product_title}}
                    </h4>
                    <span class="price">Rs.
                                            @if($product->compare_price != 'NULL')
                                            <span class="price" style="color:black;text-decoration:line-through;padding-right: 2%;">
                                              {{$product->compare_price}}
                                              
                                            </span> 
                                            <span class="price">
                                               {{$product->price}}
                                            </span>
                                            @else
                                             <span class="price">
                                              {{$product->price}}
                                            </span>
                                            @endif
                    <!-- <h5 class="h1-style price">
                        id {{$product->id}}
                    </h5> -->
                    <!-- <h5 class="h1-style price">
                        Rs. {{$product->weight}}
                    </h5> -->
                    <p>Brand : 
                        <a href="{{URL::to('/collections/'.$product->brand_slug)}}" title="{{$product->brand_name}}">
                        {{$product->brand_name}}
                        </a>
                    </p>
                    @if($product->totalquantity <= 0)
                                    <!-- <input type="submit" class="paddingbot somemarginless btn btn-primary out-of-stock" value="Out of stock" > -->
                                    <div class=" out-of-stock" ><span class="soldout">SOLD OUT</span> </div>
                                    @endif
                                    @if($product->totalquantity <= 5 &&  $product->totalquantity > 0)
                                    <div style="background: #dcaa31 !important;width: 16%;color: #fff;padding: 6px;"><span class="leftqnty">Only {{$product->totalquantity}} Left!</span> </div>
                                    
                                    @endif
                    <div class="product-form section">
                            <div class="input-row">
                            @if($product->totalquantity > 0)
                            <?php
                              $product_image = ProductImages::where('product_id', '=', $product->id)->select('images')->first();
                            ?>
                                <form>
                                    Quantity <input type="number" class ="cartnumb" name="quantity" onkeypress="return isNumberKey(event)" min="1" oninput="validity.valid||(value='');" value="1" style="width: 45px;">
                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url"><br><br>
                                    <div class="success-review"></div>
                                    <img src="{{URL::to('images/5star.png')}}" style="width: 19%;"> {{count($product_reviews)}} REVIEWS<br>
                                     <a id="popup" onclick="div_show()" style="cursor: pointer;"> Write a review</a><br><br>
                                    <input type="submit" class="detailsubmits btn btn-primary add-to-cart submits" value="Add to Cart" id="" style="background: #494846 !important;color: #fff !important;border-radius: 1px !important;">
                                </form>

                               @else
                               <button class="btn btn-primary" style="background: #453f3f !important;">Out of Stock</button><br><br>
                                <div class="success-review"></div>
                               <img src="{{URL::to('images/5star.png')}}" style="width: 19%;"> {{count($product_reviews)}}<br>
                                     <a id="popup" onclick="div_show()" style="cursor: pointer;"> Write a review</a><br><br>
                                    <button class="btn btn-primary myn" style="background: red !important;"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;Email When Available</button> 
                                  @endif
                            </div>
                           <div class="cart-error"></div>
                    </div>
                    <div class="description user-content lightboximages">
                        {!!html_entity_decode($product->product_description)!!}
                    </div>
                    <!-- Underneath the description -->
                   
                </div>
            </div>
           
            <!-- /.product-detail -->
            <div id="related-products" class=" cf">
                <h4 class="align-centre">You may also like
                </h4>
                <div class="collection-listing related-collection cf row-spacing">
                    @foreach($similar_products as $product)
                    <?php
                      $product_image = ProductImages::where('product_id', '=', $product->id)->select('images')->first();
                    ?>
                    <div data-product-id="4932398211" class="chickenproductblock product-block" id="detailcont">

                        <div class="block-inner">
                         @if($product->totalquantity <= 0)
                                   <div class="soldexception out-of-stock"><span class="soldout">SOLD OUT</span> </div>
                                    @endif
                                    @if($product->totalquantity <= 5 &&  $product->totalquantity > 0)
                                    <div class="meatoutofstock out-of-stock" style="background: #dcaa31 !important;"><span class="leftqnty">Only {{$product->totalquantity}} Left!</span> </div>
                                    
                                    @endif
                        <div class="input-row cartbtn" >
;

                         @if($product->totalquantity > 0)
                                  <form>
                                     <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                                    @if(isset($product->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
                                    @endif
                                   <!--  <input type="submit" class="btn btn-primary add-to-cart detail-mob-sub mobbut" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;     border-radius: 3px !important; width: 78%; "> -->
                                   <input type="submit" class="cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                                  </form>
                                  @else
                                   <!--  <button class="btn btn-primary myn" style="background: red !important;">Email When Available</button>  -->
                                     <!--  <button class ="cartmobhide btn btn-primary emailbtn myn" data-id="{{$product->id}}" style="background: red !important;border-color: red !important; white-space: normal;  ">Email When Available</button>  -->
                                       <button class="emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Notify Me &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                                  @endif
                                </div>
                            <div class="image-cont">
                            @if(isset($product->product_images))
                                <a class="image-link" href="{{URL::to('/products/'.$product->slug)}}">
                                    <img src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" />
                                </a>
                            @else
                                <a class="image-link" href="{{URL::to('/products/'.$product->slug)}}">
                                    <img src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="{{$product->product_title}}" />
                                </a>
                            @endif
                                <a class="hover-info" href="{{URL::to('/products/'.$product->slug)}}">
                                    <div class="inner">
                                        <div class="innerer">
                                            <div class="title">{{$product->product_title}}
                                            </div>
                                            <span class="price">
                                              Rs. {{$product->price}}
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- /#related-products -->
            <div class="container">
            <hr>
            <h6> <img class="reviewimg" src="{{URL::to('images/5star.png')}}" style="width: 11%;"  > {{count($product_reviews)}} REVIEWS</h6>
            <hr>
                 @foreach($product_reviews as $review)
                 <?php 
                 $str = $review->name;
                 ?>
                 @if($review->status == 2)
                 <div style="margin-bottom: 8%;float: left;padding-right: 2%;">
                    <div class="numberCircle"><?php echo $str[0];  ?></div>
                    </div>
                    <div >
                    <p id="rename">{{$review->name}} </p>
                                
                @if($review->rating == 1 )
                <img class="reviewimg" src="{{URL::to('images/1star.png')}}" style="width: 11%;" >
                @endif
                @if($review->rating == 2 )
                <img class="reviewimg" src="{{URL::to('images/2star.png')}}" style="width: 11%;" >
                @endif
                @if($review->rating == 3 )
                <img class="reviewimg" src="{{URL::to('images/3star.png')}}" style="width: 11%;" >
                @endif
                @if($review->rating == 4 )
                <img class="reviewimg" src="{{URL::to('images/4star.png')}}" style="width: 11%;" >
                @endif
                @if($review->rating == 5 )
                <img class="reviewimg" src="{{URL::to('images/5star.png')}}" style="width: 11%;" > 
                @endif
                <h6 id="reviewtitle">{{$review->title}}</h6>
                <p id="reviewcomment">{{$review->comment}}</p>
                </div>

                <hr>
                @endif

                @endforeach
            </div>
        </div>
        <!-- /.product-page-layout -->
    </div>
    <script>
  $(".cartnumb").on("click", function () {
   $(this).select();
});


</script>
   
<script>
  function isNumberKey(evt){
    // alert("Negative Quantity is not allowed ");
    var charCode = (evt.which) ? evt.which : event.keyCode;
    return !(charCode > 31 && (charCode < 49 || charCode > 57 || charCode < 48));
}
</script>
<script src="{{'js/detail.js'}}"></script>
<script>
$(function () {
 
  $("#rateYo").rateYo({
    rating: 5,
    fullStar: true,
 
    onSet: function (rating, rateYoInstance) {


        $('.hiddenrating').val(rating);
 
      // alert("Rating is set to: " + rating);
    }
  });
});
</script>
<script>
 $('.submitreview').click(function(e){
    e.preventDefault();
    
    var name = document.getElementById("name").value;
    var title = document.getElementById("title").value;
    var review = document.getElementById("msg").value;
if (name == '' || title == '' || review == '') {
// alert("Fill All Fields");
$("#error").css("display","block");
$("#error").css("color","red");
}
else
{
// var msgval = $("#msg").val();
    var base_url = "{{URL::to('/')}}";
       $.ajax({
        url:base_url+'/review',
          type:'POST',
          data:$("#reviewForm").serialize(),
          success:function(data){
            if (data.status==1) {
            // $("#npopupContact").modal('hide');
            div_hide();
             // window.location.reload();
             // alert(data.message);
             $('.success-review').html(data.message);
             $('.success-review').addClass('succcolor');


              // toastr.success(data.message, {timeOut: 100000});
              // return;
            }
            // $('.user-detail').html(data);
            // toastr.success('Customer has been added successfully.', {timeOut: 100000})
            // $('.email-invoice').prop("disabled", true);
          }
      });
   }
    });
</script>

<script>
    function check_empty() {
if (document.getElementById('name').value == "" || document.getElementById('email').value == "" || document.getElementById('msg').value == "") {
alert("Fill All Fields !");
} else {
document.getElementById('form').submit();
alert("Form Submitted Successfully...");
}
}
//Function To Display Popup
function div_show() {
$('#reviewForm')[0].reset();
document.getElementById('abc').style.display = "block";
}
//Function to Hide Popup
function div_hide(){
document.getElementById('abc').style.display = "none";
}
</script>
<!-- The Modal -->
<div id="myModal" class="modal vertically-centered">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <input type="email" name="Email_available" placeholder="email" required="">
    <button class="btn-danger" style="background:red;">Submit</button>
  </div>

</div>
 <script>
// Get the modal
// var modal = document.getElementById('myModal');

// // Get the button that opens the modal
// var btn = document.getElementsByClassName("myn");


// // Get the <span> element that closes the modal
 

// // When the user clicks the button, open the modal 
// btn.onclick = function() {
//     modal.style.display = "block";
// }
$('.myn').click( function(){
   $('#myModal').show();
});
$('.close').click(function(){
    $('#myModal').hide();
});


</script>

<!-- <script type="text/javascript">
     $(
 
     function () {
           var rules = {
            name: {
                required: true
            }
        };
        var messages = {
            txtName: {
                required: "Please enter name"
            }
        };
       $("reviewForm").validate({rules:rules, messages:messages});
          
         
          $(".myn").click(
              function () {

                  $("#dlg").dialog(
                      {
                          modal: true,
                          buttons: {
                              "Save": function () {
                                alert(  $("#frm").valid());
                              },
                              "Cancel": function () {
                                  $("#dlg").dialog("close");
              }
                          }

                      }
                      );
              }
              );

      });
</script> -->

@endsection