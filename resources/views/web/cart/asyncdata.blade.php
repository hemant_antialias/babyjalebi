<div class="col-md-8" style="border: 1px solid #ccc;">
    <div class="row padding">
        <div id="deladdress">
            <div class="col-md-6">
                <h5>DELIVERY ADDRESS</h5>
                <span>Sabra Anand</span>
                <br>
                <span>Sabri Road, New Delhi- 110030</span>
                <br>
                <span>9873593465</span>
            </div>
            <div class="col-md-3">
                <span><i class="fa fa-pencil" aria-hidden="true"></i>EDIT</span>
            </div>
            <div class="col-md-3">
                <h6 id="newaddress">ADD NEW ADDRESS</h6>
            </div>
        </div>
        <!--deladdress ends here -->
    </div>

    <hr>
    <div class="row padding" style="background: #f3f3f3">
        <div class="clearfix"></div>
        <form id="useraddress">
        <div id="userinfo" style="display:none;">
            <div class="col-md-6 padding-bottom">
                <label>First Name</label>
                <input type="text" name="fname" class="form-control" id="fname">
            </div>
            <div class="col-md-6 padding-bottom">
                <label>Last Name</label>
                <input type="text" name="lname" class="form-control" id="lname">
            </div>
            <div class="clearfix"></div>
            <div class="col-md-8" padding-bottom>
                <label>ADDRESS</label>
                <input type="text" name="address" class="form-control" style="width:98%;">
            </div>
            <div class="col-md-4 padding-bottom">
                <label>APT/SUITE (Optional)</label>
                <input type="text" name="apt" class="form-control">
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 padding-bottom">
                <label>CITY</label>
                <input type="text" name="city" class="form-control">
            </div>
            <div class="col-md-6 padding-bottom">
                <label>POSTAL CODE</label>
                <input type="text" name="postcode" class="form-control">
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 padding-bottom">
                <label>STATE</label>
                <input type="text" name="stat" class="form-control">
            </div>
            <div class="col-md-6 padding-bottom">
                <label>PHONE</label>
                <input type="text" name="phone" class="form-control">
            </div>
            <button type="submit" class="save-address">Save</button>
        </div>
        </form>
        <!--userinfo ends here -->
    </div>
    <!--col-md-7 ends here -->
</div>
<div class="col-md-4 border">

    <h5>INSTANT DELIVERY</h5>
    <div class="padding"></div>
    <span><input type="radio" name="checkedradio"  style="float: left;margin-right: 10px;" checked="checked">60 MINUTES</span>
    <br>
    <div class="padding"></div>
    <span><input type="radio" name="checkedradio"  style="float: left;margin-right: 10px;" checked="checked">FREE SHIPPING</span>
    <br>
    <div class="padding"></div>

    <h5>DELIVERY SLOTS</h5>
    <div class="padding"></div>
    <span><input type="radio" name="delivery" class="floatleft" style="margin-right:10px;"> 9:00am - 12:00pm </span>
    <br>
    <span><input type="radio" name="delivery" class="floatleft" style="margin-right:10px;"> 9:00am - 12:00pm</span>
    <br>
    <span><input type="radio" name="delivery" class="floatleft" style="margin-right:10px;"> 9:00am - 12:00pm</span>
    <br>
    <span><input type="radio" name="delivery" class="floatleft" style="margin-right:10px;"> 9:00am - 12:00pm</span>
    <br>
    <div class="padding"></div>
    <h5>SCHEDULE DELIVERY</h5>
    <div class="padding"></div>
    <input type="text" placeholder="click to show datepicker" id="pickyDate" />
    <div class="padding-bottom"></div>
    <div class="padding-bottom"></div>
    <div class="padding-bottom"></div>
    <div class="padding-bottom"></div>
    <div class="padding-bottom"></div>
    <div class="padding-bottom"></div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      $('#newaddress').click(function() {
        $('#userinfo').slideToggle("fast");
      });
  });

  $('.save-address').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    var formdata = $('#useraddress').serialize();

    $.ajax({
      url: base_url+'/add-user-address',
      type: "post",
      data: 
      {
        "id": "{{Auth::id()}}",
      },
      success:function(data)
      {
        if (data.status == 1) {
          alert(data.message);
        }
      }
    });
  });
</script>