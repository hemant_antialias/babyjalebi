<?php

    $cart_content = Cart::content();
    
    $cart_total = Cart::subtotal('2', '.', '');
?>
<style type="text/css">

</style>
@if($data)
new
@endif
<input type="hidden" value="{{Cart::count()}}" class="cart_count">
<div id="cart-summary-overlay" class="cf" >
        <div class="item">
            <div>Just added:
            </div>
                <div class="cols">
                    <div class="img">
                        <img src="{{$cartItem->options->image_url}}">
                    </div>
                    <div class="info">
                        <div>{{$cartItem->name}}</div>
                        <div>Rs.{{$cartItem->price}}</div>
                    </div>
                    <div class="detail">
                        <div>Qty {{$cartItem->qty}}</div>
                        <div>Total Rs.{{($cartItem->qty*$cartItem->price)}}</div>
                    </div>
                </div>
        </div>
        <div class="cart">
            <div id="subtotalcart">Subtotal <span>Rs.{{$cart_total}}</span></div>
            <div class="cart-text">Excl. shipping&nbsp;</div>
            <div><span id="shop-more" class="button altcolour">Keep Shopping</span>
            <a class="button" style="color:white; background: #636465 !important;" href="{{URL::to('/cart')}}">Cart</a></div></div>
        </div>
</div>
<script>

    $("#shop-more").on('click', function(){
        $("#cart-summary-overlay").fadeOut();
        $("#cart-summary-overlay").fadeOut("slow");
        $("#cart-summary-overlay").fadeOut(2000);
    });
</script>