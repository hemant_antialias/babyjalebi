@extends('web.web_layout')

@section('content')
    <div id="content">
        <div class="container">
            <div class="page-header cf">
                <h1 class="majortitle">Chicken
                </h1>
                <div class="user-content lightboximages">
                    <div style="text-align: center; padding-left: 30px;">
              <span style="color: #666666;">Fresh Raw Chicken which is of great quality and flavour. 
              </span>
                    </div>
                    <div style="text-align: center; padding-left: 30px;">Savour our chicken cuts like boneless chicken, Chicken Thighs, Chicken Breast and a lot more.
                    </div>
                    <br>
                    <div style="text-align: center;">
              <span style="color: #666666;">
              </span>
                    </div>
                    <div style="text-align: center;">
              <span style="color: #666666;">
              </span>
                    </div>
                    <div style="text-align: center;">
              <span style="color: #000000;">
              </span>
                    </div>
                </div>
                <div class="filters">
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.cat-brand">Brand
              <span class="state">+
              </span>
                    </a>
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.cat-cook-style">Cook Style
              <span class="state">+
              </span>
                    </a>
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.cat-price">Price
              <span class="state">+
              </span>
                    </a>
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.cat-state">State
              <span class="state">+
              </span>
                    </a>
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.cat-weight">Weight
              <span class="state">+
              </span>
                    </a>
            <span class="sort tags">
              <label for="sort-by">Sort by
              </label>
              <select id="sort-by" data-initval="manual">
                  <option value="price-ascending">Price, low to high
                  </option>
                  <option value="price-descending">Price, high to low
                  </option>
                  <option value="title-ascending">Alphabetically, A-Z
                  </option>
                  <option value="title-descending">Alphabetically, Z-A
                  </option>
                  <option value="created-ascending">Date, old to new
                  </option>
                  <option value="created-descending">Date, new to old
                  </option>
                  <option value="best-selling">Best Selling
                  </option>
              </select>
            </span>
            <span class="view-as">
              <span class="view-as-label">View
              </span>
              <a id="view-as-tiles" class="active" href="#">Grid
                  <div class="fluff1">
                  </div>
                  <div class="fluff2">
                  </div>
                  <div class="fluff3">
                  </div>
                  <div class="fluff4">
                  </div>
              </a>
              <a id="view-as-stream"  href="#">Stream
                  <div class="fluff">
                  </div>
              </a>
            </span>
                </div>
                <!-- /.filters -->
                <div class="social-area">
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.social-row">Share
              <span class="state">+
              </span>
                    </a>
                </div>
            </div>
            <!-- /.page-header -->
        </div>
        <!-- /.container -->
        <div class="multi-tag-row">
            <div class="tags nav-row social-row height-hidden hidden">
                <div class="container">
                    <div class="social">
              <span class="socitem label">Share:
              </span>
                        <div class="socitem twittercont">
                            <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet
                            </a>
                        </div>
                        <div class="socitem facebookcont">
                            <div class="fb-like" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Category tag filter -->
            <div class="tags nav-row cat-brand hidden">
                <ul>
                    <li data-tag="meta-filter-brand-howdyji">
                        <a title="Narrow selection to products matching tag meta-filter-Brand-Howdyji" href="chicken/meta-filter-brand-howdyji.html">Howdyji
                        </a>
                    </li>
                    <li data-tag="meta-filter-brand-la-carne-cuts">
                        <a title="Narrow selection to products matching tag meta-filter-Brand-La Carne Cuts" href="chicken/meta-filter-brand-la-carne-cuts.html">La Carne Cuts
                        </a>
                    </li>
                    <li data-tag="meta-filter-brand-lionfresh">
                        <a title="Narrow selection to products matching tag meta-filter-Brand-Lionfresh" href="chicken/meta-filter-brand-lionfresh.html">Lionfresh
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Category tag filter -->
            <div class="tags nav-row cat-cook-style hidden">
                <ul>
                    <li data-tag="meta-filter-cook-style-raw">
                        <a title="Narrow selection to products matching tag meta-filter-Cook Style-Raw" href="chicken/meta-filter-cook-style-raw.html">Raw
                        </a>
                    </li>
                    <li data-tag="meta-filter-cook-style-ready-to-eat">
                        <a title="Narrow selection to products matching tag meta-filter-Cook Style-Ready To Eat" href="chicken/meta-filter-cook-style-ready-to-eat.html">Ready To Eat
                        </a>
                    </li>
                    <li data-tag="meta-filter-cook-style-slow-cooked-meats">
                        <a title="Narrow selection to products matching tag meta-filter-Cook Style-Slow Cooked Meats" href="chicken/meta-filter-cook-style-slow-cooked-meats.html">Slow Cooked Meats
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Category tag filter -->
            <div class="tags nav-row cat-price hidden">
                <ul>
                    <li data-tag="meta-filter-price-rs100-200">
                        <a title="Narrow selection to products matching tag meta-filter-Price-Rs100-200" href="chicken/meta-filter-price-rs100-200.html">200
                        </a>
                    </li>
                    <li data-tag="meta-filter-price-rs200-300">
                        <a title="Narrow selection to products matching tag meta-filter-Price-Rs200-300" href="chicken/meta-filter-price-rs200-300.html">300
                        </a>
                    </li>
                    <li data-tag="meta-filter-price-rs300-400">
                        <a title="Narrow selection to products matching tag meta-filter-Price-Rs300-400" href="chicken/meta-filter-price-rs300-400.html">400
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Category tag filter -->
            <div class="tags nav-row cat-state hidden">
                <ul>
                    <li data-tag="meta-filter-state-chilled">
                        <a title="Narrow selection to products matching tag meta-filter-State-Chilled" href="chicken/meta-filter-state-chilled.html">Chilled
                        </a>
                    </li>
                    <li data-tag="meta-filter-state-frozen">
                        <a title="Narrow selection to products matching tag meta-filter-State-Frozen" href="chicken/meta-filter-state-frozen.html">Frozen
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Category tag filter -->
            <div class="tags nav-row cat-weight hidden">
                <ul>
                    <li data-tag="meta-filter-weight-100g-200g">
                        <a title="Narrow selection to products matching tag meta-filter-Weight-100g-200g" href="chicken/meta-filter-weight-100g-200g.html">200g
                        </a>
                    </li>
                    <li data-tag="meta-filter-weight-200g-300g">
                        <a title="Narrow selection to products matching tag meta-filter-Weight-200g-300g" href="chicken/meta-filter-weight-200g-300g.html">300g
                        </a>
                    </li>
                    <li data-tag="meta-filter-weight-300g-400g">
                        <a title="Narrow selection to products matching tag meta-filter-Weight-300g-400g" href="chicken/meta-filter-weight-300g-400g.html">400g
                        </a>
                    </li>
                    <li data-tag="meta-filter-weight-400g-500g">
                        <a title="Narrow selection to products matching tag meta-filter-Weight-400g-500g" href="chicken/meta-filter-weight-400g-500g.html">500g
                        </a>
                    </li>
                    <li data-tag="meta-filter-weight-500g">
                        <a title="Narrow selection to products matching tag meta-filter-Weight-500g+" href="chicken/meta-filter-weight-500g.html">500g+
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.multi-tag-row -->
        <div class="container">
            <div class="collection-listing cf">
                <div class="product-list">
                    <div data-product-id="4932398211" class="product-block">
                        <div class="block-inner">
                            <div class="image-cont">
                                <a class="image-link more-info" href="chicken/products/chicken-cheese-and-onion.html">
                                    <img src="products/shutterstock_172593842_be6593ef-24b7-4549-ab33-db230a903001_large4457.jpg?v=1462589299" alt="Chicken &amp; Cheese and Onion (500g) - Lion Fresh" />
                                </a>
                                <a class="hover-info more-info" href="chicken/products/chicken-cheese-and-onion.html">
                                    <div class="inner">
                                        <div class="innerer">
                                            <div class="title">Chicken & Cheese and Onion (500g)
                                            </div>
                        <span class="price">
                          Rs. 275.00
                        </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <div class="container inner cf">
                                <div class="gallery layout-column-half-left ">
                                    <div class="main-image">
                                        <a href="chicken/products/chicken-cheese-and-onion.html">
                                            <img src="css/assets/blank9a04.gif?14144647724873477262" data-src="//products/shutterstock_172593842_be6593ef-24b7-4549-ab33-db230a903001_large.jpg?v=1462589299" alt="Chicken &amp; Cheese and Onion (500g) - Lion Fresh"/>
                                        </a>
                                    </div>
                                </div>
                                <div class="detail layout-column-half-right">
                                    <h2 class="h1-style title">
                                        <a href="chicken/products/chicken-cheese-and-onion.html">Chicken & Cheese and Onion (500g)
                                        </a>
                                    </h2>
                                    <h2 class="h1-style price">
                                        Rs. 275.00
                                    </h2>
                                    <p>Brand
                                        <a href="vendors5776.html?q=LionFresh" title="">LionFresh
                                        </a>
                                    </p>
                                    <div class="product-form section">
                                        <form class="form" action="https://www.lionfresh.com/cart/add" method="post" enctype="multipart/form-data" data-product-id="4932398211">
                                            <div class="input-row">
                                                <label for="quantity">Quantity
                                                </label>
                                                <input id="quantity" class="select-on-focus" name="quantity" value="1" size="2" />
                                            </div>
                                            <a href="https://gurgaon.lionfresh.com/collections/chicken/products/chicken-cheese-and-onion" target="_blank" style="color:#d8920c;" id="forgurgaon">
                                                <b class="gurg">Get Delivered in Gurgaon
                                                </b>
                                            </a>
                                            <!-- to show description in stream layout-->
                                            <!--       <br>
                    <table width="573">
                    <tbody>
                    <tr>
                    <td width="573">
                    <p>These fully-cooked &amp; ready to eat all-natural Chicken Sausages mixed with bits of cheese and onion are bursting with flavor. Great for BBQ's, in buns, in easy bake casseroles, or heated just-as-is with whatever sides, relishes, or chutneys tickle your tastebuds. </p>
                    <p>Packed Frozen</p>
                    </td>
                    </tr>
                    </tbody>
                    </table> -->
                                            <div class="input-row">
                                                <input type="hidden" name="id" value="24921417027" />
                                            </div>
                                            <div class="yotpo bottomLine"
                                                 data-appkey="VZD53PFkMTiDvkcahLl4P532sAhIPYlOdgOHKX0z"
                                                 data-domain="lionfrsh.myshopify.com"
                                                 data-product-id="7755160195"
                                                 data-product-models="7755160195"
                                                 data-name="Chicken Curry Cut (12 piece / c. 750g) - Skinless"
                                                 data-url=""
                                                 data-image-url="//products/shutterstock_303853652_large.jpg%3Fv=1469796800"
                                                 data-description="&lt;table width=&quot;100%&quot;&gt;
                                               &lt;tbody&gt;
                                               &lt;tr&gt;
                                               &lt;td&gt;
                                               &lt;p&gt; Skinless whole bird curry cut ideal for making home made curries and stews.&lt;/p&gt;
                                               &lt;p&gt; &lt;/p&gt;
                                               &lt;p&gt;Approx 700-750g &lt;/p&gt;
                                               &lt;/td&gt;
                                               &lt;/tr&gt;
                                               &lt;/tbody&gt;
                                               &lt;/table&gt;"
                                                 data-bread-crumbs="meta-filter-Brand-Lionfresh;meta-filter-Cook Style-Raw;meta-filter-Price-Rs200-300;meta-filter-Weight-500g+;">
                                            </div>
                                            <div class="input-row">
                                                <input type="submit" value="Add to Cart" />
                                            </div>
                                        </form>
                                    </div>
                                    <a class="more" href="chicken/products/chicken-curry-cut-12-piece-c-750g-skinless.html">More Details &rarr;
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div data-product-id="4932395395" class="product-block">
                        <div class="block-inner">
                            <div class="image-cont">
                                <a class="image-link more-info" href="chicken/products/whole-chicken.html">
                                    <img src="products/shutterstock_191123765_large8767.jpg?v=1465382951" alt="Whole Chicken - Skinless (Approx 700g) - Lion Fresh" />
                    <span class="productlabel general">
                      <span>Only 1 left!
                      </span>
                    </span>
                                </a>
                                <a class="hover-info more-info" href="chicken/products/whole-chicken.html">
                                    <div class="inner">
                                        <div class="innerer">
                                            <div class="title">Whole Chicken - Skinless (Approx 700g)
                                            </div>
                        <span class="price">
                          Rs. 220.00
                        </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <div class="container inner cf">
                                <div class="gallery layout-column-half-left ">
                                    <div class="main-image">
                                        <a href="chicken/products/whole-chicken.html">
                                            <img src="css/assets/blank9a04.gif?14144647724873477262" data-src="//products/shutterstock_191123765_large.jpg?v=1465382951" alt="Whole Chicken - Skinless (Approx 700g) - Lion Fresh"/>
                                        </a>
                                    </div>
                                </div>
                                <div class="detail layout-column-half-right">
                    <span class="productlabel general">
                      <span>Only 1 left!
                      </span>
                    </span>
                                    <h2 class="h1-style title">
                                        <a href="chicken/products/whole-chicken.html">Whole Chicken - Skinless (Approx 700g)
                                        </a>
                                    </h2>
                                    <h2 class="h1-style price">
                                        Rs. 220.00
                                    </h2>
                                    <p>Brand
                                        <a href="vendors5776.html?q=LionFresh" title="">LionFresh
                                        </a>
                                    </p>
                                    <div class="product-form section">
                                        <form class="form" action="https://www.lionfresh.com/cart/add" method="post" enctype="multipart/form-data" data-product-id="4932395395">
                                            <div class="input-row">
                                                <label for="quantity">Quantity
                                                </label>
                                                <input id="quantity" class="select-on-focus" name="quantity" value="1" size="2" />
                                            </div>
                                            <a href="https://gurgaon.lionfresh.com/collections/chicken/products/whole-chicken" target="_blank" style="color:#d8920c;" id="forgurgaon">
                                                <b class="gurg">Get Delivered in Gurgaon
                                                </b>
                                            </a>
                                            <!-- to show description in stream layout-->
                                            <!--       <table width="100%">
                    <tbody>
                    <tr>
                    <td> <span>Weight: 1 piece. Approx 700g</span>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="573">
                    <colgroup><col width="573"></colgroup>
                    <tbody>
                    <tr height="20">
                    <td class="xl68" style="height: 15.0pt; width: 430pt;" height="20" width="573">That’s right, the whole chicken. Skinless</td>
                    </tr>
                    </tbody>
                    </table> -->
                                            <div class="input-row">
                                                <input type="hidden" name="id" value="15278077315" />
                                            </div>
                                            <div class="yotpo bottomLine"
                                                 data-appkey="VZD53PFkMTiDvkcahLl4P532sAhIPYlOdgOHKX0z"
                                                 data-domain="lionfrsh.myshopify.com"
                                                 data-product-id="4932395395"
                                                 data-product-models="4932395395"
                                                 data-name="Whole Chicken - Skinless (Approx 700g)"
                                                 data-url=""
                                                 data-image-url="//products/shutterstock_191123765_large.jpg%3Fv=1465382951"
                                                 data-description="&lt;table width=&quot;100%&quot;&gt;
                                               &lt;tbody&gt;
                                               &lt;tr&gt;
                                               &lt;td&gt; &lt;span&gt;Weight: 1 piece. Approx 700g&lt;/span&gt;
                                               &lt;/td&gt;
                                               &lt;/tr&gt;
                                               &lt;/tbody&gt;
                                               &lt;/table&gt;
                                               &lt;table border=&quot;0&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; width=&quot;573&quot;&gt;
                                               &lt;colgroup&gt;&lt;col width=&quot;573&quot;&gt;&lt;/colgroup&gt;
                                               &lt;tbody&gt;
                                               &lt;tr height=&quot;20&quot;&gt;
                                               &lt;td class=&quot;xl68&quot; style=&quot;height: 15.0pt; width: 430pt;&quot; height=&quot;20&quot; width=&quot;573&quot;&gt;That’s right, the whole chicken. Skinless&lt;/td&gt;
                                               &lt;/tr&gt;
                                               &lt;/tbody&gt;
                                               &lt;/table&gt;"
                                                 data-bread-crumbs="meta-filter-Brand-Lionfresh;meta-filter-Cook Style-Raw;meta-filter-Price-Rs100-200;meta-filter-State-Chilled;meta-filter-Weight-500g+;">
                                            </div>
                                            <div class="input-row">
                                                <input type="submit" value="Add to Cart" />
                                            </div>
                                        </form>
                                    </div>
                                    <a class="more" href="chicken/products/whole-chicken.html">More Details &rarr;
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div data-product-id="5435085315" class="product-block">
                        <div class="block-inner">
                            <div class="image-cont">
                                <a class="image-link more-info" href="chicken-wings-fresh.html">
                                    <img src="products/shutterstock_61008112_largeb776.jpg?v=1462589324" alt="Chicken Wings - Skinless (250g) - Lion Fresh - 1" />
                                </a>
                                <a class="hover-info more-info" href="chicken-wings-fresh.html">
                                    <div class="inner">
                                        <div class="innerer">
                                            <div class="title">Chicken Wings - Skinless (250g)
                                            </div>
                        <span class="price">
                          Rs. 120.00
                        </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <div class="container inner cf">
                                <div class="gallery layout-column-half-left has-thumbnails">
                                    <div class="main-image">
                                        <a href="chicken/products/chicken-wings-fresh.html">
                                            <img src="css/assets/blank9a04.gif?14144647724873477262" data-src="//products/shutterstock_61008112_large.jpg?v=1462589324" alt="Chicken Wings - Skinless (250g) - Lion Fresh - 1"/>
                                        </a>
                                    </div>
                                    <div class="thumbnails">
                                        Images
                                        /
                                        <a  class="active"  title="Chicken Wings - Skinless (250g) - Lion Fresh - 1" href="products/shutterstock_61008112_largeb776.jpg?v=1462589324" data-full-size-url="//products/shutterstock_61008112.jpg?v=1462589324">
                        <span>1
                        </span>
                                        </a>
                                        /
                                        <a  title="Buy Chicken Wings Online Delhi- Skinless (250g) - Lion Fresh - 2" href="products/shutterstock_160320674_largec14c.jpg?v=1467712187" data-full-size-url="//products/shutterstock_160320674.jpg?v=1467712187">
                        <span>2
                        </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="detail layout-column-half-right">
                                    <h2 class="h1-style title">
                                        <a href="chicken-wings-fresh.html">Chicken Wings - Skinless (250g)
                                        </a>
                                    </h2>
                                    <h2 class="h1-style price">
                                        Rs. 120.00
                                    </h2>
                                    <p>Brand
                                        <a href="vendors5776.html?q=LionFresh" title="">LionFresh
                                        </a>
                                    </p>
                                    <div class="product-form section">
                                        <form class="form" action="https://www.lionfresh.com/cart/add" method="post" enctype="multipart/form-data" data-product-id="5435085315">
                                            <div class="input-row">
                                                <label for="quantity">Quantity
                                                </label>
                                                <input id="quantity" class="select-on-focus" name="quantity" value="1" size="2" />
                                            </div>
                                            <a href="chicken-wings-fresh" target="_blank" style="color:#d8920c;" id="forgurgaon">
                                                <b class="gurg">Get Delivered in Gurgaon
                                                </b>
                                            </a>
                                            <!-- to show description in stream layout-->
                                            <!--       <p> </p>
                    <meta charset="utf-8">
                    <p>These parts of the chicken are fantastic to eat. They come on the bone and when roasted, grilled or barbecued, they are delicious. Skinless.</p>
                    <p>Packed Fresh &amp; Chilled</p>
                    <p> </p> -->
                                            <div class="input-row">
                                                <input type="hidden" name="id" value="16742296259" />
                                            </div>
                                            <div class="yotpo bottomLine"
                                                 data-appkey="VZD53PFkMTiDvkcahLl4P532sAhIPYlOdgOHKX0z"
                                                 data-domain="lionfrsh.myshopify.com"
                                                 data-product-id="5435085315"
                                                 data-product-models="5435085315"
                                                 data-name="Chicken Wings - Skinless (250g)"
                                                 data-url=""
                                                 data-image-url="//products/shutterstock_61008112_large.jpg%3Fv=1462589324"
                                                 data-description="&lt;p&gt; &lt;/p&gt;
                                               &lt;meta charset=&quot;utf-8&quot;&gt;
                                               &lt;p&gt;These parts of the chicken are fantastic to eat. They come on the bone and when roasted, grilled or barbecued, they are delicious. Skinless.&lt;/p&gt;
                                               &lt;p&gt;Packed Fresh &amp;amp; Chilled&lt;/p&gt;
                                               &lt;p&gt; &lt;/p&gt;"
                                                 data-bread-crumbs="meta-filter-Brand-Lionfresh;meta-filter-Cook Style-Raw;meta-filter-State-Chilled;">
                                            </div>
                                            <div class="input-row">
                                                <input type="submit" value="Add to Cart" />
                                            </div>
                                        </form>
                                    </div>
                                    <a class="more" href="chicken/products/chicken-wings-fresh.html">More Details &rarr;
                                    </a>
                                </div>
                                <script>
                                    window.productJSON.push({
                                        "id":5435085315,"title":"Chicken Wings - Skinless (250g)","handle":"chicken-wings-fresh","description":"\u003cp\u003e \u003c\/p\u003e\n\u003cmeta charset=\"utf-8\"\u003e\n\u003cp\u003eThese parts of the chicken are fantastic to eat. They come on the bone and when roasted, grilled or barbecued, they are delicious. Skinless.\u003c\/p\u003e\n\u003cp\u003ePacked Fresh \u0026amp; Chilled\u003c\/p\u003e\n\u003cp\u003e \u003c\/p\u003e","published_at":"2016-02-23T17:37:00+05:30","created_at":"2016-02-23T17:40:50+05:30","vendor":"LionFresh","type":"","tags":["meta-filter-Brand-Lionfresh","meta-filter-Cook Style-Raw","meta-filter-State-Chilled"],"price":12000,"price_min":12000,"price_max":12000,"available":true,"price_varies":false,"compare_at_price":null,"compare_at_price_min":0,"compare_at_price_max":0,"compare_at_price_varies":false,"variants":[{
                                            "id":16742296259,"title":"Default Title","option1":"Default Title","option2":null,"option3":null,"sku":"10014","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Chicken Wings - Skinless (250g)","public_title":null,"options":["Default Title"],"price":12000,"weight":250,"compare_at_price":null,"inventory_quantity":6,"inventory_management":"shopify","inventory_policy":"deny","barcode":""}
                                        ],"images":["\/\/cdn.shopify.com\/s\/files\/1\/1127\/1070\/products\/shutterstock_61008112.jpg?v=1462589324","\/\/cdn.shopify.com\/s\/files\/1\/1127\/1070\/products\/shutterstock_160320674.jpg?v=1467712187"],"featured_image":"\/\/cdn.shopify.com\/s\/files\/1\/1127\/1070\/products\/shutterstock_61008112.jpg?v=1462589324","options":["Title"],"content":"\u003cp\u003e \u003c\/p\u003e\n\u003cmeta charset=\"utf-8\"\u003e\n\u003cp\u003eThese parts of the chicken are fantastic to eat. They come on the bone and when roasted, grilled or barbecued, they are delicious. Skinless.\u003c\/p\u003e\n\u003cp\u003ePacked Fresh \u0026amp; Chilled\u003c\/p\u003e\n\u003cp\u003e \u003c\/p\u003e"}
                                    );
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pagination-row">
        </div>
    </div>
    <!-- /#content -->
    

@endsection