<?php
use App\ProductImages;
?>

@foreach($products as $product)
    <?php
      $product_image = ProductImages::where('product_id', '=', $product->id)->select('images')->first();
    ?>
    <div data-product-id="5435085315" class="product-block">
        <div class="block-inner">
            <div class="block-inner">

            @if($product->totalquantity <= 0)
                <!-- <input type="submit" class="btn btn-primary out-of-stock" value="Out of stock" style="width: 77% !important;border: none !important;" > -->
                 <div class=" out-of-stock" ><span>SOLD OUT</span> </div>
            @endif
            <div class="input-row cartbtn" >
                @if($product->totalquantity > 0)
                  <form class="add-to-cart">
                     <input type="text" name="quantity" value="1" style="display: none;">
                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                    @if(isset($product->product_images))
                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
                    @endif
                   <input type="button" onclick="addToCart(this)" class="cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                  </form>
                  @else
                    <!-- <button class="btn btn-primary" style="background: red !important; x`border-color: red !important;">Email When Available</button>  -->
                     <button class="emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Notify Me &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                   
                  @endif
                </div>
                @if(isset($product->product_images))
                <a class="image-link" href="{{URL::to('/products/'.$product->slug)}}">
                    <img src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" />
                </a>
                @endif
                <a class="hover-info" href="{{URL::to('/products/'.$product->slug)}}">
                    <div class="inner">
                        <div class="innerer">
                            <div class="title">{{$product->product_title}}
                            </div>
                           @if($product->compare_price != NULL)
                                            <span class="price" style='color:red;text-decoration:line-through'>
                                              Rs. {{$product->price}}
                                            </span> 
                                            <span class="price">
                                              Rs. {{$product->compare_price}}
                                            </span>
                                            @else
                                             <span class="price">
                                              Rs. {{$product->price}}
                                            </span>
                                            @endif
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

<script>

function addToCart(obj){
    var url = "{{URL::to('/add-to-cart')}}"
    $.ajax({
       type: "post",
       url: url,
       data: $(obj).closest('form').serialize(),
       success: function(data)
       {
            if (data.status==0) {
                alert(data.message);
                $('.update_cart_count').html('('+data.cart_count+')');
                return;
            }
            $('#cart-summary').html(data);
            var cart_count = $('.cart_count').val();
            $('.update_cart_count').html('('+cart_count+')');
            $("#cart-summary-overlay").fadeIn();
            $("#cart-summary-overlay").fadeIn("slow");
            $("#cart-summary-overlay").fadeIn(2000);
            $("#cart-summary-overlay").css({"top": "-7px"});

       }
    });
}

</script>
@endforeach