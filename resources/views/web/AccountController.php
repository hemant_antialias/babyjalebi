<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\AdminCustomerRequest;
use Auth;
use Validator;
use App\Address;
use App\User;
use App\Order;
use App\OrderDetail;
use App\Inventory;
use App\OrderRequest;
use App\PaymentStatus;
use DB;
use Cart;
use Hash;
use Mail;
use App\Http\Requests\AdminChangePasswordRequest;
class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        parent::__construct();
        
    }

    // public function getAccount()
    // {
    //     return view('web.user.account');
    // }

    public function addAddress(){


        return view('web.user.add_address');
    }

    public function editAddress($id){
        $userid = Auth::id();

       $address = Address::where('user_id', '=', $userid)->where('id', $id)->first();

        return view('web.user.edit_address', compact('address'));
    }

    public function createAddress(Request $request, $id=null){
		
//		$id = Auth::id();
		// dd($request->all());
		if ($id) {
            $address = Address::find($id);
            $success_msg = "Address updated Successfully.";
        }else{
    	    $address = new Address;
            $success_msg = "Address created Successfully.";
        }
        $address->user_id = Auth::id();
        $address->company = $request->get('company');
    	$address->address = $request->get('address');
    	$address->city = $request->get('city');
    	$address->state = $request->get('state');
    	$address->country = $request->get('country');
        $address->zip = $request->get('zip');
    	$address->notes = $request->get('notes');
		if($request->get('default_address')) {
    	   $address->is_default = $request->get('default_address');
		} else {
    	   $address->is_default = 0;		
		}
        if($request->get('billing_address')) {
            $address->is_billing = $request->get('billing_address');
        } else {
            $address->is_billing = 0;       
        }
		$address->save();
	    return redirect('/user/account')->with('success_msg', $success_msg);
    }

    public function addUserAddress(Request $request){
        if ($request->get('id')) {
        
        $address->user_id = $request->get('id');
        $address->company = $request->get('company');
        $address->address = $request->get('address');
        $address->city = $request->get('city');
        $address->state = $request->get('state');
        $address->country = $request->get('country');
        $address->zip = $request->get('zip');
        $address->notes = $request->get('notes');
            if($request->get('default_address')!="") {
                $address->is_default = $request->get('default_address');
            } else {
                $address->is_default = 0;       
            }
        $address->save();

        $data['status'] = 1;
        $data['message'] = "Address added.";

        }else{
            $data['status'] = 0;
            $data['message'] = "User not found.";
        }
    }
	
    public function userAddress(){
        
        $userid = Auth::id();

        $address = Address::where('user_id', $userid)->get(); 
        return view('web.user.user_address',compact('address'));
    }

    public function editProfile(){

        $id = Auth::id();
        $user = User::find($id);

        return view('web.user.edit_profile', compact('user'));
    }

    public function thankyou(Request $request, $ordercode){

        $name = Auth::user()->first_name;
        $last_name = Auth::user()->last_name;

        $email = Auth::user()->email;
        $phone = Auth::user()->contact;
        $userid = Auth::id();
        // $address = Address::where('user_id', '=', $userid)->where('is_default', 1)->first();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                    ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email','users.contact', 'address.address', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
                    ->first();

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();

        $user_id_detail = User::Select('total_spent' , 'total_orders')->where('id', $userid)->first();
        // dd($user_id_detail->total_spent + $order->payble_amount);
        $total_amnt = $user_id_detail->total_spent + $order->payble_amount;
        $total_order = $user_id_detail->total_orders + 1;
        $user_id_detail->total_spent = $total_amnt;
        $user_id_detail->total_orders = $total_order;
        $user_id_detail->save();
        // dd($total_amnt);

        $user_email = Auth::user()->email;
        if ($order->vendor_id == 1) {
            $oredrid = 'D'.$order->id;
        }
        if ($order->vendor_id == 3) {
            $oredrid = 'G'.$order->id;
        }

        try {
            Mail::send('mail.order.order_success', [], function($message) use($user_email, $oredrid){
                $message->from('admin@lionfresh.com');
                $message->to($user_email, "Lionfres Support")->subject("Order #".$oredrid." confirmed");
            });
        } catch (Exception $e) {
            
        }

        return view('web.checkout.thankyou', compact('name','last_name','phone', 'order', 'order_detail', 'ordercode', 'address', 'oredrid'));
    }

    public function postEditProfile(Request $request){

        $id = Auth::id();

        $user = User::find($id);
        $success_msg = "User Profile has been updated Successfully.";
        $user->first_name= $request->get('first_name');
        $user->last_name= $request->get('last_name');		
    	$user->email = $request->get('email');
    	$user->contact= $request->get('contact');
		$user->save();
        
	    return redirect('/user/account')->with('success_msg', $success_msg);
    }
    public function settings(){

        $id = Auth::id();
		$user = DB::table('users')->where('id', $id)->first();

		return view('web.user.settings',compact('user'));
    }

	public function postChangepassword(AdminChangePasswordRequest $request) {
    	
	    $id = Auth::id();
		$user = User::where('id', $id)->first();
		$old_password  	= $request->get('current_password');
		$password  		= $request->get('new_password');
		if (Hash::check($old_password, $user->password)) {

			$user->password = Hash::make($password);
			$user->save();

			return redirect('user/account')->with('success_msg', 'Your password has been changed');
		}else{
			return redirect('user/settings')->with('err_msg', 'Your old password is incorrect');
		}
    }

    public function orderListbyusrid(Request $request){

        $userid = Auth::id();
        $address = Address::where('user_id', '=', $userid)->orderBy('created_at', 'desc')->first();
        $userdetail = User::where('id', '=', $userid)->first();

        $orderlist_byuserid = DB::table('order_request')
               ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
               ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
               ->where('order_request.user_id', $userid)
               ->select('order_request.*', 'payment_status.payment_status_name', 'order_status.status_title')
               ->orderBy('order_request.created_at', 'desc')
               ->paginate(10);

        return view('web.user.account', compact('orderlist_byuserid','address','userdetail'));
    }


    public function customerInfo(Request $request, $ordercode){

        if (!Auth::check()) {
            return redirect('/login');
        }

        $name = Auth::user()->first_name;
        $email = Auth::user()->email;
        $userid = Auth::id();
        $address = Address::where('user_id', '=', $userid)
                            ->where('address_type', '=', 1)
                            ->orderBy('created_at', 'desc')
                            ->first();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email', 'users.country','users.state','users.city','users.address','users.contact', 'users.zip')
                    ->first();

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();

        return view('web.checkout.customerinfo', compact('order', 'order_detail', 'name', 'email', 'address'));
    }

    public function shippingMethod(Request $request, $ordercode){

        if (!Auth::user()) {
            return redirect('/login');
        }

        $name = Auth::user()->first_name;
        $email = Auth::user()->email;
        $userid = Auth::id();
        $address = Address::where('user_id', '=', $userid)
                            ->where('address_type', '=', 1)
                            ->orderBy('created_at', 'desc')
                            ->first();

        if (!$address) {
            return redirect('/checkout/customer-info/'.$ordercode);
        }

        $order_request = Order::where('order_code', $ordercode)->first();
        $order_request->payble_amount = $order_request->amount;
        $order_request->shipping = null;
        $order_request->custom_rate_value = null;
        $order_request->tax_rate = null;
        $order_request->coupon_code = null;
        $order_request->discount = null;
        $order_request->save();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email', 'users.country','users.state','users.city','users.address','users.contact', 'users.zip')
                    ->first();
        // dd($order);
        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price','product.tax', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();
            
        return view('web.checkout.shipping_method', compact('order', 'order_detail', 'name', 'email', 'address'));
    }

    public function paymentMethod(Request $request, $ordercode){
            
        $name = Auth::user()->first_name;
        $email = Auth::user()->email;
        $userid = Auth::id();
        $address = Address::where('user_id', '=', $userid)->first();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email', 'users.country','users.state','users.city','users.address','users.contact', 'users.zip')
                    ->first();

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.tax', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();
        // dd($order_detail);

        return view('web.checkout.payment_method', compact('order', 'order_detail', 'name', 'email', 'address'));
    }

  public function addCheckoutAddress(Request $request){

    $validator = Validator::make($request->all(), [
        // 'first_name'    => 'required',
        'shipping_address' => 'required',
        'shipping_city' => 'required',
        'shipping_state' => 'required',
        'shipping_zip' => 'required',
        'shipping_contact' => 'required',
    ]);

    if ($validator->fails()) {
        return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
    }

    $ordercode = $request->get('ordercode');
    $shipping_address = $request->get('shipping_address');
    $shipping_city = $request->get('shipping_city');
    $shipping_state = $request->get('shipping_state');
    $shipping_country = "India";
    $shipping_zip = $request->get('shipping_zip');
    $shipping_contact = $request->get('shipping_contact');
    
    $user = User::find(Auth::id());
    $user->contact = $shipping_contact;
    $user->save();

    $user_id = Auth::id();
    $address = Address::where('user_id', '=', $user_id)
                    ->orderBy('created_at', 'desc')
                    ->first();
    if ($address) {
        $address->address = $shipping_address;
        $address->city = $shipping_city;
        $address->state = $shipping_state;
        $address->country = $shipping_country;
        $address->zip = $shipping_zip;
        $address->address_type = 1;
        $address->is_default = 1;       
        $address->save();
    }else {
        $address = new Address;
        $address->user_id = Auth::id();

        $address->address = $shipping_address;
        $address->city = $shipping_city;
        $address->state = $shipping_state;
        $address->country = $shipping_country;
        $address->zip = $shipping_zip;
        $address->address_type = 1;
        $address->is_default = 1;       
        $address->save();
    }

    return redirect('/checkout/shipping-method/'.$ordercode);

  } 

  public function addCheckoutShipping(Request $request){
    // dd($request->all());
    $shipping = $request->get('shipping');
    $order_code = $request->get('ordercode');
    $total_tax = $request->get('total_tax');
    // dd($request->get('payble_amount'));
    $order_request = Order::where('order_code', '=', $order_code)->first();
    // dd($order_request);
    if ($request->get('coupon_code')) {
        $order_request->coupon_code = $request->get('coupon_code');
        $order_request->discount = $request->get('discount_price');
        $order_request->payble_amount = $request->get('payble_amount')-$request->get('total_tax');
        $order_request->save();
    }
    if ($request->get('notes')) {
        $order_request->notes = $request->get('notes');
    }
    if ($request->get('total_tax')) {
        $order_request->tax_rate = $request->get('total_tax');
    }
    if ($shipping == 'custom') {
        $order_request->shipping = 'custom';
        $order_request->custom_rate_value = 50;
        if ($request->get('coupon_code')) {
            $order_request->payble_amount = $request->get('payble_amount') + 50;
        }else{
            $order_request->payble_amount = $order_request->payble_amount +$request->get('total_tax')+ 50;
        }
    }else{
        $order_request->payble_amount = $order_request->payble_amount +$request->get('total_tax');
        $order_request->shipping = 'free';
    }
    $order_request->shipping_address_id = $request->get('shipping_address');
    $order_request->save();
    // dd($order_request);
    return redirect('/checkout/payment-method/'.$order_code);
  }

  public function completeOrder(Request $request){
    // dd($request->all());
    if ($request->get('bill_address') == 2) {
        $validator = Validator::make($request->all(), [
            'billing_address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }  
    }
    $payment_method = $request->get('payment_method');
    $order_code = $request->get('ordercode');
    $order_request = Order::where('order_code', '=', $order_code)->first();

    $order_detail = OrderDetail::where('order_id', $order_request->id)->get();
    foreach ($order_detail as $detail) {
        $product_id = $detail->product_id;
        $quantity = $detail->quantity;
        $store_id = $detail->vendor_id;

        // $inventory = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();
        $inventory = DB::table('inventory')
                        ->join('product', 'inventory.product_id', '=', 'product.id', 'left')
                        ->where('inventory.store_id', $store_id)
                        ->where('inventory.product_id', $product_id)
                        ->select('inventory.*', 'product.product_title')
                        ->first();
        
        if ($inventory->quantity == 0) {
            return redirect('/cart')->with('oot_message', 'Sorry '.$inventory->product_title.' is no longer available.We will notify you when the product is back in stock. Sorry for the Inconvenience.');
        }

        if ($inventory->quantity < $quantity) {
            return redirect('/cart')->with('oot_message', 'Sorry '.$inventory->product_title.' is only '.$inventory->quantity.' quantity left in Stock. Please change the quantity in your cart. We will notify you when more stock is available.');
        }
    }

    // Save billing address
    if ($request->get('bill_address') == 2) {
        $address = new Address;
        $address->user_id = $order_request->user_id;
        $address->address = $request->get('billing_address');
        $address->country = "India";
        $address->state = $request->get('state');
        $address->city = $request->get('city');
        $address->zip = $request->get('zip');
        $address->phone = $request->get('phone');
        $address->address_type = 1;
        $address->is_billing = 1;
        $address->save();
        $order_request->billing_address_id = $address->id;

    }
    if ($request->get('bill_address') == 1) {
        $order_request->billing_address_id = $order_request->shipping_address_id;
    }

    if ($payment_method == 'netbanking') {
        return redirect('/cpayment?ordercode='.$order_code);
    }

    if ($payment_method == 'paytm') {
        return redirect('/payment?ordercode='.$order_code);
    }

    if ($payment_method == 'cod') {
        $order_request->order_status = 1;
        $order_request->order_from = COD;
        $order_request->status = 5;


    }

    $order_request->save();

    $order_detail = OrderDetail::where('order_id', $order_request->id)->get();
    foreach ($order_detail as $detail) {
        $product_id = $detail->product_id;
        $quantity = $detail->quantity;
        $store_id = $detail->vendor_id;

        $inventory = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();
        if ($inventory) {
            $inventory->quantity = $inventory->quantity - $quantity;
            $inventory->save();
        }
    }

   
        
    
    Cart::destroy();

    return redirect('/checkout/thankyou/'.$order_code);
  }

}