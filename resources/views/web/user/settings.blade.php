@extends('web.web_layout')

@section('content')
<style>
	.editpage
	{

    margin-left: auto;
    margin-right: auto;
    width: 40%;
	}
	input, textarea
	{
		width: 100%;
	}
</style>
@if(Session::has('success_msg'))
                <p class="succolor">{{Session::get('success_msg')}}</p>
            @endif
            @if(Session::has('err_msg'))
    <p class="errorcolor">{{Session::get('err_msg')}}</p>
@endif
<div class="main-content">
		<div class="main-content-inner">
			
			<br>
			<div class="row">
				<div class="col-xs-12 editpage">
					<h3>Change Password</h3>
						{{ Form::open(['url' => '/user/change-password', 'method' => 'post', 'class' => 'form-horizontal']) }}
						
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Current Password </label>

							<div class="col-sm-9">
								<input type="password" name="current_password" value="{{ old('current_password') }}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
								@if ($errors->has('current_password'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('current_password') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> New Password </label>

							<div class="col-sm-9">
								<input type="password" name="new_password" id="form-field-1" value="{{ old('new_password') }}" placeholder="" class="col-xs-10 col-sm-5" />
								@if ($errors->has('new_password'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('new_password') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Confirm New Password </label>
							
							<div class="col-sm-9">
								<input type="password" name="confirm_new_password" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
								@if ($errors->has('confirm_new_password'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('confirm_new_password') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
						<br>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
						<br><br>
					{{ Form::close() }}
				</div>
				
				

				<?php /*?><div class="col-xs-12">
					<div class="deactivate-section" style="clear:both">
						<h4 class="sub-heading">Deactivate Account</h4>	
							{{Form::open(array('url' => 'admin/deactivate', 'method' => 'post', 'id' => 'deactivate'))}}
							<div class="item block-level">
								<label>I understand that deactivating my account will also deactivate my all products, profile & other information.</label>
					        	
							</div>

							<div class="modal fade" id="deactivateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								  <div class="modal-dialog">
								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								        <h4 class="modal-title" id="myModalLabel">Deactivate Account</h4>
								      </div>
								      	<div class="modal-body">
								      	<div class="block-level row">
								      		<div class="col-xs-6"><label>Current Password:</label></div>
								      		<div class="col-xs-6">
								      		{{  Form::password('password', array('class' => 'password', 'placeholder'=>'Current Password','required' => 'required')) }}
								      		</div>
								    		 	
							      		</div>
							      		<br>
								      		<h5 id="body_text"></h5>
								        	<h5 >I understand that deactivating my account will also deactivate my all products, profile & other information.</h5>				      
								  		</div>
								       <div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								        <button type="submit" class="btn btn-danger" id="deleteAccount" style="margin-right:10px">Deactivate Account</button>
								      </div>
								    </div>
								  </div>
							</div>
							{{ Form::close() }}
							{{ Form::button('Deactivate My Account', array('name' => 'delete_account', 'class' => 'btn submit-bttn singn-up-bttn', 'id' => 'deactivateaccount')) }}
					</div>
				</div><?php */?>
			</div>
		</div>
	</div>
@endsection
