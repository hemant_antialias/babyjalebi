@extends('web.web_layout')

@section('content')
<link rel="stylesheet" type="text/css" href="{{URL::to('css/meat_products.css')}}">
<style type="text/css">
    .out-of-stock{
        z-index: 1 !important;
    background: black !important;
    color: #fff !important;
    display: block !important;
    /* border-bottom-right-radius: 15px !important; */
    position: absolute;
    z-index: 999;
    text-align: center;
    /* border-bottom-left-radius: 15px !important; */
    width: 41% !important;
    margin-left: 58% !important;
    margin-right: auto !important;
    margin-top: 5%;
    padding-top: 4px;
    padding-bottom: 4px !important;
    font-weight: 500;
    /* font-size: 11px !important; */
}

.meantnotifyme{
    margin-bottom: -40% !important;
}
    @media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px)  { /* IPAD STYLES GO HERE */
   
   .collection-listing .product-block {
    height: auto;
    width: 226px !important;
    padding-left: 1%;
    margin-left: 1%;
    margin-right: 1%;
    padding-right: 1%;
    min-width: 0;
}
    
}
  @media only screen and (max-width: 767px){
    .collection-listing .product-block {
    height: auto;
    width: 50% !important;
    padding-left: 1%;
    padding-right: 1%;
    min-width: 0;
}
     .meatoutofstock{
        width: 52% !important;
	margin-left:48% !important;
    }
    .leftqnty {
    font-size: 10px !important;
}
}


@media only screen and ( max-width: 360px){

    .cartmeatmob{
        margin-top: 5% !important;
    }

.leftqnty{
  font-size: 12px !important;
}
.out-of-stock{
    margin-left: 48% !important;
}
@media only screen and ( max-width: 412px){
    .cartmeatmob{
        margin-top: 4% !important;
    }

}
@media only screen and ( max-width: 414px){
    .cartmeatmob{
        margin-top: 4% !important;
    }

}
@media only screen and ( max-width: 375px){
    .cartmeatmob{
        margin-top: 5% !important;
    }
}


</style>

<?php  
use App\ProductImages;
?>
  

<div class="bootstrap-iso">



    <div class="container" id="content">
        <div >
            <div class="col-md-9 col-md-offset-3 col-xs-12 col-sm-12 ">
                <h1 class="majortitle fontcorrect" style="text-align: center;">
                    {!!html_entity_decode($meat_types->name)!!}
                </h1>
                <div class="user-content lightboximages">
                    <div style="text-align: center; padding-left: 30px;">
              <span style="color: #666666;"> 
              </span>
                    </div>
                    <div style="text-align: center; padding-left: 30px;">{!!html_entity_decode($meat_types->description)!!}
                  
                    <br>
                   
                </div>
               
                <!-- <div class="social-area">
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.social-row">Share
              <span class="state">+
              </span>
                    </a>
                </div> -->
            </div>
            <!-- /.page-header -->
        </div>
        <!-- /.container -->
        <div class="multi-tag-row">
            <div class="tags nav-row social-row height-hidden hidden">
                <div class="">
                    <!-- <div class="social">
              <span class="socitem label">Share:
              </span>
                        <div class="socitem twittercont">
                            <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet
                            </a>
                        </div>
                        <div class="socitem facebookcont">
                            <div class="fb-like" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false">
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            
        <!-- /.multi-tag-row -->
        <div class="">

        <!-- Fiters Section -->
        @include('web.filter')

        <div class="thumbholder col-md-9 col-xs-12 col-sm-12 ">

            <div class="collection-listing ">
                <div class="product-list">
                    @foreach($products as $product)
                    <?php
                      $product_image = ProductImages::where('product_id', '=', $product->id)->select('images')->first();
                    ?>
                    <div data-product-id="5435085315" class="ipadblock chickenproductblock product-block product-block-mob">
                        <div class="block-inner">
                         @if($product->totalquantity <= 0)
                                 <!--    <input type="submit" class="btn btn-primary out-of-stock" value="Out of stock" style="width: 77% !important;border: none !important;" id="outofstock1"> -->
                                  <div class=" out-of-stock" ><span class="soldout">SOLD OUT</span> </div>
                                    @endif
                                    @if($product->totalquantity <= 5 &&  $product->totalquantity > 0)
                                    <div class="meatoutofstock out-of-stock" style="background: #dcaa31 !important;"><span class="leftqnty">Only {{$product->totalquantity}} Left!</span> </div>
                                   <!--  <input type="submit" class="marginleftchangeindex paddingbot somemarginless btn btn-primary out-of-stock" style="background: #f08035 !important" value="Only {{$product->totalquantity}} Left!" > -->
                                    @endif
                            <div class="input-row cartbtn " >
                                @if($product->totalquantity > 0)
                                  <form>
                                     <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                                    @if(isset($product->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
                                    @endif
                                    <input type="submit" class="cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmeatmob cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                                  </form>
                                  @else
                                       <!--  <button class ="cartmobhide btn btn-primary emailbtn myn" data-id="{{$product->id}}" style="background: red !important;border-color: red !important; white-space: normal;  ">Email When Available</button>  -->
                                        <button class="meantnotifyme emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Notify Me &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button> 
                                  @endif
                                </div>

                            <div class="image-cont">
                                @if(isset($product->product_images))

                                <a class="image-link" href="{{URL::to('/products/'.$product->slug)}}">
                                    <img class="blockimginner" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}"  />
                                </a>
                                @endif

                                <div class="inner">
                                        <div class="innerer somelineheight">
                                            <a href="{{URL::to('/products/'.$product->slug)}}" class="title meat_title">{{$product->product_title}}
                                            </a>
                                            <br>
                                            <span class="price">Rs.</span>
                                            @if($product->compare_price != 'NULL')
                                            <span class="price" style='color:red;text-decoration:line-through'>
                                              {{$product->compare_price}}
                                              
                                            </span> 
                                            <span class="price">
                                               {{$product->price}}
                                            </span>
                                            @else
                                             <span class="price">
                                              {{$product->price}}
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                            </div>

                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="container pagination-row">
        </div>
    </div>
    <!-- /#content -->
</div><!--bootstrap-iso ends here -->
</div></div>
@endsection




