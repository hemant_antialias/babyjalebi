<?php
  if (!isset($cities)) {
    $cities = null;
  }
?>

<style type="text/css">
 @media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px)  { /* IPAD STYLES GO HERE */
    
    .social-linkpad{
        display: none !important;
    }
    .registerforipad{
        margin-top: 6% !important;
    }
}
    
    .web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: 0.9;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 9999;
   display: none;
}
.form-control {
    display: block;
    width: 91%;
    height: 50px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555; 
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 0px;
   
    
}
.form-control:focus {
    border-color: #66afe9;
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, .6);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, .6)
}
.form-control::-moz-placeholder {
    color: #999;
    opacity: 1
}
.form-control:-ms-input-placeholder {
    color: #999
}
.form-control::-webkit-input-placeholder {
    color: #999
}
.form-control::-ms-expand {
    background-color: transparent;
    border: 0
}
.form-control[disabled],
.form-control[readonly],
fieldset[disabled] .form-control {
    background-color: #eee;
    opacity: 1
}
.form-control[disabled],
fieldset[disabled] .form-control {
    cursor: not-allowed
}
textarea.form-control {
    height: auto
}

.web_dialog
{
   display: none;
       
    position: fixed;
     width: 40%;
    top: 24%;
    padding-top: 70px;
    padding-left: 30px;
    padding-right: 40px;
    padding-bottom: 40px;
    left: 28%;
    
    background-color: #ffffff;
    border: 2px solid #b87c0a;
    
    z-index: 9999;
    
    font-size: 10pt;
    height: 30%;
}
.web_dialog_title
{
   border-bottom: solid 2px #b87c0a;
   background-color: #b87c0a;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
.js-example-basic-multiple{
    float: left;
    margin-left: 5px;
    margin-right: 5px;
    
    width: 30%;
}
#popsubmit{
    background: #b87c0a !important;
    border: none;
    color: #fff;
    font-size: 14px;
    line-height: 12px;
    border-radius: 0px !important;
    height: auto;
    text-decoration: none!important;
    text-transform: capitalize;
    cursor: pointer;
    vertical-align: middle;
    width: 27%;
    text-align: center;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    -moz-transition: background-color 100ms color 100ms border-color 100ms;
    -webkit-transition: background-color 100ms color 100ms border-color 100ms;
    transition: background-color 100ms color 100ms border-color 100ms;
    

    letter-spacing: 1px;
        margin: 0px;
    height: 20px;
 
    text-transform: uppercase;
}
.locdialog{
    /*float: right;*/
    margin-top: 40px;
    color: #57555d;
}
.popup-closer-location{
     position: absolute;
   top: 0%;
   float: right;
   text-align: right;
   margin-left: 91%;
   cursor: pointer;
}
.displaynone
  {
    display: none !important;
  }
.notforweb{ display: none !important; }
</style>    
<style type="text/css">
  @media screen and (max-width: 580px) {
    .notforweb{ display:block !important ; }

      .web_dialog
    {
    margin-left: auto;
    top: 5% !important;
    height: 50%;
    margin-right: auto;
    width: 82%;
    left: 0%;
        
    }
    #city {
      width: 90% !important;
    }
      #store_id
{
      width: 90% !important;
    margin-top: 10px;
    margin-left: 6px;
}
    #popsubmit {
      
        margin-top: 20px;
    margin-left: 1%;
    margin-right: 0%;
    width: 80%;
    }
    .popup-closer-location {
    position: absolute;
    top: 2%;
    float: right;
    text-align: right;
    margin-left: 85%;
    cursor: pointer;
}
.fa-map-marker { float: left;
    margin-top: 4%;     margin-right: 2%;
    margin-left: 2%; }

#mobmenclosebtn{ 
          position: absolute;
          display: block !important;
          top: -1px;
  }


}

#mobmenclosebtn{ 
          display: none;
  }
#closemobmenubtn{ 
      height: 30px;
    width: 42px;
    position: absolute;
    top: 0px;
    background: #d8920c;
    right: -43px;
    text-align: center;
 }
 
  .compact{ display: block; }

  
</style>    


        <div class="multi-level-nav">
           <button class="mobile-nav-toggle button compact" id="mobmenclosebtn">X</button>
            <div class="tier-1">
                <ul data-menu-handle="main-menu">
                    <a class="mobhidelog" href="{{URL::to('/home')}}" title="Lion Fresh Delhi">
                        <img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="Lion Fresh Delhi" style="width:85%;">
                    </a>
                    <li class="active btnShowSimple">
                        <i class="fa fa-map-marker " aria-hidden="true"></i><a href="javascript:void(0)">
                        @if(Session::has('store'))
                            {{Session::get('store')}}
                        @endif
                        </a>
                    </li>
                    <li>
                        <a href="">SHOP BY MEAT
                        </a>
                        <ul data-menu-handle="shop-by-meat">
                            @foreach($meat_type as $type)
                            @if($type->status == 1)
                            <li>
                                <a href="{{URL::to('/collections/'.$type->slug)}}" style="padding-right: 40px;">{{$type->name}}
                                </a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <a href="">SHOP BY CATEGORY
                        </a>
                        <ul data-menu-handle="shop-by-category">
                            @foreach($categories as $category)
                            @if($category->status == 1)
                            <li>
                                <a href="{{URL::to('/collections/'.$category->slug)}}" style="padding-right: 40px;">{{$category->name}}
                                </a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <a href="">SHOP BY BRAND
                        </a>
                        <ul data-menu-handle="shop-by-brand">
                            @foreach($brandname as $brand)
                            
                            <li>
                                <a href="{{URL::to('/collections/'.$brand->slug)}}" style="padding-right: 40px;">{{$brand->name}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    
                    <?php 
                      $blogcategory = DB::table('blog_category')->select('slug','category')->get();
                    ?>
                    @foreach($blogcategory as $blogcat)
                    @if ($loop->last)
                        <?php
                            $class = "displaynone";
                        ?>
                    @else
                        <?php
                            $class = "";
                        ?>
                            @endif
                    <li>
                        <a href="{{URL::to('/blog/'.$blogcat->slug)}}" class="{{$class}}">{{$blogcat->category}}
                        </a>
                    </li>
                    @endforeach
                   
                    
                    
                   
                    </li>
                   @if (Auth::guest())
                    <li class="account-links notforweb registerforipad" style="display: -webkit-box;">
                    <span class="register">
                        <a href="{{url('/register')}}" id="customer_register_link" style="color: #000;">REGISTER
                        </a>
                    </span>
                    <br>
                    <span class="login">
                        <a href="{{url('/login')}}" id="customer_login_link" style="color: #000;">LOG IN
                        </a>
                    </span>
                    <span class="login">
                        <a href="{{url('/cart')}}" class="cart-count " style="color: #fff; cursor: pointer;" id="cartcss">
                       Cart <i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="update_cart_count">({{Cart::count()}})</span>
                        </a>
                    </span>
                    <span>
                        <!-- <input type="submit" name="submit" id="submits"> -->
                    </span>
                    
                    </li>
                    @else
                    <li class="dropdown notforweb" style="display: -webkit-box;text-transform: capitalize;">
                        <span class="login">
                            <a href="{{url('/user/account')}}" id="customer_login_link" >{{ Auth::user()->first_name }}
                            </a>
                        <span class="login">
                            <a href="{{url('/logout')}}" id="customer_login_link">Logout
                            </a>
                        </span>
                        
                    </li>
                    @endif
                </ul>

                <div class="social-links">
                    <ul class="social-linkpad">
                        <li class="facebook">
                            <a href="https://www.facebook.com/LionFreshOnline/" target="_blank" title="Facebook">Facebook
                            </a>
                        </li>
                        <li class="twitter">
                            <a href="https://twitter.com/Lionfreshonline" target="_blank" title="Twitter">Twitter
                            </a>
                        </li>
                        <li class="instagram">
                            <a href="https://www.instagram.com/lionfresh_online/" target="_blank" title="Instagram">Instagram
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- popup code by aj -->
       
        <div id="overlay" class="web_dialog_overlay"></div>
    
<div id="dialog" class="web_dialog">
<div class="popup-closer-location"><i class="fa fa-close fa-1x"></i></div>
    <form method="get" action="{{URL::to('/home')}}">
    <h4 class="locdialog">Want to change your location ?</h4>
            <select class="js-example-basic-multiple form-control" id="city" name="city">
              <option value="">Select City</option>
              @foreach($cities as $city)
                <option value="{{strtolower($city->city_id)}}">{{$city->city}}</option>
              @endforeach
            </select>
           
              <!-- @foreach($cities as $city)
                <option value="{{strtolower($city->city_id)}}">{{$city->city}}</option>
              @endforeach -->
            
             <span class="store-types" style="
    margin-right: 2%;
">
              
            </span>
             <select class="js-example-basic-multiple form-control static" id="city" name="city" >
              <option value="">Select Locality</option>
              <span class="store-types">
              
            </span>
            </select>
           <input type="submit" id="popsubmit" ><div class="clearfix"></div>
           
    </form>
   
</div>
<script type="text/javascript">

   $(document).ready(function ()
   {
      $(".btnShowSimple").click(function (e)
      {
         ShowDialog(false);
         e.preventDefault();
      });

      $("#btnShowModal").click(function (e)
      {
         ShowDialog(true);
         e.preventDefault();
      });

      $("#closemobmenubtn").click(function (e)
      {
         HideDialog();
         e.preventDefault();
      });

      $("#btnSubmit").click(function (e)
      {
         var brand = $("#brands input:radio:checked").val();
         $("#output").html("<b>Your favorite mobile brand: </b>" + brand);
         HideDialog();
         e.preventDefault();
      });

   });

   function ShowDialog(modal)
   {
      $("#overlay").show();
      $("#dialog").fadeIn(300);

      if (modal)
      {
         $("#overlay").unbind("click");
      }
      else
      {
         $("#overlay").click(function (e)
         {
            HideDialog();
         });
      }
   }

   function HideDialog()
   {
      $("#overlay").hide();
      $("#dialog").fadeOut(300);
   } 
        
</script>
<script type="text/javascript">
  var base_url = '{{URL::to('/')}}';

  // Get the all stores by city wise
  $('#city').change(function(){
    var city_id = $('#city').val();
    // alert(city_id);
    $.ajax({
        url: base_url+'/all-stores-by-city',
        type: "post",
        data: 
        {
          "city_id": city_id,
        },
        success:function(data)
        {
          // alert(data);
          $('.static').hide(); 
          $('.store-types').html(data);
        }
    });
  });

</script>
<script type="text/javascript">
 $('.popup-closer-location').click(function() {
 $('#dialog, #overlay').hide();
});
</script>