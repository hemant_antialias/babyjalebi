@extends('web.web_layout')

@section('content')
<?php  
use App\ProductImages;
use App\Product;
?>
<link rel="stylesheet" type="text/css" href="{{URL::to('css/index.css')}} ">
<script type="text/javascript">
var google_tag_params = {
ecomm_prodid: "",
ecomm_pagetype: "home",
ecomm_totalvalue: ,
};
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 884057952;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/884057952/?guid=ON&amp;script=0"/>
</div>
</noscript>
<style>
.paleftspace{
    padding-left: 18px;
}
    .full-width-image
    {
        /*height: 400px;*/
    }
    .viewarticleblog{
            text-align: center;
    margin-top: 6%;
    }
    .cnt{ text-align: center; }
    /* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {   
    background-color: #fefefe;
    margin: auto;
    padding-top: 40px;
    border: 1px solid #888;
    width: 26%;
    height: 15%;
    margin-top: 8%;
    padding-left: 74px;
    border-radius: 8px;
    padding-right: 0px;
}

/* The Close Button */
.close {
        margin-bottom: 21%;
    margin-top: -42px;
    margin-right: 2px;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.somemarginless{
    margin-left: 36.5% !important;
}
@media only screen and (max-width: 768px){
    .indexoutofstock{
        width: 48% !important;
    }
    .leftqnty{
  font-size: 12px !important;
}
.cartmobicon {
    display: block !important;
    background: black;
    color: white;
    position: relative;
    padding-top: 2px;
    padding-bottom: 3px;
    width: 20%;
    float: right;
    margin-right: -1% !important;
    margin-top: 3% !important;

}
}
.emailwhenavailbtncss {
    background: black !important;
    /* border-color: #839683 !important; */
    letter-spacing: 0.9px;
    font-size: 12px !important;
    margin-left: 0px !important;
    margin-bottom: -72% !important;
    height: 31px !important;
    padding-top: 2px !important;
    padding-bottom: 2px !important;
    width: 88% !important;
}
.paddingbot{
    padding-bottom: 13px !important;
}
.out-of-stock{
    margin-left: 48% !important;
}
.marginleftchangeindex{
    margin-left: 36.5% !important;
}
@media only screen and (width: 412px){
    .cartmobicon{
        margin-top: 11% !important;
    }
    .out-of-stock{
    font-size: 10px !important;
    width: 48% !important;
}
}
@media only screen and (width: 375px){
    .cartmobicon{
        margin-top: 6% !important;
    }
    .out-of-stock{
    font-size: 10px !important;
    width: 48% !important;
}
}
@media only screen and (width: 414px){
    .cartmobicon{
        margin-top: 12% !important;
    }
    .out-of-stock{
    font-size: 10px !important;
    width: 48% !important;
}
}
.soldout{
    font-size: 10px !important;
}
.out-of-stock{
    font-size: 10px !important;
}
.emailwhenavailbtncss{
    margin-bottom: -41%!important;
    width: 88% !important;
}
@media only screen and ( width: 360px){
    .out-of-stock{
    margin-left: 5%;
}
}
.emailwhenavailbtncss {
    margin-bottom: -41%!important;
    width: 81% !important;
}
.leftqnty{
  font-size: 13px;
}
</style>
        <!-- Main slideshow -->
        <div class="slideshow">
            <div class="flexslider auto-play scaled-text-base">
                <ul class="slides">
                    @foreach($banners as $banner)
                    <li class="slide">
                        <a href="{{$banner->link}}">
                            <img src="{{URL::to('banner_images/'.$banner->city_id.'/'.$banner->image_name)}}" alt="" />
                        </a>
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
        <div class="collection-slider-row is-showing-collections">
            <div class="collection-slider">
                <h1 class="hometitle h4-style align-center has-paging">
                    <span>Browse by Category
                    </span>
                </h1>
                <div class="collection-listing paleftspace">
                    <div class="product-list">
                        <!-- /.product-block -->

                        @foreach($product_categories as $category)
                        
                        <div class="product-block collection-block" id="detailcont">
                            <div class="block-inner">

                                <div class="image-cont">
                                    <a class=" href="{{URL::to('/collections/'.$category->slug)}}">
                                        <a href="{{URL::to('/collections/'.$category->slug)}}" title="{{$category->name}}">
                                            @if($category->image)    
                                                <img src="{{URL::to('/product_categories/'.$category->image)}}" alt="{{$category->name}}" />
                                            @else
                                                <img title="No Image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="No Image" style="width: 50px;">
                                            @endif
                                        </a>    `
                                    </a>
                                    <a class=" href="{{URL::to('/collections/'.$category->slug)}}">
                              <span class="productlabel general">
                                <span>
                                  <span class="title">{{$category->name}}
                                  </span>
                                <?php  
                                    $store_id = Session::get('store_id');

                                    $count = Product::where('product_category', $category->id)->whereIn('vendor_id', [$store_id, 0])->count();
                                ?>
                                @if($count > 0)
                                <span class="count">{{$count}} products
                                </span>
                                @else
                                <span class="count">No products yet.
                                </span>
                                @endif
                                </span>
                              </span>
                                    </a>
                                </div>
                                <!-- /.image-cont -->
                            </div>
                            <!-- /.block-inner -->
                        </div>
                        @endforeach
                        <!-- /.product-block -->
                    </div>
                </div>
            </div>
            <!-- /.collection-listing -->
        </div>
        
        <!-- /.container -->
        <!-- Slot B -->
        <!-- Welcome text -->
        <div class="container padded-row layout-content-with-image-right cf">
            <div class="content user-content">
                <h1 class="cnt majortitle in-content">About Us
                
                </h1>
                LionFresh.com
                  <span> delivers high quality meats and seafood safely and
                    <strong>hygienically packed
                    </strong> directly to you. We believe that fresh ingredients and high quality food lead to nutritious, delicious meals. We travel near and far to bring our customers the very best — right to their front doors.
                  </span>
                <br>
                <br>
                  <span>From our
                    <strong>own state of the art processing unit
                    </strong> to our relationships with farms around the world,
                  </span>LionFresh.com
                  <span> brings you a choice of products like never before.
                  </span>
                <p> 
                </p>
                <ul>
                    <li>
                      <span style="line-height: 1.5;">Order online or via dedicated hotline
                      </span>
                    </li>
                    <li>
                      <span style="line-height: 1.5;">Hygienically prepared and vacuum packed
                      </span>
                    </li>
                    <li>
                      <span style="line-height: 1.5;">Delivering everything from high end imported meats, cold cuts and sausages to our famous smoked bacon.
                      </span>
                    </li>
                    <li>
                      <span style="line-height: 1.5;">High quality seafood
                      </span>
                    </li>
                    <li>
                      <span style="line-height: 1.5;">Slow Cooked meats using ‘Sous Vide’ techniques
                      </span>
                    </li>
                    <ul>
                    </ul>
                    <ul>
                    </ul>
                </ul>
            </div>
            <div class="image-cont">
                <img src="{{URL::to('css/assets/home_welcome_image9a04.jpg?14144647724873477262')}}" alt="High Quality Meats" />
            </div>
        </div>
        <!-- Slot G -->
        <!-- Collection slider -->
        <div class="collection-slider-row collection-slider-3">
            <div class="collection-slider">
                <h1 class="hometitle h4-style align-center">
                    <a href="{{URL::to('/collections/la-carne-cuts')}}">
                      <span>LA CARNE CUTS
                      </span>
                    </a>
                </h1>
                <div class="view-all align-center">
                    <a href="{{URL::to('/collections/la-carne-cuts')}}">See All
                    </a>
                </div>
                <div class="collection-listing paleftspace">
                    <div class="product-list">
                    @foreach($lacarne_products as $product)
                   
                        <div data-product-id="5160999875" class="product-block" id="detailcont">
                            <div class="block-inner">
                            @if($product->totalquantity <= 0)
                                    <!-- <input type="submit" class="paddingbot somemarginless btn btn-primary out-of-stock" value="Out of stock" > -->
                                    <div class=" out-of-stock" ><span class="soldout">SOLD OUT</span> </div>
                                    @endif
                                    @if($product->totalquantity <= 5 &&  $product->totalquantity > 0)
                                    <div class="indexoutofstock out-of-stock" style="background: #dcaa31 !important;"><span class="leftqnty">Only {{$product->totalquantity}} Left!</span> </div>
                                    
                                    @endif
                            <div class="input-row cartbtn" >
                            @if($product->totalquantity > 0)
                                  <form>
                                     <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                                    @if(isset($product->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
                                    @endif
                                    
                                     <input type="submit" class="cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                                  </form>
                                   @else
                                    <button class="emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Notify Me &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button> 
                                    
                                  @endif
                                </div>

                                <div class="image-cont" id="producthover">
                                    @if(isset($product->product_images))
                                        <a class="image-link" href="{{URL::to('/products/'.$product->slug)}}">
                                            <img src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="American Style Bacon - Imported (150g) - Lion Fresh - 1" />
                                        </a>
                                    @else
                                        <a class="image-link" href="{{URL::to('/products/'.$product->slug)}}">
                                            <img title="No Image" class="block aspect-ratio__content" src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="No Image" style="width: 50px;">
                                        </a>
                                    @endif
                                    
                                    <a class="hover-info" href="{{URL::to('/products/'.$product->slug)}}">
                                        <div class="inner">
                                            <div class="innerer">
                                                <div class="title">{{$product->product_title}}
                                                </div>
                                 <span class="price">Rs.
                                            @if($product->compare_price != 'NULL')
                                            <span class="price" style="color:black;text-decoration:line-through;padding-right: 2%;">
                                              {{$product->compare_price}}
                                              
                                            </span> 
                                            <span class="price">
                                               {{$product->price}}
                                            </span>
                                            @else
                                             <span class="price">
                                              {{$product->price}}
                                            </span>
                                            @endif
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                      @endforeach
                    </div>
                </div>
            </div>
            <!-- /.collection_slider -->
            <div class="collection-listing slider-collection-listing">
                <div class="product-detail">
                </div>
            </div>
        </div>
        <!-- Slot C -->
        <div class="slide full-width-image">
            <div class="scaled-text-base">
                
                    <a href="{{URL::to('/collections/chicken')}}"><img src="{{URL::to('css/assets/home_ad_19a04.jpg?14144647724873477262')}}" alt="" /></a>
                
            </div>
        </div>
        <!-- Slot E -->
        <!-- Collection slider -->
        @if(isset($howdyji_products))
        <div class="collection-slider-row collection-slider-1">
            <div class="collection-slider">
                <h1 class="hometitle h4-style align-center">
                    <a href="{{URL::to('/collections/howdyji')}}">
                      <span>HOWDYJI
                      </span>
                    </a>
                </h1>
                <div class="view-all align-center">
                    <a href="{{URL::to('/collections/howdyji')}}">See All
                    </a>
                </div>
                <div class="collection-listing paleftspace">
                    <div class="product-list">
                    @foreach($howdyji_products as $howdyji_product)
                    
                        <div data-product-id="5160918723" class="product-block" id="detailcont">
                            <div class="block-inner">
                             @if($howdyji_product->totalquantity <= 0)
                                    <!-- <input type="submit" class="paddingbot somemarginless btn btn-primary out-of-stock" value="Out of stock" > -->
                                    <div class=" out-of-stock" ><span>SOLD OUT</span> </div>
                                    @endif
                                    @if($howdyji_product->totalquantity <= 5 &&  $howdyji_product->totalquantity > 0)
                                    <div class=" out-of-stock" style="background: #dcaa31 !important;"><span class="leftqnty">Only {{$howdyji_product->totalquantity}} Left!</span> </div>
                                    @endif
                            <div class="input-row cartbtn">
                            @if($howdyji_product->totalquantity > 0)
                                  <form>
                                     <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$howdyji_product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$howdyji_product->id}}" class="product_id">

                                    <input type="hidden" name="product_price" value="{{$howdyji_product->price}}" class="product_price">
                                    @if(isset($howdyji_product->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$howdyji_product->id.'/featured_images/'.$howdyji_product->product_images)}}" alt="{{$howdyji_product->product_title}}" class="image_url">
                                    @endif

                                   <input type="submit" class="cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                                  </form>
                                    @else
                                  <!--   <button class="btn btn-primary myn" data-id="{{$howdyji_product->id}}" style="background: red !important;">Email When Available</button> 
 -->                                    <!-- <button class="emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$howdyji_product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Email when Available &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button> -->
                                    <button class="emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$howdyji_product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Notify Me &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button> 
                                    @endif
                                </div>
                                <div class="image-cont">
                                    @if(isset($howdyji_product->product_images))
                                    <a class="image-link" href="{{URL::to('/products/'.$howdyji_product->slug)}}">
                                        <img src="{{URL::to('/product_images/'.$howdyji_product->id.'/featured_images/'.$howdyji_product->product_images)}}" alt="{{$howdyji_product->product_title}}" />
                                    </a>
                                    @endif
                                    <a class="hover-info" href="{{URL::to('/products/'.$howdyji_product->slug)}}">
                                        <div class="inner">
                                            <div class="innerer">
                                                <div class="title">{{$howdyji_product->product_title}}
                                                </div>
                                        <span class="price">Rs.
                                            @if($howdyji_product->compare_price != 'NULL')
                                            <span class="price" style="color:black;text-decoration:line-through;padding-right: 2%;">
                                              {{$howdyji_product->compare_price}}
                                              
                                            </span> 
                                            <span class="price">
                                               {{$howdyji_product->price}}
                                            </span>
                                            @else
                                             <span class="price">
                                              {{$howdyji_product->price}}
                                            </span>
                                            @endif
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                    @endforeach
                      </div>
                </div>
            </div>
            <!-- /.collection_slider -->
            <div class="collection-listing slider-collection-listing">
                <div class="product-detail">
                </div>
            </div>
        </div>
        @endif
        <!-- Slot D -->
        <!-- Blog slideshow -->
        <div class="container hideonmobile padded-row">
        <div class="layout cf">
            @foreach($blogs as $blog)
           
                @if ($loop->first)
                    <?php
                        $class = "left";
                    ?>
                @else
                    <?php
                        $class = "";
                    ?>
                @endif
                <div class="col-third user-content {{$class}} spaced-row">
                    <div class="article-image">
                        <a href="{{URL::to('/blog/'.$blog->category.'/'.$blog->slug)}}"" title="">
                            <img src="{{URL::to('blog/'.$blog->id.'/'.$blog->image)}}" alt="{{URL::to('/blog/'.$blog->category.'/'.$blog->slug)}}" />
                        </a>
                         <h1 class="hometitle h4-style align-center">
                <a href="{{URL::to('/blog/'.$blog->category.'/'.$blog->slug)}}">{{$blog->title}}
                </a>
            </h1>
                    </div>
                    {!!html_entity_decode($blog->excerpt)!!}
                    <p class="viewarticleblog">  
                        <a class="button" href="{{URL::to('/blog/'.$blog->category.'/'.$blog->slug)}}">View article &raquo;
                        </a>
                    </p>
                </div>
               
           
            @endforeach
             </div>
        </div>
        <!-- Slot F -->
        <!-- Collection slider -->
        <div class="collection-slider-row collection-slider-2">
    <div class="collection-slider">
        <h1 class="hometitle h4-style align-center">
            <a href="{{URL::to('/collections/lionfresh')}}">
              <span>Lion Fresh
              </span>
            </a>
        </h1>
        <div class="view-all align-center">
            <a href="{{URL::to('/collections/lionfresh')}}">See All
            </a>
        </div>
        <div class="collection-listing paleftspace">
                    <div class="product-list">
                    @foreach($lionfresh_products as $lionfresh_product)
                   
                        <div data-product-id="5160918723" class="product-block" id="detailcont">
                            <div class="block-inner">
                            @if($lionfresh_product->totalquantity <= 0)
                                    <!-- <input type="submit" class="btn btn-primary out-of-stock" value="Out of stock" > -->
                                    <div class=" out-of-stock" ><span>SOLD OUT</span> </div>
                                    @endif
                                    @if($lionfresh_product->totalquantity <= 5 &&  $lionfresh_product->totalquantity > 0)
                                    <div class=" out-of-stock" style="background: #dcaa31 !important;"><span class="leftqnty">Only {{$lionfresh_product->totalquantity}} Left!</span> </div>
                                   <!--  <input type="submit" class="marginleftchangeindex paddingbot somemarginless btn btn-primary out-of-stock" style="background: #f08035 !important" value="Only {{$lionfresh_product->totalquantity}} Left!" > -->
                                    @endif
                            <div class="input-row cartbtn"> 
                             @if($lionfresh_product->totalquantity > 0)
                                  <form>
                                    <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$lionfresh_product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$lionfresh_product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$lionfresh_product->price}}" class="product_price">
                                    @if(isset($lionfresh_product->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$lionfresh_product->id.'/featured_images/'.$lionfresh_product->product_images)}}" alt="{{$lionfresh_product->product_title}}" class="image_url">
                                    @endif
                                     <input type="submit" class="cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                                  </form>
                                   @else
                                  <!--   <button class="btn btn-primary myn" data-id="{{$lionfresh_product->id}}" style="background: red !important;">Email When Available</button>  -->
                                     <button class="emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$lionfresh_product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Notify Me &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button> 
                                  @endif
                                </div>
                                <div class="image-cont">
                                @if(isset($lionfresh_product->product_images))
                                    <a class="image-link" href="{{URL::to('/products/'.$lionfresh_product->slug)}}">
                                        <img src="{{URL::to('/product_images/'.$lionfresh_product->id.'/featured_images/'.$lionfresh_product->product_images)}}" alt="{{$lionfresh_product->product_title}}" />
                                    </a>
                                @endif
                                    <a class="hover-info" href="{{URL::to('/products/'.$lionfresh_product->slug)}}">
                                        <div class="inner">
                                            <div class="innerer">
                                                <div class="title">{{$lionfresh_product->product_title}}
                                                </div>
                                            <span class="price">Rs.
                                            @if($howdyji_product->compare_price != 'NULL')
                                            <span class="price" style="color:black;text-decoration:line-through;padding-right: 2%;">
                                              {{$lionfresh_product->compare_price}}
                                              
                                            </span> 
                                            <span class="price">
                                               {{$lionfresh_product->price}}
                                            </span>
                                            @else
                                             <span class="price">
                                              {{$lionfresh_product->price}}
                                            </span>
                                            @endif
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                    @endforeach
                      </div>
                </div>
    </div>
    <!-- /.collection_slider -->
    <div class="collection-listing slider-collection-listing">
        <div class="product-detail">
        </div>
    </div>
</div>
 <script>
// Get the modal
// var modal = document.getElementById('myModal');

// // Get the button that opens the modal
// var btn = document.getElementsByClassName("myn");


// // Get the <span> element that closes the modal
 

// // When the user clicks the button, open the modal 
// btn.onclick = function() {
//     modal.style.display = "block";
// }
// 


$('.myn').click( function(){
   $('#myModal').show();
});
$('.close').click(function(){
    $('#myModal').hide();
});


</script>

<script></script>


@endsection

