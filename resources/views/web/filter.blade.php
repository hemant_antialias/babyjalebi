<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="https://resources/demos/style.css"> -->
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<?php
$allbrands = DB::table('product_category')->where('collection_type_id', '=', 3)->get();
$categories = DB::table('product_category')->where('collection_type_id', '=', 1)->get();
$check_brand = DB::table('product_category')->where('slug', $category)->first();
?>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script>
  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    }
                               );
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
                       " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  }
   );
</script>
<link rel="stylesheet" type="text/css" href="{{URL::to('css/filter.css')}} ">
</style>
<div id="myOverlay">
</div>
<form id="search-filter">
  <div class="filtermob col-md-3 col-xs-12 col-sm-12 filtercolumn" style="padding: 0;margin-top: 1%;">
    <div class="panel panel-default" id="panelhead">
      Filter<img src="{{URL::to('images/filter.png')}}" class="filterpanelheadimg" style="margin-left:2%;"></i>

    </div>
    
    <br>
    <div class="filtercontent" id="panelcont">
      <div class="filterbrand"> 
        <span>SORT BY
        </span><br>
        <select name="sort_filter" class="sort_filter">
          <option value="newest">
            Newest
          </option>
          <option value="ltoh">
            Price, Low to High
          </option>
          <option value="htol">
            Price, High to Low
          </option>
          <option value="popularity">
            Popularity
          </option>
        </select>
      </div>
      <!--filterbrand ends here -->
      <hr class="filtermarg">
      <div class="filterbrand">
        <p>
          <span class="filt colwhit" for="amount" style="color: #666 !important;">PRICE
          </span>
          <input type="text" id="amount" name="price_filter" readonly style="border:0; color:black; font-weight:400;">
        </p>
        <input type="hidden" id="min_price" name="min_price">
        <input type="hidden" id="max_price" name="max_price">
        <div id="slider-range">
        </div>
      </div>
      <hr class="filtermarg">
      <div class="filterbrand">
        <span class="filt colwit" for="weight_filter">WEIGHT
        </span>
        <input type="text" id="weight_filter" name="weight_filter" readonly style="border:0; color:black; font-weight:400;">
        <input type="hidden" id="min_weight" name="min_weight">
        <input type="hidden" id="max_weight" name="max_weight">
        <div id="slider-weight-range">
        </div>
      </div>
      <hr>
        <div class="filterbrand"> 
                <input id="cook_style" type="checkbox" >      
                <label class="filt filt_1" for="cook_style">COOK STYLE</label>
                <div class="expand">
                @foreach($categories as $category)
                  <div class="checkbox">
                    <label><input name="cook_style[]" type="checkbox" value="{{$category->id}}">{{$category->name}}</label>
                  </div>
                @endforeach
              </div>
           </div><!--filterbrand ends here -->
      @if(!(($check_brand->slug == 'lionfresh') || ($check_brand->slug == 'la-carne-cuts') || ($check_brand->slug == 'howdyji')))
      <hr>
      <div class="filterbrand"> 
        <input id="toggle" type="checkbox">
        <label class="filt filt_1" for="toggle">BRAND
        </label>
        <div class="expand">
          @foreach($allbrands as $brand)
          <div class="checkbox">
            <label>
              <input name="product_brand[]" class="product_brand" type="checkbox" value="{{$brand->id}}">{{$brand->name}}
            </label>
          </div>
          @endforeach
        </div>
        <hr> 
        <button class="btn btn-primary reset-filters" style="background: #ffffff !important;color: white !important;border-radius: 3px !important;/* margin-left: 0px; */margin-bottom: 10px;border: none;font-size: 14px !important;width: 100%;box-shadow: 0 0 0 0;border-radius: 0px !important;background: #d8930a !important;font-weight: bold;">Reset Filters
    </button>
   
      </div>
      <!--filterbrand ends here -->
      @endif
      
     
      <!--filterbrand ends here -->
    </div>
    <!--filtercontent ends here -->
  </div>
  <!--col-md-4 ends here -->
</form>
<!-- form ends here -->

<script type="text/javascript">
  $("#clientsOpen").click(function() {
    $('body').show();
  }
                         );
</script>
<script type="text/javascript">
  $('#clientsOpen').click(function () {
    $('#clientsDropDown #clientsDashboard').slideToggle({
      direction: "up"
    }
                                                        , 300);
    $(this).toggleClass('clientsClose');
  }
                         );
  // end click
  $('#x').click(function () {
    $('#clientsDropDown #clientsDashboard').slideToggle({
      direction: "Down"
    }
                                                        , 300);
    $(this).toggleClass('clientsClose');
  }
               );
  // end click
</script>
<script>
  $('#panelhead').on('click',function(){
    $('#panelcont').slideToggle();
});

</script>
<script>

// widget draggable//

  $('#widget').draggable();
</script>
<script>
  function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();
</script>
<script type="text/javascript">
    var wrap = $("#search-filter");

wrap.on("scroll", function(e) {
    
  if (this.scrollTop > 147) {
    wrap.addClass("fix-search");
  } else {
    wrap.removeClass("fix-search");
  }
  
});
</script>

