<?php
use App\ProductImages;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="ltr" class="no-js multi-step windows chrome desktop page--no-banner page--logo-main page--show  card-fields">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, height=device-height, minimum-scale=1.0, user-scalable=0">
    <title>Lion Fresh - Checkout
    </title>
    <!--[if lt IE 9]>
<link rel="stylesheet" media="all" href="//cdn.shopify.com/app/services/11271070/assets/85073731/checkout_stylesheet/v2-ltr-edge-83902ab997cc4efcbcba9496d7d037de-1825223211479188468/oldie" />
<![endif]-->
    <!--[if gte IE 9]><!-->
    <link href="{{URL::to('web-assets/css/chekout.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!--<![endif]-->
    <meta data-browser="chrome" data-browser-major="55">
    <meta data-body-font-family="Helvetica Neue" data-body-font-type="system">
    <script type="text/javascript">
      window.ShopifyExperiments = {
      }
    </script>
    <script src="{{URL::to('web-assets/css/chekout.css')}}">
    </script>
    <script src="{{URL::to('web-assets/js/checkout.js')}}"></script>
    <script src="{{URL::to('/js/jquery-2.1.4.min.js')}}"></script>

    <style>
    .displayhideweb{
      display: none;
    }
.apply {
    background: #2a9dcc;
    padding: 3.2%;
    padding-left: 6%;
    padding-right: 6%;
    color: #fff;
}
.applyfromphone {
    background: #2a9dcc;
    padding: 3.2%;
    padding-left: 6%;
    padding-right: 6%;
    color: #fff;
}
.banner{
  text-align: center;
}
.order-summary-toggle {
    background: #000000 !important;
    border-top: 1px solid #969191;
    border-bottom: 1px solid #969191;
    padding: 1.25em 0;
    -webkit-flex-shrink: 0;
    -ms-flex-negative: 0;
    flex-shrink: 0;
    text-align: left;
    width: 100%;
}
.order-summary-toggle__icon {
    fill: #ffffff!important;}
 .order-summary-toggle__text {
    color: #ffffff!important;
    vertical-align: middle;
    transition: color 0.2s ease-in-out;
    display: none;
}
.order-summary-toggle__dropdown {
    vertical-align: middle;
    transition: fill 0.2s ease-in-out;
    fill: #ffffff !important;
}
.order-summary-toggle:hover .order-summary-toggle__icon, .order-summary-toggle:focus .order-summary-toggle__icon {
    fill: #ffffff;
}
.order-summary-toggle:hover .order-summary-toggle__text, .order-summary-toggle:focus .order-summary-toggle__text {
    color: #ffffff;
}
.total-recap__final-price {
    font-size: 1.28571em;
    line-height: 1em;
    color: #ffffff;
}
.order-summary-toggle:hover .order-summary-toggle__dropdown, .order-summary-toggle:focus .order-summary-toggle__dropdown {
    fill: #ffffff;
}
.input-radio:checked {
    border: none;
    box-shadow: 0 0 0 10px #000000 inset;
}
.input-radio{
    border: none;
    box-shadow: 0 0 0 0 #7e8182 inset;
}
.btn{ background: #ef9f1d; }
.btn:hover {
    background: #202223;
    color: white;
}
.step__footer__previous-link{
      color: #ef9f1d;
}
.previous-link__icon {
    fill: #ef9f1d;
  }
  @media only screen and (max-width: 600px)
{
   .displayhideweb{
      display: block !important;
    }
    .mobhide{
      display: none;
    }
}


    </style>


  </head>
  <body>
    <div class="banner" data-header>
      <div class="wrap">
        <a href="https://www.lionfresh.com" class="logo logo--left">
           <img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="Lion Fresh" style="width: 85%;" />
        </a>
      </div>
    </div>
    <button class="order-summary-toggle order-summary-toggle--show" data-drawer-toggle="[data-order-summary]">
  <div class="wrap">
    <div class="order-summary-toggle__inner">
      <div class="order-summary-toggle__icon-wrapper">
        <svg width="20" height="19" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__icon">
          <path d="M17.178 13.088H5.453c-.454 0-.91-.364-.91-.818L3.727 1.818H0V0h4.544c.455 0 .91.364.91.818l.09 1.272h13.45c.274 0 .547.09.73.364.18.182.27.454.18.727l-1.817 9.18c-.09.455-.455.728-.91.728zM6.27 11.27h10.09l1.454-7.362H5.634l.637 7.362zm.092 7.715c1.004 0 1.818-.813 1.818-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817zm9.18 0c1.004 0 1.817-.813 1.817-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817z"/>
        </svg>
      </div>
      <div class="order-summary-toggle__text order-summary-toggle__text--show">
        <span>Show order summary</span>
        <svg width="11" height="6" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000"><path d="M.504 1.813l4.358 3.845.496.438.496-.438 4.642-4.096L9.504.438 4.862 4.534h.992L1.496.69.504 1.812z" /></svg>
      </div>
      <div class="order-summary-toggle__text order-summary-toggle__text--hide">
        <span>Hide order summary</span>
        <svg width="11" height="7" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000"><path d="M6.138.876L5.642.438l-.496.438L.504 4.972l.992 1.124L6.138 2l-.496.436 3.862 3.408.992-1.122L6.138.876z" /></svg>
      </div>
      <div class="order-summary-toggle__total-recap total-recap" data-order-summary-section="toggle-total-recap">
        <?php 
                        $total_tax = [];
                      ?>
                      @foreach($order_detail as $detail)
                        <?php 
                          $tax = $detail->tax/100*$detail->price;
                          array_push($total_tax, $tax);
                        ?> 
                      @endforeach
                      <?php
                      $final_tax = array_sum($total_tax);
                      ?>   
       <!--  <span class="amount_after_discount total-recap__final-price" data-checkout-payment-due-target="201625">Rs. {{round($order->payble_amount+$final_tax, 2)}}</span> -->
      </div>
    </div>
  </div>
</button>

    <div class="content" data-content>
      <div class="wrap">
        <div class="sidebar" role="complementary">
          <div class="sidebar__header">
            <a href="https://www.lionfresh.com" class="logo logo--left">
<img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="Lion Fresh" style="width: 85%;" />
            </a>
             <img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="Lion Fresh" style="width: 85%;" />
          </div>
          <div class="sidebar__content">
            <div class="order-summary order-summary--is-collapsed" data-order-summary>
              <h2 class="visually-hidden">Order summary
              </h2>
              <div class="order-summary__sections">
                <div class="order-summary__section order-summary__section--product-list">
                  <div class="order-summary__section__content">
                    <table class="product-table">
                      <caption class="visually-hidden">Shopping cart
                      </caption>
                      <thead>
                        <tr>
                          <th scope="col">
                            <span class="visually-hidden">Product image
                            </span>
                          </th>
                          <th scope="col">
                            <span class="visually-hidden">Description
                            </span>
                          </th>
                          <th scope="col">
                            <span class="visually-hidden">Quantity
                            </span>
                          </th>
                          <th scope="col">
                            <span class="visually-hidden">Price
                            </span>
                          </th>
                        </tr>
                      </thead>
                      <tbody data-order-summary-section="line-items">
                        @foreach($order_detail as $detail)
                        <?php
$product_image = ProductImages::where('product_id', '=', $detail->product_id)->select('images')->first();
?>
                        <tr class="product">
                          <td class="product__image">
                            <div class="product-thumbnail">
                              <div class="product-thumbnail__wrapper">
                                @if(isset($detail->product_images))
                                <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" /> @else
                                <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" /> @endif
                              </div>
                              <span class="product-thumbnail__quantity" aria-hidden="true">{{$detail->quantity}}
                              </span>
                            </div>
                          </td>
                          <td class="product__description">
                            <span class="product__description__name order-summary__emphasis">{{$detail->product_title}}
                            </span>
                            <span class="product__description__variant order-summary__small-text">
                            </span>
                          </td>
                          <td class="product__quantity visually-hidden">
                            {{$detail->quantity}}
                          </td>
                          <td class="product__price">
                            <?php $qty =  $detail->quantity;?>
                            <span class="order-summary__emphasis">Rs. {{$detail->price*$qty}}
                            </span>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div class="order-summary__scroll-indicator">
                      Scroll for more items
                      <svg xmlns="http://www.w3.org/2000/svg" width="10" height="12" viewBox="0 0 10 12">
                        <path d="M9.817 7.624l-4.375 4.2c-.245.235-.64.235-.884 0l-4.375-4.2c-.244-.234-.244-.614 0-.848.245-.235.64-.235.884 0L4.375 9.95V.6c0-.332.28-.6.625-.6s.625.268.625.6v9.35l3.308-3.174c.122-.117.282-.176.442-.176.16 0 .32.06.442.176.244.234.244.614 0 .848" />
                      </svg>
                    </div>
                  </div>
                </div>
                <div class="order-summary__section order-summary__section--discount" data-reduction-form="update">
                 
                  <form class="edit_checkout" action="/11271070/checkouts/16d807336e0758feda31e0bd838b93c8" accept-charset="UTF-8" method="post">
                    <input name="utf8" type="hidden" value="&#x2713;" />
                    <input type="hidden" name="_method" value="patch" />
                    <input type="hidden" name="authenticity_token" value="Lv50gClK70F4dINrPgpEvD0f0JvHcm4EDrRxinpKrEc4VECm/w8xUymhv3jM6SiPVCE2Imx8mBRZwi1jOg+Z8w==" />
                    <input type="hidden" name="step" value="shipping_method" />
                  </form>
                </div>
                <div class="mobhide order-summary__section order-summary__section--total-lines" data-order-summary-section="payment-lines">
                  <table class="total-line-table">
                    <caption class="visually-hidden">Cost summary
                    </caption>
                    <thead>
                      <tr>
                        <th scope="col">
                          <span class="visually-hidden">Description
                          </span>
                        </th>
                        <th scope="col">
                          <span class="visually-hidden">Price
                          </span>
                        </th>
                      </tr>
                    </thead>
                    <tbody class="total-line-table__tbody">
                      <tr class="total-line total-line--subtotal">
                        <td class="total-line__name">Subtotal
                        </td>
                        <td class="total-line__price">
                          <span class="order-summary__emphasis" data-checkout-subtotal-price-target="189500">
                            Rs. {{$order->amount}}
                          </span>
                        </td>
                      </tr>
                      <?php 
                        $total_tax = [];
                        $cart_content = Cart::content();
                      ?>
                      @foreach($cart_content as $detail)
                        <?php 
                          $tax = $detail->options->tax/100*$detail->price*$detail->qty;
                          array_push($total_tax, $tax);
                        ?> 
                      @endforeach
                      <?php
                      $final_tax = array_sum($total_tax);
                      ?>
                      <tr class="total-line total-line--subtotal">
                        <td class="total-line__name">Tax</td>
                        <td class="total-line__price">
                          <span class="order-summary__emphasis" id="tax_value" data-checkout-subtotal-price-target="189500">
                            Rs. {{round($final_tax, 2)}}
                          </span>
                        </td>
                      </tr>

                @if(isset($order->discount) && $order->discount != 0)
                <tr class="total-line total-line--shipping">
                  <td class="total-line__name">Discount</td>
                  <td class="total-line__price">
                    <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                      Rs.{{$order->discount}}
                    </span>
                  </td>
                </tr>
                @endif

                @if(isset($order->coupon_code) && $order->coupon_code != "")
                <tr class="total-line total-line--shipping">
                  <td class="total-line__name">Coupon Code</td>
                  <td class="total-line__price">
                    <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                    {{$order->coupon_code}}
                    </span>
                  </td>
                </tr>
                @endif

                      <tr class="total-line total-line--shipping discount_price_div">
                        <td class="total-line__name">Discount (<span class="order-summary__emphasis discount_code" data-checkout-total-shipping-target="0">
                          
                          </span>)</td>
                        <td class="total-line__price">
                          (-) <span class="order-summary__emphasis discount_price" data-checkout-total-shipping-target="0">
                            
                          </span>
                        </td>
                        <td class="delete_discount" align="center" style="cursor: pointer;">X</td>
                      </tr>
                      @if($order->coupon_code == '')
                      <div id="discount_div"><input type="text" name="discout" id="discout" placeholder="Enter Discount Code" style="padding: 3%;border: 1px solid #ccc ; width: 69%; background: white; " /> <button type="submit" name="apply" class="apply">Apply</button></div><br />
                      @endif
                      <div class="discount-message" style="color: green;"></div>

                    <!--   <tr class="total-line total-line--shipping discount_code_div">
                        <td class="total-line__name">Coupon Code</td>
                        <td class="total-line__price">
                          
                        </td>
                      </tr> -->
                     
                    </tbody>
                    <tfoot class="total-line-table__footer">
                      <tr class="total-line">
                        <td class="total-line__name payment-due-label">
                          <span class="payment-due-label__total">Total
                          </span>
                        </td>
                        <td class="total-line__price payment-due">
                          <!-- <span class="payment-due__currency">INR
                          </span> -->
                          <span class="payment-due__price amount_after_discount" data-checkout-payment-due-target="201625">
                            Rs. {{round($order->payble_amount+$final_tax, 2)}}
                          </span>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="main" role="main">
          <div class="main__header">
            <a href="https://www.lionfresh.com" class="logo logo--left">
              <h1 class="logo__text">Lion Fresh
              </h1>
            </a>
          </div>
           <div class="displayhideweb order-summary__section order-summary__section--total-lines " data-order-summary-section="payment-lines">
                  <div id="discount_div"><input type="text" name="discout" id="discout1" placeholder="Enter Discount Code" style="padding: 3%;border: 1px #ccc solid;width: 69%; background: white; "> <button type="submit" name="apply" class="applyfromphone">Apply</button></div><br><div class="discount-message" style="color: green;"></div><table class="total-line-table">
                    <caption class="visually-hidden">Cost summary
                    </caption>
                    <thead>
                      <tr>
                        <th scope="col">
                          <span class="visually-hidden">Description
                          </span>
                        </th>
                        <th scope="col">
                          <span class="visually-hidden">Price
                          </span>
                        </th>
                      </tr>
                    </thead>
                    <tbody class="total-line-table__tbody">
                      <tr class="total-line total-line--subtotal">
                        <td class="total-line__name">Subtotal
                        </td>
                        <td class="total-line__price">
                          <span class="order-summary__emphasis" data-checkout-subtotal-price-target="189500">
                            Rs. {{$order->amount}}
                          </span>
                        </td>
                      </tr>
                                                                 
                                               
                                                                  <tr class="total-line total-line--subtotal">
                        <td class="total-line__name">Tax</td>
                        <td class="total-line__price">
                          <span class="order-summary__emphasis" data-checkout-subtotal-price-target="189500">
                            Rs. {{round($final_tax, 2)}}
                          </span>
                        </td>
                      </tr>

                
                @if(isset($order->discount) && $order->discount != 0)
                <tr class="total-line total-line--shipping">
                  <td class="total-line__name">Discount</td>
                  <td class="total-line__price">
                    <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                      Rs.{{$order->discount}}
                    </span>
                  </td>
                </tr>
                @endif

                @if(isset($order->coupon_code) && $order->coupon_code != "")
                <tr class="total-line total-line--shipping">
                  <td class="total-line__name">Coupon Code</td>
                  <td class="total-line__price">
                    <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                    {{$order->coupon_code}}
                    </span>
                  </td>
                </tr>
                @endif

                      <tr class="total-line total-line--shipping discount_price_div">
                        <td class="total-line__name">Discount (<span class="order-summary__emphasis discount_code" data-checkout-total-shipping-target="0">
                          
                          </span>)</td>
                        <td class="total-line__price">
                          (-) <span class="order-summary__emphasis discount_price" data-checkout-total-shipping-target="0">
                            
                          </span>
                        </td>
                        <td class="delete_discount" align="center" style="cursor: pointer;">X</td>
                      </tr>
                     
                                            
                                            

                    <!--   <tr class="total-line total-line--shipping discount_code_div">
                        <td class="total-line__name">Coupon Code</td>
                        <td class="total-line__price">
                          
                        </td>
                      </tr> -->
                     
                    </tbody>
                    <tfoot class="total-line-table__footer">
                      <tr class="total-line">
                        <td class="total-line__name payment-due-label">
                          <span class="payment-due-label__total">Total
                          </span>
                        </td>
                        <td class="total-line__price payment-due">
                          <!-- <span class="payment-due__currency">INR
                          </span> -->
                          <span class="payment-due__price amount_after_discount" data-checkout-payment-due-target="201625">
                            Rs. {{round($order->payble_amount+$final_tax, 2)}}
                          </span>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
          <div class="main__content">
            <div class="step" data-step="shipping_method">
              <input type="hidden" name="previous_step" id="previous_step" value="shipping_method" />
              <input type="hidden" name="step" value="payment_method" />
              <div class="step__sections">
                <div class="content-box">
                  <div class="content-box__row content-box__row--secondary">
                    <div class="content-box__header">
                      <div class="content-box__header__title">
                        <h3>Shipping address
                        </h3>
                      </div>
                      <div class="content-box__header__action">
                        <a href="{{URL::to('/checkout/customer-info/'.$order->order_code)}}">
                          @if(isset($address))
                          <span aria-hidden>Edit
                          </span>
                          <span class="visually-hidden">Edit shipping address
                          </span>
                          @else
                          <span aria-hidden>Add Shipping Address
                          </span>
                          <span class="visually-hidden">Edit shipping address
                          </span>
                          @endif
                        </a> 
                      </div>
                    </div>
                    <p>
                      @if(isset($address)) {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                      <br>{{$address->address}}
                      <br>{{$address->location}}
                      <br>{{$address->zip}} {{$address->city}} {{$address->state}}
                      <br>{{$address->country}}
                      <br>+91 {{Auth::user()->contact}} @else
                    <h4>Kindly Add Your Shipping Address
                    </h4> @endif
                    </p>
                </div>
              </div>

              <div class="section section--shipping-method">
                <div class="section__header">
                  <h2 class="section__title">Customer Notes
                  </h2><br>
                  <form method="post" action="{{URL::to('/checkout/add-shipping')}}">
                  
                  <textarea name="notes" rows="8" cols="50" placeholder="Notes" style="padding: 3%;border: 1px #ccc solid;width: 94%;"></textarea>
                </div>
                </div>

              <div class="section section--shipping-method">
                <div class="section__header">
                  <h2 class="section__title">Shipping method
                  </h2>
                </div>

                
                <input type="hidden" name="ordercode" value="{{$order->order_code}}">
                <input type="hidden" name="shipping_address" value="{{$address->id}}">
                <input type="hidden" name="coupon_code" class="coupon_code" value="">
                <input type="hidden" name="discount_price" class="discounted_price" value="">
                <input type="hidden" name="payble_amount" class="payble_amount" value="">
                <input type="hidden" value="{{$final_tax}}" name="total_tax" id="final_tax">




                  <div class="section__content">
                    <div class="content-box" data-shipping-methods>
                    @if($order->payble_amount >= 700)
                      <div class="content-box__row">
                        <div class="radio-wrapper" data-shipping-method="shopify-Free%20Shipping-0.00">
                          <div class="radio__input">
                            <input class="input-radio afterdiscount" type="radio" value="free" checked="checked" name="shipping"  />
                          </div>
                          <label class="radio__label" for="checkout_shipping_rate_id_shopify-free20shipping-000">
                            <span class="radio__label__primary shippingmeth" data-shipping-method-label-title="Free Shipping">
                              Free Shipping
                            </span>
                            <span class="radio__label__accessory">
                              <span class="content-box__emphasis shippingprice">
                                Free
                              </span>
                            </span>
                          </label>
                        </div>
                        <!-- /radio-wrapper-->
                      </div>
                      @else
                      <div class="content-box__row">
                        <div class="radio-wrapper" data-shipping-method="shopify-Free%20Shipping-0.00">
                          <div class="radio__input">
                            <input class="input-radio" type="radio" value="custom" checked="checked" name="shipping"  />
                          </div>
                          <label class="radio__label" for="checkout_shipping_rate_id_shopify-free20shipping-000">
                            <span class="radio__label__primary" data-shipping-method-label-title="Free Shipping">
                              Shipping charges
                            </span>
                            <span class="radio__label__accessory">
                              <span class="content-box__emphasis" style="float:right">
                                Rs.  50 
                              </span>
                            </span>
                          </label>
                        </div>
                        <!-- /radio-wrapper-->
                      </div>
                      @endif
                    </div>
                  </div>
                  </div>
            </div>
            <div class="step__footer">
              <a href="{{URL::to('/checkout/payment-method')}}">
                <input type="submit" class="btn__content step__footer__continue-btn btn" value="Continue to payment method">
                
                <i class="btn__spinner icon icon--button-spinner">
                </i>
              </a>
                </form>
              <a class="step__footer__previous-link" href="{{URL::to('/checkout/customer-info/'.$order->order_code)}}">
                <svg class="previous-link__icon icon--chevron icon" xmlns="http://www.w3.org/2000/svg" width="6.7" height="11.3" viewBox="0 0 6.7 11.3">
                  <path d="M6.7 1.1l-1-1.1-4.6 4.6-1.1 1.1 1.1 1 4.6 4.6 1-1-4.6-4.6z" />
                </svg>
                Return to customer information
              </a>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    </div>

    <script>
      $(document).ready(function(){
        $('.discount_price_div').show();
        $('.discount_code_div').show();

      });

      $('.delete_discount').click(function(){
        var base_url = "{{URL::to('/')}}";

        $.ajax({
          url: base_url+'/remove-discount',
          type: "post",
          data: 
          {
            "ordercode": "{{$order->order_code}}",
          },
          success:function(data)
          {
            if (data.status==1) {
              $('#discount_div').show();
              $('.discount_price_div').hide();
              $('.discount_code_div').hide();
              $('.discounted_price').val(" ");
              $('.coupon_code').val(" ");
              $('.discount-message').hide();
              $('#final_tax').val(data.final_tax);
              $('#tax_value').html('Rs. '+ data.final_tax);
              $('.amount_after_discount').html("Rs."+"{{$order->payble_amount+$final_tax}}");
              if({{$order->payble_amount+$final_tax}} > 700)
              {
                 $('.shippingprice').html('Free');
                 $('.afterdiscount').val('Free');
                 $('.shippingmeth').html('Free Shipping');

              }
              $('.payble_amount').val(" ");
            };
          }
        });
      });

      $('.apply').click(function(){
        var base_url = "{{URL::to('/')}}";
        var discout = $("#discout").val();
        var taxvalue = $("#tax_value").val();
        // alert(taxvalue)

        $.ajax({
          url: base_url+'/check-discount',
          type: "post",
          data: 
          {
            "ordercode": "{{$order->order_code}}",
            "discout": discout
          },
          success:function(data)
          {
            if(data.status==0){
              $('.discount-message').html(data.message);
            }
            if(data.status==1){
              $('.discount-message').html(data.message);
              $('.discount_price_div').show();
              $('.discount_code_div').show();
              $('.discount_price').html("Rs."+data.discount_value);
              $('.discount_code').html(data.coupon_code);
              $('.amount_after_discount').html("Rs."+(data.after_discount_price + taxvalue));
              if(data.after_discount_price + taxvalue <= 700)
              {
                 $('.shippingprice').html('Rs.  50 ');
                 $('.afterdiscount').val('custom');
                 $('.shippingmeth').html('Shipping charges');

              }
              $('#discount_div').hide();
              $('.discounted_price').val(data.discount_value);
              $('.coupon_code').val(data.coupon_code);
              $('#final_tax').val(data.final_tax);
              $('#tax_value').html('Rs. '+ data.final_tax);
              $('.payble_amount').val(data.after_discount_price);
            }
          }
        });
        
      });

      $('.applyfromphone').click(function(){
        var base_url = "{{URL::to('/')}}";
        var discout = $("#discout1").val();
        var taxvalue = $("#tax_value").val();
        // alert(taxvalue)

        $.ajax({
          url: base_url+'/check-discount',
          type: "post",
          data: 
          {
            "ordercode": "{{$order->order_code}}",
            "discout": discout
          },
          success:function(data)
          {
            if(data.status==0){
              $('.discount-message').html(data.message);
            }
            if(data.status==1){
              $('.discount-message').html(data.message);
              $('.discount_price_div').show();
              $('.discount_code_div').show();
              $('.discount_price').html("Rs."+data.discount_value);
              $('.discount_code').html(data.coupon_code);
              $('.amount_after_discount').html("Rs."+(data.after_discount_price + taxvalue));
              if(data.after_discount_price + taxvalue <= 700)
              {
                 $('.shippingprice').html('Rs.  50 ');
                 $('.afterdiscount').val('custom');
                 $('.shippingmeth').html('Shipping charges');

              }
              $('#discount_div').hide();
              $('.discounted_price').val(data.discount_value);
              $('.coupon_code').val(data.coupon_code);
              $('#final_tax').val(data.final_tax);
              $('#tax_value').html('Rs. '+ data.final_tax);
              $('.payble_amount').val(data.after_discount_price);
            }
          }
        });
        
      });
    </script>
<script>
(function (global) {

  if(typeof (global) === "undefined")
  {
    throw new Error("window is undefined");
  }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

    // making sure we have the fruit available for juice....
    // 50 milliseconds for just once do not cost much (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };
  
  // Earlier we had setInerval here....
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        
    noBackPlease();

    // disables backspace on page except on input fields and textarea..
    document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };
    
    };

})(window);
</script>
  
  </body>
</html>
