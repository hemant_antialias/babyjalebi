<?php
  use App\ProductImages;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="ltr" class="no-js multi-step windows chrome desktop page--no-banner page--logo-main page--show  card-fields">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, height=device-height, minimum-scale=1.0, user-scalable=0">

    <title>BabyJalebi - Checkout</title>

    

    <style type="text/css">
      #section--billing-address__different{
        display: none;
      }
      .banner{ text-align: center; }
      .order-summary-toggle {
    background: #272424;}
    .order-summary-toggle {
    background: #000000 !important;
    border-top: 1px solid #969191;
    border-bottom: 1px solid #969191;
    padding: 1.25em 0;
    -webkit-flex-shrink: 0;
    -ms-flex-negative: 0;
    flex-shrink: 0;
    text-align: left;
    width: 100%;
}
.order-summary-toggle__icon {
    fill: #ffffff!important;}
    .order-summary-toggle__text {
    color: #ffffff!important;
    vertical-align: middle;
    transition: color 0.2s ease-in-out;
    display: none;
}
.order-summary-toggle__dropdown {
    vertical-align: middle;
    transition: fill 0.2s ease-in-out;
    fill: #ffffff !important;
}
.total-recap__final-price {
    font-size: 1.28571em;
    line-height: 1em;
    color: #ffffff !important;
}
.input-radio:checked {
    border: none;
    box-shadow: 0 0 0 10px #000000 inset !important;
}
.input-radio{
    border: none;
    box-shadow: 0 0 0 0 #7e8182 inset !important;
}
.btn{ background: #ef9f1d !important; }
.btn:hover {
    background: #202223 !important;
    color: white !important;
}
.step__footer__previous-link{
      color: #ef9f1d !important;
}
.previous-link__icon {
    fill: #ef9f1d !important;}

.apply {
    background: #2a9dcc;
    padding: 3.2%;
    padding-left: 6%;
    padding-right: 6%;
    color: #fff;
}
    </style>
    

<!--[if lt IE 9]>
<link rel="stylesheet" media="all" href="//cdn.shopify.com/app/services/11271070/assets/85073731/checkout_stylesheet/v2-ltr-edge-83902ab997cc4efcbcba9496d7d037de-1825223211479188468/oldie" />
<![endif]-->

<!--[if gte IE 9]><!-->
        <link href="{{URL::to('web-assets/css/chekout.css')}}" rel="stylesheet" type="text/css" media="all" />
       

<!--<![endif]-->






    <meta data-browser="chrome" data-browser-major="55">
<meta data-body-font-family="Helvetica Neue" data-body-font-type="system">

  <script type="text/javascript">
    window.shopifyExperiments = {}
  </script>


  <!-- <script src="{{URL::to('web-assets/js/checkout.js')}}"></script> -->

    <script src="{{URL::to('/js/jquery-2.1.4.min.js')}}"></script>

    

  
  




  <script id="__st">
//<![CDATA[
var __st={"a":11271070,"offset":19800,"reqid":"0efeafe6-b72c-40c7-a29f-5cafff48cd44","pageurl":"checkout.shopify.com\/11271070\/checkouts\/16d807336e0758feda31e0bd838b93c8?step=contact_information","u":"598448ed1239","t":"checkout","offsite":1,"cid":5179483139};
//]]>
</script>
<script>
//<![CDATA[
      (function() {
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
        s.src = '//cdn.shopify.com/s/javascripts/shopify_stats.js?v=6';
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
      })();

//]]>
</script>


<style>
  .field--half {
    width: 50%;
}
.help-block
{
  color: red;
}
a .link {
    text-decoration: none;
    color: #ef9f1d !important;}
    a:hover, .link:hover {
    color: #ef9f1d !important;
}
</style>


  </head>
  <body>
    <div class="banner" data-header>
      <div class="wrap">
        
<a href="https://www.lionfresh.com" class="logo logo--left">
    <img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="Lion Fresh" style="width: 85%;" />
</a>

      </div>
    </div>

    <button class="order-summary-toggle order-summary-toggle--show" data-drawer-toggle="[data-order-summary]">
  <div class="wrap">
    <div class="order-summary-toggle__inner">
      <div class="order-summary-toggle__icon-wrapper">
        <svg width="20" height="19" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__icon">
          <path d="M17.178 13.088H5.453c-.454 0-.91-.364-.91-.818L3.727 1.818H0V0h4.544c.455 0 .91.364.91.818l.09 1.272h13.45c.274 0 .547.09.73.364.18.182.27.454.18.727l-1.817 9.18c-.09.455-.455.728-.91.728zM6.27 11.27h10.09l1.454-7.362H5.634l.637 7.362zm.092 7.715c1.004 0 1.818-.813 1.818-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817zm9.18 0c1.004 0 1.817-.813 1.817-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817z"/>
        </svg>
      </div>
      <div class="order-summary-toggle__text order-summary-toggle__text--show">
        <span>Show order summary</span>
        <svg width="11" height="6" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000"><path d="M.504 1.813l4.358 3.845.496.438.496-.438 4.642-4.096L9.504.438 4.862 4.534h.992L1.496.69.504 1.812z" /></svg>
      </div>
      <div class="order-summary-toggle__text order-summary-toggle__text--hide">
        <span>Hide order summary</span>
        <svg width="11" height="7" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000"><path d="M6.138.876L5.642.438l-.496.438L.504 4.972l.992 1.124L6.138 2l-.496.436 3.862 3.408.992-1.122L6.138.876z" /></svg>
      </div>
      <div class="order-summary-toggle__total-recap total-recap" data-order-summary-section="toggle-total-recap">
        <span class="total-recap__final-price" data-checkout-payment-due-target="201625">Rs. {{round($order->amount+$order->tax_rate_value, 2)}}</span>
      </div>
    </div>
  </div>
</button>


    <div class="content" data-content>
      <div class="wrap">
        <div class="sidebar" role="complementary">
          <div class="sidebar__header">
            
<a href="https://www.lionfresh.com" class="logo logo--left">
    <img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="Lion Fresh" style="width: 85%;" />
</a>

          </div>
          <div class="sidebar__content">
            <div class="order-summary order-summary--is-collapsed" data-order-summary>
  <h2 class="visually-hidden">Order summary</h2>

  <div class="order-summary__sections">
    <div class="order-summary__section order-summary__section--product-list">
  <div class="order-summary__section__content">
    <table class="product-table">
      <caption class="visually-hidden">Shopping cart</caption>
      <thead>
        <tr>
          <th scope="col"><span class="visually-hidden">Product image</span></th>
          <th scope="col"><span class="visually-hidden">Description</span></th>
          <th scope="col"><span class="visually-hidden">Quantity</span></th>
          <th scope="col"><span class="visually-hidden">Price</span></th>
        </tr>
      </thead>
      <tbody data-order-summary-section="line-items">
      @foreach($order_detail as $detail)
      <?php
        $product_image = ProductImages::where('product_id', '=', $detail->product_id)->select('images')->first();
      ?>
        <tr class="product">
          <td class="product__image">
            <div class="product-thumbnail">
              <div class="product-thumbnail__wrapper">
              @if(isset($detail->product_images))
                <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" />
              @else
                <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" />
              @endif
              </div>
              <span class="product-thumbnail__quantity" aria-hidden="true">{{$detail->quantity}}</span>
            </div>
          </td>
          <td class="product__description">
            <span class="product__description__name order-summary__emphasis">{{$detail->product_title}}</span>
            <span class="product__description__variant order-summary__small-text"></span>
          </td>
          <td class="product__quantity visually-hidden">
            1
          </td>
          <td class="product__price">
            <?php $qty =  $detail->quantity;?>
            <span class="order-summary__emphasis">Rs. {{$detail->price*$qty}}</span>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>

    <div class="order-summary__scroll-indicator">
      Scroll for more items
      <svg xmlns="http://www.w3.org/2000/svg" width="10" height="12" viewBox="0 0 10 12"><path d="M9.817 7.624l-4.375 4.2c-.245.235-.64.235-.884 0l-4.375-4.2c-.244-.234-.244-.614 0-.848.245-.235.64-.235.884 0L4.375 9.95V.6c0-.332.28-.6.625-.6s.625.268.625.6v9.35l3.308-3.174c.122-.117.282-.176.442-.176.16 0 .32.06.442.176.244.234.244.614 0 .848"/></svg>
    </div>
  </div>
</div>


      <div class="order-summary__section order-summary__section--discount" data-reduction-form="update">
        <!-- <form class="edit_checkout" action="/11271070/checkouts/16d807336e0758feda31e0bd838b93c8" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="7oQjjt5TJLJd9u2xqvxeu9YLSGPOO32jsy0JipPRDQP4LheoCBb6oAwj0aJYHzKIvzWu2mU1i7PkW1Vj05Q4tw==" />
  <input type="hidden" name="step" value="contact_information" />

  <div class="fieldset">
    <div class="field ">
      <label class="field__label" for="checkout_reduction_code">Discount</label>
      <div class="field__input-btn-wrapper">
        <div class="field__input-wrapper">
          <input placeholder="Discount" class="field__input" data-discount-field="true" autocomplete="off" size="30" type="text" name="checkout[reduction_code]" id="checkout_reduction_code" />
        </div>

        <button type="submit" class="field__input-btn btn btn--default btn--disabled">
          <span class="btn__content visually-hidden-on-mobile">Apply</span>
          <i class="btn__content shown-on-mobile icon icon--arrow"></i>
          <i class="btn__spinner icon icon--button-spinner"></i>
        </button>
      </div>

    </div>
  </div>
</form> -->
<form class="edit_checkout" action="/11271070/checkouts/16d807336e0758feda31e0bd838b93c8" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="+XFViKAW7Ciwq7xxoHht7zkIyqutYZnAGJPpSIT0emLv22GudlMyOuF+gGJSmwHcUDYsEgZvb9BP5bWhxLFP1g==" />
  <input type="hidden" name="step" value="contact_information" />

</form>
      </div>

    <div class="order-summary__section order-summary__section--total-lines" data-order-summary-section="payment-lines">
  
  <div id="discount_div"><input type="text" name="discout" id="discout" placeholder="Enter Discount Code" style="padding: 3%;border: 1px #ccc solid;width: 69%; background: white; "> <button type="submit" name="apply" class="apply">apply</button></div><br><div class="discount-message" style="color: green;"></div>


  
  <table class="total-line-table">
    <caption class="visually-hidden">Cost summary</caption>
    <thead>
      <tr>
        <th scope="col"><span class="visually-hidden">Description</span></th>
        <th scope="col"><span class="visually-hidden">Price</span></th>
      </tr>
    </thead>
      <tbody class="total-line-table__tbody">
        <tr class="total-line total-line--subtotal">
          <td class="total-line__name">Subtotal</td>
          <td class="total-line__price">
            <span class="order-summary__emphasis" data-checkout-subtotal-price-target="189500">
              Rs.{{round($order->amount, 2)}}
            </span>
          </td>
        </tr>

      @if($order->discount != '')
        <tr class="total-line total-line--shipping discount_price_div">
          <td class="total-line__name">Discount (<span class="order-summary__emphasis discount_code" data-checkout-total-shipping-target="0">
            {{$order->coupon_code}}
            </span>)</td>
          <td class="total-line__price">
            (-) <span class="order-summary__emphasis discount_price" data-checkout-total-shipping-target="0">
              {{$order->discount}}
            </span>
          </td>
          <td class="delete_discount" align="center" style="cursor: pointer;">X</td>
        </tr>
      @else
        <tr class="total-line total-line--shipping discount_price_div" style="display: none;">
          <td class="total-line__name">Discount (<span class="order-summary__emphasis discount_code" data-checkout-total-shipping-target="0">
            
            </span>)</td>
          <td class="total-line__price">
            (-) <span class="order-summary__emphasis discount_price" data-checkout-total-shipping-target="0">
              
            </span>
          </td>
          <td class="delete_discount" align="center" style="cursor: pointer;">X</td>
        </tr>
      @endif
        <tr class="total-line total-line--subtotal">
          <td class="total-line__name">Tax</td>
          <td class="total-line__price">
            <span class="order-summary__emphasis" id="tax_value" data-checkout-subtotal-price-target="189500">
              Rs. {{round($order->tax_rate, 2)}}
            </span>
          </td>
        </tr>

        <tr class="total-line total-line--subtotal">
          <td class="total-line__name">Shipping</td>
          <td class="total-line__price">
            <span class="order-summary__emphasis" id="shipping_value" data-checkout-subtotal-price-target="189500">
              Rs. <span class="shipping_rate_value">{{round($order->custom_rate_value, 2)}}</span>
            </span>
          </td>
        </tr>

<!--         <tr class="total-line total-line--shipping">
            <td class="total-line__name">Discount code can be applied on the next page</td>
            <td class="total-line__price">
              <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                
              </span>
            </td>
          </tr> -->

          <!-- @if(isset($order->discount))
          <tr class="total-line total-line--shipping">
            <td class="total-line__name">Discount</td>
            <td class="total-line__price">
              <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                Rs.{{$order->discount}}
              </span>
            </td>
          </tr>
          @endif

           @if(isset($order->coupon_code))
          <tr class="total-line total-line--shipping">
            <td class="total-line__name">Coupon Code</td>
            <td class="total-line__price">
              <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
              {{$order->coupon_code}}
              </span>
            </td>
          </tr>
          @endif -->

      </tbody>

    <tfoot class="total-line-table__footer">
      <tr class="total-line">
        <td class="total-line__name payment-due-label">
          <span class="payment-due-label__total">Total</span>
        </td>
        <td class="total-line__price payment-due">
          <!-- <span class="payment-due__currency">INR</span> -->
          <span class="payble_amount" data-checkout-payment-due-target="201625">
            Rs. {{$order->payble_amount}}
          </span>
        </td>
      </tr>
    </tfoot>
  </table>
</div>

  </div>
</div>

          </div>
        </div>
        <div class="main" role="main">
          <div class="main__header">
            
<a href="https://www.lionfresh.com" class="logo logo--left">
    <h1 class="logo__text">Lion Fresh</h1>
</a>


            
          </div>
          <div class="main__content">
            <div class="step" data-step="contact_information">
  <!-- <form novalidate="novalidate" class="edit_checkout" action="{{URL::to('/checkout/add-address')}}" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="RapPETp0dppPEXW+5Ty8RVCVoaGPAQS6+HQl952egzdTAHs37DGoiB7ESa0X39B2OatHGCQP8qqvAnke3du2gw==" /> -->

  <input type="hidden" name="previous_step" id="previous_step" value="contact_information" />
  <input type="hidden" name="step" value="shipping_method" />

  <div class="step__sections">
      
<div class="section section--contact-information">
  <div class="section__header">
    <h2 class="section__title">Customer information</h2>
  </div>
  <div class="section__content">
      <input value="arya.hemant@rocketmail.com" size="30" type="hidden" name="checkout[email]" id="checkout_email" />
      <div class="logged-in-customer-information">
        <div class="logged-in-customer-information__avatar-wrapper">
          <div class="logged-in-customer-information__avatar gravatar" style="background-image: url(https://www.gravatar.com/avatar/d02df312ce8a93204c7550ae084bbc68.jpg?s=100&amp;d=blank);filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=&#39;https://www.gravatar.com/avatar/d02df312ce8a93204c7550ae084bbc68.jpg?s=100&amp;d=blank&#39;, sizingMethod=&#39;scale&#39;)" role="img" aria-label="Avatar"></div>
        </div>

        <p class="logged-in-customer-information__paragraph">
          <span class="page-main__emphasis">{{$name}}</span> ({{$email}})<br>
          <a href="{{url('/logout')}}">Log out</a>
        </p>
      </div>
  </div> 
</div> 


  <div class="section section--shipping-address" data-shipping-address
                                                    data-update-order-summary>

    <div class="section__header">
      <h2 class="section__title">
          Shipping address
      </h2>
    </div>

<form method="post" action="{{URL::to('/checkout/add-address')}}" class="animate-floating-labels">
<input type="hidden" name="ordercode" value="{{$order->order_code}}">

<input type="hidden" name="ordercode" value="{{$order->order_code}}">
    <div class="section__content">
      <div class="fieldset" data-address-fields>

<div class="field--half field field--required field--show-floating-label" data-address-field="first_name">
  <label class="field__label" for="checkout_shipping_address_first_name">First name</label>
  <div class="field__input-wrapper">
    <input placeholder="First name" autocomplete="shipping given-name" data-backup="first_name" class="field__input" size="30" type="text" value="{{Auth::user()->first_name}}" name="first_name" id="checkout_shipping_address_first_name" />
  </div>
  @if ($errors->has('first_name'))
    <span class="help-block">
        <strong>{{ $errors->first('first_name') }}</strong>
    </span>
  @endif
</div>
<div class="field--half field field--required field--show-floating-label" data-address-field="last_name">
  <label class="field__label" for="checkout_shipping_address_last_name">Last name</label>
  <div class="field__input-wrapper">
    <input placeholder="Last name" autocomplete="shipping family-name" data-backup="last_name" class="field__input" size="30" type="text" value="{{Auth::user()->last_name}}" name="last_name" id="checkout_shipping_address_last_name" />
  </div>
</div>

<div class="field--two-thirds field field--required" data-address-field="address1">
  <label class="field__label" for="checkout_shipping_address_address1">Address</label>
  <div class="field__input-wrapper">

    @if(isset($address->address))
    <input placeholder="Address" autocomplete="shipping address-line1" data-backup="address1" class="field__input" size="30" type="text" value="{{$address->address}}" name="shipping_address" id="checkout_shipping_address_address1" />
    @else
    <input placeholder="Address" autocomplete="shipping address-line1" data-backup="address1" class="field__input" size="30" type="text" value="{{ old('shipping_address') }}" name="shipping_address" id="checkout_shipping_address_address1" />
    @endif
    @if ($errors->has('shipping_address'))
      <span class="help-block">
          <strong>{{ $errors->first('shipping_address') }}</strong>
      </span>
    @endif
  </div>
</div>
<!--   <div class="field--third field field--optional" data-address-field="address2">
    <label class="field__label" for="checkout_shipping_address_address2">Apt, suite, etc. (optional)</label>
    <div class="field__input-wrapper">
      <input placeholder="Apt, suite, etc. (optional)" autocomplete="shipping address-line2" data-backup="address2" class="field__input" size="30" type="text" value="2nd floor" name="checkout[shipping_address][address2]" id="checkout_shipping_address_address2" />
    </div>
</div> -->
<div data-address-field="city" class="field--half field field--required field--show-floating-label">
  <label class="field__label" for="checkout_shipping_address_city">City</label>
  <div class="field__input-wrapper">

  @if(isset($address->city))
    <input placeholder="City" autocomplete="shipping address-level2" data-backup="city" class="field__input" size="30" type="text" value="{{$address->city}}" name="shipping_city" id="checkout_shipping_address_city"  style="    margin-bottom: -1%;" />
  @else
    <input placeholder="City" autocomplete="shipping address-level2" data-backup="city" class="field__input" size="30" type="text" value="{{ old('shipping_city') }}" name="shipping_city" id="checkout_shipping_address_city" style="    margin-bottom: -1%;"  />
  @endif

   @if ($errors->has('shipping_city'))
      <span class="help-block">
          <strong>{{ $errors->first('shipping_city') }}</strong>
      </span>
  @endif
  </div>
</div>

<div data-address-field="city" class="field--half field field--required field--show-floating-label">
  <label class="field__label" for="checkout_shipping_address_city">State</label>
  <div class="field__input-wrapper">

  @if(isset($address->state))
    <input placeholder="State" autocomplete="shipping address-level2" data-backup="city" class="field__input" size="30" type="text" value="{{$address->state}}" name="shipping_state" id="checkout_shipping_address_city" />
  @else
    <input placeholder="State" autocomplete="shipping address-level2" data-backup="city" class="field__input" size="30" type="text" value="{{ old('shipping_state') }}" name="shipping_state" id="checkout_shipping_address_city" />
  @endif

  @if ($errors->has('shipping_state'))
      <span class="help-block">
          <strong>{{ $errors->first('shipping_state') }}</strong>
      </span>
  @endif
  </div>
</div>

<div data-address-field="country" class="field--half field field--required field--show-floating-label">
  <label class="field__label" for="checkout_shipping_address_country">Country</label>
  <div class="field__input-wrapper">
    @if(isset($address->country))
      <input placeholder="Country" autocomplete="shipping address-line2" data-backup="address2" class="field__input" size="30" type="text" value="{{$address->country}}" name="checkout[shipping_address][address2]" id="checkout_shipping_address_address2" />
     @else
      <input placeholder="Country" autocomplete="shipping address-line2" data-backup="address2" class="field__input" size="30" type="text" value="" name="checkout[shipping_address][address2]" id="checkout_shipping_address_address2" />
    @endif

  </div>
</div>
<input class="visually-hidden" autocomplete="shipping address-level1" tabindex="-1" data-autocomplete-field="province" size="30" type="text" name="checkout[shipping_address][province]" />

<div data-address-field="zip" class="field--half field field--required field--show-floating-label">
  <label class="field__label" for="checkout_shipping_address_zip">Postal code</label>
  <div class="field__input-wrapper">
  @if(isset($address->zip))
    <input placeholder="110034" autocomplete="shipping postal-code" data-backup="zip" class="field__input field__input--zip" size="30" type="text" value="{{$address->zip}}" name="shipping_zip" id="checkout_shipping_address_zip" />
    @else
    <input placeholder="110034" autocomplete="shipping postal-code" data-backup="zip" class="field__input field__input--zip" size="30" type="text" value="{{ old('shipping_zip') }}" name="shipping_zip" id="checkout_shipping_address_zip" />
    @endif

      @if ($errors->has('shipping_zip'))
      <span class="help-block">
          <strong>{{ $errors->first('shipping_zip') }}</strong>
      </span>
      @endif
  </div>
</div>
  <div data-address-field="phone" class="field field--required">
    <label class="field__label" for="checkout_shipping_address_phone">Phone</label>
    <div class="field__input-wrapper">
      <input placeholder="Phone" autocomplete="shipping tel" data-backup="phone" data-phone-formatter="true"  class="field__input" size="30" type="tel" value="{{Auth::user()->contact}}" name="shipping_contact" id="checkout_shipping_address_phone" />
    </div>
      @if ($errors->has('shipping_contact'))
      <span class="help-block">
          <strong>{{ $errors->first('shipping_contact') }}</strong>
      </span>
      @endif
</div>

      </div> 
    
  </div>

  

<div class="step__footer">
  <input type="submit" class="btn__content step__footer__continue-btn btn " value="Continue to payment method">
  <i class="btn__spinner icon icon--button-spinner"></i>
</form>
  <a class="step__footer__previous-link" href="{{URL::to('/cart')}}">
<svg class="previous-link__icon icon--chevron icon" xmlns="http://www.w3.org/2000/svg" width="6.7" height="11.3" viewBox="0 0 6.7 11.3">
    <path d="M6.7 1.1l-1-1.1-4.6 4.6-1.1 1.1 1.1 1 4.6 4.6 1-1-4.6-4.6z" />
</svg>
Return to cart</a>
</div>

</div>


          </div>
          <div class="main__footer">
            <div class="modals">
    <div class="modal-backdrop" role="dialog" id="policy-18374787" aria-labelledby="policy-18374787-title" data-modal-backdrop>
  <div class="modal">
    <div class="modal__header">
      <h1 class="modal__header__title" id="policy-18374787-title">
        Privacy policy
      </h1>
      <div class="modal__close">
        <button type="button" class="icon icon--close-modal" data-modal-close>
          <span class="visually-hidden">
            Close
          </span>
        </button>
      </div>
    </div>
    <div class="modal__content">
      <svg class="modal__loading-icon icon icon--spinner" width="32" height="32" xmlns="http://www.w3.org/2000/svg"><path d="M32 16c0 8.837-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0v2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14h2z" /></svg>

    </div>
  </div>
</div>

    <div class="modal-backdrop" role="dialog" id="policy-18374723" aria-labelledby="policy-18374723-title" data-modal-backdrop>
  <div class="modal">
    <div class="modal__header">
      <h1 class="modal__header__title" id="policy-18374723-title">
        Refund policy
      </h1>
      <div class="modal__close">
        <button type="button" class="icon icon--close-modal" data-modal-close>
          <span class="visually-hidden">
            Close
          </span>
        </button>
      </div>
    </div>
    <div class="modal__content">
      <svg class="modal__loading-icon icon icon--spinner" width="32" height="32" xmlns="http://www.w3.org/2000/svg"><path d="M32 16c0 8.837-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0v2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14h2z" /></svg>

    </div>
  </div>
</div>

    <div class="modal-backdrop" role="dialog" id="policy-18374851" aria-labelledby="policy-18374851-title" data-modal-backdrop>
  <div class="modal">
    <div class="modal__header">
      <h1 class="modal__header__title" id="policy-18374851-title">
        Terms of service
      </h1>
      <div class="modal__close">
        <button type="button" class="icon icon--close-modal" data-modal-close>
          <span class="visually-hidden">
            Close
          </span>
        </button>
      </div>
    </div>
    <div class="modal__content">
      <svg class="modal__loading-icon icon icon--spinner" width="32" height="32" xmlns="http://www.w3.org/2000/svg"><path d="M32 16c0 8.837-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0v2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14h2z" /></svg>

    </div>
  </div>
</div>

</div>


<!-- <div role="contentinfo" aria-label="Footer">
    <ul class="policy-list">
        <li class="policy-list__item">
          <a title="Refund policy" data-modal="policy-18374723" data-close-text="Close" href="https://checkout.shopify.com/11271070/policies/18374723.html">Refund policy</a>
        </li>
        <li class="policy-list__item">
          <a title="Privacy policy" data-modal="policy-18374787" data-close-text="Close" href="https://checkout.shopify.com/11271070/policies/18374787.html">Privacy policy</a>
        </li>
        <li class="policy-list__item">
          <a title="Terms of service" data-modal="policy-18374851" data-close-text="Close" href="https://checkout.shopify.com/11271070/policies/18374851.html">Terms of service</a>
        </li>
    </ul>
</div> -->

<div id="dialog-close-title" class="hidden">Close</div>

          </div>
        </div>
      </div>
    </div>

    <script>
      $('.apply').click(function(){
        var base_url = "{{URL::to('/')}}";
        var discout = $("#discout").val();
        var taxvalue = $("#tax_value").val();

        $.ajax({
          url: base_url+'/check-discount',
          type: "post",
          data: 
          {
            "ordercode": "{{$order->order_code}}",
            "discout": discout
          },
          success:function(data)
          {
            if(data.status==0){
              $('.discount-message').html(data.message);
            }
            if(data.status==1){
              $('.discount-message').html(data.message);
              $('.discount_price_div').show();
              $('.discount_code_div').show();
              $('.discount_price').html("Rs."+data.discount_value);
              $('.discount_code').html(data.coupon_code);
              $('.amount_after_discount').html("Rs."+(data.after_discount_price + taxvalue));
              $('#discount_div').hide();
              $('.shipping_rate_value').html(data.shipping);
              $('#tax_value').html('Rs. '+ data.final_tax);
              $('.payble_amount').html(data.after_discount_price);
            }
          }
        });
      });

      $('.delete_discount').click(function(){
        var base_url = "{{URL::to('/')}}";

        $.ajax({
          url: base_url+'/remove-discount',
          type: "post",
          data: 
          {
            "ordercode": "{{$order->order_code}}",
          },
          success:function(data)
          {
            if (data.status==1) {
              $('#discount_div').show();
              $('.discount_price_div').hide();
              $('.discount_code_div').hide();
              $('.discount-message').hide();
              $('.shipping_rate_value').html(data.shipping);
              $('#tax_value').html('Rs. '+ data.final_tax);
              $('.payble_amount').html('Rs. '+ data.total_amount);
            };
          }
        });
      });
    </script>
    
      <script type="text/javascript">
        
      window.shopifyAnalytics = window.shopifyAnalytics || {};
      window.shopifyAnalytics.meta = window.shopifyAnalytics.meta || {};
      window.shopifyAnalytics.meta.currency = 'INR';
      var meta = {};
      for (var attr in meta) {
        window.shopifyAnalytics.meta[attr] = meta[attr];
      }
    
      </script>

      <script type="text/javascript">
        window.shopifyAnalytics.merchantGoogleAnalytics = function() {
          
        };
      </script>

      <script type="text/javascript" class="analytics">
        (window.gaDevIds=window.gaDevIds||[]).push('BwiEti');
        

        (function () {
          var customDocumentWrite = function(content) {
            var jquery = null;

            if (window.jQuery) {
              jquery = window.jQuery;
            } else if (window.Checkout && window.Checkout.$) {
              jquery = window.Checkout.$;
            }

            if (jquery) {
              jquery('body').append(content);
            }
          };

          var trekkie = window.shopifyAnalytics.lib = window.trekkie = window.trekkie || [];
          if (trekkie.integrations) {
            return;
          }
          trekkie.methods = [
            'identify',
            'page',
            'ready',
            'track',
            'trackForm',
            'trackLink'
          ];
          trekkie.factory = function(method) {
            return function() {
              var args = Array.prototype.slice.call(arguments);
              args.unshift(method);
              trekkie.push(args);
              return trekkie;
            };
          };
          for (var i = 0; i < trekkie.methods.length; i++) {
            var key = trekkie.methods[i];
            trekkie[key] = trekkie.factory(key);
          }
          trekkie.load = function(config) {
            trekkie.config = config;
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.onerror = function(e) {
              (new Image()).src = '//v.shopify.com/internal_errors/track?error=trekkie_load';
            };
            script.async = true;
            script.src = 'https://cdn.shopify.com/s/javascripts/tricorder/trekkie.storefront.min.js?v=2016.12.06.1';
            var first = document.getElementsByTagName('script')[0];
            first.parentNode.insertBefore(script, first);
          };
          trekkie.load(
            {"Trekkie":{"appName":"storefront","environment":"production","defaultAttributes":{"shopId":11271070}},"Performance":{"navigationTimingApiMeasurementsEnabled":true,"navigationTimingApiMeasurementsSampleRate":0.001},"Google Analytics":{"trackingId":"UA-75364171-1","domain":"auto","siteSpeedSampleRate":"10","enhancedEcommerce":true,"doubleClick":true,"includeSearch":true},"Facebook Pixel":{"pixelIds":["701339193301932"],"agent":"plshopify1.2"}}
          );

          var loaded = false;
          trekkie.ready(function() {
            if (loaded) return;
            loaded = true;

            window.shopifyAnalytics.lib = window.trekkie;
            
      ga('require', 'linker');
      function addListener(element, type, callback) {
        if (element.addEventListener) {
          element.addEventListener(type, callback);
        }
        else if (element.attachEvent) {
          element.attachEvent('on' + type, callback);
        }
      }
      function decorate(event) {
        event = event || window.event;
        var target = event.target || event.srcElement;
        if (target && (target.getAttribute('action') || target.getAttribute('href'))) {
          ga(function (tracker) {
            var linkerParam = tracker.get('linkerParam');
            document.cookie = '_shopify_ga=' + linkerParam + '; ' + 'path=/';
          });
        }
      }
      addListener(window, 'load', function(){
        for (var i=0; i < document.forms.length; i++) {
          var action = document.forms[i].getAttribute('action');
          if(action && action.indexOf('/cart') >= 0) {
            addListener(document.forms[i], 'submit', decorate);
          }
        }
        for (var i=0; i < document.links.length; i++) {
          var href = document.links[i].getAttribute('href');
          if(href && href.indexOf('/checkout') >= 0) {
            addListener(document.links[i], 'click', decorate);
          }
        }
      });
    

            var originalDocumentWrite = document.write;
            document.write = customDocumentWrite;
            try { window.shopifyAnalytics.merchantGoogleAnalytics.call(this); } catch(error) {};
            document.write = originalDocumentWrite;

            
        window.shopifyAnalytics.lib.page(
          "Checkout - Contact information",
          {"path":"\/checkout\/contact_information","search":"","url":"https:\/\/checkout.shopify.com\/11271070\/checkouts\/16d807336e0758feda31e0bd838b93c8?step=contact_information"}
        );
      
            
          });

          
      var eventsListenerScript = document.createElement('script');
      eventsListenerScript.async = true;
      eventsListenerScript.src = "//cdn.shopify.com/s/assets/shop_events_listener-37861b7e433c159ab7ac8932b47d1b8813173005dff35ce87a5900b43e3492ed.js";
      document.getElementsByTagName('head')[0].appendChild(eventsListenerScript);
    
        })();
      </script>
      
  </body>
</html>
