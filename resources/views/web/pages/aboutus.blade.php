@extends('web.web_layout')

@section('content')

<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
        
        
        
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle">About Us</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
    
   
    
</div>
</div>
  </div>
  <div class="user-content row-spacing lightboximages textjustify">
    When a bunch of guys with a common love for excellent meat come together, good things happen:<br><br>We began Primo Foods in 2013 with the simple goal of providing the highest quality meat products across India. We set up our factory near Gurgaon- not too far from New Delhi - and imported the world’s best machinery from Germany and Australia to ensure top-notch quality.<br><br>We believe that availability of high quality food leads to nutritious, delicious meals; and in March 2016, Primo Foods launched LionFresh.com - an online market for our own products that customers can trust. Lionfresh.com delivers our<strong> high quality meats (fresh, ready to eat, slow-cooked and frozen), as well as seafood, all securely and hygienically packed directly to your front door</strong>. From our own state of the art processing and delivery units to our relationships with farms worldwide, LionFresh.com travels near and far to bring you both quality and choice like never before. <br><br>With a large presence across all Tier-One cities, and our <strong>in-house brands La Carne Cuts, Howdyji and LionFresh.com</strong>, Primo Foods is one of India's fastest growing, food processing businesses. While we pat ourselves on the back for what we are doing, we've got lots of plans ahead, and are busy researching ways to reach more cities, more families, and more hungry people all the time. <br><br>For more information about our business - please do visit <a href="http://www.primofoodsindia.com/" target="_blank">www.PrimoFoodsIndia.com</a>
  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>
@endsection