@extends('web.web_layout')

@section('content')
<style type="text/css">
  
.lightboximages p{ text-align: justify; }

</style>

<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
        
        
        
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle">Privacy Policy</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
    
   
    
</div>
</div>
  </div>
  <div class="user-content row-spacing lightboximages">
   <p>This privacy policy sets out how LionFresh (hereafter referred to as 'us' or 'we') uses and protects any information that you give us when you use this website.</p>

<p>We are committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.</p>

<p>We may change this policy occasionally by updating this page. You should check this page from time to time to ensure that you accept any changes. This policy is effective from March 1, 2016.</p>

<h4>We may collect the following information:</h4>

<p>Name and job title, contact information including email address, demographic information such as pincode, preferences and interests, other information relevant to customer surveys and/or offers</p>

<p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>

<h4>Internal record keeping.</h4>
<p>We may use the information to improve our products and services.
We may periodically send promotional emails about new products, special offers or other information, which we think you may find interesting using the email address, which you have provided.
Occasionally, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customise the website according to your interests.
We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>

<h4>HOW WE USE COOKIES</h4>

<p>A cookie is a small file, which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>

<p>We use traffic log cookies to identify which pages are being used. This helps us analyse data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>

<p>Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>

<p>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website. </p>

<h4>LINKS TO OTHER WEBSITES</h4>

<p>Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information, which you provide whilst visiting such sites and this privacy statement does not govern such sites. You should exercise caution and look at the privacy statement applicable to the website in question.</p>

<h4>CONTROLLING YOUR PERSONAL INFORMATION</h4>

<h5>You may choose to restrict the collection or use of your personal information in the following ways:</h5>

<p>Whenever you are asked to fill in a form on the website, look for the box that you can click to indicate that you do not want the information to be used by us for direct marketing purposes if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at info@LionFresh.com we will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties, which we think you may find interesting if you tell us that you wish this to happen.</p>

<p>You may request details of personal information, which we hold about you under the data protection act 1998. A small fee will be payable. If you would like a copy of the information held on you please write to info@LionFresh.com</p>

<p>If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>
  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>
@endsection