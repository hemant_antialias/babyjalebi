@extends('web-files.web_layout')
<style type="text/css">
input[type=number]::-webkit-outer-spin-button,
input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance:textfield;
}
    .panel-body{
      /*  margin-left: 30%;
        margin-right: 30%;*/
    }
    .panel-heading {
       /* margin-right: 18%;
        margin-left: 18%;*/
    }
    .btn {width: 100%;}
    .panel-heading.subdiv {
    background: transparent !important;
    border: none;
    font-size: 24px;
    font-family: 'EB Garamond', serif;
    font-weight: normal;
}
    .panel-default{
        text-align: center;
    }
    .textalign {
        text-align: center;
    }
    .subdiv{
        margin-top: 10px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .centerblock input[type='password'], input[type='number'], input[type='text'],, input[type='email'] {
   /*width: 50%;*/
   
   margin-left: auto;
   margin-right: auto;
   font-size: 15px;
   line-height: 15px;
   background-color: #ffffff ;
   color: #636465 ;
   border: 1px solid #e7e7e1 ;
   padding: 11px 15px;
   margin: 0;
   vertical-align: middle;
   /*max-width: 100%;*/
   letter-spacing: 1px;
       
}
.form-control
{
   height: 45px !important;
    width: 100% ; 
}
.widthform{
    width: 100% !important;
}
.popup-closer-location
{
    top:5% !important;
}

</style>

@section('content')
<div class="container">
    <div class="registerrow row ">
        <div class="col-sm-12 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading subdiv">REGISTER</div>
                <div class="panel-body">
                    <form class="form-horizontal " role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            

                            <div>
                                <input id="first_name" type="text" class="widthform form-control textalign " name="first_name" value="{{ old('first_name') }}" placeholder="FIRST NAME" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            
                            <div>
                                <input id="last_name" type="text" class="widthform form-control textalign " name="last_name" value="{{ old('last_name') }}" placeholder="LAST NAME" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           

                            <div>
                                <input id="email" type="email" class="widthform form-control textalign " name="email" value="{{ old('email') }}" placeholder="EMAIL" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                           

                            <div>

                                <input id="number" type="number" class="widthform form-control textalign " name="contact" value="{{ old('number') }}" placeholder="PHONE NUMBER" required>

                                @if ($errors->has('contact'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            

                            <div>
                                <input id="password" type="password" class="widthform form-control textalign regpas" name="password"  placeholder="ENTER PASSWORD (MINIMUM 6 CHARACTERS)" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                           

                            <div>
                                <input id="password-confirm" type="password" class="widthform form-control textalign " name="password_confirmation" placeholder="CONFIRM PASSWORD" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="subdiv">
                                <button type="submit" class="btn btn-primary registersubmit" >
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
