@extends('web-files.web_layout')

@section('content')
<style>
#email {width: 100% !important;}

.panel-default {border: none !important;}
.panel-default>.panel-heading {
    color: #333;
    background-color: transparent !important;
    border-color: transparent !important;
    font-size: 24px;
}
.panel-body{
       /* margin-left: 30%;
        margin-right: 30%;*/
    }
    .panel-heading {
       /* margin-right: 18%;
        margin-left: 18%;*/
    }
    .panel-default{
        text-align: center;
    }
    .textalign {
        text-align: center;
    }
    .subdiv{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    input[type='password'], input[type='number'], input[type='text'],, input[type='email'] {
   /*width: 50%;*/
   
   margin-left: auto;
   margin-right: auto;
   font-size: 15px;
   line-height: 15px;
   background-color: #ffffff ;
   color: #636465 ;
   border: 1px solid #e7e7e1 ;
   padding: 11px 15px;
   margin: 0;
   vertical-align: middle;
   /*max-width: 100%;*/
   letter-spacing: 1px;
       
}
.form-control
{
   height: 36px !important;
    width: 100% !important; 
    text-align: center;
}
.panel-body {
    margin-left: 30%;
    margin-right: 30%;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                            <div class="">
                                <input id="email" type="email" class="form-control" placeholder="E-Mail" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

                            <div class="">
                                <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <!-- <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label> -->
                            <div class="">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
