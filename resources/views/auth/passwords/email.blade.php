@extends('web.web_layout')

<!-- Main Content -->
@section('content')
<style>
#email {width: 100% !important;}

.panel-default {border: none !important;}
.panel-default>.panel-heading {
    color: #333;
    background-color: transparent !important;
    border-color: transparent !important;
    font-size: 24px;
}
.panel-body{
       /* margin-left: 30%;
        margin-right: 30%;*/
    }
    .panel-heading {
       /* margin-right: 18%;
        margin-left: 18%;*/
    }
    .panel-default{
        text-align: center;
    }
    .textalign {
        text-align: center;
    }
    .subdiv{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    input[type='password'], input[type='number'], input[type='text'],, input[type='email'] {
   /*width: 50%;*/
   
   margin-left: auto;
   margin-right: auto;
   font-size: 15px;
   line-height: 15px;
   background-color: #ffffff ;
   color: #636465 ;
   border: 1px solid #e7e7e1 ;
   padding: 11px 15px;
   margin: 0;
   vertical-align: middle;
   /*max-width: 100%;*/
   letter-spacing: 1px;
       
}
.form-control
{
   height: 36px !important;
    /*width: 100%; */
    text-align: center;
}
.panel-body {
    margin-left: 30%;
    margin-right: 30%;
}
.widthform{
    width: 100% !important;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            <br>
            <br>
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                            <div class="">
                                <input id="email" type="email" class="form-control widthform" placeholder="Enter Your E-Mail" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary" style="width: 99%;">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
