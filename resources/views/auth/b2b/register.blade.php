<!DOCTYPE html>
<html>
<head>
	<title>login</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/signup.css">
	
</head>
<body>
<style>
	
		body{ background: white; }
		.personaldetails{
			    padding: 20px;
			    border: 1px dashed gainsboro;
			    background: #f7f7f7;
			    margin: 1px;
		}
		.checkbox+.checkbox, .radio+.radio{
			margin-top: 11px;
		}
		.margintop5{
			margin-top: 10px;
			border-radius: 4px;
		    padding: 3px;
		    border: 1px solid gainsboro;
		    padding-left: 9px;
		    padding-right: 0px;
		}
		.special{
			width: 63%;
		}
		.registerbtn{
			    width: 130px;
    padding: 10px;
		}
		#submitblock{
			    margin: 10px;
    		float: right;
		}
		#signup{
			width: 10%;
			margin-left: auto;
			margin-right: auto;
			color: #7d7d7d;
		}
		input { 
    text-align: left !important; 
}
		h3{ margin-bottom: 35px; }
		label { margin-bottom: 10px; font-weight: 500 !important; }
		/* webkit solution */
::-webkit-input-placeholder { text-align:left; /*color:#d4d1d1;*/}
/* mozilla solution */
input:-moz-placeholder { text-align:left; }

</style>
<div class="container" >
<form method="post" action="{{url('/business/register')}}">
			<h2 id="signup">Sign Up</h2>
	<div class="personaldetails row">
		<h3>PERSONAL DETAILS</h3>
			<div class="form-group col-md-4 col-sm-12">
			  <label for="usr">First Name:</label>
			  <input type="text" name="first_name" class="form-control" id="usr">
			</div>
			<div class="form-group col-md-4 col-sm-12">
			  <label for="mobno">Last Name:</label>
			  <input type="text" name="last_name" class="form-control" id="mobno">
			</div>
			<div class="form-group col-md-4 col-sm-12">
			  <label for="mobno">Email:</label>
			  <input type="text" name="email" class="form-control" id="mobno">
			</div>
			<div class="form-group col-md-4 col-sm-12">
			  <label for="phone">Phone No:</label>
			  <input type="text" name="phone" class="form-control" id="phone">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="phone">Password:</label>
			  <input type="text" name="password" class="form-control" id="phone">
			</div>

			<div class="form-group col-md-6 col-sm-12">
			  <label for="usr">Address:</label>
			  <input type="text" name="address" class="form-control" id="usr">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="usr">Address 2:</label>
			  <input type="text" name="address2" class="form-control" id="usr">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="usr">City:</label>
			  <input type="text" name="city" class="form-control" id="usr">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="usr">State:</label>
			  <input type="text" name="state" class="form-control" id="usr">
			</div>
	</div>
	<div class="personaldetails row">
		<h3>BUSINESS DETAILS</h3>
		   <div class="form-group">
		   		<h4>Type of Business</h4>
		   		<div class="checkbox col-md-4 col-sm-4">
				  <label><input type="checkbox" name="business_type[]" value="hotel">Hotel</label>
				</div>
				<div class="checkbox col-md-4 col-sm-4">
				  <label><input type="checkbox" name="business_type[]" value="restaurant">Restaurant</label>
				</div>
				<div class="checkbox col-md-4 col-sm-4">
				  <label><input type="checkbox" name="business_type[]" value="qsr">QSR</label>
				</div>
				<div class="checkbox col-md-4 col-sm-4">
				  <label><input type="checkbox" name="business_type[]" value="retailer">Retailer</label>
				</div>
				<div class="checkbox col-md-4 col-sm-4">
				  <label><input type="checkbox" name="business_type[]" value="dealer">Distributor/ Dealer</label>
				</div>
				<div class="checkbox col-md-4 col-sm-4">
				  <label><input type="checkbox" name="business_type[]" value="exporter">Exporter</label>
				</div>
				<div class="checkbox col-md-12 col-sm-12">
				  <label><input type="checkbox" name="business_type[]" value="specify">Specify</label>
				</div>
		   </div>
			
			<div class="form-group col-md-4 col-sm-12">
			  <label for="businame">Business Name:</label>
			  <input type="text" name="business_name" class="form-control" id="businame">
			</div>
			<div class="form-group col-md-4 col-sm-12">
			  <label for="bmobno">Mobile no: (Optional)</label>
			  <input type="text" name="mobile" class="form-control" id="bmobno">
			</div>
			<div class="form-group col-md-4 col-sm-12">
			  <label for="busiphone">Business Phone No:</label>
			  <input type="text" name="phone" class="form-control" id="busiphone">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="bizadd">Business Address:</label>
			  <input type="text" name="baddress" class="form-control" id="bizadd">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="bizadd">Business Address 2:</label>
			  <input type="text" name="baddress2" class="form-control" id="bizadd">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="bizadd">Business City:</label>
			  <input type="text" name="bcity" class="form-control" id="bizadd">
			</div>
			<div class="form-group col-md-6 col-sm-12">
			  <label for="bizadd">Business State:</label>
			  <input type="text" name="bstate" class="form-control" id="bizadd">
			</div>
	</div>

	<div class="personaldetails row">
		<h3>STATUTORY DETAILS</h3>
			<div class="form-group col-md-3 col-sm-12">
			  <label for="lst">Local Sales Tax & Validity Date</label>
			  <input type="text" name="lst" class="form-control special" id="lst" placeholder="Registration Number">
			  <!-- <label>Validity</label> -->
			  <input type="text" name"lst_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
			<div class="form-group col-md-3 col-sm-12 ">
			  <label for="lst">Service Tax </label>
			  <input type="text" name="service_tax" class="form-control special" id="lst" placeholder="Registration Number">
			 
			  <input type="text" name="st_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
			<div class="form-group col-md-3 col-sm-12">
			  <label for="lst">Central Sales Tax </label>
			  <input type="text" name="cst" class="form-control special" id="lst" placeholder="Registration Number">
			   <input type="text" name="cst_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
			<div class="form-group col-md-3 col-sm-12">
			  <label for="lst">Value Added Tax </label>
			  <input type="text" name="vat" class="form-control special" id="lst" placeholder="Registration Number">
			   <input type="text" name="vat_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
			<div class="form-group col-md-3 col-sm-12">
			  <label for="lst">FDA License </label>
			  <input type="text" name="fda_license" class="form-control special" id="lst" placeholder="Registration Number">
			   <input type="text" name="fda_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
			<div class="form-group col-md-3 col-sm-12">
			  <label for="lst">TIN </label>
			  <input type="text" name="tin" class="form-control special" id="lst" placeholder="Registration Number">
			   <input type="text" name="tin_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
			<div class="form-group col-md-3 col-sm-12">
			  <label for="lst">PAN </label>
			  <input type="text" name="pan" class="form-control special" id="lst" placeholder="Registration Number">
			   <input type="text" name="pan_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
			<div class="form-group col-md-3 col-sm-12">
			  <label for="lst">Other Licenses, if any </label>
			  <input type="text" name="other" class="form-control special" id="lst" placeholder="Registration Number">
			   <input type="text" name="other_date" class="datepicker margintop5" placeholder="Validity : " /></p>
			</div>
	</div>
		<div class="checkbox col-md-12 col-sm-12">
					  <label><input type="checkbox" value="">I hereby declare and certify that I am the authorized person to fill this form and the information provided above is true, correct and complete.</label>
		</div>
		<div id="submitblock">
			<button type="submit" class="btn btn-warning" id="registerbtn">Register</button>
		</div>
</form>
</div>
<footer  id="footermainblock">
	  
		<div class="container footcontainer">
			<div class="footcols">ABOUT US</div>
			<div class="footcols">CARRIERS</div>
			<div class="footcols">BLOGS</div>
			<div class="footcols">CONTACT US</div>
		</div>
	  
	</footer>


<!-- all the javascript goes here  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
	// Insert placeholder as prefix in the value, when user makes a change.
$(".datepicker").datepicker({
    onSelect: function(arg) {
        $(this).val(arg);
    }
});
</script>
</body>
</html>