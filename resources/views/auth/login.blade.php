@extends('web-files.web_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{URL::to('css/login.css')}} ">
<style type="text/css">
.loginpara1and2{ color:#ec4e4e ;margin-left:auto; margin-right:auto; width:40%;}
@media only screen and ( max-width: 680px){
  .loginpara1and2{ width: 80%;}
}
  .row{
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    font-family:verdana;
    text-align: center;
    margin-top: 10px;
    margin-bottom: 40px;
  }
  .row input[type='email']{
    text-align: center;
    width: 362px;
    margin-left: auto;
    margin-right: auto;
    padding: 11px 15px;
    letter-spacing: 1px;
    border-radius: 0px;
  }
  .row input[type='password']{
    text-align: center;
    width: 362px;
    margin-left: auto;
    margin-right: auto;
    padding: 11px 15px;
    letter-spacing: 1px;
    border-radius: 0px;
  }
  .row button{
    background: #464e54 ;
    color: white;
    text-align: center;
    margin-top: 20px;
    margin-bottom: 10px;
  }
  .row h2 {
    margin-top: 30px;
  }
  .row button{
    width: 34%;
  }
  @media only screen  and ( max-width: 420px){
   
.row
  {
width: 100%;
  }
  .row input[type='email'] {
    text-align: center;
    width: 292px;
    margin-left: auto;
    margin-right: auto;
    padding: 11px 15px;
    letter-spacing: 1px;
    border-radius: 0px;
}
.row input[type='password']
{
  width: 292px;
}
}
@media only screen  and ( max-width: 340px){
  .row
  {
width: 100%;
  }
  .row input[type='email'] {
    text-align: center;
    width: 235px;
    margin-left: auto;
    margin-right: auto;
    padding: 11px 15px;
    letter-spacing: 1px;
    border-radius: 0px;
}
.row input[type='password']
{
  width: 235px;
}

</style>
<div class="container">
  <div class="row" style="">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}
      <h2>LOGIN
      </h2>
      <input type="email" class="form-control large" id="usr" name="email" value="{{ old('email') }}" placeholder="EMAIL" style="" required autofocus>
      <br>
      <br>
      
      <input type="password" class="form-control" name="password" id="pwd" placeholder="PASSWORD" style="">
      <br>
      
      @if(Session::has('message'))
        <p class="loginpara1and2">We Are Making Our Systems Even More Robust. Your Password Has Been Changed To <b>"lionfresh123"</b>.
        <p class="loginpara1and2">  After logging in,Kindly Update With A Fresh Password In The My-Account Section.</p>
      @endif

      @if ($errors->has('password'))

      <span class="help-block">
        <br>
        <strong style="color: red;">{{ $errors->first('password') }}
        </strong>
      </span>
      @endif
      @if ($errors->has('email'))
      <span class="help-block1">
        <br>
        <strong style="color: red;">{{ $errors->first('email') }}
        </strong>
      </span>
      <br>
      @endif
      <!-- <div class="forgotpassword"> 
        <a href = "">Forgot your password?
        </a>
      </div> -->
     
      <input type="checkbox" name="remember_me" id="remember_me"><span>  Remember Me</span><br>
      <a href="{{URL::to('/password/reset')}}">Forgot your password?</a><br>
      <button type="submit" class="btn btn-primary">
        Login
      </button>
    </form>
    <div class="clearfix">
    </div>
    
     Create account <a href="{{URL::to('/register')}}" style="color: #d8920c;">Register
      </a> 
    
  </div>
</div>
<script>
            $(function() {
 
                if (localStorage.chkbx && localStorage.chkbx != '') {
                    $('#remember_me').attr('checked', 'checked');
                    $('#usr').val(localStorage.usrname);
                    $('#pwd').val(localStorage.pass);
                } else {
                    $('#remember_me').removeAttr('checked');
                    $('#username').val('');
                    $('#pass').val('');
                }
 
                $('#remember_me').click(function() {
 
                    if ($('#remember_me').is(':checked')) {
                        // save username and password
                        localStorage.usrname = $('#usr').val();
                        localStorage.pass = $('#pwd').val();
                        localStorage.chkbx = $('#remember_me').val();
                    } else {
                        localStorage.usrname = '';
                        localStorage.pass = '';
                        localStorage.chkbx = '';
                    }
                });
            });
 
        </script>
@endsection
  