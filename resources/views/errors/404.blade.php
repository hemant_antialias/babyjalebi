@extends('web-files.web_layout')

@section('content')
	<br><br><br><br>
	<div class="imgfornotfoundpage" style="text-align: center;">
		<img src="{{url('/web-asset/img/mainlogo.png')}} " id="notfoundimg">
	</div>
	<div id="notfoundsometext" style="text-align: center;">
		<p id="notfoundpara">
			Grrrr! The page you are looking for is away from us.
		</p>
		<p> Return to our   <span id="returnhome"><a href="{{URL::to('/')}}"> homepage .</a> </span> </p>
	</div>
<br><br><br><br>
@endsection