<?php
use App\ProductImages;
?>

@foreach($products as $product)

  <li class="annoushka balenciaga grid-view  col-xs-12 col-md-4 col-sm-6 post-96 product type-product status-publish has-post-thumbnail product_cat-couture product_cat-fall-2016 product_cat-spring-2017 product_tag-accessories product_tag-bridal product_tag-trend first instock sale featured downloadable taxable shipping-taxable purchasable product-type-simple">
    <div class="product-wrapper">
      <div class="product-img-wrapper">
         @if(isset($categoryslug) && !isset($subcategoryslug))
                  <a href="{{url('/product/'. strtolower($categoryslug). '/' .$product->slug)}}" title="{{$product->slug}}" class="woocommerce-LoopProduct-link">  
                  @else
                    <a href="{{url('/'.$categoryslug. '/' .$subcategoryslug. '/' .$product->slug)}}" title="{{$product->slug}}" class="woocommerce-LoopProduct-link">  
                    @endif
          <div class="background-img-product">
          </div>
          <!-- <span class="onsale">Top Pick -->
          </span>
          <img width="265" height="365" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="{{$product->product_title}}" title="product11" />
          
          
          <div class="clear">
          </div>     
        </a>  
      </div>
      <div class="product-description-wrapper">

       @if(isset($categoryslug) && !isset($subcategoryslug))
          </a>
        <a class="product-title1" href="{{url('/'. strtolower($categoryslug). '/' .$product->slug)}}" title="Click to learn more about Donec eu libero tristique egestas malesu">

          <h2 class="woocommerce-loop-product__title">{{$product->product_title}}
          </h2>   
        </a>
          @endif
          
          @if(isset($categoryslug) && isset($subcategoryslug))
          </a>
        <a class="product-title2" href="{{url('/'.$categoryslug. '/' .$subcategoryslug. '/' .$product->slug)}}" title="Click to learn more about Donec eu libero tristique egestas malesu">

          <h2 class="woocommerce-loop-product__title">{{$product->product_title}}
          </h2>   
        </a>
          @endif
         

        @if($product->compare_price != 'NULL')
        <span class="price">
          <del>
            <span class="woocommerce-Price-amount amount">
              <span class="woocommerce-Price-currencySymbol"> {{$product->compare_price}}
              </span>
            </span>
          </del> 
          <ins>
            <span class="woocommerce-Price-amount amount">
              <span class="woocommerce-Price-currencySymbol">&#x20B9; {{$product->price}}
              </span>
            </span>
          </ins>
        </span>
        @else
        <span class="price">
          <del>
            <span class="woocommerce-Price-amount amount">
              <span class="woocommerce-Price-currencySymbol">&#x20B9; {{$product->price}}
              </span>
            </span>
          </del> 
        @endif
       <!--  <div class="star-rating" title="Rated 5.00 out of 5">
          
        </div>   -->    
        <div class="buttons-wrapper">
         <!-- <form>
                           <input type="text" name="quantity" value="1" style="display: none;">
                          <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                          <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                          <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                          @if(isset($product->product_images))
                          <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
                          @endif
                          <input type="submit" class=" button product_type_simple add_to_cart_button ajax_add_to_cart cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Bag" style="background: #000 !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                          <div class="cartmeatmob cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                        </form> -->

         <!--  @if(isset($categoryslug))
          <a href="{{url('/'. $categoryslug. '/' .$product->slug)}}" class=" yith-wcqv-button" >Quick Viewdd</a>
          @endif
          @if(isset($parentcat) && isset($categoryslug))
          <a href="{{url('/'.$parentcat. '/' .$categoryslug. '/' .$product->slug)}}" class=" yith-wcqv-button" >Quick View</a>
          @endif
          </a> -->
              
          <span class="product-tooltip">
          </span>
        </div>
      </div>
    </div>
  </li>
@endforeach

<script>

function addToCart(obj){
    var url = "{{URL::to('/add-to-cart')}}"
    $.ajax({
       type: "post",
       url: url,
       data: $(obj).closest('form').serialize(),
       success: function(data)
       {
            if (data.status==0) {
                alert(data.message);
                $('.update_cart_count').html('('+data.cart_count+')');
                return;
            }
            $('#cart-summary').html(data);
            var cart_count = $('.cart_count').val();
            $('.update_cart_count').html('('+cart_count+')');
            $("#cart-summary-overlay").fadeIn();
            $("#cart-summary-overlay").fadeIn("slow");
            $("#cart-summary-overlay").fadeIn(2000);
            $("#cart-summary-overlay").css({"top": "-7px"});

       }
    });
}

</script>
