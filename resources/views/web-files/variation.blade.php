<style>
    div.product .images-wrapper.vertical-thumbs div.images .slider-navi {
    position: absolute;
    top: 58%;
    width: 81.3%;
    right: 0;
    z-index: 100;
    transform: translateY(-50%);
}
.owl-carousel .owl-wrapper-outer
{
        height: 386px;
}
</style>
                                <!-- <span class="onsale">Top Pick</span> -->
									  <div class="images">
                                    <div class="main-slider vertical-thumbs" data-owl="container" data-owl-slides="1" data-owl-type="with-thumbs" data-owl-transition="fade" data-magnific="container">
                                        @foreach($product_images as $images)
                                        <a href="{{URL::to('/product_images/'.$pid.'/'.$images->images)}}" itemprop="image" class="woocommerce-main-image" data-effect="mfp-move-from-top" title="Dresses4">
                                            <img id="prodimg" width="530" height="750" src="{{URL::to('/product_images/'.$pid.'/'.$images->attribute_value_id.'/'.$images->images)}}" class="attachment-shop_single size-shop_single wp-post-image" alt="" srcset="{{URL::to('/product_images/'.$pid.'/'.$images->attribute_value_id.'/'.$images->images)}} 530w, {{URL::to('/product_images/'.$pid.'/'.$images->attribute_value_id.'/'.$images->images)}} 213w" sizes="(max-width: 768px) 92vw, (max-width: 992px) 345px, (max-width: 1200px) 455px, 555px" />
                                            </a>
                                             @endforeach
                                    </div>
                                    <div class='slider-navi vertical-thumbs'><span class='prev'><i class='fa fa-arrow-circle-o-left' style="font-size: 36px;color: #000;"></i></span><span class='next'><i class="fa fa-arrow-circle-o-right" style="font-size: 36px;color: #000;"></i></span>
                                    </div>
                                    <div class="thumb-slider vertical-thumbs" data-owl-thumbs="container">

                                        @foreach($product_images as $images)
                                        <img width="85" height="130" src="{{URL::to('/product_images/'.$pid.'/'.$images->attribute_value_id.'/'.$images->images)}}" class="attachment-brideliness-single-product-thumb size-brideliness-single-product-thumb wp-post-image" alt="" /> 

                                        @endforeach 
                                            <img id="myImg1" src="{{URL::to('/web-asset/img/babyvideo.jpg')}}" alt="Trolltunga, Norway" width="300" height="200">
                                    </div>
                                </div>

                                <script type="text/template" id="tmpl-unavailable-variation-template">
    <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var ajax_var = {
        "url": "http:\/\/brideliness.themes.zone\/wp-admin\/admin-ajax.php",
        "nonce": "3fd77b7b12"
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "http:\/\/brideliness.themes.zone\/wp-json\/",
            "namespace": "contact-form-7\/v1"
        },
        "recaptcha": {
            "messages": {
                "empty": "Please verify that you are not a robot."
            }
        },
        "cached": "1"
    };
    var wc_add_to_cart_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "http:\/\/brideliness.themes.zone\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
    };
    var wc_single_product_params = {
        "i18n_required_rating_text": "Please select a rating",
        "review_rating_required": "yes",
        "flexslider": {
            "rtl": false,
            "animation": "slide",
            "smoothHeight": false,
            "directionNav": false,
            "controlNav": "thumbnails",
            "slideshow": false,
            "animationSpeed": 500,
            "animationLoop": false
        },
        "zoom_enabled": "",
        "photoswipe_enabled": "",
        "flexslider_enabled": ""
    };
    var woocommerce_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%"
    };
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments"
    };
    var yith_woocompare = {
        "ajaxurl": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "actionadd": "yith-woocompare-add-product",
        "actionremove": "yith-woocompare-remove-product",
        "actionview": "yith-woocompare-view-table",
        "actionreload": "yith-woocompare-reload-product",
        "added_label": "Added",
        "table_title": "Product Comparison",
        "auto_open": "yes",
        "loader": "http:\/\/brideliness.themes.zone\/wp-content\/plugins\/yith-woocommerce-compare\/assets\/images\/loader.gif",
        "button_text": "Add to Compare",
        "cookie_name": "yith_woocompare_list"
    };
    var yith_qv = {
        "ajaxurl": "\/wp-admin\/admin-ajax.php",
        "loader": "http:\/\/brideliness.themes.zone\/wp-content\/plugins\/yith-woocommerce-quick-view\/assets\/image\/qv-loader.gif",
        "is2_2": ""
    };
    var yith_wcwl_l10n = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "redirect_to_cart": "no",
        "multi_wishlist": "",
        "hide_add_button": "1",
        "is_user_logged_in": "",
        "ajax_loader_url": "http:\/\/brideliness.themes.zone\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif",
        "remove_from_wishlist_after_add_to_cart": "yes",
        "labels": {
            "cookie_disabled": "We are sorry, but this feature is available only if cookies are enabled on your browser.",
            "added_to_cart_message": "<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"
        },
        "actions": {
            "add_to_wishlist_action": "add_to_wishlist",
            "remove_from_wishlist_action": "remove_from_wishlist",
            "move_to_another_wishlist_action": "move_to_another_wishlsit",
            "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem"
        }
    };
    var megamenu = {
        "timeout": "300",
        "interval": "100"
    };
    var wysijaAJAX = {
        "action": "wysija_ajax",
        "controller": "subscribers",
        "ajaxurl": "http:\/\/brideliness.themes.zone\/wp-admin\/admin-ajax.php",
        "loadingTrans": "Loading...",
        "is_rtl": ""
    };
    var _wpUtilSettings = {
        "ajax": {
            "url": "\/wp-admin\/admin-ajax.php"
        }
    };
    var wc_add_to_cart_variation_params = {
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "i18n_no_matching_variations_text": "Sorry, no products matched your selection. Please choose a different combination.",
        "i18n_make_a_selection_text": "Please select some product options before adding this product to your cart.",
        "i18n_unavailable_text": "Sorry, this product is unavailable. Please choose a different combination."
    };
    /* ]]> */
</script>
<script type='text/javascript' src="{{URL::to('admin-assets/js/productdetail.min.js')}}"></script>
 <script>
   $( '#myModal').on('click', function(event) {
    $("#myModal").hide();
});
    // $("#myModal").hide();

</script>
<script type="text/javascript">
    // Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var img1 = document.getElementById('myImg1');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img1.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
</script>