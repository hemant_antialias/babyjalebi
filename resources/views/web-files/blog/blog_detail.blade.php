@extends('web.web_layout')
@section('content')
<div id="content">
  <div class="container">
    <div class="page-header cf">
      <h1 class="majortitle">{{$blog_detail->category}}
      </h1>
      
      <!-- /.filters -->
     
  <!-- /.pagetitle -->
  <div class="tags nav-row spaced-row hidden">
    <ul>
      <li>
        <a title="Show articles tagged pork belly" href="recipe/tagged/pork-belly.html">pork belly
        </a>
      </li>
    </ul>
  </div>
  <div class="article-list">
    <div class="article  lightboximages">
      <div class="container">
        <div class="article-image">
          <img src="{{URL::to('blog/'.$blog_detail->id.'/'.$blog_detail->image)}}" alt="Ruchira Hoon's Mediterranean Yogurt, Raisin and Sumac Roast Chicken">
        </div>
        <h2>{{$blog_detail->title}}
        </h2>
        <div class="user-content">
          <table width="100%">
            <tbody>
              <tr>
                <td>
                  <p>&nbsp;Article &amp; Pictures by : Ruchira Hoon &nbsp;
                  </p>
                  <p>Curated and published by the 
                    <a href="http://www.delhisecretsupperclub.com/youre-going-hear-roar-experience-ordering-off-lionfresh/" target="_blank">Delhi Secret Supper Club
                    </a>
                  </p>
                </td>
              </tr>
            </tbody>
          </table>
          <p>{!!html_entity_decode($blog_detail->description)!!}
          </p>
        </div>
        <div class="meta">
          <span class="iconmeta time">
            September 06, 2016

            <span class="author">by Abhishek Jain
              </span>
          </span>
        </div>
      </div>
      <!-- /.container -->
    </div>
    <!-- /.article -->
  </div>
  <div class="container pagination-row">
  </div>
</div>
</div>
@stop

