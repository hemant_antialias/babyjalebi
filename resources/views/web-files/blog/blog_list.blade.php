@extends('web.web_layout')
@section('content')
<style type="text/css">
  .blogh2{
    text-align: center;
  }
  .blogbeautify{
        width: 80%;
    margin-left: auto;
    margin-right: auto;
  }
  .imgbeautify{
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    text-align: left;
  }
  @media only screen and ( max-width: 600px ) {
    .blogbeautify{
      width: 100%;
    }
  }

</style>

<div class="container">
  <div class="article-list">
    <div class="article  lightboximages">
      
        @foreach($blogs as $blog)
        <div class="article-image blogbeautify">
          <a href="{{URL::to('/blog/'.$blog->category.'/'.$blog->slug)}}">
            <img src="{{URL::to('blog/'.$blog->id.'/'.$blog->image)}}" alt="Ruchira Hoon's Mediterranean Yogurt, Raisin and Sumac Roast Chicken">
          </a>
        </div>
        <h2 class="blogh2 blogbeautify">
           <a href="{{URL::to('/blog/'.$blog->category.'/'.$blog->slug)}}" title="">{{$blog->title}}</a>
          
        </h2>
        <div class="user-content blogbeautify">
          <p>{!!html_entity_decode($blog->excerpt)!!}
          </p>
        </div>
        <div class="continue-reading blogbeautify">
          <a href="{{URL::to('/blog/'.$blog->category.'/'.$blog->slug)}}" title="">View article »
          </a>
        </div>
        <div class="meta blogbeautify">
          <span class="iconmeta time">
            {{Carbon\Carbon::parse($blog->created_at)->diffForHumans() }}
            <span class="author">{{$blog->author_name}}
            </span>
          </span>
        </div> 
       <hr>
    @endforeach
    </div>
    <!-- /.article -->
  
  </div>

  {{$blogs->links()}}

   </div>
      <!-- /.container -->
  <div class="container pagination-row">
  </div>

@stop
