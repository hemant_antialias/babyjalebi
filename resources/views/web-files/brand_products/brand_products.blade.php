@extends('web.web_layout')

@section('content')
<?php
use App\ProductImages;
?>

<link rel="stylesheet" type="text/css" href="{{URL::to('css/brand_products1.css')}} ">

<style type="text/css">
   
.marginleftchangeindex {
    margin-left: 36.5% !important;
}
  .filtercolumn{
    border: 2px solid #d8920c;
    padding-top: 20px;
    padding-bottom: 20px;
    padding-left: 8px;

  }
  #panelhead{
    text-align: center;
    padding-top: 4px;
    background: #d8920c;
    color: white;
        height: 30px;
  }
  .filterbrand span{
    font-weight: bold;
  }
  #slider-container {
    width:350px;
    margin-left:30px;
}
 .collection-listing .product-block
 {
/*    float: left;*/
    width: 252px;
 }
.out-of-stock{
        z-index: 1 !important;
    background: black !important;
    color: #fff !important;
    display: block !important;
    /* border-bottom-right-radius: 15px !important; */
    position: absolute;
    z-index: 999;
    text-align: center;
    /* border-bottom-left-radius: 15px !important; */
    width: 52% !important;
    margin-left: 48% !important;
    margin-right: auto !important;
    margin-top: 5%;
    padding-top: 4px;
    padding-bottom: 4px !important;
    font-weight: 500;
    /* font-size: 11px !important; */
}
.soldout{
  font-size: 10px !important;
}
@media only screen and ( device-width: 360px ) and ( device-height:640px){
     .collection-listing .product-block {
    height: auto;
    width: 50% !important;
    padding-left: 1%;
    padding-right: 1%;
    min-width: 0;
}
  .cartmobiconiphone6 {
    margin-top: 11% !important;
  }
}
.leftqnty{
  font-size: 13px !important;
}
.paddingbot{
    padding-bottom: 13px !important;
}

@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px)  { /*IPAD STYLES GO HERE */
    
    .social-linkpad{ display: none !important; }
    
   .collection-listing .product-block {
       height: auto;
    width: 31.3% !important;
    padding-left: 1%;
    margin-left: 1%;
    margin-right: 1%;
    padding-right: 1%;
    min-width: 0;
       float: left !important;
}
.title{ font-size:13px !important;}
    html {
    -webkit-text-size-adjust: none;
    margin-right: -10%;
}
    
    }
</style>


<div class="bootstrap-iso">

    <div class="container" id="content">
        <div >
            <div class="col-md-9 col-md-offset-3 col-xs-12 col-sm-12">
                <h1 class="majortitle fontcorrect" style="text-align: center;">{{$brands->name}}
                </h1>
                <div class="user-content lightboximages">
                    <div style="text-align: center; padding-left: 30px;">
              <span style="color: #666666;">
              </span>
                    </div>
                    <div style="text-align: center; padding-left: 30px;">
                    {!!html_entity_decode($brands->description)!!}
                   
                    <br>
                    <div style="text-align: center;">
              <span style="color: #666666;">
              </span>
                    </div>
                    <div style="text-align: center;">
              <span style="color: #666666;">
              </span>
                    </div>
                    <div style="text-align: center;">
              <span style="color: #000000;">
              </span>
                    </div>
                </div>
               
                <!-- /.filters -->
                <!-- <div class="social-area">
                    <a href="#" class="tags" data-toggle-target=".tags.nav-row.social-row">Share
              <span class="state">+
              </span>
                    </a>
                </div> -->
            </div>
            <!-- /.page-header -->
        </div>
        <!-- /.container -->
       <!-- /.multi-tag-row -->
        <div class="">


        <!-- Fiters Section -->
        @include('web.filter')

        <div class="thumbholder col-md-9 col-xs-12 col-sm-12">
            <div class="collection-listing">
                <div class="product-list">
                    @foreach($products as $product)
                    <?php
                      $product_image = ProductImages::where('product_id', '=', $product->id)->select('images')->first();
                    ?>
                    <div data-product-id="5435085315" class="product-block">
                        <div class="block-inner">
                           <div class="block-inner">
                            @if($product->totalquantity <= 0)
                                    <!-- <input type="submit" class="brandoutofstock btn btn-primary out-of-stock" value="Out of stock" style="width: 77% ;margin-left:12% ;border: none !important;" > -->
                                    <div class=" out-of-stock" ><span class="soldout">SOLD OUT</span> </div>
                                    @endif
                                     @if($product->totalquantity <= 5 &&  $product->totalquantity > 0)
                                     <div class=" out-of-stock" style="background: #dcaa31 !important;"><span class="leftqnty">Only {{$product->totalquantity}} Left!</span> </div>
                                    <!-- <input type="submit" class="marginleftchangeindex paddingbot somemarginless btn btn-primary out-of-stock" style="background: #f08035 !important" value="Only {{$product->totalquantity}} Left!" > -->
                                    @endif
                              <div class="input-row cartbtn" >
                                @if($product->totalquantity > 0)
                                  <form>
                                     <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                                    @if(isset($product->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
                                    @endif
                                    <input type="submit" class="cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" style="background: #d8920c !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                                  </form>
                                  @else
                                    <!--  <button class="btn btn-primary myn" data-id="{{$product->id}}" style="background: red !important;border-color: red !important; white-space:normal; white-space: normal;  margin-top: -23px; ">Email When Available</button>  -->
                                     <button class="emailwhenavailbtncss btn btn-primary emailbtn myn email-when-available" data-id="{{$product->id}}" style="background: black; border-color:black; border-radius:0px; margin-bottom:-17%;">Notify Me &nbsp;
                                   <i class="fa fa-chevron-right" aria-hidden="true"></i></button> 

                                  @endif
                                </div>
                                @if(isset($product->product_images))

                                <a class="image-link" href="{{URL::to('/products/'.$product->slug)}}">
                                    <img src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" />
                                </a>
                                @endif
                                <!-- <a class="hover-info" href="{{URL::to('/products/'.$product->slug)}}"> -->
                                    <div class="inner">
                                        <div class="innerer somelineheight">
                                            <a href="{{URL::to('/products/'.$product->slug)}}" class="title meat_title">{{$product->product_title}}
                                            </a><br>
                                            <span class="price">Rs.</span>
                                            @if($product->compare_price != 'NULL')
                                            <span class="price" style="color:black;text-decoration:line-through;padding-right: 2%;">
                                              {{$product->compare_price}}
                                              
                                            </span> 
                                            <span class="price">
                                               {{$product->price}}
                                            </span>
                                            @else
                                             <span class="price">
                                              {{$product->price}}
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                               <!--  </a> -->
                            </div>
                        </div>
                    </div>
                     @endforeach
                </div>
            </div>
          </div>
        </div>
        <div class="container pagination-row">
        </div>
    </div>
    <!-- /#content -->
</div><!--bootstrap-iso ends here -->
</div>
@endsection