<?php
  use App\ProductImages;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="ltr" class="no-js multi-step windows chrome desktop page--no-banner page--logo-main page--show  card-fields">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, height=device-height, minimum-scale=1.0, user-scalable=0">
    <link rel="icon" type="image/x-icon" href="{{URL::to('web-asset/favicons/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <title>BabyJalebi - Checkout</title>

    
    <style type="text/css">
     .colordva {
    height: 30px;
    width: 30px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}

.find_location input{
    background: url(http://ec2-13-126-180-167.ap-south-1.compute.amazonaws.com/web-assets/images/location.png) no-repeat;
       border-bottom: 2px solid #02c9c6;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    padding: 5px 20px;
    text-align: left;
    background-size: 13px;
    background-position: left center;}
    .content {font-family: lato, sans-serif;}
      #section--billing-address__different{
        display: none;
      }
      .banner{ text-align: center; }
      .order-summary-toggle {
    background: #272424;}
    .order-summary-toggle {
    background: #000000 !important;
    border-top: 1px solid #969191;
    border-bottom: 1px solid #969191;
    padding: 1.25em 0;
    -webkit-flex-shrink: 0;
    -ms-flex-negative: 0;
    flex-shrink: 0;
    text-align: left;
    width: 100%;
}
.order-summary-toggle__icon {
    fill: #ffffff!important;}
    .order-summary-toggle__text {
    color: #ffffff!important;
    vertical-align: middle;
    transition: color 0.2s ease-in-out;
    display: none;
}
.order-summary-toggle__dropdown {
    vertical-align: middle;
    transition: fill 0.2s ease-in-out;
    fill: #ffffff !important;
}
.total-recap__final-price {
    font-size: 1.28571em;
    line-height: 1em;
    color: #ffffff !important;
}
.input-radio:checked {
    border: none;
    box-shadow: 0 0 0 10px #000000 inset !important;
}
.input-radio{
    border: none;
    box-shadow: 0 0 0 0 #7e8182 inset !important;
}
.btn{ background: #02c9c6 !important; }
.btn:hover {
    background: #1B6AAA !important;
    color: white !important;
}
.step__footer__previous-link{
      color: #ef9f1d !important;
}
.previous-link__icon {
    fill: #ef9f1d !important;}
    .acol{ color: #ef9f1d; }
    </style>
    

<!--[if lt IE 9]>
<link rel="stylesheet" media="all" href="//cdn.shopify.com/app/services/11271070/assets/85073731/checkout_stylesheet/v2-ltr-edge-83902ab997cc4efcbcba9496d7d037de-1825223211479188468/oldie" />
<![endif]-->

<!--[if gte IE 9]><!-->
  <link href="{{URL::to('web-assets/css/chekout.css')}}" rel="stylesheet" type="text/css" media="all" />

<!--<![endif]-->

  




    <meta data-browser="chrome" data-browser-major="55">
<meta data-body-font-family="Helvetica Neue" data-body-font-type="system">

  <script type="text/javascript">
    window.shopifyExperiments = {}
  </script>


  <script src="{{URL::to('web-assets/js/checkout.js')}}"></script>

    
    <script src="{{URL::to('web-assets/js/card_fields.js')}}" rel="stylesheet" type="text/css" media="all" />
    </script>

  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    
    <script src="//cdn.shopify.com/app/services/11271070/javascripts/countries/85073731/en/countries-0e29d8b0e25432114d0d383a4491a5bff68463ec-1482478828.js"></script>


  </head>
  <body>
    <div class="banner" data-header>
      <div class="wrap">
        
<a href="{{URL::to('/')}}" class="logo logo--left">
      <img src="{{URL::to('web-asset/img/mainlogo.png')}}" > </a>
</a>

      </div>
    </div>

    <button class="order-summary-toggle order-summary-toggle--show" data-drawer-toggle="[data-order-summary]">
  <div class="wrap">
    <div class="order-summary-toggle__inner">
      <div class="order-summary-toggle__icon-wrapper">
        <svg width="20" height="19" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__icon">
          <path d="M17.178 13.088H5.453c-.454 0-.91-.364-.91-.818L3.727 1.818H0V0h4.544c.455 0 .91.364.91.818l.09 1.272h13.45c.274 0 .547.09.73.364.18.182.27.454.18.727l-1.817 9.18c-.09.455-.455.728-.91.728zM6.27 11.27h10.09l1.454-7.362H5.634l.637 7.362zm.092 7.715c1.004 0 1.818-.813 1.818-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817zm9.18 0c1.004 0 1.817-.813 1.817-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817z"/>
        </svg>
      </div>
      <div class="order-summary-toggle__text order-summary-toggle__text--show">
        <span>Show order summary</span>
        <svg width="11" height="6" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000"><path d="M.504 1.813l4.358 3.845.496.438.496-.438 4.642-4.096L9.504.438 4.862 4.534h.992L1.496.69.504 1.812z" /></svg>
      </div>
      <div class="order-summary-toggle__text order-summary-toggle__text--hide">
        <span>Hide order summary</span>
        <svg width="11" height="7" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle__dropdown" fill="#000"><path d="M6.138.876L5.642.438l-.496.438L.504 4.972l.992 1.124L6.138 2l-.496.436 3.862 3.408.992-1.122L6.138.876z" /></svg>
      </div>
      <div class="order-summary-toggle__total-recap total-recap" data-order-summary-section="toggle-total-recap">
        <!-- <span class="total-recap__final-price" data-checkout-payment-due-target="201625">Rs. {{round($order->payble_amount, 2)}}</span> -->
      </div>
    </div>
  </div>
</button>


    <div class="content" data-content>
      <div class="wrap">
        <div class="sidebar" role="complementary">
          <div class="sidebar__header">
            
<a href="https://www.babyjalebi.com" class="logo logo--left">
    <img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="babyjalebi" style="width: 85%;" />
</a>

          </div>
          <div class="sidebar__content">
            <div class="order-summary order-summary--is-collapsed" data-order-summary>
  <h2 class="visually-hidden">Order summary</h2>

  <div class="order-summary__sections">
    <div class="order-summary__section order-summary__section--product-list">
  <div class="order-summary__section__content">
    <table class="product-table">
      <caption class="visually-hidden">Shopping cart</caption>
      <thead>
        <tr>
          <th scope="col"><span class="visually-hidden">Product image</span></th>
          <th scope="col"><span class="visually-hidden">Description</span></th>
          <th scope="col"><span class="visually-hidden">Quantity</span></th>
          <th scope="col"><span class="visually-hidden">Price</span></th>
        </tr>
      </thead>
      <tbody data-order-summary-section="line-items">
      @foreach($order_detail as $detail)
      <?php
        $product_image = ProductImages::where('product_id', '=', $detail->product_id)->select('images')->first(); 
        $varaitionimage1 = DB::table('attribute_value')
                          ->join('product_variations','product_variations.attribute_value_id','=','attribute_value.id','left')
                          ->select('attribute_value.*','product_variations.id as varid')
                          ->where('product_variations.product_id' , '=' ,$detail->product_id)
                          ->where('attribute_value.value' , '=' ,$detail->pattern_name)
                          ->first();

            // echo $detail->product_id;

        if($varaitionimage1)
                  {
        $varaitionimage = DB::table('product_variation_images')
                          ->select('product_variation_images.*')
                          ->where('variation_id' , '=' ,$varaitionimage1->varid)->first();
                          ?>
        <tr class="product">
          <td class="product__image">
            <div class="product-thumbnail">
              <div class="product-thumbnail__wrapper">
              @if(isset($varaitionimage->images))
                <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/'.$varaitionimage1->id.'/'.$varaitionimage->images)}}" />
             
                  @else
                  
                   <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" />
             <!--    <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" /> -->
              @endif
              </div>
              <span class="product-thumbnail__quantity" aria-hidden="true">{{$detail->quantity}}</span>
            </div>
          </td>
          <?php
          }
        else{
          ?>
          <td class="product__image">
            <div class="product-thumbnail">
              <div class="product-thumbnail__wrapper">
              
                  
                   <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" />
            
              </div>
              <span class="product-thumbnail__quantity" aria-hidden="true">{{$detail->quantity}}</span>
            </div>
          </td>
          <?php
        }                 
      ?>
         <td class="product__description">
            <span class="product__description__name order-summary__emphasis">{{$detail->product_title}}</span>
            <span class="product__description__variant order-summary__small-text"></span>
             @if($detail->pattern_name)
                  @if ($detail->pattern_name == 'No Swatch')
                    @else
                  Pattern:{{$detail->pattern_name}}
                  @endif
            
                  @endif
                  <br>
                   @if($detail->text)
                  Text:{{$detail->text}}
                  <br>
                  @endif
                  @if($detail->font)
                  Font:{{$detail->font}}
                  <br>
                  @endif
                   @if($detail->color)
                  Color:{{$detail->color}} <div class="colordva" style="background-color:{{$detail->color}};"> </div>
                  <br>
                  @endif
                   @if($detail->pattern_color)
                  Pattern color:{{$detail->pattern_color}}
                  @endif
          </td>
          <td class="product__quantity visually-hidden">
            {{$detail->quantity}}
          </td>
          <td class="product__price">
            <?php $qty =  $detail->quantity;?>
            <span class="order-summary__emphasis">Rs. {{$detail->price*$qty}}</span>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>

    <div class="order-summary__scroll-indicator">
      Scroll for more items
      <svg xmlns="http://www.w3.org/2000/svg" width="10" height="12" viewBox="0 0 10 12"><path d="M9.817 7.624l-4.375 4.2c-.245.235-.64.235-.884 0l-4.375-4.2c-.244-.234-.244-.614 0-.848.245-.235.64-.235.884 0L4.375 9.95V.6c0-.332.28-.6.625-.6s.625.268.625.6v9.35l3.308-3.174c.122-.117.282-.176.442-.176.16 0 .32.06.442.176.244.234.244.614 0 .848"/></svg>
    </div>
  </div>
</div>


      <div class="order-summary__section order-summary__section--discount" data-reduction-form="update">
        <form class="edit_checkout" action="/11271070/checkouts/16d807336e0758feda31e0bd838b93c8" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="t41xq+59l/yce+pFcxFrMOusV1PtvUrWkSZF9VH44dGhJ0WNODhJ7s2u1laB8gcDgpKx6kazvMbGUBkcEb3UZQ==" />
  <input type="hidden" name="step" value="payment_method" />

  <!-- <div class="fieldset">
    <div class="field ">
      <label class="field__label" for="checkout_reduction_code">Discount</label>
      <div class="field__input-btn-wrapper">
        <div class="field__input-wrapper">
          <input placeholder="Discount" class="field__input" data-discount-field="true" autocomplete="off" size="30" type="text" name="checkout[reduction_code]" id="checkout_reduction_code" />
        </div>

        <button type="submit" class="field__input-btn btn btn--default btn--disabled">
          <span class="btn__content visually-hidden-on-mobile">Apply</span>
          <i class="btn__content shown-on-mobile icon icon--arrow"></i>
          <i class="btn__spinner icon icon--button-spinner"></i>
        </button>
      </div>

    </div>
  </div> -->
</form>
<form class="edit_checkout" action="/11271070/checkouts/16d807336e0758feda31e0bd838b93c8" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="g30k3MyrpAQawaPVirLvIOoZYdnhgY7XmwtfFkwiqUyV1xD6Gu56FksUn8Z4UYMTgyeHYEqPeMfMfQP/DGec+A==" />
  <input type="hidden" name="step" value="payment_method" />

</form>
      </div>

    <div class="order-summary__section order-summary__section--total-lines" data-order-summary-section="payment-lines">
  <table class="total-line-table">
    <caption class="visually-hidden">Cost summary</caption>
    <thead>
      <tr>
        <th scope="col"><span class="visually-hidden">Description</span></th>
        <th scope="col"><span class="visually-hidden">Price</span></th>
      </tr>
    </thead>
      <tbody class="total-line-table__tbody">
        <tr class="total-line total-line--subtotal">
          <td class="total-line__name">Subtotal</td>
          <td class="total-line__price">
            <span class="order-summary__emphasis" data-checkout-subtotal-price-target="189500">
              Rs. {{$order->amount}}
            </span>
          </td>
        </tr>

          @if(isset($order->discount) && $order->discount != 0 && $order->coupon_code && $order->coupon_code != "")
            <tr class="total-line total-line--shipping">
              <td class="total-line__name">Discount ( {{$order->coupon_code}} )</td>
              <td class="total-line__price">
                <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                  (-) Rs.{{$order->discount}}
                </span>
              </td>
            </tr>
            @endif

            @if(isset($order->tax_rate) && $order->tax_rate != 0)
            <tr class="total-line total-line--shipping">
              <td class="total-line__name">Tax </td>
              <td class="total-line__price">
                <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                  (+) Rs.{{round($order->tax_rate,2)}}
                </span>
              </td>
            </tr>
            @endif           

          <tr class="total-line total-line--shipping">
            <td class="total-line__name">Shipping</td>
            <td class="total-line__price">
              <span class="order-summary__emphasis" data-checkout-total-shipping-target="0">
                @if(isset($order->custom_rate_value))
                  (+) Rs.{{$order->custom_rate_value}}
                @else
                Free
                @endif
              </span>
            </td>
          </tr>

<!--           <tr class="total-line total-line--taxes " data-checkout-taxes>
            <td class="total-line__name">Taxes</td>
            <td class="total-line__price">
              <span class="order-summary__emphasis" data-checkout-total-taxes-target="12125">Rs. 121.25</span>
            </td>
          </tr> -->

      </tbody>
    <tfoot class="total-line-table__footer">
      <tr class="total-line">
        <td class="total-line__name payment-due-label">
          <span class="payment-due-label__total">Total</span>
        </td>
        <td class="total-line__price payment-due">
          <!-- <span class="payment-due__currency">INR</span> -->
          <span class="payment-due__price">
            Rs. {{round($order->payble_amount, 2)}}
          </span>
        </td>
      </tr>
    </tfoot>
  </table>
</div>

  </div>
</div>

          </div>
        </div>
        <div class="main" role="main">
          <div class="main__header">
            
<a href="https://www.babyjalebi.com" class="logo logo--left">
    <h1 class="logo__text">BabyJalebi</h1>
</a>

            
          </div>
          <div class="main__content">
            <div class="step" data-step="payment_method">
      <div class="step__sections">
      <div class="section section--reductions hidden-on-desktop">
  <div class="section__header">
    <h2 class="section__title">Discount</h2>
  </div>

  <div class="section__content">
      <div data-reduction-form>
        <form class="edit_checkout" action="/11271070/checkouts/16d807336e0758feda31e0bd838b93c8" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="UmtZNxxoaqkl8J5aGpO1y9kSajV1v4KA5c48CnEivAJEwW0Ryi20u3QloknocNn4sCyMjN6xdJCyuGDjMWeJtg==" />
  <input type="hidden" name="step" value="payment_method" />

  
</form>
<form class="edit_checkout" action="/11271070/checkouts/16d807336e0758feda31e0bd838b93c8" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="3mf/5B4rAyuQyJvSLdDBFzUOHornSw2K0q0gt3hszVLIzcvCyG7dOcEdp8HfM60kXDD4M0xF+5qF23xeOCn45g==" />
  <input type="hidden" name="step" value="payment_method" />

</form>
      </div>

  </div>
</div>

    </div>
<img src="https://loading.io/spinners/whirl/index.rotate-whirl-ajax-vortex-spinner.svg" id="gif" style="display: block; margin: 0 auto; width: 100px; visibility: hidden; position: absolute; left: 50%; top: 50%; transform: translate(-50%); -webkit-transform: translate(-50%); -moz-transform: translate(-50%); z-index: 99;">

<form method="post" id="formgif" action="{{URL::to('/checkout/order')}}">
    <input type="hidden" name="ordercode" value="{{$order->order_code}}" />

      <div class="step__sections">
          <div class="section section--payment-method" data-payment-method>
  <div class="section__header">
    <h2 class="section__title">Payment method</h2>
    <br>
    <div class="content-box__row">
        <div class="radio-wrapper" data-shipping-method="shopify-Free%20Shipping-0.00">
          <div class="radio__input">
            <input class="input-radio" type="radio" value="netbanking" checked="checked" name="payment_method" id="netbankinglionfresh" />
          </div>
          <label class="radio__label" for="netbankinglionfresh">
            <span class="radio__label__primary" data-shipping-method-label-title="Free Shipping">
              Pay with CCAvenue (Credit card, Debit card, Net Banking)
            </span>        
          </label>        
        </div> <!-- /radio-wrapper-->
    </div>
      <!-- <div class="content-box__row">
        <div class="radio-wrapper" data-shipping-method="shopify-Free%20Shipping-0.00">
          <div class="radio__input">
            <input class="input-radio" type="radio" value="paytm" checked="checked" name="payment_method" />
          </div>
          <label class="radio__label" for="checkout_shipping_rate_id_shopify-free20shipping-000">
            <span class="radio__label__primary" data-shipping-method-label-title="Free Shipping">
              PayTM
            </span>
           
</label>        </div> <!-- /radio-wrapper-->
      <!-- </div>  -->
      <!-- <div class="content-box__row">
        <div class="radio-wrapper" data-shipping-method="shopify-Free%20Shipping-0.00">
          <div class="radio__input">
            <input class="input-radio" type="radio" value="freecharge" checked="checked" name="payment_method" id="checkout_shipping_rate_id_shopify-free20shipping-000" />
          </div>
          <label class="radio__label" for="checkout_shipping_rate_id_shopify-free20shipping-000">
            <span class="radio__label__primary" data-shipping-method-label-title="Free Shipping">
              FreeCharge
            </span>
            
</label>        </div> <!-- /radio-wrapper-->
      <!-- </div> -->
      <!-- <div class="content-box__row">
        <div class="radio-wrapper" data-shipping-method="shopify-Free%20Shipping-0.00">
          <div class="radio__input">
            <input class="input-radio" type="radio" value="paytm" checked="checked" name="payment_method" id="checkout_shipping_rate_id_shopify-free20shipping-000" />
          </div>
          <label class="radio__label" for="checkout_shipping_rate_id_shopify-free20shipping-000">
            <span class="radio__label__primary" data-shipping-method-label-title="Free Shipping">
              Pay with Paytm
            </span>
            
</label>        </div> 
      </div> -->

      <!-- <div class="content-box__row">
        <div class="radio-wrapper" data-shipping-method="shopify-Free%20Shipping-0.00">
          <div class="radio__input">
            <input class="input-radio" type="radio" value="freecharge" checked="checked" name="payment_method" id="checkout_shipping_rate_id_shopify-free201shipping-000" />
          </div>
          <label class="radio__label" for="checkout_shipping_rate_id_shopify-free201shipping-000">
            <span class="radio__label__primary" data-shipping-method-label-title="Free Shipping">
              Pay with FreeCharge
            </span>
            
</label>        </div> 
      </div> -->
  

      <div class="content-box__row " >
        <div class="radio-wrapper codbuttom" data-shipping-method="shopify-Free%20Shipping-0.00" style="display: none">
          <div class="radio__input">
            <input class="input-radio" type="radio" value="cod" name="payment_method" id="checkout_shipping_rate_id_shopify-free205shipping-000" />
          </div>
          <label class="radio__label" for="checkout_shipping_rate_id_shopify-free205shipping-000">
            <span class="radio__label__primary" data-shipping-method-label-title="Free Shipping">
              COD
            </span>
            
</label>        </div> 
<br>
 <p> Please enter your pin code to check cod is availabe or not in your location</p>
      <br>
      <div class="find_location"><input type="type" name="" id="pincode"  placeholder="Type Your Pin Code"> <button type="submit" name="apply" class="checkpincode" style="background: #02c9c6 !important">CHECK</button></div><br><div class="discount-message" style="color: green;"></div><div class="discount-message1" style="color: red;"></div>
      </div>

     
  </div>
</div>

  <div class="section section--billing-address" data-billing-address>
    <div class="section__header">
      <h2 class="section__title">Billing address</h2>
    </div>

    <div class="section__content">
      <div class="content-box">
        <div class="radio-wrapper content-box__row">
          <div class="radio__input">
            <input class="input-radio" data-backup="different_billing_address_false" type="radio" value="1" checked="checked" name="bill_address" id="checkout_different_billing_address_false" />
          </div>

          <label class="radio__label content-box__emphasis" for="checkout_different_billing_address_false">
            Same as shipping address
</label>        </div>

        <div class="radio-wrapper content-box__row">
          <div class="radio__input">
            <input class="input-radio" data-backup="different_billing_address_true" aria-expanded="true" aria-controls="section--billing-address__different" type="radio" value="2" name="bill_address" id="checkout_different_billing_address_true" />
          </div>
          <label class="radio__label content-box__emphasis" for="checkout_different_billing_address_true">
            Use a different billing address
</label>        </div>

        <div class="radio-group__row content-box__row content-box__row--secondary" id="section--billing-address__different">
          <div class="fieldset" data-address-fields>
      

<div class="field--two-thirds field field--required" data-address-field="address1">
  <label class="field__label" for="checkout_billing_address_address1">Address</label>
  <div class="field__input-wrapper">
    <input placeholder="Address" autocomplete="billing address-line1" data-backup="address1" class="field__input" size="30" type="text" name="billing_address" value="{{ old('billing_address') }}" id="checkout_billing_address_address1" />
  </div>
</div>
@if ($errors->has('billing_address'))
    <span class="help-block">
        <strong>{{ $errors->first('billing_address') }}</strong>
    </span>
  @endif
  <div class="field--third field field--optional" data-address-field="address2">
    <label class="field__label" for="checkout_billing_address_address2">Apt, suite, etc. (optional)</label>
    <div class="field__input-wrapper">
      <input placeholder="Apt, suite, etc. (optional)" autocomplete="billing address-line2" data-backup="address2" class="field__input" size="30" type="text" name="apt_or_suit" id="checkout_billing_address_address2" />
    </div>
</div>

<div data-address-field="city" class="field field--required">
  <label class="field__label" for="checkout_billing_address_city">City</label>
  <div class="field__input-wrapper">
    <input placeholder="City" autocomplete="billing address-level2" data-backup="city" class="field__input" size="30" value="{{ old('city') }}" type="text" name="city" id="checkout_billing_address_city" />
  </div>
</div>
@if ($errors->has('city'))
    <span class="help-block">
        <strong>{{ $errors->first('city') }}</strong>
    </span>
  @endif

  <div data-address-field="province" class="field--three-eights field field--required">
    <label class="field__label" for="checkout_billing_address_province">State</label>
    <div class="field__input-wrapper field__input-wrapper--select">
      <input placeholder="State" data-backup="province" class="field__input" type="text" name="state" id="checkout_billing_address_province" />
    </div>
  </div>
  <div data-address-field="zip" class="field--quarter field field--required">
    <label class="field__label" for="checkout_billing_address_zip">Postal code</label>
    <div class="field__input-wrapper">
      <input placeholder="Zip Code" autocomplete="billing postal-code" data-backup="zip" value="{{ old('zip') }}" class="field__input field__input--zip" size="30" type="text" name="zip" id="checkout_billing_address_zip" />
    </div>
  </div>
  @if ($errors->has('zip'))
    <span class="help-block">
        <strong>{{ $errors->first('zip') }}</strong>
    </span>
  @endif
  <div data-address-field="zip" class="field--quarter field field--required">
    <label class="field__label" for="checkout_billing_address_zip">State</label>
    <div class="field__input-wrapper">
      <input placeholder="State" autocomplete="billing postal-code" data-backup="zip" class="field__input field__input--zip" size="30" type="text" name="state" id="checkout_billing_address_zip" />
    </div>
  </div>
  <div data-address-field="zip" class="field--quarter field field--required">
    <label class="field__label" for="checkout_billing_address_zip">Country</label>
    <div class="field__input-wrapper">
      <input placeholder="Country" autocomplete="billing postal-code" data-backup="zip" class="field__input field__input--zip" size="30" type="text" name="country" id="checkout_billing_address_zip" />
    </div>
  </div>
    <div data-address-field="phone" class="field field--required">
      <label class="field__label" for="checkout_billing_address_phone">Phone</label>
      <div class="field__input-wrapper">
        <input placeholder="Phone" autocomplete="billing tel" data-backup="phone" value="{{ old('phone') }}" data-phone-formatter="true" class="field__input" size="30" type="tel" name="phone" id="checkout_billing_address_phone" />
      </div>
  </div>
  @if ($errors->has('phone'))
    <span class="help-block">
        <strong>{{ $errors->first('phone') }}</strong>
    </span>
  @endif
      </div>
    </div>
  </div>
</div>
</div>
<!-- 



          <div class="section section--optional">
            <div class="section__content">
              <div class="checkbox-wrapper">
                <div class="checkbox__input">
                  <input name="checkout[buyer_accepts_marketing]" type="hidden" value="0" /><input class="input-checkbox" data-backup="buyer_accepts_marketing" type="checkbox" value="1" checked="checked" name="checkout[buyer_accepts_marketing]" id="checkout_buyer_accepts_marketing" />
                </div>
                <label class="checkbox__label" for="checkout_buyer_accepts_marketing">
                  Subscribe to our newsletter
</label>              </div>
            </div>
          </div> -->
      </div>

      

<div class="step__footer">
    <input type="hidden" name="checkout[total_price]" id="checkout_total_price" value="{{$order->payble_amount}}" />
    <input type="hidden" name="complete" value="1" />
    <button name="button" type="submit" id="compltbtn" class="step__footer__continue-btn btn ">
  <span class="btn__content">Complete order</span>
  <i class="btn__spinner icon icon--button-spinner"></i>
</button>

  <a class="step__footer__previous-link" href="{{URL::to('/checkout/customer-info/'.$order->order_code)}}">
<svg class="previous-link__icon icon--chevron icon" xmlns="http://www.w3.org/2000/svg" width="6.7" height="11.3" viewBox="0 0 6.7 11.3">
    <path d="M6.7 1.1l-1-1.1-4.6 4.6-1.1 1.1 1.1 1 4.6 4.6 1-1-4.6-4.6z" />
</svg>
Return to customer info</a>
</div>

</form>
</div>


          </div>
          <div class="main__footer">
            <div class="modals">
    <div class="modal-backdrop" role="dialog" id="policy-18374787" aria-labelledby="policy-18374787-title" data-modal-backdrop>
  <div class="modal">
    <div class="modal__header">
      <h1 class="modal__header__title" id="policy-18374787-title">
        Privacy policy
      </h1>
      <div class="modal__close">
        <button type="button" class="icon icon--close-modal" data-modal-close>
          <span class="visually-hidden">
            Close
          </span>
        </button>
      </div>
    </div>
    <div class="modal__content">
      <svg class="modal__loading-icon icon icon--spinner" width="32" height="32" xmlns="http://www.w3.org/2000/svg"><path d="M32 16c0 8.837-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0v2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14h2z" /></svg>

    </div>
  </div>
</div>

    <div class="modal-backdrop" role="dialog" id="policy-18374723" aria-labelledby="policy-18374723-title" data-modal-backdrop>
  <div class="modal">
    <div class="modal__header">
      <h1 class="modal__header__title" id="policy-18374723-title">
        Refund policy
      </h1>
      <div class="modal__close">
        <button type="button" class="icon icon--close-modal" data-modal-close>
          <span class="visually-hidden">
            Close
          </span>
        </button>
      </div>
    </div>
    <div class="modal__content">
      <svg class="modal__loading-icon icon icon--spinner" width="32" height="32" xmlns="http://www.w3.org/2000/svg"><path d="M32 16c0 8.837-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0v2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14h2z" /></svg>

    </div>
  </div>
</div>

    <div class="modal-backdrop" role="dialog" id="policy-18374851" aria-labelledby="policy-18374851-title" data-modal-backdrop>
  <div class="modal">
    <div class="modal__header">
      <h1 class="modal__header__title" id="policy-18374851-title">
        Terms of service
      </h1>
      <div class="modal__close">
        <button type="button" class="icon icon--close-modal" data-modal-close>
          <span class="visually-hidden">
            Close
          </span>
        </button>
      </div>
    </div>
    <div class="modal__content">
      <svg class="modal__loading-icon icon icon--spinner" width="32" height="32" xmlns="http://www.w3.org/2000/svg"><path d="M32 16c0 8.837-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0v2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14h2z" /></svg>

    </div>
  </div>
</div>

</div>


<!-- <div role="contentinfo" aria-label="Footer">
    <ul class="policy-list">
        <li class="policy-list__item">
          <a title="Refund policy" data-modal="policy-18374723" data-close-text="Close" href="https://checkout.shopify.com/11271070/policies/18374723.html" class="acol">Refund policy</a>
        </li>
        <li class="policy-list__item">
          <a title="Privacy policy" data-modal="policy-18374787" data-close-text="Close" href="https://checkout.shopify.com/11271070/policies/18374787.html" class="acol">Privacy policy</a>
        </li>
        <li class="policy-list__item">
          <a title="Terms of service" data-modal="policy-18374851" data-close-text="Close" href="https://checkout.shopify.com/11271070/policies/18374851.html" class="acol">Terms of service</a>
        </li>
    </ul>
</div> -->

<div id="dialog-close-title" class="hidden">Close</div>

          </div>
        </div>
      </div>
    </div>
      <script type="text/javascript">
        $(document).ready(function(){
        $('.discount_price_div').show();
        $('.discount_code_div').show();

      });
            $(function() {
            $("input[type=radio]").on('click', function(){
                if ($('#checkout_different_billing_address_true').is(':checked')){
                    $("#section--billing-address__different").slideDown("slow");
                } else { 
                    $("#section--billing-address__different").slideUp("slow");
                }
            });
            });
      </script>
    
      <script type="text/javascript">
        
      window.shopifyAnalytics = window.shopifyAnalytics || {};
      window.shopifyAnalytics.meta = window.shopifyAnalytics.meta || {};
      window.shopifyAnalytics.meta.currency = 'INR';
      var meta = {};
      for (var attr in meta) {
        window.shopifyAnalytics.meta[attr] = meta[attr];
      }
    
      </script>

      <script type="text/javascript">
        window.shopifyAnalytics.merchantGoogleAnalytics = function() {
          
        };
      </script>

      <script type="text/javascript" class="analytics">
        (window.gaDevIds=window.gaDevIds||[]).push('BwiEti');
        

        (function () {
          var customDocumentWrite = function(content) {
            var jquery = null;

            if (window.jQuery) {
              jquery = window.jQuery;
            } else if (window.Checkout && window.Checkout.$) {
              jquery = window.Checkout.$;
            }

            if (jquery) {
              jquery('body').append(content);
            }
          };

          var trekkie = window.shopifyAnalytics.lib = window.trekkie = window.trekkie || [];
          if (trekkie.integrations) {
            return;
          }
          trekkie.methods = [
            'identify',
            'page',
            'ready',
            'track',
            'trackForm',
            'trackLink'
          ];
          trekkie.factory = function(method) {
            return function() {
              var args = Array.prototype.slice.call(arguments);
              args.unshift(method);
              trekkie.push(args);
              return trekkie;
            };
          };
          for (var i = 0; i < trekkie.methods.length; i++) {
            var key = trekkie.methods[i];
            trekkie[key] = trekkie.factory(key);
          }
          trekkie.load = function(config) {
            trekkie.config = config;
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.onerror = function(e) {
              (new Image()).src = '//v.shopify.com/internal_errors/track?error=trekkie_load';
            };
            script.async = true;
            script.src = 'https://cdn.shopify.com/s/javascripts/tricorder/trekkie.storefront.min.js?v=2016.12.06.1';
            var first = document.getElementsByTagName('script')[0];
            first.parentNode.insertBefore(script, first);
          };
          trekkie.load(
            {"Trekkie":{"appName":"storefront","environment":"production","defaultAttributes":{"shopId":11271070}},"Performance":{"navigationTimingApiMeasurementsEnabled":true,"navigationTimingApiMeasurementsSampleRate":0.001},"Google Analytics":{"trackingId":"UA-75364171-1","domain":"auto","siteSpeedSampleRate":"10","enhancedEcommerce":true,"doubleClick":true,"includeSearch":true},"Facebook Pixel":{"pixelIds":["701339193301932"],"agent":"plshopify1.2"}}
          );

          var loaded = false;
          trekkie.ready(function() {
            if (loaded) return;
            loaded = true;

            window.shopifyAnalytics.lib = window.trekkie;
            
      ga('require', 'linker');
      function addListener(element, type, callback) {
        if (element.addEventListener) {
          element.addEventListener(type, callback);
        }
        else if (element.attachEvent) {
          element.attachEvent('on' + type, callback);
        }
      }
      function decorate(event) {
        event = event || window.event;
        var target = event.target || event.srcElement;
        if (target && (target.getAttribute('action') || target.getAttribute('href'))) {
          ga(function (tracker) {
            var linkerParam = tracker.get('linkerParam');
            document.cookie = '_shopify_ga=' + linkerParam + '; ' + 'path=/';
          });
        }
      }
      addListener(window, 'load', function(){
        for (var i=0; i < document.forms.length; i++) {
          var action = document.forms[i].getAttribute('action');
          if(action && action.indexOf('/cart') >= 0) {
            addListener(document.forms[i], 'submit', decorate);
          }
        }
        for (var i=0; i < document.links.length; i++) {
          var href = document.links[i].getAttribute('href');
          if(href && href.indexOf('/checkout') >= 0) {
            addListener(document.links[i], 'click', decorate);
          }
        }
      });
    

            var originalDocumentWrite = document.write;
            document.write = customDocumentWrite;
            try { window.shopifyAnalytics.merchantGoogleAnalytics.call(this); } catch(error) {};
            document.write = originalDocumentWrite;

            
        window.shopifyAnalytics.lib.page(
          "Checkout - Payment",
          {"path":"\/checkout\/payment","search":"","url":"https:\/\/checkout.shopify.com\/11271070\/checkouts\/16d807336e0758feda31e0bd838b93c8?step=payment_method"}
        );
      
            
          });

          
      var eventsListenerScript = document.createElement('script');
      eventsListenerScript.async = true;
      eventsListenerScript.src = "//cdn.shopify.com/s/assets/shop_events_listener-37861b7e433c159ab7ac8932b47d1b8813173005dff35ce87a5900b43e3492ed.js";
      document.getElementsByTagName('head')[0].appendChild(eventsListenerScript);
    
        })();
      </script>
    <script>
(function (global) {

  if(typeof (global) === "undefined")
  {
    throw new Error("window is undefined");
  }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

    // making sure we have the fruit available for juice....
    // 50 milliseconds for just once do not cost much (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };
  
  // Earlier we had setInerval here....
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        
    noBackPlease();

    // disables backspace on page except on input fields and textarea..
    document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };
    
    };

})(window);
</script>
<script>
      $('.checkpincode').click(function(){
        event.preventDefault();
        var base_url = "{{URL::to('/')}}";
        var pincode = $("#pincode").val();
        // alert(pincode);


        $.ajax({
          url: base_url+'/check-pincode',
          type: "post",
          data: 
          {
            "pincode": pincode
          },
          success:function(data)
          {
            if(data.status==0){
              $('.discount-message').html(data.message);
              $('.codbuttom').css("display", "block");
            }
            if(data.status==1){
              $('.discount-message').html(data.message);
              $('.codbuttom').css("display", "none");
              $('.discount-message').css("display", "red");
              
            }
          }
        });
      });
    </script>
    <script>
        
        $('#formgif').submit(function() {
    // $('#gif').css('visibility', 'visible');
    $("#compltbtn").prop("disabled",true);
});
      </script>
  </body>
</html>
