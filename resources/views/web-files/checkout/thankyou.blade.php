@extends('web-files.web_layout')

@section('content')
<?php
  use App\ProductImages;
?>

<!-- Google Code for Order Placed Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 884057952;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "UUByCI2NznAQ4M7GpQM";
var google_conversion_value = <?php echo round($order->payble_amount, 2) ?>;
var google_conversion_currency = "INR";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/884057952/?value=<?php echo round($order->payble_amount, 2) ?>&amp;currency_code=INR&amp;label=UUByCI2NznAQ4M7GpQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<style>
  .layout-content-with-image-left .image-cont, .layout-content-with-image-right .image-cont {
    display: inline-block;
    zoom: 1;
    width: 49%;
    text-align: center;
    vertical-align: middle;
}
.layout-content-with-image-left .content, .layout-content-with-image-right .content {
    display: inline-block;
    zoom: 1;
    width: 49%;
    float: right;
    vertical-align: middle;
}
.image-cont {     float: left;
                   width: 100% !important; 
  }

.thankorderdetails{
      width: 41%;
    float: left;
}
.thankyouspecial4{
      border: 2px dashed gainsboro;
    padding: 10px;
    margin-left: 5%;
}
.thankyouspecial5{
  border-top: 1px dashed gainsboro;
    padding-top: 15px;
}
.thankcontainer {margin-bottom: 50px; margin-top: 50px;}
.thankyouSpecial {margin-bottom: 20px;}
/*.thankyouspecialcase7{
  width: 270px ;
}*/
.sub_amount {float: right; font-size: 14px; width: 250px;}
@media only screen and (max-width: 600px){
  .sub_amount {width: 162px;}
  .layout-content-with-image-left .content, .layout-content-with-image-right .content {margin-bottom: 30px !important;}
  }
@media only screen and (max-width: 480px){
/*.thankcontainer{ display: table !important; width: 100% ;}
.thankorderdetails{ display:table-footer-group !important; width: 100% !important; }
.image-cont{ display: table-header-group !important;width: 100% !important; }
.content { display: table-row-group !important;width: 100% !important; }*/
.thankcontainer{ display: flex !important; flex-direction: column !important; }
.thankcontainer > div { width: 100% ; }

 .image-cont{  order: 1 !important; text-align: center !important; margin-right: 0px !important; }
 .content img{ width: 50px !important ; padding-right: 0 !important; }
.content{ order: 2 !important; }
.content hr { width: 200%;}
.thankorderdetails{ order: 3 !important; }
.thankorderdetails h6 { font-size: 12px !important;  }
.finalamounmtfontsize { font-size: 12px !important;
    width: 100%; font-weight: bold; }
.mrgin_rgt_ty_page{ margin-right: -125px; }


</style>
<script type="text/javascript">
  
$('.content').insertBefore('.thankorderdetails');

</script>
<div class="thankcontainer container padded-row layout-content-with-image-right cf">
<div class="image-cont" style="text-align: left;">
   
</div> <!--image-cont ends here -->
<div class="thankorderdetails">
     <img src="{{URL::to('/web-assets/images/thankyou.png')}}" alt="High Quality Meats" style="width: 120px;">
    <h3 style="color: #d8920c;">Thank you {{$name}}</h3>
     <h6>For ordering with us</h6>
     
    <h6>Order ID #{{$oredrid}}</h6>
   
    <br>
    <h6><strong> DELIVERY ADDRESS</strong></h6>

    <p>{{$name}} {{$last_name}}<br>
    {{$order->address}}, {{$order->city}},{{$order->state}}, {{$order->zip}}<br>
    {{$phone}}</p>
<br>
    <h6><strong>BILLING ADDRESS</strong></h6>
    <p>{{$name}} {{$last_name}}<br>
    {{$order->baddress}}, {{$order->bcity}},{{$order->bstate}}, {{$order->bzip}}<br>
    {{$phone}}</p>
  </div>
  <div class="content user-content thankyouspecial4 thankyouspecial6">

 
  @foreach($order_detail as $detail)
    <?php
      $product_image = ProductImages::where('product_id', '=', $detail->product_id)->select('images')->first();
       $varaitionimage1 = DB::table('attribute_value')
                          ->join('product_variations','product_variations.attribute_value_id','=','attribute_value.id','left')
                          ->select('attribute_value.*','product_variations.id as varid')
                          ->where('product_variations.product_id' , '=' ,$detail->product_id)
                          ->where('attribute_value.value' , '=' ,$detail->pattern_name)
                          ->first();

            // echo $detail->product_id;
if($varaitionimage1)
                  {
        $varaitionimage = DB::table('product_variation_images')
                          ->select('product_variation_images.*')
                          ->where('variation_id' , '=' ,$varaitionimage1->varid)->first();
    
    ?>

  
  <div class="thankyouSpecial" style="display: -webkit-inline-box;">
  <div>
    @if(isset($varaitionimage->images))
      <img src="{{URL::to('/product_images/'.$detail->product_id.'/'.$varaitionimage1->id.'/'.$varaitionimage->images)}}" alt="{{$detail->product_title}}" style="width: 70px;padding-right: 35%;">
    @else
      <img src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" alt="{{$detail->product_title}}" style="width: 70px;padding-right: 35%;">
      <!-- <img src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="{{$detail->product_title}}" style="width: 70px;padding-right: 35%;"> -->
    @endif
</div>

<?php
}
else
{

  ?>
  <div class="thankyouSpecial" style="display: -webkit-inline-box;">
  
  <div>
<img src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" alt="{{$detail->product_title}}" style="width: 70px;padding-right: 35%;">
</div>
<?php
}
?>
     <?php
$oredrid;
$rev = round($order->payble_amount, 2);
$tax = round($order->tax_rate, 2);
$ship = $order->custom_rate_value;
$qty1 = $detail->quantity;
?>
<script type="text/javascript">
ga('require', 'ecommerce');
ga('ecommerce:addTransaction', {
  'id': '<?php echo $oredrid;?>',                     // Transaction ID. Required.
  'affiliation': 'Store Name',   // Affiliation or store name.
  'revenue': '<?php echo $rev?>',               // Grand Total.
  'shipping': '<?php echo $ship?>',                  // Shipping.
  'tax': '<?php echo $tax?>'                     // Tax.
});
 
ga('ecommerce:addItem', {
  'id': '<?php echo $oredrid;?>',                     // Transaction ID. Required.
  'name': '<?php echo $detail->product_title ?>',    // Product name. Required.
  'sku': 'DD23444',                 // SKU/code.
  'category': 'Party Toys',         // Category or variation.
  'price': '<?php echo $detail->price*$qty1 ?>',                 // Unit price.
  'quantity': '<?php echo $qty1 ?>'                   // Quantity.
});

ga('ecommerce:send');
</script>
    <div class="thankyouspecialcase7" >
      <p class="thankyoucartp">{{$detail->product_title}}({{$detail->quantity}})</p>

       @if($detail->pattern_name)
       @if ($detail->pattern_name == 'No Swatch')
       @else
                  Pattern:{{$detail->pattern_name}}
                  @endif
            
                  @endif
                  <br>
                   @if($detail->text)
                  Text:{{$detail->text}}
                  <br>
                  @endif
                  @if($detail->font)
                  Font:{{$detail->font}}
                  <br>
                  @endif
                   @if($detail->color)
                  Color:<div class="colordva" style="background-color:{{$detail->color}};"> </div>
                  <br>
                  @endif
                   @if($detail->pattern_color)
                  Pattern color:{{$detail->pattern_color}}
                  @endif
                  <br>
    </div>

    </div>
    <?php $qty =  $detail->quantity;?>

    <div class="mrgin_rgt_ty_page thankyouspecial1" style="float: right;">
     <span >Price</span  >  Rs {{$detail->price*$qty}}
    </div>


   
  
  @endforeach
  @if(isset($order->notes) && $order->notes != '')
  <span>Notes: </span><br>  {{$order->notes}}
  @endif
  <div class="sub_amount">
  <span>Sub Total: </span> <b class="mrgin_rgt_ty_page thankyouspecial2" style="float: right;">Rs. {{$order->amount}}</b><br>
 
  @if(isset($order->discount) && $order->discount != 0)
  <span>Discount:({{$order->coupon_code}}) </span>    <span  style="float:right;"> (-) Rs.{{$order->discount}}</span>
  @endif
  <br>
   @if(isset($order->tax_rate) && $order->tax_rate != 0)

    <span>Tax </span>    <span class="thankyouspecial2" style="float:right;"> (+) Rs.{{round($order->tax_rate, 2)}}</span>
          
            @endif        
            <br>
  <!-- @if(isset($order->coupon_code) && $order->coupon_code != '')
  <span>Coupon: </span>  {{$order->coupon_code}}
  @endif
  <br> --> 
  @if(isset($order->custom_rate_value))
  <span>Shipping Rate: </span>  <span class="mrgin_rgt_ty_page thankyouspecial2" style="float:right;" >(+) Rs.{{$order->custom_rate_value}}</span>
   
   <div>
<h6 class="finalamounmtfontsize thankyouspecial3" style="margin-top: 10px;"><span> FINAL AMOUNT </span> <b class="mrgin_rgt_ty_page thankyouspecial2" style="float: right;"> Rs. {{round($order->payble_amount, 2)}} </b><br></h6>
  
  </div>
  @else
  <span>Shipping: </span>  Free
   
   <div>
<h6 class="thankyouspecial5 finalamounmtfontsize">FINAL AMOUNT Rs. {{round($order->payble_amount, 2)}} <br></h6>
  
  </div>
  @endif
 
  </div>
  <div style="float: right;">
    
  </div>
  </div>
  
</div>
@endsection