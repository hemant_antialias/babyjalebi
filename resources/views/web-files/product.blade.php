@extends('web-files.web_layout')
<?php

use App\ProductCategory;
use App\ProductCategories;
use App\Product;
use App\CategoryParent;

?>
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> @section('content')

<style type="text/css">
.woocommerce ul.products li.product .product-wrapper .product-img-wrapper, .woocommerce-page ul.products li.product .product-wrapper .product-img-wrapper
{
  max-height: 200px;
}
.breadcrumb
{
      margin-bottom: 5%;
}
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
    .squaredFour {
  width: 20px;
  position: relative;
  margin: 20px auto;
  label {
    width: 20px;
    height: 20px;
    cursor: pointer;
    position: absolute;
    top: 0;
    left: 0;
    background: #fcfff4;
    background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
    border-radius: 4px;
    box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
    &:after {
      content: '';
      width: 9px;
      height: 5px;
      position: absolute;
      top: 4px;
      left: 4px;
      border: 3px solid #333;
      border-top: none;
      border-right: none;
      background: transparent;
      opacity: 0;
      transform: rotate(-45deg);
    }
    &:hover::after {
      opacity: 0.5;
    }
  }
  input[type=checkbox] {
    visibility: hidden;
    &:checked + label:after {
      opacity: 1;
    }
  }    
}
</style>
<link rel='stylesheet' id='css-0-css' href="{{URL::to('admin-assets/css/productdetail.min.css')}}" type='text/css' media='all' />

<script type='text/javascript' src="{{URL::to('admin-assets/js/productdetail2.min.js')}}"></script>
</head>

<!--  Site Header (start)-->
<div class="loading" style="display:none">&nbsp;</div>
<div class="site-wrapper archive tax-product_cat term-brooches term-294 woocommerce woocommerce-page mega-menu-primary-nav layout-two-col-left single_type_1 wpb-js-composer js-comp-ver-5.1.1 vc_responsive">

    <!--  Site Header (end)-->
<!-- 
    <div class="page-title-description">
        <div class="overlay"></div>
        <div class="title-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                    </div>
                </div>
            </div>

        </div>
    </div> -->

    <!-- <div class="store-banner">
        <div class="overlay"></div>
        <div class="container" style=" text-align:center;">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <div class=" store-banner-inner">
                        <div class="banner-text ">
                            <p class="banner-description">make your choice</p>
                            <h3 class="banner-title">{{$categoryslug}} </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vertical-helper"></div>
        </div>
    </div> -->

    <!-- Content wrapper (start)-->
    <div id="main" class="site-main container">
        <div class="row">
@if($categoryslug == 'diaper-bags')
            <div class="container breadcrumb">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-sx-12">
                       <img src="{{URL('web-assets/images/diaper-bags-banner.jpg')}}">
                    </div>
                </div>
            </div>
    @endif



            <div class="container">
                <div class="row">

                    <div id="content" class="site-content woocommerce columns-3 site-content col-xs-12 col-md-12 col-sm-8 " role="main">


                        
                        <div class="product-list">
                         <ul class="products grid-layout" data-isotope="container" data-isotope-layout="fitrows" data-isotope-elements="product">
                         
                         
             @foreach($products as $product)

            <li class="annoushka balenciaga grid-view  col-xs-12 col-md-3 col-sm-6 post-96 product type-product status-publish has-post-thumbnail product_cat-couture product_cat-fall-2016 product_cat-spring-2017 product_tag-accessories product_tag-bridal product_tag-trend first instock sale featured downloadable taxable shipping-taxable purchasable product-type-simple">
              <div class="product-wrapper">
                <div class="product-img-wrapper">
                @if(isset($categoryslug) && !isset($subcategoryslug))
                  <a href="{{url('/'. strtolower($categoryslug). '/' .$product->slug)}}" title="{{$product->slug}}" class="woocommerce-LoopProduct-link">  
                  @else
                    <a href="{{url('/'.$categoryslug. '/' .$subcategoryslug. '/' .$product->slug)}}" title="{{$product->slug}}" class="woocommerce-LoopProduct-link">  
                    @endif
                    <div class="background-img-product">
                    </div>
                    <!-- <span class="onsale">Top Pick -->
                    </span>
                    <img width="265" height="365" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="{{$product->product_title}}" title="product11" />
                    
                    
                    <div class="clear">
                    </div>     
                  </a>  
                </div>
                <div class="product-description-wrapper">

                 @if(isset($categoryslug) && !isset($subcategoryslug))
                    </a>
                  <!-- <a class="product-title" href="{{url('/'. strtolower($categoryslug). '/' .$product->slug.'/'.$product->slug)}}" title="{{$product->slug}}"> -->
                  <a class="product-title" href="{{url('/'. strtolower($categoryslug). '/' .$product->slug)}}" title="{{$product->slug}}">

                    <h2 class="woocommerce-loop-product__title">{{$product->product_title}}
                    </h2>
                    </a>
                    @if ($categoryslug == 'gifts')
                    @else

                    <img src="{{url('/diaper_bag/1.png')}}"> 
                    <img src="{{url('/diaper_bag/2.png')}}">
                    <img src="{{url('/diaper_bag/3.png')}}">
                    <img src="{{url('/diaper_bag/4.png')}}">
                    <img src="{{url('/diaper_bag/5.png')}}">
                    <img src="{{url('/diaper_bag/6.png')}}">
                    <span class="product_span">Available in 7 more prints</span>
                    <br>
                  
                    @endif
                    @endif
                    
                    @if(isset($categoryslug) && isset($subcategoryslug))
                    
                  <a class="product-title" href="{{url('/'.$categoryslug. '/' .$subcategoryslug. '/' .$product->slug)}}" title="{{$product->slug}}">

                    <h2 class="woocommerce-loop-product__title">{{$product->product_title}}
                    </h2>   
                  </a>
                    @endif
                   

                  @if($product->compare_price != 'NULL')
                  <span class="price">
                    <del>
                      <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol"> {{$product->compare_price}}
                        </span>
                      </span>
                    </del> 
                    <ins>
                      <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">&#x20B9; @if(isset($subcategoryslug) == 'all-bedding-collections') 699 - 12,500 @else {{$product->price}} @endif
                        </span>
                      </span>
                    </ins>
                  </span>
                  @else
                  <span class="price">
                    <del>
                      <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">&#x20B9; {{$product->price}}
                        </span>
                      </span>
                    </del> 
                  @endif
                 <!--  <div class="star-rating" title="Rated 5.00 out of 5">
                    
                  </div>   -->    
                  <div class="buttons-wrapper">
                  <!--  <form>
                                     <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
                                    @if(isset($product->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
                                    @endif
                                    <input type="submit" class=" button product_type_simple add_to_cart_button ajax_add_to_cart cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Bag" style="background: #000 !important;color: #fff !important;  border-radius: 3px !important; width: 78%; ">
                                    <div class="cartmeatmob cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div>
                                  </form>
 -->
                 <!--    @if(isset($categoryslug))
                    <a href="{{url('/'. $categoryslug. '/' .$product->slug)}}" class=" yith-wcqv-button" >Quick Viewdd</a>
                    @endif
                    @if(isset($parentcat) && isset($categoryslug))
                    <a href="{{url('/'.$parentcat. '/' .$categoryslug. '/' .$product->slug)}}" class=" yith-wcqv-button" >Quick View</a>
                    @endif
                    </a> -->
                        
                    <span class="product-tooltip">
                    </span>
                  </div>
                </div>
              </div>
            </li>
             @endforeach
          </ul>
          </div>
        </div>
                    <!-- #content -->

                   <!--  <div id="sidebar-shop" class="widget-area col-xs-12 col-sm-4 col-md-3 col-md-pull-9 col-sm-pull-8 sidebar" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
                        <div id="brideliness_collapsing_categories-3" class="widget widget_brideliness_collapsing_categories">
                            <h3 class="widget-title"><span>Categories</span></h3>
                            <ul class="collapse-categories">
                             <?php
                                        $menulist = ProductCategory::where('is_menu', 1)->get();
                                        // echo $menulist;
                                        ?>
                                        @foreach($menulist as $menu)
                                        <?php
                                        $id = $menu->id;
                                        $countsubcat = CategoryParent::where('parent_category_id' , '=',$id )->count();
                                        $products1 = Product::where('product_category' , '=', $id)->count();


                                        ?>
                                <li class="cat-item cat-item-213 cat-parent current-cat-parent"><a class="category" href="{{url('/'.$menu->slug.'/')}}">{{$menu->name}}</a><a href="#<?php echo $id;?>" class="show-children" data-toggle="collapse" aria-controls="children-of-213" aria-expanded="false"> @if($countsubcat > 0) 
                                <span></span>
                                @endif
                                </a> <span class="count">({{$products1}})</span>
                                @if($countsubcat > 0) 
                                    <ul id='<?php echo $id;?>' class='children collapse <?php if($menulist->first() == $menu) echo 'in'; ?>'>
                                     <?php 
                                            $id = $menu->id;
                                            $submenus = DB::table('category_parent')
                                                ->join('product_category', 'product_category.id', '=', 'category_parent.category_id', 'left')
                                                ->where('category_parent.parent_category_id', $id)
                                                ->select('category_parent.*', 'product_category.name as category_name','product_category.slug','product_category.id as pid')
                                                ->get();
                                            ?>
                                             @foreach($submenus as $submenu)
                                             <?php 


                                            $products = ProductCategories::where('category_id' , '=', $submenu->pid)->count();
                                            $submenu->pid;
                                            ?>
                                            {{$submenu->pid}}
                                        <li class="cat-item cat-item-294 current-cat"><a class="category" href="{{url($menu->slug.'/'.$submenu->slug.'/')}}">{{$submenu->category_name}}</a> @if($products > 0) 
                                <span></span>
                                @endif
                                </a> <span class="count">({{$products}})</span>
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                        @endforeach
                                        </ul>
                        </div>
                        <div id="brideliness_shop_filters_widget-6" class="widget widget_brideliness_shop_filters_widget">
                            <h3 class="widget-title"><span>Color</span></h3>
                            <ul data-isotope="filters" class="filters-group gride-mode" data-filter-group="color">
                                <li data-filter="" class="filter selected is-checked"><input type="checkbox" name="color" name=""></span>All</li>
                                <li class="filter" data-filter=".beige"><input type="checkbox" name="color" ></span><span>Beige</span><span class="counter">(1)</span></li>
                                <li class="filter" data-filter=".champagne"><input type="checkbox" name="color" ></span><span>Champagne</span><span class="counter">(1)</span></li>
                                <li class="filter" data-filter=".coral"><input type="checkbox" name="color" ></span><span>Coral</span><span class="counter">(1)</span></li>
                                <li class="filter" data-filter=".grey"><input type="checkbox" name="color" ></span><span>Grey</span><span class="counter">(1)</span></li>
                                <li class="filter" data-filter=".mint"><input type="checkbox" name="color" ></span><span>Mint</span><span class="counter">(1)</span></li>
                                <li class="filter" data-filter=".purple"><input type="checkbox" name="color" ></span><span>Purple</span><span class="counter">(1)</span></li>
                                <li class="filter" data-filter=".white"><input type="checkbox" name="color" ></span><span>White</span><span class="counter">(1)</span></li>
                            </ul>
                        </div>
                        
                        <div id="woocommerce_price_filter-2" class="widget woocommerce widget_price_filter">
                            <h3 class="widget-title"><span>price</span></h3>
                            <form method="get" action="http://brideliness.themes.zone/product-category/accessories/brooches/">
                                <div class="price_slider_wrapper">
                                    <div class="price_slider" style="display:none;"></div>
                                    <div class="price_slider_amount">
                                        <input type="text" id="min_price" name="min_price" value="" data-min="5" placeholder="Min price" />
                                        <input type="text" id="max_price" name="max_price" value="" data-max="150" placeholder="Max price" />
                                        <button type="submit" class="button">Filter</button>
                                        <div class="price_label" style="display:none;">
                                            Price: <span class="from"></span> &mdash; <span class="to"></span>
                                        </div>

                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> -->

                </div>
            </div>

        </div>
    </div>
    <!-- Content wrapper (end)-->

    <aside id="sidebar-shop-bottom" class="widget-area" role="complementary" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
        <div class="container">
            <div class="row">
                <div class="shop-bottom-sidebar col-xs-12 col-sm-12 col-md-6">
                    <div id="woocommerce_products-6" class="widget woocommerce widget_top_rated_products">
                        <h3 class="shop-bottom-sidebar">NEW INS</h3>
                        <ul class="product_list_widget">
                        @foreach($recentproduct as $newproduct)
                        <?php

                         $productcatd = DB::table('product_categories')
                                        ->join('product_category', 'product_category.id', '=', 'product_categories.category_id', 'left')
                                        ->select('product_category.slug')
                                        ->where('product_categories.product_id' , '=' ,$newproduct->id)->first();
                                        // echo $productcatd->slug;

                         ?>
                            <li>
                                <a href="{{URL('bedding/'.$productcatd->slug.'/'.$newproduct->slug)}}">
                                    <img width="100" height="145" src="{{URL::to('/product_images/'.$newproduct->id.'/featured_images/'.$newproduct->product_images)}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" /> 
                                    <span class="product-title">{{$newproduct->product_title}}</span>
                                </a>
                                <!-- <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rs</span>{{$newproduct->price}}</span></del> --> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#x20B9; </span>{{$newproduct->price}}</span></ins>

                            </li>
                            @endforeach

                          
                        </ul>
                    </div>
                </div>
                <div class="shop-bottom-sidebar col-xs-12 col-sm-12 col-md-6">
                    <div id="woocommerce_top_rated_products-2" class="widget woocommerce widget_top_rated_products">
                        <h3 class="shop-bottom-sidebar">BEST SELLERS</h3>
                       <ul class="product_list_widget">
                        @foreach($recentproduct as $newproduct)
                             <?php

                         $productcatd = DB::table('product_categories')
                                        ->join('product_category', 'product_category.id', '=', 'product_categories.category_id', 'left')
                                        ->select('product_category.slug')
                                        ->where('product_categories.product_id' , '=' ,$newproduct->id)->first();
                                        // echo $productcatd->slug;

                         ?>
                            <li>
                                <a href="{{URL('bedding/'.$productcatd->slug.'/'.$newproduct->slug)}}">
                                    <img width="100" height="145" src="{{URL::to('/product_images/'.$newproduct->id.'/featured_images/'.$newproduct->product_images)}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" /> 
                                    <span class="product-title">{{$newproduct->product_title}}</span>
                                </a>
                                <!-- <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rs</span>{{$newproduct->price}}</span></del>  --><ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#x20B9; </span>{{$newproduct->price}}</span></ins>

                            </li>
                            @endforeach

                          
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </aside>

</div>

<div id="yith-quick-view-modal">

    <div class="yith-quick-view-overlay"></div>

    <div class="yith-wcqv-wrapper">

        <div class="yith-wcqv-main">

            <div class="yith-wcqv-head">
                <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a>
            </div>

            <div id="yith-quick-view-content" class="woocommerce single-product"></div>

        </div>

    </div>

</div>

<script type="text/template" id="tmpl-unavailable-variation-template">
    <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>

<!-- Filter Script -->
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<script>



$('.sort_filter').unbind().bind('change', function(){
    showProductFilter();
});
// $('.reset-filters').on('click', function(e){
//     window.location.reload(true);
// });

<?php
    if (isset($categoryslug)) {
        $categoryslug = $categoryslug;
    }else{
        $categoryslug = null;
    }
?>

$( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 2000,
      step: 50,
      values: [ 0, 2000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
        $('#min_price').val(ui.values[ 0 ]);
        $('#max_price').val(ui.values[ 1 ]);

      },
      stop: function(event, ui) {
            showProductFilter();
       }
    });
    $( "#amount" ).val( "Rs." + $( "#slider-range" ).slider( "values", 0 ) +
      " - Rs." + $( "#slider-range" ).slider( "values", 1 ) );
  } );

// weight filter
$( function() {
    $( "#slider-weight-range" ).slider({
      range: true,
      min: 0,
      max: 2000,
      step: 50,
      values: [ 0, 2000 ],
      slide: function( event, ui ) {
        $( "#weight_filter" ).val( ui.values[ 0 ] + "gm - " + ui.values[ 1 ] + "gm");
        $('#min_weight').val(ui.values[ 0 ]);
        $('#max_weight').val(ui.values[ 1 ]);
      },
      stop: function( event, ui ) {
            showProductFilter();
      }
    });
    $( "#weight_filter" ).val($( "#slider-weight-range" ).slider( "values", 0 ) + "gm - "
       + $( "#slider-weight-range" ).slider( "values", 1 ) + "gm" );
  } );

function showProductFilter(page = null){
    <?php
        if (isset($categoryslug)) {
            $categoryslug = $categoryslug;
        }else{
            $categoryslug = null;
        }

        if (isset($subcategoryslug)) {
            $subcategoryslug = $subcategoryslug;
        }else{
            $subcategoryslug = null;
        }
    ?>
    var url = "{{URL::to('/ajax/product-list?categoryslug='.$categoryslug.'&subcategoryslug='.$subcategoryslug)}}"; 
    searchurl = url.replace(/&amp;/g, '&');

    $.ajax({
        url: searchurl,
        type: "GET",
        data: $("#search-filter").serialize(),
        async:true,
        cache:false,
        beforeSend: function (jqXHR, settings) {showProductFilter
          $(".loading").show();
        },
        success: function (data, textStatus, jqXHR) {
          $('.product-list').html(data);
        },
        complete: function () {
          $(".loading").hide();
          window.location = '#';
        }
  });
}
</script>
  
<script type='text/javascript' src="{{URL::to('admin-assets/js/productdetail.min.js')}}"></script>

@endsection
