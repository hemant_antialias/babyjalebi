@extends('web-files.web_layout')

@section('content')

<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
        
        
        
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle" style= "font-family: Lato"; !important>Personalization Services</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
      
</div>
</div>
  </div>
  <div class="user-content row-spacing lightboximages textjustify">
    What happens when a new life comes to your world? It completely changes, doesn’t it?<br>
    <br>To make your experience even more exciting and full of love, Baby Jalebi is here to support you whole heartedly.
    Our company has been made out of that special love that only comes from being a new mother.<br>
    <br>We understand how taking care of your little baby is so important and are conscious of your feelings while preparing for their arrival. 
   The heart of Baby Jalebi lies in its research and innovation towards creating new products that merge seamlessly into your baby’s life making everything just a little more comfortable, 
   fun and easier for the new mother and baby. <br>
   Personalize your products here!!<br>

  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>
@endsection