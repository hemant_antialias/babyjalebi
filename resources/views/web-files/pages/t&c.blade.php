@extends('web-files.web_layout')

@section('content')

<style type="text/css">
        .nonbullets{
            list-style-type: none;
        }
        .note{
                font-weight: bolder;
                background-color: yellow;
        }
        .uppertext{
            text-transform: uppercase;
        }
    </style>


<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
        
        
        
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle">Terms & Conditions</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
    
   
    
</div>
</div>
  </div>
  <div class="user-content row-spacing lightboximages">
      
      <p class="textjustify">BabyJalebi is the sole property of Heirlooms India Pvt. Ltd. Please ensure that you have read the following terms and conditions carefully before using BabyJalebi services; they set out what you can expect from us and the terms on which you agree to be bound as and when you place your order.</p>
      
      <h4>1. BECOMING A CUSTOMER</h4>
      <p class="textjustify">
        <ul>
            <li>To place an order with us, you must be over 18 years of age, require delivery in our area and have a credit or debit card acceptable to us in your name.</li>
            <li>We reserve the right to decline any order or to suspend your Account with us at our discretion.</li>
            <li>Our Terms and Conditions will apply when you place your order.</li>
      
        </ul>  
      </p>
      <h4>2. DELIVERY</h4>
              <p class="textjustify">
        <ul>
            <li>We will always aim to deliver your order within the delivery slot requested by you at the time you place your order.</li>
            <li>If nobody is available at the primary delivery address (nominated by you at the time you place your order) to take delivery of the order, we will leave notification of the delivery instead either to the secondary delivery address or secure place (nominated by you at the time you place your order).</li>
            <li>Events outside our control, for example, extreme weather conditions, may occasionally mean we are unable to deliver your goods at the time agreed. In these circumstances, we will contact you as early as possible to rearrange the delivery time. In such an event, our liability to you shall be limited to the price of the goods ordered and the cost of delivery.</li>
      
        </ul>  
      </p>
      
      
      
      
<!--
   <p class="textjustify"> <b>Refund policy-</b> Mode and duration missing (Refund will be processed through NEFT within 10 working days).</p>

<p class="textjustify"><b>Cancellation Policy-</b> Cancellation duration missing (Customer can cancel the order within 15 minutes after placing the order if he/she wish to).</p>
-->
  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>

<div class="container">
    <div class="return">
        <h3 class="uppertext">
            return and refund
        </h3>
        <ul>
            <li>We take the utmost care in serving our customers and deliver the orders as per booking confirmation. However if you observe any damage in packaging products are not ok or the quantity or type/category is not as per the order booking, kindly check all these at the time of delivery in front our delivery staff and inform the customer care executive immediately . We will replace it for you at the next earliest delivery time or day as per availability of the products and quantity you had ordered. Any claims thereafter would not be entertained under any circumstances.</li>
            <li>We strictly follow No Refund Policy once the order is confirmed and delivered to customer. However if we could not redeliver the return order as per our above mention return policy then we will refund the already paid amount as per payment option selected within 3 to 4 working days.</li>
            <li>Order Cancellation:</li>
            <li>A Customer can opt to cancel order within 30 minutes for the order placed  by calling our customer service and receive refund of advance paid if any.</li>

        </ul>
        <h3 class="uppertext">
            Delivery, Packaging, and Order Value
        </h3>
        <ul>
            <li>Order value for below mentioned area.</li>
            <li>West and Central Delhi</li>
            <li>Areas  Order Value</li>
            <li>Central Delhi and Adjoining Areas    Rs 1000/- Free Shipping.</li>
            <li>West Delhi and Adjoining Areas Rs 1000/- Free Shipping</li>
            <li>Dwarka and Adjoining Area Rs 1500/- Free Shipping</li>
            <li>Noida   Rs 1000/- Free Shipping</li>    
        </ul>
        <p class="note">Note: - To verify your delivery locations please confirm your locality section on website’s front page</p>
        <ul class="nonbullets">
            <li>1) Minimum order value is Rs 400/-</li>
            <li>2) We offer free delivery on orders above Rs 700/- For orders below Rs 700/-, a minimal delivery charge of Rs 50 will be applicable.</li>
            <li>3) Delivery of the orders will be made only to the address mentioned during the booking. Please ensure the delivery address with Landmark and receiver's name and contact no. details for your order is accurate and complete. Change of address can be accepted only of the product which has not left for delivery.</li>
            <li>4) For Cash on Delivery payment option customer has to make full payment at the time of delivery.</li>
            <li>5) We will not be responsible for not delivering order as per notified time given to customer in situations or circumstances like transport hurdles, traffic conditions, strikes, accident, natural calamity etc. or any other condition which is beyond our control. Customer has to accept the delivery and liable to make full payment as per order booking.</li>
            <li>6) We do take utmost care and maintain high quality and hygiene while packing your order.</li>
            <li>7) We use very good quality packing material to maintain hygiene and freshness we recommend to consume raw fresh items as soon as possible and to be kept in fridge 4 degree C and frozen items need to be kept in freezer minus 18 degree C.</li>
            <li>8) Promo codes to be applicable on the order we will not be responsible for the promo code once the order is placed and out for delivery.</li>
        </ul><br>
    </div>
</div>
@endsection