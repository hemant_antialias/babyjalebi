@extends('web-files.web_layout')

@section('content')

<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
        
        
        
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle" style= "font-family: Lato"; !important>FAQ's</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
      
</div>
</div>
  </div>
  <div class="user-content row-spacing lightboximages textjustify">
    • PAYMENTS<br>

• SHIPPING AND DELIVERY<br>

• ORDER TRACKING<br>

• CANCELLATION / MODIFICATION<br>

• RETURNS POLICY<br>

• TERMS & CONDITIONS FOR RETURN<br>

• PRIVACY POLICY<br>

• INTRODUCTORY AND SIGN UP OFFER<br><br>


PAYMENTS<br><br>

How do I pay for my order?<br><br>
We have a number of payment methods. Choose the one that is most convenient for you. The options are:

Online Bank Transfer through your Credit, Debit, or Cash Card

Cash on Delivery (COD)<br><br>

Do you offer a Cash On Delivery (COD) payment option? How does it work?<br><br>
Yes, we support the COD payment method. You can make the payment in cash to the delivery agent once the product reaches you.
<br><br>
What should I do if my payment doesn't get through or fails?<br><br>
If your payment transaction fails, please contact our Customer Service immediately on +91 8588850079 or email us at wecare@babyjalebi.com<br><br>




SHIPPING AND DELIVERY<br><br>

Do I have to pay shipping charges?<br><br>
Yes, you have to pay shipping charges for transactions.
<br><br>
By when will I receive the ordered products?<br><br>
If you have purchased online, with a Credit, Debit, or Cash card, you will receive an order receipt and product delivery information email as soon as the order is placed.
<br><br>
If the payment method is Cash On Delivery (COD), we will give you a confirmation via phone and or email.
<br><br>
All products will be shipped within 48hours and delivered within 4 – 5 working days.
<br><br>
How do I know whether the product can be delivered to my area? Can I opt for Cash On Delivery (COD) payment in my area?<br><br>
Baby Jalebi is trying its best to ship its products as far and wide as possible. However, there are still a few areas where we are unable to ship. While placing an order, you will receive an intimation of the same. Similarly, you will also be informed on whether or not the Cash On Delivery (COD) payment option is available in your respective area.<br><br>
A few months ago, I had ordered from Baby Jalebi and the order was delivered successfully to my address. However, this time, I am unable to place an order as my Pincode is not being recognized. Why so?<br><br>
There were a few areas and Pincodes that were serviced by Baby Jalebi earlier; however, they are unserviceable now due to courier issues. If you are trying to place an order using one of these Pincodes, we won't be able to take your order. We regret the inconvenience. To place your order successfully, we request you to provide a different / alternate address with a serviceable Pincode. Please get in touch with our Customer Service on +91 8588850079 to know which Pincodes are serviceable.
<br><br>
 What is the mode of delivery at Baby Jalebi?<br><br>
We have partnered with First Flight courier services for the shipment of our products across India. Depending upon your location and reach, we also make use of several other trustworthy domestic service providers to deliver products.
<br><br>
Why have I received only a part of my order?<br><br>
Each product has its own delivery duration. Please note that, if you have ordered multiple items, the entire order may not be delivered together and you would be receiving partial shipments, as each product may be delivered as per the individual delivery timeline. However we try and send maximum products from your order in one shipment.


<br><br>

ORDER TRACKING<br><br>

How do I know if my order has been placed successfully?<br><br>
Within a few minutes of successfully placing your order, you will receive an email confirmation from Baby Jalebi. This email will include all the important details related to your order. Please do not delete this email, as it, more or less, acts as an order receipt.
<br><br>
How do I check the status of my order?<br><br>
We will provide you with a link to the First Flight website in your confirmation email.

Through this link you will be able to track your parcel.


<br><br>

CANCELLATION / MODIFICATION<br><br>

Can I cancel an order?<br><br>
Though the Baby Jalebi website itself does not have any option to cancel an order once the payment is made, you can definitely get in touch with our Customer Service on +91 8588850079 at the earliest (within 24 hours of placing the order) with your Order No. to cancel it. If your order has not been processed, we can cancel it and refund the amount as Baby Jalebi Credits.
<br><br>
Can I modify or change my order?<br><br>
Our helpful and friendly Customer Service Executives are always at your service in case you want to modify your order; simply call +91 8588850079 . Please note if your order has already been shipped, we will not modify your order. They will also be happy to help you if you want to update your shipping address. Remember to be as quick as possible and contact our Customer Service at the earliest for anything like this.
<br><br>
I've cancelled an order. By when can I expect a refund?<br><br>
Once your order has been cancelled, we will immediately process the refund. Your refund will be in the form of Baby Jalebi Credits, which can be used for any of your subsequent purchases with us. The subsequent purchases need to be made within 60 days. 
<br><br>
Can I mix and match items from different sets and collections? In other words, can you personalise a set? <br><br>
Bedding sets cannot be broken.<br><br>

The bumper & cot skirt can be purchased individually.

Incase you wish to personalize a product please contact us on +91 8588850079 or wecare@babyjalebi.com
<br><br>
 

 How do I contact Baby Jalebi Customer Service Team?<br><br>
You can get in touch with our Customer Service by emailing us at wecare@babyjalebi.com or can call us on +91 8588850079 between 10 a.m. and 7 p.m.


<br><br>

RETURNS POLICY<br><br>

When you shop with Baby Jalebi, it is our topmost priority to deliver quality products and to give you the most enjoyable and hassle-free online shopping experience. Keeping this in mind, our Returns Policy is also safe, easy and transparent.
<br><br>
 

In which cases can I return the product? <br><br>

You can return a product in the following two scenarios:<br><br>

1. A product is damaged, possibly in transit. Baby Jalebi does have a highly stringent Quality Control Department; however, at times, a few hitches might go unnoticed.
<br><br>
2. Mismatched product delivered, in terms of style, and / or size.
<br><br>
If you want to return the product for any other reason apart from the above-mentioned ones, please get in touch with our Customer Service within 48 hours from the receipt of delivery. You can either call our Customer Service on +91 8588850079 between 10.00 a.m. and 7.00 p.m. (IST), or email us at wecare@babyjalebi.com . When can I return the product?
<br><br>
Please get in touch with us within 48 hours from the date of receiving your shipment.
<br><br>
Who pays the postal/shipping charges for the returned goods?
This depends on things such as the availability of our associated courier service in your area, the size / weight of the product. Please call our Customer Service on +91 8588850079 for further details.
<br><br>
What is the returns procedure? 
<br><br>
Step 1
Call our Customer Service on +91 8588850079 between 10.00 a.m. and 7.00 p.m. (IST) from Monday to Saturday. You can also email us at wecare@babyjalebi.com and mention the details of the return.
<br><br><
Step 2
Once your returns request is validated over phone, we will send you an email authorizing a return, along with the address for the same. Please print a copy of the email and attach it to the return shipment. To be eligible for the return, kindly use our associated courier partners to send us back the product. In a rare case, due to logistical constraints, if we are unable to collect a return, and you've arranged for the same, you would be reimbursed your shipping costs after an approval from Baby Jalebi's management. Remember, we must receive the package within 10 days of us sending the authorising email. If the pick-up is being arranged by us via our authorised courier partner, kindly facilitate the handover from your delivery address within 48 hours of receipt of the authorisation email.
<br><br>
 Step 3
After receiving the package, we will inspect the same. Once approved as an eligible return by our Quality Assurance Team, we will ensure that we replace the product. If the product is not available, we will issue a merchandise credit in the appropriate amount. Your returns issue will be closed within 10 days of our receiving the return package from you.
<br><br>
What is the Return Policy for the discounted products?<br><br>
Baby Jalebi does not offer replacements on discounted or season-end sale products.
<br><br>
I've cancelled an order. Can I expect a cash refund?
No refunds are applicable for cancellations, Baby Jalebi will offer you credits in such case, which can be redeemed on your next purchase. A refund is applicable only under the sole discretion of Baby Jalebi Retail Pvt.Ltd. All refunds would be through a NEFT transfer/Cheque deposit in your Bank Account and no Cash payments will be made in lieu of the same.


<br><br>

Terms & Conditions for Return:<br><br>

1. The final decision on whether a returned item will be approved or rejected lies completely with Baby Jalebi Quality Assurance Team.
<br><br>
2. The returned items will only be accepted in their original packaging, which includes bags, tags, boxes, invoices, documentations, warranty cards (if any), all accessories, and all the things that were present when the item was delivered to you.
<br><br>
3. The product you're returning must be unused, unwashed and in its original condition. If it is used, damaged, soiled or altered, the return won't be processed and the items will be sent back to you.
<br><br>
4. PLEASE DO NOT ACCEPT THE PACKAGE IN DAMAGED OR SOILED CONDITION.
<br><br>
5. Products in sets cannot be returned individually.
<br><br>
6. If we are unable to send you a replacement of the item that you have returned, we will offer you a credit note for the same. (Validity for the credit note is three months from the date of issue). Store credit given to the customer is not transferable to other user/s.
<br><br>
7. To return an item, we will arrange to pick the parcel from the given address. If the service is not available in your area, please arrange for a return via a trusted courier service of your choice.
<br><br>
8. Due to logistical constraints, if we are unable to collect a return, and you've arranged for the same, you would be reimbursed your shipping costs (maximum Rs. 150), after an approval from Baby Jalebi’s management.
<br><br>
9. No refunds are applicable for cancellations, Baby Jalebi will offer you credits in such case, which can be redeemed on your next purchase. A refund is applicable only under the sole discretion of Baby Jalebi Retail Pvt.Ltd. All refunds would be through a NEFT transfer/Cheque deposit in your Bank Account and no Cash payments will be made in lieu of the same.
<br><br>
Also, in this case, please let us know the Tracking No. (i.e., AWB Delivery Confirmation), as we are not responsible for lost or stolen packages. When we receive your returned package, we will inspect the item to confirm that it is unused and in its original packaging.
<br><br>
10. Please note that the products might look different from the ones you see on the website.
<br><br>
Shipping an item back to Baby Jalebi:
<br><br>
We offer pick up service from all major cities in India. Please call our Customer Service on +91 8588850079 to know if the service is available in your area.For other locations, as advised by our Customer Service Executive, we offer Shopping Credits as India Circus Credit to compensate for return shipping cost.


<br><br>

PRIVACY POLICY<br><br>

What is Baby Jalebi’s Privacy Policy?<br><br>
To maintain your right to privacy while shopping with us is one of our biggest priorities. We, thus, only ask for the most necessary information that is required for us to complete your order or to contact you regarding its status. The information we collect includes your name, email address, shipping address and billing address. Apart from the Baby Jalebi team, this information is only shared with our associated courier partner that will be delivering your ordered items. Please be assured that Baby Jalebi does not store your credit / debit card or online bank account information.
<br><br>
 

Please note that Baby Jalebi reserves the right to update this Privacy Policy at anytime. We will send you an intimation of the same on the email address that you have shared with us.
<br><br>
Does Baby Jalebi store my information?<br><br>
Baby Jalebi only stores information that is uploaded by you during the registration process, along with shared comments, conversations and interactions with other members on the website. No information such as credit / debit card details or online back account information, etc., is stored in our database. These details are directly entered by you into the bank payment gateways, which are 100 percent secure. We do not handle this sensitive data or store it.
<br><br>
E-Gift Card Policy<br><br>

- E-Gift Cards bought on our website can only be redeemed online.

- E-Gift Cards are valid till 6 months from the date of purchase

Baby Jalebi is not responsible if the E-Gift Card is lost or stolen or used without permission

If the order value exceeds the gift card amount, the outstanding balance must be paid by debit card/credit card/net banking or Cash on delivery.

- Shipping cost if not covered by the gift card value on final purchase must be paid for by for by the end user.

E-Gift Card Policy is subject to change at any time without prior notice.<br><br>
  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>
@endsection