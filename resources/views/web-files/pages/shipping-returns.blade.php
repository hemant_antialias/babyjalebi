@extends('web-files.web_layout')

@section('content')


<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
        
        
        
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle" style= "font-family: Lato"; !important>Shipping and Returns Policy</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
      
</div>
</div>
  </div>
  <div class="user-content row-spacing lightboximages textjustify">
    EXCHANGES & RETURNS<br><br>

We invest a great deal of love and care in the making of each Baby Jalebi piece.
If for some reason, you are unsure of the product or we’ve missed a spot and you’d like to exchange them,
you can write to us at  wecare@babyjalebi.com or Call / WhatsApp us on + 91 8588850079 and someone from our customer care team
will contact you within 1-2 business days and will guide you through the process of returning and exchanging the item. 
They may request for further information/photographs to assess your return request. Please do assist them.<br>


    <br>DISCLAIMER : <br>
1. If a piece is exchanged once, it cannot be exchanged the second time.<br><br>
2. Please note that not all products are eligible for exchange.
All personalised products are non returnable and non exchangeable <br>
<br>
3. HOW TO EXCHANGE? <br>

You must inform us within 7 days of delivery of the product. Please send us an email to <a href="http://www.babyjalebi.com/" target="_blank">www.babyjalebi.com</a> or Whatsapp us on + 91 8588850079 with order no and attach an image of the piece purchased.
Once you have mailed us, please ship out the pieces to the following address:<br><br>

BABY JALEBI /HEIRLOOMS <br>

15/2 MILESTONE OLD PALAM GURGAON ROAD <br>

GURGAON , HARYANA <br>

122001<br>

    <br>The cost for shipping the product back to us is to be borne by you. When we deliver your corrected pair, BABY JALEBI will take care of the shipping.

Please ensure that the product is in its original condition and packaging. Item(s) which show signs of being used will not be eligible for returns or exchanges.

We would require all original papers to be returned with the product.

Baby Jalebi reserves all rights to inspect the condition of the product before processing any returns or exchanges..

As a company policy we do not provide refund on any product sold. Once the Baby Jalebi pieces are sold they can be exchanged for replacement or a credit note (provided the aforementioned terms and conditions are met).


     <br><br>

  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>

@endsection