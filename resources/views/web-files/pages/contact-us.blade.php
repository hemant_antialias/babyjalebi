@extends('web-files.web_layout')

@section('content')
<style>
  .error
  {
    display: none;
  }
  .success
  {
    display: block;
    color: green;
  }
</style>
<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
           
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle" style= "font-family: Lato";>Contact Us</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
    
     
</div>
</div>
  </div>
  <div class="col-md-6 user-content row-spacing lightboximages">
   <h4>Registered address:</h4><br>
        Baby Jalebi<br>
		5, Shopping Center,<br>
		West End, New Delhi - 110021 <br>
		India
   <p class="textjustify"> <h5>For any queries please contact us via email:- <a href="mailto:wecare@babyjalebi.com">wecare@babyjalebi.com </a> or call us.<span class="tel:+918588850079" >
                (+91)8588850079
            </span></h5> </p>
          </div>
                <div class="col-md-6 touchpad" > 
      <h3 id="error" style="display: none;">Please fill all the required fields</h3>
      <form action="#" id="reviewForm" method="post" class="form comment-form" name="form">
  <fieldset>
    <legend>Get in touch:</legend>
     Name <span style="color: red; font-size: 14px;">*</span>
    <input type="text" class="form-control form_input" name="name"  id="name" value="" placeholder="Enter your name" required='required'><br>
     Phone <span style="color: red; font-size: 14px;">*</span>
    <input type="text" class="form-control form_input" name="contact" id="contact" value="" placeholder="Enter your phone number" required='required'><br>
     Email <span style="color: red; font-size: 14px;">*</span>
    <input type="text" class="form-control form_input" id="mail" name="email" value="" placeholder=" Enter your Email id" required='required'><br>
    Message:
    <textarea name="message" value="" placeholder="Enter your message" col="30" rows="5"></textarea>
<br>
    <input name="submit" type="submit" id="submit" class="submit submitreview" value="Submit" />
  </fieldset>
</form>
<h5 class="success" style="display: none; color: green;">Thank you for contacting us, we'll get back to you shortly. </h5>
</div>
</div>

  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>

</div>
      
</div> 
          <script>
 $('.submitreview').click(function(e){
    e.preventDefault();
      
    // var name = document.getElementById("name");
    var name = document.getElementById('name').value;
    var contact = document.getElementById("contact").value;
    var mail = document.getElementById("mail").value;
    // alert(name);
    // var review = document.getElementById("msg").val;
    // alert(review);
if (name == '' || contact == '' || mail == '') {
// alert("Fill All Fields");
$("#error").css("display","block");
$("#error").css("color","red");
}
else
{
// var msgval = $("#msg").val();
    var base_url = "{{URL::to('/')}}";
       $.ajax({
        url:base_url+'/sendcontact',
          type:'POST',
          data:$("#reviewForm").serialize(),
          success:function(data){
            if (data.status==1) {
            // $("#npopupContact").modal('hide');
            div_hide();
             // window.location.reload();
             // alert(data.message);
             // $('.success-review').html(data.message);
             // $('.-review').addClass('succcolor');
             


              // toastr.success(data.message, {timeOut: 100000});
              // return;
            }
            // $(".success").css("display", "block");
            var form = document.getElementById("reviewForm");
            form.reset();
            // $('form :input[type="text"]').attr('value', '');
            $(".success").css("display", "block");
            // $('.user-detail').html(data);
            // toastr.success('Customer has been added successfully.', {timeOut: 100000})
            // $('.email-invoice').prop("disabled", true);
          }
      });
   }
    });
</script>




@endsection