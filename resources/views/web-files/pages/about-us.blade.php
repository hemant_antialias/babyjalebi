@extends('web-files.web_layout')

@section('content')

<div class="bootstrap-iso">
    <div id="content">
        <div class="container">

        <div id="content">
        
        
        
        
        
          <div class="container cf">
  <div class="abpad cf">
    <h1 class="majortitle" style= "font-family: Lato"; !important>About Us</h1>
    <div class="social-area">
<div class="social">
    <!-- <span class="socitem label">Share:</span> -->
      
</div>
</div>
  </div>
  <div class="user-content row-spacing lightboximages textjustify">
    What happens when a new life comes to your world? It completely changes, doesn’t it?<br>
    <br>To make your experience even more exciting and full of love, Baby Jalebi is here to support you whole heartedly.
    Our company has been made out of that special love that only comes from being a new mother.<br>
    <br>We understand how taking care of your little baby is so important and are conscious of your feelings while preparing for their arrival. 
   The heart of Baby Jalebi lies in its research and innovation towards creating new products that merge seamlessly into your baby’s life making everything just a little more comfortable, 
   fun and easier for the new mother and baby. 
Our unique products like the beautiful baby cot bedding, the bed in a bag & the diaper bags are bundled together with soft breathable and 100% cotton fabrics,
 topped with cheerful and versatile prints and colors to make sure that our products are unique and fun.
     <br><br>Having spent years manufacturing excellent luxury products for industry leaders and designer brands globally,
      we wanted to use our technical prowess, design sense and experience to create unique baby products for the precious little ones. 
      We are proud to say all our products at Baby Jalebi are 100% made in India and manufactured in our state-of-the-art facility in NCR.
      And not only that, we will soon be shipping internationally as well.
      <br><br>We believe in creating a happy, safe, exciting, fun and beautiful world. We hope that our customers, 
      especially the small, cute and cuddly ones will enjoy using our products as much as we love creating them.<br><br>

With all our love and some magic,<br>
Rati Nehra and Gunia Chopra<br>
Founders<br><a href="http://www.babyjalebi.com/" target="_blank">www.BabyJalebi.com</a>
  </div>
</div>
        
    </div>

        </div>
    </div>    
    <br><br>
</div>
@endsection