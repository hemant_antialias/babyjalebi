@extends('web-files.web_layout')
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
<style>

    .owl-carousel .owl-item img{
            display: block;
    width: 100%;
    /*height: 410px !important;*/
    }
    .card {
    float: left;
    width: 100%;
    padding: .15rem;
    margin-bottom: .15rem;
    border: 0;
}
.card {
    position: relative;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
    background-color: #fff;
    border-radius: .25rem;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */
  /*  padding-top: 100px;*/ /* Location of the box */
    left: 50%  !important;
    top: 50%  !important;
    transform: translate(-50%, -50%) !important;
    -webkit-transform: translate(-50%, -50%) !important;
    -moz-transform: translate(-50%, -50%) !important;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    padding-left: 17%;
    padding-right: 17%;
    padding-top: 10%;
}

/* Modal Content (Image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption {
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}
.close {
        right: 43px;
    position: fixed;
    top: 27px;
    color: #f1f1f1 ;
    font-size: 45px !important;
    font-weight: bold;
    transition: 0.3s;
}
</style>
@section('content')

<div id="myModal" class="modal close1">

 <!-- The Close Button -->
  <span class="close close1" style="color: white !important" onclick="document.getElementById('myModal').style.display='block'">&times;</span>


 <!-- Modal Content (The Image) --> 
  <div id="img01">
  <!-- <a href="javascript:;" class="textright" id="close">Close</a> -->
  <iframe id="utube" width="100%" height="480" src="https://www.youtube.com/embed/L3DnPn3um_0" frameborder="0" allowfullscreen></iframe>

  </div>

</div>
<!-- Content wrapper (start)-->
<div id="main" class="site-main container">
  <div class="row">
    <main id="content" class="site-content col-xs-12 col-md-12 col-sm-12" itemscope="itemscope" itemprop="mainContentOfPage">
      <div class="entry-content">
        <div class="vc_row wpb_row vc_row-fluid ">
          
          <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                    <div class="owl-carousel owl-theme padbot" >
                    @foreach($sliders as $slide)
                        <div class="item "><a href="{{url::to('/'.$slide->link)}}"><img src="{{URL::to('/banner_images/'.$slide->id.'/'.$slide->image_name)}}" ></a> </div>
                    @endforeach
                  
                </div>
          <div class="vc_row wpb_row vc_row-fluid " style="margin-bottom:3px;width: 100%;margin-left: 0%;">
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper card">
                   <a href="{{url::to('/bed-in-a-bag/the-bed-in-a-bag')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " style="margin-left: -4px;">
                     <img width="1492"  src="{{URL::to('categories/featured_images/'.$homepage_cat[0]->image)}}" alt="">
                    
                         <!-- <a class="button-banner button5" style="left: 7%;top:65%;" href="#" title="VIEW NOW" target="">EXPLORE
                         </a> -->
                    </figure></a>
                  </div>
                </div>

            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper card">
                  <a href="{{url::to('/diaper-bags')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " >
                      <img width="1492"  src="{{URL::to('categories/featured_images/'.$homepage_cat[1]->image)}}"  alt="">
                     
                   <!--   <a class="button-banner button5" style="left: 7%;top:65%;" href="#" title="VIEW NOW" target="">EXPLORE
                  </a> -->
                    </figure></a>
                  </div>
                </div>
            </div>
        </div>
  
               
              </div>
            </div>
      </main>
        </div>
        <div class="vc_row wpb_row vc_row-fluid " style="margin-bottom:3px;">
            <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper card">
                  <a href="{{url::to('/bedding/cot-bumpers')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " style="margin-left: -4px;">
                      <img width="1492"  src="{{URL::to('images/cot-bumper.jpg')}}" alt="">
                     
                         <!-- <a class="button-banner button5" style="left: 7%;top:65%;" href="#" title="VIEW NOW" target="">EXPLORE
                         </a> -->
                    </figure></a>
                  </div>
                </div>

            </div>
            <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper card">
                   <a href="{{url::to('/bedding/boy-bedding-collections')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " style="margin-left: 2px;">
                      <img width="1492"  src="{{URL::to('images/boybedding.jpg')}}" alt="">
                     
                   <!--   <a class="button-banner button5" style="left: 7%;top:65%;" href="#" title="VIEW NOW" target="">EXPLORE
                  </a> -->
                    </figure></a>
                  </div>
                </div>
            </div>
            <!-- three -->
             <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper card">
                   <a href="{{url::to('/bedding/pillow')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " style="margin-left: 4px;">
                      <img width="1492"  src="{{URL::to('images/pillow.jpg')}}" alt="">
                     
                   <!--   <a class="button-banner button5" style="left: 7%;top:65%;" href="#" title="VIEW NOW" target="">EXPLORE
                  </a> -->
                    </figure></a>
                  </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid " style="margin-bottom:3px;margin-left: -19px !important;">
        <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper card">
                    <a href="{{url::to('/gifts')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " >
                      <img width="1492"  src="{{URL::to('web-asset/img/baby-banner.jpg')}}" class=" attachment-full size-full" alt="">
                      
                    </figure>
                  </a>
                  </div>
                </div>
            </div>
            </div>
        <div class="vc_row wpb_row vc_row-fluid " style="margin-bottom:3px;">
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper">
                    <a href="{{url::to('/gifts')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " style="margin-left: -4px;">
                      <img width="1492"  src="{{URL::to('categories/featured_images/'.$homepage_cat[2]->image)}}" class=" attachment-full size-full" alt="" >
                     <!--  <figcaption style="width: 98%;">
                        <h4 class="secondary-caption " style="left: 7%;top:35%;">
                              <span style="color: #ffffff;font-size:40px;/* font-style:italic; */font-family: 'Lato', sans-serif;font-weight: 400;">

                                    Category6
                                  
                                </span>
                            </h4>
                      </figcaption> -->
                         <!-- <a class="button-banner button5" style="left: 7%;top:65%;" href="#" title="VIEW NOW" target="">EXPLORE
                         </a> -->
                    </figure>
                  </a>
                  </div>
                </div>

            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  " >
                     <img id="myImg" src="{{URL::to('/web-asset/img/video.jpg')}}" alt="Trolltunga, Norway" class=" attachment-full size-full" alt="" style="margin-left: 5px;">
                      <!-- <img width="1492"  id="myImg" src="{{URL::to('/web-asset/img/babyvideo.jpg')}}" class=" attachment-full size-full" alt="" style="margin-left: 5px;"> -->
                      
                    </figure>
                  </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid ">
            <div class="br-wrap-title  vc_custom_1478554553186">
                      <h2 style="font-size: 26px;text-align: center;font-family: 'Oswald', sans-serif;font-weight:400;font-style:normal;    margin-top: 30px;" class="br-title ">
                        <span>OUR STORY
                        </span>
                      </h2>
            </div>

            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper ">
                    <div class="story_paragraph">
                        <P>
                                         At Baby Jalebi we know babies!
Founded by two mothers, we understand how taking care of your little baby is so important and are conscious of your feelings while preparing for their arrival. 
The heart of Baby Jalebi lies in its research and innovation towards creating new products that merge seamlessly into your baby’s life making everything just a little more comfortable, fun and easier for the new mother and baby. 
 
We began making and selling our products with limited prints on a small scale and before we knew it Baby Jalebi had become so much bigger than we had imagined.
 
                        </P>
                    </div>
                  </div>
                </div>

            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner vc_custom_1486566105119">
                  <div class="wpb_wrapper">
                    <a href="{{URL::to('/about-us')}}">
                    <figure class="brideliness-banner banner-with-effects effect-bubba2  ">
                      <img width="1492"  src="{{URL::to('web-asset/img/image8.png')}}" class=" attachment-full size-full" alt="">
              </figure></a>
                  </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1478537271174 vc_row-has-fill">


        </div>
         <div class="vc_row wpb_row vc_row-fluid vc_custom_1478537271174 vc_row-has-fill" style="margin-top: 32px">
            <!--  <div class="testimonialtop" >
                <img src="{{URL::to('web-asset/img/mombaby.png')}}">
                 
             </div>
             <div class="paratop">
                <p>
                 As a first time mom I was overwhelmed with the idea of going out with my newborn baby. But the Bed in a bag made it really easy.It's compact ,light &amp; easy and clean.
                 </p>
                 <div class="usertesti">
                    <hr class="hrtesti">
                    <span class="spantesti">Diana Grey</span>
                    <hr class="hrtesti">
                 </div>
             </div>
             <img src="{{URL::to('web-asset/img/image9.png')}}"> -->

<div id="first-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
        
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
        </ol>
       
        <div class="carousel-inner" role="listbox">
         
            <div class="item active slide1">
               <!--  <div class="row"><div class="container">
                    <div class="col-md-12 text-center">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="{{URL::to('web-asset/img/mombaby.png')}}">
                    </div>
                    <div class="col-md-12 text-left">
                        <h3 data-animation="animated bounceInDown">As a first time mom I was overwhelmed with the idea of going out with my newborn baby. But the Bed in a bag made it really easy.It's compact ,light &amp; easy and clean.</h3>
                            
                     </div>
                </div></div> -->
                <img src="{{URL::to('web-assets/images/testimonial1.png')}}">
             </div> 
           
            <div class="item slide2">
                <!-- <div class="row"><div class="container">
                <div class="col-md-12 text-center">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="{{URL::to('web-asset/img/mombaby.png')}}">
                    </div>
                    <div class="col-md-12 text-left">
                       <h3 data-animation="animated bounceInDown">As a first time mom I was overwhelmed with the idea of going out with my newborn baby. But the Bed in a bag made it really easy.It's compact ,light &amp; easy and clean.</h3>
                     </div>
                    
                </div></div> -->
                <img src="{{URL::to('web-assets/images/testimonial2.png')}}">
            </div>
           
            <div class="item slide3">
                <!-- <div class="row"><div class="container">
                  <div class="col-md-12 text-center">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="{{URL::to('web-asset/img/mombaby.png')}}">
                    </div> 
                    <div class="col-md-12 text-left">
                       <h3 data-animation="animated bounceInDown">As a first time mom I was overwhelmed with the idea of going out with my newborn baby. But the Bed in a bag made it really easy.It's compact ,light &amp; easy and clean.</h3>
                     </div>
                      
                </div></div> -->
                <img src="{{URL::to('web-assets/images/testimonial3.png')}}">
            </div>
           
            <div class="item slide4">
                <!-- <div class="row"><div class="container">
                 <div class="col-md-12 text-center">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="{{URL::to('web-asset/img/mombaby.png')}}">
                    </div> 
                    <div class="col-md-12 text-left">
                       <h3 data-animation="animated bounceInDown">As a first time mom I was overwhelmed with the idea of going out with my newborn baby. But the Bed in a bag made it really easy.It's compact ,light &amp; easy and clean.</h3>
                     </div>
                    
                </div></div> -->
                <img src="{{URL::to('web-assets/images/testimonial4.png')}}">
            </div>
            <div class="item slide4">
                <!-- <div class="row"><div class="container">
                 <div class="col-md-12 text-center">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="{{URL::to('web-asset/img/mombaby.png')}}">
                    </div> 
                    <div class="col-md-12 text-left">
                       <h3 data-animation="animated bounceInDown">As a first time mom I was overwhelmed with the idea of going out with my newborn baby. But the Bed in a bag made it really easy.It's compact ,light &amp; easy and clean.</h3>
                     </div>
                    
                </div></div> -->
                <img src="{{URL::to('web-assets/images/testimonial5.png')}}">
            </div>
           
    
        </div>
        
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
        </a>
    </div>
</div> 
         </div>


        <div class="vc_row wpb_row vc_row-fluid vc_custom_1478537271174 vc_row-has-fill">
        <br>
          <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="br-wrap-title  vc_custom_1478554553186">
                      <h2 style="font-size: 26px;text-align: center;font-family:'Oswald';font-weight:400;font-style:normal; margin-top: 30px;" class="br-title ">
                        <span>INSTAGRAM FEED
                        </span>
                      </h2>
                </div>
            <div  class="brideliness-woo-shortcode home-2-cols with-slider" id='owl593f341ca861c'>
                  <div class='title-wrapper'>
                  </div>
                  <div class="woocommerce columns-4">
                    <ul class="products">
                    <?php $count = 0; ?>
                    @foreach($instagrams as $insta)
                    <?php if($count == 4) break; ?>
                      <li class="annoushka balenciaga grid-view  col-xs-12 col-md-3 col-sm-3 post-99 product type-product status-publish has-post-thumbnail product_cat-jewellery product_cat-jewellery-new product_cat-pre-fall-2017 first instock sale featured shipping-taxable purchasable product-type-variable has-default-attributes has-children">
                        <div class="product-wrapper">
                        <a href="{{$insta->link}}">
                          <div class="product-img-wrapper" target="_blank">
                            <!-- <a href="../product/woo-single-2/index.html" class="woocommerce-LoopProduct-link">       -->
                              <div class="background-img-product">
                              </div>
                             <!--  <span class="onsale">Top Pick
                              </span> -->
                              <img width="265" height="365" src="{{$insta->images->standard_resolution->url}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="product2" title="product2" />
                             <!--  <a href="../index2d20.html?action=yith-woocompare-add-product&amp;id=99" class="compare" data-product_id="99" rel="nofollow">Add to Compare
                              </a> -->
                           <!--    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-99">
                                <div class="yith-wcwl-add-button hide" style="display:none">
                                  <a href="index5f9c.html?add_to_wishlist=99" rel="nofollow" data-product-id="99" data-product-type="variable" class="add_to_wishlist" >
                                    SAVE FOR LATER
                                  </a>
                                  <img src="../wp-content/plugins/yith-woocommerce-wishlist/assets/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
                                </div>
                                <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                  <span class="feedback">Product added
                                  </span>
                                  <a href="../wishlist/index.html" rel="nofollow">
                                    Browse Wishlist         
                                  </a>
                                </div>
                                <div class="yith-wcwl-wishlistexistsbrowse show" style="display:block">
                                  <span class="feedback">
                                  </span>
                                  <a href="../wishlist/index.html" rel="nofollow">
                                    Browse Wishlist         
                                  </a>
                                </div>
                                <div style="clear:both">
                                </div>
                                <div class="yith-wcwl-wishlistaddresponse">
                                </div>
                              </div> -->
                              <div class="clear">
                              </div>        
                              </div>
                            </a>        

                        </div>
                      </li>
                      <?php $count++; ?>
                      @endforeach
                     
                    </ul>
                  </div>
                </div>
          </div>
        </div>
        
      </div>
      <!-- .entry-content -->
    </main>
    <!--.site-content-->
  </div>
</div>
<!-- Content wrapper (end)-->
<script>
   $( '#myModal').on('click', function(event) {
    $("#myModal").hide();

});
    // $("#myModal").hide();
  
</script>

<script type="text/javascript">
    // Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var img1 = document.getElementById('myImg1');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    // captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  ytplayer = document.getElementById("img01");
	ytplayer.stopVideo();
}

</script>
<script type="text/javascript">
	 $(".close1").click(function() {
        jQuery.each($("iframe"), function() {
            $(this).attr({
                src: $(this).attr("src")
            });
        });
        return false;
    });
</script>
<!-- <script type="text/javascript">
	 jQuery(".close").click(function() {
      // changes the iframe src to prevent playback or stop the video playback in our case
      $('#utube').each(function(index) {
        $(this).attr('src', $(this).attr('src'));
        return false;
      });

</script> -->
<script type="text/javascript">






</script>
@endsection

