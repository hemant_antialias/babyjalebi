@extends('web-files.web_layout')

@section('content')
<style type="text/css">
	#add_address_form{
		margin-left: 3%;
		margin-right: 3%;
		margin-bottom: 3%;
	}
	#note_add_address{
		width: 100%;
    height: 140px;
    text-align: center;
   
    
	}
	#note_add_address:hover{
		border-color: #d8920c;
	}
	#add_address_address{
		width: 100%;
		border: #a6a6a6 1px solid;
	}
	.col-sm-4 input[type='text']{
		border-radius: 8px;
		width: 100%;
		text-align: center;
	}
	#user_img_row{
		margin-top: 40px;
		margin-left: auto;
	}
	.col-sm-4 input[type='text']
	{
		border: #a6a6a6 1px solid;
	}
	.col-sm-4 input[type='text']:hover{
		border-color: #d8920c;
	}
	.fa-user-circle{
		color: #d8920c;
	}
	.sub_btn_address{
		text-align: center;
	}
	#address_submit_btn{
		width: 30%;
		background: #3a3a42;
		height: 50px;
		border-color: #646a6f;
	}
	#address_submit_btn:hover {
		background: #d8920c;
		border-color: #d8920c;
		color: black;
	}
	.foot {
    border-top: 1px solid #999999;
    position:fixed;
    width: 600px;
    z-index: 10000;
    text-align:center;
    height: 500px;
    font-size:18px;
    color: #000;
    background: #FFF;
    display: flex;
    justify-content: center; /* align horizontal */
    border-top-left-radius:25px;
    border-top-right-radius:25px;
    right: 0;
    left: 0;
    margin-right: auto;
    margin-left: auto;
    bottom: -475px;
    display: none;
}

.slide-up
{
    bottom: 0px !important;
}

.slide-down
{
    bottom: -475px !important;
}
@media only screen and (max-width: 500px) {
    .foot {
        display: block;

    }
    .footstatic{
    	position: fixed;
    }
}

</style>
<script type="text/javascript">
	$(document).ready(function() {
  $('.foot').click(function() {
      if($('.foot').hasClass('slide-up')) {
        $('.foot').addClass('slide-down', 1000, 'easeOutBounce');
        $('.foot').removeClass('slide-up'); 
      } else {
        $('.foot').removeClass('slide-down');
        $('.foot').addClass('slide-up', 1000, 'easeOutBounce'); 
      }
  });
});

</script>
<div class="bootstrap-iso">
	<div class="row">
				<div class="col-xs-12 col-md-10" id="add_address_form">
						{{ Form::open(['url' => 'user/add-address', 'method' => 'post', 'class' => 'form-horizontal']) }}

						<div class="form-group" id="user_img_row">
							<i class="fa fa-user-circle fa-4x" aria-hidden="true"></i>
							{{Auth::user()->first_name}}  {{Auth::user()->last_name }} {{Auth::user()->email}}
							<!-- <a href="#">Log Out </a> -->

						</div>
							
						
							
						
						
						

						<div class="form-group">
							

							<div class="col-sm-4">
								<input type="text" name="address" id="add_address_address" placeholder="Address" />
							</div>
						
							

							<div class="col-sm-4">
								<input type="text" name="city" id="form-field-1" placeholder="City"/>
							</div>
						

						
							

							<div class="col-sm-4">
								<input type="text" name="zip" id="form-field-1" placeholder="Zip Code"/>
							</div>
						
						</div><!--form-group ends here -->
						
							<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-sm-4">
								<input type="text" name="state" id="form-field-1" placeholder="State" class="col-xs-10 col-sm-5" />
							</div>
						

						

							<div class="col-sm-4">
								<input type="text" name="country" id="form-field-1" placeholder="Country"/>
							</div>

							<div class="col-sm-4">
								<input type="text" name="company" id="form-field-1" placeholder="Company" />
							</div>
						</div><!--form-group ends here-->


						<!-- <div class="form-group">
							

							<div class="col-md-12">
								<input type="text" name="notes" id="note_add_address" placeholder="Enter the Note"/>
							</div>
						</div> -->

						<div class="form-group">
							

							<div class="col-sm-9" style="margin-left:25px;">
								<input type="checkbox" name="default_address" value="1" checked="checked" />
								Default
							</div>
							<div class="col-sm-9" style="margin-left:25px;">
								<input type="checkbox" name="billing_address" value="1" checked="checked" />
								Use as Billing
							</div>
						</div>
						
							<div class="col-md-12 sub_btn_address">
								<button class="btn btn-info" type="submit" id="address_submit_btn">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						
					{{ Form::close() }}
				</div>
				
			</div><!--row ends here -->
		

</div><!--bootstrap-iso ends here -->	
<div class="foot"> 
filters
</div>		
@endsection
