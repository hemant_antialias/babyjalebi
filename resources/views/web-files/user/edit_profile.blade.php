@extends('web-files.web_layout')

@section('content')

<style>
	.editpage
	{

    margin-left: auto;
    margin-right: auto;
    width: 40%;
	}
	input, textarea
	{
		width: 100%;
	}
</style>


@if(Session::has('success_msg'))
	<p class="succolor">{{Session::get('success_msg')}}</p>
@endif

<div class="row">

				<div class="col-xs-12 editpage" >
				<br>
				<h3>Edit Profile</h3>
						{{ Form::model($user, ['url' => '/user/edit-profile', 'method' => 'post', 'class' => 'form-horizontal']) }}
						

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> First Name </label>

							<div class="col-sm-9">
								<input type="text" name="first_name" id="form-field-1" placeholder="Name" class="col-xs-10 col-sm-5" value="{{$user->first_name}}" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Last Name </label>

							<div class="col-sm-9">
								<input type="text" name="last_name" id="form-field-1" placeholder="Name" class="col-xs-10 col-sm-5" value="{{$user->last_name}}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email </label>

							<div class="col-sm-9">
								<input type="text" name="email" id="form-field-1" placeholder="Email" class="col-xs-10 col-sm-5" value="{{$user->email}}"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact</label>

							<div class="col-sm-9">
								<input type="text" name="contact" id="form-field-1" placeholder="Contact" class="col-xs-10 col-sm-5" value="{{$user->contact}}"/>
							</div>
						</div>
							<br><br>
						
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
						<br>
						<br>
					{{ Form::close() }}
				</div>
				
			</div>
@endsection
