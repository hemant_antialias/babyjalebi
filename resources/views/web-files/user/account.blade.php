@extends('web-files.web_layout')

@section('content')
<?php  
use App\OrderRequest;
use App\Address;
use App\User;
use App\PaymentStatus;
?>
	<style type="text/css">
	.bootstrap-iso {margin-bottom: 40px;}
		th{
			font-size: small;
		}
		i {
			color: #efb60a;
		}
		h2{
			text-align: center;
		}
		#tabright{
			margin-top: 45px;
		}
		
	</style>
	<div class="alert alert-success">
		@if(Session::has('success_msg'))
			<p class="succolor">{{Session::get('success_msg')}}</p>
		@endif
	</div>
	<section class="bootstrap-iso">
<h2>Welcome {{Auth::user()->first_name}}</h2>
<div class="container">
	<div class="col-md-4">
		<i class="fa fa-user-o fa-4x" aria-hidden="true"></i>
		<h4>{{Auth::user()->firstname}}</h4>
		<p>
			{{$userdetail->email}}<br>
			@if(isset($address))
			{{$address->address}}<br>{{$address->city}}, 
			{{$address->state}}, {{$address->country}}<br><br>
			<a href="{{URL::to('/user/edit-address/'.$address->id)}}">EDIT</a>
			@endif
			
		</p><br>
		<h5><a href="{{URL::to('user/add-address')}}" style="color: #000; font-size: 12px; margin-bottom: 20px;display: inline-block; "> + ADD NEW ADDRESS</a></h5>
		
		<h5><a href="{{URL::to('user/edit-profile')}}" style="color: #000; font-size: 12px; margin-bottom: 20px;display: inline-block;">EDIT PROFILE</a></h5>
			
		<h5><a href="{{URL::to('user/settings')}}" style="color: #000; font-size: 12px; margin-bottom: 20px;display: inline-block;">CHANGE PASSWORD</a></h5>
		
	</div>
	@if(count($orderlist_byuserid) > 0)
	<div class="col-md-8" id="tabright">
		<table class="table">
    <thead>
      <tr>
        <th>Order</th>
        <th>Date</th>
        <th>Payment Status</th>
        <th>Order Status</th>
        <th>Sub Total</th>
      </tr>
    </thead>
    <tbody>
   	
    @foreach($orderlist_byuserid as $orderlist)
      <tr>
         @if($orderlist->vendor_id == 1)
                                    <td>
                                        D{{$orderlist->id}}
                                    </td>
                                    @else
                                    <td>
                                        G{{$orderlist->id}}
                                    </td>
                                    @endif
                                    
        <td>{{Carbon\Carbon::parse($orderlist->created_at)->toDateString()}}</td>
        <td>{{$orderlist->payment_status_name}}</td>
        <td>{{$orderlist->status_title}}</td>
        <td>Rs. {{$orderlist->payble_amount}}</td>
      </tr>
      @endforeach

      
    </tbody>

  </table>
  <div style="float: right;">
  {{ $orderlist_byuserid->links() }}
  </div>
	</div>
	@else
		<h5 id="norderyet">No Orders Yet.</h5>
	@endif
 </div>
	
</section>

@endsection