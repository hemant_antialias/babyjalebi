@extends('web-files.web_layout')
<?php
use App\Product;
?>
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
@section('content')
<div id="main" class="site-main container">
  <div class="row">
<main id="content" class="site-content col-xs-12 col-md-12 col-sm-12" itemscope="itemscope" itemprop="mainContentOfPage">
      <div class="entry-content">
				
				<?php for ($i=0; $i < count($subcategories); $i++) { ?>

						

						@if($i%2==0)
						<div class="vc_row wpb_row vc_row-fluid ">

				        	<div class="wpb_column vc_column_container vc_col-sm-12">
				        		<div class="vc_column-inner vc_custom_1486566105119">
								  <div class="wpb_wrapper">
								  <a href="{{URL::to('/'.$categoryslug.'/'.$subcategories[$i]->slug)}}">
								    <figure class="brideliness-banner banner-with-effects effect-bubba2  ">
								      <img width="1492" height="189" src="{{URL::to('/categories/featured_images/'.$subcategories[$i]->image)}}" class="home_page_img2 attachment-full size-full" alt="">
								     
									  <!--   <a class="button-banner button5" style="left: 38%;top:65%;" href="#" title="VIEW NOW" target="">VIEW NOW
                					    </a> -->
								    </figure></a>
								  </div>
								</div>

				        	</div>
				        	<!-- <div class="wpb_column vc_column_container vc_col-sm-4">
				        		<div class="paragraph">
				        			@if($categoryslug == 'diaper-bags')
								       <?php
								       $productslug = Product::where('product_category' , '=' ,$subcategories[$i]->id)->first();
								       // dd($productslug->slug);
								       ?>
								        @if(isset($productslug->slug))
                                                <?php
                                                $urlfor = URL::to('diaper-bags/'.$subcategories[$i]->slug.'/'.$productslug->slug);
                                                ?>
                                                @else
                                                <?php
                                                $urlfor = '#';
                                                ?>
                                                @endif
				        			<a href="<?php echo $urlfor; ?>"><h3 class="para_heading">{{$subcategories[$i]->name}}</h3></a>
				        			 @else
				        			<a href="{{URL::to('/'.$categoryslug.'/'.$subcategories[$i]->slug)}}"><h3 class="para_heading">{{$subcategories[$i]->name}}</h3></a>
								        
								         @endif
				        			<p>

				        				 {!!$subcategories[$i]->description!!}
				        			</p>
				        		</div>
				        	</div> -->
				        </div>
				        @endif

				        @if($i%2==1)

				        <div class="vc_row wpb_row vc_row-fluid ">
				        	<!-- <div class="wpb_column vc_column_container vc_col-sm-4">
				        		<div class="paragraph">
				        		@if($categoryslug == 'diaper-bags')
								       <?php
								       $productslug = Product::where('product_category' , '=' ,$subcategories[$i]->id)->first();
								       // dd($productslug->slug);
								       ?>
								        @if(isset($productslug->slug))
                                                <?php
                                                $urlfor = URL::to('diaper-bags/'.$subcategories[$i]->slug.'/'.$productslug->slug);
                                                ?>
                                                @else
                                                <?php
                                                $urlfor = '#';
                                                ?>
                                                @endif
		        			<a href="<?php echo $urlfor; ?>"><h3 class="para_heading"><h3 class="para_heading">{{$subcategories[$i]->name}}</h3></h3>
		        			 @else
				        			<a href="{{URL::to('/'.$categoryslug.'/'.$subcategories[$i]->slug)}}"><h3 class="para_heading"><h3 class="para_heading">{{$subcategories[$i]->name}}</h3>
								        
								         @endif
				        			<p>
				        				 {!!$subcategories[$i]->description!!}
				        			</p>
				        		</div>
				        	</div> -->
				        	<div class="wpb_column vc_column_container vc_col-sm-12">
				        		<div class="vc_column-inner vc_custom_1486566105119">
								  <div class="wpb_wrapper">
								  <a href="{{URL::to('/'.$categoryslug.'/'.$subcategories[$i]->slug)}}">
								    <figure class="brideliness-banner banner-with-effects effect-bubba2  ">

								      <img width="1492" height="189" src="{{URL::to('/categories/featured_images/'.$subcategories[$i]->image)}}" class="home_page_img2 attachment-full size-full" alt="">
								    <!--   <figcaption>
								        <h4 class="secondary-caption " style="left: 30%;top:35%;">
								          <span style="color: #ffffff;font-size:40px;FONT-WEIGHT: BOLD;/* font-style:italic; */font-family: 'LATO', SANS-SERIF;">
								            
								          </span>
								        </h4>
								      </figcaption> -->
								      <!-- <a class="button-banner button5" style="left:38%; top:65%;" href="#" title="VIEW NOW" target="">VIEW NOW
                 					 </a> -->
								    </figure></a>
								  </div>
								</div>

				        	</div>
				        	
				        </div>
				        @endif
				<?php } ?>   	
				</div>
	</main>
	</div>
	</div>

@endsection