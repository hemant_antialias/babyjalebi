@extends('web-files.web_layout')

@section('content')
<style>
.error400price{ color: #f35656 }
.deleteprocart:hover{ color: #d8920c; }
.rowcart:hover{ border-top: 1px dashed #d8920c;
border-bottom: 1px dashed #d8920c; }

.rowcart:hover #cartimgproduct{ border: 1px dashed #d8920c; }
.cartnumb{ width: 65px;
    height: 40px; }
.martopcolcart{
      margin-top: 1%;
    margin-bottom: 1%;
}
.rowcart{    padding-top: 10px;
    padding-bottom: 10px;
    border-top: 1px dashed gainsboro;
    border-bottom: 1px dashed gainsboro;
    width: 100%;
}
#cartimgproduct{ border: 1px dashed gainsboro; padding: 3px; }
  .glyphicon { margin-right:5px; }
  /*.cartnumb{ width: 25%; }*/
.thumbnail
{
    margin-bottom: 20px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #428bca;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}
.product_qty
{
  width: 24%;
  margin-left: -19%;
}
.cartheading{
  margin-top: 5%;
}
@media only screen and ( max-width: 600px){
.rowcart {position: relative;}
  i.fa.fa-2x.fa-times-circle-o.deleteprocart {position: absolute; top: 3px; right: 5px;}
  .prodSpecial {float: left;}
  .formSpecial {float: left !important;}
  .widt_20 {width: 200px; float: right !important;} 
}

@media only screen and ( max-width: 520px){
  .error400price{     width: 100%;
    color: #f35656;
    float: left;
    margin-left: 33%; }
  .prodname{
        font-size: 10px;
    line-height: 1.3;
    padding-top: -28px;
    margin-top: -35px;
    padding-bottom: 17px;
  }
  .cartnumb{
    width: 41px;
    height: 23px;
  }
  .martopcolcart{ margin-top: 0%; }
}

</style>


<div class="container specialCase" style="margin-bottom: 10%;">
  <?php
    $cart_content = Cart::content();
    $cart_total = Cart::subtotal('2', '.', '');

    $price = 0; 
    $ids = 1;
  ?>
  @if(count($cart_content) > 0)
    <div id="products" class="taroset row list-group">
          <div class="align-center cartheading">
             
             <h2 class="cartSpecialcase" style="text-align: center;">YOUR SHOPPING CART(<?php echo count($cart_content); ?> Items)</h2>
          </div>
          
        <div class="item  col-md-12">
            <div width="200" border="1" id="cart-content">
  <!-- <tr class="resphead">
    <div>Image</div>
    <div>Title</div>
    <div>Product Price</div>
    <div>Quantity</div>
    <div>Price</div>
    <div>Delete</div> 
  </tr>
 -->
 <span id="error" class="animated fadeOut" style="color: red"></span>
  <div class="bootstrap-iso SpecialCase1">
  <?php
    $i = 1;
  ?>
   <div  class=" row" style="border: 1px #efefef solid; margin-right: 15px;">
                <h4 class=" col-md-2 col-xs-3 martopcolcart" >
                 PRODUCTS
                </h4>
                <h4 class=" col-md-3 col-xs-7 martopcolcart"></h4>
                <h4 class=" col-md-2 col-xs-3 martopcolcart">PRICE</h4>
                 <h4 class=" col-md-2 col-xs-3 martopcolcart"> QUANTITY
                 </h4>
                 <h4 class="amount col-md-2 col-xs-3 martopcolcart" id="">PRICE
                 </h4>
                 </div>
                 <?php
                 $items = array();
                 ?>
  @foreach($cart_content as $product)
  <?php
    $id = $i++;
  ?>
        <div  class=" row rowcart " style="border: 1px #efefef solid;">
                <div class=" col-md-2 col-xs-3 " >
                  @if($product->options->image_url == null)
                  <?php 
                  $relatedproducts = DB::table('product')
                                                ->where('product_title', $product->name)
                                                ->first();
                  ?>
                  <img class="group list-group-image" src="{{URL::to('/product_images/'.$relatedproducts->id.'/featured_images/'.$relatedproducts->product_images)}}" width="100" height="80" id="cartimgproduct" />
                  @else
                  <img class="group list-group-image" src="{{$product->options->image_url}}" width="100" height="80" id="cartimgproduct" />
                  @endif
                  <br>
                   @if($product->options->pattern)
                  Pattern:{{$product->options->pattern}}
                  @endif
                  <br>
                   @if($product->options->ptext)
                  Text:{{$product->options->ptext}}
                  <br>
                  @endif
                   @if($product->options->font)
                  Font:{{$product->options->font}}
                  <br>
                  @endif
                   @if($product->options->color)
                  Color:{{$product->options->color}}
                  @endif

                </div>
                <div class=" col-md-3 col-xs-7 martopcolcart">{{$product->name}}</div>

                <div class=" col-md-2 col-xs-3 martopcolcart">Rs.{{$product->price}}</div>
                 <div class=" col-md-2 col-xs-3 martopcolcart"> <input type="number" class="cartnumb" min="1" value="{{$product->qty}}" onchange="saveCart(this);" onkeypress="return isNumberKey(event)" id="{{$product->rowId}}" oninput="validity.valid||(value='');" data-pid="{{$product->id}}" data-amountid="{{'amount_'.$id}}" class="product_qty" size="3"/>
                 </div>
                 <input type="hidden" name="textname" id="textname" value="{{$product->options->ptext}}">
                 <div class="amount col-md-2 col-xs-3 martopcolcart" id="{{'amount_'.$id}}">Rs. @if($product->options->ptext)
                 <?php
                 $staticprice = $product->qty*250;
                 ?>
                 {{($product->qty*$product->price + $staticprice)}}
                 <?php
                 $items[] = $staticprice;
                 ?>
                 @else
                 {{($product->qty*$product->price)}}
                 @endif
                 </div>
                 <div class="delete_product" data-id="{{$product->rowId}}" align="center" style="cursor: pointer;"><i class="fa fa-2x fa-times-circle-o deleteprocart" aria-hidden="true" ></i>
                </div>
                 <?php $price = $product->price+$price; ?>
                <input type="hidden" value="{{$product->price}}">
        </div >

    @endforeach
    <br>
    <a href="http://{{($product->options->fullurl)}}"><h5 class="cartSpecialcase">CONTINUE SHOPPING</h5></a>
    <?php
    $persnlization = array_sum($items);
    // $persnlization = $countval * 250;
?>
 </div>
</div>
<div class="fewmore">

  <span class="prodtotalprice prodtotpricmob" style="float: right;">Total Price :Rs. <span class="sub_total_price" style="font-weight: bold;">  {{$cart_total + $persnlization}}</span></span>


<input type="hidden" value="<?php echo $price; ?>" class="sub_total_price_value" />
<input type="hidden" name="subtotal" id="subtotal" value="<?php echo $price; ?>"/>
<p class="error400price" style="">{{Session::get('message')}}</p>

<p class="error400price" style="">
@if(Session::has('oot_message'))
  {{Session::get('oot_message')}}
@endif
<p>

<!-- <div id="discount_div"><input type="text" name="discout" id="discout" placeholder="Enter Discount Code" /> <button type="submit" name="apply" class="apply">Apply</button></div><br /> -->
<br><br>
<form  class="formSpecial"  method="post" action="{{URL::to('/order/new')}}" style="float: right;">
      <input type="hidden" class="discounted_amount" name="discount_amount">
      <input type="hidden" class="discounted_coupon_code" name="discounted_coupon_code">
      <input type="hidden" class="discounted_amount" name="totalamnt" value="{{$cart_total + $persnlization}}">
      <!-- Notes: <input type="text" name="notes"> -->
      <button class="prodcheckout" type="submit" name="checkout" style="">Checkout</button>
      </form>
       @if(Auth::check())
  <form  class="formSpecial widt_20"  method="post" action="{{URL::to('/guestorder/new')}}" style="float: right; display: none">
  &nbsp;&nbsp;&nbsp;&nbsp;
      <input type="hidden" class="discounted_amount" name="discount_amount">
      <input type="hidden" class="discounted_coupon_code" name="discounted_coupon_code">
      <input type="hidden" class="discounted_amount" name="totalamnt" value="{{$cart_total + $persnlization}}">
      <!-- Notes: <input type="text" name="notes"> -->
      <button class="prodcheckout" type="submit" name="checkout" style="">Checkout  as a guest user</button>&nbsp;&nbsp;&nbsp;&nbsp;
      
     
    </form> 
    @else
     <form  class="formSpecial widt_20"  method="post" action="{{URL::to('/guestorder/new')}}" style="float: right;">
  &nbsp;&nbsp;&nbsp;&nbsp;
      <input type="hidden" class="discounted_amount" name="discount_amount">
      <input type="hidden" class="discounted_amount" name="totalamnt" value="{{$cart_total + $persnlization}}">
      <input type="hidden" class="discounted_coupon_code" name="discounted_coupon_code">
      <!-- Notes: <input type="text" name="notes"> -->
      <button class="prodcheckout" type="submit" name="checkout" style="">Checkout  as a guest user</button>&nbsp;&nbsp;&nbsp;&nbsp;
      
     
    </form> 
    @endif
    <p class="prodiscount prodSpecial" style="margin-top:10px;">Discount code can be applied on the next page</p>
     @if(Session::has('message'))
     <br><br>
       
      @endif
</div>
<div class="discount-message"></div>

        <div style="width: 200px;"> 
    <!-- <span class="discount-value-text"> Discount Value: Rs. </span> -->
    <!-- <span class="discount-value"> </span> -->
    <!-- <span class="close" style="font-size:18px; cursor: pointer;">X</span><br> -->
    <!-- <span class="total_price_text">Total Price: Rs. </span> <span class="total_price1"></span> -->
     <!-- <div class="total_price_without_discount">Total Price:Rs. {{$cart_total}}</div> -->
    </div>
    <input type="hidden" class="total-after-discount">
    </div>
    </div>
    @else
    <br><br>
  
  <h1 style="text-align: center;">YOUR CART</h1>
  <p style="text-align: center;">Your cart is currently empty.</p>
 
@endif
</div>
<script>
  $(document).ready(function() {
      $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
      $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
  });
  var base_url = '{{URL::to('/')}}';
    // $(document).ready(function() {
    // $(".total_price_text").hide();        
    // $(".total_price_without_discount").show();
    // $(".discount-value-text").hide();
    // $(".close").hide();
    // $('.apply').click(function(){
    //   var subtotal = $("#subtotal").val();
    //   var discout = $("#discout").val();
    //   var subtotal = $(".sub_total_price").html();
      
    //   $.ajax({
    //     url: base_url+'/check-discount',
    //     type: "post",
    //     data: 
    //     {
    //     "discout": discout,
    //     "subtotal": subtotal,
    //     },
    //     success:function(data)
    //     {
    //       if(data.status==1){
    //       var discountval = $('.discount-value').html(data.discount_value);
    //       $('.discounted_amount').val(data.discount_value);
    //       $('.discounted_coupon_code').val(data.coupon_code);
    //       $("#discount_div").hide();
    //       $(".total_price_text").show();        
    //       $(".discount-message").show();
    //       $(".discount-value-text").show();
    //       $(".total_price_without_discount").hide();
    //       $(".close").show();
    //       }
    //       $(".discount-message").show();
    //       $('.discount-message').html(data.message);
    //       var total_price = data.after_discount_price;
    //       $(".total_price1").html(total_price);
    //     }
    //   });
    // }); 
    
    // $('.close').click(function(){
    // $(".discount-value-text").hide();   
    // $(".total_price_text").hide();
    // $(".total_price_without_discount").show();
    // $(".discount-message").hide();
    // $(".close").hide();
    // $("#discount_div").show();
    // $("#discout").val("");
    //   $(".discount-value").empty(); 
    //   $(".total_price1").empty();       
    // });
    
    
    });
</script>
<script>
  $('.delete_product').click(function(){
    var id = $(this).attr('data-id');
 var base_url = '{{URL::to('/')}}';
    $.ajax({
        url: base_url+'/delete-product',
        type: "post",
        data: 
        {
              "id": id,
          },
        success:function(data)
        {
          if (data.status == 1) {
            $('.sub_total_price').html(data.subtotal);
            $('.total_price_without_discount').html(data.subtotal);
          var rowindex = $(this).closest('div').attr('data-id');
              $(".rowcart").eq(rowindex).remove();
              location.reload();

          }
      }
    });
    
  });

  function saveCart(obj) {
    var base_url = "{{URL::to('/')}}";
    var quantity = $(obj).val();
    var id = $(obj).attr("id");
    var amount_id = $(obj).attr("data-amountid");
    var product_id = $(obj).attr("data-pid");
    
    $.ajax({
      url: base_url+'/update-cart',
      type: "POST",
      data: 'id='+id+'&quantity='+quantity+'&pid='+product_id,
      success: function(data, status){
          if (data.status==0) {
            $(obj).val(data.quantity);
            $('#'+amount_id+'').html('Rs.'+data.total_product_price);
            // alert(data.message);
            $('#error').html(data.message);
          }

          if (data.status == 1) {
            $(".sub_total_price").html(data.subtotal)
            $('.total_price_without_discount').html(data.subtotal);
            $('#'+amount_id+'').html('Rs.'+data.total_product_price);
          }
      }
    });

  }
</script>
<script>
  $(".cartnumb").on("click", function () {
   $(this).select();
});


</script>
<script>
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    return !(charCode > 31 && (charCode < 49 || charCode > 57 || charCode < 48));
}
</script>
@endsection