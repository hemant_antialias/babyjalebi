@extends('web-files.web_layout')
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> @section('content')
  <link href='https://fonts.googleapis.com/css?family=Myraid' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Tahoma' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Seoge' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="{{URL::to('web-assets/css/rating.css')}}">
        <script src="{{URL::to('web-assets/js/rating.js')}}"></script> 
    <script>
function myFunction() {
    document.getElementById("demo").innerHTML = "Paragraph changed.";
}
</script>

<style>
@font-face {
    font-family: 'baskerville-regularregular';
    src: url('{{URL("web-assets/fonts/baskerville-regular-webfont.woff2")}}') format('woff2'),
         url('{{URL("web-assets/fonts/baskerville-regular-webfont.woff")}}') format('woff');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'American Typewriter';
    src: url('AmericanTypewriter.eot');
    src: url('{{URL("web-assets/fonts/AmericanTypewriter.eot?#iefix")}}') format('embedded-opentype'),
        url('{{URL("web-assets/fonts/AmericanTypewriter.woff")}}') format('woff'),
        url('{{URL("web-assets/fonts/AmericanTypewriter.ttf")}}') format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'Edwardian Script ITC';
    src: url('EdwardianScriptITC.eot');
    src: url('{{URL("web-assets/fonts/EdwardianScriptITC.eot?#iefix")}}') format('embedded-opentype'),
        url('{{URL("web-assets/fonts/EdwardianScriptITC.woff")}}') format('woff'),
        url('{{URL("web-assets/fonts/EdwardianScriptITC.ttf")}}') format('truetype');
    font-weight: normal;
    font-style: normal;
}
 #text{text-align: center;
        margin: 100px;
        font-size: 45px;
        font-family: sans-serif;}

    .wishlist_table .add_to_cart,
    a.add_to_wishlist.button.alt {
        border-radius: 16px;
        -moz-border-radius: 16px;
        -webkit-border-radius: 16px;
    }
    .fontbutn
    {
        background: none;
    border: #cfcbc8 1px solid;
    color: #a7a4a2;
    }
.pttrnbox
{
    padding: 10px;
    line-height: 3.5;
    vertical-align: top;
    border: 1px solid #e1e1e1;
    margin-right: 2%;
    -webkit-box-shadow: -1px 1px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: -1px 1px 5px 0px rgba(0,0,0,0.75);
    box-shadow: -1px 1px 5px 0px rgba(0, 0, 0, 0.36);
}
.slider_plus {position: relative;}

.printchatbox
{
   text-transform: uppercase;
    position: absolute;
    left: 50%;
    top: 22%;
    z-index: 99;
    width: auto;
    font-size: 25px;
    background: #000;
    color: #fff;
    padding: 3px 20px;
}
.pttrnbox:hover
{
       padding: 10px;
    line-height: 3.5;
    vertical-align: top;
    border: 1px solid #e1e1e1;
    margin-right: 2%;
    -webkit-box-shadow: -1px 1px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: -1px 1px 5px 0px rgba(0,0,0,0.75);
    box-shadow: -1px 1px 5px 0px rgba(0, 0, 0, 0.36);
    background: #e1e1e1;
    color: #fff;
}
.atvive
{
      padding: 10px;
    line-height: 3.5;
    vertical-align: top;
    border: 1px solid #e1e1e1;
    margin-right: 2%;
    -webkit-box-shadow: -1px 1px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: -1px 1px 5px 0px rgba(0,0,0,0.75);
    box-shadow: -1px 1px 5px 0px rgba(0, 0, 0, 0.36);
    background: #e1e1e1;
    color: #fff;
}
 #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */

    width: 100%; /* Full width */
    height: 100%;
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
   
}

.modal-body {
	position: absolute;
	width: 800px;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	-webkit-transform: translate(-50%, -50%);
	-moz-transform: translate(-50%, -50%);
}
/* Modal Content (Image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption {
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)}
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)}
    to {transform:scale(1)}
}
 
/* The Close Button */
.close {
    position: absolute;
    /*top: 15px;
    right: 35px;*/
    top: 0;
    right:10px;
    color: #f1f1f1 ;
    font-size: 45px !important;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #fff;
    text-decoration: none;
    cursor: pointer;
}
.owl-carousel .owl-wrapper-outer
{
        height: 386px;
}
.modal-backdrop
{
    display: none;
}
.modal-open
{
    overflow: auto !important;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
.find_location {float: right;}
.find_location input{
    background: url(http://ec2-13-126-180-167.ap-south-1.compute.amazonaws.com/web-assets/images/location.png) no-repeat;
       border-bottom: 2px solid #02c9c6;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    padding: 5px 20px;
    text-align: left;
    background-size: 13px;
    background-position: left center;}
</style>

<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
.success-review.succcolor {
    font-family: 'EB Garamond', serif;
    font-size: 26px;
    color: #000;
}
</style>
<link rel='stylesheet' id='css-0-css' href="{{URL::to('admin-assets/css/productdetail.min.css')}}" type='text/css' media='all' />

<script type='text/javascript' src="{{URL::to('admin-assets/js/productdetail2.min.js')}}"></script>

<!-- <link rel="stylesheet" href="{{URL::to('web-asset/css/productdetail.css')}}"> -->

</head>
<!-- success add product in cart -->
<div class="modal fade" id="myModal3">
    <div class="modal-dialog">
    
  
      <div class="modal-content">
  
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
       


<div class="modal-footer">
<img src="{{URL('/web-asset/img/Logo_BJ.png')}}">

      <p>Your product has been added
to your cart.</p>
<?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

 <a href="<?php echo $actual_link; ?>" class="continue_shop cont">Continue Shopping</a>
<a href="www.google.com" class="chec_btm">Checkout</a> 
       
<!-- <a href="www.google.com">ddsd</a> -->


        </div> 
      </div>
      
    </div>
  </div>
<!--  Site Header (start)-->

<div class="site-wrapper">
<div id="myModal" class="modal"> 

 <!-- The Close Button -->
  <span class="close" style="color: white !important" onclick="document.getElementById('myModal').style.display='block'">&times;</span>


 <!-- Modal Content (The Image) --> 
  <div id="img01">
  <!-- <a href="javascript:;" class="textright" id="close">Close</a> -->
    <div class="modal-body">
  <iframe id="cartoonVideo" width="854" height="480" src="{{$product->video_link}}" frameborder="0" allowfullscreen></iframe>
</div>
  </div>

</div>
    <!--  Site Header (end)-->

    <div class="page-title-description">
        <div class="overlay"></div>
        <div class="title-wrapper" style="background-image:url(http://brideliness.themes.zone/wp-content/uploads/2016/10/shop-banner.jpg);
                                              background-repeat:repeat;
                                              background-position:top center;
                                              background-attachment:fixed;
                                                background-color:">

        </div>
    </div>



    <!-- Content wrapper (start)-->
    <div id="main" class="site-main container product-template-default single single-product postid-96 woocommerce woocommerce-page mega-menu-primary-nav layout-one-col single_type_1 wpb-js-composer js-comp-ver-5.1.1 vc_responsive">
        <div class="row">

            <div class="container breadcrumb"> 
                    <div class="row">
                            <div class="col-md-12 col-sm-12 col-sx-12">
            <nav class="woocommerce-breadcrumb"><a href="{{URL('/')}}">Home</a><span class="delimeter"> &gt;</span><a href="#">{{$product->slug}}</a></nav>             </div></div></div>

            <div class="container">
                <div class="row">

                    <div id="content" class="site-content woocommerce columns-4 col-xs-12 col-md-12 col-sm-12" role="main">

                        <div id="product-96" class="post-96 product type-product status-publish has-post-thumbnail product_cat-couture product_cat-fall-2016 product_cat-spring-2017 product_tag-accessories product_tag-bridal product_tag-trend first instock sale featured downloadable taxable shipping-taxable purchasable product-type-simple">
                            <div class="slider_plus">
                            <!-- <div class='printchatbox' id='printchatbox' style="text-align: center;"></div> -->
                            <div class="images-wrapper vertical-thumbs">

                                <!-- <span class="onsale">Top Pick</span> -->

                                <div class="images">
                                    <div class="main-slider vertical-thumbs" data-owl="container" data-owl-slides="1" data-owl-type="with-thumbs" data-owl-transition="fade" data-magnific="container">

                                        @foreach($product_images as $images)

                                        <a href="{{URL::to('/product_images/'.$product->id.'/'.$images->attribute_value_id.'/'.$images->images)}}" itemprop="image" class="woocommerce-main-image" data-effect="mfp-move-from-top" title="Click Me">
                                            <img id="prodimg" width="530" height="750" src="{{URL::to('/product_images/'.$product->id.'/'.$images->attribute_value_id.'/'.$images->images)}}" class="attachment-shop_single size-shop_single wp-post-image" alt="" srcset="{{URL::to('/product_images/'.$product->id.'/'.$images->attribute_value_id.'/'.$images->images)}} 530w, {{URL::to('/product_images/'.$product->id.'/'.$images->attribute_value_id.'/'.$images->images)}} 213w" sizes="(max-width: 768px) 92vw, (max-width: 992px) 345px, (max-width: 1200px) 455px, 555px" />
                                            </a>

                                             @endforeach

                                    </div>
                                    
                                    <div class='slider-navi vertical-thumbs'><span class='prev'><i class='fa fa-arrow-circle-o-left' style="font-size: 36px;color: #000;"></i></span><span class='next'><i class="fa fa-arrow-circle-o-right" style="font-size: 36px;color: #000;"></i></span>
                                    </div>
                                    <div class="thumb-slider vertical-thumbs" data-owl-thumbs="container">
                                    
                                        @foreach($product_images as $images)
                                        <img width="85" height="130" src="{{URL::to('/product_images/'.$product->id.'/'.$images->attribute_value_id.'/'.$images->images)}}" class="attachment-brideliness-single-product-thumb size-brideliness-single-product-thumb wp-post-image" alt="" /> 

                                        @endforeach
                                        @if($product->video_link)
                                        
                                        <img id="myImg" src="{{URL::to('/web-asset/img/babyvideo.jpg')}}" alt="Trolltunga, Norway" width="300" height="200">
                                    
                                        @endif
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="woocommerce-tabs wc-tabs-wrapper">
                                <ul class="tabs wc-tabs" role="tablist">
                                    <!-- <li class="description_tab" id="tab-title-description" role="tab" aria-controls="tab-description">
                                        <a href="#tab-description">Description</a>
                                    </li>
                                    <li class="additional_information_tab" id="tab-title-additional_information" role="tab" aria-controls="tab-additional_information">
                                        <a href="#tab-additional_information">Additional information</a>
                                    </li> -->
                                    @if($product->overview)
                                    <li class="description_tab" id="overview1" role="tab" aria-controls="overview">
                                        <a href="#overview">Overview</a>
                                    </li>
                                    @endif @if($product->details_that_matter)
                                    <li class="description_tab" id="details_that_matter1" role="tab" aria-controls="details_that_matter">
                                        <a href="#details_that_matter">Details that matter</a>
                                    </li>
                                    <!-- @endif @if($product->safety_information)
                                    <li class="description_tab" id="safety_information1" role="tab" aria-controls="safety_information">
                                        <a href="#safety_information">Safety information </a>
                                    </li>
                                    @endif  -->
                                    @if($product->dimensions)
                                    <li class="description_tab" id="dimensions1" role="tab" aria-controls="dimensions">
                                        <a href="#dimensions">Dimensions</a>
                                    </li>
                                    @endif @if($product->care)
                                    <li class="description_tab" id="care1" role="tab" aria-controls="care">
                                        <a href="#care">Care</a>
                                    </li>
                                   <!--  @endif @if($product->our_commitment)
                                    <li class="description_tab" id="our_commitment1" role="tab" aria-controls="our_commitment">
                                        <a href="#our_commitment">Our commitment </a>
                                    </li>
                                    @endif @if($product->in_house_design)
                                    <li class="description_tab" id="in_house_design1" role="tab" aria-controls="in_house_design">
                                        <a href="#in_house_design">IN ouse design </a>
                                    </li>
                                    @endif -->

                                    <li class="reviews_tab" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
                                        <a href="#tab-reviews">Reviews</a>
                                    </li>
                                    <!-- <li class="reviews_tab" id="video1" role="tab" aria-controls="tab-reviews">
                                        <a href="#video">Video</a>
                                    </li> -->
                                </ul>
                             <!--    <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description">

                                    <h2>Description</h2>

                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                                </div> -->
                              <!--   <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--additional_information panel entry-content wc-tab" id="tab-additional_information" role="tabpanel" aria-labelledby="tab-title-additional_information">

                                    <h2>Additional information</h2>

                                    <table class="shop_attributes">

                                        <tr>
                                            <th>brands</th>
                                            <td>
                                                <p>Annoushka, Balenciaga</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div> -->
                                
                                @if($product->overview)
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="overview" role="tabpanel" aria-labelledby="overview1">

                                   {!!html_entity_decode($product->overview)!!}
                                </div>
                                @endif
                                @if($product->details_that_matter)
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="details_that_matter" role="tabpanel" aria-labelledby="details_that_matter1">

                                {!!html_entity_decode($product->details_that_matter)!!}

                                </div>
                                @endif
                                <!-- @if($product->safety_information)
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="safety_information" role="tabpanel" aria-labelledby="safety_information1">

                                {!!html_entity_decode($product->safety_information)!!}

                                </div>
                                @endif -->
                                @if($product->dimensions)
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="dimensions" role="tabpanel" aria-labelledby="dimensions1">

                                {!!html_entity_decode($product->dimensions)!!}

                                </div>
                                @endif
                                @if($product->care)
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="care" role="tabpanel" aria-labelledby="care1">

                                {!!html_entity_decode($product->care)!!}

                                </div>
                                @endif
                                 
                               <!--  @if($product->our_commitment)
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="our_commitment" role="tabpanel" aria-labelledby="our_commitment1">   

                                {!!html_entity_decode($product->our_commitment)!!}

                                </div>
                                @endif -->
                                <!-- @if($product->in_house_design)
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="in_house_design" role="tabpanel" aria-labelledby="in_house_design1">

                                  {!!html_entity_decode($product->in_house_design)!!}

                                </div>
                                @endif -->
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content wc-tab" id="tab-reviews" role="tabpanel" aria-labelledby="tab-title-reviews">
                                    <div id="reviews" class="woocommerce-Reviews">
                                    <?php 
                                     $reviews = DB::table('product_reviews')->where('product_id' , '=' ,$product->id)->where('status','=',2)->get();
                                     $reviews_count = DB::table('product_reviews')->where('product_id' , '=' ,$product->id)->where('status','=',2)->count();
                                     ?> 


                                        <div id="comments">
                                            <h2 class="woocommerce-Reviews-title">{{$reviews_count}} reviews for <span>{{$product->product_title}}</span></h2>

                                            <ol class="commentlist">
                                            @foreach($reviews as $review)
                                                <li class="comment even thread-even depth-1" id="li-comment-39">

                                                    <div id="comment-39" class="comment_container">

                                                       <!--  <img alt='' src='http://0.gravatar.com/avatar/3472757f6a3732d6470f98d7d7e9cece?s=60&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/3472757f6a3732d6470f98d7d7e9cece?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 gravatar' height='60' width='60' /> -->
                                                        <div class="comment-text">

                                                           <!--  <div class="star-rating">
                                                                <span style="width:100%"><strong>5</strong> out of 5</span>
                                                            </div>
 -->
                                                         <!--    <p class="meta">
                                                                <strong class="woocommerce-review__author" itemprop="author">Coen Jacobs</strong> <span class="woocommerce-review__dash">&ndash;</span>
                                                                <time class="woocommerce-review__published-date" itemprop="datePublished" datetime="2013-06-07T12:21:30+00:00">June. 7. 2013.</time>
                                                            </p> -->

                                                            <div class="description">
                                                                <div class="comment_sec">
                                                                    <div class="user_image"></div>
                                                                    <div class="user_comment">
                                                            <p class="user_name">{{$review->name}}</p>
                                                            <p>{{$review->email}}</p>
                                                                <p class="user_text">{{$review->comment}}</p>
                                                                </div>
                                                            </div>
                                                                <!-- <div id="wp-ulike-comment-39" class="wpulike wpulike-default">
                                                                    <div class="counter"><a data-ulike-id="39" data-ulike-type="likeThisComment" data-ulike-status="3" class="wp_ulike_btn text">Like</a><span class="count-box">0</span></div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                                <!-- #comment-##
                                            </ol>

                                        </div>

                                        <div id="review_form_wrapper">
                                            <div id="review_form">
                                                <div id="respond" class="comment-respond">
                                                    <span id="reply-title" class="comment-reply-title"><span>Add a review</span> <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a></small></span>
                                                    <form action="#" id="reviewForm" method="post" class="form comment-form" name="form">
                                                    <!-- <form action="http://brideliness.themes.zone/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate> -->
                                                        <div class="success-review"></div>
                                                    <form action="#" id="reviewForm" method="post" class="form comment-form" name="form">
                                                        <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p>
                                                    

                                                        <p class="comment-form-author">
                                                            <label for="author">Name <span class="required">*</span></label>
                                                            <input id="name" name="name" type="text" value="" size="30" aria-required="true" required />
                                                        </p>
                                                        <p class="comment-form-email">
                                                            <label for="email">Email <span class="required">*</span></label>
                                                            <input id="email" name="email" type="email" value="" size="30" aria-required="true" required />
                                                        </p>
                                                        <input  name="product_id" placeholder="Title" class="hiddenrating1" value="{{$product->id}}" type="hidden">
                                                         <p class="comment-form-author">
                                                            <label for="author">Rating <span class="required">*</span></label>
                                                            <input  name="rating" placeholder="Title" class="hiddenrating" value="5" type="hidden">
                                                             <div id="rateYo"></div>
                                                        </p>
                                                       
                                                        <p class="comment-form-comment">
                                                            <label for="comment">Your review <span class="required">*</span></label>
                                                            <textarea id="msg" name="message" cols="45" rows="8" aria-required="true" required></textarea>
                                                        </p>
                                                        <p class="form-submit">
                                                            <input name="submit" type="submit" id="submit" class="submit submitreview" value="Submit" />
                                                        </p>
                                                    </form>
                                                </div>
                                                <!-- #respond -->
                                            </div>
                                        </div>

                                        <div class="clear"></div>
                                    </div>
                            </div>
                            
                            </div>
                            <div class="varimg">

                            <div class="summary entry-summary">

                                <div class="special_wrap">
                                    <h1 class="product_title entry-title">{{$product->product_title}}</h1>
                                </div>
                                     @if ($subcategory != 'girl-bedding-collections' && $subcategory != 'all-bedding-collections' && $subcategory != 'boy-bedding-collections' && $subcategory != 'neutral-bedding')
                                      <div class="special_wrap">
                                    <p class="price">@if($product->compare_price)<del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#x20B9; </span>{{$product->compare_price}}</span></del>@endif <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#x20B9;  </span>{{$product->price}}</span></ins>
                                    </p>

                                </div>
                               
    
                                @if($attributname->value == 'No Swatch')
                                @else
                                <table class="variations" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td class="label">
                                                <label for="pa_color"></label>
                                            </td>
                                            <?php 
                                            $productvar = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_variations.product_id', $product->id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id')
                                            ->groupby('product_variations.attribute_value_id')
                                            ->get();
                                            $varname = DB::table('product_variations')
                                            ->join('attribute_value', 'product_variations.attribute_value_id', '=', 'attribute_value.id', 'left')
                                            ->select('attribute_value.*')
                                            ->groupby('product_variations.attribute_value_id')
                                            ->get();
                                            ?>
                                           

                                            <b>Available Prints: </b> <p class="varname" name="varname" id="varname"> {{$attributname->value}}</p><br><br>
                                           
                                            @foreach($productvar as $key => $variation)
                                           

                                            <input type="hidden" value="{{$product->id}}">
                                            <?php 
                                            $varnames = DB::table('attribute_value')
                                            ->select('value')
                                            ->where('id',$variation->attribute_value_id)
                                            ->get();

                                            ?>
                                            @foreach($varnames as $key => $varname)
                                           
                                            <img src="{{URL::to('/product_images/'.$product->id.'/'.$variation->attribute_value_id.'/'.$variation->images)}}" width="92" height="50" title="{{$varname->value}}" alt="{{$variation->attribute_value_id}}" href="javascript:void(0)" class="variationsimg" style="cursor: pointer;">

                                            
                                            @endforeach
                                           
                                            @endforeach

                                            
                                             <!-- <button type="button" id="button5" onclick="document.getElementById('printchatbox').style.color ='red';" style="background: #bb81fa;"></button> &nbsp;&nbsp;
                                        <button type="button" id="button6" onclick="document.getElementById('printchatbox').style.fontFamily ='Seoge';" style="background: #80eefb;"></button>&nbsp;&nbsp;
                                        <button type="button" id="button7" onclick="document.getElementById('printchatbox').style.fontFamily ='Lato';" style="background: #81f997;"></button> -->
                                        </tr>

                                    </tbody>
                                </table>
                                @endif
                                <form class="cart" method="post" enctype='multipart/form-data'>
                                    <div class="quantity">
                                        <input type="number" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" />
                                    </div>

                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="varname" value="{{$attributname->value}}" class="varname">
                                    <input type="hidden" name="fullurl" value="<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" class="fullurl">

                                    <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
                                    <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price"> @if(isset($product->product_images))
                                    <input type="hidden" name="image_url1" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url image_url1"> @endif

                                    <input type="submit" class=" single_add_to_cart_button button alt button product_type_simple add_to_cart_button ajax_add_to_cart cartmobhide btn btn-primary add-to-cart detail-mob-sub" value="Add to Cart" data-toggle="modal" data-target="#myModal3">

                                    


                                  
                                    @if($product->personalised == 1)

                                    <div >
                                     <br><br>
                                     <button type="button" data-toggle="collapse" class=" single_add_to_cart_button button alt button product_type_simple add_to_cart_button ajax_add_to_cart cartmobhide btn btn-primary detail-mob-sub"  data-target="#demo" style="background: none;width: 100%;color: #78cbad;font-size: 15px; text-transform: uppercase;">PERSONALIZE IT  <span style="text-align: center;">For Rs.250</span></button>

                                        <div id="demo" class="collapse" style="height: auto;border: #dcdcdc 1px solid;padding: 6%;background: #fbfbfb;">
                                        <h3>Enter Text</h3>
                                       
                                        <input type='text' name='pname' class='chatinput' id='chatinput' maxlength="10" style="width: 100%">
                                        <span>Maximum number of characters: 10</span>
                                        <br><br><div class='' id='printchatbox' style="text-align: center; text-transform: uppercase;"></div><br>
                                        <h4>Select font</h4>
                                        <hr>
                                        <input type="hidden" name="font" id="font">
                                        <button type="button"  id="button1" value="Baskerville" name="font" class="fontbutn my_button" onclick="document.getElementById('printchatbox').style.fontFamily ='baskerville-regularregular';">Baskerville</button>
                                        <button type="button"  id="button2" value="American Typewriter" name="font" class="fontbutn my_button" onclick="document.getElementById('printchatbox').style.fontFamily ='American Typewriter';">American Typewriter</button>
                                        <button type="button"  id="button3" value="Edwardian Script" name="font" class="fontbutn my_button" onclick="document.getElementById('printchatbox').style.fontFamily ='Edwardian Script ITC';">Edwardian Script</bu tton>
                                       <!--  <button type="button"  id="button4"  name="font" value="Lato" class="fontbutn" onclick="document.getElementById('printchatbox').style.fontFamily ='Lato';">Lato</button> -->
                                        <button type="button" id="button4" class="fontbutn" onclick="document.getElementById('printchatbox').style.fontFamily ='Lato';">None</button>
                                        <br><br> 
                                        <input type="hidden" name="color" id="color">
                                        <button type="button" id="button5" value="Pink" class="my_color" onclick="document.getElementById('printchatbox').style.color ='Pink';" style="background: pink;"></button>
                                        <button type="button" id="button6" onclick="document.getElementById('printchatbox').style.color ='red';" value="red" class="my_color" style="background: red;"></button>
                                        <button type="button" id="button7" value="blue" class="my_color" onclick="document.getElementById('printchatbox').style.color ='blue';" style="background: blue;"></button>
                                         
                                          <button type="button" id="button7" value="Turquoise" class="my_color" onclick="document.getElementById('printchatbox').style.color ='Turquoise';" style="background: turquoise;"></button>
                                        </div>    
                                        
                                       
                                        </div>
                                        @endif

                                </form>
                                @else
                                <h2> Starting <span class="woocommerce-Price-currencySymbol">&#x20B9; </span> 699 - 12,500<!-- - <span class="woocommerce-Price-currencySymbol">&#x20B9; </span> 6000 --></h2>
                                <a href="#selectbelow">SELECT FROM BELOW</a>
                              @endif
                               
                            </div>
                            <!-- .summary -->

                            
                                </div>

       <div class="new_acordian">
    <div class="demo">

    
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                        Overview
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    {!!html_entity_decode($product->overview)!!}
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                        Details that matter
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    {!!html_entity_decode($product->details_that_matter)!!}
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                        Dimensions
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                 {!!html_entity_decode($product->dimensions)!!}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                        <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                        care
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">
                  {!!html_entity_decode($product->care)!!}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                        <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                        Reviews
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">
                   <?php 
                                     $reviews = DB::table('product_reviews')->where('product_id' , '=' ,$product->id)->where('status','=',2)->get();
                                     $reviews_count = DB::table('product_reviews')->where('product_id' , '=' ,$product->id)->where('status','=',2)->count();
                                     ?> 


                                        <div id="comments">
                                            <h2 class="woocommerce-Reviews-title">{{$reviews_count}} reviews for <span>{{$product->product_title}}</span></h2>

                                            <ol class="commentlist">
                                            @foreach($reviews as $review)
                                                <li class="comment even thread-even depth-1" id="li-comment-39">

                                                    <div id="comment-39" class="comment_container">

                                                       <!--  <img alt='' src='http://0.gravatar.com/avatar/3472757f6a3732d6470f98d7d7e9cece?s=60&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/3472757f6a3732d6470f98d7d7e9cece?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 gravatar' height='60' width='60' /> -->
                                                        <div class="comment-text">

                                                           <!--  <div class="star-rating">
                                                                <span style="width:100%"><strong>5</strong> out of 5</span>
                                                            </div>
 -->
                                                         <!--    <p class="meta">
                                                                <strong class="woocommerce-review__author" itemprop="author">Coen Jacobs</strong> <span class="woocommerce-review__dash">&ndash;</span>
                                                                <time class="woocommerce-review__published-date" itemprop="datePublished" datetime="2013-06-07T12:21:30+00:00">June. 7. 2013.</time>
                                                            </p> -->

                                                            <div class="description">
                                                            <p>{{$review->name}}</p>
                                                            <p>{{$review->email}}</p>
                                                                <p>{{$review->comment}}</p>
                                                                <!-- <div id="wp-ulike-comment-39" class="wpulike wpulike-default">
                                                                    <div class="counter"><a data-ulike-id="39" data-ulike-type="likeThisComment" data-ulike-status="3" class="wp_ulike_btn text">Like</a><span class="count-box">0</span></div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                                <!-- #comment-##
                                            </ol>

                                        </div>

                                        <div id="review_form_wrapper">
                                            <div id="review_form">
                                                <div id="respond" class="comment-respond">
                                                    <span id="reply-title" class="comment-reply-title"><span>Add a review</span> <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a></small></span>
                                                    <form action="#" id="reviewForm" method="post" class="form comment-form" name="form">
                                                    <!-- <form action="http://brideliness.themes.zone/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate> -->
                                                        <div class="success-review"></div>
                                                    <form action="#" id="reviewForm" method="post" class="form comment-form" name="form">
                                                        <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p>
                                                    

                                                        <p class="comment-form-author">
                                                            <label for="author">Name <span class="required">*</span></label>
                                                            <input id="name" name="name" type="text" value="" size="30" aria-required="true" required />
                                                        </p>
                                                        <p class="comment-form-email">
                                                            <label for="email">Email <span class="required">*</span></label>
                                                            <input id="email" name="email" type="email" value="" size="30" aria-required="true" required />
                                                        </p>
                                                        <input  name="product_id" placeholder="Title" class="hiddenrating1" value="{{$product->id}}" type="hidden">
                                                         <p class="comment-form-author">
                                                            <label for="author">Rating <span class="required">*</span></label>
                                                            <input  name="rating" placeholder="Title" class="hiddenrating" value="5" type="hidden">
                                                             <div id="rateYo"></div>
                                                        </p>
                                                       
                                                        <p class="comment-form-comment">
                                                            <label for="comment">Your review <span class="required">*</span></label>
                                                            <textarea id="msg" name="message" cols="45" rows="8" aria-required="true" required></textarea>
                                                        </p>
                                                        <p class="form-submit">
                                                            <input name="submit" type="submit" id="submit" class="submit submitreview" value="Submit" />
                                                        </p>
                                                    </form>
                                                </div>
                </div>
            </div>
        </div>

    </div><!-- panel-group -->
    
    
</div><!-- container -->
</div>                         

                                @if($product->video_link)
                                <div class="video_sec" id="video" role="tabpanel" aria-labelledby="care1">

                               <iframe width="100%" height="480" src="{{$product->video_link}}" frameborder="0" allowfullscreen></iframe>

                                </div>
                                @endif




                            <section class="related products">
                                 @if ($subcategory != 'girl-bedding-collections' && $subcategory != 'all-bedding-collections' && $subcategory != 'boy-bedding-collections' && $subcategory != 'neutral-bedding' && $subcategory && $subcategory != 'bed-in-a-bag')

                                    <h2 class="related-title"><span>YOU MIGHT ALSO LIKE</span></h2>

                                @else

                                

                                    <h2 class="related-title" id="selectbelow"><span>@if($subcategory != 'bed-in-a-bag')SELECT FROM BELOW @else BUY WITH MATCHING PILLOW @endif </span></h2>
                              
                                @endif
                                <!-- <h2 class="related-title" id="selectbelow"><span>BUY MATCHING PILLOW</span></h2> -->
                                <!-- <h2 class="related-title" id="selectbelow"><span>BUY MATCHING PILLOW</span></h2> -->
                                <!-- <h2 class="related-title" id="selectbelow"><span>BUY MATCHING PILLOW</span></h2> -->
<!-- {{$subcategory}} -->
                                   
                                <ul class="products">
                        @foreach($relatedp as $newproduct)

                            <?php 

                            $productrelated = DB::table('product')->where('id' , '=' ,$newproduct->related_pid)->orderBy('price','asc')->get();
                             // $productrelated = DB::table('product')
                             //                ->join('product_categories','product_categories.product_id','=','product.id', 'left')
                             //                ->where('product.id' , '=' ,$newproduct->related_pid)
                             //                ->get(); 

                           
                            ?>

                            @foreach($productrelated as $relatedp)
                                        <?php 
                                         // dd($productrelated);
                            $reltdcat = DB::table('product_categories')
                                                ->join('product_category','product_category.id','=','product_categories.category_id','left')
                                                ->select('product_category.*')
                                                ->where('product_id' , '=' ,$relatedp->id)->first();
                            // dd($reltdcat);

?>
                                     <li class="annoushka balenciaga grid-view  col-xs-12 col-md-4 col-sm-6 post-96 product type-product status-publish has-post-thumbnail product_cat-couture product_cat-fall-2016 product_cat-spring-2017 product_tag-accessories product_tag-bridal product_tag-trend first instock sale featured downloadable taxable shipping-taxable purchasable product-type-simple">
              <div class="product-wrapper">
                <div class="product-img-wrapper">
                  <a href="{{URL('/bedding/'.$reltdcat->slug.'/'.$relatedp->slug)}}" class="woocommerce-LoopProduct-link">   
                    <div class="background-img-product">
                    </div>
                   <!--  <span class="onsale">Top Pick
                    </span> -->
                    <img width="265" height="365" src="{{URL::to('/product_images/'.$relatedp->id.'/featured_images/'.$relatedp->product_images)}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="{{$relatedp->product_title}}" title="product11" />
                    
                    
                    <div class="clear">
                    </div>     
                  </a>  
                </div>
                <div class="product-description-wrapper">
                
                  <a class="product-title" href="{{URL('/bedding/'.$reltdcat->slug.'/'.$relatedp->slug)}}" title="Click to learn more about Donec eu libero tristique egestas malesu">
<div class="prodect_detail_text">
                    <h2 class="woocommerce-loop-product__title">{{$relatedp->product_title}}
                    </h2> 

                  </a>

                  @if($relatedp->personalised == 1)  
                  <p>

                    <a href="{{URL('/bedding/'.$reltdcat->slug.'/'.$relatedp->slug)}}">Personalization Available</a></p>
                    @endif
                    
                   

                  @if($relatedp->compare_price != 'NULL')

</div>

                  <span class="price">
                   <!--  <del>
                      <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol"> Rs. {{$relatedp->compare_price}}
                        </span>
                      </span>
                    </del>  -->
                    <ins>
                      <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">&#x20B9; {{$relatedp->price}}
                        </span>
                      </span>
                    </ins>
                  </span>
                  @else
                  <span class="price">
                    <del>
                      <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">&#x20B9; {{$relatedp->price}}
                        </span>
                      </span>
                    </del> 
                  @endif
                 <!--  <div class="star-rating" title="Rated 5.00 out of 5">
                    
                  </div>   -->    
               
                  <div class="buttons-wrapper">
                   <form>
                                    <div class="quantity">
                                        <input type="number" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" />
                                    </div><br>
                                     <input type="text" name="quantity" value="1" style="display: none;">
                                    <input type="hidden" name="product_title" value="{{$relatedp->product_title}}" class="product_title">
                                    <input type="hidden" name="product_id" value="{{$relatedp->id}}" class="product_id">
                                    <input type="hidden" name="varname" value="{{$attributname->value}}" class="varname">
                                    <input type="hidden" name="product_price" value="{{$relatedp->price}}" class="product_price">
                                    @if(isset($relatedp->product_images))
                                    <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$relatedp->id.'/featured_images/'.$relatedp->product_images)}}" class="image_url">
                                    @endif
                                    <input type="submit" class=" add-to-cart" value="Add to Bag" style="background: #000 !important;color: #fff !important;  border-radius: 3px !important; width: 78%; " data-toggle="modal" data-target="#myModal3">
                                    <!-- <div class="cartmeatmob cartmobicon cartmobiconiphone6"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> </div> -->
                                  </form>
                   
                    <!-- <a href="#" class=" yith-wcqv-button" >Quick View</a> -->
                    
                        
                    <span class="product-tooltip">
                    </span>

                  
                </div>
              </div>
            </li>

            </span>
            @endforeach
             @endforeach
                                </ul>

                            </section>

                        </div>
                        <!-- #product-96 -->

                    </div>
                    <!-- #content -->

                </div>
            </div>

        </div>
    </div>
    <!-- Content wrapper (end)-->

    <aside id="sidebar-shop-bottom" class="widget-area" role="complementary" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
        <div class="container">
            <div class="row">
                <div class="shop-bottom-sidebar col-xs-12 col-sm-12 col-md-6">
                    <div id="woocommerce_products-6" class="widget woocommerce widget_top_rated_products">
                        <h3 class="shop-bottom-sidebar">NEW INS</h3>
                        <ul class="product_list_widget">
                        @foreach($recentproduct as $newproduct)
                         <?php

                         $productcatd = DB::table('product_categories')
                                        ->join('product_category', 'product_category.id', '=', 'product_categories.category_id', 'left')
                                        ->select('product_category.slug')
                                        ->where('product_categories.product_id' , '=' ,$newproduct->id)->first();
                                        // echo $productcatd->slug;

                         ?>
                            <li>
                                <a href="{{URL('bedding/'.$productcatd->slug.'/'.$newproduct->slug)}}">
                                    <img width="100" height="145" src="{{URL::to('/product_images/'.$newproduct->id.'/featured_images/'.$newproduct->product_images)}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" /> 
                                    <span class="product-title">{{$newproduct->product_title}}</span>
                                </a>
                               <!--  <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>{{$newproduct->price}}</span></del> -->
                                <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#x20B9; </span>{{$newproduct->price}}</span></ins>

                            </li>
                            @endforeach

                          
                        </ul>
                    </div>
                </div>
                <div class="shop-bottom-sidebar col-xs-12 col-sm-12 col-md-6">
                    <div id="woocommerce_top_rated_products-2" class="widget woocommerce widget_top_rated_products">
                        <h3 class="shop-bottom-sidebar">BEST SELLERS</h3>
                       <ul class="product_list_widget">
                        @foreach($recentproduct as $newproduct)
                        <?php

                         $productcatd = DB::table('product_categories')
                                        ->join('product_category', 'product_category.id', '=', 'product_categories.category_id', 'left')
                                        ->select('product_category.slug')
                                        ->where('product_categories.product_id' , '=' ,$newproduct->id)->first();
                                        // echo $productcatd->slug;

                         ?>

                            <li>
                                <a href="{{URL('bedding/'.$productcatd->slug.'/'.$newproduct->slug)}}">
                                    <img width="100" height="145" src="{{URL::to('/product_images/'.$newproduct->id.'/featured_images/'.$newproduct->product_images)}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" /> 
                                    <span class="product-title">{{$newproduct->product_title}}</span>
                                </a>
                               <!--  <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rs</span>{{$newproduct->price}}</span></del> --> 
                               <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#x20B9; </span>{{$newproduct->price}}</span></ins>

                            </li>
                            @endforeach

                          
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </aside>

</div>

<div id="yith-quick-view-modal">

    <div class="yith-quick-view-overlay"></div>

    <div class="yith-wcqv-wrapper">

        <div class="yith-wcqv-main">

            <div class="yith-wcqv-head">
                <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a>
            </div>

            <div id="yith-quick-view-content" class="woocommerce single-product"></div>

        </div>

    </div>






</div>


<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(window).load(function() {

            var page = 2;
            var total = 0;
            var loading = false;

            $('.brideliness-get-more-posts').on('click', function() {
                var post_type = 'post';
                if (!loading) {
                    if (page > total) {
                        return false;
                    } else {
                        loading = true;
                        loadPosts(page);
                    }
                    page++;
                }
            });

            // Ajax loading Function
            function loadPosts(pageNumber) {
                $.ajax({
                    url: "http://brideliness.themes.zone/wp-admin/admin-ajax.php",
                    type: 'POST',
                    data: "action=get_more&page_no=" + pageNumber,
                    beforeSend: function() {
                        if (total >= page) {
                            $('.brideliness-get-more-posts').hide();
                            $("#content").append(
                                '<div class="infinity-blog"><div id="temp_load"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading... \
                                    </div></div>');
                        };
                    },
                    success: function(html) {
                        $("#temp_load").remove();
                        $("#content").append(html); // This will be the div where our content will be loaded*/
                        if (total > page) {
                            $('.brideliness-get-more-posts').show();
                        }
                        loading = false;
                    },
                });
            };

        });
    });
</script>



<script type="text/javascript">
    $(document).ready(function() {
        $('.my_button').click(function() {
            var font = $(this).attr("value");
            $('#font').val(font);
            // alert(font);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.my_color').click(function() {
            var color = $(this).attr("value");
            $('#color').val(color);
            // alert(font);
        });
    });
</script>
<script>
var inputBox = document.getElementById('chatinput');

inputBox.onkeyup = function(){
    // alert('fd');
    document.getElementById('printchatbox').innerHTML = inputBox.value;
}
</script>
<script>
     $('.variationsimg').click(function(event){
        var container = document.getElementById("demo");
            var content = container.innerHTML;
            container.innerHTML= content;
            $('#printchatbox').load(document.URL +  ' #printchatbox');
            
        var base_url = '{{URL::to('/')}}';
        var imgvar = $(this).attr("alt");
        var href = $(this).attr("title");
        var imgpro = document.getElementById("prodimg").src;
        // alert(imgpro);
         $('.image_url1').attr('value', imgpro);
        $('.varname').html(href);
        $('.varname').val(href);
        // $('.image_url1').val(imgpro);
        var pid = <?php echo $product->id; ?>;

event.preventDefault();
        // alert(pid);  
      $.ajax({
          url: base_url+'/ajax/variationsimges',
          type: "post",
          async: 'false',
        cache: 'false',
          data: 
          {
                "imgvar": imgvar,
                "pid": pid,
            },

          success:function(data)
          {
          $('.images-wrapper').html(data);

          } 
      });
      });

</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
    <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var ajax_var = {
        "url": "http:\/\/brideliness.themes.zone\/wp-admin\/admin-ajax.php",
        "nonce": "3fd77b7b12"
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "http:\/\/brideliness.themes.zone\/wp-json\/",
            "namespace": "contact-form-7\/v1"
        },
        "recaptcha": {
            "messages": {
                "empty": "Please verify that you are not a robot."
            }
        },
        "cached": "1"
    };
    var wc_add_to_cart_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "http:\/\/brideliness.themes.zone\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
    };
    var wc_single_product_params = {
        "i18n_required_rating_text": "Please select a rating",
        "review_rating_required": "yes",
        "flexslider": {
            "rtl": false,
            "animation": "slide",
            "smoothHeight": false,
            "directionNav": false,
            "controlNav": "thumbnails",
            "slideshow": false,
            "animationSpeed": 500,
            "animationLoop": false
        },
        "zoom_enabled": "",
        "photoswipe_enabled": "",
        "flexslider_enabled": ""
    };
    var woocommerce_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%"
    };
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments"
    };
    var yith_woocompare = {
        "ajaxurl": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "actionadd": "yith-woocompare-add-product",
        "actionremove": "yith-woocompare-remove-product",
        "actionview": "yith-woocompare-view-table",
        "actionreload": "yith-woocompare-reload-product",
        "added_label": "Added",
        "table_title": "Product Comparison",
        "auto_open": "yes",
        "loader": "http:\/\/brideliness.themes.zone\/wp-content\/plugins\/yith-woocommerce-compare\/assets\/images\/loader.gif",
        "button_text": "Add to Compare",
        "cookie_name": "yith_woocompare_list"
    };
    var yith_qv = {
        "ajaxurl": "\/wp-admin\/admin-ajax.php",
        "loader": "http:\/\/brideliness.themes.zone\/wp-content\/plugins\/yith-woocommerce-quick-view\/assets\/image\/qv-loader.gif",
        "is2_2": ""
    };
    var yith_wcwl_l10n = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "redirect_to_cart": "no",
        "multi_wishlist": "",
        "hide_add_button": "1",
        "is_user_logged_in": "",
        "ajax_loader_url": "http:\/\/brideliness.themes.zone\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif",
        "remove_from_wishlist_after_add_to_cart": "yes",
        "labels": {
            "cookie_disabled": "We are sorry, but this feature is available only if cookies are enabled on your browser.",
            "added_to_cart_message": "<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"
        },
        "actions": {
            "add_to_wishlist_action": "add_to_wishlist",
            "remove_from_wishlist_action": "remove_from_wishlist",
            "move_to_another_wishlist_action": "move_to_another_wishlsit",
            "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem"
        }
    };
    var megamenu = {
        "timeout": "300",
        "interval": "100"
    };
    var wysijaAJAX = {
        "action": "wysija_ajax",
        "controller": "subscribers",
        "ajaxurl": "http:\/\/brideliness.themes.zone\/wp-admin\/admin-ajax.php",
        "loadingTrans": "Loading...",
        "is_rtl": ""
    };
    var _wpUtilSettings = {
        "ajax": {
            "url": "\/wp-admin\/admin-ajax.php"
        }
    };
    var wc_add_to_cart_variation_params = {
        "wc_ajax_url": "\/product\/woo-album-4\/?wc-ajax=%%endpoint%%",
        "i18n_no_matching_variations_text": "Sorry, no products matched your selection. Please choose a different combination.",
        "i18n_make_a_selection_text": "Please select some product options before adding this product to your cart.",
        "i18n_unavailable_text": "Sorry, this product is unavailable. Please choose a different combination."
    };
    /* ]]> */
</script>
<script type='text/javascript' src="{{URL::to('admin-assets/js/productdetail.min.js')}}"></script>
<!-- video popup -->
 <script>
   $( '#myModal').on('click', function(event) {
    $("#myModal").hide();
});
    // $("#myModal").hide();

</script> 
<script type="text/javascript">
    // Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var img1 = document.getElementById('myImg1');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  ytplayer = document.getElementById("img01");
    ytplayer.stopVideo();
}
</script> 
<script type="text/javascript">
     $(".close").click(function() {
        jQuery.each($("iframe"), function() {
            $(this).attr({
                src: $(this).attr("src")
            });
        });
        return false;
    });
</script>
<script type="text/javascript">
     $(".modal").click(function() {
        modal.style.display = "none";
        jQuery.each($("iframe"), function() {
            $(this).attr({
                src: $(this).attr("src")
            });
        });
        return false;
    });
</script>
<script>
 $('.submitreview').click(function(e){
    e.preventDefault();
    
    var name = document.getElementById("name").value;
    var title = document.getElementById("email").value;
    var review = document.getElementById("msg").value;
    // alert(review);
if (name == '' || title == '' || review == '') {
// alert("Fill All Fields");
$("#error").css("display","block");
$("#error").css("color","red");
}
else
{
// var msgval = $("#msg").val();
    var base_url = "{{URL::to('/')}}";
       $.ajax({
        url:base_url+'/review',
          type:'POST',
          data:$("#reviewForm").serialize(),
          success:function(data){
            $("#reviewForm")[0].reset();
            // $("#reviewForm").resetForm();

            //  alert(data.status);
            // if (data.status==1) {
            // $("#npopupContact").modal('hide');
            // div_hide();
             // window.location.reload();
             $('#reviewForm').delay(1000).fadeOut();
             $('.success-review').html(data.message);
             $('.success-review').addClass('succcolor');



              // toastr.success(data.message, {timeOut: 100000});
              // return;
            // }
            // $('.user-detail').html(data);
            // toastr.success('Customer has been added successfully.', {timeOut: 100000})
            // $('.email-invoice').prop("disabled", true);
          }
      });
   }
    });
</script>
<script>
$(function () {
 
  $("#rateYo").rateYo({
    rating: 5,
    fullStar: true,
    starWidth: "20px",
    ratedFill: "#02c9c6",
 
    onSet: function (rating, rateYoInstance) {


        $('.hiddenrating').val(rating);
 
      // alert("Rating is set to: " + rating);
    }
  });
});
</script>

<script type="text/javascript">
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

<script>
    $('.cont').click(function(e){
    // alert("fdfdfd");
    // $('#myModal3')
    $('#myModal3').hide();
    $('.modal-backdrop').hide();

    });
</script>
<script>
    $('.chec_btm').click(function(e){
    // alert("fdfdfd");
    location.href = "{{URL::to('/cart')}}";
    });
</script>

@endsection