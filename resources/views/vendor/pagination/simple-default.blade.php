@if ($paginator->hasPages())
<style type="text/css">
    .previouspagi{
        list-style-type: none;
        float: left;
    }
    .nextpagi{
        list-style-type: none;
        float: right;
    }
    #paginationrow{
        margin-bottom: 40px;
    }
</style>
    <ul class="pagination" id="paginationrow">
        <!-- Previous Page Link -->
        @if ($paginator->onFirstPage())
            <li class="disabled previouspagi" ><span>Previous &laquo;</span></li>
        @else
            <li class="previouspagi"><a href="{{ $paginator->previousPageUrl() }}" rel="prev" >Previous &laquo;</a></li>
        @endif

        <!-- Next Page Link -->
        @if ($paginator->hasMorePages())
            <li class="nextpagi"><a href="{{ $paginator->nextPageUrl() }}" rel="next" >Next &raquo;</a></li>
        @else
            <li class="disabled nextpagi"><span>Next &raquo;</span></li>
        @endif
    </ul>
@endif
