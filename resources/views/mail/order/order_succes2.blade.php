<!DOCTYPE html>
<html>
<head>
	<title>emailer</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
	#likep{
		color: #969696;
    font-size: 17px;
    margin-bottom: 20px;
	}
	#logo{
	height: 200px;
}
#logoblock{
	text-align: center;
	font-weight: bolder;
}
#logoblock p {
	    color: grey;
    font-weight: 300;
    font-size: 17px;
}
#recieptdiv{
	background-color: #dc9f2a;
	    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    text-align: center;
    padding-top: 8px;
    padding-bottom: 12px;

}
#recieptdiv span { color: white; }
#recieptdiv h1{ color: white; }
#downloadbtn{
	    width: 100%;
    padding-top: 15px;
    padding-bottom: 15px;
    font-size: xx-large;
}
#paidwith{
	/*text-align: center;*/
	text-align: center;
    font-size: 15px;
    margin-bottom: 20px;
}
#feedbacksection{
	background-color: #212121;
    color: white;
    text-align: center;
    padding-bottom: 24px;
    padding-top: 20px;
}
#socialsection{
	background-color: #eaeaea;
	text-align: center;
	padding-top: 20px;
	padding-bottom: 20px;
}
#socialpara2{
	margin-top: 20px;
	margin-bottom: 20px;
}
#foot{
	    background-color: #212121;
    color: white;
    padding-top: 20px;
    padding-bottom: 20px;
    height: 90px;

}
.prodblock {
	    text-align: center;
    font-size: 14px;
    width: 50%;
    margin-left: auto;
    margin-right: auto;
}
.leftfl{
	float: left;
	margin-right: 15px;
}
.textbol{
	font-weight: bolder;
}
.feedsmiley{
	height: 40px;
	margin-right: 15px;
}
#recieptdiv2{
	    padding: 40px;
	text-align: center;
    /* margin: 28px; */
    background: black;
    color: white;
}
.ta{
	font-size: 14px;
}
.paras{ font-size: 17px; font-weight: 300; }

#col9{
	    padding: 0 20px 20px;
    font-family: 'Open Sans',Verdana,Tahoma,sans-serif;
    font-size: 12px;
    line-height: 1.6;
    font-weight: 400;
    color: #ffffff;
        padding-top: 15px;
    text-align: left;
    word-break: break-word;
    background-color: black;
}
footer {
        font-size: 14px;
    padding-right: 10px;
    padding-left: 20px;
    /* bottom: 0; */
    padding-top: 20px;
    padding-bottom: 30px;
    width: 100%;
    /* height: 60px; */
    color: white;
    /* height: 60px; */
    background-color: #212121;
}
body{ font-family: 'Open Sans',Verdana,Tahoma,sans-serif;
    ;
        font-family: 'Open Sans',Verdana,Tahoma,sans-serif;
    font-size: 22px;
    line-height: 1.3;
   
    font-weight: 500;
    word-break: break-word;
 }
 .forps{
margin-top: 10px;
    font-size: 18px;
 }
 #like{
 	text-align: center;
    margin-top: 25px;
    margin-bottom: 25px;
    padding-top: 20px;
    background: #f1f1f1;
    padding-bottom: 20px;

 }
 #downloadbtn{
 	background: #dc9f2a;
 	border: #dc9f2a;
 	color: white;
 }
 td{ text-align: center; }
 th{ text-align: center; }
td:first-Child{ width: 40%; }
 @media screen and (max-width:  580px) {
    body {
        font-size: 14px;
    }
    #logoblock p{
    	    margin: 60px;
    margin-left: 10px !important;
    margin-right: 10px !important;
    }
    #logo{ 
    	margin-top: 20px !important;
     }
    /* #pd1{ width: 100% }*/
    button{
    	width: 86% !important;
    	margin: 15px !important;
    }
    .prodblock{
    	width: 100%;
    }
}
</style>

</head>


<body>
<div class="container">
	
	<div id="logoblock">
		<img src="logo_svg.svg" alt="loghere" id="logo">
		<h1 style="margin-bottom: 50px;">Hey Bharat, thank you so much for your order!</h1>
		<p style="margin: 60px;">We know this is your first time with us and we hope you are happy and you enjoy the experience of ordering from Lion Fresh. If there’s anything that we can do to make you even happier, please let us know!</p>
	</div>	
	
	<div>
		<div id="recieptdiv">
			<h1 style=" font-size: 26px; ">Your Receipt</h1>
			<span style="font-size:15px;">TUESDAY, JANUARY 31, 2017, 11:48 AM</span>
		</div>
		<div id="recieptdiv2">
			<span>Invoice Number: c_1e23d</span>
		</div>
	</div>
	<div>
		 <table class="ta table table-striped ">
			    <thead>
			      <tr>
			        <th>Description</th>
			        <th>Unit Price</th>
			        <th>Total</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>
			        	<span class="leftfl">1x</span>
			        	<p>Howdyji - Chicken Masala Franks 4" (400g)</p>

			        </td>
			        <td>₹15.25	</td>
			        <td>₹15.25</td>
			      </tr>
			     
			    </tbody>
		 </table>
		 <button type="button" class="btn btn-warning" id="downloadbtn" style=" font-size: 24px; background: #dc9f2a; ">Buy Now</button>
	</div>
	<div style=" text-align: center; ">
		<table class="ta table table-striped">
			    <thead>
			      <tr>
			        <th>Description</th>
			        <th>Unit Price</th>
			        <th>Total</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>
			        	<span class="leftfl">1x</span>
			        	<p>Howdyji - Chicken Masala Franks 4" (400g)</p>

			        </td>
			        <td>₹15.25	</td>
			        <td>₹15.25</td>
			      </tr>
			    <tfoot>
			      <tr>
			        <td>
			        	

			        </td>
			        <td class="textbol">Discount</td>
			        <td>₹15.25</td>
			      </tr>
			       <tr>
			        <td>
			        	

			        </td>
			        <td class="textbol">Discount</td>
			        <td>₹15.25</td>
			      </tr>
			    </tfoot> 
			     
			    </tbody>
		 </table>
		 <p id="paidwith" >Paid with VISA: **** **** **** 4242 / Gift Card</p>
	</div>
	<div class="row" id="like" style=" text-align: center;">
	<h3>You might also like...</h3>
	<p id="likep">Check out these awesome products just waiting for you in our store.</p>
		<div class="col-md-6" style=" text-align:center; margin-top: 10px; " >
			<div class="prodblock" style=" text-align:center;">
				<img src="pd1.jpg" style=" height:300px; ">
				<p class="forps">Authentic Pork 'Kransky' Sausage (280g)</p>
				<p>Authentic Pork 'Kransky' Sausage (280g)
						La Carne Cuts Pork Kransky is a savory Polish pork sausage smoked with our trademark beechwood w...
				</p>
				<button type="button" class="btn btn-warning" id="downloadbtn" style=" font-size: 24px; background: #dc9f2a; ">Buy Now</button>
			</div>
		</div>
		<div class="col-md-6" style=" text-align:center; margin-top: 10px ">
			<div class="prodblock" style=" text-align:center;">
				<img class="img-responsive" src="pd1.jpg" style=" height: 300px; margin-left: auto; margin-right: auto; ">
				<p class="forps">Pork Bockwurst Sausage (280g)</p>
				<p>Authentic Pork 'Kransky' Sausage (280g)
						La Carne Cuts Pork Kransky is a savory Polish pork sausage smoked with our trademark beechwood w...</p>
				 <button type="button" class="btn btn-warning" id="downloadbtn" style=" font-size: 24px; background: #dc9f2a; ">Buy Now</button>
			</div>
		</div>
	</div>
	<div id="feedbacksection" >
		<p style=" font-size: 18px; ">We want your feedback!</p>
		<p class="paras" style="    padding-top: 20px;">We value your opinion and are always on the look out for ways to improve our service.
Click below to leave feedback on your experience.</p>
			<div style=" padding-top: 20px; ">
				<img src="happy.svg" class="feedsmiley">
				<img src="sad.svg" class="feedsmiley">
			</div>
		
	</div>
	<div id="socialsection">
		<h2>let's be more social together</h2>
		<p class="paras" style=" margin-top: 30px; " >We're constantly sharing product news and promotions on our social pages, click the links below to follow your favourite stream!</p>
		<p class="paras" id="socialpara2" style=" margin-bottom: 35px; margin-top: 35px; ">If you want more information about our group company, you can always go to our Primo Foods India website </p>
		<div>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<div class="clearfix"></div>
	<footer class="row" style=" margin-left: 0px; margin-right: 0px; ">
      <div >
      	  <div class="col-md-9">
	      	<h4>QUESTIONS?</h4>
	        <p >If you need any help, just reply to this email and we will get back to you as soon as possible.</p>
	      </div>  
	        <div class="col-md-3">
	        	<span>27B Prithviraj Road</span>
				<span>India</span>
	        </div>
      </div>
    </footer>

</div>




<!-- all the javascript here at the bottom of the page to make the page load faster -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>