<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title></title>

  <style type="text/css">
  .colordva {
    height: 30px;
    width: 30px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}

 @media only screen and (max-width: 480px) {
        body,table,td,p,a,li,blockquote {
          -webkit-text-size-adjust:none !important;
        }
        body {width: 100%;}
        table {width: 100% !important;}

        .responsive-image img {
          height: auto !important;
          max-width: 100% !important;
          width: 100% !important;
        }
    .bord{border:0!important;}
    .non{display:none !important;}
    .fon{font-size:17px !important;}
      }


  </style>    
</head>
<body style=" width: 600px; margin: 0 auto; font-family: arial; padding: 10px; te">
 
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" align="center">
               <img src="http://babyjalebi.com/images/logoforinvoice.png">
                
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr><td valign="top" height="20px"></td></tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                Hello, {{$name}} {{$last_name}}
                
            </td>
        </tr>
    </table>
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr><td valign="top" height="20px"></td></tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" style="font-size: 14px;">
               Thank you for your order from Babyjalebi. Once your package ships we will send an email with a link to track your order. If you have any questions about your order please contact us at wecare@babyjalebi.com or call us at 8588850079 Monday - Friday, 8am - 5pm PST.
                
            </td>
        </tr>
    </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr><td valign="top" height="30px"></td></tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" style="font-size: 14px;">
              Your order confirmation is below. Thank you again for your business.
                
            </td>
        </tr>
    </table>
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr><td valign="top" height="30px"></td></tr>
    </table>
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" style="font-size: 17px;">
              <strong> Your Order #{{$orderid}} </strong>(placed on {{$order->created_at}} IST)
                
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr><td valign="top" height="20px"></td></tr>
    </table>
    <!--  <table width="280" border="0" cellpadding="0" cellspacing="0" align="left" style="border: 1px solid #ccc">
        <tr>
            <td style="background: #ccc; padding: 5px; font-size: 13px;">
            Billing Information:
            </td>
        </tr>
     
    </table> -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="right">
        <tr>
<td>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<thead>
<tr>
<th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">
Billing Information:</th>
<th width="10"></th>
<th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">
Payment Method:</th>
</tr>
</thead>
<tbody>
<tr>
<td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
{{$name}} {{$last_name}}<br>
{{$order->baddress}}, <br>
{{$order->bcity}},{{$order->bstate}}, {{$order->bzip}}<br>
    T:{{$phone}}
</td>
<td>&nbsp;</td>
<td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
<p>@if($order->status == 2) CCAvenue @else COD @endif </p>
</td>
</tr>
</tbody>
</table>
<br>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<thead>
<tr>
<th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">
Shipping Information:</th>
<th width="10"></th>
<th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">
Shipping Method:</th>
</tr>
</thead>
<tbody>
<tr>
<td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
{{$order->address}}<br>
{{$order->city}},{{$order->state}}, {{$order->zip}}<br>
T:{{$phone}}
<td>&nbsp;</td>
<td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
Delhivery Lastmile - Delhivery &nbsp; </td>
</tr>
</tbody>
</table>
<br>
<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border:1px solid #eaeaea">
<thead>
<tr>
<th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Item</th>
<th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Sku</th>
<th align="center" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Qty</th>
<th align="right" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Subtotal</th>
</tr>
</thead>
<tbody>
<tr>
@foreach($order_detail as $detail)
<td align="left" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
<strong style="font-size:11px">{{$detail->product_title}}
 @if($detail->pattern_name)
       @if ($detail->pattern_name == 'No Swatch')
       @else
                  Pattern:{{$detail->pattern_name}}
                  @endif
            
                  @endif
                  <br>
                   @if($detail->text)
                  Text:{{$detail->text}}
                  <br>
                  @endif
                  @if($detail->font)
                  Font:{{$detail->font}}
                  <br>
                  @endif
                   @if($detail->color)
                  Color:{{$detail->color}} <div class="colordva" style="background-color:{{$detail->color}};height: 30px; width: 30px;-webkit-border-radius: 5px; border-radius: 5px;"> </div>
                  <br>
                  @endif
                   @if($detail->pattern_color)
                  Pattern color:{{$detail->pattern_color}}
                  @endif
                  <br>
                   </strong></td>
<td align="left" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
- </td>
<td align="center" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
{{$detail->quantity}}</td>
<?php $qty =  $detail->quantity;?>
<td align="right" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
Rs {{$detail->price*$qty}} </td>
</tr>
@endforeach
</tbody>
<tbody>
<tr>
<td colspan="3" align="right" style="padding:3px 9px">Subtotal </td>
<td align="right" style="padding:3px 9px">Rs {{round($order->payble_amount, 2)}} </td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px">Shipping &amp; Handling </td>
<td align="right" style="padding:3px 9px">Rs 0.00 </td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px"><strong>Grand Total</strong>
</td>
<td align="right" style="padding:3px 9px"><strong>Rs {{round($order->payble_amount, 2)}}</strong> </td>
</tr>
</tbody>
</table>
<p style="font-size:12px;margin:0 10px 10px 0"></p>
</td>
</tr>
     
    </table>

</body>
</html>