<!DOCTYPE html>
<html>
<head>
	<title>emailer</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="styl.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.container{
	text-align: center;
}
#date{ float: left; }
.padtop{ padding-top: 9.5%; }
#firstcol{ text-align: left; font-family: monospace; } .firstcol{ text-align: left; }
#firstcol h6{ font-size: 16px !important; }
#col2{ text-align: justify; font-family: monospace; padding-left: 23%; } #col2 h6 { font-size: 16px !important; }
#invoice { text-align: left; }
#seconddiv img{
	height: 130px;
    margin-top: -12%;
    float: right;
    margin-right: 10%;
}
#thirddiv1 span{ float: left; }
.tr{
	    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
   border-top: 0px !important;
}
.ba{
	font-weight: bold;
}
h5{
	font-weight: lighter;
	font-size: 16px;
	font-family: monospace;
}
td{
	border-top: 2px solid #bfbcbc !important ;
}
.table>thead>tr>th{
	border-bottom: 2px solid #231e1e;
}
.table{
	text-align: left;
}
.billto {
	float: left;
}
.div1child{ text-align: left; /*margin-left: 30%;*/ }
.div2child{ float: right;
    margin-right: 22%;
    text-align: left; }
.aligndivright{ float: right;}
.mobmargin{
	margin-top: -84px;
    margin-right: 7%;
}
@media screen and (max-width: 600px){
.mobmargin{ margin-top: -43px; }
}



	</style>

</head>
<body>
<div class="container" id="firstrow">
	
	<p>Unit of Heirlooms India </p>
	
		<img src="{{URL::to('admin-assets/images/icons/adm_.png')}}" alt="logohere"> 
	</div>
	<div class="clearfix"></div>
	<div class="row " id="thirddiv" >
		<div class="col-md-6 col-sm-6 col-xs-6" id="thirddiv1" >
 			        <!-- <div class="billto">
				
			</div> -->
			<div class="div1child">
			<span class="ba">Bill to</span> <br>
				  @if(isset($baddress))
                  <div class="col-sm-12">
                  <BR>

                    {{$order->first_name}} {{$order->last_name}}<BR></BR> 
                    {{$baddress->address}}<BR></BR> 
                    {{$baddress->address2}}<BR></BR> 
                    {{$baddress->zip}} {{$baddress->city}} {{$baddress->state}}<BR></BR> 
                    {{$baddress->country}} <BR></BR>
				<h5> Tel : {{$baddress->phone}}</h5>
                      
                  @else
                  <div class="col-sm-12">
                    {{$order->first_name}} {{$order->last_name}}<BR></BR> 
                    {{$address->address}}<BR></BR> 
                    {{$address->address2}}<BR></BR> 
                    {{$address->zip}} {{$address->city}} {{$address->state}}<BR></BR> 
                    {{$address->country}} <BR></BR> 
                    @if($order->contact == 'null')
				<h5> Tel : {{$baddress->phone}}</h5>
				@else
				<h5> Tel : {{$order->contact}}</h5>
				@endif
                  </div>
                  @endif

			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6" id="thirddiv1">
			<!-- <div class="billto"> -->
				
			<!-- </div> -->
			<div class="div2child">
			<span class="ba">Ship to</span><br>
				 {{$order->first_name}} {{$order->last_name}}<BR>
                    {{$address->address}}<BR>
                    {{$address->address2}}<BR>
                    {{$address->zip}} {{$address->city}} {{$address->state}}<BR>
                    {{$address->country}} <BR>

                    @if($order->contact == 'null')
				<h5> Tel : {{$address->phone}}</h5>
				@else
				<h5> Tel : {{$order->contact}}</h5>
				@endif
			</div>
		</div> 
	</div>
	<div class="clearfix"></div>
	<div class="row padtop">
		<table class="table">
		    <thead>
		      <tr>
		        <th>Item description</th>
		        <th>Qty </th>
		        <th>Price</th>
		        <th>Item Total</th>
		      </tr>
		    </thead>
		    <tbody>
		     @foreach($order_detail as $detail)
		      <tr>
		        <td class="ba">{{$detail->product_title}}
		        	<br>
		         @if($detail->pattern_name)
                  @if ($detail->pattern_name == 'No Swatch')
                    @else
                  Pattern:{{$detail->pattern_name}}
                  @endif
            
                  @endif
                  <br>
                   @if($detail->text)
                  Text:{{$detail->text}}
                  <br>
                  @endif
                  @if($detail->font)
                  Font:{{$detail->font}}
                  <br>
                  @endif
                   @if($detail->color)
                  Color:<div class="colordva" style="background-color:{{$detail->color}};"> </div>
                  <br>
                  @endif
                   @if($detail->pattern_color)
                  Pattern color:{{$detail->pattern_color}}
                  @endif</td>
		        <td>&times; &nbsp;{{$detail->quantity}}</td>
		        <td>{{$detail->price}}</td>
		        <td>Rs. {{$detail->price*$detail->quantity}}</td>
		      </tr>
		     @endforeach
		      
		    </tbody>
		</table>
		  <hr>
	</div>
	<div class="row">
		<div class="col-md-8 firstcol">
			<h5>notes</h5>
			<p>{{$order->notes}}</p>
		</div>
		<div class="mobmargin col-md-4 col-xs-4 col-sm-4 aligndivright">
			 <table class="table">
			    <tbody>
			      <tr>

			        <td class="tr">
			        @if($order->discount == "")
			        <span class="ba">Discounts </span class="ba">
			        </td>
			        <td class="tr">Rs. -0.00</td>
			        @else
			        <span class="ba">Discounts "{{$order->coupon_code}}"</span class="ba">
			        </td>
			        <td class="tr">Rs. -{{$order->discount}}</td>
			        @endif 
			        
			      </tr>
			      <tr>
			        <td><span class="ba"> Subtotal </span class="ba"></td>
			        <td>Rs. {{$order->amount}}</td>
			       
			      </tr>
			       <tr>
			        <td> Shipping: - {{$order->shipping}} </td>
                                @if($order->shipping ==='free')
                                <td>Free</td>
                                @else
                                <td>Rs. {{$order->custom_rate_value}}</td>
                                @endif
			      </tr>
			      <tr>
			        <td><span class="ba">Sales Tax</span class="ba"> </td>
			        @if($order->tax_rate == NULL)
			        <td>Rs. 0.00</td>
			        @else
			        <td>Rs. {{round($order->tax_rate , 2)}}</td>
			        @endif
			      </tr>
			      <tr>
			      	 <td><span class="ba">Total</span class="ba"> </td>

			      	 <td>Rs. {{round($order->payble_amount , 2)}}</td>
			      </tr>
			    </tbody>
			 </table>
			 <hr>
		</div>
	</div>
	<div class="" >
		<h5 class="ba">Thanks for your Business!</h5>
		<p>If you have any questions, please do get in touch with us.</p>
	</div>
	<div>
		<h4 class="ba">Baby Jalebi</h4>
		<h5></h5>
		<h5> CIN U15209DL2013PTC247462C-130,</h5>
		<h5></h5>
	    <h5>E: wecare@babyjalebi.com / P: 85 888 5079 / Follow us:&nbsp;&nbsp;<span><i class="fa fa-facebook" aria-hidden="true"></i></span>&nbsp;&nbsp;<span><i class="fa fa-instagram" aria-hidden="true"></i>
</span></h5>
	</div>
	
</div> 

<!-- all the javascript here to make the page load faster -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
