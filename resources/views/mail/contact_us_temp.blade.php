<!DOCTYPE html>
<html>
<head>
    <title>emailer</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    #logo{
    /*height: 200px;*/
}
#logoblock{
    text-align: center;
    font-weight: bolder;
}
#logoblock p {
        color: black;
    font-weight: 100;
    font-size: 17px;
}
#recieptdiv{
    background-color: #dc9f2a;
        border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    text-align: center;
    padding-top: 8px;
    padding-bottom: 12px;

}
#recieptdiv span { color: white; }
#recieptdiv h1{ color: white; }
#downloadbtn{
        width: 100%;
    padding-top: 15px;
    padding-bottom: 15px;
    font-size: xx-large;
}
#paidwith{
    /*text-align: center;*/
    text-align: center;
    font-size: 15px;
    margin-bottom: 20px;
}
#feedbacksection{
    background-color: #212121;
    color: white;
    text-align: center;
    padding-bottom: 24px;
    padding-top: 20px;
}
#socialsection{
    background-color: #eaeaea;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
}
#socialpara2{
    margin-top: 20px;
    margin-bottom: 20px;
}
#foot{
        background-color: #212121;
    color: white;
    padding-top: 20px;
    padding-bottom: 20px;
    height: 90px;

}
.prodblock {
        text-align: center;
    font-size: 14px;
    width: 50%;
    margin-left: auto;
    margin-right: auto;
}
.leftfl{
    float: left;
    margin-right: 15px;
}
.textbol{
    font-weight: bolder;
}
.feedsmiley{
    height: 40px;
    margin-right: 15px;
}
#recieptdiv2{
        padding: 40px;
    text-align: center;
    /* margin: 28px; */
    background: black;
    color: white;
}
.ta{
    font-size: 14px;
}
.paras{ font-size: 17px; font-weight: 300; }

#col9{
        padding: 0 20px 20px;
    font-family: 'Open Sans',Verdana,Tahoma,sans-serif;
    font-size: 12px;
    line-height: 1.6;
    font-weight: 400;
    color: #ffffff;
        padding-top: 15px;
    text-align: left;
    word-break: break-word;
    background-color: black;
}
footer {
        font-size: 14px;
    padding-right: 10px;
    padding-left: 20px;
    /* bottom: 0; */
    padding-top: 20px;
    padding-bottom: 30px;
    width: 100%;
    /* height: 60px; */
    color: white;
    /* height: 60px; */
    background-color: #212121;
}
body{ font-family: 'Open Sans',Verdana,Tahoma,sans-serif;
    ;
        font-family: 'Open Sans',Verdana,Tahoma,sans-serif;
    font-size: 22px;
    line-height: 1.3;
   
    font-weight: 500;
    word-break: break-word;
 }
 .forps{
margin-top: 10px;
    font-size: 18px;
 }
 #like{
    /*text-align: center;*/
    margin-top: 25px;
    margin-bottom: 25px;
    padding-top: 20px;
    background: #f1f1f1;
    padding-bottom: 20px;

 }
 td{ text-align: center; }
 th{ text-align: center; }
td:first-Child{ width: 40%; }
 @media screen and (max-width:  580px) {
    body {
        font-size: 14px;
    }
    #logoblock p{
            margin: 60px;
    margin-left: 10px !important;
    margin-right: 10px !important;
    }
    #logo{ 
        margin-top: 20px !important;
     }
    /* #pd1{ width: 100% }*/
    button{
        width: 86% !important;
        margin: 15px !important;
    }
}
table.blueTable {
  border: 1px solid #1C6EA4;
  background-color: #EEEEEE;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  border: 1px solid #AAAAAA;
  padding: 3px 2px;
}
table.blueTable tbody td {
  font-size: 13px;
}
table.blueTable tr:nth-child(even) {
  background: #D0E4F5;
}
table.blueTable tfoot td {
  font-size: 14px;
}
table.blueTable tfoot .links {
  text-align: right;
}
table.blueTable tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}
</style>

</head>
<body style="font-family: 'Open Sans',Verdana,Tahoma,sans-serif;font-size: 22px;line-height: 1.3;font-weight: 500;word-break: break-word;">
<div class="container">
    
   <div id="logoblock" style="text-align: center;font-weight: bolder;">
        <img src="{{URL::to('web-asset/img/logo_235.png')}} " alt="Babyjalebi" id="logo" style="margin-top: 100px; width: 35%;">
        <table class="blueTable">
<tbody>
<tr>
<td>Name</td>
<td>{!! $user !!}</td>
</tr>
<tr>
<td>Mail</td>
<td>{!! $mail !!}</td>
</tr>
<tr>
<td>Phone</td>
<td>{!! $contact !!}</td>
</tr>
<tr>
<td>Message</td>
<td>{!! $msg !!}</td>
</tr>
</tbody>
</table>
       <!--  -->
        
        <!-- <p>
As a way of saying thanks for ordering from us today, here is a coupon for 10% off your any purchase next time..<br><br>
<b>Your Discount Coupon  </b></p>
<span style="background-color:#fff;border:#d8920c 2px dashed;padding: 9px 60px; margin-bottom: 29%;"> 1TA0-A20V-17PL</span>
 -->
    </div>  
    
    <div class="clearfix"></div>
   

</div>






</body>
</html>