@extends('admin.admin_layout')


@section('content')
<?php
  // use Carbon\Carbon;
  use App\Product;
  use App\Address;
?>
<style type="text/css">
.foredit
{
  background: #e0e0e0;
    padding-top: 13%;
    border-radius: 39px;
    padding-bottom: 15%;
    padding-right: 24%;
    padding-left: 24%;
    margin-right: 22%;
    color: #fff;
    font-size: 16px;
}
     @media only screen and (max-width: 500px) {
        .mobileshow{
            z-index: 9!important;
            
                    }
        #comment{
            width: 100%;
                    }
                                                }
    
  ul{
    list-style-type: none;
  }
  .property_image {
    height: 170px;
  }
  .background-layout
  {
    padding: 2%;
    background-color: #ffffff;
    border-radius: 3px;
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
   
  }
  .padding-lft
  {
     margin-right: 3%;
  }
  .ui-feed__timeline{position:relative;padding-top:15px;background-color:rgba(255,255,255,0);border-radius:3px;transition:background-color 200ms ease-in-out}.ui-feed__timeline>.ui-feed__item--accordion{opacity:0;transition:opacity 200ms ease-in-out}.ui-feed__timeline>.ui-feed__item--message{margin-bottom:15px}.ui-feed__timeline>.ui-feed__item--action{margin-top:0}.ui-feed__timeline .ui-feed__marker{z-index:1;transition:border-color 200ms ease-in-out}.ui-feed__timeline .ui-feed__message{min-width:0;max-width:100%}.ui-feed__timeline::after{content:' ';position:absolute;top:0;bottom:0;left:34px;width:3px;background:#e3e6e9;opacity:0;transition:opacity 200ms ease-in-out}.ui-feed__timeline.ui-feed__item--revealed{background-color:white}.ui-feed__timeline.ui-feed__item--revealed>.ui-feed__item--accordion{opacity:1}.ui-feed__timeline.ui-feed__item--revealed .ui-feed__marker{border-color:#ffffff}.ui-feed__timeline.ui-feed__item--revealed>.ui-feed__item--action{padding-bottom:15px}.ui-feed__timeline.ui-feed__item--revealed::after{opacity:1}.ui-form__section{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;margin:-8px}
  .ui-feed:not(.ui-feed--plain){position:relative}.ui-feed:not(.ui-feed--plain)::after{content:' ';position:absolute;top:5px;bottom:18px;left:34px;width:3px;background:#e3e6e9}@media (max-width: 550px), (min-width: 768px) and (max-width: 790px){.ui-feed__full-width{overflow:hidden;margin-right:-10px;margin-left:-22px}.ui-feed__full-width .next-card{border-radius:0}.ui-feed__full-width .ui-feed__item{margin-right:4px}.ui-feed__full-width .ui-feed__item--framed{margin-right:9px}.ui-feed__full-width .ui-feed__item--framed::before{left:-9px;right:-5px}}.ui-feed__section{position:relative;z-index:2}.ui-feed__item{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-align-items:center;-ms-flex-align:center;align-items:center;position:relative;margin-bottom:20px;margin-left:16px}.ui-feed__item>*{-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto}.ui-feed__item--card{-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;width:100%;margin-left:0}.ui-feed__item--card>.next-card{width:100%}.ui-feed__item--heading{margin-top:25px;margin-bottom:10px}.ui-feed__item--card+.ui-feed__item:not(.ui-feed__item--card){margin-top:25px}.ui-feed__item:not(.ui-feed__item--card)+.ui-feed__item--card{margin-top:10px}.ui-feed__item--action{margin-left:26px;margin-top:-10px}.ui-feed__item--message,.ui-feed__item--date{margin-left:26px}.ui-feed__item--card-context{padding-top:10px;margin-bottom:15px}.ui-feed__item--date{margin-top:15px;margin-bottom:20px}.ui-feed__item--date+.ui-feed__item--message{margin-top:20px}.ui-feed__item--framed{padding:5px 0}.ui-feed__item--framed::before{content:'';position:absolute;top:-10px;left:-26px;right:0;bottom:-10px;border:1px solid #c3cfd8;border-radius:3px;z-index:-1}.ui-feed__separator{margin-top:-5px;color:#e3e6e9;background-color:#e3e6e9}.ui-feed__marker{-webkit-flex:none;-ms-flex:none;flex:none;width:13px;height:13px;margin-right:10px;vertical-align:middle;border-radius:50%;border:3px solid #ebeef0;background-color:#c3cfd8}.ui-feed__spacer{-webkit-flex:none;-ms-flex:none;flex:none;-webkit-align-self:baseline;-ms-flex-item-align:baseline;align-self:baseline;width:19px;height:13px;margin-right:10px}.ui-feed__item--heading .ui-feed__marker{width:auto;height:auto;margin-right:5px;border:0;background-color:transparent}.ui-feed__marker--date{background-color:#c3cfd8}.ui-feed__marker--user-action{background-color:#0078bd}.ui-feed__marker--error{background-color:#ff5d5d}.ui-feed__item--accordion{margin-bottom:0;margin-left:0;padding-left:16px;background-color:#f5f6f7}.ui-feed__item--accordion.ui-accordion--is-expanded{margin-bottom:15px}.ui-feed__item__accordion-content{min-width:0;max-width:100%;margin-left:18px;padding-left:20px;border-left:3px solid #e3e6e9}.ui-feed__item__accordion-content>.ui-accordion__panel{padding:10px 10px 10px 0}.ui-feed__timeline{position:relative;padding-top:15px;background-color:rgba(255,255,255,0);border-radius:3px;transition:background-color 200ms ease-in-out}.ui-feed__timeline>.ui-feed__item--accordion{opacity:0;transition:opacity 200ms ease-in-out}.ui-feed__timeline>.ui-feed__item--message{margin-bottom:15px}.ui-feed__timeline>.ui-feed__item--action{margin-top:0}.ui-feed__timeline .ui-feed__marker{z-index:1;transition:border-color 200ms ease-in-out}.ui-feed__timeline .ui-feed__message{min-width:0;max-width:100%}.ui-feed__timeline::after{content:' ';position:absolute;top:0;bottom:0;left:34px;width:3px;background:#e3e6e9;opacity:0;transition:opacity 200ms ease-in-out}.ui-feed__timeline.ui-feed__item--revealed{background-color:white}.ui-feed__timeline.ui-feed__item--revealed>.ui-feed__item--accordion{opacity:1}.ui-feed__timeline.ui-feed__item--revealed .ui-feed__marker{border-color:#ffffff}.ui-feed__timeline.ui-feed__item--revealed>.ui-feed__item--action{padding-bottom:15px}.ui-feed__timeline.ui-feed__item--revealed::after{opacity:1}.ui-form__section{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;margin:-8px}.ui-form__section .ui-form__section{margin:0}.ui-form__section+.ui-form__section{margin-top:8px}
    
 
    
</style>
<!-- Modal for email change-->
<div id="userEmailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit email
        </h4>
      </div>
      <div class="modal-body">
        <h5>Notification emails will be sent to this address
        </h5>
        <br>
        <div class="form-group">
          <div class="col-sm-12">
           <label for="form-field-1"> Email 
          </label>
            <input type="hidden" class="user_id" name="id" value="{{$order->userid}}" />
         <input type="hidden" class="user_id" name="order_id" value="{{$order->order_code}}" />


            {{Form::text('email',$order->email, array('id' => 'email','class' => 'col-xs-12 col-sm-12 user_email'))}}
            @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <br>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="button" class="btn btn-default change-email">Apply
        </button>
      </div>
    </div>
  </div>
</div>
<!-- Modal for address change-->
<div id="userAddressModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content editcustomer">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit shipping address
        </h4>
      </div>
      <div class="modal-body">

      <form id="customer-edit-shipping">
         <input type="hidden" class="user_id" name="id" value="{{$order->userid}}" />
         <input type="hidden" class="user_id" name="order_id" value="{{$order->order_code}}" />

        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> First Name
            </label>
            {{Form::text('first_name',$order->first_name, array('id' => 'first_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> Last Name 
            </label>
            {{Form::text('last_name',$order->last_name, array('id' => 'last_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
          <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Company
            </label>
            {{Form::text('company',$address->company, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('company'))
            <span class="help-block">
              <strong>{{ $errors->first('company') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Phone Number 
            </label>
            {{Form::text('contact',$address->contact, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('contact'))
            <span class="help-block">
              <strong>{{ $errors->first('contact') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 
            </label>
            {{Form::text('address',$address->address, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 2
            </label>
            {{Form::text('address2',$address->address2, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div >
            <label for="form-field-1"> Country 
            </label>
            {{Form::text('country',$address->country, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
          <label for="form-field-1"> City 
            </label>
            {{Form::text('city',$address->city, array('id' => 'city','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> State 
            </label>
            {{Form::text('state',$address->state, array('id' => 'state','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1">Pin code 
            </label>
            {{Form::text('zip',$address->zip, array('id' => 'zip','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
      </div>
      <br>
      <br>
      <div style="float: right;margin: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="submit" id="update-add" class="btn btn-default add-to-order">Apply
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal for billing address change-->
<!-- {{$order->userid}} -->
<?php
                        $address2 = Address::where('user_id', $order->userid)->where('is_billing','=',1)->first();
                        // dd($address2);
                  ?>
<div id="billingAddressModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content editcustomer">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit Billing address
        </h4>
      </div>
      <div class="modal-body">

      <form id="customer-edit-billing">
        <input type="hidden" class="user_id" name="user_id" value="{{$order->userid}}" />
        <input type="hidden" class="user_id" name="order_id" value="{{$order->order_code}}" />

       <!--  <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> First Name 
            </label>
            {{Form::text('first_name',$order->first_name, array('id' => 'first_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> Last Name 
            </label>
            {{Form::text('last_name',$order->last_name, array('id' => 'last_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div> -->
          <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Company
            </label>
             @if($address2 !== null)
            {{Form::text('company',$address2->company, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('company',$address->company, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @endif
            @if ($errors->has('company'))
            <span class="help-block">
              <strong>{{ $errors->first('company') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Phone Number 
            </label>
            @if($address2 !== null)
            {{Form::text('contact',$address2->phone, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('contact',$address->phone, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @endif
            @if ($errors->has('contact'))
            <span class="help-block">
              <strong>{{ $errors->first('contact') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 
            </label>
            @if($address2 !== null)
            {{Form::text('address',$address2->address, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('address',$address->address, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 2
            </label>
            @if($address2 !== null)
            {{Form::text('address2',$address2->address2, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('address2',$address->address2, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div >
            <label for="form-field-1"> Country 
            </label>
            @if($address2 !== null)
            {{Form::text('country',$address2->country, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('country',$address->country, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
          <label for="form-field-1"> City 
            </label>
            @if($address2 !== null)
            {{Form::text('city',$address2->city, array('id' => 'city','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('city',$address->city, array('id' => 'city','class' => 'col-xs-12 col-sm-12'))}}
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> State 
            </label>
            @if($address2 !== null)
            {{Form::text('state',$address2->state, array('id' => 'state','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('state',$address->state, array('id' => 'state','class' => 'col-xs-12 col-sm-12'))}}
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> Pin code 
            </label>
            @if($address2 !== null)
            {{Form::text('zip',$address2->zip, array('id' => 'zip','class' => 'col-xs-12 col-sm-12'))}}
            @else
            {{Form::text('zip',$address->zip, array('id' => 'zip','class' => 'col-xs-12 col-sm-12'))}}
            @endif

          </div>
        </div>
      </div>
      <br>
      <br>
      <div style="float: right;margin: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="submit" id="update-billing-address" class="btn btn-default add-to-order">Apply
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="main-content">
  <div class="main-content-inner">
      <div class="main-content">
        <div class="main-content-inner">
          <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
              <li>
                <i class="ace-icon fa fa-home home-icon">
                </i>
                <a href="{{URL::to('/admin/order')}}">Order
                </a>
              </li>
              <li class="active">Order Detail
              </li>
            </ul>
            <!-- /.breadcrumb -->
          </div>
          <br>
          <div class="row">
            <form method="POST"  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
              <input name="_token" type="hidden" value="CtzGitGxF9mxRmfqSWVT2BIrCYC6cw8Qx14cRyjO">
              <div class="col-md-12">
                <div class="col-xs-12 mobileshow" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;position: inherit;  ">
                
                <div class="col-md-6">
              <span style="font-weight: 200;" >Order Status:</span>  <h2 style="color: #02c9c6;">{{$order->status_title}}</h2>
              <input type="hidden" name="ordercode" value="{{$order->order_code}}" >
                        
                       
                         </div>

                         <div class="col-md-6" style="float: right;text-align: right;">
                      <?php 
                        $month =$order->created_at;

                                        echo "Order Date: ".$newDateTime = date("jS F", strtotime($month));
                         ?>
                         <br>
                        
                            Order code : Baby_{{$order->id}}
                             <p style="font-weight: initial;">Payment Status: 
                        @if($order->payment_gateway_id == 1)
                         CCAvenue
                         @elseif($order->payment_gateway_id == 2)
                         Paytm
                         @else
                          Cash On Delivery  
                         @endif</p>
                                 
                                   
                    </div>
                    <div class="hr hr32 hr-dotted"></div>
                    <br>
                  <div class="widget-body">
                  
                         <div class="hr hr32 hr-dotted"></div>
                          <div class="user-detail"></div>
                          
                  <div class="user-detail-old">
                <div class="form-group col-md-4">
                  <label style="width: 100%;"> 
                    <b>Contacts  
                    </b> 
                     <span style="float: right;">  <a id="edit-email"> <i class="fa fa-pencil foredit" aria-hidden="true"></i></a>
                  </label>
                  <br>
                    <a href="{{URL::to('/admin/customers/'.$order->userid)}}">{{$order->first_name}} {{$order->last_name}}</a>
                    <br><br>
                    <a class="user_email">{{$order->email}}</a>
                    @if(isset($order->note) && $order->note != '')
                      <br><br>
                      <p>Customer Notes: {{$order->note}}</p>
                    @endif
                  </div>
               
                 
                <div class="form-group col-md-4 vertical-line">
                  <label class="col-sm-8" for="form-field-1"> DEFAULT ADDRESS 
                  </label>
                  <span class="col-sm-4" style="float: right;">  <a id="edit-address" style="float: right;"> <i class="fa fa-pencil foredit" aria-hidden="true"></i></a></span>
                  <div class="col-sm-12">

                  <BR>
                  @if($order->order_from == 1)
                      {{$order->first_name}} {{$order->last_name}}<BR>
                      {{$address->address}}<BR>
                      {{$address->address2}}<BR>
                      {{$address->zip}} {{$address->city}} {{$address->state}}<BR>
                      {{$address->country}} <BR>
                      @if($order->contact == NULL)
                      {{$address->phone}}
                      @else
                      {{$order->contact}}<BR>
                      @endif
                    @else
                      {{$order->first_name}} {{$order->last_name}}<BR>
                      {{$address->address}}<BR>
                      {{$address->address2}}<BR>
                      {{$address->zip}} {{$address->city}} {{$address->state}}<BR>
                      {{$address->country}} <BR>
                      @if($order->contact == NULL)
                      {{$address->phone}}
                      @else
                      {{$order->contact}}<BR>
                      @endif
                    @endif
                    
                  </div>
                </div>
                </div>
                
                <div class="form-group col-md-4">
                  <label class="col-sm-8" for="form-field-1"> BILLING ADDRESS 
                  </label>
                  <span class="col-sm-4" style="float: right;">  <a id="edit-address1" style="float: right;"> <i class="fa fa-pencil foredit" aria-hidden="true"></i></a></span>
                  <div class="billing-address"></div>
                  
                  @if($address2)
                  <div class="col-sm-12 oldbilling">
                  <BR>
                    {{$order->first_name}} {{$order->last_name}}<BR>
                    {{$address2->address}}<BR>{{$address2->address2}}<BR>
                    {{$address2->zip}} {{$address2->city}} {{$address2->state}}<BR>
                    {{$address2->country}} <BR>
                      {{$address2->phone}}
                  </div>
                  @else
                  <div class="col-sm-12 oldbilling">
                    Same as Shipping Address
                  </div>
                  @endif

                </div>
                

                        <div class="widget-main no-padding">
                          <table class="table table-bordered table-striped">
                            <img src="{{URL::to('images/logo.svg')}}" style="display: none;" id="logoimg" alt="logohere"/>

                            <tbody>
                            @foreach($order_detail as $detail)
                            
                            <?php
                                   $varaitionimage1 = DB::table('attribute_value')
                                              ->join('product_variations','product_variations.attribute_value_id','=','attribute_value.id','left')
                                              ->select('attribute_value.*','product_variations.id as varid')
                                              ->where('product_variations.product_id' , '=' ,$detail->product_id)
                                              ->where('attribute_value.value' , '=' ,$detail->pattern_name)
                                              ->first();

            // echo $detail->product_id;

                                  if($varaitionimage1)
                  {
        $varaitionimage = DB::table('product_variation_images')
                          ->select('product_variation_images.*')
                          ->where('variation_id' , '=' ,$varaitionimage1->varid)->first();
                          ?>
        <tr class="product">
          <td class="product__image">
            <div class="product-thumbnail">
              <div class="product-thumbnail__wrapper">
              @if(isset($varaitionimage->images))
                <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/'.$varaitionimage1->id.'/'.$varaitionimage->images)}}" style="width: 50px" />
             
                  @else
                  
                   <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" style="width: 50px" />
             <!--    <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" /> -->
              @endif
              </div>
              <!-- <span class="product-thumbnail__quantity" aria-hidden="true">{{$detail->quantity}}</span> -->
            </div>
          </td>
          <?php
          }
        else{
          ?>
          <td class="product__image">
            <div class="product-thumbnail">
              <div class="product-thumbnail__wrapper">
              
                  
                   <img alt="{{$detail->product_title}}" class="product-thumbnail__image" src="{{URL::to('/product_images/'.$detail->product_id.'/featured_images/'.$detail->product_images)}}" style="width: 50px" />
            
              </div>
              <!-- <span class="product-thumbnail__quantity" aria-hidden="true">{{$detail->quantity}}</span> -->
            </div>
          </td>
          <?php
        }                 
      ?>
                                <td>{{$detail->product_title}}
                                  <br>
                                 @if($detail->pattern_name)
                  @if ($detail->pattern_name == 'No Swatch')
                     @else
                  Pattern:{{$detail->pattern_name}}
                  @endif
            
                  @endif
                  <br>
                   @if($detail->text)
                  Text:{{$detail->text}}
                  <br>
                  @endif
                  @if($detail->font)
                  Font:{{$detail->font}}
                  <br>
                  @endif
                   @if($detail->color)
                  Color:<div class="colordva" style="background-color:{{$detail->color}};"> </div>
                  <br>
                  @endif
                   @if($detail->pattern_color)
                  Pattern color:{{$detail->pattern_color}}
                  @endif

                                <td>
                                  <p>Rs. {{$detail->price}} x {{$detail->quantity}}</p>
                                </td>

                                <td class="hidden-480">
                                  <span >Rs. {{$detail->price*$detail->quantity}}</span>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div><!-- /.widget-main -->
                      </div>
                      <br>

                  <div class="form-group">
                    
                    <div class="col-sm-7">
                      <div class="form-group">
                        <label class="col-sm-12" for="form-field-1"> Order Note 
                        </label>
                        <div class="col-sm-12">
                          <textarea name="notes" id="form-field-1" value="{{$order->notes}}" placeholder="Add a Note" class="col-xs-10 col-sm-12 customer-notes" rows="6" cols="50"></textarea> 
                          <br><br><button class="btn btn-primary submit_notes">Save</button>
                        </div>
                      </div>

                    </div>
                    <div class="col-md-5">
                    
                        <div class="widget-main no-padding">
                          <table class="table table-bordered table-striped">
                            

                            <tbody>
                              <tr>
                                <td> Subtotal </td>
                                <td> Rs. {{$order->amount}} </td>
                              </tr>
                              <tr>
                                <td> Discount: {{$order->coupon_code}}</td>
                                @if($order->discount == NULL)
                                <td>Rs. 0.00</td>
                                @else
                                <td>Rs. {{$order->discount}}</td>
                                @endif
                              </tr>

                              <tr>
                                <td> Shipping: - {{$order->shipping}} </td>
                                @if($order->shipping ==='free')
                                <td>Free</td>
                                @else
                                <td>Rs. {{$order->custom_rate_value}}</td>
                                @endif
                              </tr>
                              <tr>
                                <td> VAT </td>
                                @if($order->tax_rate == NULL)
                                <td>Rs. 0.00</td>
                                @else
                                <td>Rs. {{round($order->tax_rate , 2)}}</td>
                                @endif
                              </tr>
                              <tr>
                                <td> <b>Total</td>
                                <td>Rs. {{round($order->payble_amount , 2)}}</td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </div>

                    </div>
                  </div>
                   <hr>
                   <h5 class="col-md-8"><i class="ace-icon fa fa-file-pdf-o bigger-510"></i>  PRINT INVOICE</h5> 
              <button style="float: right;" class="btn btn-info email-invoice">PRINT INVOICE <i class="fa fa-spinner fa-spin email-invoice" style="font-size:18px; display:none;"></i></button>
            <br><br>
           
                  <div class="hr hr32 hr-dotted"></div>
                    <div class="col-md-10" >
                      <h5>PAYMENT OF <strong>RS. {{$order->payble_amount}}</strong> is {{strtoupper($order->payment_status_name)}}.</h5>
                    </div>
                    <div class="hr hr32 hr-dotted"></div>
                    <div class="col-md-10" >
                    <!-- <h5>ALL ITEMS WERE SHIPPED</h5> -->
                    </div>
                  <!-- <div class="col-md-2" >
                  
                  </div> -->


                  @if($order->status == 2)
                    <div class="col-md-2" >

                        <button class="btn btn-info restock">
                          <i class="ace-icon fa fa-check bigger-110">
                          </i>
                          Restock
                        </button>
                    </div>
                  @endif
                  
                  
                </div>
                <br>
                <br>

              </div>
              
                <!-- <div class="col-md-4 background-layout" style="margin-top: 3%; "> 

                <div class="form-group">
                 <label class="col-sm-8" for="form-field-1"> Tags
                  </label>
                  <span class="col-sm-4" style="float: right;">  <a href="" style="float: right;"> View All Tags</a></span>
                  <div class="col-sm-12">
                    <input type="text" name="weight" id="form-field-1" placeholder="VIP, sale shopper, etc." class="col-xs-10 col-sm-12" />
                  </div>
                </div>
                @if(isset($tags))
                <div>
                  @foreach($tags as $tag)
                    <h6>{{$tag->name}}</h6>
                  @endforeach
                </div>
                @endif
              </div> -->
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <br>
  
  <h4>Comment</h4><br>
  <form id="postComment">
    <textarea name="comment" id="comment" rows="8" cols="100"></textarea><br>
    <input type="submit" value="Post" class="btn post-comment">
  </form>
<!-- 
  <div class="order-comment ">
    @if(isset($ordercomment))
      @foreach($ordercomment as $comment)
      <li style="list-style-type: none;">
        <h4><stong>{{$comment->name}}</stong></h4>
        <span><i class="menu-icon fa fa-file-text-o">
                </i> -> {{$comment->comment}}</span><span style="float: right;">({{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}})</span>
      </li><hr>
      @endforeach
    @endif
  </div> -->
  <div class="ui-feed order-comment">
  <div id="timeline_container" refresh="timeline_container" class="ui-feed__section">
   


        <span class="timeline__event-anchor" id="timeline__event-anchor56638436227"></span>
            @if(isset($ordercomment))
                    @foreach($ordercomment as $comment)
            <div id="event56638436227" class="ui-feed__timeline ui-accordion__toggler" define="{'accordion56638436227': Shopify.UIAccordion.for($(this).children('.ui-accordion:first')[0])}" bind-class="{'ui-feed__item--revealed': accordion56638436227.isExpanded(), 'ui-accordion__toggler': true}">
              <div class="ui-feed__item ui-feed__item--message  ui-feed__item--accordion-activator" data-accordion-toggler-for="a1291b97" id="ui-accordion__toggler--1" aria-expanded="false" aria-controls="ui-accordion__panel--1">
                  <span class="ui-feed__marker ui-feed__marker--user-action"></span>

                <span class="ui-feed__message">
                  
                    
                
                    <div class="timeline__message-container">
                   <span class="timeline__inner-message timeline__inner-message--indicator">
                      <h4><stong>{{$comment->name}}</stong></h4>
                      <span>{{$comment->comment}}</span>
                    
                    </span>
                    <time id="timeline__time-56638436227" class="timeline__time" refresh="timeline__time" style="float: right;">
                      
                        ({{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}})<span bind-show="Shopify.Time.isDifferentTimezone()" class="hide"> IST</span>
                        
                     
                    </time>
                    </div>
                    
                  

              </span>

              </div>
                
            </div>
             @endforeach
                      @endif
        
        
        
  </div>
</div>
  <div class="show-comment"></div>
  <!-- /.page-content -->
</div>




@stop

@section('scripts')
  <script>
  $('.post-comment').click(function(e){
    var base_url = "{{URL::to('/')}}";
    e.preventDefault();
    $.ajax({
          url: base_url+'/admin/order/post-comment',
          type: "post",
          data: 
          {
            "order_code": "{{$order->order_code}}",
            "comment": $('#comment').val()
          },
          success:function(data)
          {
            $('.show-comment').html(data);
            $('.order-comment').remove();
            $('#comment').val("");
            toastr.info("Comment added.", {timeOut: 10000})      

          }
      });
  });

  $('.submit_notes').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    var notes = $('.customer-notes').val();
    var order_id = "{{$order->id}}";

    $.ajax({
      url: base_url+'/admin/order/add-notes',
      type: "post",
      data: 
      {
        "notes": notes,
        "order_id": order_id
      },
      success:function(data)
      {
        toastr.success(data.message, {timeOut: 10000});
      }
    });

  });

  // Restock Product
  $('.restock').click(function(e){
    var base_url = "{{URL::to('/')}}";
    e.preventDefault();
    $('.restock').attr('disabled','disabled');

    $.ajax({
          url: base_url+'/admin/product/restock',
          type: "post",
          data: 
          {
            "order_id": "{{$order->id}}",
          },
          success:function(data)
          {
            toastr.info(data.message, {timeOut: 10000})      
            $(".restock").removeAttr('disabled');
          }
      });
  });

  $('.email-invoice').click(function(e){

    // alert("fdfdfd");

    e.preventDefault();
    // var base_url = '{{URL::to('/')}}';
      // alert("clckced");
var img = $('#logoimg').attr('src');
// alert(img.getAttribute('src')); // foo.jpg
// alert(img); 

      var image = $('#imagename').val();
      console.log(image);
      $.ajax({
          url: '{{URL::to("/mail/order/invoice")}}',
          type: "post",
          data: 
          {
            "order_code": "{{$order->order_code}}",
            "logoimg": img,
          },
          success:function(responseInvoice)
          {
            var printContents = responseInvoice;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;

            window.print();
           
           setTimeout(function(){window.close();}, 4);
            document.body.innerHTML = originalContents;
             location.reload();
            return false
            // window.print();
            // console.log(data);
          }
      });
    });

  </script>
  <script>
    $('#edit-email').click(function(){
    $('#userEmailModal').modal('show');
  });

  $('#edit-address').click(function(){
    $('#userAddressModal').modal('show');
  });

  $('#edit-address1').click(function(){
    $('#billingAddressModal').modal('show');
  });

    $('#update-add').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/add-customer1',
        type:'POST',
        data:$("#customer-edit-shipping").serialize(),
        success:function(data){
          if (data.status == 0) {
            toastr.success(data.message, {timeOut: 100000})
            return;
          };
          $('.user-detail-old').hide();
          $('.user-detail').html(data);
          $('#userAddressModal').modal('hide');
          // $('.modal-backdrop.in').css('dispplay','none');
          $('.modal-backdrop.fade.in').css('position','inherit');
          toastr.success('Shipping address updated.', {timeOut: 100000});
          e.preventDefault();

        }
      });
  });
    $('#update-billing-address').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/update-billing-address1',
        type:'POST',
        data:$("#customer-edit-billing").serialize(),
        success:function(data){
          $('.oldbilling').hide();
          $('.billing-address').html(data);
          // alert(data);
          $('#billingAddressModal').modal('hide');
          // $('.modal-backdrop.in').css('dispplay','none');
          $('.modal-backdrop.fade.in').css('position','inherit');
          toastr.success('Billing address updated.', {timeOut: 100000})
        }
      });
  });

  $('.change-email').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/change-customer-email',
        type:'POST',
        data:{
          'id': $('.user_id').val(),
          'email': $('#email').val()
        },
        success:function(data){
          if (data.status == 1) {
            $('#userEmailModal').modal('hide');
            $('.user_email').text(data.email);
            $('.modal-backdrop.in').modal('hide');
            $('.modal-backdrop.in').css('opacity','0');
            toastr.success('Email address updated.', {timeOut: 100000})
          }else{
            toastr.success('User Not Found.', {timeOut: 100000})
          }
        }
      });
  });
  </script>
@endsection