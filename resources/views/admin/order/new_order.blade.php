@extends('admin.admin_layout')


@section('content')

<style>
#successpara{ float: left; margin-right: 10px; }
.successpostnoti{
                display: none;
                background: green !important;
    padding: 15px;
    border-radius: 9px;
    opacity: 0.6;
    position: absolute;
    top: 7%;
    width: auto;
    right: 4%;
    color: white;
    z-index: 9999999;
}
 #toast-container > div {
   /*background: black !important;*/
   position: absolute !important;
   top: 31px;
   right: 4%;
}
   
}
ul
{
list-style-type: none !important;
}
.products-c:hover
{
  background: #eff9fd;
    box-shadow: 0 1px 0 #ebeef0;
}
.hide
{
  display: none;

}
.show
{
  display: block;
}
.popover 
{
  width: 100%;
    padding: 5%;
}
input[type=checkbox], input[type=radio] {
    cursor: pointer;
}
.embed-responsive, .modal, .modal-open, .progress {
    overflow: inherit;
}
.category_name
{
  border-radius: 3px!important;
    color: #858585;
    background-color: #f1f1f1;
    /* border: 1px solid #D5D5D5; */
    padding: 11px 13px 13px;
    font-size: 14px;
    font-family: inherit;
    -webkit-box-shadow: none!important;
    box-shadow: none!important;
    -webkit-transition-duration: .1s;
    transition-duration: .1s;
    cursor: pointer;
    background-image: url(http://www.lexisnexis.com/risk/international/images/arrow_a.png);
    background-position: right 10px;
    background-repeat: no-repeat;
    /* background-position: 100% 14%; */
    background-size: 22px 19px;
}
.tag_name
{
  border-radius: 3px!important;
    color: #858585;
    background-color: #f1f1f1;
    /* border: 1px solid #D5D5D5; */
    padding: 11px 13px 13px;
    font-size: 14px;
    font-family: inherit;
    -webkit-box-shadow: none!important;
    box-shadow: none!important;
    -webkit-transition-duration: .1s;
    transition-duration: .1s;
    cursor: pointer;
    background-image: url(http://www.lexisnexis.com/risk/international/images/arrow_a.png);
    background-position: right 10px;
    background-repeat: no-repeat;
    /* background-position: 100% 14%; */
    background-size: 22px 19px;
}
.products-c
{
  border-radius: 3px!important;
    color: #858585;
    background-color: #f1f1f1;
    /* border: 1px solid #D5D5D5; */
    padding: 11px 13px 13px;
    font-size: 14px;
    font-family: inherit;
    -webkit-box-shadow: none!important;
    box-shadow: none!important;
    -webkit-transition-duration: .1s;
    transition-duration: .1s;
    cursor: pointer;
    background-image: url(http://www.lexisnexis.com/risk/international/images/arrow_a.png);
    background-position: right 10px;
    background-repeat: no-repeat;
    /* background-position: 100% 14%; */
    background-size: 22px 19px;
        margin-bottom: 7px !important;
}
.vendor_name
{
  border-radius: 3px!important;
    color: #858585;
    background-color: #f1f1f1;
    /* border: 1px solid #D5D5D5; */
    padding: 11px 13px 13px;
    font-size: 14px;
    font-family: inherit;
    -webkit-box-shadow: none!important;
    box-shadow: none!important;
    -webkit-transition-duration: .1s;
    transition-duration: .1s;
    cursor: pointer;
}
.chosen-container-single .chosen-search {
    position: relative;
    z-index: 1010;
    margin: 0;
    padding: 6px 6px;
    white-space: nowrap;
    /* display: none; */
}

</style>

<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{URL::to('/admin/order')}}">Order</a>
          </li>
          <li class="active">Create Order</li>
        </ul><!-- /.breadcrumb -->
      </div>
      <br>
      <div>
       @if(Session::has('success'))

        {!! Session::get('success') !!}
       @endif
      </div>
      <div class="row">
        
            {{ Form::open(['url' => 'admin/order/new', 'method' => 'post', 'class' => 'form-horizontal']) }}
            <br>
            
            <div class="col-md-8" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);">
            
            
            <div class="form-group">
              <label class="col-sm-12" for="form-field-1"> Order details </label>
              
              <button class="btn btn-info browse_product" type="button" style="float: right;">
                  
                  Browse Product
                </button>
              <br>  
              <div class="col-sm-9">
              {{Form::text('product_name',null, array('id' => 'product-name','class' => 'orderinputaj col-xs-12 col-sm-12', 'style' => 'height:44px' , 'placeholder'=>'Find Products....'))}}
                <!-- <input type="text" name="product_name" id="form-field-1" placeholder="" class="col-xs-12 col-sm-5" /> -->
               @if ($errors->has('product_name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('product_name') }}</strong>
                                  </span>
                              @endif
              </div>
            </div>
            <div class="order-detail">

            </div>
            <div class="form-group previous_div">
            <div class="col-sm-8">
              <label for="form-field-1"> Notes </label><br>
                <input type="text" name="notes" id="form-field-1" placeholder="Add a note..." class="col-xs-12 col-sm-12 order-notes" />
                </div>
                <br>
              <div class="col-sm-4">
              Subtotal   <span style="float: right;" >0.00</span> <br><br>
                Add Discount<span style="float: right;" id="discount_value">--- </span><br><br>
              Add Shipping  <span style="float: right;">---</span><br><br>
              Taxes       <span style="float: right;">0</span> <br><br>
              <storng>Total</storng>   <span style="float: right;" class="final-total-amount-old">0.00</span> <br><br>
              </div>
            </div>

            <hr>

            <!-- <h5 class="col-md-8"><i class="ace-icon fa fa-file-pdf-o bigger-510"></i>  PRINT INVOICE</h5> 
              <button style="float: right;" class="btn btn-info email-invoice" disabled="disabled">PRINT INVOICE <i class="fa fa-spinner fa-spin email-invoice" style="font-size:18px; display:none;"></i></button>
            <br><br>
            <hr> -->
            <h5 class="col-md-12"><i class="ace-icon fa fa-credit-card"></i>  ACCEPT PAYMENT</h5>
            <hr>
            <br>
            <div class="form-group">

              <div class="col-md-offset-2 col-md-10">
                <button class="btn btn-info save-as-draft create-order" onclick="createOrder('5')" type="button">
                  <i class="ace-icon fa fa-check bigger-110"></i>
                  Save as Draft
                </button>
                <button class="btn btn-info mark-paid create-order" type="button" onclick="createOrder('2')">
                  <i class="ace-icon fa fa-check bigger-110"></i>
                  Mark as Paid
                </button>
                <button class="btn btn-info mark-pending create-order" type="button" onclick="createOrder('5')">
                  <i class="ace-icon fa fa-check bigger-110"></i>
                  Mark as Pending
                </button>
              </div>
            </div>
            </div>

            <div class="col-md-4" >
            <div class="col-md-12" style="padding: 7%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1); margin-bottom: 7%;">
            <div class="form-group">
              <label class="col-sm-12" for="form-field-1"> Find or create a customer </label>

              <div class="col-sm-12" >
              <div> 
                {{Form::select('customer_name', ['' => 'Select Customer'] + $customer,null, array('id' => 'customer_name','class' => 'col-xs-12 col-sm-12'))}}
                
                {{ Form::close() }}
              </div>
              <div class="user-detail" style="padding-top: 10%;">


              </div>
              <div id="add_new_customer" class="btn btn-primary">Add Customer
                <style type="text/css">
                  #toast-container>div {
                    background-color: green!important;
                    /*opacity: none !important;*/
                    color: white !important;
                    /* display: block !important; */
                }

                </style>
              </div>
              </div>
              </div>
            </div>

            
            <br>
            <div class="col-md-12" style="padding: 7%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1); margin-bottom: 7%;">
            <div class="form-group">
              <div class="col-sm-12" class="form-control">
              <label for="form-field-1"> Tags </label>
                <select name="order_tags[]" id="order-tag" multiple style="width:100%;">
                  @foreach($order_tags as $tag )
                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            </div>

            </div>
          
        <!-- </div> -->
      </div>
      <!-- <div class="col-md-6">
      
      </div>
      <div class="col-md-6">
      <div class="order-detail">
        
      </div> -->
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      
<!-- Modal content-->
        <div class="modal-content">
        <style type="text/css">
  #toast-container>div {
    background-color:green!important;
    /*opacity: none !important;*/
    color: white !important;
    /* display: block !important; */
}

</style>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <a><i id="backspace" class="fa fa-arrow-circle-left fa-2x" style="display:none; float: left;margin-top: 1px; margin-right: 13px;"></i></a> <h4 class="modal-title">   Select Products</h4>
          </div>
          <div class="modal-body" >
            <input type="hidden" id="store-id" name="store_id"> 
            {{Form::text('product_name',null, array('id' => 'product-name','class' => 'col-xs-12 col-sm-12 search-product-name' , 'placeholder'=>'Search....'))}}
              <br><br><br>
              
              <div class="product-list-types" id="modal_body_neworder" >
              </div>
              <div class="option-list">
            <h5 class="all-products products-c" style="border-top: #ccc 1px solid;padding: 2%;border-bottom: #ccc 1px solid;cursor: pointer;margin-bottom: 0;margin-top: 0;"><span class="all_product_types_back"></span> All Products</h5>
                  <!-- <h5 class="popular-products products-c">Popular Products</h5> -->
                  <h5 class="collection-products products-c" style="padding: 2%;border-bottom: #ccc 1px solid;cursor: pointer;margin-bottom: 0;margin-top: 0;"><span class="all_product_types_back"></span>Categories</h5>
                  <!-- <h5 class="product-types products-c">Product Types</h5> -->
                  <h5 class="product-tags products-c" style="padding: 2%;border-bottom: #ccc 1px solid;cursor: pointer;margin-bottom: 0;margin-top: 0;">Tags</h5>
                  
                  </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-default add-to-order">Add to Order</button>
          </div>
        </div>

    </div>
  </div>
  <!---add customer popup start---->
<!-- Modal for add new customer-->

<div id="new_customer_form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div id="response">
    </div>
    <!-- Modal content-->
    <form id="customerForm">
      <div class="modal-content" style="overflow: auto;height: 600px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
          <h4 class="modal-title">Create New Customer 
          </h4>
        </div>
        <div class="modal-body" >
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> First Name 
              </label>
              {{Form::text('first_name',null, array('id' => 'first_name','class' => 'col-xs-12 col-sm-12'))}}
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> Last Name 
              </label>
              {{Form::text('last_name',null, array('id' => 'last_name','class' => 'col-xs-12 col-sm-12'))}}
            </div>
          </div>
          <div class="form-group col-md-12">
            <div>
              <label for="form-field-1"> Email
              </label>
              {{Form::text('email',null, array('id' => 'email','class' => 'col-xs-12 col-sm-12'))}}
            </div>
          </div>
         
          <div class="form-group col-md-12">
            <label for="form-field-1"> SHIPPING ADDRESS 
            </label>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> Company 
              </label>
              {{Form::text('company',null, array('id' => 'company','class' => 'col-xs-12 col-sm-12'))}}
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> Mobile Number 
              </label>
              {{Form::number('contact',null, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
              @if ($errors->has('contact'))
              <span class="help-block">
                <strong>{{ $errors->first('contact') }}
                </strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> Address 
              </label>
              {{Form::text('address',null, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> Address 2
              </label>
              {{Form::text('address2',null, array('id' => 'address2','class' => 'col-xs-12 col-sm-12'))}}
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> Country 
              </label>
              {{Form::text('country','India', array('id' => 'address','class' => 'col-xs-12 col-sm-12' , 'disabled'))}}
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> City <span style="color: red">*</span>
              </label>
              {{Form::text('city',null, array('id' => 'city','class' => 'col-xs-12 col-sm-12','required' => 'required'))}}
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> State <span style="color: red">*</span>
              </label>
              {{Form::text('state',null, array('id' => 'state','class' => 'col-xs-12 col-sm-12','required' => 'required'))}}
            </div>
          </div>
          <div class="form-group col-md-6">
            <div>
              <label for="form-field-1"> Pin code <span style="color: red">*</span>
              </label>
              {{Form::number('zip',null, array('id' => 'zip','class' => 'col-xs-12 col-sm-12','required' => 'required'))}}
            </div>
          </div>

        </div>
        <br>
        <br>

       <div class="col-md-12" style="float: right;margin-top: 3%;margin-bottom: 3%;">
       <div class="hr hr32 hr-dotted"></div>
  <div style="float: right;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
          </button>
          <button type="button" class="btn btn-default add-customer-new">Save customer <i class="fa fa-spinner fa-spin add-customer-loader" style="display:none;"></i>
          </button>
        </div>
</div>
      </div>
    </form>
  </div>
</div>
<script src="{{URL::to('admin-assets/js/chosen.jquery.min.js')}}"></script>

<script>
  $("#customer_name").chosen({no_results_text: "Oops, nothing found!"});
  $("#vendor_name").chosen({no_results_text: "Oops, nothing found!"});
  
</script>


  <!---add customer popup end---->
  @section('scripts')
    <script>
    var base_url = '{{URL::to('/')}}';

    $('body').on('click', '.all_product_types_back', function(){
      alert('back');
      var data = $('.modal-body').html();
      console.log(data);
      // $('#myModal').modal('show');
    });

    
    $('.browse_product').click(function(){
      var store_id = $('#vendor_name').val();
      if (store_id == 0) {
        toastr.info("Please select store", {timeOut: 1000000})
        return;

      };
      $('#store-id').val(store_id);
      var store_id = $('#store-id').val();
      // $('input:checkbox').removeAttr('checked');
      $('#myModal').modal('show');
    });
    // Show all products
    $('#product-name').keyup(function(){
      $('input:checkbox').removeAttr('checked');
      var store_id = $('#vendor_name').val();

      if (store_id == 0) {
        toastr.info("Please select store", {timeOut: 1000000})
        return;
      };
      $('#store-id').val(store_id);
      var store_id = $('#store-id').val();
      var heading = $('.all-products').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $(".product-list-types").show();
        $(".option-list").hide();
        $("#backspace").css('display' , 'block');
      $('.product-list-types .products-c').remove();
      var pName = $('#product-name').val();
        $.ajax({
          url: base_url+'/admin/ajax/show_product',
          type: "post",
          data: 
          {
            "pName": pName,
            "store_id": store_id
          },
          success:function(data)
          {
            $('.product-list-types').html(data);
          }
      });
    });
    // Show all products
    $('.all-products').click(function(){
      var heading = $('.all-products').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $('.product-list-types .products-c').remove();
      var store_id = $('#store-id').val();

      $.ajax({
          url: base_url+'/allproducts',
          type: "get",
          data: 
          {
                "store_id": store_id,
            },
          success:function(data)
          {
          $('.product-list-types').html(data);
          }
      });
    });

    // Show all product category
    $('.collection-products').click(function(){
      var heading = $('.collection-products').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $('.product-list-types .products-c').remove();
      $.ajax({
          url: base_url+'/allcategories',
          type: "get",
          data: 
          {
                // "id": product_id,
            },
          success:function(data)
          {
          $('.product-list-types').html(data);
          }
      });
    });

    function allProductsByCategory(obj){
      var category_id = $(obj).attr('data-id');
      var heading = $('.category_name').html();
      var heading = $('.category_name').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $('.product-list-types .products-c').remove();
      $.ajax({
          url: base_url+'/allproductsbycategory',
          type: "get",
          data: 
          {
                "category_id": category_id,
            },
          success:function(data)
          {
          $('.product-list-types').html(data);
          }
      });
    }

    function allProductsByTags(obj){
      var tag_id = $(obj).attr('data-id');
      var heading = $('.tag_name').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $('.product-list-types .products-c').remove();
      $.ajax({
          url: base_url+'/allproductsbytags',
          type: "get",
          data: 
          {
                "tag_id": tag_id,
            },
          success:function(data)
          {
          $('.product-list-types').html(data);
          }
      });
    }

    function allProductsByVendor(obj){
      var vendor_id = $(obj).attr('data-id');
      var heading = $('.tag_name').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $('.product-list-types .products-c').remove();
      $.ajax({
          url: base_url+'/allproductsbyvendor',
          type: "get",
          data: 
          {
                "vendor_id": vendor_id,
            },
          success:function(data)
          {
          $('.product-list-types').html(data);
          }
      });
    }

    // Show all vendors
    $('.vendors').click(function(){
      var heading = $('.vendors').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $('.product-list-types .products-c').remove();
      $.ajax({
          url: base_url+'/allvendors',
          type: "get",
          data: 
          {
                // "id": product_id,
            },
          success:function(data)
          {
          $('.product-list-types').html(data);
          }
      });
    });   

    // Show all tags
    $('.product-tags').click(function(){
      var heading = $('.product-tags').html();
      $('.modal-title').html(heading);
      $('#myModal').modal('show');
      $('.product-list-types .products-c').remove();
      $.ajax({
          url: base_url+'/alltags',
          type: "get",
          data: 
          {
                // "id": product_id,
            },
          success:function(data)
          {
          $('.product-list-types').html(data);
          }
      });
    });   

     function getProductIds() {
         var allVals = [];
         $('.pid :checked').each(function() {
           allVals.push($(this).val());
         });
         return allVals;
      }

     function getProductQuantity() {
         var allQty = [];
         $('.quantity').each(function() {
           allQty.push($(this).val());
         });
         return allQty;
      }


    $('.add-to-order').click(function(){
      var product_ids = getProductIds();
      var totalamount = $(".totalamount").val();
      $.ajax({
          url: base_url+'/admin/add-to-order',
          type: "post",
          data: 
          {
                "product_ids": product_ids,
                "store_id": $('#vendor_name').val()
            },
          success:function(data)
          {
          $(".previous_div").hide();
          $('.order-detail').html(data);
          $('#myModal').modal('hide');
          $(".final-total-amount").html(totalamount);

          }
      });
      
    });

      // Get Customer Detail
      $('#customer_name').change(function(){

        $('.email-invoice').prop("disabled", false);
        var customerid = $('#customer_name').val();
        if (!customerid) {
          $('.user-detail').html('');
        }

      $.ajax({
          url: base_url+'/admin/ajax/customer',
          type: "post",
          data: 
          {
                "customerid": customerid,
            },
          success:function(data)
          {
          $('.user-detail').html(data);
          }
      });
      });

    </script>

    <script>
      $( "#backspace" ).click(function() {
         $(".product-list-types").hide();
         $(".option-list").show();
         $("#backspace").css('display' , 'none');
      });
    </script>
    
    <script>
      $( ".products-c" ).click(function() {
         $(".product-list-types").show();
         $(".option-list").hide();
         $("#backspace").css('display' , 'block');
      });
    </script>

    <script>
    var i=1;
      function createOrder(status) {
        var cid = $('#cid').val();
        if (!cid && $("#customer_name").val()=="") {
            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "2000",
                                "hideDuration": "5000",
                                "timeOut": "1050",
                                "extendedTimeOut": "1700",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              }
          toastr.error("Select Customer", {timeOut: 100000});
          return;
        }
        $('.create-order').attr('disabled','disabled');
        var product_ids = getProductIds();
        var qty = getProductQuantity();       
        var deleted_value = $('.deleted_value').text();
        var new_product_id = $(product_ids).not([deleted_value]).get();
        var vendor_id = $('#vendor_name').val();
        var order_tag = $('#order-tag').val();
        if (cid) {
          var customer_id = cid;
        }else{
          var customer_id = $('#customer_name').val();
        }
        var discount = $('#discountPrice').val();
        var discount_type = $('#discount_type').val();
        var total_amount = $('.total-amount').html();
        var final_total_amount = $(".final-total-amount").html();
        var discount_value = $("#discount_value").html();
        var reason_value = $("#reason_value").html();
        var order_notes = $('#order-notes').val();
        var shipping = $('.shipping').val();
        var custom_ship_name = $('#custom_ship_name').val();
        var custom_ship_value = $('#custom_ship_value').val();
        var tax = $('#tax_value').html();
        $.ajax({
            url: base_url+'/admin/order/new',
            type: "post",
            data:
            {
                  "product_ids": new_product_id,
                  "quantitynew": qty,
                  "vendor_id": vendor_id,
                  "customer_id": customer_id,
                  "total_amount": total_amount,
                  "discount": discount,
                  "discount_type": discount_type,
                  "final_total_amount": final_total_amount,
                  "discount_value": discount_value,
                  "order_tag": order_tag,
                  "reason_value": reason_value,
                  "shipping" : shipping,
                  "custom_ship_name" : custom_ship_name,
                  "custom_ship_value" : custom_ship_value,
                  "tax" : tax,
                  "notes_value": order_notes,
                  "status": status
              },
            success:function(data)
            {
              if (data.status == 1) {
                toastr.success(data.message, {timeOut: 100000})
                $('.create-order').removeAttr('disabled');
                $('toast-container').css('background', 'green');

              };

              if (data.status == 6) {
               $('.successpostnoti').prepend("<p id='successpara'>Order saved as draft! </div>").show( "slow",function(){
               var audio = new Audio('{{URL::to('web-assets/youve-been-informed.ogg')}}');
                 audio.play();
                     } ).delay(5500).fadeOut(1500);
                window.location.reload(true);
                $('toast-container').css('background', 'green');
              };
              
              if (data.status == 2) {
               $('.successpostnoti').prepend("<p id='successpara'>Order saved as paid! </div>").show( "slow",function(){
               var audio = new Audio('{{URL::to('web-assets/youve-been-informed.ogg')}}');
                 audio.play();
                     } ).delay(5500).fadeOut(1500);
                window.location.reload(true);
                $('toast-container').css('background', 'green');
              };
              
              if (data.status == 5) {
               $('.successpostnoti').prepend("<p id='successpara'>Order saved as pending! </div>").show( "slow",function(){
               var audio = new Audio('{{URL::to('web-assets/youve-been-informed.ogg')}}');
                 audio.play();
                     } ).delay(5500).fadeOut(1500);
                window.location.reload(true);
                $('toast-container').css('background', 'green');
              };
            }
        });
        i++;
        }
    </script>
    
    <script>
    $('.popover-markup>.trigger').popover({
        html: true,

        title: function () {
            return $(this).parent().find('.head').html();
            
        },
        content: function () {
            return $(this).parent().find('.content').html();
        }
    });

    $('.popover-markup1>.trigger').popover({
        html: true,
        title: function () {
            return $(this).parent().find('.head').html();
             $('.popover-markup').addClass('hide');
            $('.popover-markup').removeClass('show');
            $('.popover-markup1').removeClass('hide');
            $('.popover-markup1').addClass('show');
        },
        content: function () {
            return $(this).parent().find('.content').html();
        }
    });

    $('.search-product-name').keyup(function(){
      var pName = $('.search-product-name').val();
      var store_id = $('#store-id').val();

      $.ajax({
        url: base_url+'/admin/ajax/show_product',
        type: "post",
        data: 
        {
          "pName": pName,
          "store_id": store_id
        },
        success:function(data)
        {
          $('.product-list-types').html(data);
        }
      });
    });


      // Send invoice to user using email
    $('.email-invoice').click(function(e){
        e.preventDefault();
        $("i.email-invoice").css('display' , 'block');

        var product_ids = getProductIds();
        var deleted_value = $('.deleted_value').text();
        var new_product_id = $(product_ids).not([deleted_value]).get();

        var qty = getProductQuantity();       
        var vendor_id = $('#vendor_name').val();
        var cid = $('#cid').val();

        if (cid) {
          var customer_id = cid;
        }else{
          var customer_id = $('#customer_name').val();
        }

        var discount = $('#discountPrice').val();
        var discount_type = $('input[name=discount_by]:checked').val();
        var total_amount = $('.total-amount').html();
        var final_total_amount = $(".final-total-amount").html();
        var discount_value = $("#discount_value").html();
        var reason_value = $("#reason_value").html();
        var order_notes = $('#order-notes').val();

        var shipping = $('input[name=shipping]:checked').val();
        var custom_ship_name = $('#custom_ship_name').val();
        var custom_ship_value = $('#custom_ship_value').val();
        var tax = $('input[name=tax]:checked').val();


      $.ajax({
          url: base_url+'/admin/order/send-invoice',
          type: "post",
          data: 
          {
            "product_ids": product_ids,
            "quantitynew": qty,
            "vendor_id": vendor_id,
            "customer_id": customer_id,
            "total_amount": total_amount,
            "discount": discount,
            "discount_type": discount_type,
            "final_total_amount": final_total_amount,
            "discount_value": discount_value,
            "reason_value": reason_value,
            "shipping" : shipping,
            "custom_ship_name" : custom_ship_name,
            "custom_ship_value" : custom_ship_value,
            "tax" : tax,
            "notes_value": order_notes
          },
          success:function(responseInvoice)
          {
            var printContents = responseInvoice;
var originalContents = document.body.innerHTML;
document.body.innerHTML = printContents;
window.print();
window.close();
document.body.innerHTML = originalContents;
return false
            // window.print();
            // console.log(data);
          }
      });
    });

      $('#product-name').keyup(function(){
      var product_name = $('#product-name').val();
      $(".search-product-name").val(product_name);
    });
  </script>
  <script>
function goBack() {
    window.history.back();
}
</script>
<script>
function myFunction() {
    window.print();
}
</script>
<script>
  $('#add_new_customer').click(function(){
    $('#new_customer_form').modal('show');
  });
  </script>
  <script>
  
  $('.add-customer-new').click(function(){
      $('.add-customer-loader').css('display','block');


       $.ajax({

          url:base_url+'/admin/add-customer',
          type:'POST',
          data:$("#customerForm").serialize(),
          success:function(data){
            if (data.status==0) {
              toastr.success(data.message, {timeOut: 100000});
              $('toast-container').css('background', 'green');
              return;
            }
            $('.add-customer-loader').css('display','none');
            $("#new_customer_form").modal('hide');
            $('.user-detail').html(data);
            // alert('Customer has been added successfully.', {timeOut: 100000});
            // var audio = new Audio('{{URL::to("web-assets/served.ogg")}}');
            // $('.successpostnoti').audio.play();
             $('.successpostnoti').prepend("<p id='successpara'>Customer added successfully!! </div>").show( "slow",function(){
               var audio = new Audio('{{URL::to('web-assets/youve-been-informed.ogg')}}');
                 audio.play();
                     } ).delay(5500).fadeOut(1500);
            $('#toast-container').css('background', 'green!important');
            $('.email-invoice').prop("disabled", true);
          }
      });
    });
</script>
<script type="text/javascript">
  
    var audio = new Audio('{{URL::to('web-assets/served.ogg')}}');

    function post()
    {
      audio.play();
      $('successpostnoti').close();
    }

  </script>
<style type="text/css">
  #toast-container>div {
    background-color: rgba(199, 21, 21, 0.92) !important;
    /*opacity: none !important;*/
    color: white !important;
    /* display: block !important; */
}

</style>
<div class="successpostnoti" onclick="post();">
  <i class="fa fa-2x fa-thumbs-o-up" aria-hidden="true" style="float:right; margin-right:5px;    margin-top: -4px;"></i>
  
</div>
  @endsection
@endsection