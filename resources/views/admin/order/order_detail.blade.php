<style>
  td{
    padding: 2% !important;
  }

  .radioright{
    float: right;
  }
  .hide
  {
    display: none;
  }
  .show
  {
    display: block;
  }
  .deleteMeetingButton {
    clear:both;
    cursor:pointer;
    width:90px;
    height:30px;
    border-radius: 4px;
    background-color: #5CD2D2;
    border:none;
    text-align:center;
    line-height:10px;
    color:#FFFFFF;
    font-size:11px;
    font-family:Arial, Helvetica, sans-serif;
    font-weight:bold;
}
.deleteMeetingCancel {
    clear:both;
    cursor:pointer;
    width:90px;
    height:30px;
    border-radius: 4px;
    background-color: #5CD2D2;
    border:none;
    text-align:center;
    line-height:10px;
    color:#FFFFFF;
    font-size:11px;
    font-family:Arial, Helvetica, sans-serif;
    font-weight:bold;
    content:"XXXX";
}
#deleteMeetingConfirmDeleted {
    display:none;
}
/* added code below */
.deleteMeetingClose {
    font-size: 1.5em;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 5px;
}
.popover-content {
    padding: 0px 12px;
}
.blocktext
{
  pointer-events: none;
}
.showtext
{
  pointer-events:painted ;

}
</style>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="custom"){
          alert('clicked');
            $('#showinputs').removeClass('hide');
    $('#showinputs').addClass('show' );
        }
        if($(this).attr("value")=="free"){
            $('#showinputs').removeClass('show');
           $('#showinputs').addClass('hide' );
        }
       
    });
});
</script>
<div class="row">
  <ul>
    <table style="width: 100%;">
      <?php

        $total_price = 0; 
        $i = 1;
        $cart_content = Cart::content();
        // dd($cart_content);
        $cart_total = Cart::subtotal('2', '.', '');

        $total_tax = [];
        foreach ($cart_content as $product) {
          if ($product->options->is_coupon == 1) {
            array_push($total_tax, $product->options->tax);
            $final_tax = array_sum($total_tax);
          }elseif($product->options->is_not_coupon == 1){
            array_push($total_tax, $product->options->tax);
            $final_tax = array_sum($total_tax); 
          }else{
            array_push($total_tax, $product->options->tax*$product->qty);
            $final_tax = array_sum($total_tax);
          }
        }
        if (isset($final_tax)) {
          $final_tax = $final_tax;
        }else{
          $final_tax = 0.00;
        }


        // dd($final_tax);
        $total_discount = [];
        foreach ($cart_content as $product) {
          array_push($total_discount, $product->options->discount);
          $final_discount = array_sum($total_discount);
        }
        // dd($final_discount);
        if (isset($final_discount)) {
          $final_discount = $final_discount;
        }else{
          $final_discount = 0.00;
        }
        // dd($final_discount);
      ?>
      @foreach($cart_content as $product)
      
      <tr id="{{$product->price}}" class="currentRow<?php echo $i; ?>">

        <td data-id="{{$product->id}}"></td>
        <td class="next-table__image-cell hide-when-printing">
          <img title="{{$product->name}}" class="block aspect-ratio__content" src="{{$product->options->image_url}}" alt="{{$product->name}}" style="width: 50px;">
        </td>
        <td>{{$product->name}}
          <br>SKU: {{$product->options->i_sku}}
        </td>
        <td>Price: Rs.{{$product->price}}
        </td>
        <td>
          <input type="number" name="quantity" value="{{$product->qty}}" size="2" class="ajquantity quantity quantitynew" min="1" data-productid="{{$product->id}}" id="{{$product->rowId}}" serialNumber = 
                 <?php echo $i; ?> />
        </td>
        <input type="hidden" name="product_price" class="product_price<?php echo $i; ?>" value="{{$product->price}}" />
        <td class="ajsecondprice">Rs.
          <div class="multiply_product_price<?php echo $i; ?>">{{$product->price*$product->qty}}
          </div>
        </td>
        <?php $total_price += $product->price; ?>
        <td data-pid="{{$product->id}}" data-id="{{$product->rowId}}" onClick="deleteProduct(this);">
          <button type="button" class="ajremovebtn removebutton stotal_price" title="Remove this row"><i class="fa fa-lg fa-times" aria-hidden="true"></i>

          </button>
        </td>
      </tr>
      <?php $i++; ?>
      @endforeach
    </table>
    <input type="hidden" id="pdiscount">
    <input type="hidden" id="fdiscount">
    <input type="hidden" id="discount_coupon_code">

    <div class="form-group">
      <div class="col-sm-8">
        <label for="form-field-1"> Notes 
        </label>
        <br>
        <input type="text" name="notes" id="order-notes" placeholder="Add a note..." class="col-xs-10 col-sm-12 order-notes" />
      </div>



    <span id="message" style="margin-left:10px;"></span>
      <br>
      <div class="col-sm-4">
        <div class="popover-markup"> 
        Subtotal  -

        <span style="float: right;" >Rs. <span class="total-amount">{{$cart_total}}</span>
        </span> 

        <br><br>

          <!-- <span id="discount-code"></span><br> -->
          @if($final_discount > 0)
            <span>
              Discount 
              <button type="button" class="removebutton removediscount stotal_price" title="Remove this row"  style="cursor: pointer;width: 22px;height: 22px;">X</button><br>
              @if(isset($coupon_discount_code))
                {{$coupon_discount_code}}
              @endif
            </span>
            @if(isset($reason))
            <span id="reason_value">({{$reason}})
            </span>
            @endif
            <span style="float: right;" id="discount_value">{{round($final_discount, 2)}} </span>
            <span style="float: right;">Rs. </span>
          @else
            @if(isset($err_message))
            <p>Invalid Coupon Code</p>
            @endif
            <!-- <a href="javascript:void(0)" class="trigger discount_div_text" id="adddiscounth">Add Discount</a> -->
            <a href="javascript:void(0)" class="trigger discount_div_text" id="discountdiv">Add Discount</a>
            <span style="float: right;" id="discount_value">{{round($final_discount, 2)}} </span>
            <span style="float: right;">Rs. </span>
          @endif
          <div class="content hide" id="discount">
                <span class="deleteMeetingClose" id="discountclose1" >&times;</span>
            <div class="form-group">

              <input type="hidden" value="price" id="discount_type">

              Discount in <hr>
              
               <input type="radio" name="discount_by" id="discount_by_per" size="2" value="percent" class="radioright discount_by_button" />Percentage<br>


              <input type="radio" name="discount_by" id="discount_by_coupon" size="2" value="coupon" class="radioright discount_by_button" />Coupon <br>

               <input type="radio" name="discount_by" id="discount_by" size="2" value="price" class="radioright discount_by_button" checked="checked" />Rupee<br>
              <br />
              <hr>
              <input type="text" name="discountPrice" id="discountPrice" class="form-control" placeholder="Enter Amount">
              <!-- Reason -->
              <input type="text" name="dreason" id="dreason" class="form-control" placeholder="Enter Reason">
            </div>
            <button id="discount" type="submit" class="btn btn-default btn-block add_discount">
              Submit
            </button>


            <script>
            $("#adshippingclick").click(function(){
    $(".content").hide();
});


            

            </script>

            <script>

  $("#discountclose1").click(function(){
    $(".popover").hide();
     $(".addshpng").removeClass('blocktext');


});
  
  $("#discountdiv").click(function(){
    $(".addshpng").addClass('blocktext');
    


});



</script>
          </div>
        </div> 
        <div class="discount_div">
          <span style="float: right;">&nbsp; 
           <span id="discount_value">
            </span>&nbsp;</span> 
            <span id="reason_value">
            </span>
            </div>
          </span>
       
        <br>
        <div class="popover-markup1 shipping_div"> 
          <!-- <a href="javascript:void(0)" class="trigger" id="adshippingclick" >Add Shipping -->
          <a href="javascript:void(0)" class="trigger addshpng">Add Shipping

          </a> 
          <span style="float: right;" id="discount_value">Rs. 0</span>
          <div class="head hide">Add Shipping
             <span class="deleteMeetingClose" id="discountclose" >&times;</span>
          </div> 
          <div class="content hide">
            <div class="form-group">
              
              <input type="radio" name="shipping" value="free" checked="checked" class="discount_by_button shipping radioright" id="freeshipping" id="freeship">Free Shipping<br>
              <input type="radio" name="shipping" value="custom" class="discount_by_button shipping radioright" id="custship" >Custom
              <br><br>
              <style type="text/css">
    .box{
        
        display: none;
        
    }
   
</style>
<script type="text/javascript">
 
    // $("#custship").click(function() {
    //     // var test = $(this).val();

    //     $(".box").css(" display ", " block");
    //     // $("#Cars" + test).show();
    // });
$( "#custship" ).on( "click", function() {
  $(".box").css( "display", "block" );
});

</script>
<script type="text/javascript">
  $( "#freeship" ).on( "click", function() {
  $(".box").css( "display", "none" );
});
</script>


<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="custom"){
            $(".box").not(".red").hide();
            $(".red").show();
        }
        if($(this).attr("value")=="free"){
            // $(".box").not(".green").hide();
            $(".red").hide();
        }
        
    });
});
</script>

<script>



  $("#discountclose").click(function(){
    $(".popover").fadeOut();
    $(".popover").hide();
 $("#discountdiv").removeClass('blocktext');


});
  
  $(".addshpng").click(function(){
    $("#discountdiv").addClass('blocktext');



});
</script>

              <div class="red box">
              <hr>
                <input type="text" class="form-control" name="custom_ship_name" id="custom_ship_name" placeholder="custom name"><br>
                <input type="text" class="form-control" name="custom_ship_value" id="custom_ship_value" placeholder="custom value">
              </div>
            </div>
            <button type="submit" class="btn btn-default btn-block add-shipping">
              Submit
            </button>
          </div>
        </div>    
        
        <span style="float: right;" class="ship_discount_free">
        </span> 
        <div class="ship_discount_div">
            <span id="ship_reason_value"></span>

            <span id="close_ship_button" style="cursor: pointer;"> <button type="button" class="removebutton stotal_price" title="Remove this row"> X
          </button></span><br>
          <span id="ship_reason_name"></span>
            <span style="float: right;">&nbsp; 

           <span id="ship_discount_value"></span><span id="freeshp"></span>
        </div>
        <br>
        
<div class="popover-markup2"> 
          Taxes
          <!-- <a href="javascript:void(0)" class="trigger tax_div">Taxes -->
          <span id="taxval1"></span><span style="float: right;" id="tax_value">{{round($final_tax, 2)}}</span>
          <span style="float: right;">Rs. </span>
        </div> 
        <br>
        <br>
         <span style="display: none;" class="discount-total-amount">
        </span>
        <storng>Total
        </storng>

        <span style="float: right;" class="final-total-amount">{{round(($cart_total+$final_tax-$final_discount), 2)}}
        
        </span> <span style="float: right;">Rs. </span>
        <input type="hidden" value="{{round(($cart_total+$final_tax-$final_discount), 2)}}" id="total_amount">
        <br>
        <br>
      </div>
    </div>
    </div>
</div>


<script>
  

$('input[id=customshipping]').on('click', function() {
    $('#showinputs').removeClass('hide');
    $('#showinputs').addClass('show' );
});

</script>
<script>
  
  $('#vendor_name').change(function(){
      var product_cart_count = "{{Cart::count()}}"
      if (product_cart_count > 0) {
        window.location.reload(true);
      }
    });  

  $('.popover-markup>.trigger').popover({
    html: true,
    title: function () {
      return $(this).parent().find('.head').html();
    }
    ,
    content: function () {
      return $(this).parent().find('.content').html();
    }
  }
                                       );
  $('.popover-markup2>.trigger').popover({
    html: true,
    title: function () {
      return $(this).parent().find('.head').html();
    }
    ,
    content: function () {
      return $(this).parent().find('.content').html();
    }
  }
                                       );
  $('.popover-markup1>.trigger').popover({
    html: true,
    title: function () {
      return $(this).parent().find('.head').html();
      $('.popover-markup').addClass('hide');
      $('.popover-markup').removeClass('show');
      $('.popover-markup1').removeClass('hide');
      $('.popover-markup1').addClass('show');
    }
    ,
    content: function () {
      return $(this).parent().find('.content').html();
    }
  }
               );

  $(".discount_div").hide();

     function getProductDisIds() {
         var allVals = [];
         $('td').each(function() {
           allVals.push($(this).attr("data-id"));
         });
         return allVals;
      }
    $(document).on('click','.discount_by_button',function(e){
      var discountTypeButton = $(this).val();
      $('#discount_type').val(discountTypeButton);
      if(discountTypeButton=="price"){
        $("#discountPrice").attr("placeholder", "Enter Amount").placeholder();  
      }
      if(discountTypeButton=="percent"){
        $("#discountPrice").attr("placeholder", "Enter % Amount").placeholder();    
      }
      if(discountTypeButton=="coupon"){
        $("#discountPrice").attr("placeholder", "Enter Coupon Code").placeholder();
      }
    });
  $("#close_discount_button").hide();
  var loading = false;
  $(document).on('click','.add_discount',function(e){
      e.preventDefault();
      
      var customer_id = $('#customer_name').val();      
      var discount_by = $("input:radio[id=discount_by]:checked").val();
      var discount_by_per = $("input:radio[id=discount_by_per]:checked").val();
      var discount_by_coupon = $("input:radio[id=discount_by_coupon]:checked").val(); 
      var totalValue=$(".total-amount").text();
      var dprice = $("#discountPrice").val();
      var dreason = $("#dreason").val();
      var shipval = $("#ship_discount_value").text();
      var taxval = $("#tax_value").text();
      
      var product_ids = getProductDisIds();
      // alert(product_ids);
      if(discount_by)
      {
        // if(discount_by=="price" && dprice>totalValue){
        //   alert("Discount price should be less than total price");
        //   return false;
        // }
        if (loading) {
          return ;
        }
        loading = true;
        $.ajax({
            url: base_url+'/admin/add-discounts',
            type: "post",
            data: 
            {
              "product_ids": product_ids,
              "fdiscount": dprice,
              "reason": dreason,
              "store_id": $('#vendor_name').val()
            },
            success:function(data)
            {
              $(".previous_div").hide();
              $('.order-detail').html(data);
            },
            error: function () {
              loading = false;
            }
        });
    
      }
      if(discount_by_per)
      {
        if(discount_by_per=="percent" && dprice>99){
          alert("Discount % should be less than 100");
          return false;
        }
        if (loading) {
          return ;
        }
        loading = true;
        $.ajax({
            url: base_url+'/admin/add-discounts',
            type: "post",
            data: 
            {
              "product_ids": product_ids,
              "pdiscount": dprice,
              "store_id": $('#vendor_name').val()
            },
            success:function(data)
            {
              $(".previous_div").hide();
              $('.order-detail').html(data);
            },
            error: function () {
              loading = false;
            }
        });
      }
      if(discount_by_coupon)
      {
        if (loading) {
          return ;
        }
        loading = true;
        $.ajax({
          url: base_url+'/admin/add-discounts',
          type: "post",
          data: 
            {
              "discount_coupon_code": dprice,
              "totalValue": totalValue,
              "product_ids": product_ids,
              "customer_id": customer_id,
              "store_id": $('#vendor_name').val()
            },
          success:function(data)
            {
              $(".previous_div").hide();
              $('.order-detail').html(data);
            },
            error: function () {
              loading = false;
            }
        });

      }   

  });

  // for adding shipping option
  $("#close_ship_button").hide();
  $(document).on('click','.add-shipping',function(e){
    e.preventDefault();
    var ship_discount_by = $("input:radio[name=shipping]:checked").val();
    if (ship_discount_by == 'free') {
      // $('.ship_reason_value').text(ship_discount_by);
      $("#ship_reason_value").text("Shipping");
        $(".ship_discount_div").show();
        $("#close_ship_button").show();
        $("#ship_reason_value").show();
        $(".shipping_div").hide();
        // ship_discount_value
        $("#freeshp").text("Free Shipping");
      $('.popover').fadeOut();
    }
    if (ship_discount_by == 'custom') {
      var totalValue1=$(".final-total-amount").text();
      var shipping_price = $("#custom_ship_value").val();
      var shipping_name = $("#custom_ship_name").val();
      var base_url = "{{URL::to('/')}}";
      
      if (loading) {
        return ;
      }
      loading = true;
      $.ajax({
          url: base_url+'/admin/add-shipping',
          type: "POST",
          data: 
          {
            "shipping": shipping_price,
            "shipping_name": shipping_name,
            "store_id": $('#vendor_name').val(),
            "totalValue": $('#total_amount').val()
          },
          success:function(data)
          {
            $(".final-total-amount").text(data.total);
            $("#ship_discount_value").text(data.shipping);
            $("#ship_reason_value").text("Shipping");
            $("#ship_reason_name").text(data.shipping_name);
            $(".ship_discount_div").show();
            $("#close_ship_button").show();
            $("#ship_reason_value").show();
            $("#ship_reason_name").show();
            $(".shipping_div").hide();
           
            $('.popover').fadeOut();
          },
          error: function () {
            loading = false;
          }
      }); 
      // if(Math.floor(shipping_price) == shipping_price && $.isNumeric(shipping_price)){
        // shipping_price = parseFloat(shipping_price);
        // discount = parseFloat(discount);
        // total_with_shipping = +totalValue1 + +shipping_price;
        // alert(total_with_shipping);
        // $(".final-total-amount").text(total_with_shipping);
        // $("#ship_discount_value").text(shipping_price);
        // $("#ship_reason_value").text(shipping_name);
        // $(".ship_discount_div").show();
        // $("#close_ship_button").show();
        // $("#ship_reason_value").show();
        // $(".shipping_div").hide();
       
        // $('.popover').fadeOut();
      // } else {
        // alert('Shipping value should be integer');
      // }     

    }
  });

  $("#close_ship_button").click(function(){
    var totalValue1=$(".final-total-amount").text();
    var ship_discount_value=$("#ship_discount_value").text();
    $(".final-total-amount").text(totalValue1-ship_discount_value);
    $("#ship_discount_value").text("");
    $("#close_ship_button").hide();
    $("#ship_reason_value").hide();
    $("#ship_reason_name").hide();
    $(".shipping_div").show();
    $("#freeshp").text("");
  });

  // $("#close_discount_button").click(function(){
  //   var totalValue1=$(".final-total-amount").text();
  //   var discount_value=$("#discount_value").text();
  //   $(".final-total-amount").text(+totalValue1 + +discount_value);
  //   $("#discount_value").text(0.00);
  //   $("#close_discount_button").hide();    
  //   $("#reason_value").show();
  //   $(".discount_div_text").show();
  //   $("#discount-code").html('');
  //   $("#reason_value").text('');
  // });
  
  var sum = 0;
  var total = 0;

  $(".quantity").change(function(){
      var sum = 0;
      
      var direction = this.defaultValue < this.value
      this.defaultValue = this.value;
      if (direction){
        var checktype = "increase";
      } 
      else{
        var checktype = "decrease";
      } 

      $(".total-amount").text(0);
      var serialNo = $(this).attr("serialNumber");
      var productid = $(this).attr("data-productid");
      var id = $(this).attr("id");
      var product_price = $(".product_price"+serialNo).val();
      var qty = $(this).val();
      var total = 0;
      $.ajax({
          url: base_url+'/admin/calc-total-qty',
          type: "post",
          data: 
          {
            "productid": productid,
            "qty": qty,
            "checktype": checktype,
            "id": id,
            "store_id": $('#vendor_name').val()
            // "tax" : $("#tax_value").text()

          },
          success:function(data)
          {
            if (data.status == 0) {
              alert(data.message);
              return;
            };
            $(".previous_div").hide();
            $('.order-detail').html(data);
          }
      });
      var discount_by = $("input:radio[name=discount_by]:checked").val();
      var shippingprice = $("input:radio[name=shipping]:checked").val();

      var discountPrice = $("#discountPrice").val();

  }
);
  function deleteProduct(obj){
    var id = $(obj).attr("data-id");
    var pid = $(obj).attr("data-pid");
    deleteProductFromSession(id, pid);
  }

  function deleteProductFromSession(id, pid){
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url: base_url+'/admin/order/delete-product-from-session',
        type: "post",
        data: 
        {
          "id": id,
          "pid": pid,
          "store_id": $('#vendor_name').val()
        },
        success:function(data)
        {
          toastr.success("Product Deleted Successfully.", {timeOut: 100000});
          $(".previous_div").hide();
          $('.order-detail').html(data);
        }
    });

  }

$('.removediscount').click(function () {
  var base_url = "{{URL::to('/')}}";
    $.ajax({
        url: base_url+'/admin/remove-discount',
        type: "POST",
        data: 
        {
          "id": null,
          "store_id": $('#vendor_name').val()
        },
        success:function(data)
        {
          $(".previous_div").hide();
          $('.order-detail').html(data);
        }
    });
});
</script>

