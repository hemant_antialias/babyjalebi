@extends('admin.admin_layout')
<style type="text/css">
  .requird
  {
    color:red;
  }
    .table-bordered>tbody>tr>td{
        font-size: 13px;
    }
    .input-filter {
        width: 90px !important;
    }
   /* #toast-container > div {
      background-color: black !important ;
     
    }*/
    /*.colorchange {bottom:2%; ; position: absolute !important; right: 5% !important; opacity: 0.6 !important; border-radius: 5px !important;  }*/
   
/*#toast-container>div
    {
        background-color: rgba(8, 10, 10, 0.98) ; 
        opacity: none !important;
         opacity: 4 !important;
        display: block !important;
    }*/
    #toast-container > div {
   /*background: black !important;*/
   position: absolute !important;
   top: 31px;
   right: 4%;
   
}

 .dataTables_paginate
    {
        display: none;
    }
.colorchange{ opacity: 0.8 !important; }

</style>
  
@section('content')
<div class='row'>
     <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Order List</h3>
                    <!-- <form method="get" action="{{URL::to('admin/order')}}">
                        Search by Phone/Location: <input type="text" name="search">
                        <input type="submit" value="Search" class="btn btn-default">
                    </form> -->
                    <div class="col-md-4">
                     <form method="get" action="{{URL::to('admin/order')}}">
                            <input type="text" name="data_from_table" placeholder="Master Search">

                        <input type="submit" value="Search" class="btn btn-default">
                    </form>
                    </div>
                    <div class="col-md-4">
                      <!--  <label for="id-date-range-picker-1">Export Data By Date
                          </label> -->
                        <div class="row" >
                          <div class="col-xs-8 col-sm-12">
                             
                            <div class="input-group" id="datehere">
                              <span class="input-group-addon" style="background: #02c9c6; color: white; border: #02c9c6;  " >
                                <i class="fa fa-calendar bigger-110 datepic">
                                </i>
                              </span>
                             <input type="text" name="daterange"  class="datepic" value="" style="width: 100%; color : #02c9c6;" />
                            
                            </div>
                          </div>
                          <form action="{{URL::to('/admin/export-by-day')}}" method="post">
                           <input type="hidden" name="startdate" value="" id="start_date">
                              <input type="hidden" name="enddate" value="" id="end_date">
                              <br>
                           <input type="submit" value="Export Data By Date"  class="btn btn-default" style="float: right;margin-top: 4%;">
                           </form>
                        </div>
                        <hr>
                        </div>
                    <div class="col-md-4">
                    <a href="{{URL::to('/admin/order')}}"><button id="" class="btn btn-default btnchanges"><i class="fa fa-repeat" aria-hidden="true" style="font-size: 18px;"></i></button></a>
                     <a href="{{URL::to('/admin/order/new')}}"><button id="createorder" class="btn btn-default btnchanges"> Create Order</button></a>
                     <a href="{{URL::to('all-records-csv')}}"><button id="export1" class="btn btn-default exlport btnchanges"> Export Order</button></a>
                     </div>
                     <br><br>
                    <!-- Minimum price: -->
          <div class="order-data">
                        <table id="orders-dynamic-table" class="table table-striped table-bordered table-hover" data-page-length='50'>
                            <thead>
                                <tr>
                                    <!-- <th></th> -->
                                    <th>Order ID</th>

                                    <!-- <th>Order</th> -->
                                    <th style=" width: 7%; " >Time</th>
                                    <th>Customer</th>
                                    <th>Email</th>
                                    
                                    <th style="width:12%;" >Payment Status</th>
                                    <th>Order Status</th>
                                    <th>Total</th>
                                </tr>
                                <tr>
                                   <!--  <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th> -->
                                    <!-- <th class="input-filter">Order ID</th> -->

                                    <th class="input-filter">Order</th>
                                    <th></th>
                                    <th class="input-filter">Customer</th>
                                    <th class="input-filter">Email</th>
                                    <th class=" payment-status">Payment Status</th>
                                    <th class=" order-status">Order Status</th>
                                    <th></th>
                                </tr>

                            </thead>
                            
                            <tbody class="product-inventory">

                            @foreach($orders as $order)
                                <tr data-id="{{$order->id}}">
                                    <!-- <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace product-id" value="{{$order->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>

 -->                              
                                    @if($order->notes == NULL)
                                    <td>
                                        <a href="{{URL::to('/admin/order/'.$order->order_code)}}">Baby_{{$order->id}}</a>
                                    </td>
                                    @else
                                   <td>
                                        <a href="{{URL::to('/admin/order/'.$order->order_code)}}">Baby_{{$order->id}}<span style="color:red;">*</span></a>
                                    </td>
                                    @endif
                                   
                                    <td>

                                        <?php
                                        $month =$order->created_at;

                                        echo $newDateTime = date("jS F", strtotime($month));
                                        echo "<br>";
                                        echo $newDateTime1 = date('h:i A', strtotime($month));
                                          ?>
                                    </td>
                                    <td>{{$order->first_name}} {{$order->last_name}}</td>
                                    <td>{{$order->email}}</td>
                                    <td class="">
                                        <span class="label label-sm label-inverse arrowed-in" style="display:none;">{{$order->payment_status_name}}</span>
                                        <p style="display:none;">{{$order->payment_id}}</p> {{  Form::select('payment_status', $payment_status,$order->payment_id,['class'=>'form-control payment_status']) }}
                                    </td>
                                <td class="">
                                        <span class="label label-sm label-inverse arrowed-in" style="display:none;">{{$order->status_title}}</span>
                                        <p style="display:none;">{{$order->order_id}}</p>  {{  Form::select('order_status', $order_status,$order->order_id,['class'=>'form-control order_status']) }}

                                    </td>
                                    <td>
                                        Rs. {{round($order->payble_amount, 2)}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
{!! $orders->render() !!}
                    </div>

      </div>
    </div>

    </div>
</div>
@stop
@section('scripts')
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script>
      $(document).ready(function() {

            $('.payment-status').each( function () {
                var payment_status = '<?php foreach($payment_status as $status => $status_title){ echo "<option value=".$status. ">".$status_title."</option>"; } ?>';

                $(this).html( '<select><option value="">Select</option>'+payment_status+'</select>' );
            } );

        $('.order-status').each( function () {

                var order_status = '<?php foreach($order_status as $status => $status_title){ echo "<option value=". $status .">".$status_title."</option>"; } ?>';
                    $(this).html( '<select><option value="">Select</option>'+order_status+'</select>' );
            } );

           
        $('.input-filter').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" style="    width: 100%; " />' );
        } );

        var myTable = $('#orders-dynamic-table').DataTable({
          paging: true,
          sort: false,
          searching: true,
          select: {
                    style: 'multi'
                }
        });

        // Apply the search
        myTable.columns().every( function () {
            var that = this;
            
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


            serachTextBox.on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );

            serachSelectBox.on( 'change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


        } );

            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });

            $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
            
            //select/deselect all rows according to table header checkbox
            $('#orders-dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
                var th_checked = this.checked;//checkbox inside "TH" table header
                
                $('#orders-dynamic-table').find('tbody > tr').each(function(){
                    var row = this;
                    if(th_checked) myTable.row(row).select();
                    else  myTable.row(row).deselect();
                });
            });
    } );
    </script>
    <script>
    $('.order_status').change(function(event){
        event.preventDefault();
        var base_url = "{{URL::to('/')}}";
        var status = $(this).val();
        // alert(status);
  
        var trid = $(this).closest('tr').attr('data-id');
        // alert(trid);
        $.ajax({
            url: base_url+'/admin/order/change-order-status',
            type: "post",
            data: 
            {
                "id": trid,
                "status": status
            },
            success:function(data)
            {
                toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "800",
  "hideDuration": "700",
  "timeOut": "750",
  "extendedTimeOut": "600",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
                if (data.status == 1) {
                     toastr.options.positionClass = "toast-top-right";
                    toastr.info("Status changed", {timeOut: 3000})
                     toastr.options.positionClass = "toast-top-center";
                    toastr.info("Status changed", {timeOut: 500});
                   $('toast-container').css('background', 'green');
                }
            }
        });
    });

    $('.payment_status').change(function(){
        var base_url = "{{URL::to('/')}}";
        var status = $(this).val();
        var trid = $(this).closest('tr').attr('data-id');
        $.ajax({
            url: base_url+'/admin/order/change-payment-status',
            type: "post",
            data: 
            {
                "id": trid,
                "status": status
            },

            success:function(data)
            {

         

 toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "800",
  "hideDuration": "800",
  "timeOut": "750",
  "extendedTimeOut": "600",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
                if (data.status == 1) {
                    toastr.info("Status changed", {timeOut: 1000})
                    toastr.info("Status changed", {timeOut: 500});
                    $('toast-container').css('background', 'green !important');

                }
            }
        });
    });
$
    </script>
    <script>
     (function() {
    $( '#datehere' ).daterangepicker({  maxDate: new Date() });
  });

    
$(function() {
 

    $('#datehere').daterangepicker();
    $('input[name="daterange"]').daterangepicker();
    $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {

   
     
      startDate = picker.startDate.format('YYYY-MM-DD');
      endDate = picker.endDate.format('YYYY-MM-DD');
     
      var base_url = "{{URL::to('/')}}"
      $.ajax({
          url:base_url+'/admin/get-sales',
          type:'POST',
          data:{
            "startDate": startDate,
            "endDate": endDate
          },


          success:function(data){
            $('.total_sales').html(data.totalSales);
            $('.total_online_sales').html(data.totalOnlineSales);
            $('.total_online_orders').html(data.totalOnlineOrders);
            $('.total_offline_sales').html(data.totalOfflineSales);
            $('.total_offline_orders').html(data.totalOfflineOrders);
          }
      });
    });

});

// for export data
$('.export').click(function(e){
    e.preventDefault();
    var base_url = '{{URL::to("/")}}';
      // alert("clckced");

      $.ajax({
          url: base_url+'order/export',
          type: "post",
          data: 
          {
            
          },
          success:function(responseInvoice)
          {
            // var printContents = responseInvoice;
            // var originalContents = document.body.innerHTML;
            // document.body.innerHTML = printContents;
            // window.print();
            // window.close();
            // document.body.innerHTML = originalContents;
            return false
            // window.print();
            // console.log(data);
          }
      });
    });


</script>
<!-- /.main-container -->
      @include('admin.js')
      <script type="text/javascript" src="{{URL::to('admin-assets/js/jquery-3.1.1.min.js')}}"></script>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"> </script> -->
      <script type="text/javascript" src="{{URL::to('admin-assets/js/moment.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />

    
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="{{URL::to('admin-assets/js/daterangepicker.js')}}"></script>
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script> -->
<link rel="stylesheet" type="text/css" src="{{URL::to('admin-assets/css/daterangepicker.css')}}" />
<script>
  $('.date_input').daterangepicker({
    maxDate: "0"
})

</script>
 <script>  
    var j = jQuery.noConflict();
j(function() {
 

    // j('#datehere').daterangepicker({minDate: 0});
    // j('input[name="daterange"]').daterangepicker();
    j('.datepic').daterangepicker();

    j('.datepic').on('apply.daterangepicker', function(ev, picker) {
     
     
      startDate = picker.startDate.format('YYYY-MM-DD');
      maxDate =picker.startDate;
      endDate = picker.endDate.format('YYYY-MM-DD');
      j('#start_date').val(startDate);
      j('#end_date').val(endDate);
    });

});



</script>
 
@endsection