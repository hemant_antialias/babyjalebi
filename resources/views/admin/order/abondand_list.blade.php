@extends('admin.admin_layout')
<style type="text/css">
	.requird
	{
		color:red;
	}
    .table-bordered>tbody>tr>td{
        font-size: 13px;
    }
    .input-filter {
        width: 90px !important;
    }
    #toast-container > div {
      background-color: black !important ;
      opacity: 4 !important;
    }
    /*.colorchange {bottom:2%; ; position: absolute !important; right: 5% !important; opacity: 0.6 !important; border-radius: 5px !important;  }*/
    #toast-container > div {
   /*background: black !important;*/
   position: absolute !important;
   top: 31px;
   right: 4%;
   
}
.colorchange{ opacity: 0.8 !important; }

</style>
  
@section('content')
<div class='row'>
     <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Abandoned Order List</h3>
                    <div style="float: right;">
                    <button id="refresh" class="btn btn-default btnchanges"><i class="fa fa-repeat" aria-hidden="true" style="font-size: 18px;"></i></button>
                     </div>
                     <br><br>
                    <!-- Minimum price: -->
     			<div class="order-data">
                        <table id="orders-dynamic-table" class="table table-striped table-bordered table-hover" data-page-length='50'>
                            <thead>
                                <tr>
                                    <!-- <th></th> -->
                                    <th>Order</th>
                                    <th style=" width: 9%; " >Time</th>
                                    <th>Customer</th>
                                    <th>Store</th>
                                    <th>Order Status</th>
                                    <th>Total</th>
                                </tr>
                                <tr>
                                   <!--  <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th> -->
                                    <th class="input-filter">Order</th>
                                    <th></th>
                                    <th class="input-filter">Customer</th>
                                    <th class="input-filter">Store</th>
                                    <th class="hidden-480 order-status">Order Status</th>
                                    <th></th>
                                </tr>

                            </thead>
                            
                            <tbody class="product-inventory">

                            @foreach($orders as $order)
                                <tr data-id="{{$order->id}}">
                                    <!-- <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace product-id" value="{{$order->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>
 -->
                                    @if($order->vendor_id == 1)
                                    <td>
                                        <a href="{{URL::to('/admin/order/'.$order->order_code)}}">D{{$order->id}}</a>
                                    </td>
                                    @else
                                    <td>
                                        <a href="{{URL::to('/admin/order/'.$order->order_code)}}">G{{$order->id}}</a>
                                    </td>
                                    @endif
                                    
                                   <td>

                                        <?php
                                        $month =$order->created_at;

                                        echo $newDateTime = date("jS F", strtotime($month));
                                        echo "<br>";
                                        echo $newDateTime1 = date('h:i A', strtotime($month));
                                          ?>
                                    </td>
                                    <td>{{$order->first_name}} {{$order->last_name}}</td>
                                     @if(!isStore())
                                    <td>{{$order->store_name}}</td>
                                    @endif

									<td class="hidden-480">
                                        <span class="label label-sm label-inverse arrowed-in" style="display:none;">{{$order->status_title}}</span>
                                        <p style="display:none;">{{$order->order_id}}</p>  {{  Form::select('order_status', $order_status,$order->order_id,['class'=>'form-control order_status']) }}

                                    </td>
                                    <td>
                                        Rs. {{round($order->payble_amount,2)}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

			</div>
		</div>

    </div>
</div>
@stop
@section('scripts')
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script>
    	$(document).ready(function() {

    		$('.order-status').each( function () {
                var order_status = '<?php foreach($order_status as $status => $status_title){ echo "<option value=". $status .">".$status_title."</option>"; } ?>';
                    $(this).html( '<select><option value="">Select</option>'+order_status+'</select>' );
            } );

           
		    $('.input-filter').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" placeholder="Search" style="    width: 124px; " />' );
		    } );

		    var myTable = $('#orders-dynamic-table').DataTable({
		    	paging: true,
		    	sort: false,
		    	searching: true,
		    	select: {
                    style: 'multi'
                }
		    });

		    // Apply the search
		    myTable.columns().every( function () {
		        var that = this;
		        
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


		        serachTextBox.on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );

		        serachSelectBox.on( 'change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


		    } );

            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });

            $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
            
            //select/deselect all rows according to table header checkbox
            $('#orders-dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
                var th_checked = this.checked;//checkbox inside "TH" table header
                
                $('#orders-dynamic-table').find('tbody > tr').each(function(){
                    var row = this;
                    if(th_checked) myTable.row(row).select();
                    else  myTable.row(row).deselect();
                });
            });
		} );
    </script>
    <script>
    $('.order_status').change(function(){
        var base_url = "{{URL::to('/')}}";
        var status = $(this).val();
        // alert(status);
        var trid = $(this).closest('tr').attr('data-id');
        $.ajax({
            url: base_url+'/admin/order/change-order-status',
            type: "post",
            data: 
            {
                "id": trid,
                "status": status
            },
            success:function(data)
            {
                if (data.status == 1) {
                     toastr.options.positionClass = "toast-top-center";
                    toastr.info("Status changed", {timeOut: 500});
                    $('#toast-container').addClass('colorchange');
                    location.reload();
                }
            }
        });
    });
    </script>
 
@endsection