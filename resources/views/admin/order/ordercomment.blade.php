<div class="ui-feed">
  <div id="timeline_container" refresh="timeline_container" class="ui-feed__section">
   


        <span class="timeline__event-anchor" id="timeline__event-anchor56638436227"></span>
            @foreach($order_comments as $comments)
            <div id="event56638436227" class="ui-feed__timeline ui-accordion__toggler" define="{'accordion56638436227': shopify.UIAccordion.for($(this).children('.ui-accordion:first')[0])}" bind-class="{'ui-feed__item--revealed': accordion56638436227.isExpanded(), 'ui-accordion__toggler': true}">
              <div class="ui-feed__item ui-feed__item--message  ui-feed__item--accordion-activator" data-accordion-toggler-for="a1291b97" id="ui-accordion__toggler--1" aria-expanded="false" aria-controls="ui-accordion__panel--1">
                  <span class="ui-feed__marker ui-feed__marker--user-action"></span>

                <span class="ui-feed__message">
                  
                    
                
                    <div class="timeline__message-container">
                   <span class="timeline__inner-message timeline__inner-message--indicator">
                      <h4><stong>{{$comments->name}}</stong></h4>
                      <span>{{$comments->comment}}</span>
                    
                    </span>
                    <time id="timeline__time-56638436227" class="timeline__time" refresh="timeline__time" style="float: right;">
                      
                        ({{Carbon\Carbon::parse($comments->created_at)->diffForHumans()}})<span bind-show="shopify.Time.isDifferentTimezone()" class="hide"> IST</span>
                        
                     
                    </time>
                    </div>
                    
                  

              </span>

              </div>
                
            </div>
             @endforeach
        
        
        
  </div>
</div>