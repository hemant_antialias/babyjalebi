@extends('admin.admin_layout')
<style type="text/css">
	.requird
	{
		color:red;
	}
</style>

@section('content')
<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Product</a>
					</li>
					<li class="active">Add Product</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/product/new', 'method' => 'post', 'class' => 'form-horizontal']) }}
						
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> First Name </label>

							<div class="col-sm-9">
								<input type="text" name="i" id="form-field-1" placeholder="First Name" class="col-xs-10 col-sm-5" />
							    @if ($errors->has('first_name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('first_name') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Last Name </label>

							<div class="col-sm-9">
								<input type="text" name="last_name" id="form-field-1" placeholder="Last Name" class="col-xs-10 col-sm-5" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email </label>

							<div class="col-sm-9">
								<input type="text" name="email" id="form-field-1" placeholder="Email" class="col-xs-10 col-sm-5" />
							</div>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<label>
									<input name="is_accept_marketing" type="checkbox" class="ace ace-checkbox-2" />
									<span class="lbl"> Customer accepts marketing</span>
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<input name="is_tax_exempt" type="checkbox" class="ace ace-checkbox-2" />
									<span class="lbl"> Customer is tax exempt</span>
								</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Company </label>

							<div class="col-sm-9">
								<input type="text" name="company" id="form-field-1" placeholder="Company" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Phone </label>

							<div class="col-sm-9">
								<input type="text" name="phone" id="form-field-1" placeholder="Phone" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address </label>

							<div class="col-sm-9">
								<input type="text" name="address" id="form-field-1" placeholder="Address" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address Con't </label>

							<div class="col-sm-9">
								<input type="text" name="address_cont" id="form-field-1" placeholder="Address Con't" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> City </label>

							<div class="col-sm-9">
								<input type="text" name="city" id="form-field-1" placeholder="City" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Zip Code </label>

							<div class="col-sm-9">
								<input type="text" name="zip" id="form-field-1" placeholder="Zip Code" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> State </label>

							<div class="col-sm-9">
								<input type="text" name="state" id="form-field-1" placeholder="State" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Country </label>

							<div class="col-sm-9">
								<input type="text" name="country" id="form-field-1" placeholder="Country" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Note </label>

							<div class="col-sm-9">
								<input type="text" name="note" id="form-field-1" placeholder="Note" class="col-xs-10 col-sm-5" />
							</div>
						</div>

<!-- 						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tag </label>

							<div class="col-sm-9">
								<input type="text" name="tags" id="form-field-tags" date-role="tagsinput" value="Tag Input Control" placeholder="Enter tags ..." />
							</div>
						</div> -->
						

						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Reset
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>

@stop

@section('page_scripts')

<script type="text/javascript">
 
</script>

@stop
