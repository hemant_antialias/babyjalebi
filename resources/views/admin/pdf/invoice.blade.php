<!DOCTYPE html>
<html>

<head>
    <title></title>
</head>

<body>

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="space-6"></div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-large">
                            <h3 class="widget-title grey lighter">
								<i class="ace-icon fa fa-leaf green"></i>
								Customer Invoice
							</h3>

                            <div class="widget-toolbar no-border invoice-info">
                                <span class="invoice-info-label">Invoice:</span>
                                <br />
                                <span class="invoice-info-label">Date:</span>
                                <span class="blue">04/04/2014</span>
                            </div>

                            <div class="widget-toolbar hidden-480">
                                <a href="#">
                                    <i class="ace-icon fa fa-print"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main padding-24">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
                                                <b>Customer Info</b>
                                            </div>
                                        </div>

                                        <div>
                                            <ul class="list-unstyled  spaced">
                                                <li>
                                                    <i class="ace-icon fa fa-caret-right green"></i>Street, City
                                                </li>

                                                <li>
                                                    <i class="ace-icon fa fa-caret-right green"></i>Zip Code
                                                </li>

                                                <li>
                                                    <i class="ace-icon fa fa-caret-right green"></i>State, Country
                                                </li>

                                                <li class="divider"></li>

                                                <li>
                                                    <i class="ace-icon fa fa-caret-right green"></i> Contact Info
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->

                                <div class="space"></div>

                                <div>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="center">#</th>
                                                <th>Product</th>
                                                <th class="hidden-xs">Description</th>
                                                <th class="hidden-480">Discount</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td class="center">1</td>

                                                <td>
                                                    <a href="#">{{$order->status_title}}</a>
                                                </td>
                                                <td class="hidden-xs">
                                                    Test product Description
                                                </td>
                                                <td class="hidden-480"> --- </td>
                                                <td>Rs.10</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>

                                <div class="hr hr8 hr-double hr-dotted"></div>

                                <div class="row">
                                    <div class="col-sm-5 pull-right">
                                        <h4 class="pull-right">
																Total amount :
																<span class="red">Rs.395</span>
															</h4>
                                    </div>
                                </div>

                                <div class="space-6"></div>
                                <div class="well">
                                    Thank you for choosing Babyjalebi products. We believe you will be satisfied by our services.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- PAGE CONTENT ENDS -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div>
    <!-- /.page-content -->

</body>

</html>