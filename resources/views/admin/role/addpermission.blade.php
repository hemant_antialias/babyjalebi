@extends('admin.admin_layout')


@section('content')

<style>
	.vendor-img {
    	height: 123px; 
    	width: 111px; 
}
</style>

	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Permission</a>
					</li>
					<li class="active">Add Permission</li>
				</ul>

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/add-permission', 'method' => 'post', 'class' => 'form-horizontal']) }}
						

						<div class="form-group">
							<div>
							<label for="form-field-1">Name </label>
								<input type="text" name="name" id="form-field-1" placeholder="Name" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						
						<div class="form-group">
							<div>
							<label for="form-field-1">Display Name </label>
								<input type="text" name="display_name" id="form-field-1" placeholder="Display Name" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-10 col-md-2">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
						
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>


@endsection