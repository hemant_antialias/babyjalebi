@extends('admin.admin_layout')


@section('content')

<style>
	.vendor-img {
    	height: 123px; 
    	width: 111px; 
}
.addusrprm{
	height: 20px;
	width: 100%;
}
.addusrprminput{
	float: left;
	width: 10%;
}
.addusrprmname{
width: 34%;
}
</style>

	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Role</a>
					</li>
					<li class="active">Add Role</li>
				</ul>

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/add-role', 'method' => 'post', 'class' => 'form-horizontal']) }}
						

						<div class="form-group">
							<div>
							<label for="form-field-1">Name </label>
								<input type="text" name="name" id="form-field-1" placeholder="Name" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						
						<div class="form-group">
							<div>
							<label for="form-field-1">Display Name </label>
								<input type="text" name="display_name" id="form-field-1" placeholder="Display Name" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="col-md-4">
						<h2>Permissions</h2>
						</div>
						<div class="col-md-12 block-background">
						<div class="form-group col-sm-6" style="margin-right: 3%;">
							@foreach($permissions as $permission)
								<div class="addusrprm">
									 <input class="addusrprminput" type="checkbox" name="permissions[]" value="{{$permission->id}}" id="form-field-1" placeholder="Phone" class="col-xs-10 col-sm-12" />
									 <span class="addusrprmname">{{$permission->display_name}}</span>
								</div>
							@endforeach
						</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-10 col-md-2">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
						
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>


@endsection