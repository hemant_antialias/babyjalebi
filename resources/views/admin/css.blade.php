<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="{{URL::to('admin-assets/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/font-awesome/4.5.0/css/font-awesome.min.css')}}" />

<!-- page specific plugin styles -->

<!-- text fonts -->
<link rel="stylesheet" href="{{URL::to('admin-assets/css/fonts.googleapis.com.css')}}" />

<!-- ace styles -->
<link rel="stylesheet" href="{{URL::to('admin-assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

<!--[if lte IE 9]>
	<link rel="stylesheet" href="{{URL::to('admin-assets/css/ace-part2.min.css')}}" class="ace-main-stylesheet" />
<![endif]-->
<!-- <link rel="stylesheet" href="{{URL::to('admin-assets/css/ace-skins.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/ace-rtl.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/ace-rtl.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/fileinput.min.css')}}" /> -->
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> -->
<link rel="stylesheet" href="{{URL::to('admin-assets/css/toastr.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/select2.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/daterangepicker.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/bootstrap-datetimepicker.min.css')}}" />
<link rel="stylesheet" href="{{URL::to('admin-assets/css/fileinput.min.css')}}" />


<script src="{{URL::to('admin-assets/js/jquery-2.1.4.min.js')}}"></script>


<!--[if lte IE 9]>
  <link rel="stylesheet" href="{{URL::to('admin-assets/css/ace-ie.min.css')}}" />
<![endif]-->
