@extends('admin.admin_layout')


@section('content')

<style>
	.vendor-img {
    	height: 123px; 
    	width: 111px; 
}
</style>

	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Store</a>
					</li>
					<li class="active">Add Staff</li>
				</ul>

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/vendor/new', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) }}
						
						<div class="row">
						<div class="col-md-4">
						<h3>Store Staff</h3>
						</div>
						<div class="col-md-8 block-background">
						<div class="form-group col-md-12">
							<div>
							<label for="form-field-1">Name </label>
								<input type="text" name="store_name" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-12" />
							    @if ($errors->has('store_name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('store_name') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
				

						
						</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-offset-10 col-md-2">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>


@endsection