@extends('admin.admin_layout')


@section('content')
	
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="{{URL::to('/admin/blog')}}">Blog</a>
					</li>
					<li class="active">Add Blog</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12 block-section">
						{{ Form::open(['url' => 'admin/blog/new', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) }}
						
						

						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Author Name </label>
								<input type="text" name="author_name" id="form-field-1" placeholder="Author Name" class="col-xs-10 col-sm-12" />
							    @if ($errors->has('author_name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('author_name') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Title</label>
								<input type="text" name="title" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group">

							<div class="col-sm-9">
								<label for="form-field-1"> Excerpt </label>
								<!-- <input type="text" name="excerpt" id="form-field-1" placeholder="Excerpt" class="col-xs-10 col-sm-12" /> -->
								<textarea name="excerpt" id="form-field-1" placeholder="Excerpt" class="col-xs-10 col-sm-5 my-editor" /></textarea>

							
							</div>

							<div class="col-sm-9">
							<br>
							<label for="form-field-1"> Blog Category </label>
								<select name="category" style="width: 100%;">
									<option>Select Category</option>
									@foreach($category as $c)
									<option value="{{$c->id}}">{{$c->category}}</option>
									@endforeach
								</select>							

							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-9">

							<label for="form-field-1"> Description </label>
								<textarea name="description" id="form-field-1" placeholder="Description" class="col-xs-10 col-sm-5 my-editor" /></textarea>

							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Seo Title </label>
								<input type="text" name="seo_title" id="form-field-1" placeholder="Seo Title" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Meta Keywords </label>
								<textarea name="meta_keyword" id="form-field-1" placeholder="Meta Keywords" class="col-xs-10 col-sm-12" /></textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Meta Description </label>
								<textarea name="meta_description" id="form-field-1" placeholder="Description" class="col-xs-10 col-sm-12" /></textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Blog Image </label>
							<input type="file" name="blog_image" id="blog_image" class="col-xs-10 col-sm-12" />
							</div>
						</div>
              			<img class="blog-image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" height="100" width="100">
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Reset
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>

@endsection

@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection