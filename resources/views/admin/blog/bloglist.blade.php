@extends('admin.admin_layout')


@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Blog List</h3>
                    <!-- <button id="refresh" class="btn btn-default">Refresh</button> -->
                    <div style="float: right;">
                    <button id="refresh" class="btn btn-default">Refresh</button>
                    <a href="{{URL::to('/admin/blog/new')}}"> <button id="createcategory" class="btn btn-default">Create blog</button></a>
                    </div>
                    <br><br><br>
                    <!-- div.dataTables_borderWrap -->
                    <div class="inventory-data">
                        <table id="blog-dynamic-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Author Name</th>
                                    <th>Blog Title</th>
                                    <th>Blog Category</th>
                                </tr>
                                <tr>
                                    <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="input-filter">Author Name</th>
                                    <th class="input-filter">Blog Title</th>
                                    <th class="input-filter">Blog Category</th>
                                </tr>
                            </thead>

                            <tbody class="product-inventory">

                            @foreach($blogs as $blog)
                                <tr>
                                    <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace category-id" value="{{$blog->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>

                                    <td>{{$blog->author_name}}</td>
                                    <td>
                                        <a href="{{URL::to('/admin/blog/edit/'.$blog->id)}}">{{$blog->title}}</a>
                                    </td>
                                    <td>
                                    	{{$blog->category}}
                                    </td>
                                   <!--  <td class="hidden-480">
                                        @if($blog->status == 1)
                                            <span class="label label-sm label-success">Active</span>
                                        @else
                                            <span class="label label-sm label-warning">Inactive</span>
                                        @endif
                                    </td> -->
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.col -->
    </div><!-- /.row -->

@stop

@section('scripts')

    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
            $('.input-filter').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('.category-status').each( function () {
                var product_status = '<option value="active">Active</option><option value="inactive">Inactive</option>';
                $(this).html( '<select><option value="">Select</option>'+product_status+'</select>' );
            } );

            var myTable = $('#blog-dynamic-table').DataTable({
                paging: true,
                sort: true,
                searching: true,
                select: {
                    style: 'multi'
                }
            });

                        // Apply the search
            myTable.columns().every( function () {
                var that = this;
                
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


                serachTextBox.on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachSelectBox.on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


            } );
            
            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });
        });

    </script>

@endsection