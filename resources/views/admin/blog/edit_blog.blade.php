@extends('admin.admin_layout')


@section('content')
	
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="{{URL::to('/admin/blog')}}">Blog</a>
					</li>
					<li class="active">Edit Blog</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12 block-section">
						{{ Form::open(['url' => 'admin/blog/edit/'.$blog->id, 'method' => 'post', 'files'=>true, 'class' => 'form-horizontal']) }}

						


							<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Author Name </label>
								<input type="text" name="author_name" id="form-field-1" placeholder="Author Name" class="col-xs-10 col-sm-12" value="{{$blog->author_name}}" />
							   
							</div>
						</div>


						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Title</label>
								<input type="text" name="title"  value="{{$blog->title}}" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group">

						<div class="col-sm-9">
							<label for="form-field-1"> Excerpt </label>
								<!-- <input type="text" name="excerpt" id="form-field-1" placeholder="Excerpt" class="col-xs-10 col-sm-12" /> -->
								<textarea name="excerpt"   id="form-field-1" placeholder="Excerpt" class="col-xs-10 col-sm-5 my-editor" />{{$blog->excerpt}}</textarea>

							
						</div>
						<div class="col-sm-9">
							<br>
							<label for="form-field-1"> Blog Category </label>
								{{  Form::select('category', $blog_categories,$blog->blog_category_id,['class'=>'form-control']) }}					

							</div>


						
						</div>
						<div class="form-group">
							<div class="col-sm-9">

							<label for="form-field-1"> Description </label>
								<textarea name="description" id="form-field-1" placeholder="Description" class="col-xs-10 col-sm-5 my-editor" />{{$blog->description}}</textarea>

							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Seo Title </label>
								<input type="text" value="{{$blog->seo_title}}" name="seo_title" id="form-field-1" placeholder="Seo Title" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						
						<div class="form-group">
							

							<div class="col-sm-9">
							<label for="form-field-1"> Meta Keywords </label>
								<textarea name="meta_keyword" id="form-field-1" placeholder="Meta Keywords" class="col-xs-10 col-sm-12" />{{$blog->meta_keyword}}</textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Meta Description </label>
								<textarea name="meta_description" id="form-field-1" placeholder="Description" class="col-xs-10 col-sm-12" />{{$blog->meta_description}}</textarea>
							</div>
						</div>

						<div class="form-group">
							
							<div class="col-sm-9">
							<label for="form-field-1"> Blog Image </label>

								<input type="file" name="blog_image" id="blog_image" class="col-xs-10 col-sm-12" />
								@if(isset($blog->image))
									<img class="blog-image" src="{{URL::to('/blog/'.$blog->id.'/'.$blog->image)}}" height="100" width="100">
								@else	
									<img class="blog-image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" height="100" width="100">
								@endif						
							</div>
					
						</div>
					
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Reset
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>

@endsection

@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection