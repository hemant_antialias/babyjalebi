<script src="{{URL::to('admin-assets/js/select2.min.js')}}"></script>

<select name="exclude_collection[]" id="exclude-collection-name" class="form-control" multiple>
	@foreach($categories as $category )
		dd($category->name);
		<option value="{{$category->id}}">{{$category->name}}</option>
	@endforeach
</select>


<script>
	$('#exclude-collection-name').select2({
		placeholder: 'Choose Collection',
		tags: true
	});
</script>

