<script src="{{URL::to('admin-assets/js/select2.min.js')}}"></script>

<select name="product[]" id="product-name" class="form-control" multiple>
	@foreach($products as $product )
		<option value="{{$product->id}}">{{$product->product_title}}</option>
	@endforeach
</select>
@if ($errors->has('product'))
    <span class="help-block" style="width:500px">
        <strong>{{ $errors->first('product') }}</strong>
    </span>
@endif

<script>
	$('#product-name').select2({
		placeholder: 'Choose products',
		tags: true
	});
</script>