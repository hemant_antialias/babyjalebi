@if(count($productByCategory)>0)
<table width="200" border="1"  style="margin-left:25px; margin-top:75px;">
  <tr>
    <td>Image</td>
    <td>Product Title</td>
    <td>Price</td>
  </tr>
  @foreach($productByCategory as $productName)
  <tr>
    
    @if($productName->images!='')
          <td class="next-table__image-cell hide-when-printing">
            <img title="{{$productName->images}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$productName->id.'/'.$productName->images)}}" alt="{{$productName->product_title}}" style="width: 50px;">
          </td>
        @else
          <td class="next-table__image-cell hide-when-printing">
            <img title="No Image" class="block aspect-ratio__content" src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="No Image" style="width: 50px;">
           </td>
        @endif


    <td>{{$productName->product_title}}</td>
    <td>{{$productName->price}}</td>
  </tr>
  @endforeach
</table>
@endif