<table id="simple-table" class="table  table-bordered table-hover product-detail-by-keyword">
	@if(count($products)>0)
	<thead>
		<tr>
			<th>Product</th>
			<th>Price</th>
			<th>Quantity</th>
			<th></th>
		</tr>
	</thead>

	<tbody>

		@foreach($products as $product)
			<tr>
				<td>
					<a href="#">{{$product->product_title}}</a>
				</td>
				<td>Rs.{{$product->price}}</td>


				<?php
					$cart_content = Cart::content();

					$product_id = $product->id;
					$data = Cart::search(function ($cartItem, $rowId) use ($product_id) {
			            return $cartItem->id == $product_id;
			        });
					// var_dump($data);
				?>

				@if(count($data) > 0)
				<?php
            		
            		$qty = '';
					foreach ($data as $value) {
		                $qty = $value->qty;
		            }
				?>
				<td><input type="number" value="{{$qty}}" class="product_quantity_{{$product->id}}" disabled="disabled" style="width: 50%;"></td>
				@else
				<td><input type="number" value="1" class="product_quantity_{{$product->id}}" style="width: 50%;"></td>
				@endif
				@if($product->quantity > 0)
					<td>
						@if(count($data) > 0)
							Added
						@else
		          		<form id="add_to_order_{{$product->id}}">
				            <input type="hidden" name="quantity" value="1" class="pquantity_{{$product->id}}">
				            <input type="hidden" name="product_title" value="{{$product->product_title}}" class="product_title">
				            <input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
				            <input type="hidden" name="store_id" value="" class="store_id">
				            <input type="hidden" name="product_price" value="{{$product->price}}" class="product_price">
				            @if(isset($product->product_images))
				            <input type="hidden" name="image_url" value="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" class="image_url">
				            @endif
				            
				             <input type="button" onclick="addToOrder('{{$product->id}}');" class="cartmobhide btn btn-primary add-to-cart-{{$product->id}} detail-mob-sub" value="Add" style="background: #02c9c6 !important;color: #fff !important;  border-radius: 3px !important; width: 100%; ">
				            
		          		</form>
		          		@endif
		          	</td>
			    @else
			    	<td>Out of Stock</td>
		        @endif
			</tr>
		@endforeach 
	</tbody>
		@else
		<h3>No Product Found</h3>
		@endif

</table>
<script>


	$('.product_id').click(function(){
		var base_url = "{{URL::to('/')}}";
		if($(this).is(":checked"))
	    {
	        
	        $.ajax({
	          	url: base_url+'/admin/add-product-to-session',
	          	type: "post",
	          	data: 
	          	{
	                "product_id": $(this).val()
              		// "store_id": $('#vendor_name').val()
	            },
	          	success:function(data)
	          	{
	          		console.log('product added to session.');
	          	}
      		});
	    }
	    if ($(this).is(":not(:checked)")) {
	    	$.ajax({
	          	url: base_url+'/admin/delete-product-to-session',
	          	type: "post",
	          	data: 
	          	{
	                "product_id": $(this).val()
              		// "store_id": $('#vendor_name').val()
	            },
	          	success:function(data)
	          	{
	          		console.log('product deleted from session.');
	          	}
      		});
	    }
	});
</script>
<script>
function addToOrder(id)
{
	var vendor_id = $('#vendor_name').val();
    $('.store_id').val(vendor_id);
    var pquantity = $('.product_quantity_'+id).val();
    $('.pquantity_'+id).val(pquantity);
    var url = "{{URL::to('/admin/add-to-cart')}}"
    $.ajax({
       type: "post",
       url: url,
       data: $('#add_to_order_'+id).serialize(),
       success: function(data)
       {
            if (data.status==0) {
                alert(data.message);
                $('.update_cart_count').html('('+data.cart_count+')');
                return;
            }
            if (data.status==1) {
            	$('.add-to-cart-'+id).val('Added.');
            	toastr.success(data.message, {timeOut: 100000})
            	$('.add-to-cart-'+id).attr('disabled', true);
            }
       }
    });	
}
  // $('.add-to-cart').click(function(e){
  //   e.preventDefault();
    // var vendor_id = $('#vendor_name').val();
    // $('.store_id').val(vendor_id);
    // var pquantity = $('.product_quantity_').val();
    // alert(pquantity);
    // // $('.pquantity').val(pquantity);

    // var url = "{{URL::to('/admin/add-to-cart')}}"
    // $.ajax({
    //    type: "post",
    //    url: url,
    //    data: $(this).closest('form').serialize(),
    //    success: function(data)
    //    {
    //         if (data.status==0) {
    //             alert(data.message);
    //             $('.update_cart_count').html('('+data.cart_count+')');
    //             return;
    //         }
    //         if (data.status==1) {
    //         	$('.add-to-cart').val('Added.');
    //         	toastr.success(data.message, {timeOut: 100000})
    //         }
    //    }
    // });
  // });

</script>