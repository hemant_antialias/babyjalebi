<table id="simple-table" class="table  table-bordered table-hover product-detail-by-keyword">
	@if(count($products)>0)
	<thead>
		<tr>
			<th></th>
			<th>Product</th>
			<th>Price</th>
		</tr>
	</thead>

	<tbody>

		@foreach($products as $product)
			<tr>
				<td class="center">
					<label class="pos-rel pid">
						<input type="checkbox" value="{{$product->id}}" class="ace" />
						<span class="lbl"></span>
					</label>
				</td>
				<td>
					<a href="#">{{$product->product_title}}</a>
				</td>
				<td>Rs.{{$product->price}}</td>
			</tr>
		@endforeach
	</tbody>
		@else
		<h3>No Product Found</h3>
		@endif

</table>
