<script src="{{URL::to('admin-assets/js/select2.min.js')}}"></script>

<select name="collection[]" id="collection-name" class="form-control" multiple>
	@foreach($categories as $category )
		<option value="{{$category->id}}">{{$category->name}}</option>
	@endforeach
</select>


<script>
	$('#collection-name').select2({
		placeholder: 'Choose Collection',
		tags: true
	});
</script>