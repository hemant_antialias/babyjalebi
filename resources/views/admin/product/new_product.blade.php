	@extends('admin.admin_layout')


	@section('content')


	<style type="text/css">
	#dvPreview img
	{
		padding-left: 2%; 
	}
		ul{
			list-style-type: none;
		}
		.property_image {
			height: 170px;
		}
		.background-layout
		{
			padding: 2%;
	    background-color: #ffffff;
	    border-radius: 3px;
	    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
	   
		}
		.padding-lft
		{
			 margin-right: 3%;
		}
	  .next-input--stylized
	  {

	    position: relative;
	    display: -webkit-flex;
	    display: -ms-flexbox;
	    display: flex;
	    overflow: hidden;
	    background: #ffffff;
	    display: inline-block;
	    max-width: auto;
	    min-width: 75px;
	    vertical-align: baseline;
	    width: auto;
	    height: auto;
	    padding: 1px;
	    margin: 0;
	    border: 1px solid #95a7b7;
	    border-radius: 0;
	    border-style: inset;
	    color: #000000;
	    -webkit-appearance: none;
	    -moz-appearance: none;
	    padding: 5px 10px;
	    border: 1px solid #d3dbe2;
	    border-radius: 3px;
	    font-size: 1.14286rem;
	    line-height: 1.71429rem;
	    font-weight: 400;
	    text-transform: initial;
	    letter-spacing: initial;
	    box-sizing: border-box;
	    display: block;
	    width: 100%;
	    margin-right: 0.5px;

	  }
	  .file-caption
	  {
	  	height: 42px !important;
	  }
	</style>	
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="{{URL::to('/admin/product')}}">Product</a>
						</li>
						<li class="active">Add Product</li>
					</ul><!-- /.breadcrumb -->

				</div>
				<br>
				<div class="row">

					
							{{ Form::open(['url' => 'admin/product/new', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}
													<div class="col-md-12">
													<div class="col-xs-12" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;">
												

							<div class="form-group col-sm-12">
						<!-- <label class=""> <b>Organization  </b> </label> -->
						</div>
							<!-- <div class="form-group col-sm-4">
								<label class="col-sm-12" for="form-field-1"> Product Type </label>

								<div class="col-sm-12">
									<select name="meat_type" class="col-sm-12" id="meat_type">
										<option value="">Select Product Type</option>
										@foreach($meat_types as $meat_type )
											<option value="{{$meat_type->id}}">{{$meat_type->name}}</option>
										@endforeach
									</select>
									
									</div> -->
									<!-- <div class="col-md-6">
									<button type="button" data-toggle="modal" class="btn btn-primary btn-file" style="width: 100%;" data-target="#addProductType">Add New Meat Type</button>
								</div> -->
							<!-- </div> -->

							<!-- <div class="form-group col-sm-4">
								<label class="" for="form-field-1"> Brand </label>

								<div class="col-sm-12">
									<select name="brand_type" class="col-md-12">
										<option value="">Select Brand</option>
										@foreach($brands as $brand )
											<option value="{{$brand->id}}">{{$brand->name}}</option>
										@endforeach
									</select>
									
								</div>
							</div> -->
							<div class="form-group col-sm-8">
								<label class="" for="form-field-1"> Title </label>

								<div class="col-sm-12">
									<input type="text" name="product_title" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-12" />
								    @if ($errors->has('product_title'))
		                                <span class="help-block">
		                                    <strong>{{ $errors->first('product_title') }}</strong>
		                                </span>
		                            @endif
								</div>
							</div>
							<div class="form-group col-sm-4">
							<div class="">
								<label for="form-field-1"> Category Name </label>
									<select name="product_category[]" onclick="myFunction()" id="catval" class="col-md-12" multiple>
										<option value="">Select Category</option>
										@foreach($product_categories as $category )
											<option value="{{$category->id}}">{{$category->name}}</option>
										@endforeach
									</select>

								    @if ($errors->has('product_category'))
		                                <span class="help-block">
		                                    <strong>{{ $errors->first('product_category') }}</strong>
		                                </span>
		                            @endif
								</div>
							</div>

								<div class="hr hr32 hr-dotted col-md-12"></div>
							<div class="form-group">
								<label class="col-sm-12" for="form-field-1">Short Description </label>

								<div class="col-sm-12">
										<textarea name="product_description" class="col-sm-12 my-editor"></textarea>
								</div>
							</div>
							<div class="hr hr32 hr-dotted col-md-12"></div>
					          <div class="form-group col-md-4">
								<label class="" for="form-field-1"> Video Link </label>

								<div class="col-sm-12">
									<input type="text" name="video_link" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-12" />
								    @if ($errors->has('link'))
		                                <span class="help-block">
		                                    <strong>{{ $errors->first('link') }}</strong>
		                                </span>
		                            @endif
								</div>
							</div>

							<div class="form-group col-md-4">
								<div class="" class="form-control">
								<label for="form-field-1"> Tags </label>
									<select name="product_tag[]" id="product-tag" multiple style="width:100%;">
										@foreach($product_tags as $tag )
											<option value="{{$tag->id}}">{{$tag->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
					          
						<div class="form-group col-md-4">
						<label class="col-sm-12"> <b>Visibility  </b> </label>
							</div>
							<div class="col-md-2">
							<div class="form-group">
								<!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->

											<input name="online_status" type="checkbox" value="1" checked="checked" /> Online <br>
											<input name="personalised" type="checkbox" value="1" /> Personalised <br>
							</div>
							</div>
							<div class="col-md-2" id="beddingtag" style="display: none;"> 
							<div class="form-group ">
								<!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->

											<input name="dohar" type="checkbox" value="1" /> Dohar <br>
											<input name="quilt" type="checkbox" value="1" /> Quilt 
							</div>
							</div>
							<div class="hr hr32 hr-dotted col-md-12"></div>
							<div class="form-group col-sm-8">
								<label class="col-sm-12" style="height: 42px !important;" for="form-field-1">Images </label>

								<div class="">
									<input id="product_images" style="height: 42px !important;" name="product_images[]" type="file" multiple>
								</div>
								<div id="dvPreview">
								</div>
							</div>
							<br>
							<br>
							<div class="col-md-4">
					          <div class="form-group col-md-12">
					            <div class="col-sm-12">
					              <label for="form-field-1"> Featured Image 
					              </label>
					              <input type="file" name="featured_images" id="featured_image" class="col-xs-10 col-sm-12" />
					            </div>
					            </div>
					            <div class="col-md-12">
					             
					              <div id='output_image'>
					              </div>
					              <img class="featured-image" src="{{URL::to('/web-assets/images/default_product.jpg')}}" style="width: 25%">
					            </div>
					          </div>
					</div>
							</div>

							<div class="col-md-12" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;margin-top: 3%;">
							<div class="form-group">
								
							<label class="col-sm-12"> <b>Pricing</b> </label>
							<br><br>
								<div class="col-sm-4">
								  	<label class="" for="form-field-1"> Price </label>
								  	<br>
								  	<input type="text" name="price" id="form-field-1" value="" placeholder="Rs. 0.00" class="col-xs-10 col-sm-12">
								</div>
								<div class="col-sm-4">
									<label class="" for="form-field-1"> Compare at Price </label><br>
								  	<input type="text" name="compare_price" id="form-field-1" placeholder="Rs." class="col-xs-10 col-sm-12">
								</div>
								<div class="col-sm-4">
									<label class="" for="form-field-1"> Quantity</label><br>
								  	<input type="text" name="quantity" id="form-field-1" placeholder="Quantity" class="col-xs-10 col-sm-12">
								</div>
							</div>
							<!-- <div class="form-group">
								<div class="checkbox">
									<label>
										<input name="is_tax" type="checkbox" class="ace ace-checkbox-2" checked="checked" />
										<span class="lbl"> Charge taxes on this product</span>
									</label>
								</div>
							</div>	 -->
							<div class="hr hr32 hr-dotted"></div>
							<div class="form-group">
							<label class="col-sm-12"> <b>Inventory  </b> </label>
							<br><br>
								<div class="col-sm-4">
								<label for="form-field-1"> SKU(Stock Keeping Unit) </label>
									<input type="text" name="i_sku" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
								</div>
							
								<div class="col-sm-4">
								<label for="form-field-1"> Barcode (ISBN, UPC, GTIN, etc.) </label>
									<input type="text" name="i_barcode" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
								</div>
								<div class="form-group col-sm-4">
								<label class="" for="form-field-1"> Tax </label>

								<div class="col-sm-12">
									<select name="tax" class="col-md-12">
										<option value="">Select Tax</option>
										<option value="0">0%</option>
										<option value="12.5">12.5%</option>
										<option value="5">5%</option>
										<option value="5.25">5.25%</option>
										<option value="13.125">13.125%</option>
									</select>
								</div>
							</div>

							</div>
							<div class="hr hr32 hr-dotted"></div>
							<label class="col-md-12"> <b>Shipping  </b> </label>
							<div class="form-group col-md-4">
							
								<label class="col-sm-12" for="form-field-1"> Weight </label>

								<div class="col-sm-12">
									<input type="text" name="weight" id="form-field-1" placeholder="Weight" class="col-xs-11 col-sm-11" />
									<select name="unit" style="height: 34px;">
										<option value="g">g</option>
										<option value="lb">lb</option>
										<option value="oz">oz</option>
										<option value="kg">kg</option>
										
									</select>
								</div>
							</div>
							<div class="form-group col-md-4">
								<div class="">
	              <label for="form-field-1"> Page Title </label>
									<input type="text" name="product_page_title" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
								</div>
							</div>
							<div class="form-group col-md-4">
								<div class="col-sm-12">
	              <label for="form-field-1"> Page URL </label>
	              <div class="next-input--stylized"><span class="next-input__add-on next-input__add-on--before"></span><input class="next-input next-input--invisible" size="30" type="text" name="url" placeholder="" style="width: 71%; border: none;"></div>
									<!-- <input type="text" name="url" id="form-field-1" placeholder="https://www.lionfresh.com/products/" class="col-xs-10 col-sm-12" /> -->
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> Meta Description </label>
									<textarea name="meta_description"  class="col-md-12 my-editor"></textarea>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> Overview </label>
									<textarea name="overview"  class="col-md-12 my-editor"></textarea>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> Details that matter </label>
									<textarea name="details_that_matter"  class="col-md-12 my-editor"></textarea>
								</div>
							</div>
							<!-- <div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> Safety information </label>
									<textarea name="safety_information"  class="col-md-12 my-editor"></textarea>
								</div>
							</div> -->
							<div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> Dimensions </label>
									<textarea name="dimensions"  class="col-md-12 my-editor"></textarea>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> Care </label>
									<textarea name="care"  class="col-md-12 my-editor"></textarea>
								</div>
							</div>
							<!-- <div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> Our commitment </label>
									<textarea name="our_commitment"  class="col-md-12 my-editor"></textarea>
								</div>
							</div> -->
							<!-- <div class="form-group col-md-12">
								<div class="">
	              <label for="form-field-1"> IN House design </label>
									<textarea name="in_house_design"  class="col-md-12 my-editor"></textarea>
								</div>
							</div> -->
							<!-- for color -->
							<div id='variation1' style="display: none">
							<div class="col-md-12" >
							 <div class="pi1" id='vari1' >
							 <div class="price_inventory" id="set1">

							 <?php
							  $attribute_values = DB::table('attribute_value')
                                                            ->where('attribute_id', $productattribute[0]->id)->get();
							 ?>
                            <div class="form-group ">
                                <label class="control-label">{{$productattribute[0]->name}}</label>
                                <div class="">
                                    <select name="attributename0[]" class="form-control col-md-7 col-xs-12">
                                    <option value="">Select</option>
                                        @foreach($attribute_values as $value)
                                        	<option value="{{$value->id}}">{{$value->value}}</option>                                    
                                   		@endforeach                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Images</label>
                                <div class="">

								<input type="file" style="height: 42px !important;" name="product_images_var[]" id="image_file" class="attr_image_file" multiple>
								</div>
								<div id="dvPreviews">
								</div>
                            </div>
                            
                            <button class="btn btn-danger remove" onclick="delDupe1()">DEL -</button>
                            </div>
                        </div>
                            <button type="button" class="btn btn-success add" onclick="duplicate1()">ADD +</button>
                            </div>
                            </div>
                            <!-- end color section -->
                            <div id='variation' style="display: none">
							<div class="col-md-12" >
							 <div class="pi" id='vari' >
							 <div class="price_inventory" id="set">
							 <?php
							  $attribute_values = DB::table('attribute_value')
                                                            ->where('attribute_id', $productattribute[1]->id)->get();
							 ?>
                            <div class="form-group ">
                                <label class="control-label">{{$productattribute[1]->name}}</label>
                                <div class="">
                                    <select name="attributename0[]" class="form-control col-md-7 col-xs-12">
                                    <option value="">Select</option>
                                        @foreach($attribute_values as $value)
                                        	<option value="{{$value->id}}">{{$value->value}}</option>                                    
                                   		@endforeach                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Images</label>
                                <div class="">

								<input type="file" style="height: 42px !important;" name="product_images_var[]" id="image_file" class="attr_image_file" multiple>
								</div>
								<div id="dvPreviews">
								</div>
                            </div>
                            
                            <button class="btn btn-danger remove" onclick="delDupe()">DEL -</button>
                            </div>
                        </div>
                            <button type="button" class="btn btn-success add" onclick="duplicate()">ADD +</button>
                            </div>
                            </div>
                             <div class="form-group col-md-12">
							<input type="button" name="answer" id="answer" value="Add Pattern" class="btn btn-success" onclick="showDiv()" />
							</div>
							<div class="form-group col-md-12">
							<input type="button" name="answer" id="answer1" value="Add Color" class="btn btn-success" onclick="showDiv1()" />
							</div>

							  <div class="form-group">
            <label class="col-sm-12" for="form-field-1"> RELATED  PRODUCTS
            </label>
            <div class="col-sm-12 product-list">
              {{Form::select('product[]', $products,null, array('id' => 'product_name','class' => 'col-xs-10 col-sm-12', 'multiple'))}}
            </div>
          </div>
           
          <div class="product_list">
          </div>
							<div class="form-group">
								<div class="col-md-offset-10 col-md-2">
									<button class="btn btn-info" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
								</div>
							</div>
							
							<!-- <div class="form-group">
								<div class="col-md-offset-10 col-md-2">
									<button class="btn btn-info" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
								</div>
							</div> -->
							</div>
							</div>
							
					 
					<br>

						{{ Form::close() }}
					  
				</div>
			</div>
		</div>
		
	@endsection

	@section('scripts')
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
	<script>
		var base_url = '{{URL::to("/")}}';
		$('body').on('click', '.add-product-type', function(){

			// alert('dd');
			$.ajax({
	            url:base_url+'/admin/product/add-product-type',
	            type:'post',
	            data:{
	            	'product_type': $('#product_type').val()
	            },
	            success:function(data){
	            	if (data.status == 1) {
	            		$('#meat_type').append($('<option value="'+data.type_id+'" selected="selected">'+data.product_type+'</option>'));
						$("#addProductType").modal('hide');
						toastr.success(data.message, {timeOut: 100000})
	            	};
	            }
			});
			document.getElementById("product_type").value='';
		});
	</script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	
	<!-- inline scripts related to this page -->
			<script type="text/javascript">
				jQuery(function($){
		
		$('textarea[data-provide="markdown"]').each(function(){
	        var $this = $(this);

			if ($this.data('markdown')) {
			  $this.data('markdown').showEditor();
			}
			else $this.markdown()
			
			$this.parent().find('.btn').addClass('btn-white');
	    })
		
		
		
		function showErrorAlert (reason, detail) {
			var msg='';
			if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
			else {
				//console.log("error uploading file", reason, detail);
			}
			$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
			 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
		}

		//$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

		//but we want to change a few buttons colors for the third style
		$('#editor1').ace_wysiwyg({
			toolbar:
			[
				'font',
				null,
				'fontSize',
				null,
				{name:'bold', className:'btn-info'},
				{name:'italic', className:'btn-info'},
				{name:'strikethrough', className:'btn-info'},
				{name:'underline', className:'btn-info'},
				null,
				{name:'insertunorderedlist', className:'btn-success'},
				{name:'insertorderedlist', className:'btn-success'},
				{name:'outdent', className:'btn-purple'},
				{name:'indent', className:'btn-purple'},
				null,
				{name:'justifyleft', className:'btn-primary'},
				{name:'justifycenter', className:'btn-primary'},
				{name:'justifyright', className:'btn-primary'},
				{name:'justifyfull', className:'btn-inverse'},
				null,
				{name:'createLink', className:'btn-pink'},
				{name:'unlink', className:'btn-pink'},
				null,
				{name:'insertImage', className:'btn-success'},
				null,
				'foreColor',
				null,
				{name:'undo', className:'btn-grey'},
				{name:'redo', className:'btn-grey'}
			],
			'wysiwyg': {
				fileUploadError: showErrorAlert
			}
		}).prev().addClass('wysiwyg-style2');

		
		/**
		//make the editor have all the available height
		$(window).on('resize.editor', function() {
			var offset = $('#editor1').parent().offset();
			var winHeight =  $(this).height();
			
			$('#editor1').css({'height':winHeight - offset.top - 10, 'max-height': 'none'});
		}).triggerHandler('resize.editor');
		*/
		

		$('#editor2').css({'height':'200px'}).ace_wysiwyg({
			toolbar_place: function(toolbar) {
				return $(this).closest('.widget-box')
				       .find('.widget-header').prepend(toolbar)
					   .find('.wysiwyg-toolbar').addClass('inline');
			},
			toolbar:
			[
				'bold',
				{name:'italic' , title:'Change Title!', icon: 'ace-icon fa fa-leaf'},
				'strikethrough',
				null,
				'insertunorderedlist',
				'insertorderedlist',
				null,
				'justifyleft',
				'justifycenter',
				'justifyright'
			],
			speech_button: false
		});
		
		


		$('[data-toggle="buttons"] .btn').on('click', function(e){
			var target = $(this).find('input[type=radio]');
			var which = parseInt(target.val());
			var toolbar = $('#editor1').prev().get(0);
			if(which >= 1 && which <= 4) {
				toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
				if(which == 1) $(toolbar).addClass('wysiwyg-style1');
				else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
				if(which == 4) {
					$(toolbar).find('.btn-group > .btn').addClass('btn-white btn-round');
				} else $(toolbar).find('.btn-group > .btn-white').removeClass('btn-white btn-round');
			}
		});


		

		//RESIZE IMAGE
		
		//Add Image Resize Functionality to Chrome and Safari
		//webkit browsers don't have image resize functionality when content is editable
		//so let's add something using jQuery UI resizable
		//another option would be opening a dialog for user to enter dimensions.
		if ( typeof jQuery.ui !== 'undefined' && ace.vars['webkit'] ) {
			
			var lastResizableImg = null;
			function destroyResizable() {
				if(lastResizableImg == null) return;
				lastResizableImg.resizable( "destroy" );
				lastResizableImg.removeData('resizable');
				lastResizableImg = null;
			}

			var enableImageResize = function() {
				$('.wysiwyg-editor')
				.on('mousedown', function(e) {
					var target = $(e.target);
					if( e.target instanceof HTMLImageElement ) {
						if( !target.data('resizable') ) {
							target.resizable({
								aspectRatio: e.target.width / e.target.height,
							});
							target.data('resizable', true);
							
							if( lastResizableImg != null ) {
								//disable previous resizable image
								lastResizableImg.resizable( "destroy" );
								lastResizableImg.removeData('resizable');
							}
							lastResizableImg = target;
						}
					}
				})
				.on('click', function(e) {
					if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
						destroyResizable();
					}
				})
				.on('keydown', function() {
					destroyResizable();
				});
		    }

			enableImageResize();

			/**
			//or we can load the jQuery UI dynamically only if needed
			if (typeof jQuery.ui !== 'undefined') enableImageResize();
			else {//load jQuery UI if not loaded
				//in Ace demo ./components will be replaced by correct components path
				$.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
					enableImageResize()
				});
			}
			*/
		}


	});
	</script>
	<script>
		function showDiv() {
   document.getElementById('variation').style.display = "block";
   // $("#" + id).attr('id', 'sel' + rowIndex);
   document.getElementById('answer').style.display = "none";
   document.getElementById('answer1').style.display = "none";
}

	</script>
	<script>
		function showDiv1() {
   document.getElementById('variation1').style.display = "block";
   // alert('ffdfd')
   // $("#" + id).attr('id', 'sel' + rowIndex);
   document.getElementById('answer').style.display = "none";
   document.getElementById('answer1').style.display = "none";
}

	</script>
<script>



var i = 0;
var origina2 = document.getElementById('set');
var form2 = document.getElementById('vari');
var btn2 = form2.querySelector('button');

var attrvalue;
var base_url = "{{url('/')}}"

$('select').change(function(){
	attrvalue = $(this).val();
	// alert(attrvalue);
})

$('.attr_image_file').on('change', function(e){
	e.preventDefault();
		var data = new FormData();
		jQuery.each(jQuery(this)[0].files, function(i, file) {
		data.append('file-'+i, file);
		data.append('attrid', attrvalue);
	});

	$.ajax({
      	url: base_url+'/admin/add-variation-image',
     	type: "post",
     	cache: false,
     	contentType: false,
     	processData: false,
      	data: data,
      	success:function(data)
      	{
	      	console.log('done');
      		
      	}
  	});

});

function duplicate() {
  	i++;
  	var clone2 = origina2.cloneNode(true);

  	clone2.id = "clone" + i;
  	form2.appendChild(clone2);
  	
	$('select').change(function(){
    	attrvalue = $(this).val();
    })

  	$('.attr_image_file').on('change', function(e){
		e.preventDefault();
  		var data = new FormData();
			jQuery.each(jQuery(this)[0].files, function(i, file) {
    		data.append('file-'+i, file);
    		data.append('attrid', attrvalue);
		});

		$.ajax({
          	url: base_url+'/admin/add-variation-image',
         	type: "post",
         	cache: false,
         	contentType: false,
         	processData: false,
          	data: data,
          	success:function(data)
          	{
	      		console.log('done');
          	}
      	});

	});

}

function delDupe() {

  var clone1 = document.getElementById('clone' + i);

  if (!clone1) {
    return false;
  }

  form1.removeChild(clone1);

  i--;
}
</script>
<!-- for color -->
<script>



var i = 0;
var original = document.getElementById('set1');
var form1 = document.getElementById('vari1');
var btn1 = form1.querySelector('button');

var attrvalue;
var base_url = "{{url('/')}}"

$('select').change(function(){
	attrvalue = $(this).val();
})

$('.attr_image_file').on('change', function(e){
	e.preventDefault();
		var data = new FormData();
		jQuery.each(jQuery(this)[0].files, function(i, file) {
		data.append('file-'+i, file);
		data.append('attrid', attrvalue);
	});

	$.ajax({
      	url: base_url+'/admin/add-variation-image',
     	type: "post",
     	cache: false,
     	contentType: false,
     	processData: false,
      	data: data,
      	success:function(data)
      	{
	      	console.log('done');
      		
      	}
  	});

});

function duplicate1() {
  	i++;
  	var clone1 = original.cloneNode(true);

  	clone1.id = "clone" + i;
  	form1.appendChild(clone1);
  	
	$('select').change(function(){
    	attrvalue = $(this).val();
    })

  	$('.attr_image_file').on('change', function(e){
		e.preventDefault();
  		var data = new FormData();
			jQuery.each(jQuery(this)[0].files, function(i, file) {
    		data.append('file-'+i, file);
    		data.append('attrid', attrvalue);
		});

		$.ajax({
          	url: base_url+'/admin/add-variation-image',
         	type: "post",
         	cache: false,
         	contentType: false,
         	processData: false,
          	data: data,
          	success:function(data)
          	{
	      		console.log('done');
          	}
      	});

	});

}

function delDupe1() {

  var clone1 = document.getElementById('clone' + i);

  if (!clone1) {
    return false;
  }

  form1.removeChild(clone1);

  i--;
}
</script>
<script>
  var base_url = "{{URL::to('/')}}";
  $("#product_name").chosen({
    no_results_text: "Oops, nothing found!"}
                           ).change(function(e, params){
    var product_id = $("#product_name").chosen().val();
    $.ajax({
      url: base_url+'/admin/add-products-to-category',
      type: "post",
      data: 
      {
        "id": product_id,
      },
      success:function(data)
      {
        $('.product_list').html(data);
      }
    });
  });

  $('#store_id').change(function(){
    var store_id = $('#store_id').val();

    $.ajax({
        url: base_url+'/all-products-by-stores-category',
        type: "post",
        data: 
        {
          "store_id": store_id,
        },
        success:function(data)
        {
          $('.product-label').css('display', 'block');
          $('.product-list').html(data);
        }
    });
  });

</script>
<!-- for showing product images -->
<script language="javascript" type="text/javascript">
window.onload = function () {
    var fileUpload = document.getElementById("product_images");
    fileUpload.onchange = function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = document.getElementById("dvPreview");
            dvPreview.innerHTML = "";
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            for (var i = 0; i < fileUpload.files.length; i++) {
                var file = fileUpload.files[i];
                if (regex.test(file.name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = document.createElement("IMG");
                        img.height = "150";
                        img.width = "150";
                        img.src = e.target.result;
                        dvPreview.appendChild(img);
                    }
                    reader.readAsDataURL(file);
                } else {
                    alert(file.name + " is not a valid image file.");
                    dvPreview.innerHTML = "";
                    return false;
                }
            }
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    }
};
</script>
<script>
	function myFunction() {
    var x = $('#catval').val();

    // alert(x);
    if (x == 3) {
        $("#beddingtag").css("display", "block");
    } else {
        $("#beddingtag").css("display", "none");
        
    }
}
</script>
<!-- variations product images -->

	@endsection