@extends('admin.admin_layout')

@section('content')
<div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                
                    <h3 class="header smaller lighter blue">Category List</h3>
                    <div style="float: right;">
                    <button id="refresh" class="btn btn-default">Refresh</button>
                    <a href="{{URL::to('/admin/product/category/new')}}"> <button id="createcategory" class="btn btn-default">Create Category</button></a>
                    </div>
                    <br><br><br>
                    <!-- div.dataTables_borderWrap -->
                    <div class="inventory-data">
                        <table id="product-dynamic-table" class="table table-striped table-bordered table-hover" data-page-length='50'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Collection Type</th>
                                    <th class="hidden-480 vendor-status">Status</th>
                                </tr>
                                <tr>
                                    <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="input-filter">Name</th>
                                    <th class="input-filter">Collection Type</th>
                                    <th class="hidden-480 category-status">Status</th>
                                </tr>
                            </thead>

                            <tbody class="product-inventory">

                            @foreach($product_categories as $category)
                                <tr data-id="{{$category->id}}">
                                    <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace category-id" value="{{$category->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <a href="{{URL::to('/admin/product/category/edit/'.$category->id)}}">{{$category->name}}</a>
                                    </td>
                                   
                                    <td>{{$category->collection_type_name}}</td>
                                    <td class="hidden-480">
                                        {{ Form::select('status', ['1' => 'Active', '0' => 'Inactive'], $category->status, ['class' => 'field status']) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.col -->
    </div><!-- /.row -->
@stop
@section('scripts')

    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
            $('.input-filter').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('.category-status').each( function () {
                var product_status = '<option value="active">Active</option><option value="inactive">Inactive</option>';
                $(this).html( '<select><option value="">Select</option>'+product_status+'</select>' );
            } );

            var myTable = $('#product-dynamic-table').DataTable({
                paging: true,
                sort: true,
                searching: true,
                select: {
                    style: 'multi'
                }
            });

                        // Apply the search
            myTable.columns().every( function () {
                var that = this;
                
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


                serachTextBox.on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachSelectBox.on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                            alert("ddffdfd");
                    }
                } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


            } );
            
            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });
        });

    </script>
    <script>
    $('.status').change(function(){
        var base_url = "{{URL::to('/')}}";
        var status = $('.status').val();
        var trid = $(this).closest('tr').attr('data-id');
        $.ajax({
            url: base_url+'/admin/category/change-status',
            type: "post",
            data: 
            {
                "id": trid,
                "status": status
            },
            success:function(data)
            {
                if (data.status == 1) {
                    toastr.success('Status changed.', {timeOut: 100000})
                }
            }
        });
    })
    </script>

@endsection