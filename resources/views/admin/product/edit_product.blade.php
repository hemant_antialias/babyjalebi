@extends('admin.admin_layout')


@section('content')

<style type="text/css">
.deletevar
{
	position: absolute;
    float: right;
    right: 6px;
    top: -6px;
    background: red;
    color: #fff;
    height: 30px;
    width: 30px;
    line-height: 28px;
    text-align: center;
    border-radius: 50%;
    border: 1px solid #dc0404;
}
  ul{
    list-style-type: none;
  }
  .property_image {
    height: 170px;
  }
  .background-layout
  {
    padding: 2%;
    background-color: #ffffff;
    border-radius: 3px;
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
   
  }
  .padding-lft
  {
     margin-right: 3%;
  }
  .next-input--stylized
  {

    position: relative;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    overflow: hidden;
    background: #ffffff;
    display: inline-block;
    max-width: auto;
    min-width: 75px;
    vertical-align: baseline;
    width: auto;
    height: auto;
    padding: 1px;
    margin: 0;
    border: 1px solid #95a7b7;
    border-radius: 0;
    border-style: inset;
    color: #000000;
    -webkit-appearance: none;
    -moz-appearance: none;
    padding: 5px 10px;
    border: 1px solid #d3dbe2;
    border-radius: 3px;
    font-size: 1.14286rem;
    line-height: 1.71429rem;
    font-weight: 400;
    text-transform: initial;
    letter-spacing: initial;
    box-sizing: border-box;
    display: block;
    width: 100%;
    margin-right: 0.5px;

  }
</style>  
  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="#">Product</a>
          </li>
          <li class="active">Edit Product</li>
        </ul><!-- /.breadcrumb -->

      </div>
      <br>
      <div class="row">

        
            {{ Form::model($product, ['url' => 'admin/product/edit/'.$product->id, 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}
                        <div class="col-md-12">

                        <div class="col-xs-12" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;">
                        <div class="form-group">
            <label class="col-sm-12"> <b>Organization  </b> <br></label>
            <br><br>
            </div>

             <div class="form-group col-sm-8">
              <label class="col-sm-12" for="form-field-1"> Title </label>

              <div class="col-sm-12">
                <!-- <input type="text" name="product_title" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-12" /> -->
                 {{ Form::input('text', 'product_title',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
                  @if ($errors->has('title'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('title') }}</strong>
                                  </span>
                              @endif
              </div>
            </div>
              <div class="form-group col-sm-4">
            <div class="col-sm-12">
              <label for="form-field-1"> Category Name </label>
                {{  Form::select('product_category[]', $product_categories,null,['class'=>'form-control', 'multiple']) }}
                  @if ($errors->has('product_category'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('product_category') }}</strong>
                                  </span>
                              @endif

                  @if ($errors->has('product_category'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('product_category') }}</strong>
                                  </span>
                              @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-12" for="form-field-1"> Short Description </label>

              <div class="col-sm-12">
                   {{ Form::textarea('product_description',null, ['id' => 'form-field-1', 'class' => 'my-editor']) }}
              </div>
            </div>

            <div class="hr hr32 hr-dotted col-md-12"></div>
              
                    <div class="form-group col-md-4">
                <label class="" for="form-field-1"> Video Link </label>

                <div class="col-sm-12">
                  {{ Form::input('text', 'video_link',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
                  
                </div>
              </div>

              <div class="col-md-4">  
          
            
            <div class="form-group">
              <div class="col-sm-12" class="form-control">
              <label for="form-field-1"> Tags </label>
                {{  Form::select('product_tag[]', $product_tags,null,['class'=>'form-control col-md-12','id' => 'product-tag', 'multiple' => 'multiple']) }}
              </div>
            </div>
             <?php 
              $tags = DB::table('product_tag')
                          ->join('tags', 'product_tag.tag_id', '=', 'tags.id')
                          ->where('product_id', '=', $product->id)
                          ->select('tags.name', 'tags.id')
                          ->get();
            ?>
            <div class="form-group">
              <!-- <label class="col-sm-12" for="form-field-1"> Tags </label> -->

              <div class="col-sm-12">

                  <ul>
                    @foreach($tags as $tag)
                      <li class="product-tag">
                        {{$tag->name}} <span class="delete_tag"  style="cursor:pointer" data-tagid="{{$tag->id}}" onClick="deleteTag(this)">X</span>
                      </li>
                    @endforeach
                  </ul>
              </div>
            </div>
            </div>
            <div class="form-group col-md-4">
            <label class="col-sm-12"> <b>Visibility  </b> </label>
              </div>
              <div class="col-md-4">
              <div class="form-group">
                <!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->
            <?php if(!$product->status == '1')
              {?>
                 <input name="online_status" type="checkbox"> Online  
             <?php }
             else
              { ?>

             <input name="online_status" type="checkbox" checked="checked"> Online  
             <?php }
              ?>
              
              <br>
              </div>
              </div>
              <div class="col-md-4">
              <div class="form-group">

                <!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->
            <?php if(!$product->personalised == '1')
              {?>
                 <input name="personalised" type="checkbox"> Personalised 
             <?php }
             else
              { ?>

             <input name="personalised" type="checkbox" checked="checked"> Personalised
             <?php }
              ?>
              
              <br>
              </div>
              </div>

              <div class="hr hr32 hr-dotted col-md-12"></div>
                <div class="form-group col-sm-8">
                <label class="col-sm-12" style="height: 42px !important;" for="form-field-1">Images </label>

                <div class="">
                 <input id="product_images" name="product_images[]" type="file" multiple class="file-loading">
                </div>
              </div>
              <br>
              <br>
              <div class="col-md-4">
                    <div class="form-group col-md-12">
                      <div class="col-sm-12">
                        <label for="form-field-1"> Featured Image 
                        </label>
                        <input type="file" name="featured_images" id="featured_image" class="col-xs-10 col-sm-12" />
                      </div>
                      </div>
                      <div class="col-md-12">
                       
                        <div id='output_image'>
                        </div>
                        <img class="featured-image" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" style="width: 100%">
                      </div>
                    </div>

            </div>
              </div>
                    
            <div class="col-md-12" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;margin-top: 3%;">
            <div class="form-group">
              
            <label class="col-sm-12"> <b>Pricing</b> </label>
            <br><br>
              <div class="col-sm-4">
                  <label class="" for="form-field-1"> Price </label>
                  <br>
                  {{ Form::input('text', 'price',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
              </div>
              <div class="col-sm-4">
                <label class="" for="form-field-1"> Compare at Price </label><br>
                  {{ Form::input('text', 'compare_price',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
              </div>
                <div class="form-group col-sm-4">
              <label class="col-sm-12" for="form-field-1"> Quantity </label>

              <div class="col-sm-12">
                  {{ Form::input('text', 'quantity',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
               
                <!-- <select name="tax" class="col-md-12">
                  <option value="">Select Tax</option>
                  <option value="12.5">12.5%</option>
                  <option value="5">5%</option>
                  
                </select> -->
              </div>
            </div>
            </div>
            <div class="form-group">
          <!--     <div class="checkbox">
                <label>
                  {{ Form::checkbox('is_tax', 0, null, ['class' => 'ace ace-checkbox-2']) }}
                  <span class="lbl"> Charge taxes on this product</span>
                </label>
              </div>
            </div>   -->
            <div class="hr hr32 hr-dotted"></div>
            <div class="form-group">
            <label class="col-sm-12"> <b>Inventory  </b> </label>
            <br><br>
              <div class="col-sm-4">

              <label for="form-field-1"> SKU(Stock Keeping Unit) </label>
                {{ Form::input('text', 'i_sku',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
              </div>
            
              <div class="col-sm-4">
              <label for="form-field-1"> Barcode (ISBN, UPC, GTIN, etc.) </label>
                {{ Form::input('text', 'i_barcode',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
              </div>
               <div class="form-group col-sm-4">
              <label class="col-sm-12" for="form-field-1"> Tax </label>

              <div class="col-sm-12">
               {{  Form::select('tax',['' => 'Select Tax','0' => '0%','12.5' => '12.5%','5' => '5%','5.25' => '5.25%', '13.125' => '13.125%'], $product->tax,['class'=>'form-control']) }}
                <!-- <select name="tax" class="col-md-12">
                  <option value="">Select Tax</option>
                  <option value="12.5">12.5%</option>
                  <option value="5">5%</option>
                  
                </select> -->
              </div>
            </div>
            </div>
            <div class="hr hr32 hr-dotted"></div>
            <label class="col-md-12"> <b>Shipping  </b> </label>
            <div class="form-group col-md-4">
           
              <label class="col-sm-12" for="form-field-1"> Weight </label>

              <div class="col-sm-12">
                 {{ Form::input('text', 'weight',null, ['id' => 'form-field-1', 'class' => 'col-xs-11 col-sm-11']) }}
                {{  Form::select('unit', array(
                'lb' => 'lb', 
                'oz' => 'oz', 
                'kg' => 'kg',
                'g' => 'g',
                )
                ,null,['style' => 'height:34px']) }}
              </div>
            </div>
           
          

            <div class="form-group col-md-4">
              <div class="col-sm-12">
              <label for="form-field-1"> Page Title </label>
                 {{ Form::input('text', 'product_page_title',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
              </div>
            </div>
            <div class="form-group col-md-4">
              <div class="col-sm-12">
              <label for="form-field-1"> Page URL </label>
              <div class="next-input--stylized"><span class="next-input__add-on next-input__add-on--before" style="float: left;margin-top: 4px;font-size: 11px;">https://www.babyjalebi.com/products/</span>
              {{ Form::input('text', 'url',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12 next-input next-input--invisible', 'style' => 'width: 71%; border: none;']) }}
              </div>
              </div>
                
             
            </div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1"> Meta Description </label>
                {{ Form::textarea('meta_description',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div>
            <div class="hr hr32 hr-dotted col-md-12"></div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1"> Overview </label>
                {{ Form::textarea('overview',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div>
            <div class="hr hr32 hr-dotted col-md-12"></div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1"> Details that matter </label>
                {{ Form::textarea('details_that_matter',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div>
            <!-- <div class="hr hr32 hr-dotted col-md-12"></div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1"> Safety information </label>
                {{ Form::textarea('safety_information',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div> -->
            <div class="hr hr32 hr-dotted col-md-12"></div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1"> Dimensions </label>
                {{ Form::textarea('dimensions',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div>
            <div class="hr hr32 hr-dotted col-md-12"></div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1"> Care </label>
                {{ Form::textarea('care',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div>
            <!-- <div class="hr hr32 hr-dotted col-md-12"></div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1">Our commitment </label>
                {{ Form::textarea('our_commitment',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div>
            <div class="hr hr32 hr-dotted col-md-12"></div>
            <div class="form-group">
              <div class="col-sm-12">
              <label for="form-field-1"> IN House design </label>
                {{ Form::textarea('in_house_design',null, ['id' => 'form-field-1', 'rows' => '4', 'cols' => '65', 'class' => 'col-xs-10 col-sm-12 my-editor']) }}
              </div>
            </div> -->
            <div id='variation1' style="display: none">
							<div class="col-md-12" >
							 <div class="pi1" id='vari1' >
							 <div class="price_inventory" id="set1">

							 <?php
							  $attribute_values = DB::table('attribute_value')
                                                            ->where('attribute_id', $productattribute[0]->id)->get();
							 ?>
                            <div class="form-group ">
                                <label class="control-label">{{$productattribute[0]->name}}</label>
                                <div class="">
                                    <select name="attributename0[]" class="form-control col-md-7 col-xs-12">
                                    <option value="">Select</option>
                                        @foreach($attribute_values as $value)
                                        	<option value="{{$value->id}}">{{$value->value}}</option>                                    
                                   		@endforeach                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Images</label>
                                <div class="">

								<input type="file" style="height: 42px !important;" name="product_images_var[]" id="image_file" class="attr_image_file" multiple>
								</div>
								<div id="dvPreviews">
								</div>
                            </div>
                            
                            <button class="btn btn-danger remove" onclick="delDupe1()">DEL -</button>
                            </div>
                        </div>
                            <button type="button" class="btn btn-success add" onclick="duplicate1()">ADD +</button>
                            </div>
                            </div>
                            <!-- end color section -->
                            <div id='variation' style="display: none">
							<div class="col-md-12" >
							 <div class="pi" id='vari' >
							 <div class="price_inventory" id="set">
							 <?php
							  $attribute_values = DB::table('attribute_value')
                                                            ->where('attribute_id', $productattribute[1]->id)->get();
							 ?>
                            <div class="form-group ">
                                <label class="control-label">{{$productattribute[1]->name}}</label>
                                <div class="">
                                    <select name="attributename0[]" class="form-control col-md-7 col-xs-12">
                                    <option value="">Select</option>
                                        @foreach($attribute_values as $value)
                                        	<option value="{{$value->id}}">{{$value->value}}</option>                                    
                                   		@endforeach                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Images</label>
                                <div class="">

								<input type="file" style="height: 42px !important;" name="product_images_var[]" id="image_file" class="attr_image_file" multiple>
								</div>
								<div id="dvPreviews">
								</div>
                            </div>
                            
                            <button class="btn btn-danger remove" onclick="delDupe()">DEL -</button>
                            </div>
                        </div>
                            <button type="button" class="btn btn-success add" onclick="duplicate()">ADD +</button>
                            </div>
                            </div>
                             <div class="form-group col-md-12">
							<input type="button" name="answer" id="answer" value="Add Pattern" class="btn btn-success" onclick="showDiv()" />
							</div>
							<div class="form-group col-md-12">
							<input type="button" name="answer" id="answer1" value="Add Color" class="btn btn-success" onclick="showDiv1()" />
							</div>
            <div class="hr hr32 hr-dotted col-md-12"></div>

@foreach($productvariation_count as $cnt)
<div class="row">
	<div class="col-md-12">
	
		<h3>{{$cnt->value}}</h3>
		</div>

		@foreach($productvariation as $variationimg)


				@if($cnt->id == $variationimg->id)
						<div class="col-md-2">
							<a href="#" class="deletevar" id="{{$variationimg->varid}}"><i class="fa fa-close"></i></a>
			            	<img class="thumbnail img-responsive" src="{{URL::to('/product_images/'.$variationimg->pid.'/'.$variationimg->id.'/'.$variationimg->images)}}">
			           

			            </div>
				@endif
         @endforeach
	<div class="hr hr32 hr-dotted col-md-12"></div>
</div>
            
             
             
           

          @endforeach 

            
           
<div class="form-group">
            <label class="col-sm-12" for="form-field-1"> RELATED  PRODUCTS
            </label>
            <div class="col-sm-12 product-list">
              {{Form::select('product[]', $products,null, array('id' => 'product_name','class' => 'col-xs-10 col-sm-12', 'multiple'))}}
            </div>
          </div>
          <div class="row">
<div class="table-responsive col-md-12">
<table class="table table-striped table-bordered">
  <tr>
    <th>Image</th>
    <th>Product Title</th>
    <th>Price</th>
  </tr>
          @foreach($relatedp as $newproduct)

                            <?php 

                            $productrelated = DB::table('product')->where('id' , '=' ,$newproduct->related_pid)->get();
                             // $productrelated = DB::table('product')
                             //                ->join('product_categories','product_categories.product_id','=','product.id', 'left')
                             //                ->where('product.id' , '=' ,$newproduct->related_pid)
                             //                ->get(); 

                           
                            ?>

                            @foreach($productrelated as $relatedp)
                                        <?php 
                                         // dd($productrelated);
                            $reltdcat = DB::table('product_categories')
                                                ->join('product_category','product_category.id','=','product_categories.category_id','left')
                                                ->select('product_category.*')
                                                ->where('product_id' , '=' ,$relatedp->id)->first();
                            // dd($reltdcat);

?>


  <tr>
    
    @if($relatedp->product_images!='')
          <td class="next-table__image-cell hide-when-printing" >
            <img title="{{$relatedp->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$relatedp->id.'/featured_images/'.$relatedp->product_images)}}" alt="{{$relatedp->product_title}}" style="width: 50px;">
          </td>
        @else
          <td class="next-table__image-cell hide-when-printing">
            <img title="No Image" class="block aspect-ratio__content" src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="No Image" style="width: 50px;">
           </td>
        @endif


    <td>{{$relatedp->product_title}}</td>
    <td>{{$relatedp->price}}</td>
    <td class="deleterelatedproduct" id="{{$relatedp->id}}"> <button> Delete </button></td>
  </tr>


@endforeach
             @endforeach
             </table>
</div>
</div>
           
          <div class="product_list col-md-8">
          </div>
              <div class="form-group">
                <div class="col-md-offset-10 col-md-2">
                  <button class="btn btn-info" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Submit
                  </button>
                </div>
              </div>
                          </div>
              
           
          <br>


          {{ Form::close() }}

      

        
    
    </div>
  </div>
@endsection

@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<?php
  use App\ProductImages;
  $product_image = ProductImages::where('product_id', '=', $product->id)->select('id', 'images')->get();
  // dd($product_image);
  // /product_images/'.$product->id.'/'.$product_image->images
?>

<script>
  $("#product_images").fileinput({
      initialPreview: [
            <?php foreach ($product_image as $image) { ?>
              '{{URL::to("/product_images/".$product->id.'/'.$image->images)}}',
            <?php } ?>

        ],
      initialPreviewAsData: true,
      initialPreviewConfig: [
        <?php foreach ($product_image as $image) { ?>
          {caption: "{{$product->product_title}}", width: "150px", key: "{{$image->id}}"},
         <?php } ?>
      ],
      deleteUrl: "/admin/product/delete",
      overwriteInitial: false,
      uploadAsync: true,
      maxFileCount: 5,
      // showUpload: false
  });

  function deleteTag(obj){
    var base_url = "{{URL::to('/')}}";
    var tag_id = $(obj).attr('data-tagid');

    $.ajax({
          url: base_url+'/delete-tag',
          type: "post",
          data: 
          {
                "tag_id": tag_id,
            },
          success:function(data)
          {
            if (data.status==1) {
              // alert(data.message);
            toastr.success(data.message, {timeOut: 100000});

              $(this).closest().remove();
            }
            if (data.status==0) {
              alert(data.message);
            }
          }
      });
  }
</script>
  <script>
    function showDiv() {
   document.getElementById('variation').style.display = "block";
   document.getElementById('answer').style.display = "none";
}

  </script>
<script>
      var i = 0;
var original = document.getElementById('set');
var form1 = document.getElementById('vari');
var btn1 = form1.querySelector('button');

function duplicate() {
  i++;
  var clone1 = original.cloneNode(true);

  clone1.id = "clone" + i;
  form1.appendChild(clone1);

  btn1.id = "btn" + i;
   var j=0;
   $('input[name="product_images_var[]"').each(function(){
      $(this).attr('name', 'product_images_var[]' + j + '[]'); 
      $(this).attr('id', 'product_images_var' + j + '[]'); 
     j++;
  });
}

function delDupe() {

  var clone1 = document.getElementById('clone' + i);

  if (!clone1) {
    return false;
  }

  form1.removeChild(clone1);

  i--;
}

</script>
<!-- for delete images -->
<!-- <script>
   $('.vardelete').click(function(e){
    e.preventDefault();
      var varid = $(this).attr('id');
      // alert(varid);
var base_url = '{{URL::to('/')}}';
      $.ajax({
        url: base_url+'/admin/ajax/delete-var-img',
        type: "post",
        data: 
        {
          "varid": varid
        },
        success:function(data)
        {

          if (data.status == 1) 
          {
            toastr.success(data.message, {timeOut: 100000})
          $("."+varid).fadeOut("slow");
          // $('#1').hide('fade');

          }
        }
      });
    });
</script> -->
<script>
  var base_url = "{{URL::to('/')}}";
  $("#product_name").chosen({
    no_results_text: "Oops, nothing found!"}
                           ).change(function(e, params){
    var product_id = $("#product_name").chosen().val();
    $.ajax({
      url: base_url+'/admin/add-products-to-category',
      type: "post",
      data: 
      {
        "id": product_id,
      },
      success:function(data)
      {
        $('.product_list').html(data);
      }
    });
  });

  $('#store_id').change(function(){
    var store_id = $('#store_id').val();

    $.ajax({
      
        url: base_url+'/all-products-by-stores-category',
        type: "post",
        data: 
        {
          "store_id": store_id,
        },
        success:function(data)
        {
          $('.product-label').css('display', 'block');
          $('.product-list').html(data);
        }
    });
  });
</script>
<!-- delete variation -->
<script>
  $('.deletevar').click(function(e){
    e.preventDefault();
    var url = "{{URL::to('/delete-variation')}}";
    if (confirm('Are you sure! you want to delete this variation')) {
    // confirm("Are you sure! you want to delete this variation");
    $(this).parent().addClass("removing_product");
    var varid = $(this).attr('id');
  	// alert(varid);
    $.ajax({
       type: "post",
       url: url,
       data: {
       	'varid':varid
       },
       success: function(data)
       {
        
       	$('.removing_product').remove();
        // window.location.reload();
        
       }
    });
}
  });
  
</script>
<script>
  $('.deleterelatedproduct').click(function(e){
    e.preventDefault();
    var url = "{{URL::to('/delete-related-product')}}";
    if (confirm('Are you sure! you want to delete this variation')) {
    // confirm("Are you sure! you want to delete this variation");
    $(this).parent().addClass("removing_product");
    var element = $(this);
    var varid = $(this).attr('id');
    // alert(varid);
    $.ajax({
       type: "post",
       url: url,
       data: {
        'varid':varid
       },
       success: function(data)
       {
        
        $('.removing_product').remove();
        // window.location.reload();
        element.remove();
        
       }
    });
}
  });
  
</script>
<script>
		function showDiv() {
   document.getElementById('variation').style.display = "block";
   // $("#" + id).attr('id', 'sel' + rowIndex);
   document.getElementById('answer').style.display = "none";
   document.getElementById('answer1').style.display = "none";
}

	</script>
	<script>
		function showDiv1() {
   document.getElementById('variation1').style.display = "block";
   // alert('ffdfd')
   // $("#" + id).attr('id', 'sel' + rowIndex);
   document.getElementById('answer').style.display = "none";
   document.getElementById('answer1').style.display = "none";
}

	</script>
<script>



var i = 0;
var origina2 = document.getElementById('set');
var form2 = document.getElementById('vari');
var btn2 = form2.querySelector('button');

var attrvalue;
var base_url = "{{url('/')}}"

$('select').change(function(){
	attrvalue = $(this).val();
	// alert(attrvalue);
})

$('.attr_image_file').on('change', function(e){
	e.preventDefault();
		var data = new FormData();
		jQuery.each(jQuery(this)[0].files, function(i, file) {
		data.append('file-'+i, file);
		data.append('attrid', attrvalue);
	});

	$.ajax({
      	url: base_url+'/admin/add-variation-image',
     	type: "post",
     	cache: false,
     	contentType: false,
     	processData: false,
      	data: data,
      	success:function(data)
      	{
	      	console.log('done');
      		
      	}
  	});

});

function duplicate() {
  	i++;
  	var clone2 = origina2.cloneNode(true);

  	clone2.id = "clone" + i;
  	form2.appendChild(clone2);
  	
	$('select').change(function(){
    	attrvalue = $(this).val();
    })

  	$('.attr_image_file').on('change', function(e){
		e.preventDefault();
  		var data = new FormData();
			jQuery.each(jQuery(this)[0].files, function(i, file) {
    		data.append('file-'+i, file);
    		data.append('attrid', attrvalue);
		});

		$.ajax({
          	url: base_url+'/admin/add-variation-image',
         	type: "post",
         	cache: false,
         	contentType: false,
         	processData: false,
          	data: data,
          	success:function(data)
          	{
	      		console.log('done');
          	}
      	});

	});

}

function delDupe() {

  var clone1 = document.getElementById('clone' + i);

  if (!clone1) {
    return false;
  }

  form1.removeChild(clone1);

  i--;
}
</script>
<!-- for color -->
<script>



var i = 0;
var original = document.getElementById('set1');
var form1 = document.getElementById('vari1');
var btn1 = form1.querySelector('button');

var attrvalue;
var base_url = "{{url('/')}}"

$('select').change(function(){
	attrvalue = $(this).val();
})

$('.attr_image_file').on('change', function(e){
	e.preventDefault();
		var data = new FormData();
		jQuery.each(jQuery(this)[0].files, function(i, file) {
		data.append('file-'+i, file);
		data.append('attrid', attrvalue);
	});

	$.ajax({
      	url: base_url+'/admin/add-variation-image',
     	type: "post",
     	cache: false,
     	contentType: false,
     	processData: false,
      	data: data,
      	success:function(data)
      	{
	      	console.log('done');
      		
      	}
  	});

});

function duplicate1() {
  	i++;
  	var clone1 = original.cloneNode(true);

  	clone1.id = "clone" + i;
  	form1.appendChild(clone1);
  	
	$('select').change(function(){
    	attrvalue = $(this).val();
    })

  	$('.attr_image_file').on('change', function(e){
		e.preventDefault();
  		var data = new FormData();
			jQuery.each(jQuery(this)[0].files, function(i, file) {
    		data.append('file-'+i, file);
    		data.append('attrid', attrvalue);
		});

		$.ajax({
          	url: base_url+'/admin/add-variation-image',
         	type: "post",
         	cache: false,
         	contentType: false,
         	processData: false,
          	data: data,
          	success:function(data)
          	{
	      		console.log('done');
          	}
      	});

	});

}

function delDupe1() {

  var clone1 = document.getElementById('clone' + i);

  if (!clone1) {
    return false;
  }

  form1.removeChild(clone1);

  i--;
}
</script>
@endsection