@extends('admin.admin_layout')

@section('content')
 <?php
        use App\ProductImages;
        use App\Vendor;
        ?>
        <style type="text/css">
            .dataTables_filter
          {
           /* display: none;*/
          }
          .updatebtn{
            width: 56%;
    padding: 0px;
    padding-top: 3.5px;
    padding-bottom: 3.5px;
  /*  border-color: #d8920c;
    background: #d8920c !important;
          }*/
        </style>
    <div class="row">
        <div class="col-xs-12">

            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Inventory List</h3>
                   <div style="float: right; margin-top: -14px; ">
                            <button id="refresh" class="btn btn-default">Refresh</button>
                            <a href="{{URL::to('/admin/product/new')}}"><button id="refresh" class="btn btn-default">Create Product</button></a>
                            <a href="{{URL::to('/inventorylist')}}"><button id="export1" class="btn btn-default exlport"> Export Inventory List</button></a>
                             <br><br>
                    </div>
                    <!-- div.dataTables_borderWrap -->
                    
                    <div class="inventory-data">
                        <table id="inventory-dynamic-table" class="col-md-12 table table-striped table-bordered table-hover" data-page-length='50'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>SKU</th>
                                    <th>Product Title</th>
                                    <th>Collection</th>
                                   

                                    <th>Price</th>

                                    <th>
                                        Quantity
                                    </th>
                                    <th class="hidden-480">Status</th>

                                    
                                </tr>
                                <tr>
                                    <th class="nosort">
                                        <input type="checkbox" class="nosearch">
                                    </th>
                                    <th class="input-filter">SKU</th>
                                    <th class="input-filter">Product Title</th>
                                    <th class="input-filter">Product Category</th>
                                    
                                   
                                   
                                   
                                    

                                    <th>
                                        
                                    </th>
                                    <th class="hidden-480 input-filter nosort">Status</th>
                                   
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody class="product-inventory">
                            <?php $i = 1; ?>

                            @foreach($products as $product)

                            
                                <tr id="{{$product->id}}">
                                    <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace product-id" value="{{$product->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>
                                    <td class="hidden-480">{{$product->i_sku}}</td>
                                    
                                    <td>
                                        <a href="{{URL::to('/admin/product/edit/'.$product->id)}}">{{$product->product_title}}</a>
                                    </td>
                                    <td>{{$product->category_name}}</td>
                                    

                                                                      
                                    <td>Rs.{{$product->price}}</td>
                                    
                                    <td class="pqty">{{$product->quantity}}</td>

                                    <td class="hidden-480">
                                        <span class="label label-sm label-inverse arrowed-in">Active</span>
                                    </td>
    
                                    
                                        <td>
                                            <input type="number" min="1" value="" class="product_quantity<?php echo $i; ?>" style="width: 36%;height: 44px;padding-top: 1px;">
                                            <button id="refresh" data-id="{{$product->id}}" data-quantity="{{$i}}" onClick="updateQuantity(<?php echo $product->id; ?>, this);" class="updatebtn btn btn-default get_quantity">Update</button>
                                        </td>
                                </tr>
                                      <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.col -->
    </div><!-- /.row -->
                    
@endsection

@section('scripts')
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script type="text/javascript">



        $(document).ready(function() {
            
            $('.input-filter').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search .." class="fullwitdh" />' );
                // $(this).html( '<input type="text" placeholder="Search '+title+'" class="fullwitdh" />' );
            } );

            var myTable = $('#inventory-dynamic-table').DataTable({
                paging: true,
                bSortable: true,
                bSearchable: true,
                select: {
                    style: 'multi'
                },
                'aoColumnDefs': [
                {
                    'bSortable': false,
                    'aTargets': ['nosort']
                },
                {
                    'bSearchable' : false,
                    'aTargets': ['nosearch']
                }
                ]
            });


            // Apply the search
            myTable.columns().every( function () {
                var that = this;
                
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


                serachTextBox.on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachSelectBox.on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


            } );
            
            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });
        });

    </script>

    <script>



    </script>

    <script type="text/javascript">
    var base_url = '{{URL::to("/")}}';

    function updateQuantity(id, obj) {
        var $row = $(this).closest("tr");

        var qid = $(obj).attr('data-quantity');
        var quantity = $(".product_quantity"+qid).val();
        <?php 
            if(isset($stores)){
                $store = $stores->id;
            }else{
                $store = '';
            }
        ?>
        var store = "{{$store}}";
          $.ajax({
            url: base_url+'/admin/quantity/update',
            type: "post",
            data: 'id='+id+'&quantity='+quantity+'&store='+store,
            success: function(data, status){
                if (data.status == 0) {
                    // alert("uodate");
                    toastr.success(data.message, {timeOut: 100000})
                    location.reload();
                }
                if (data.status == 1) {
                    toastr.success(data.message, {timeOut: 100000})
                    location.reload();
                    // alert("uodate");
                }
            }
        });
    }

    </script>
@endsection