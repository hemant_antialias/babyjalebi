@extends('admin.admin_layout')
@section('content')
<style>
 .scroll
    {
    margin-bottom: 2%;
    height: 250px !important;
    overflow: scroll;
}
  .category-image {
    height: 123px;
    width: 111px;
    float: right;
  }
  input[type=checkbox], input[type=radio] {
    line-height: normal;
    vertical-align: text-top;
    width: 25px;
    height: 17px;
    padding: 0;
    margin: 0;
    position: relative;
    overflow: hidden;
    top: 1px;
    float: right;
    margin-right: 20px;
  }
  
</style>
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon">
          </i>
          <a href="{{URL::to('/admin/categorylist')}}">Category
          </a>
        </li>
        <li class="active">Add Category
        </li>
      </ul>
      <!-- /.breadcrumb -->
    </div>
    <br>
    <div class="row">
      <div class="col-xs-12">
        {{ Form::open(['url' => 'admin/product/category/new', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) }}
        <div class="col-md-8 ">
        <div class="col-md-12 block-section">
 
       
          <div class="form-group" style="padding: 2%;">
            <div>
              <label for="form-field-1"> Category Name 
              </label>
              <input type="text" name="name" id="form-field-1" placeholder="e.g. Name" class="col-xs-10 col-sm-12" />
              @if ($errors->has('cat_name'))
              <span class="help-block">
                <strong>{{ $errors->first('cat_name') }}
                </strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <label for="form-field-1"> Description 
              </label>
              <!-- <textarea name="description" cols="55" rows="6" class="col-xs-10 col-sm-12"></textarea> -->
              <textarea name="description" rows="6" cols="55" class="col-xs-10 col-sm-12 my-editor"></textarea>
            </div>
          </div>
<hr>
       
          <div class="product_list">
          </div>
        <!--   <div class="form-group">
              <label class="col-sm-12" style="height: 42px !important;" for="form-field-1">Images </label>

              <div class="col-sm-12">
                <input id="category_slider" style="height: 42px !important;" name="category_slider[]" type="file" multiple data-show-upload="false" class="file-loading">
              </div>
            </div> -->
        </div>
        </div>
        <div class="col-md-4 block-background" style="margin-bottom: 2%;">	
          <div class="form-group">
            <label class="col-sm-12"> 
            <b>Category Type</b><br><br>
            <!-- <input name="special" value="" type="checkbox" > Special -->
            <input name="is_parent" value="1" type="checkbox" > Is Parent
            <br>
            <br>

              <b>Visibility  
              </b> 
            </label>
           <!--  <div class="col-sm-12">
              <div class="input-group">
                <br>
                <input name="facebook_status" type="checkbox" checked="checked"> Facebook 
                <br>
              </div>
            </div> -->
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->
              <input name="online_status" type="checkbox"> Online store 
              <br>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->
              <input name="homepage" type="checkbox"> On Home Page
              <br>
            </div>
          </div>
        </div>
        <div class="col-md-4 block-background scroll" style="margin-bottom: 2%;">
          <div class="form-group">
            <div class="col-sm-12">
              <h4>Category Parent
              </h4><br>

              @foreach($collection_types as $collection_type)
              @if($collection_type->parent_id == NULL)
                {{$collection_type->name}} <input type="checkbox" name="collection_type[]" value="{{$collection_type->id}}">
                <hr>
                @endif
              @endforeach
            </div>
          </div>
        </div>
        <br><br>
        
        <div class="col-md-4 block-background">
         
          <div class="form-group col-md-8">
            <div class="col-sm-8">
              <label for="form-field-1"> Category Image 
              </label>
              <input type="file" name="category_image" id="category_image" class="col-xs-10 col-sm-12" />
            </div>
            </div>
            <div class="col-md-4">
             
              <div id='output_image'>
              </div>
              <img class="category-image" src="{{URL::to('/web-assets/images/default_product.jpg')}}">
            </div>
          </div>
        </div>
        <div class="hr hr32 hr-dotted">
        </div>
        <div class="form-group">
          <div class="col-md-offset-9 col-md-3">
            <br>
            <br>
            <button class="btn btn-info" type="submit">
              <i class="ace-icon fa fa-check bigger-110">
              </i>
              Submit
            </button>
            &nbsp; &nbsp; &nbsp;
            <button class="btn" type="reset">
              <i class="ace-icon fa fa-undo bigger-110">
              </i>
              Reset
            </button>
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<script>
  var base_url = "{{URL::to('/')}}";
  $("#product_name").chosen({
    no_results_text: "Oops, nothing found!"}
                           ).change(function(e, params){
    var product_id = $("#product_name").chosen().val();
    $.ajax({
      url: base_url+'/admin/add-products-to-category',
      type: "post",
      data: 
      {
        "id": product_id,
      },
      success:function(data)
      {
        $('.product_list').html(data);
      }
    });
  });

  $('#store_id').change(function(){
    var store_id = $('#store_id').val();

    $.ajax({
        url: base_url+'/all-products-by-stores-category',
        type: "post",
        data: 
        {
          "store_id": store_id,
        },
        success:function(data)
        {
          $('.product-label').css('display', 'block');
          $('.product-list').html(data);
        }
    });
  });

</script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<script>
  $("#category_slider").fileinput({
      // uploadUrl: "http://localhost/file-upload-single/1", // server upload action
      uploadAsync: true,
      showRemove: true,
      maxFileCount: 5,
      showUpload: false
  });
</script>
<!-- inline scripts related to this page -->
    <script type="text/javascript">
      jQuery(function($){
  
  $('textarea[data-provide="markdown"]').each(function(){
        var $this = $(this);

    if ($this.data('markdown')) {
      $this.data('markdown').showEditor();
    }
    else $this.markdown()
    
    $this.parent().find('.btn').addClass('btn-white');
    })
  
  
  
  function showErrorAlert (reason, detail) {
    var msg='';
    if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
    else {
      //console.log("error uploading file", reason, detail);
    }
    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
     '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
  }

  //$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

  //but we want to change a few buttons colors for the third style
  $('#editor1').ace_wysiwyg({
    toolbar:
    [
      'font',
      null,
      'fontSize',
      null,
      {name:'bold', className:'btn-info'},
      {name:'italic', className:'btn-info'},
      {name:'strikethrough', className:'btn-info'},
      {name:'underline', className:'btn-info'},
      null,
      {name:'insertunorderedlist', className:'btn-success'},
      {name:'insertorderedlist', className:'btn-success'},
      {name:'outdent', className:'btn-purple'},
      {name:'indent', className:'btn-purple'},
      null,
      {name:'justifyleft', className:'btn-primary'},
      {name:'justifycenter', className:'btn-primary'},
      {name:'justifyright', className:'btn-primary'},
      {name:'justifyfull', className:'btn-inverse'},
      null,
      {name:'createLink', className:'btn-pink'},
      {name:'unlink', className:'btn-pink'},
      null,
      {name:'insertImage', className:'btn-success'},
      null,
      'foreColor',
      null,
      {name:'undo', className:'btn-grey'},
      {name:'redo', className:'btn-grey'}
    ],
    'wysiwyg': {
      fileUploadError: showErrorAlert
    }
  }).prev().addClass('wysiwyg-style2');

  
  /**
  //make the editor have all the available height
  $(window).on('resize.editor', function() {
    var offset = $('#editor1').parent().offset();
    var winHeight =  $(this).height();
    
    $('#editor1').css({'height':winHeight - offset.top - 10, 'max-height': 'none'});
  }).triggerHandler('resize.editor');
  */
  

  $('#editor2').css({'height':'200px'}).ace_wysiwyg({
    toolbar_place: function(toolbar) {
      return $(this).closest('.widget-box')
             .find('.widget-header').prepend(toolbar)
           .find('.wysiwyg-toolbar').addClass('inline');
    },
    toolbar:
    [
      'bold',
      {name:'italic' , title:'Change Title!', icon: 'ace-icon fa fa-leaf'},
      'strikethrough',
      null,
      'insertunorderedlist',
      'insertorderedlist',
      null,
      'justifyleft',
      'justifycenter',
      'justifyright'
    ],
    speech_button: false
  });
  
  


  $('[data-toggle="buttons"] .btn').on('click', function(e){
    var target = $(this).find('input[type=radio]');
    var which = parseInt(target.val());
    var toolbar = $('#editor1').prev().get(0);
    if(which >= 1 && which <= 4) {
      toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
      if(which == 1) $(toolbar).addClass('wysiwyg-style1');
      else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
      if(which == 4) {
        $(toolbar).find('.btn-group > .btn').addClass('btn-white btn-round');
      } else $(toolbar).find('.btn-group > .btn-white').removeClass('btn-white btn-round');
    }
  });


  

  //RESIZE IMAGE
  
  //Add Image Resize Functionality to Chrome and Safari
  //webkit browsers don't have image resize functionality when content is editable
  //so let's add something using jQuery UI resizable
  //another option would be opening a dialog for user to enter dimensions.
  if ( typeof jQuery.ui !== 'undefined' && ace.vars['webkit'] ) {
    
    var lastResizableImg = null;
    function destroyResizable() {
      if(lastResizableImg == null) return;
      lastResizableImg.resizable( "destroy" );
      lastResizableImg.removeData('resizable');
      lastResizableImg = null;
    }

    var enableImageResize = function() {
      $('.wysiwyg-editor')
      .on('mousedown', function(e) {
        var target = $(e.target);
        if( e.target instanceof HTMLImageElement ) {
          if( !target.data('resizable') ) {
            target.resizable({
              aspectRatio: e.target.width / e.target.height,
            });
            target.data('resizable', true);
            
            if( lastResizableImg != null ) {
              //disable previous resizable image
              lastResizableImg.resizable( "destroy" );
              lastResizableImg.removeData('resizable');
            }
            lastResizableImg = target;
          }
        }
      })
      .on('click', function(e) {
        if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
          destroyResizable();
        }
      })
      .on('keydown', function() {
        destroyResizable();
      });
      }

    enableImageResize();

    /**
    //or we can load the jQuery UI dynamically only if needed
    if (typeof jQuery.ui !== 'undefined') enableImageResize();
    else {//load jQuery UI if not loaded
      //in Ace demo ./components will be replaced by correct components path
      $.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
        enableImageResize()
      });
    }
    */
  }


});
</script>
@endsection