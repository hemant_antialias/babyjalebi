@extends('admin.admin_layout')
@section('content')
<style>
 .scroll
    {
    margin-bottom: 2%;
    height: 250px !important;
    overflow: scroll;
}
  .category-image {
    height: 123px;
    width: 111px;
  }
  input[type=checkbox], input[type=radio] {
    line-height: normal;
    vertical-align: text-top;
    width: 25px;
    height: 17px;
    padding: 0;
    margin: 0;
    position: relative;
    overflow: hidden;
    top: 1px;
    float: right;
    margin-right: 20px;
}
</style>
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon">
          </i>
          <a href="{{URL::to('/admin/categorylist')}}">Product Category
          </a>
        </li>
        <li class="active">Edit Category
        </li>
      </ul>
      <!-- /.breadcrumb -->
    </div>
    <br>
        <input type="hidden" id="cid" value="{{$category->id}}" />
    <div class="row">
      <div class="col-xs-12">
        {{ Form::model($category, ['url' => 'admin/product/category/edit/'.$category->id, 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) }}
        <div class="col-md-8 ">

          <div class="col-md-12 block-section">
            <div class="form-group" style="padding: 2%;">
              <div>
                <label for="form-field-1"> Category Name 
                </label>
                {{ Form::input('text', 'name',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
                @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}
                  </strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="form-field-1"> Description 
                </label>
                {{ Form::textarea('description',null, ['id' => 'form-field-1', 'rows' => '6', 'class' => 'col-xs-10 col-sm-12 my-editor' , 'cols' => '55']) }}
              </div>

            </div>
             <br><br> <br><br>
            <?php
              $collection_pivot = DB::table('collection_type_pivots')
                                      ->where('collection_id', '=', $category->id)
                                      ->select('collection_type_id')
                                      ->get();

              $all_data = array();
              foreach($collection_pivot as $collection){
                   $all_data[] =  $collection->collection_type_id;
              }
            ?>
           <!--  <div class="form-group">
              <div class="col-sm-12">
                <label for="form-field-1"> Collection Type 
                </label><br>
                @foreach($collection_types as $collection_type)
                  
                  {{$collection_type->name}} <input type="radio" name="collection_type" value="{{$collection_type->id}}">
                  <hr>
                @endforeach
              </div>
            </div> -->
           <div class="form-group">
              <label class="col-sm-12" style="height: 42px !important;" for="form-field-1">Images </label>

              <div class="col-sm-12">
                <input type="file" name="category_slider[]" id="category_slider" class="col-xs-10 col-sm-12" multiple data-show-upload="false" class="file-loading" />
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 block-background" style="margin-bottom: 2%;">  
        <div class="form-group">
            <label class="col-sm-12"> 
            <b>Category Type</b><br><br>
              <?php if(!$category->is_parent == '1')
              {?>
                 <input name="is_parent" type="checkbox"> Is parent
             <?php }
             else
              { ?>

             <input name="is_parent" type="checkbox" checked="checked"> Is parent
             <?php }
              ?>
            <br>
            <br>

              <b>Visibility  
              </b> 
            </label>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->
               <?php if(!$category->status == '1')
              {?>
                 <input name="online_status" type="checkbox"> Online store 
             <?php }
             else
              { ?>

             <input name="online_status" type="checkbox" checked="checked"> Online store 
             <?php }
              ?>
              <br>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <!-- <label class="col-sm-12" for="form-field-1"> Vendor </label> -->
              <?php if(!$category->in_homepage == '1')
              {?>
                 <input name="homepage" type="checkbox"> On Home Page
             <?php }
             else
              { ?>

             <input name="homepage" type="checkbox" checked="checked"> On Home Page
             <?php }
              ?>
              <br>
            </div>
          </div>
        </div>
        
        <div class="col-md-4 block-background scroll" style="margin-bottom: 2%;">
          <div class="form-group">
            <div class="col-sm-12">
              <h4>Collection Type 
              </h4><br>
              @foreach($collection_types as $collection_type)
                 @if($collection_type->parent_id == NULL)
                {{$collection_type->name}} <input type="checkbox" name="collection_type[]" value="{{$collection_type->id}}" <?php if($collection_type->id == $category->collection_type_id) { echo 'checked="checked"'; } ?> >
                <hr>
                @endif
                @endforeach

             <!--  @foreach($collection_types as $collection_type)
                {{$collection_type->name}} <input type="radio" name="collection_type[]" value="{{$collection_type->id}}">
                <hr>
              @endforeach -->
            </div>
          </div>
        </div>
        <br><br>
        <div class="col-md-4 block-background">
          <div class="form-group col-md-8">
            <div class="col-sm-8">
              <label for="form-field-1"> Category Image 
              </label>
              <input type="file" name="category_image" id="category_image" class="col-xs-10 col-sm-12" />
            </div>
          </div>
          <div class="col-md-4">
            <div id='output_image'>
            </div>
            <img class="category-image" src="{{URL::to('/categories/featured_images/'.$category->image)}}">
          </div>
        </div>
        <div class="hr hr32 hr-dotted">
        </div>
        <div class="form-group">
          <div class="col-md-offset-9 col-md-3">
            <br>
            <br>
            <button class="btn btn-info" type="submit">
              <i class="ace-icon fa fa-check bigger-110">
              </i>
              Submit
            </button>
            &nbsp; &nbsp; &nbsp;
            <button class="btn" type="reset">
              <i class="ace-icon fa fa-undo bigger-110">
              </i>
              Reset
            </button>
          </div>
        </div>
        {{ Form::close() }}
      </div>
      <?php
        $catId = explode(",",$category->product_ids);
      ?>
   <!--    <div class="form-group">
        <label class="col-sm-12" for="form-field-1"> Products 
        </label>
        <div class="col-sm-12">
          {{Form::select('product', $products,$catId, array('id' => 'product_name','class' => 'col-xs-10 col-sm-12', 'multiple'))}}
        </div>
      </div> -->


<div class="show_product_data"></div>
@if(count($productByCategory)>0)
<table  border="1"  style="margin-top:10px;" id="old_showproduct_by_cat" class=" col-md-12 table table-striped table-bordered table-hover dataTable no-footer">
  <tr>
    <td>Image</td>
    <td>Product Title</td>
    <td>Price</td>
  </tr>
  @foreach($productByCategory as $productName)
  <tr>
    
    @if($productName->images!='')
          <td class="next-table__image-cell hide-when-printing">
            <img title="{{$productName->images}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$productName->id.'/'.$productName->images)}}" alt="{{$productName->product_title}}" style="width: 50px;">
          </td>
        @else
          <td class="next-table__image-cell hide-when-printing">
            <img title="No Image" class="block aspect-ratio__content" src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="No Image" style="width: 50px;">
           </td>
        @endif


    <td>{{$productName->product_title}}</td>
    <td>{{$productName->price}}</td>
  </tr>
  @endforeach
</table>
@endif
    </div>
  </div>
</div>

<div class="product_list">
</div>
@endsection


@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<script>
  var base_url = "{{URL::to('/')}}";
  $("#product_name").chosen({
    no_results_text: "Oops, nothing found!"}
                           ).change(function(e, params){
    var product_id = $("#product_name").chosen().val();
    var cid = $("#cid").val();

    $.ajax({
      url: base_url+'/admin/add-products-to-category',
      type: "post",
      data: 
      {
        "id": product_id,
        "cid": cid,
      }
      ,
      success:function(data)
      {
        $('.product_list').html(data);
      }
    });
  });

  // $("#product_name").change(function(){
  //   var pid = $("#product_name").val();
  //   var cid = $("#cid").val();
  //   //alert(pid);

  //    $.ajax({
  //           url: base_url+'/admin/add-product-category',
  //           type: "GET",
  //           data: 'cid='+cid+'&pid='+pid,
  //           success: function(data, status){
  //               //if (data.status == 1) {
  //                 //  alert(data.message);
  //                   $("#old_showproduct_by_cat").hide();
  //                   $(".show_product_data").html(data);
  //               //}
  //           }
  //       });
  // });
</script>

<?php
  use App\CategorySliders;
  $product_image = CategorySliders::where('category_id', '=', $category->id)->select('id', 'image')->get();
  // dd($product_image);
  // /product_images/'.$product->id.'/'.$product_image->images
?>
<script>
  $("#category_slider").fileinput({
      initialPreview: [
            <?php foreach ($product_image as $image) { ?>
              '{{URL::to("/categories/category_sliders/".$category->id.'/'.$image->image)}}',
            <?php } ?>

        ],
      initialPreviewAsData: true,
      initialPreviewConfig: [
        <?php foreach ($product_image as $image) { ?>
          {caption: "{{$category->name}}", width: "150px", key: "{{$image->id}}"},
         <?php } ?>
      ],
      deleteUrl: "/admin/categoryslider/delete",
      overwriteInitial: false,
      uploadAsync: true,
      maxFileCount: 5,
      // showUpload: false
  });
</script>
@endsection