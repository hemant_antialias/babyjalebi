<script src="{{URL::to('admin-assets/js/chosen.jquery.min.js')}}"></script>

<select name="product[]" id="product_name" class="form-control" multiple>
	@foreach($products as $product )
		<option value="{{$product->id}}">{{$product->product_title}}</option>
	@endforeach
</select>

<script>
	$("#product_name").chosen({
    no_results_text: "Oops, nothing found!"}).change(function(e, params){
    var product_id = $("#product_name").chosen().val();
    $.ajax({
      url: base_url+'/admin/add-products-to-category',
      type: "post",
      data: 
      {
        "id": product_id,
      }
      ,
      success:function(data)
      {
        $('.product_list').html(data);
      }
    }
          );
  }
 );
</script>