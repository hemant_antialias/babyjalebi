@extends('admin.admin_layout')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Review List</h3>
                  
                     <br><br>
                    <!-- Minimum price: -->
                    <!-- <input type="text" id="min" name="min"> -->
                    <!-- div.dataTables_borderWrap -->
                    <div class="inventory-data">
                        <table id="product-dynamic-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Product Title</th>
                                    <th>Review Rating</th>
                                    <th>Review Title</th>
                                    <th>Review Description</th>
                                    <th>Action</th>
                                    <!-- <th>Type</th> -->
                                    <!-- <th>Brand</th> -->

                                </tr>
                                <!-- make it th for sorting -->
                               <!--  <tr>
                                    <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th></th>
                                    <th class="input-filter ">Product Title</th>
                                    <th></th>
                                    <th></th>
                                    <th class="hidden-480 product-status ">Status</th>
                                    <th class="hidden-480 product-type ">Type</th>
                                    <!-- <th class="hidden-480 product-brand ">Brand</th> -->

                                </tr>
                            </thead>

                            <tbody class="product-inventory">

                            @foreach($reviews as $review)
                             
                                <tr data-id="{{$review->id}}">

                                    <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace product-id" value="{{$review->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>
                                    <td>{{$review->product_title}}</td>
                                    <td>{{$review->rating}}</td>
                                    <td>{{$review->title}}</td>
                                    <td>{{$review->comment}}</td>
                                    <td>
                                        {{ Form::select('review_status', ['' => 'Select', '0' => 'Reject', '2' => 'Approved', '1' => 'Pending'], $review->status, ['class' => 'field review_status']) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.col -->
    </div><!-- /.row -->
                    
@stop

@section('scripts')

    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>

    <script>
    $('.review_status').change(function(){
        var base_url = "{{URL::to('/')}}";
        var review_status = $('.review_status').val();
        var trid = $(this).closest('tr').attr('data-id');
        $.ajax({
            url: base_url+'/admin/review/change-status',
            type: "post",
            data: 
            {
                "id": trid,
                "review_status": review_status
            },
            success:function(data)
            {
                if (data.status == 1) {
                    toastr.success('Status changed.', {timeOut: 100000})
                }
            }
        });
    })
    </script>
    
@endsection