@if(count($productByCategory)>0)
<style>
  td, th {
    padding: 0;
    padding: 1%;
}
</style>
<div class="row">
<div class="table-responsive col-md-12">
<table class="table table-striped table-bordered">
  <tr>
    <th>Image</th>
    <th>Product Title</th>
    <th>Price</th>
  </tr>
  @foreach($productByCategory as $productName)
  <tr>
    
    @if($productName->product_images!='')
          <td class="next-table__image-cell hide-when-printing" >
            <img title="{{$productName->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$productName->id.'/featured_images/'.$productName->product_images)}}" alt="{{$productName->product_title}}" style="width: 50px;">
          </td>
        @else
          <td class="next-table__image-cell hide-when-printing">
            <img title="No Image" class="block aspect-ratio__content" src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="No Image" style="width: 50px;">
           </td>
        @endif


    <td>{{$productName->product_title}}</td>
    <td>{{$productName->price}}</td>
  </tr>
  @endforeach
</table>
</div>
</div>
@endif