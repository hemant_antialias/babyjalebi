@extends('admin.admin_layout')

@section('content')
<div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Store List</h3>
                    <div style="float: right;">
                    <button id="refresh" class="btn btn-default">Refresh</button>
                    <a href="{{URL::to('/admin/vendor/new')}}"> <button id="createvendor" class="btn btn-default">Create Store</button></a>
                     </div>
                     <br><br>

                    <!-- div.dataTables_borderWrap -->
                    <div class="inventory-data">
                        <table id="vendor-dynamic-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Store Name</th>
                                    <th>Email</th>
                                    <th>Customer Email</th>
                                    <th>City</th>
                                    <th>Status</th>
                                </tr>
                                <tr>
                                    <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="input-filter">Store Name</th>
                                    <th class="input-filter">Email</th>
                                    <th class="input-filter">Customer Email</th>
                                    <th class="input-filter">City</th>
                                    <th class="hidden-480 vendor-status">Status</th>
                                </tr>
                            </thead>

                            <tbody class="product-inventory">

                            @foreach($vendors as $vendor)
                                <tr>
                                    <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace product-id" value="{{$vendor->vendorid}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <a href="{{URL::to('/admin/vendor/edit/'.$vendor->vendorid)}}">{{$vendor->store_name}}</a>
                                    </td>
                                    <td>{{$vendor->email}}</td>
                                    <td>{{$vendor->customer_email}}</td>
                                    <td>{{$vendor->cityname}}</td>

                                    <td class="hidden-480">
                                        @if($vendor->is_active == 1)
                                            <span class="label label-sm label-success">Active</span>
                                        @else
                                            <span class="label label-sm label-warning">Inactive</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.col -->
    </div><!-- /.row -->
@stop

@section('scripts')

    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script>
$(document).ready(function() {
            
            $('.input-filter').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('.vendor-status').each( function () {
                var vendor_status = '<option value="active">Active</option><option value="inactive">Inactive</option>';
                $(this).html( '<select><option value="">Select</option>'+vendor_status+'</select>' );
            } );

            var myTable = $('#vendor-dynamic-table').DataTable({
                paging: true,
                sort: true,
                searching: true,
                select: {
                    style: 'multi'
                }
            });

            // $('#min, #max').keyup( function() {
            //     var minvalue = $('#min').val();
            //     var filteredData = myTable
            //         .column( 2 )
            //         .data()
            //         .filter( function ( value, index ) {
            //             return value > 270 ? true : false;
            //         } );
            // } );

            // Apply the search
            myTable.columns().every( function () {
                var that = this;
                
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


                serachTextBox.on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachSelectBox.on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


            } );
            
            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });
        });

    </script>

@endsection