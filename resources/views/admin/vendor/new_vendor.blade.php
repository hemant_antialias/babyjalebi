@extends('admin.admin_layout')


@section('content')

<style>
	.vendor-img {
    	height: 123px; 
    	width: 111px; 
}
</style>

	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Store</a>
					</li>
					<li class="active">Add Store</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/vendor/new', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) }}
						
						<div class="row">
						<div class="col-md-4">
						<h3>Store details</h3>
						<p>shopify and your customers will use this information to contact you.</p>
						</div>
						<div class="col-md-8 block-background">
						<div class="form-group col-md-12">
							<div>
							<label for="form-field-1"> Store Name </label>
								<input type="text" name="store_name" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-12" />
							    @if ($errors->has('store_name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('store_name') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
				<div class="form-group col-md-6 margin-rght">
							

							<div>
							<label for="form-field-1"> Account Email </label>
								<input type="text" name="email" id="form-field-1" placeholder="Account Email" class="col-xs-10 col-sm-12" />
								@if ($errors->has('email'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('email') }}</strong>
	                                </span>
	                            @endif
								<br><br><span class=>We'll use this address if we need to contact you about your account.</span>
							</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label for="form-field-1"> Customer Email </label>
								<input type="text" name="customer_email" id="form-field-1" placeholder="Customer Email" class="col-xs-10 col-sm-12" />
								<br><br><span class=>Your customers will see this address if you email them</span>
							
							</div>
						</div>
						</div>
						</div>
						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h3>Store address</h3>
						<p>This address will appear on your invoices. You can edit the address used to calculate shipping rates in your <a href="#">shipping settings</a>.</p>
						</div>
						<div class="col-md-8 block-background">
						<div class="form-group col-md-12">
							<div>
							<label for="form-field-1"> Legal name of business </label>
							    {{ Form::input('text', 'legalname',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>
						<div class="form-group col-md-12">
							<div>
							<label for="form-field-1"> Phone </label>
								<input type="text" name="contact" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						<div class="form-group col-md-12">
							<div>
							<label for="form-field-1"> Street </label>
								<input type="text" name="address" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						<div class="form-group col-md-12">
							<div>
							<label for="form-field-1"> Apt, suite, etc. (optional) </label>
								<input type="text" name="apt_suite" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						<div class="form-group col-md-6 margin-rght">
							<div>
							<label for="form-field-1"> City </label>
                				{{  Form::select('city', $cities,null,['class'=>'col-xs-10 col-sm-12', 'id' => 'form-field-1']) }}
							</div>
						</div>
						<div class="form-group col-md-6">
							<div>
							<label for="form-field-1"> Pin Code </label>
								<input type="text" name="pin_code" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6 margin-rght">
							<div>
							<label for="form-field-1"> State </label>
								<input type="text" name="state" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label for="form-field-1"> Country </label>
								<input type="text" name="country" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						</div>
						</div>
						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h3>Store image</h3>
						</div>
						<div class="col-md-8 block-background">
						<div class="form-group">
							<div class="col-sm-9">
							<label for="form-field-1"> Store Image </label>
								<input type="file" name="vendor_image" id="vendor_image" class="col-xs-10 col-sm-12" />
							</div>
							<div class="col m4">
								<div id='output_image'></div>
								<img class="vendor-img" src="">
							</div>
						</div>
						</div>
						</div>
						<div class="hr hr32 hr-dotted"></div>
						<div class="form-group">
							<div class="col-md-offset-10 col-md-2">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>


@endsection