@extends('admin.admin_layout')

@section('content')

<div class="">

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> Edit Status Type :   <?php echo $attribute->name?></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    {{ Form::open(['url' => 'admin/attribute/postedit/'.$attribute->id, 'method' => 'post', 'files' => true, 'id' => 'demo-form2', 'class' => 'form-horizontal form-label-left', 'data-parsley-validate']) }}


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                @foreach ($errors->all() as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" value="{{$attribute->name}}" name="name" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Value <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{  Form::select('attribue_value[]', [],null,['id' => 'attribute-value', 'class' => 'form-control  col-md-7 col-xs-12', 'multiple']) }}
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                    {{Form::close()}}

                </div>
            </div>
        </div>
    </div>

</div>

<script>
    <?php
        $attributes_values = DB::table('attribute_value')
                                ->where('attribute_id', $attribute->id)
                                ->select('id', 'value')
                                ->get()
                                ->toJson();
    ?>

    var s2 = $('#attribute-value').select2({
        placeholder: 'Add value',
        data: '<?php echo $attributes_values ?>',
        tags: true
    });
</script>


@stop