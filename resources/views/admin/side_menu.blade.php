<?php
	use App\Admin;
	use App\Vendor;

	$staff_id = Session::get('staff');
    $staff = Admin::where('id', $staff_id)->first();
?>
<?php

	$stores = DB::table('vendors_details')
				->join('admin', 'vendors_details.admin_id', '=', 'admin.id', 'left')
				->select('admin.id', 'vendors_details.store_name')
				->get();


	?>
	@if (isStore())
	<?php
	$vendor = Vendor::where('admin_id', isStore()->id)->select('store_name')->first();
	?>

	<h5 style="text-align: center;color: #d8920c"> {{$vendor->store_name}}  </h5>
	@endif
	

<ul class="nav nav-list">

<img class="logoimage" src="{{URL::to('admin-assets/images/icons/logo.png')}}" alt="babyjalebi" style="width:85%;">	
    <li class="active">
		<a href="{!! URL::to('') !!}/admin/dashboard">
			<img src="{{URL::to('admin-assets/images/icons/Object.png')}}">
			<span class="menu-text"> Dashboard </span>
		</a>

		<b class="arrow"></b>
	</li>

	 <li class="active">
		<a href="{!! URL::to('') !!}/admin/menu">
			<img src="{{URL::to('admin-assets/images/icons/Object.png')}}">
			<span class="menu-text"> Menu </span>
		</a>

		<b class="arrow"></b>
	</li>

	<!-- <li class="active">
		<a href="{!! URL::to('') !!}/admin/dashboard">
			<i class="menu-icon fa fa-user"></i>
			<span class="menu-text"> Dashboard </span>
		</a>

		<b class="arrow"></b>
	</li> -->
	<li class="">
		<a href="#" class="dropdown-toggle">
			<img src="{{URL::to('admin-assets/images/icons/Object1.png')}}">
			<span class="menu-text">
				Staff User
			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="{{URL::to('/admin/user/new')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Add User
				</a>

				<a href="{{URL::to('/admin/users')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					User List
				</a>

				<a href="{{URL::to('/admin/add-permission')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Add Permission
				</a>

				<a href="{{URL::to('/admin/add-role')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Add Role
				</a>

				<b class="arrow"></b>
			</li>

		</ul>
	</li>

	<li class="">
		<a href="#" class="dropdown-toggle">
			<img src="{{URL::to('admin-assets/images/icons/Object3.png')}}">
			<span class="menu-text">
				Orders	
				<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<span class="badge badge-important">{{totalOrderCount()}}</span>
							
			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="{{URL::to('admin/order/new')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Create Order
				</a>

				<b class="arrow"></b>
			</li>
			<li class="">
				<a href="{{URL::to('admin/order')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Orders List
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="{{URL::to('/admin/abondand')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Abandoned List
				</a>

				<b class="arrow"></b>
			</li>

		</ul>
	</li>
	<li class="">
		<a href="#" class="dropdown-toggle">
          <img src="{{URL::to('admin-assets/images/icons/Object4.png')}}">

			<span class="menu-text"> Product </span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="{{URL::to('/admin/product/new')}}">
					<i class="menu-icon fa fa-plus purple"></i>
					Add product
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="{{URL::to('/admin/product')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					 Product List
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="{{URL::to('/admin/attribute/add')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					 Add Attribute
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="{{URL::to('/admin/attribute')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					 Attribute List
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="{{URL::to('/admin/products/inventory')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					 Product Inventory
				</a>

				<b class="arrow"></b>
			</li>
			<li class="">
				<a href="{{URL::to('/admin/reviewlist')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					 Product Reviews
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="{{URL::to('/admin/product/category/new')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Add Category				</a>

				<b class="arrow"></b>
			</li>
			<li class="">
				<a href="{{URL::to('/admin/categorylist')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Category List
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	<li class="">
		<a href="#" class="dropdown-toggle">
			<img src="{{URL::to('admin-assets/images/icons/Object5.png')}}">
			
			<span class="menu-text">
				Customer
			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="{{URL::to('/admin/customers/new')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Add Customer
				</a>

				<a href="{{URL::to('/admin/customers')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Customer List
				</a>

				<b class="arrow"></b>
			</li>

		</ul>
	</li>
	
	<li class="">
		<a href="{{URL::to('admin/report/reportlist')}}" >
			<img src="{{URL::to('admin-assets/images/icons/Object6.png')}}">
			
				Reports
			
		</a>

		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="#" class="dropdown-toggle">
			<img src="{{URL::to('admin-assets/images/icons/Object7.png')}}">
			<span class="menu-text">
				Discounts
			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="{{URL::to('admin/coupon/new')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Add Discount
				</a>

				<b class="arrow"></b>
			</li>
			<li class="">
				<a href="{{URL::to('admin/couponlist')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Discount List
				</a>

				<b class="arrow"></b>
			</li>

		</ul>
	</li>
	<li class="">
		<a href="#" class="dropdown-toggle">
			<img src="{{URL::to('admin-assets/images/icons/Object9.png')}}">
			<span class="menu-text">
				Homepage Slider
			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="{{URL::to('admin/add-banner')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Add Slider
				</a>
				<a href="{{URL::to('admin/bannerlist')}}">
					<i class="menu-icon fa fa-caret-right"></i>
					Slider List
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>
	<li class="">
		<a href="#" class="dropdown-toggle">
         <img src="{{URL::to('admin-assets/images/icons/Object9.png')}}">

			<span class="menu-text">
				Blog
			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="{{URL::to('admin/blog/new')}}">
					<i class="menu-icon fa fa-file-text-o"></i>
					Add Blog
				</a>

				<b class="arrow"></b>
			</li>
			<li class="">
				<a href="{{URL::to('admin/blog')}}">
					<i class="menu-icon fa fa-file-text-o"></i>
					Blog List
				</a>

				<b class="arrow"></b>
			</li>

		</ul>
	</li>
	<li class="">
		<a href="https://www.tidiochat.com/" target="_blank">
			<img src="{{URL::to('admin-assets/images/icons/Object2.png')}}">
			
				Chat
			
		</a>

		<b class="arrow"></b>
	</li>
</ul><!-- /.nav-list -->

<!-- <script type="text/javascript">
function explode(){
  var audio = new Audio('{{URL::to('web-assets/youve-been-informed.ogg')}}');
                 audio.play();
  $.ajax({
    url : " ",
    type: "POST",
    data : formData,
    success: function(data, textStatus, jqXHR)
    {
        //data - response from server
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
 
    }
});                   
}
setInterval(explode, 2000);


// 	

</script> -->