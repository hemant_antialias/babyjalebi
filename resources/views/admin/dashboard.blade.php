<!DOCTYPE html>

<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Dashboard -BabyJalebi
    </title>
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    @include('admin.css')
    <!-- inline styles related to this page -->
    <!-- ace settings handler -->
    <!-- Include Required Prerequisites -->

<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" /> -->


    <script src="{{URL::to('admin-assets/js/ace-extra.min.js')}}">
    </script>
    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
<script src="{{URL::to('admin-assets/js/html5shiv.min.js')}}"></script>
<script src="{{URL::to('admin-assets/js/respond.min.js')}}"></script>
<![endif]-->
    <style type="text/css">
    .nav > li > a:hover, .nav > li > a:focus
    {
          background-color: transparent !important;
    }
    .scroll
    {
    margin-bottom: 2%;
    height: 250px !important;
    overflow: scroll;
}
    @media only screen and ( max-width: 720px){
      #dashcolumn{
        margin-top: 20% !important ;
      }
    }



    .daterangepicker.ltr {
    direction: ltr;
    text-align: left;
        top: 175.203px;
    left: 771px;
    right: auto;
    display: block;
    width: 39%;
    display: none;
}
.centerthis{ text-align: center; }
.viewl{
margin-left: 14%;
text-transform: uppercase;
}
.lowoninventory{
  float: left;
  width: 70%;
}
.applyBtn{
  background:##02c9c6 !important;
  border: #02c9c6 !important;
}
.applyBtn:hover {
  background: black !important;
  border: black !important;
}
.cancelBtn{
  background: black !important;
  border: black !important;
}
.ranges{ float: right !important; }

      ul{
        list-style-type: none;
      }
      .property_image {
        height: 170px;
      }
      .background-layout
      {
        padding: 2%;
        background-color: #ffffff;
        border-radius: 3px;
        box-shadow: 0 2px 4px rgba(0,0,0,0.1);
        
      }
       .background-layout1
      {
        padding: 2%;
        background-color: #ffffff;
        border-radius: 3px;
        box-shadow: 0 2px 4px rgba(0,0,0,0.1);
        margin: 3px;
    width: 335px !important;
      }
      .padding-lft
      {
        margin-right: 3%;
      }
      .ace-nav>li>a>.ace-icon {
    display: inline-block;
    font-size: 32px;
    color: #FFF;
    text-align: center;
    width: 33px;
    margin-top: 7px;
}
.ace-nav>li.light-blue>a {
/*    background-color: rgba(61, 61, 61, 0.05);*/
}
/*.navbar-default {
position: fixed !important;}*/
.navbar {
    margin: 0;
    padding-left: 0;
    padding-right: 0;
    border-width: 0;
    border-radius: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    min-height: 45px;
    background: #d8920c;
    position: absolute;
    z-index: 999;
    width: 100%;
}
.ace-nav>li>a>.ace-icon {
    display: inline-block;
    font-size: 32px;
    color: #191717;
    text-align: center;
    width: 33px;
    margin-top: 7px;
}
b, strong {
    font-weight: initial !important;
}
        
 .table > thead > tr > th,{
            
    padding-top: 13px;
    padding-bottom: 14px;
    line-height: 1.42857143;
    vertical-align: top;
    font-size: 11px;
    font-weight: 400;
    letter-spacing: 1px;
    font-family: 'monteserrat', sans-serif;
        }  
        
.table > tbody > tr > td,{
            padding: 8px;
    padding-top: 18px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
        }
        .dashmedblock{
            padding-right: 0px !important;
            padding-left: 20px !important;
        }
        .dashtopblock{
                background: white;
    padding-left: 5px !important;
    padding-top: 5px;
    padding-bottom: 5px;
        } 
 .table-bordered > tbody > tr > td{
    border: 1px solid #ddd;
    font-size: 12px;
    font-size: 12px;
    vertical-align: middle !important;
        }
        .nav > li > a > img {
    max-width: none;
    width: 11%;
    margin-right: 6px;
}
 .nav-list > li.active::after {
    display: block;
    content: "";
    position: absolute;
    right: -2px;
    top: -1px;
    bottom: 0px;
    z-index: 1;
    border-style: solid;
    border-color: rgb(216, 146, 12);
    border-image: initial;
    border-width: 0px 2px 0px 0px;
}
h4, .h4 {
    font-size: 13px!important;
    font-family: monteserrat , sans-serif !important;
    letter-spacing: 1px !important;
    color: #4B4B4B !important;
    font-weight: 400 !important;       
}
    </style>
  </head>
  <body class="no-skin">
    <div id="navbar" class="navbar navbar-default ace-save-state" style=" margin-bottom: 0px; ">
        
<!--
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right">
          </i>
        </div>
-->

    <div id="navbar" class="navbar navbar-default ace-save-state" style=" background: #02c9c6;position: fixed;top: 0 !important;">
      <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
          <span class="sr-only">Toggle sidebar</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>
        </button>

       <!--  <div class="navbar-header pull-left">
          <a href="{{URL::to('/admin/dashboard')}}" class="navbar-brand">
            <small>
              <i class="fa fa-home"></i>
               <img class="logoimage" src="{{URL::to('css/assets/logo9a04.png')}}" alt="Lion Fresh Delhi" style="width:28%;">
            </small>
          </a>
        </div> -->

<a href="#"><img src="{{URL::to('admin-assets/images/icons/menu.png')}}" style="margin-left: 14.6%;margin-top:2%; width:20px; "></a>
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
          <ul class="nav ace-nav">
            @if(Session::has('admin'))

            <li class="light-blue dropdown-modal">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                      <!-- <img class="nav-user-photo" src="{{URL::to('admin-assets/images/avatars/user.jpg')}}" alt="Jason's Photo" /> -->
                       <span class="user-info" style="padding-top:8px;">
			                  <!-- <small>Welcome,
			                  </small> -->
			                  <!-- @if(isStore())
			                  {{isStore()->name}}
			                  @else -->
			                    <!-- Bharat -->
			                  <!-- @endif -->
                                <img src="{{URL::to('admin-assets/images/icons/adm_.png')}}" style="width:30%; ">

                                <span style="font-size: 14px;padding-left: 6%;">
                                    <?php
                                        $id = Session::has('admin');
                                        $admin = DB::table('admin')->where('id', $id)->first();
                                    ?>
                                    {{$admin->name}} 
                                </span>
                      <i class="ace-icon arrow fa fa-angle-down">
                      </i>
                    </a>

              <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close" style="width:320px;right:0px;top:81px;">
                <li>
                  <a href="{{URL::to('admin/settings')}}">
                    <i class="ace-icon fa fa-cog"></i>
                    Settings
                  </a>
                </li>

                <li>
                  <a href="{{URL::to('admin/edit-profile')}}">
                    <i class="ace-icon fa fa-user"></i>
                    Edit Profile
                  </a>
                </li>

                <li class="divider"></li>

                <li>
                  <a href="{{URL::to('admin/logout')}}">
                    <i class="ace-icon fa fa-power-off"></i>
                    Logout
                  </a>
                </li>
              </ul>
            </li>
            @endif
          </ul>
        </div>
      </div><!-- /.navbar-container -->
      <!-- /.navbar-container -->
    </div>
    <div class="main-container ace-save-state" id="main-container">
      <script type="text/javascript">
        try{
          ace.settings.loadState('main-container')}
        catch(e){
        }
      </script>
      <div id="sidebar" class="sidebar responsive ace-save-state">
        <script type="text/javascript">
          try{
            ace.settings.loadState('sidebar')}
          catch(e){
          }
        </script>
        <!-- Side Menu -->
        @include('admin.side_menu')
        
      </div>
      <div class="main-content">
        <div class="main-content-inner">
          
          <div class="page-content">
            <div class="ace-settings-container" id="ace-settings-container">
             <!--  <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="ace-icon fa fa-cog bigger-130">
                </i>
              </div> -->
              <div class="ace-settings-box clearfix" id="ace-settings-box">
                <div class="pull-left width-50">
                  <div class="ace-settings-item">
                    <div class="pull-left">
                      <select id="skin-colorpicker" class="hide">
                        <option data-skin="no-skin" value="#438EB9">#438EB9
                        </option>
                        <option data-skin="skin-1" value="#222A2D">#222A2D
                        </option>
                        <option data-skin="skin-2" value="#C6487E">#C6487E
                        </option>
                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0
                        </option>
                      </select>
                    </div>
                    <span>&nbsp; Choose Skin
                    </span>
                  </div>
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar
                    </label>
                  </div>
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar
                    </label>
                  </div>
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs
                    </label>
                  </div>
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)
                    </label>
                  </div>
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                    <label class="lbl" for="ace-settings-add-container">
                      Inside
                      <b>.container
                      </b>
                    </label>
                  </div>
                </div>
                <!-- /.pull-left -->
                <div class="pull-left width-50">
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                    <label class="lbl" for="ace-settings-hover"> Submenu on Hover
                    </label>
                  </div>
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                    <label class="lbl" for="ace-settings-compact"> Compact Sidebar
                    </label>
                  </div>
                  <div class="ace-settings-item">
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                    <label class="lbl" for="ace-settings-highlight"> Alt. Active Item
                    </label>
                  </div>
                </div>
                <!-- /.pull-left -->
              </div>
              <!-- /.ace-settings-box -->
            </div>
            <!-- /.ace-settings-container -->
            <div class="row" style="margin-top: 6%;">
            <div class="col-md-12" >
                     
                      <?php
                        // I'm India so my timezone is Asia/Calcutta
                        date_default_timezone_set('Asia/Calcutta');

                        // 24-hour format of an hour without leading zeros (0 through 23)
                       $Hour = date('G');

                        if ( $Hour >= 5 && $Hour <= 11 ) {
                            $greeting = "Good Morning";
                        } else if ( $Hour >= 12 && $Hour <= 17 ) {
                             $greeting = "Good Afternoon";
                        } else if ( $Hour >= 18 || $Hour <= 4 ) {
                             $greeting = "Good Evening";
                        }
                      ?>
                        <strong class="" style="text-align: left" >
                          @if(isStore())
                          <h5>{{$greeting}}, {{isStore()->name}}
                          </h5>
                          @else
                          <h5>{{$greeting}}, <?php $name = (Session::get('admin'));
                           $name;
                            $customer_list1 = DB::table('admin')
                                          ->select('id', 'name')
                                          ->where('id' , '=' , $name)
                                          ->first();  
                                          ?>
                                          Gunia
                          </h5>
                          @endif

                          
                        </strong>
                      <hr>
                      </div>
              <div class="col-xs-120" >
                <div class="row">
                  <div class="col-md-8 dashmedblock" style="background-size: contain; background-repeat: no-repeat;">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="dashtopblock col-md-12" id="dashcolumn" style="padding-right: 0;padding-left: 0;">
                      
                      <div class= "babywidth col-xs-6 col-md-6 background-layout1  totalamnt" >
                        <div >
                          
                          <h2 class=""> Rs. {{round($total_sales->payble_amount , 2)}}
                         
                          </h2>
                          <h5> Today's sales 
                          </h5>
                        </div>
                      </div>
                       <div class="babywidth col-xs-6 col-md-6 background-layout1  totalamnt" >
                        <div >
                         

                          <h2 class=""> Rs.
                          <?php 
                          if($total_today_order->totaltodaysorder == 0) 
                          {
                            echo $average_cart_value = 0;
                          }
                          else
                          {
                           $average_cart_value = $total_sales->payble_amount/$total_today_order->totaltodaysorder;
                           echo round($average_cart_value);
                          }
                         
                          ?>

                         
                          </h2>
                           <h5> Average Cart Value
                          </h5>
                        </div>
                        
                      </div>
                      <br>
                       <div class="col-xs-6 col-md-6 background-layout1  totalamnt" >
                        <div >
                         
                          <?php
                            $visitors = DB::table('visitors')
                                          ->where('visit_date', date('Y-m-d'))
                                          ->selectRaw('count(id) as hits')
                                          ->first()
                          ?>
                          <h2 class="">

                        <?php
                           $visitors = DB::table('visitors')
                                         ->where('visit_date', date('Y-m-d'))
                                         ->selectRaw('count(id) as hits')
                                         ->first()
                         ?>

                                        
                         {{$visitors->hits}}
                          </h2>
                           <h5> Today's Visitors
                          </h5>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-6 background-layout1  totalamnt">
                        <div >
                         
                          <h2 class="">
                          {{$total_today_order->totaltodaysorder}}
                          
                          </h2>
                           <h5> Today's Orders
                          </h5>
                        </div>
                      </div>
                    </div>
                     

                  <div class="col-sm-12 background-layout " style="margin-top: 3%;">
                      <div>
                      <div>
                        <div>
                          
                            <!-- <i class="ace-icon fa fa-star orange"></i> -->
                            <h4 class="lowoninventory" style="float: left;text-align: left;">These products are low on inventory</h4>
                          </h4>

                          <a href="{{URL::to('admin/report/inventory')}}"><button type="button" class="viewl btn btn-primaryviewall">View All</button></a>
                         <!--  <div class="widget-toolbar">
                            <a href="#" data-action="collapse">
                              <i class="ace-icon fa fa-chevron-up">
                              </i>
                            </a>
                          </div> -->
                        </div>
                        </div>
                        
                          <hr>
                        <div class="widget-body">
                          <div class="widget-main no-padding">
                            <table class="table table-bordered ">
                              <thead class="thin-border-bottom">
                                <tr>
                                  <th  class="th-style-leftbar">
                                    <i class="">
                                    </i>Image
                                  </th>
                                   <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Product name
                                  </th>
                                  <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Total Price
                                  </th>
                                  <th  class="th-style-leftbar">
                                    <i class="">
                                    </i> Quantity
                                  </th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($low_inventory as $product)
                                <tr>
                                <td style="width: 12%;"><img title="{{$product->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" style="width: 40px; height: 40px; "></td>
                                  
                                  <td style=" text-align: left; ">
                                    <a href="{{URL::to('admin/product/edit/'.$product->id)}}"> {{$product->product_title}}
                                    </a>
                                  </td>
                                  <td class="hidden-480 text-center" >
                                    <span class="">Rs. {{$product->price}}
                                    </span>
                                  </td>
                                  <td class="text-center">
                                    <b>{{$product->quantity}}
                                    </b>
                                  </td>
                                  
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                          <!-- /.widget-main -->
                        </div>
                        <!-- /.widget-body -->
                      </div>
                      </div>
                      <!-- /.widget-box -->
                    <div class="col-sm-12 background-layout" style="margin-top: 3%;">
                      <div>
                        <div>
                          
                            <!-- <i class="ace-icon fa fa-star orange"></i> -->
                            <h4 class="lowoninventory">These products are on discount</h4>
                          </h4>

                           <a href="{{URL::to('admin/report/discountedproduct')}}"><button type="button" class="viewl btn btn-primaryviewall">View All</button></a>
                          <hr>
                         <!--  <div class="widget-toolbar">
                            <a href="#" data-action="collapse">
                              <i class="ace-icon fa fa-chevron-up">
                              </i>
                            </a>
                          </div> -->
                        </div>
                        <div class="widget-body">
                          <div class="widget-main no-padding">
                            <table class="table table-bordered">
                              <thead class="thin-border-bottom">
                                <tr>
                                  <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Image
                                  </th>
                                   <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Product name
                                  </th>
                                  <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Total Price
                                  </th>
                                  <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Discounted Price
                                  </th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($discountedproduct as $discountproduct)
                                <tr>
                                <td style="width: 12%;"><img title="{{$discountproduct->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$discountproduct->id.'/featured_images/'.$discountproduct->product_images)}}" alt="{{$discountproduct->product_title}}" style="width: 40px; height: 40px; "></td>
                                  
                                  <td>
                                    <a href="{{URL::to('admin/product/edit/'.$discountproduct->id)}}"> {{$discountproduct->product_title}}
                                    </a>
                                  </td>
                                  <td class="hidden-480">
                                    <span class="">Rs. {{$discountproduct->compare_price}}
                                    </span>
                                  </td>
                                  <td>
                                    <b class="">Rs. {{$discountproduct->price}}
                                    </b>
                                  </td>
                                  
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                          <!-- /.widget-main -->
                        </div>
                        <!-- /.widget-body -->
                      </div>
                      </div>
                      <div class="col-sm-12 background-layout" style="margin-top: 3%;">
                      <div>
                        <div>
                          
                            <!-- <i class="ace-icon fa fa-star orange"></i> -->
                            <h4 class="lowoninventory">These products are least added to the cart</h4>
                          </h4>
                           <a href="{{URL::to('admin/report/top_cart_product')}}"><button type="button" class="viewl btn btn-primaryviewall" >View All</button></a>
                          <hr>
                         <!--  <div class="widget-toolbar">
                            <a href="#" data-action="collapse">
                              <i class="ace-icon fa fa-chevron-up">
                              </i>
                            </a>
                          </div> -->
                        </div>
                        <div class="widget-body">
                          <div class="widget-main no-padding">
                            <table class="table table-bordered">
                              <thead class="thin-border-bottom">
                                <tr>
                                  <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Image
                                  </th>
                                   <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Product name
                                  </th>
                                  <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Total Price
                                  </th>
                                  <th class="th-style-leftbar">
                                    <i class="">
                                    </i>Number
                                  </th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($top_cart_product as $product)
                                <!-- dd($product->id); -->
                                <!-- {{$product->id}} -->

                                <?php 
                  //               $id = $product->product_id;
                  //               $least_cart_product = DB::table('top_cart_product')
                  // ->join('product', 'top_cart_product.product_id', '=', 'product.id', 'left')
                  // ->whereNotIn('prodduct.id',$id)
                  // ->select('top_cart_product.id', 'top_cart_product.count', 'product.product_title', 'product.price', 'product.product_images', 'product.id')
                  // ->limit(3)
                  // ->get();
                  // dd($least_cart_product);

                                ?>
                                <tr>
                                <td style="width: 12%;"><img title="{{$product->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" style="width: 40px; height: 40px; "></td>
                                  
                                  <td>
                                    <a href="{{URL::to('admin/product/edit/'.$product->id)}}"> {{$product->product_title}}
                                    </a>
                                  </td>
                                  <td class="hidden-480">
                                    <span class="">Rs. {{$product->price}}
                                    </span>
                                  </td>
                                  <td>
                                    <b class="">{{$product->count}}
                                    </b>
                                  </td>
                                  
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                          <!-- /.widget-main -->
                        </div>
                        <!-- /.widget-body -->
                      </div>
                      </div><div class="col-sm-12 background-layout" style="margin-top: 3%;">
                      <div>
                        <div>
                          
                            <h4 class="lowoninventory">These are the highest spending customers</h4>
                          </h4>

                          <a href="{{URL::to('admin/report/customers')}}"> <button type="button" class="viewl btn btn-primaryviewall" >View All</button></a>

                          <hr>
                        </div>
                        <div class="widget-body">
                          <div class="widget-main no-padding">
                            <table class="table table-bordered table-striped">
                              <thead class="thin-border-bottom">
                                <tr>
                                  <th>
                                    <i class="">
                                    </i>Customer
                                  </th>
                                   <th>
                                    <i class="">
                                    </i>Last Order
                                  </th>
                                  <th class="hidden-480">
                                    <i class="">
                                    </i>Total Orders
                                  </th>
                                  <th>
                                    <i class="">
                                    </i>Lifetime Spent
                                  </th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($total_spent as $spent)
                                                               <tr>
                                <td>
                                <a href="{{URL::to('/admin/customers/'.$spent->id)}}">{{$spent->first_name}}</a></td>
                                  <td>
                                     {{$spent->created_at}}
                                   
                                  </td>
                                   <td>
                                    {{$spent->total_order}}
                                    
                                  </td>
                                   <td>
                                    Rs.{{$spent->total_amount}}
                                    
                                  </td>
                                 
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.widget-body -->
                      </div>
                      </div>
                        
                        
                      <!-- /.widget-box -->
                    </div>
                      
                    <div class="col-md-4">
                      <div class="col-sm-12 background-layout">
                        <label for="id-date-range-picker-1">Date Range Picker
                        </label>
                        <div class="row" style=" margin-top: 5%; ">
                          <div class="col-xs-8 col-sm-11">
                            <div class="input-group" id="datehere">
                              <span class="input-group-addon" style="background: #fff;" >
                                <i class="fa fa-calendar bigger-110 datepic">
                                </i>
                              </span>
                             <input type="text" name="daterange" class="datepic" value="" style="width: 100%; color : #333d46;" />
                            </div>
                          </div>
                        </div>
                        <hr>
                        <!-- /.widget-box -->
                         <div class="totalamnt">
                           
                              <h3 class="total_sales">Rs.  {{round($total_sales->payble_amount , 2)}}</h3>

                               <h6>
                              <!-- <i class="ace-icon fa fa-star orange"></i> -->
                            Total Sales
                            </h6>

                         
                         
                          </div>
                       
                        <br>
                        
                       
                        <div class="widget-box transparent">
                          <div class="widget-header widget-header-flat">
                            <h5 class="widget-title lighter">
                              <!-- <i class="ace-icon fa fa-star orange"></i> -->
                              TOTAL SALES BY CHANNEL
                            </h5>
                            <div class="widget-toolbar">
                              <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up">
                                </i>
                              </a>
                            </div>
                            <hr>
                          </div>
                          <div class="widget-body">
                            <div class="widget-main no-padding">
                              <table class="table table-bordered ">
                                <thead class="thin-border-bottom">
                                  <tr>
                                    <th class="th-style">
                                      <i class="">
                                      </i>name
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>price
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>Order
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Online Store
                                    </td>
                                    <td>
                                      <b class=" total_online_sales">Rs. {{round($total_online_sales->payble_amount , 2)}}
                                      </b>
                                    </td>
                                    <td class="hidden-480">
                                      <span class="total_online_orders">{{($total_order_online->totalorder)}} Orders
                                      </span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Offline Sale
                                    </td>
                                    <td>
                                      <b class="blue total_offline_sales">Rs. {{round($total_offline_sales->payble_amount , 2)}}
                                      </b>
                                    </td>
                                    <td class="hidden-480">
                                      <span class="total_offline_orders">{{($total_order_offline->totalorder)}} Orders
                                      </span>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.widget-main -->
                          </div>
                          <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-box -->
                        <div class="hr hr32 hr-dotted">
                        </div>
                        <div class="widget-box transparent">
                          <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                              <!-- <i class="ace-icon fa fa-star orange"></i> -->
                              These are the top Selling Products
                            </h4>
                            
                            <div class="widget-toolbar">
                              <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up">
                                </i>
                              </a>
                            </div>
                            <hr>
                          </div>
                          <div class="widget-body">
                            <div class="widget-main no-padding">
                              <table class="table table-bordered ">
                                <thead class="thin-border-bottom">
                                  <tr>
                                  <th class="th-style">
                                      <i class="">
                                      </i>Image
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>name
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>Number
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>Total Price
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                 @foreach($top_selling_product as $product)
                                <tr>
                                <td><img title="{{$product->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$product->product_id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" style="width: 60%"></td>
                                  
                                  <td>
                                    <a href="{{URL::to('admin/product/edit/'.$product->product_id)}}"> {{$product->product_title}}
                                    </a>
                                  </td>
                                  <td>
                                    <b class="">{{$product->total_product_record}}
                                    </b>
                                  </td>
                                  <td class="hidden-480 ">
                                  <?php 
                                  $amount = $product->total_product_record * $product->price;
                                  ?>
                                    <span class="">Rs. <?php echo $amount; ?>
                                    </span>
                                  </td>
                                </tr>

                                @endforeach
                                </tbody>
                              </table>
                              <br>
                              <a href="{{URL::to('admin/report/top_selling_product')}}"> <button type="button" class="viewl btn btn-primaryviewall">View All</button></a>
                            </div>
                            <!-- /.widget-main -->
                          </div>
                          <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-box -->
                         <div class="hr hr32 hr-dotted">
                        </div>
                        <div class="widget-box transparent">
                          <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                              <!-- <i class="ace-icon fa fa-star orange"></i> -->
                              Products most added to the cart
                            </h4>
                            
                            <div class="widget-toolbar">
                              <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up">
                                </i>
                              </a>
                            </div>
                            <hr>
                          </div>
                          <div class="widget-body">
                            <div class="widget-main no-padding">
                              <table class="table table-bordered ">
                                <thead class="thin-border-bottom">
                                  <tr>
                                  <th class="th-style">
                                      <i class="">
                                      </i>Image
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>name
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>Number
                                    </th>
                                    <th class="th-style">
                                      <i class="">
                                      </i>Total Price
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach($top_cart_product as $product)
                                <tr>
                                <td><img title="{{$product->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" style="width: 60%"></td>
                                  
                                  <td>
                                    <a href="{{URL::to('admin/product/edit/'.$product->id)}}"> {{$product->product_title}}
                                    </a>
                                  </td>
                                  <td>
                                    <b class="">{{$product->count}}
                                    </b>
                                  </td>
                                  <td class="hidden-480">
                                  <?php 
                                  $amount = $product->count * $product->price;
                                  ?>
                                    <span class="">Rs. <?php echo $amount; ?>
                                    </span>
                                  </td>
                                </tr>

                                @endforeach
                                                                </tbody>

                              </table>
                              <br>
                              <a href="{{URL::to('admin/report/most_added_to_cart')}}"> <button type="button" class="viewl btn btn-primaryviewall" style="">View All</button></a>
                            </div>
                            <!-- /.widget-main -->
                          </div>
                          <!-- /.widget-body -->
                        </div>
                      </div>
                      <!-- /.col -->
                    </div>
                  </div>
                  <!-- PAGE CONTENT ENDS -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.page-content -->
          </div>
        </div>
        <!-- /.main-content -->
        <!-- <div class="footer">
          <div class="footer-inner">
            <div class="footer-content">
              <span class="bigger-120">
               &copy; LIONFRESH DESIGNED AND DEVELOPED BY THE ANTIALIAS
              </span>
              &nbsp; &nbsp;
              <span class="action-buttons">
                <a href="#">
                  <i class="ace-icon fa fa-twitter-square light-blue bigger-150">
                  </i>
                </a>
                <a href="#">
                  <i class="ace-icon fa fa-facebook-square text-primary bigger-150">
                  </i>
                </a>
                <a href="#">
                  <i class="ace-icon fa fa-rss-square orange bigger-150">
                  </i>
                </a>
              </span>
            </div>
          </div>
        </div> -->
        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
          <i class="ace-icon fa fa-angle-double-up icon-only bigger-110">
          </i>
        </a>
      </div>
      <!-- /.main-container -->
      @include('admin.js')
      <script type="text/javascript" src="{{URL::to('admin-assets/js/jquery-3.1.1.min.js')}}"></script>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"> </script> -->
      <script type="text/javascript" src="{{URL::to('admin-assets/js/moment.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />

    
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="{{URL::to('admin-assets/js/daterangepicker.js')}}"></script>
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script> -->
<link rel="stylesheet" type="text/css" src="{{URL::to('admin-assets/css/daterangepicker.css')}}" />
<script>
  $('.date_input').daterangepicker({
    maxDate: "0"
})

</script>
 <script>  
    
$(function() {
 

    $('#datehere').daterangepicker({minDate: 0});
    // $('input[name="daterange"]').daterangepicker();
    $('.datepic').daterangepicker();

    $('.datepic').on('apply.daterangepicker', function(ev, picker) {
     
     
      startDate = picker.startDate.format('YYYY-MM-DD');
      maxDate =picker.startDate;
      endDate = picker.endDate.format('YYYY-MM-DD');

      var base_url = "{{URL::to('/')}}"
      $.ajax({
          url:base_url+'/admin/get-sales',
          type:'POST',
          data:{
            "startDate": startDate,
            "endDate": endDate
          },
          success:function(data){
            $('.total_sales').html(data.totalSales);
            $('.total_online_sales').html(data.totalOnlineSales);
            $('.total_online_orders').html(data.totalOnlineOrders);
            $('.total_offline_sales').html(data.totalOfflineSales);
            $('.total_offline_orders').html(data.totalOfflineOrders);
          }
      });
    });

});


</script>

      </body>
    </html>
