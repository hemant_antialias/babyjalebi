@extends('admin.admin_layout')


@section('content')

	<div class='row'>
     <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Staff List</h3>
                    <div style="float: right;">
                     </div>
                     <br><br>
                    <!-- Minimum price: -->
     			<div class="order-data">
                        <table id="orders-dynamic-table" class="table table-striped table-bordered table-hover" data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Role</th>
                                </tr>
                            </thead>
                            
                            <tbody class="product-inventory">

                            @foreach($staffs as $staff)
                                <tr data-id="{{$staff->id}}">
                                    <td>
                                        {{$staff->name}}
                                    </td>
                                    <td>{{$staff->email}}</td>
                                    <td>{{$staff->contact}}</td>
                                    <td>{{$staff->display_name}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

			</div>
		</div>

    </div>
</div>

@endsection