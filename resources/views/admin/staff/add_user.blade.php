@extends('admin.admin_layout')


@section('content')

<style>
	.vendor-img {
    	height: 123px; 
    	width: 111px; 
}
.addusrprm{
	height: 20px;
	width: 100%;
}
.addusrprminput{
	float: left;
	width: 10%;
}
.addusrprmname{
width: 34%;
}
</style>

	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Admin User</a>
					</li>
					<li class="active">Add User</li>
				</ul>

			</div>
			<br>
			<div class="row addusrwrapper">
				<div class="col-xs-12 ">
						{{ Form::open(['url' => 'admin/user/new', 'method' => 'post', 'class' => 'form-horizontal']) }}
						<div class="form-group col-md-5 newuserpadding">
							<div>
							<label for="form-field-1">Name </label>
								<input type="text" name="name" id="form-field-1" placeholder="Name" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						
						<div class="form-group col-md-6 newuserpadding">
							<div>
							<label for="form-field-1">Email</label>
								<input type="text" name="email" id="form-field-1" placeholder="Email" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-5 newuserpadding">
							<div>
							<label for="form-field-1">Phone</label>
								<input type="text" name="phone" id="form-field-1" placeholder="Phone" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="newusrmargin col-md-6 block-background newuserpadding">
						<div class="form-group col-sm-6" style="margin-right: 3%;">
                            <label>Roles</label>
                            <hr class="hrline">
							@foreach($roles as $role)
								<div class="addusrprm">
									 <input class="addusrprminput" type="checkbox" name="roles[]" value="{{$role->id}}" id="form-field-1" placeholder="Phone" class="col-xs-10 col-sm-12" />
									 <span class="addusrprmname">{{$role->display_name}}</span>
								</div>
							@endforeach

							
						</div>
						</div>

						<div class="form-group">
							<div class="col-md-2 subbtnpad">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
						
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>


@endsection