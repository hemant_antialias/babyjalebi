@extends('admin.admin_layout')
<link rel="stylesheet" type="text/css" src="{{URL::to('admin-assets/css/daterangepicker.css')}}" />
	<style type="text/css">
		.salesbymonth{
				padding: 16px;
				background: white;
				}
		/*.righthere{ 
			float: right;
		  }*/
		  .reportheadingright{
		  	float: left;
		  	margin-right: 10px;
		     color: #d8920c !important;
   			 background-color: #FFF;
   			 border: 1px solid #d8920c;
		  }
		  .tablebodyreport{
		  	font-size: 13px;
		  }
		  .rowreport{ background: white; margin-top: 9.5%; }
		  .wh{ background: white !important; }
		  .reportbtn{
		  	padding: 4px !important;
		    margin-top: 0px !important;
		    padding-left: 20px !important;
		    padding-right: 20px !important;
		    background: transparent !important;
		    
		    border: 1px solid #d8920c !important;
		    border-radius: 7px !important;
		  }
		  #reportscol{ color: #9e9999; }
		  .fa-caret-up{ margin-left: 4px; }
      .downro{ background: white; padding-top: 20px; padding-bottom: 20px; padding-left: 10px; margin: .5%; }
      #downtabl td{ width: 11.5%; }
      #downtabltotal{ font-size: 17px; font-weight: bolder; }
      .addwi{ width: 47% !important; }
      .inputdatepic{ background: #d8920c !important; color: white !important; border-color: #d8920c; }
      <style type="text/css">
    @media only screen and ( max-width: 720px){
      #dashcolumn{
        margin-top: 20% !important ;
      }
    }



    .daterangepicker.ltr {
    direction: ltr;
    text-align: left;
        top: 175.203px;
    left: 771px;
    right: auto;
    display: block;
    width: 39%;
    display: none;
}
.viewl{
margin-left: 14%;
text-transform: uppercase;
}
.lowoninventory{
  float: left;
  width: 70%;
}
.applyBtn{
  background: #d8920c !important;
  border: black !important;
}
.applyBtn:hover {
  background: black !important;
  border: black !important;
}
.cancelBtn{
  background: black !important;
  border: black !important;
}
.ranges{ float: right !important; }

      ul{
        list-style-type: none;
      }
      .property_image {
        height: 170px;
      }
      .background-layout
      {
        padding: 2%;
        background-color: #ffffff;
        border-radius: 3px;
        box-shadow: 0 2px 4px rgba(0,0,0,0.1);
      }
      .padding-lft
      {
        margin-right: 3%;
      }
      .ace-nav>li>a>.ace-icon {
    display: inline-block;
    font-size: 32px;
    color: #FFF;
    text-align: center;
    width: 33px;
    margin-top: 7px;
}
.ace-nav>li.light-blue>a {
    background-color: rgba(61, 61, 61, 0.05);
}
/*.navbar-default {
position: fixed !important;}*/
.navbar {
    margin: 0;
    padding-left: 0;
    padding-right: 0;
    border-width: 0;
    border-radius: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    min-height: 45px;
    background: #d8920c;
    position: absolute;
    z-index: 999;
    width: 100%;
}
.ace-nav>li>a>.ace-icon {
    display: inline-block;
    font-size: 32px;
    color: #191717;
    text-align: center;
    width: 33px;
    margin-top: 7px;
}
b, strong {
    font-weight: initial !important;
}
    </style>



@section('content')

<div class="row salesbymonth">
	<div class=" col-md-3">
		<h4><span id="reportscol"> Reports /</span> Sales by month </h4> 
	</div>
	<div class="col-md-5 col-md-offset-4">
		
			  <div class="reportheadingright input-group addwi" id="datehere">
                              <span class="inputdatepic input-group-addon">
                                <i class="fa fa-calendar bigger-110">
                                </i>
                              </span>
                             <input type="text" name="daterange" value=""  />
         </div>
        

	
		<button type="button" class="reportbtn btn btn-primary" style="background: transparent !important;color: #d8920c !important;" onclick="myPrintFunction()" >Print</button>
		<button type="button" class="reportbtn btn btn-primary" style="background: transparent !important;color: #d8920c !important;  ">Export</button>
	</div><!--col-md-5 ends here -->
</div> <!--row ends here -->

<div class="row rowreport">
	<table class="table" >
    <thead>
      <tr class="wh">
        <th>Month <i class="fa fa-caret-up" aria-hidden="true"></i></th>
        <th>Orders</th>
        <th>Gross Sales</th>
        <th>Discounts</th>
        <th>Returns</th>
        <th>Net Sales</th>
        <th>Shipping</th>
        <th>Tax</th>
        <th>Total Sales</th>
      </tr>
    </thead>
    <tbody class="tablebodyreport">
      <tr>
        <td>March 2016</td>
        <td>51</td>
        <td>Rs. 88,120.00</td>
        <td>Rs. -6,295.25</td>
        <td>Rs. 1,265.00 </td>
        <td>Rs. 83,000.00</td>
        <td>Rs. 800.00</td>
        <td>Rs. 5,012.61</td>
        <td>Rs. 88,902.46</td>
      </tr>
      <tr>
        <td>March 2016</td>
        <td>51</td>
        <td>Rs. 88,120.00</td>
        <td>Rs. -6,295.25</td>
        <td>Rs. 1,265.00 </td>
        <td>Rs. 83,000.00</td>
        <td>Rs. 800.00</td>
        <td>Rs. 5,012.61</td>
        <td>Rs. 88,902.46</td>
      </tr>
     <tr>
        <td>March 2016</td>
        <td>51</td>
        <td>Rs. 88,120.00</td>
        <td>Rs. -6,295.25</td>
        <td>Rs. 1,265.00 </td>
        <td>Rs. 83,000.00</td>
        <td>Rs. 800.00</td>
        <td>Rs. 5,012.61</td>
        <td>Rs. 88,902.46</td>
      </tr>
      <tr>
        <td>March 2016</td>
        <td>51</td>
        <td>Rs. 88,120.00</td>
        <td>Rs. -6,295.25</td>
        <td>Rs. 1,265.00 </td>
        <td>Rs. 83,000.00</td>
        <td>Rs. 800.00</td>
        <td>Rs. 5,012.61</td>
        <td>Rs. 88,902.46</td>
      </tr>
    </tbody>
  </table>
  
</div> <!--row ends here -->
<div class="downro row">
  <table id="downtabl">
    <tbody class="tablebodyreport">
      <tr>
        <td id="downtabltotal">Total</td>
        <td>6,651</td>
        <td>Rs. 88,120.00</td>
        <td>Rs. -6,295.25</td>
        <td>Rs. 1,265.00 </td>
        <td>Rs. 83,000.00</td>
        <td>Rs. 800.00</td>
        <td>Rs. 5,012.61</td>
        <td>Rs. 88,902.46</td>
      </tr>
    </tbody>
  </table>
</div>

 
      <!-- <script type="text/javascript" src="{{URL::to('admin-assets/js/jquery-3.1.1.min.js')}}"></script> -->


      <!-- <script type="text/javascript" src="{{URL::to('admin-assets/js/jquery-3.1.1.min.js')}}"></script> -->
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"> </script> -->
      <script type="text/javascript" src="{{URL::to('admin-assets/js/moment.min.js')}}"></script>
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 -->
    
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="{{URL::to('admin-assets/js/daterangepicker.js')}}"></script>
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script> -->

<script type="text/javascript">


$(function() {
 

    $('#datehere').daterangepicker();
    $('input[name="daterange"]').daterangepicker();
    $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {

   
     
      startDate = picker.startDate.format('YYYY-MM-DD');
      endDate = picker.endDate.format('YYYY-MM-DD');

      var base_url = "{{URL::to('/')}}"
      $.ajax({
          url:base_url+'/admin/get-sales',
          type:'POST',
          data:{
            "startDate": startDate,
            "endDate": endDate
          },
          success:function(data){
            $('.total_sales').html(data.totalSales);
            $('.total_online_sales').html(data.totalOnlineSales);
            $('.total_online_orders').html(data.totalOnlineOrders);
            $('.total_offline_sales').html(data.totalOfflineSales);
            $('.total_offline_orders').html(data.totalOfflineOrders);
          }
      });
    });

});


</script>
<script>
function myPrintFunction() {
    window.print();
}
</script>
@endsection 



  

