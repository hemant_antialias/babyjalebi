@extends('admin.admin_layout')
<?php 
use App\Vendor;
?>
<style type="text/css">
  .salesbymonth{
    padding: 16px;
    background: white;
  }
  /*.righthere{ 
  float: right;
  }*/
  .reportheadingright{
    float: left;
    margin-right: 10px;
    color: #d8920c !important;
    background-color: #FFF;
    border: 1px solid #d8920c;
  }
  .tablebodyreport{
    font-size: 13px;
  }
  .rowreport{
    background: white;
    margin-top: 2.5%;
  }
  .wh{
    background: white !important;
  }
  .reportbtn{
    padding: 4px !important;
    margin-top: 0px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    background: transparent !important;
    border: 1px solid #d8920c !important;
    border-radius: 7px !important;
  }
  #reportscol{
    color: #9e9999;
  }
  .fa-caret-up{
    margin-left: 4px;
  }
  .downro{
    background: white;
    padding-top: 20px;
    padding-bottom: 20px;
    padding-left: 10px;
    margin: .5%;
  }
  #downtabl td{
    width: 11.5%;
  }
  #downtabltotal{
    font-size: 17px;
    font-weight: bolder;
  }
  .addwi{
    width: 47% !important;
  }
  .inputdatepic{
    background: #d8920c !important;
    color: white !important;
    border-color: #d8920c;
  }
  <style>
  @media only screen and ( max-width: 720px){
    #dashcolumn{
      margin-top: 20% !important ;
    }
  }
  .daterangepicker.ltr {
    direction: ltr;
    text-align: left;
    top: 175.203px;
    left: 771px;
    right: auto;
    display: block;
    width: 39%;
    display: none;
  }
  .viewl{
    margin-left: 14%;
    text-transform: uppercase;
  }
  .lowoninventory{
    float: left;
    width: 70%;
  }
  .applyBtn{
    background: #d8920c !important;
    border: black !important;
  }
  .applyBtn:hover {
    background: black !important;
    border: black !important;
  }
  .cancelBtn{
    background: black !important;
    border: black !important;
  }
  .ranges{
    float: right !important;
  }
  ul{
    list-style-type: none;
  }
  .property_image {
    height: 170px;
  }
  .background-layout
  {
    padding: 2%;
    background-color: #ffffff;
    border-radius: 3px;
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
  }
  .padding-lft
  {
    margin-right: 3%;
  }
  .ace-nav>li>a>.ace-icon {
    display: inline-block;
    font-size: 32px;
    color: #FFF;
    text-align: center;
    width: 33px;
    margin-top: 7px;
  }
  .ace-nav>li.light-blue>a {
    background-color: rgba(61, 61, 61, 0.05);
  }
  /*.navbar-default {
  position: fixed !important;}*/
  .navbar {
    margin: 0;
    padding-left: 0;
    padding-right: 0;
    border-width: 0;
    border-radius: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    min-height: 45px;
    background: #d8920c;
    position: absolute;
    z-index: 999;
    width: 100%;
  }
  .ace-nav>li>a>.ace-icon {
    display: inline-block;
    font-size: 32px;
    color: #191717;
    text-align: center;
    width: 33px;
    margin-top: 7px;
  }
  b, strong {
    font-weight: initial !important;
  }
</style>
</style>
@section('content')
<div class="row salesbymonth">
  <div class=" col-md-5">
    <h4>
      <span id="reportscol"> Reports 
      </span>
      <br>
      <br> @if($report == 'customers')
      These are the highest spending customers
      @endif
      @if($report == 'discountedproduct')
      These products are on discount
      @endif
      @if($report == 'inventory')
      These products are low on inventory
      @endif
      @if($report == 'top_selling_product')
      These are the top Selling Products
      @endif
      @if($report == 'most_added_to_cart')
      Products most added to the cart
      @endif
      @if($report == 'sales_monthly')
      Sales by Month
      @endif
      @if($report == 'sales_by_product')
      Sales by Product
      @endif
      @if($report == 'sales')
      Sales
      @endif
      @if($report == 'salesdescounted')
      Sales with discount
      @endif

    </h4> 
  </div>
 <!--  <div class="col-md-5 col-md-offset-2">
    <div class="reportheadingright input-group addwi" id="datehere">
      <span class="inputdatepic input-group-addon">
        <i class="fa fa-calendar bigger-110">
        </i>
      </span>
      <input type="text" name="daterange" value=""  />
    </div>
    <button type="button" class="reportbtn btn btn-primary" onclick="myPrintFunction();" style="background: transparent !important;color: #d8920c !important;  ">Print
    </button>
    <button type="button" class="reportbtn btn btn-primary" style="background: transparent !important;color: #d8920c !important;  ">Export
    </button>
  </div> -->
  <!--col-md-5 ends here -->
</div> 
<!--row ends here -->
<div class="row rowreport">
  <div class="inventory-data">
    <table id="product-dynamic-table" class="table table-striped table-bordered table-hover" data-page-length='50'>
      @if($report == 'inventory')
      <thead>
        <tr class="wh">
          <th>
            <i class="">
            </i>Product image
          </th>
          <th>
            <i class="">
            </i>Product name
          </th>
          <th class="hidden-480">
            <i class="">
            </i>Total Price
          </th>
          <th>
            <i class="">
            </i> Quantity
          </th>
        </tr>
      </thead>
      <tbody class="tablebodyreport">
        @foreach($low_inventory as $inventory)
        <tr>
          <td style="width: 12%;">
            <img title="{{$inventory->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$inventory->id.'/featured_images/'.$inventory->product_images)}}" alt="{{$inventory->product_title}}" style="width: 40px; height: 40px; ">
          </td>
          <td>
            <a href="{{URL::to('admin/product/edit/'.$inventory->id)}}"> {{$inventory->product_title}}
            </a>
          </td>
          <td class="hidden-480">
            <span class="">Rs. {{$inventory->price}}
            </span>
          </td>
          <td>
            <b class="">{{$inventory->quantity}}
            </b>
          </td>
        </tr>
        @endforeach
        @endif
        <!-- this is for discounted product list -->
        @if($report == 'discountedproduct')
        <thead>
          <tr class="wh">
            <th>
              <i class="">
              </i>Product image
            </th>
            <th>
              <i class="">
              </i>Product name
            </th>
            <th class="hidden-480">
              <i class="">
              </i>Total Price
            </th>
            <th>
              <i class="">
              </i>Discounted Price
            </th>
          </tr>
        </thead>
      <tbody class="tablebodyreport">
        @foreach($discountedproduct as $discountproduct)
        <tr>
          <td style="width: 12%;">
            <img title="{{$discountproduct->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$discountproduct->id.'/featured_images/'.$discountproduct->product_images)}}" alt="{{$discountproduct->product_title}}" style="width: 40px; height: 40px; ">
          </td>
          <td>
            <a href="{{URL::to('admin/product/edit/'.$discountproduct->id)}}"> {{$discountproduct->product_title}}
            </a>
          </td>
          <td class="hidden-480">
            <span class="">Rs. {{$discountproduct->compare_price}}
            </span>
          </td>
          <td>
            <b class="">Rs. {{$discountproduct->price}}
            </b>
          </td>
        </tr>
        @endforeach
        @endif
        <!-- this is for top cart product list -->
        @if($report == 'top_cart_product')
        <thead class="thin-border-bottom">
          <tr>
            <th>
              <i class="">
              </i>Product image
            </th>
            <th>
              <i class="">
              </i>Product name
            </th>
            <th class="hidden-480">
              <i class="">
              </i>Total Price
            </th>
            <th>
              <i class="">
              </i>Number
            </th>
          </tr>
        </thead>
      <tbody>
        @foreach($top_cart_product as $product)
        <tr>
          <td style="width: 12%;">
            <img title="{{$product->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" style="width: 40px; height: 40px; ">
          </td>
          <td>
            <a href="{{URL::to('admin/product/edit/'.$product->id)}}"> {{$product->product_title}}
            </a>
          </td>
          <td class="hidden-480">
            <span class="">Rs. {{$product->price}}
            </span>
          </td>
          <td>
            <b class="">{{$product->count}}
            </b>
          </td>
        </tr>
        @endforeach
        @endif
        <!-- These are the highest spending customers -->
        @if($report == 'customers')
        <thead class="thin-border-bottom">
          <tr>
            <th>
              <i class="">
              </i>Customer
            </th>
            <th>
              <i class="">
              </i>Last Order
            </th>
            <th class="hidden-480">
              <i class="">
              </i>Total Orders
            </th>
            <th>
              <i class="">
              </i>Lifetime Spent
            </th>
          </tr>
        </thead>
      <tbody>
        @foreach($customer_list as $customerlist)
        <?php
          // var_dump($customerlist);
          $customer_list1 = DB::table('users')
          ->select('id', 'first_name', 'last_name', 'location')
          ->where('id' , '=' , $customerlist->user_id)
          ->first();  
          if ($customer_list1 == null) {
          continue;
          }
          $last_order = DB::table('order_request')
          ->select('created_at')
          ->orderBy('id', 'desc')
          ->where('user_id', '=', $customerlist->user_id)
          ->first();
          ?>
        <tr>
          <td>
            <a href="{{URL::to('/admin/customers/'.$customer_list1->id)}}">{{$customer_list1->first_name}}
            </a>
          </td>
          <td>
            {{$last_order->created_at}}
          </td>
          <td>
            {{$customerlist->total_order}}
          </td>
          <td>
            Rs.{{$customerlist->total_amount}}
          </td>
        </tr>
        @endforeach
        @endif
        <!-- These are the top selling product -->
        @if($report == 'top_selling_product')
        <thead class="thin-border-bottom">
          <tr>
            <th>
              <i class="">
              </i>Image
            </th>
            <th>
              <i class="">
              </i>name
            </th>
            <th>
              <i class="">
              </i>Number
            </th>
            <th class="hidden-480">
              <i class="">
              </i>Total Price
            </th>
          </tr>
        </thead>
      <tbody>
        @foreach($top_selling_product as $product)
        <tr>
          <td>
            <img title="{{$product->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$product->product_id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" style="width: 50px">
          </td>
          <td>
            <a href="{{URL::to('admin/product/edit/'.$product->product_id)}}"> {{$product->product_title}}
            </a>
          </td>
          <td>
            <b class="">{{$product->total_product_record}}
            </b>
          </td>
          <td class="hidden-480">
            <?php 
              $amount = $product->total_product_record * $product->price;
              ?>
            <span class="">Rs. 
              <?php echo $amount; ?>
            </span>
          </td>
        </tr>
        @endforeach
        @endif
        <!-- These products are the most added to cart -->
        @if($report == 'most_added_to_cart')
        <thead class="thin-border-bottom">
          <tr>
            <th>
              <i class="">
              </i>Image
            </th>
            <th>
              <i class="">
              </i>name
            </th>
            <th>
              <i class="">
              </i>Number
            </th>
            <th class="hidden-480">
              <i class="">
              </i>Total Price
            </th>
          </tr>
        </thead>
      <tbody>
        @foreach($top_cart_product as $product)
        <tr>
          <td>
            <img title="{{$product->product_title}}" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images)}}" alt="{{$product->product_title}}" style="width: 50px">
          </td>
          <td>
            <a href="{{URL::to('admin/product/edit/'.$product->id)}}"> {{$product->product_title}}
            </a>
          </td>
          <td>
            <b class="">{{$product->count}}
            </b>
          </td>
          <td class="hidden-480">
            <?php 
              $amount = $product->count * $product->price;
              ?>
            <span class="">Rs. 
              <?php echo $amount; ?>
            </span>
          </td>
        </tr>
        @endforeach
        @endif
        <!-- Sales by monthly -->
        @if($report == 'sales_monthly')
        
        <thead class="thin-border-bottom">
          <tr>
           <th>Month <i class="fa fa-caret-up" aria-hidden="true"></i></th>
        <th>Orders</th>
        <th>Gross Sales</th>
        <th>Discounts</th>
        <th>Returns</th>
        <th>Net Sales</th>
        <th>Shipping</th>
        <th>Tax</th>
        <th>Total Sales</th>
          </tr>
        </thead>
      <tbody>
        @foreach($sales_monthly as $monthlysales)

        <?php
                            $month = $monthlysales->created_at;
                             $newDateTime = date("F", strtotime($month));
                            // dd($month);
                            $year = date("y",strtotime($monthlysales->created_at));
                           // $year = date("Y"); 


                            // dd($year);
        $all_compeletd_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as gross_amount, SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                    ->whereRaw('extract(month from created_at) = ?', [$month]);
                    if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_compeletd_records_qeury->where('vendor_id', $vendor->id);

           }
                    $all_compeletd_records = $all_compeletd_records_qeury->where('order_status','=', 4)
                    ->distinct()->first();
                    // var_dump($all_records);
                    // echo $all_records->gross_amount;
                    // echo "<br>";
                    // echo $all_records->net_sale;
                    // echo $all_records->net_sale;
                    // echo $all_records->net_sale;
       $all_return_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount , SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                    ->whereRaw('extract(month from created_at) = ?', [$month]);
            if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_return_records_qeury->where('vendor_id', $vendor->id);

           }
          $all_return_records = $all_return_records_qeury->where('order_status','=',5)
                    ->distinct()->first();

        $all_records= $all_compeletd_records->gross_amount + $all_return_records->return_amount;
            
        $totalorder = $all_compeletd_records->total_order + $all_return_records->total_order;
       // dd($totalorder);

        $netsale= $all_records - $all_compeletd_records->discount - $all_return_records->return_amount;
        
        $totalsale = $netsale + $all_compeletd_records->shippingrate + $all_compeletd_records->tax;     
        // echo $year;
        ?>

       <span class="total_sales"></span>
       <?php 
       $monthNum = $month;
         $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
        $monthName;
        $yr = date("Y"); 
        ?>
        <td><?php echo $monthName."&nbsp".$yr; ?></td>
       <td>{{$totalorder}}</td>
        <td>Rs. {{$all_records}}</td>
          @if($all_compeletd_records->discount == '0' || $all_compeletd_records->discount == NULL )
         <td>Rs. 0.00</td>
        @else
        <td>Rs. -{{$all_compeletd_records->discount}}</td>
        @endif
        @if($all_return_records->return_amount == '0' || $all_return_records->return_amount == NULL )
         <td>Rs. 0.00</td>
        @else
        <td>Rs. -{{$all_return_records->return_amount}}</td>
        @endif
        <td>Rs. <?php echo round($netsale, 2) ?></td>
        @if($all_compeletd_records->shippingrate == '0' || $all_compeletd_records->shippingrate == NULL )
         <td>Rs. 0.00</td>
        @else
        
        <td>Rs. {{$all_compeletd_records->shippingrate}}</td>
        @endif
        <td>Rs. {{round($all_compeletd_records->tax, 2)}}</td>
        <td>Rs. <?php echo round($totalsale, 2) ?> </td>
        </tr>
        @endforeach
        @endif
         <!-- Sales by day -->
        @if($report == 'sales_day')
        
        <thead class="thin-border-bottom">
          <tr>
           <th>Date <i class="fa fa-caret-up" aria-hidden="true"></i></th>
        <th>Orders</th>
        <th>Gross Sales</th>
        <th>Discounts</th>
        <th>Returns</th>
        <th>Net Sales</th>
        <th>Shipping</th>
        <th>Tax</th>
        <th>Total Sales</th>
          </tr>
        </thead>
      <tbody>
       <label for="id-date-range-picker-1">Export Data By Date
                        </label>
                        <div class="row" style="margin-top: 0%;padding: 1%;">
                          <div class="col-md-4 col-sm-12" style="margin-top: 1.5%;">
                             
                            <div class="input-group" id="datehere">
                              <span class="input-group-addon" style="background: #02c9c6; color: white; border: #02c9c6;  " >
                                <i class="fa fa-calendar bigger-110 datepic">
                                </i>
                              </span>
                             <input type="text" name="daterange"  class="datepic" value="" style="width: 100%; color : #02c9c6;" />
                            
                            </div>
                          </div>
                          <div class="col-md-2">
                          <form action="{{URL::to('/admin/daywisereport-bydate')}}" method="post">
                        

                           <input type="hidden" name="startdate" value="" id="start_date">
                              <input type="hidden" name="enddate" value="" id="end_date">
                              
                           <input type="submit" value="Export Data"  class="btn btn-default" style="float: right;">
                           </form>

                        </div>
                        <div class="col-md-2" style="margin-top:15px;">
                         <a href="{{URL::to('daywise-records-csv')}}" style="float: right;"><button id="export1" class="btn btn-default exlport"> Export All Data</button></a>
                         </div>
                        
                        <br><br>
       
  @foreach($sales_day as $monthlysales)

        <?php
        
                             $month = $monthlysales->day;
                              $month1 = $monthlysales->mnth;
                              $newDateTime = date("M", strtotime($month1));
                            // dd($month1);
                            $year = date("y",strtotime($monthlysales->created_at));
                           // $year = date("Y"); 

                            // echo $year;
        $all_compeletd_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as gross_amount, SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                   ->whereRaw('extract(day from created_at) = ?', [$month])
                   ->whereRaw('extract(month from created_at) = ?', [$month1]);
          if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_compeletd_records_qeury->where('vendor_id', $vendor->id);

           }
            $all_compeletd_records = $all_compeletd_records_qeury->where('order_status','=', 4)->distinct()->distinct()->first();
                    
        $all_return_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount , SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                   ->whereRaw('extract(day from created_at) = ?', [$month])
                   ->whereRaw('extract(month from created_at) = ?', [$month1]);
        if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_return_records_qeury->where('vendor_id', $vendor->id);

           }
          $all_return_records = $all_return_records_qeury->where('order_status','=',5)
                    ->distinct()->first();

        $all_records= $all_compeletd_records->gross_amount + $all_return_records->return_amount;
            
        $totalorder = $all_compeletd_records->total_order + $all_return_records->total_order;

                    // dd($all_records->total_order);
       // $return_amount = DB::table('order_request')
       //              ->select('order_request.*', DB::raw('SUM(amount) as return_amount'))
       //              ->whereRaw('extract(day from created_at) = ?', [$month])
       //              ->whereRaw('extract(month from created_at) = ?', [$month1])
       //              ->where('order_status','=', 5)
       //              ->distinct()->first();
        $netsale= $all_records - $all_compeletd_records->discount - $all_return_records->return_amount;
        
        $totalsale = $netsale + $all_compeletd_records->shippingrate + $all_compeletd_records->tax;        
        // echo $year;
                    // dd($all_records);
        ?>

       <span class="total_sales"></span>

        
        <?php
         $monthNum = $month1;
         $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
        $monthName;
        // $yr = date("Y"); 
         ?>
       <td><?php  echo $month."&nbsp;" .$monthName ."&nbsp20".$year;
       ?></td>
        
        
                                        
      
        <td>{{$totalorder}}</td>
        <td>Rs. {{$all_records}}</td>
          @if($all_compeletd_records->discount == '0' || $all_compeletd_records->discount == NULL )
         <td>Rs. 0.00</td>
        @else
        <td>Rs. -{{$all_compeletd_records->discount}}</td>
        @endif
        @if($all_return_records->return_amount == '0' || $all_return_records->return_amount == NULL )
         <td>Rs. 0.00</td>
        @else
        <td>Rs. -{{$all_return_records->return_amount}}</td>
        @endif
        <td>Rs. <?php echo round($netsale, 2) ?></td>
        @if($all_compeletd_records->shippingrate == '0' || $all_compeletd_records->shippingrate == NULL )
         <td>Rs. 0.00</td>
        @else
        
        <td>Rs. {{$all_compeletd_records->shippingrate}}</td>
        @endif
        <td>Rs. {{round($all_compeletd_records->tax, 2)}}</td>
        <td>Rs. <?php echo round($totalsale, 2) ?> </td>
       <!--  <td>Rs. -6,295.25</td>
        <td>Rs. 1,265.00 </td>
        <td>Rs. 83,000.00</td>
        <td>Rs. 800.00</td>
        <td>Rs. 5,012.61</td>
        <td>Rs. 88,902.46</td> -->
        </tr>
        @endforeach 
        @endif
         <!-- Sales by product -->
        @if($report == 'sales_by_product')

        <thead class="thin-border-bottom">
          <tr>
           <th>Product Title <i class="fa fa-caret-up" aria-hidden="true"></i></th>
            <th>Price</th>
            <th>Net quantity</th>
            <th>Total Sales</th>
            <th>Store</th>
          </tr>
        </thead>
          <tbody>
          <div class="col-md-12">
                     <label for="id-date-range-picker-1">Export Data By Date
                        </label>
                        <div class="row" style=" margin-top: 2%; ">
                          <div class="col-md-4 col-sm-12" style="margin-top: 1.5%;">
                             
                            <div class="input-group" id="datehere">
                              <span class="input-group-addon" style="background: #d8920c; color: white; border: #d8920c;  " >
                                <i class="fa fa-calendar bigger-110 datepic">
                                </i>
                              </span>
                             <input type="text" name="daterange"  class="datepic" value="" style="width: 100%; color : #d8920d;" />
                            
                            </div>
                          </div>
                          <div class="col-md-2">
                          <form action="{{URL::to('/admin/product-bydate')}}" method="post">
                        

                           <input type="hidden" name="startdate" value="" id="start_date">
                              <input type="hidden" name="enddate" value="" id="end_date">
                              
                           <input type="submit" value="Export Data"  class="btn btn-default" style="float: right;">
                           </form>

                        </div>
                        <div class="col-md-6">
                         <a href="{{URL::to('/admin/product-alldata')}}" style="float: right;"><button id="export1" class="btn btn-default exlport"> Export All Data</button></a>
                         </div>
                        <hr>
                        <br><br>
      <?php 
      
           

                            ?>

        @foreach($order_id as $uid)
        <?php


        $total_gross = DB::table('order_detail')
                                ->select('product_id','order_id','vendor_id' , DB::raw('SUM(quantity) as total_quantity'))
                                ->where('product_id','=',$uid->product_id)
                                ->first();
                                // dd($total_gross);
                    // dd($total_gross->total_quantity);

                                if($total_gross->total_quantity == NULL)
                                  {
                                      continue;
                                  }
                                  

               $sales_by_product = DB::table('product')
                            ->select('product_title','price')
                            ->where('id','=', $total_gross->product_id)
                            ->first();

                $return_amount = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount'))
                    ->where('status', '=', 5)
                    ->distinct()->first();

            
        
        $totalsale = $total_gross->total_quantity * $sales_by_product->price;  
               
                       
        ?>
<td>{{$sales_by_product->product_title}}</td>
<td>{{$sales_by_product->price}}</td>
<td>{{$total_gross->total_quantity}}</td>
<td><?php echo $totalsale ; ?></td>
@if($total_gross->vendor_id == 1)
<td>South Delhi</td>
@else
<td>Gurugram</td>
@endif
        </tr>
        @endforeach
<span style="float: right;"> {{ $order_id->links() }}</span>
        @endif
        <!-- Finances Sales -->
        @if($report == 'sales')
        <thead class="thin-border-bottom">
          <tr>
           <th>Date <i class="fa fa-caret-up" aria-hidden="true"></i></th>
        <th>Order</th>
        <th>Gross Sales</th>
        <th>Discounts</th>
        <th>Returns</th>
        <th>Net Sales</th>
        <th>Taxes</th>
        <th>Shipping</th>
        <th>Total</th>
          </tr>
        </thead>
        <tfoot>
        <?php 
      $items = array();
      ?>
            
        </tfoot>
      <tbody>
      
        @foreach($sales as $monthlysales)
        
        <tr>
        <td>{{$monthlysales->created_at}}</td>
        <td>#{{$monthlysales->order_code}}</td>

        <td>Rs. {{$monthlysales->amount}}</td>
         @if($monthlysales->discount == 0)
         <td>--</td>
         @else
        <td>Rs. {{$monthlysales->discount}}</td>
        @endif
        <td></td>

        <?php 
       $netamont =  $monthlysales->amount  - $monthlysales->discount;
        ?>

        <td>Rs. <?php echo $netamont ?></td>
        @if($monthlysales->tax_rate_value)
        <td>Rs. {{$monthlysales->tax_rate_value}}</td>
        @else
        <td>--</td>
        
        @endif
        @if($monthlysales->custom_rate_value == NULL)
         <td>--</td>
         @else
        <td>Rs. {{$monthlysales->custom_rate_value}}</td>
        @endif
       
         <?php 
       $totalvalue =  $netamont + $monthlysales->tax_rate_value + $monthlysales->custom_rate_value;
        ?>
       <td>Rs. <?php echo  $totalvalue;
       array_push($items, $totalvalue);
        ?></td>
      
        </tr>
        @endforeach
      <tr>
                <th>Total</th>
                <th></th>
                <th>Rs. {{$total_gross->total_amount}}</th>
                <th>Rs. {{$total_discount->total_discount}}</th>
                <th></th>
                <?php $total_net_amount = $total_gross->total_amount - $total_discount->total_discount; ?>
                <th>Rs. <?php echo $total_net_amount?></th>
                <th>Rs. {{$total_discount->tax_rate}}</th>
                <th>Rs. {{$total_discount->custom_shipping}}</th>
                  <th>Rs. <?php echo array_sum($items);
        ?></th>
            </tr>
        @endif
      </tbody>
     <!-- Finances Sales -->
        @if($report == 'salesdescounted')
        <thead class="thin-border-bottom">
           <tr>
              <!-- <th></th> -->
              <th>Order ID</th>

              <!-- <th>Order</th> -->
              <th style=" width: 7%; " >Time</th>
              <th>Customer</th>
              <th>Email</th>
              @if(!isStore())
              <th>Store</th>
              @endif
              <th style="width:12%;" >Discount Coupon</th>
              <th>Discount</th>
              <th>Total</th>
          </tr>
        </thead>
        <tfoot>
        <?php 
      $items = array();
      ?>
            
        </tfoot>
      <tbody>
      <div class="col-md-12">
                     <label for="id-date-range-picker-1">Export Data By Date
                        </label>
                        <div class="row" style=" margin-top: 2%; ">
                          <div class="col-md-4 col-sm-12" style="margin-top: 1.5%;">
                             
                            <div class="input-group" id="datehere">
                              <span class="input-group-addon" style="background: #d8920c; color: white; border: #d8920c;  " >
                                <i class="fa fa-calendar bigger-110 datepic">
                                </i>
                              </span>
                             <input type="text" name="daterange"  class="datepic" value="" style="width: 100%; color : #d8920d;" />
                            
                            </div>
                          </div>
                          <div class="col-md-2">
                          <form action="{{URL::to('/admin/discountreport-bydate')}}" method="post">
                        

                           <input type="hidden" name="startdate" value="" id="start_date">
                              <input type="hidden" name="enddate" value="" id="end_date">
                              
                           <input type="submit" value="Export Data"  class="btn btn-default" style="float: right;">
                           </form>

                        </div>
                        <div class="col-md-6">
                         <a href="{{URL::to('discounted-records-csv')}}" style="float: right;"><button id="export1" class="btn btn-default exlport"> Export All Data</button></a>
                         </div>
                        <hr>
                        <br><br>
      
        @foreach($salesdescounted as $discountsale)
          <?php
          $users = DB::table('discount_coupon')->select('discount_coupon_code')->get();

          ?>

        
        
        
        <tr data-id="{{$discountsale->id}}">
                                   
                @if($discountsale->vendor_id == 1)
                <td>
                    <a href="{{URL::to('/admin/order/'.$discountsale->id)}}">D{{$discountsale->id}}</a>
                </td>
                @else
                <td>
                    <a href="{{URL::to('/admin/order/'.$discountsale->id)}}">G{{$discountsale->id}}</a>
                </td>
                @endif
                <td>

                    <?php
                    $month =$discountsale->created_at;

                    echo $newDateTime = date("jS F", strtotime($month));
                    echo "<br>";
                    echo $newDateTime1 = date('h:i A', strtotime($month));
                      ?>
                </td>
                <td>{{$discountsale->first_name}} {{$discountsale->last_name}}</td>
                <td>{{$discountsale->email}}</td>
                @if(!isStore())
                <td>{{$discountsale->store_name}}</td>
                @endif
                <td>
                {{$discountsale->coupon_code}}
                </td>
                <td>
                {{$discountsale->discount}}
                </td>
                <td>
                    Rs. {{round($discountsale->payble_amount, 2)}}
                </td>
            </tr>
        @endforeach
        <span style="float: right;"> {{ $salesdescounted->links() }}</span>
        @endif
        </tbody>

    </table>
  </div>
</div> 

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"> </script> -->
<script type="text/javascript" src="{{URL::to('admin-assets/js/jquery-3.1.1.min.js')}}"></script>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"> </script> -->
      <script type="text/javascript" src="{{URL::to('admin-assets/js/moment.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />

    
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="{{URL::to('admin-assets/js/daterangepicker.js')}}"></script>
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script> -->
<link rel="stylesheet" type="text/css" src="{{URL::to('admin-assets/css/daterangepicker.css')}}" />
<script>
  $('.date_input').daterangepicker({
    maxDate: "0"
})

</script>
 <script>  
    var j = jQuery.noConflict();
j(function() {
 

    // j('#datehere').daterangepicker({minDate: 0});
    // j('input[name="daterange"]').daterangepicker();
    j('.datepic').daterangepicker();

    j('.datepic').on('apply.daterangepicker', function(ev, picker) {
     
     
      startDate = picker.startDate.format('YYYY-MM-DD');
      maxDate =picker.startDate;
      endDate = picker.endDate.format('YYYY-MM-DD');
      j('#start_date').val(startDate);
      j('#end_date').val(endDate);
    });

});



</script>
<script>
  function myPrintFunction() {
    window.print();
  }
</script>
@endsection
