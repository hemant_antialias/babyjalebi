@extends('admin.admin_layout')
<style type="text/css">
	.orderright{
		float: right;
	}
	.orderleft{
		float: left;
	}
	.dascol1{
		    padding: 10px;
    /* padding-top: 10px; */
    background: white;
    margin-top: 40px;
    border: 1px solid gainsboro;
    border-radius: 4px;
    box-shadow: 0px 2px 2px 1px gainsboro;
	}
	.pstyling{
		    font-weight: bolder;
    font-size: 15px;
	}
	.pnumber{
		font-size: 25px;
		font-weight: bolder;
	}
	.hrok {
    margin-top: 5px;
    margin-bottom: 5px;
    border-top: 1px solid #eee;
}
.ramb { margin: 34px; }
.salesbymonth{ color: #d8920c }
.arm{ margin-top: 34px; }
.main-content-inner{
	background: #f6f8f8;
}
.reportheading{
	padding: 16px;
	
}
@media only screen and ( max-width: 620px)
{
	
}


</style>

@section('content')
<br><br>
<div class="reportheading">
	<h3>Reports</h3>
	<hr>
</div>
<div class="col-md-12  col-xs-12 "  style="background: #fff;">
  
	<div class="col-md-4" style="padding: 1%;">
<h4 style="font-weight: 600 !important;">SALES</h4>
<hr>
	<span class="salesbymonth" ><a href="{{URL::to('admin/report/sales_monthly')}}">
Sales by month</a></span> <br>
<span class="salesbymonth" ><a href="{{URL::to('admin/report/sales_day')}}">
Sales by day</a></span> <br>
		<span class="salesbymonth" ><a href="{{URL::to('admin/report/sales_by_product')}}">Sales by product</a></span><br>
		<span class="salesbymonth" ><a href="{{URL::to('admin/report/customers')}}">Sales by hour</a></span><br>
		<span class="discountedsale" ><a href="{{URL::to('admin/report/salesdescounted')}}">Sales by Discount</a></span><br>
		</div>
		

	
	<div class="col-md-4" style="padding: 1%;">
	<h4 style="font-weight: 600 !important;">PRODUCT</h4>
	<hr>
		<span class="salesbymonth" ><a href="{{URL::to('admin/report/inventory')}}"> Products low on inventory</a></span> <br>
		<span class="salesbymonth" ><a href="{{URL::to('admin/report/discountedproduct')}}"> Products on discount</a></span><br>
		<span class="salesbymonth" ><a href="{{URL::to('admin/report/top-selling-product')}}">Top Selling Products</a></span><br>
		<span class="salesbymonth" ><a href="">Products most added to the cart</a></span>

		
		</div>
<div class="col-md-4" style="padding: 1%;">
<h4 style="font-weight: 600 !important;">CUSTOMERS</h4>
<hr>
		<span class="salesbymonth" ><a href="{{URL::to('admin/report/customers')}}"> Highest spending customers</a></span><br>
		</div>
	
	







@endsection