@extends('admin.admin_layout')


@section('content')
<style type="text/css">
	ul{
		list-style-type: none;
	}
	.property_image {
		height: 170px;
	}
	.btn-file{
		padding: 2px;
	}
</style>

	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="{{URL::to('/admin/bannerlist')}}">Banner</a>
					</li>
					<li class="active">Add Banner</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row banner">
				<div class="col-xs-9 banner-div">

			{{ Form::open(['url' => 'admin/add-banner', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}

			<div class="form-group" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;">
				
			<div class="add-more-banner">
				<div class="col-sm-12">
				<label for="form-field-1"> Add Images </label>
					<input id="banner_images" name="banner_images[]" type="file" multiple class="file-loading">
					<label for="form-field-1"> Add Link </label>
					<input type="text" name="link" class="col-sm-12">
				</div>
				
				
				<div class="col-md-offset-8 col-md-4">
				<br>
			</div>
					<button class="btn btn-info add-more" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Add more
					</button>
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>
				</div>
			</div>

			<div class="form-group">
				
			</div>
			{{ Form::close() }}
			</div>
				
			</div>
		</div>
	</div>
@endsection

@section('scripts')

<script>
	$("#banner_images").fileinput({
	    // uploadUrl: "http://localhost/file-upload-single/1", // server upload action
	    uploadAsync: true,
	    showRemove: true,
	    maxFileCount: 20
	});

// $(document).ready(function(){
	var count = 2;

	$('.add-more').click(function(e){
		e.preventDefault();
		if(count < 5){
            $($('.add-more-banner').first().clone()).appendTo(".banner-div");
	        count++;
        }
	});
// });
</script>

@endsection