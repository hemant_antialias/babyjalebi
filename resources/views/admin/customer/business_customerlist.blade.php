@extends('admin.admin_layout')
<style type="text/css">
    .requird
    {
        color:red;
    }
    .table-bordered>tbody>tr>td{
        font-size: 13px;
    }
    .input-filter {
        width: 90px !important;
    }
    .rosearch input[type=text]{
width: 54px;
    }
    .input-filter input[type=text]{
        width: 100%;
    }
</style>
  
@section('content')
    <div class='row'>
     <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Customer List</h3>
                    <form method="get" action="{{URL::to('admin/customers')}}">
                        Search by Tags: <input type="text" name="customer_tags">
                        <input type="submit" value="Search" class="btn btn-default">
                          Search : <input type="text" name="data_from_table">
                        <input type="submit" value="Search" class="btn btn-default">
                    </form>
                    <div style="float: right;">
                    <button id="refresh" class="btn btn-default">Refresh</button>
                     <a href="{{URL::to('admin/customers/new')}}"><button id="createcustomer" class="btn btn-default"> Create Customer</button></a>
                      <a href="{{URL::to('all-customer-csv')}}"><button id="export1" class="btn btn-default exlport"> Export Customer</button></a>
                     </div>
                     <br><br>
                    <!-- Minimum price: -->
                <div class="order-data">
    <table id="customer-dynamic-table" data-page-length='50' style="width:100%" class="table  table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="center">ID
                    <label class="pos-rel">
                        <!-- <input type="checkbox" class="ace" /> -->
                        <span class="lbl" ></span>
                    </label>
                </th>
                <th>Customer Name </th>
                <th>Customer Email </th>
                <th>Customer Phone </th>

                <th class="rosearch">Orders</th>
                <!-- <th>Location</th> -->
                <th class="hidden-480 rosearch">Credits</th>
                <th class="hidden-480">Total Spent</th>
                <th></th>
            </tr>           
            <tr>
                <th class="center">
                    <label class="pos-rel">
                        <!-- <input type="checkbox" class="ace" /> -->
                        <span class="lbl" ></span>
                    </label>
                </th>
                <th class="input-filter">ID</th>
                <th class="input-filter">Customer Name</th>
                <th class="input-filter">Customer Email</th>
                <th class="input-filter">Customer Phone</th>

                <!-- <th class="input-filter">Location</th> -->
                <th class="hidden-480 input-filter">Orders</th>
                <th class="hidden-480 input-filter">Credits</th>
                <th class="input-filter"></th>
            </tr>
        </thead>
        
        <tbody>
        @foreach($users as $user)
        <?php
            $order = DB::table('order_request')
                        ->select(DB::raw('SUM(payble_amount) as total_amount, COUNT(id) as total_order'))
                        ->where('user_id', '=', $user->id)
                        ->where('status', '=', 2)
                        ->first();

            $last_order = DB::table('order_request')
                        ->select('order_code')
                        ->orderBy('id', 'desc')
                        ->where('user_id', '=', $user->id)
                        ->where('status', '=', 2)
                        ->first();


        ?>
            <tr>
<!--                 <td>
                   
                </td> -->
                <!-- <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace product-id" value="" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td> -->
                <td colspan="1"><a href="{{URL::to('/admin/customers/edit/'.$user->id)}}" >{{$user->id}}</a></td>

                <td><a href="{{URL::to('/admin/c1ustomers/'.$user->id)}}" >{{$user->first_name}} {{$user->last_name}}</a></td>
                <!--  -->
                <td>{{$user->email}}</td>
                <td>{{$user->contact}}</td>
                
                <td>{{$order->total_order}}</td>
                <td class="pqty"><input type="number" class="product_credits_{{$user->id}}" value="{{$user->credits}}"><button class="btn btn-primary" style=" padding: 0px; padding-left: 4px; padding-right: 4px; " onclick="updateCredits(<?php echo $user->id; ?>, this)">Save</button></td>
                
                @if($order->total_amount == NULL)
                <td class="pqty">Rs. 0.00</td>
                @else
                <td class="pqty">Rs. {{$order->total_amount}}</td>
                @endif
                <td><button class="btn btn-primary bootbox-confirm" style=" padding: 0px; padding-left: 4px; padding-right: 4px; " data-custid="{{$user->id}}">Delete</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
              </div>

            </div>
        </div>


    </div>
</div>
@endsection

@section('scripts')
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script>
$(document).ready(function() {
            
            $('.input-filter').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search" />' );
            } );


            var myTable = $('#customer-dynamic-table').DataTable({
                paging: true,
                sort: true,
                searching: true,
                select: {
                    style: 'multi'
                }
                // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });

                        // Apply the search
            myTable.columns().every( function () {
                var that = this;
                
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


                serachTextBox.on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachSelectBox.on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


            } );
            
            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });
            
        });

    </script>

    <script>
        $(".bootbox-confirm").on(ace.click_event, function() {

           var customer_id = $(this).attr('data-custid');
            var base_url = "{{URL::to('/')}}";
            bootbox.confirm("Are you sure?", function(result) {
                if(result) {
                    $.ajax({
                      url: base_url+'/admin/ajax/remove-customer',
                      type: "post",
                      data:
                      {
                        "customer_id": customer_id
                      },
                      success:function(data)
                      {
                        if (data.status == 0) {
                            alert(data.message);
                        }
                        if (data.status == 1) {
                            window.location.reload(true);
                        }
                      }
                  });
                }
            });
        });

        function updateCredits(id, obj)
        {
            var base_url = "{{URL::to('/')}}";
            var credits = $(".product_credits_"+id).val();
            $.ajax({
                  url: base_url+'/admin/business/user/update-credits',
                  type: "post",
                  data:
                  {
                    "customer_id": id,
                    "credits": credits
                  },
                  success:function(data)
                  {
                    if (data.status == 0) {
                        toastr.error(data.message, {timeOut: 100000})
                    }
                    if (data.status == 1) {
                        toastr.success(data.message, {timeOut: 100000})
                    }
                  }
              });
        }
    </script>
@endsection