@extends('admin.admin_layout')


@section('content')
<?php
  use Carbon\Carbon;
  use App\ProductImages;
?>
<style type="text/css">
	ul{
		list-style-type: none;
	}
	.property_image {
		height: 170px;
	}
	.background-layout
	{
		padding: 2%;
    background-color: #ffffff;
    border-radius: 3px;
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
   
	}
	.padding-lft
	{
		 margin-right: 3%;
	}
</style>
<!-- Modal for address change-->
<div id="alltagmodel" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height: 350px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Tags
        </h4>
      </div>
      <div class="modal-body">
        <h5><b>APPLIED TAGS</b></h5>

  <span>Select previously used tags from the list below to add them to this customer.</span>
        <br>
        <div class="hr hr32 hr-dotted"></div>
        <div class="form-group">
          <div class="col-sm-12">
          <ul>
          
          </ul>
          </div>
           <br><br>
        </div>
        
      </div>

      <div class="" style="float: right;padding: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="button" class="btn btn-default add-to-order">Apply
        </button>
      </div>
    </div>
  </div>
</div>
<!-- Modal for address change-->
<div id="changeaddress" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content editcustomer">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit shipping address
        </h4>
      </div>
      <div class="modal-body">
      <form id="customer-edit-shipping">
        <input type="hidden" class="user_id" name="id" value="{{$user->id}}" />

        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> First Name 
            </label>
            {{Form::text('first_name',$user->first_name, array('id' => 'first_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> Last Name 
            </label>
            {{Form::text('last_name',$user->last_name, array('id' => 'last_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
          <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Company
            </label>
            {{Form::text('Company',null, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('Company'))
            <span class="help-block">
              <strong>{{ $errors->first('Company') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Phone Number 
            </label>
            {{Form::text('contact',$user->contact, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('contact'))
            <span class="help-block">
              <strong>{{ $errors->first('contact') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 
            </label>
            {{Form::text('address',$user->address, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 2
            </label>
            {{Form::text('address2',$user->address2, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div >
            <label for="form-field-1"> Country 
            </label>
            {{Form::text('country',$user->country, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
          <label for="form-field-1"> City 
            </label>
            {{Form::text('city',$user->city, array('id' => 'city','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> State 
            </label>
            {{Form::text('state',$user->state, array('id' => 'state','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> Postal/zip code 
            </label>
            {{Form::text('zip',$user->zip, array('id' => 'zip','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
      </div>
      <br>
      <br>
      <div style="float: right;margin: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="submit" id="update-add" class="btn btn-default add-to-order">Apply
        </button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- for edit name and email -->
<div id="nameandmailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="height: 350px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit customer
        </h4>
      </div>
      <div class="modal-body">
        
        <br>
        <div class="form-group">
         
           <div class="col-sm-12">
           <br>
           <label for="form-field-1"> Email address 
          </label>
           <input type="hidden" class="user_id" name="id" value="{{$user->id}}" />
            {{Form::text('email',$user->email, array('id' => 'email','class' => 'col-xs-12 col-sm-12 user_email'))}}
            @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}
              </strong>
            </span>
            @endif
          </div>
           <br><br>
        </div>
        
      </div>

      <div class="" style="float: right;padding: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="button" class="btn btn-default change-email">Apply
        </button>
      </div>
    </div>
  </div>
</div>
<div class="main-content">
  <div class="main-content-inner">
      <div class="main-content">
        <div class="main-content-inner">
          <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
              <li>
                <i class="ace-icon fa fa-home home-icon">
                </i>
                <a href="#">Edit Customer
                </a>
              </li>
              <li class="active">Edit Customer
              </li>
            </ul>
            <!-- /.breadcrumb -->
          </div>
          <br>
          <div class="row">
            <form method="POST" action="http://localhost:8000/admin/product/new" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
              <input name="_token" type="hidden" value="CtzGitGxF9mxRmfqSWVT2BIrCYC6cw8Qx14cRyjO">
              <div class="col-md-8">
                <div class="col-xs-12" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;position: inherit;">
                  <div class="form-group">
                    
                    <div class="col-sm-6">
                      <h3> {{$user->first_name}} {{$user->last_name}}</h3>
                      <p> {{$user->city}} , IN</p>
                      <?php
                        $created = new Carbon($user->created_at);
                        $now = Carbon::now();
                        $difference = ($created->diff($now)->days < 1)
                            ? 'Today'
                            : $created->diffForHumans($now);
                        
                      ?>
                      <p> customer for {{$difference}}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12" for="form-field-1"> Customer Note 
                    </label>
                    <div class="col-sm-12">
                      <input type="text" name="notes" id="form-field-1" value="{{$user->notes}}" placeholder="Add a Note" class="col-xs-10 col-sm-12 customer-notes">
                      <br><br><button class="btn btn-primary submit_notes">Submit</button>
                    </div>
                  </div>
                  <div class="hr hr32 hr-dotted"></div>
                  @if(isset($order_list))
                  <div class="col-md-4" style="text-align: center;">
                  <a href="#"> Last order</a>
                  <?php
                      $created = new Carbon($last_order->created_at);
                      $now = Carbon::now();
                      $last_order_date = ($created->diff($now)->days < 1)
                          ? 'Today'
                          : $created->diffForHumans($now);
                      
                    ?>
                  <h3>{{$last_order_date}}</h3>
                  <!-- <p> # Imported via Draft Orders</p> -->
                  </div>
                  @endif
                  <div class="col-md-4" style="text-align: center;">
                  <p> Lifetime spent</p>
                  <h3>Rs. {{$order->total_amount}}</h3>
                  <p> {{$order->total_order}} orders</p>
                  </div>
                  <div class="col-md-4" style="text-align: center;">
                  <p> Average order</p>
                  <h3>Rs.<?php echo round($order->avg_amount);?></h3>
                  
                  </div>	
                </div>
                <br>
                <br>
                @if(isset($last_order))
                <div class="col-md-12" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;margin-top: 3%;">
                  <div class="form-group">
                    <label class="col-sm-12"> 
                      <b>Recent orders
                      </b> 
                    </label>
                    <br>
                    <br>
                    <div class="col-sm-9">
                      <label class="" for="form-field-1"> 
                      <a href="#">Order #{{$last_order->order_code}}</a><br>
					<p>Rs. {{$last_order->payble_amount}} </p>
                      </label>
                    @foreach($last_order_detail as $detail)
                    <?php
                      $product_image = ProductImages::where('product_id', '=', $detail->id)->select('images')->first();
                    ?>
                    <div class="col-md-9" style="display: inline-flex;">
                     <img title="{{$detail->product_title}}" style="width:100px; height: 100px;" class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$detail->id.'/featured_images/'.$detail->product_images)}}" alt="1 thumb">
                     <a href="#" style="padding: 9%;"> {{$detail->product_title}} </a>
                     </div>

                     @endforeach
                    </div>


                    <div class="col-sm-3">
                      <label class="" for="form-field-1"> @if(isset($last_order_date)){{$last_order_date}}@endif </label>
                      
                    </div>
                  </div>
                  <div class="hr hr32 hr-dotted"></div>
                 
                  <div class="form-group">
                    <div class="col-md-offset-9 col-md-3">
                    <a class="btn btn-info" href="{{URL::to('/admin/customers/all-orders/'.$user->id)}}">
                        <i class="ace-icon fa fa-check bigger-110">
                        </i>
                        View all Orders
                      </a>
                    </div>
                  </div>
                </div>
                @endif
              </div>
              <div class="col-md-4 background-layout">	
                <div class="form-group">
                  <label class="col-sm-12"> 
                    <b>Contacts  
                    </b> 
                     <span style="float: right;">  <a id="edit-email"> Edit
                  </label>
                  <br>
                  <br>
                
                 
                  <div class="col-sm-12">
                      @if(($user->email == '') || ($user->email == 'NULL'))
                       <p>Has an account</p>
                      @else
                        <a href="{{URL::to('/admin/customers/'.$user->id)}}">{{$user->first_name}} {{$user->last_name}}</a>
                        <br><br>
                        <a href="">{{$user->email}}</a>
                      @endif
                  </div>
                </div>
                 <div class="hr hr32 hr-dotted"></div>
                <div class="form-group">
                  <label class="col-sm-8" for="form-field-1"> DEFAULT ADDRESS 
                  </label>
                  <span class="col-sm-4" style="float: right;">  <a id="edit-add" style="float: right;"> Change</a></span>
                  <div class="col-sm-12">
                  <BR>
                    {{$user->first_name}} {{$user->last_name}}<BR>

					{{$user->caddress}}, {{$user->caddress2}},<BR>
					{{$user->ccity}} {{$user->czip}}, <BR>
					{{$user->cstate}} , {{'India'}} <BR>
					{{$user->contact}}<BR>
                  </div>
                </div>
                </div>
                <div class="col-md-4 background-layout" style="margin-top: 3%; ">	

                <div class="form-group">
                 <label class="col-sm-8" for="form-field-1"> Tags
                  </label>
                  <span class="col-sm-4" style="float: right;">  <a id="all-tags" style="float: right;"> View All Tags</a></span>

                  <?php 
                    $crtags = array_keys($ctags);
                  ?>
                  <div class="col-sm-12">
                    {{Form::select('customer_tag', $customer_tags,$crtags, array('id' => 'customer_tag','class' => 'col-xs-10 col-sm-12', 'multiple'))}}
                  </div>
                  <br>
                  @if(count($ctags) > 0)
                    <div class="tag_list">
                      @foreach($ctags as $tag => $value)
                        <li>{{$value}}</li>
                      @endforeach
                    </div>               
                  @endif   
                  <div class="customer_tag_list">
                  
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.page-content -->
</div>

<script>
  $('#edit-email').click(function(){
    $('#nameandmailModal').modal('show');
  }
                        );
  $('#edit-add').click(function(){
    $('#changeaddress').modal('show');
  }
                        );

$('#all-tags').click(function(){
    $('#alltagmodel').modal('show');
  }
                        );
$('#update-add').click(function(e){
    e.preventDefault();
    $('#changeaddress').modal('hide');
    $('.modal-backdrop.in').css('opacity','0');
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/add-customer',
        type:'POST',
        data:$("#customer-edit-shipping").serialize(),
        success:function(data){
          // $('#usrdata').html(data);
          $('.modal-content').modal('hide');
            $('.modal-backdrop.in').css('opacity','0');
          // $('.modal-backdrop.in').css('dispplay','none');
          $('.modal-backdrop.fade.in').css('position','inherit');
          toastr.success('Shipping address updated.', {timeOut: 100000})
        }
      });
    location.reload();
  });

  $('.change-email').click(function(e){
    e.preventDefault();
    $('#nameandmailModal').modal('hide');
    $('.modal-backdrop.in').css('opacity','0');
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/change-customer-email',
        type:'POST',
        data:{
          'id': $('.user_id').val(),
          'email': $('.user_email').val()
        },
        success:function(data){
          if (data.status == 0) {
            toastr.error(data.message, {timeOut: 100000})
          }
          if (data.status == 1) {
            $('.modal-dialog').css('dispplay' , 'none');
            $('.modal-backdrop.in').css('opacity','0');
            toastr.success('Email address updated.', {timeOut: 100000})
          }
        }
      });
  });



</script>

@stop

@section('scripts')
<script>
  var base_url = "{{URL::to('/')}}";
  $("#customer_tag").chosen({
    no_results_text: "Oops, nothing found!"}
                           ).change(function(e, params){
    var tag_id = $("#customer_tag").chosen().val();
    var cid = "{{$user->id}}";

    $.ajax({
      url: base_url+'/admin/customer/add-tags-to-customer',
      type: "post",
      data: 
      {
        "tag_ids": tag_id,
        "cid": cid,
      }
      ,
      success:function(data)
      {
        $('.tag_list').remove();
        $('.customer_tag_list').html(data);
      }
    });
  });

  $('.submit_notes').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    var notes = $('.customer-notes').val();
    var customer_id = "{{$user->id}}";

    $.ajax({
      url: base_url+'/admin/customer/add-notes',
      type: "post",
      data: 
      {
        "notes": notes,
        "customer_id": customer_id
      },
      success:function(data)
      {
        toastr.success(data.message, {timeOut: 10000});
      }
    });

  });
</script>
@endsection