@extends('admin.admin_layout')


@section('content')
<style type="text/css">
  #allordersamount{
    margin-left: 36%;
  }
  #allordersquant{ float: right;margin-right: 11%; }
	.orderdetail_products{
		margin-bottom: 10px;
	}
  .page-content{
    background-color: #ffffff !important;
  }
  .marginheretop{ margin-top: -113px; }
  .allorderheading{ text-align: center; }
  .somecolstyling{ border-bottom: 1px dashed gainsboro; margin-bottom: 15px; padding-bottom: 10px;}
  .page-content{ margin-top: 6%; }
  .imga{ padding: 10px; }
  .allprodimgname{ width: 100%; margin-bottom: 46px; text-align: center; }
</style>
<div class="allorderheading">
  <h1> All Orders</h1>
</div>
	
	@foreach($all_orders as $order)
		<?php
	        $order_detail = DB::table('order_detail')
	                    ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
	                    ->select('product.id','product.product_title', 'product.product_images')
	                    ->where('order_detail.order_id', $order->id)
	                    ->get();
		?>
		<div class="form-group">
            <!-- <label class="col-sm-12"> 
              <b>All orders
              </b> 
            </label> -->
            <br>
            <br>
            <div class="row somecolstyling">
              <div class="col-md-3">Orders</div>
              <div class="col-md-5">Products</div>
              <div class="col-md-2">Quantity</div>
              <div class="col-md-2">Price</div>
            </div>
          <div class="row">
            <label class="col-md-3 " for="form-field-1"> 
                 <a href="#">Order #{{$order->order_code}}</a><br>
                 <p>Rs. {{$order->payble_amount}} </p>
                 <label class="" for="form-field-1"> {{Carbon\Carbon::parse($order->created_at)->diffForHumans() }} </label>
              </label>
              
              <div class="col-md-3 ">
                 @foreach($order_detail as $detail)
                  <div class="allprodimgname">
                        <img title="{{$detail->product_title}}" style="width:50px; height: 50px; float: left; margin-right: 10px; " class="block aspect-ratio__content" src="{{URL::to('/product_images/'.$detail->id.'/featured_images/'.$detail->product_images)}}" alt="1 thumb">
                         <a class="imga" href="#" > {{$detail->product_title}} </a> 
                  </div>
                    @endforeach
              </div>

             
               <div class="col-md-3"> 

                 <p id="allordersquant">4 </p>
              </div>
               

              <div class="col-md-3"> 
                 <p id="allordersamount">Rs. {{$order->payble_amount}} </p>
              </div>
          </div><!--row ends here -->


           
     </div>

	@endforeach
	
@endsection