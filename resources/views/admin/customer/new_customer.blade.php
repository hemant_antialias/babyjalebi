@extends('admin.admin_layout')


@section('content')

	
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="{{URL::to('/admin/customers')}}">Customer</a>
					</li>
					<li class="active">Add Customer</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/customers/new', 'method' => 'post', 'class' => 'form-horizontal']) }}
						<div class="row">
						<div class="col-md-4">
						<h2>Customer overview</h2>
						</div>
						<div class="col-md-8 block-background bottomspace">
					<!-- 	<div class="form-group col-sm-12" style="margin-right: 3%;">
							<div class="">
							<label for="form-field-1"> Store </label>
    							{{  Form::select('store_type', $stores,null,['id' => 'store_type', 'class' => 'form-control']) }}
							    @if ($errors->has('store_type'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('store_type') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
 -->						
						<div class="form-group col-sm-6" style="margin-right: 3%;">
							<div class="">
							<label for="form-field-1"> First Name </label>
								<input type="text" name="first_name" id="form-field-1" placeholder="First Name" class="col-xs-10 col-sm-12" />
							    @if ($errors->has('first_name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('first_name') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
						<div class="form-group col-sm-6">
							<div class="">
							<label class="" for="form-field-1"> Last Name </label>
								<input type="text" name="last_name" id="form-field-1" placeholder="Last Name" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
							<label> Email </label>
								<input type="text" name="email" id="form-field-1" placeholder="Email" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						</div>
						<!-- <div class="form-group">
							<div class="checkbox">
								<label>
									<input name="is_accept_marketing" type="checkbox" class="ace ace-checkbox-2" />
									<span class="lbl"> Customer accepts marketing</span>
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<input name="is_tax_exempt" type="checkbox" class="ace ace-checkbox-2" />
									 <span class="lbl"> Customer is tax exempt</span>
								</label>
							</div>
						</div> --> 
						</div>
						</div>

						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h2>Address</h2>
						<p>The primary address of this customer.</p>
						</div>
						<div class="col-md-8 block-background">
						<div class="form-group col-md-6" style="margin-right: 3%;">
							<div>
							<label for="form-field-1"> Company </label>
								<input type="text" name="company" id="form-field-1" placeholder="Company" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label  for="form-field-1"> Phone </label>
								<input type="text" name="contact" id="form-field-1" placeholder="Phone" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
							<label> Address Line 1</label>
								<input type="text" name="address" id="form-field-1" placeholder="Address" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
							<label for="form-field-1"> Address  Line 2</label>
								<input type="text" name="address_cont" id="form-field-1" placeholder="Address Con't" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6" style="margin-right: 3%;">
							<div>
							<label for="form-field-1"> City </label>
								<input type="text" name="city" id="form-field-1" placeholder="City" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label for="form-field-1"> Pin Code </label>
								<input type="text" name="zip" id="form-field-1" placeholder="Pin Code" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6" style="margin-right: 3%;">
							<div>
							<label for="form-field-1"> State </label>
								<input type="text" name="state" id="form-field-1" placeholder="State" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label for="form-field-1"> Country </label>
								<input type="text" name="country" id="form-field-1" placeholder="Country" class="col-xs-10 col-sm-12" />
							</div>
						</div>
						</div>
						</div>
						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h2>Notes</h2>
						<p>Enter any extra notes relating to this customer.</p>
						</div>
						<div class="col-md-8 block-background">

						<div class="form-group">
							<div class="col-sm-12">
							<label for="form-field-1"> Note </label>
								<input type="text" name="notes" id="form-field-1" placeholder="Add a note to this customer..." class="col-xs-10 col-sm-12" />
							</div>
						</div>
						</div>
						</div>

						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h2>Tags</h2>
						<p>Tags can be used to categorize customers into groups.</p>
						</div>
						<div class="col-md-8 block-background">

						<div class="form-group">
							<div class="col-sm-12" class="form-control">
							<label for="form-field-1"> Tags </label>
								<select name="customer_tags[]" id="customer-tag" multiple style="width:100%;">
								@foreach($customer_tags as $tag )
										<option value="{{$tag->id}}">{{$tag->name}}</option>
									@endforeach
									
								</select>
							</div>
						</div>
						</div>
						</div>

<!-- 						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tag </label>

							<div class="col-sm-9">
								<input type="text" name="tags" id="form-field-tags" date-role="tagsinput" value="Tag Input Control" placeholder="Enter tags ..." />
							</div>
						</div> -->
						<div class="hr hr32 hr-dotted"></div>
						<div class="form-group">
							<div class="col-md-offset-9 col-md-3">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Reset
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>





@endsection