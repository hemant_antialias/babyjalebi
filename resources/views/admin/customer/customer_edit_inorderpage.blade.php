<style>
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>
<div class="row">

<div class="col-md-12">
<input type="hidden" value="{{$user->userid}}" id="cid">
  <h5><a href="{{URL::to('admin/customers/'.$user->userid)}}">{{$user->first_name}} {{$user->last_name}}
  </a></h5>
  </div>
  <div class="col-md-10">
  <a href="#" class="user_email">{{$user->email}}
  </a><br>

  @if(isset($user->note) && $user->note != '')
    Note: {{$user->note}} 
  @endif 
  </div>
  <div class="col-md-2">
  <a id="edit-email"> Edit
  </a>
  </div>
  <br><br>
  <div class="col-md-12 hr hr32 hr-dotted">
            </div>
   <div class="col-md-10"> 
  <storng>Shipping Address
  </storng> 
  <br><br>
  </div>
  <div class="col-md-2">
  <a id="edit-address">Edit
  </a>
  </div>
  <br><br>
  <div class="col-md-11" style="color: #868686;">
  {{$user->first_name}} {{$user->last_name}}
  <br>
  Address: {{$user->address}}
  Address: {{$user->address2}}
  <br>
  City: {{$user->city}}
  <br>
  State: {{$user->state}}
  <br>
  Country: {{$user->country}}
  <br>
  Pin code: {{$user->zip}}
  @if(isset($user->phone))
  <br>
  Mobile: {{$user->phone}}
  @else
  <br>
  Mobile: {{$user->contact}}
  @endif

  </div>
  <div class="col-md-12 hr hr32 hr-dotted">
            </div>
            <div class="col-md-10">

            <storng>BILLING Address
  </storng> 
  <br>
  <p style="color: #868686;" class="billing-address"> Same As above</p>
  </div>
  <div class="col-md-2"><a id="edit-address1">Edit
  </a>
 </div>
</div>
<!-- Modal for email change-->
<div id="userEmailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit email
        </h4>
      </div>
      <div class="modal-body">
        <h5>Notification emails will be sent to this address
        </h5>
        <br>
        <div class="form-group">
          <div class="col-sm-12">
           <label for="form-field-1"> Email 
          </label>
            <input type="hidden" class="user_id" name="id" value="{{$user->userid}}" />

            {{Form::text('email',$user->email, array('id' => 'email','class' => 'col-xs-12 col-sm-12 user_email'))}}
            @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <br>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="button" class="btn btn-default change-email">Apply
        </button>
      </div>
    </div>
  </div>
</div>
<!-- Modal for address change-->
<div id="userAddressModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content editcustomer">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit shipping address
        </h4>
      </div>
      <div class="modal-body">

      <form id="customer-edit-shipping">
        <input type="hidden" class="user_id" name="id" value="{{$user->userid}}" />

        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> First Name dddd
            </label>
            {{Form::text('first_name',$user->first_name, array('id' => 'first_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> Last Name 
            </label>
            {{Form::text('last_name',$user->last_name, array('id' => 'last_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
          <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Company
            </label>
            {{Form::text('company',$user->company, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('company'))
            <span class="help-block">
              <strong>{{ $errors->first('company') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Phone Number 
            </label>
            {{Form::text('contact',$user->phone, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('contact'))
            <span class="help-block">
              <strong>{{ $errors->first('contact') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 
            </label>
            {{Form::text('address',$user->address, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 2
            </label>
            {{Form::text('address2',$user->address2, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div >
            <label for="form-field-1"> Country 
            </label>
            {{Form::text('country',$user->country, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
          <label for="form-field-1"> City 
            </label>
            {{Form::text('city',$user->city, array('id' => 'city','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> State 
            </label>
            {{Form::text('state',$user->state, array('id' => 'state','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1">Pin code 
            </label>
            {{Form::text('zip',$user->zip, array('id' => 'zip','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
      </div>
      <br>
      <br>
      <div style="float: right;margin: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="submit" id="update-add" class="btn btn-default add-to-order">Apply
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal for billing address change-->
<div id="billingAddressModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content editcustomer">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit Billing address
        </h4>
      </div>
      <div class="modal-body">

      <form id="customer-edit-billing">
        <input type="hidden" class="user_id" name="user_id" value="{{$user->userid}}" />

        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> First Name 
            </label>
            {{Form::text('first_name',$user->first_name, array('id' => 'first_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> Last Name 
            </label>
            {{Form::text('last_name',$user->last_name, array('id' => 'last_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
          <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Company
            </label>
            {{Form::text('company',$user->company, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('company'))
            <span class="help-block">
              <strong>{{ $errors->first('company') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Phone Number 
            </label>
            {{Form::text('contact',$user->phone, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('contact'))
            <span class="help-block">
              <strong>{{ $errors->first('contact') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 
            </label>
            {{Form::text('address',$user->address, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 2
            </label>
            {{Form::text('address2',$user->address2, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div >
            <label for="form-field-1"> Country 
            </label>
            {{Form::text('country',$user->country, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
          <label for="form-field-1"> City 
            </label>
            {{Form::text('city',$user->city, array('id' => 'city','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> State 
            </label>
            {{Form::text('state',$user->state, array('id' => 'state','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> Pin code 
            </label>
            {{Form::text('zip',$user->zip, array('id' => 'zip','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
      </div>
      <br>
      <br>
      <div style="float: right;margin: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="submit" id="update-billing-address" class="btn btn-default add-to-order">Apply
        </button>
      </div>
      </form>
    </div>
  </div>
</div>


<script src="{{URL::to('web-assets/js/bootstrap.min.js')}}"></script>

<script>
  $('#edit-email').click(function(){
    $('#userEmailModal').modal('show');
  });

  $('#edit-address').click(function(){
    $('#userAddressModal').modal('show');
  });

  $('#edit-address1').click(function(){
    $('#billingAddressModal').modal('show');
  });

    $('#update-add').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/add-customer',
        type:'POST',
        data:$("#customer-edit-shipping").serialize(),
        success:function(data){
          if (data.status == 0) {
            toastr.success(data.message, {timeOut: 100000})
            return;
          };
          $('.user-detail').html(data);
          $('#userAddressModal').modal('hide');
          // $('.modal-backdrop.in').css('dispplay','none');
          $('.modal-backdrop.fade.in').css('position','inherit');
          toastr.success('Shipping address updated.', {timeOut: 100000})
        }
      });
  });

  $('#update-billing-address').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/update-billing-address',
        type:'POST',
        data:$("#customer-edit-billing").serialize(),
        success:function(data){
          $('.billing-address').html(data);
          $('#billingAddressModal').modal('hide');
          // $('.modal-backdrop.in').css('dispplay','none');
          $('.modal-backdrop.fade.in').css('position','inherit');
          toastr.success('Billing address updated.', {timeOut: 100000})
        }
      });
  });

  $('.change-email').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/change-customer-email',
        type:'POST',
        data:{
          'id': $('.user_id').val(),
          'email': $('#email').val()
        },
        success:function(data){
          if (data.status == 1) {
            $('.modal-content').modal('hide');
            $('.user_email').text(data.email);
            $('.modal-backdrop.in').modal('hide');
            $('.modal-backdrop.in').css('opacity','0');
            toastr.success('Email address updated.', {timeOut: 100000})
          }else{
            toastr.success('User Not Found.', {timeOut: 100000})
          }
        }
      });
  });


</script>
