@extends('admin.admin_layout')


@section('content')
	
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="{{URL::to('/admin/customers')}}">Customer</a>
					</li>
					<li class="active">Add Customer</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::model($customer, ['url' => 'admin/customers/edit/'.$customer->id, 'method' => 'post', 'class' => 'form-horizontal']) }}
						<div class="row">
						<div class="col-md-4">
						<h2>Customer overview</h2>
						</div>
						<div class="col-md-8 block-background">
						<div class="form-group col-sm-6" style="margin-right: 3%;">
						
							<div>
							<label for="form-field-1"> First Name </label>	
							    {{ Form::input('text', 'first_name',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							    @if ($errors->has('first_name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('first_name') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>
						<div class="form-group col-sm-6">
							<div>
							<label for="form-field-1"> Last Name </label>
							    {{ Form::input('text', 'last_name',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
							<label for="form-field-1"> Email </label>
							    {{ Form::input('text', 'email',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>
						<!-- <div class="form-group">
							<div class="checkbox">
								<label>
									{{ Form::checkbox('is_accept_marketing', 0, null, ['class' => 'ace ace-checkbox-2']) }}
									<span class="lbl"> Customer accepts marketing</span>
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<input name="is_tax_exempt" type="checkbox" class="ace ace-checkbox-2" />
									{{ Form::checkbox('is_tax_exempt', 0, null, ['class' => 'ace ace-checkbox-2']) }}
									<span class="lbl"> Customer is tax exempt</span>
								</label>
							</div>
						</div> -->
						</div>
						</div>

						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h2>Address</h2>
						<p>The primary address of this customer.</p>
						</div>
						<div class="col-md-8 block-background">
						<div class="form-group col-md-6" style="margin-right: 3%;">
							<div>
							<label for="form-field-1"> Company </label>
							    {{ Form::input('text', 'company',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							
						</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label  for="form-field-1"> Phone </label>
							    {{ Form::input('text', 'contact',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
							<label> Address </label>
							    {{ Form::input('text', 'address',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
							<label for="form-field-1"> Address Con't </label>
								<input type="text" name="address_cont" id="form-field-1" placeholder="Address Con't" class="col-xs-10 col-sm-12" />
							</div>
						</div>

						<div class="form-group col-md-6" style="margin-right: 3%;">
							<div>
							<label for="form-field-1"> City </label>
							    {{ Form::input('text', 'city',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label for="form-field-1"> Zip Code </label>
							    {{ Form::input('text', 'zip',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>

						<div class="form-group col-md-6" style="margin-right: 3%;">
							<div>
							<label for="form-field-1"> State </label>
							    {{ Form::input('text', 'state',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>

						<div class="form-group col-md-6">
							<div>
							<label for="form-field-1"> Country </label>
							    {{ Form::input('text', 'country',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							
						</div>
						</div>
						</div>
						</div>
						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h2>Notes</h2>
						<p>Enter any extra notes relating to this customer.</p>
						</div>
						<div class="col-md-8 block-background">

						<div class="form-group">
							<div class="col-sm-12">
							<label for="form-field-1"> Note </label>
								{{ Form::input('text', 'notes',null, ['id' => 'form-field-1', 'class' => 'col-xs-10 col-sm-12']) }}
							</div>
						</div>
						</div>
						</div>

						<div class="hr hr32 hr-dotted"></div>
						<div class="row">
						<div class="col-md-4">
						<h2>Tags</h2>
						<p>Tags can be used to categorize customers into groups.</p>
						</div>
						<div class="col-md-8 block-background">
						
						<div class="form-group">
							<div class="col-sm-12">
							<label for="form-field-1"> Tags </label>
    						{{  Form::select('customer_tags[]', $customer_tags,null,['style' => 'width:100%;', 'id' => 'edit-customer-tag', 'class' => 'form-control', 'multiple']) }}


							</div>
						</div>
						</div>
						</div>

<!-- 						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tag </label>

							<div class="col-sm-9">
								<input type="text" name="tags" id="form-field-tags" date-role="tagsinput" value="Tag Input Control" placeholder="Enter tags ..." />
							</div>
						</div> -->
						<div class="hr hr32 hr-dotted"></div>
						<div class="form-group">
							<div class="col-md-offset-9 col-md-3">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Reset
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>

@section('scripts')
	<script>
	// $output = implode(', ', array_map(
	//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
	//     $input,
	//     array_keys('<?php $customer_tags ?>')
	// ));

	var s2 = $('#edit-customer-tag').select2({
		placeholder: 'Choose a tag',
		// tags: $output,
		tags: true
	});

	</script>
@endsection
@endsection
