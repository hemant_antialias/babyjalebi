<!-- Modal for address change-->
<div id="userAddressModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content editcustomer">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h4 class="modal-title">Edit shipping address
        </h4>
      </div>
      <div class="modal-body">

      <form id="customer-edit-shipping">
         <input type="hidden" class="user_id" name="id" value="{{$order->userid}}" />
         <input type="hidden" class="user_id" name="order_id" value="{{$order->order_code}}" />

        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> First Name
            </label>
            {{Form::text('first_name',$order->first_name, array('id' => 'first_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-md-6">
          <div>
            <label for="form-field-1"> Last Name 
            </label>
            {{Form::text('last_name',$order->last_name, array('id' => 'last_name','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
          <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Company
            </label>
            {{Form::text('company',$address->company, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('company'))
            <span class="help-block">
              <strong>{{ $errors->first('company') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Phone Number 
            </label>
            {{Form::text('contact',$address->phone, array('id' => 'contact','class' => 'col-xs-12 col-sm-12'))}}
            @if ($errors->has('contact'))
            <span class="help-block">
              <strong>{{ $errors->first('contact') }}
              </strong>
            </span>
            @endif
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 
            </label>
            {{Form::text('address',$address->address, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div class="">
            <label for="form-field-1"> Address 2
            </label>
            {{Form::text('address2',$address->address2, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div >
            <label for="form-field-1"> Country 
            </label>
            {{Form::text('country',$address->country, array('id' => 'address','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
          <label for="form-field-1"> City 
            </label>
            {{Form::text('city',$address->city, array('id' => 'city','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1"> State 
            </label>
            {{Form::text('state',$address->state, array('id' => 'state','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
        <div class="editcustomer-pedding col-sm-6">
          <div>
            <label for="form-field-1">Pin code 
            </label>
            {{Form::text('zip',$address->zip, array('id' => 'zip','class' => 'col-xs-12 col-sm-12'))}}
          </div>
        </div>
      </div>
      <br>
      <br>
      <div style="float: right;margin: 5%;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
        </button>
        <button type="submit" id="update-add" class="btn btn-default add-to-order">Apply
        </button>
      </div>
      </form>
    </div>
  </div>
</div>
              
                <div class="form-group col-md-4">
                  <label class="col-sm-12"> 
                    <b>Contacts  
                    </b> 
                     <span style="float: right;">  <a href=""> <i class="fa fa-pencil foredit" aria-hidden="true"></i></a>
                  </label>
                  <br>
                  <br>
                
                 
                  <div class="col-sm-12">
                    <a href="{{URL::to('/admin/customers/'.$order->userid)}}">{{$order->first_name}} {{$order->last_name}}</a>
                    <br><br>
                    <a href="">{{$order->email}}</a>
                    @if(isset($order->note) && $order->note != '')
                      <br><br>
                      <p>Customer Notes: {{$order->note}}</p>
                    @endif
                  </div>
                </div>
                 
               <div class="form-group col-md-4">
                  <label class="col-sm-8" for="form-field-1"> DEFAULT ADDRESS 
                  </label>
                  <span class="col-sm-4" style="float: right;">  <a id="edit-address" style="float: right;"> <i class="fa fa-pencil foredit" aria-hidden="true"></i></a></span>
                  <div class="col-sm-12">

                  <BR>
                  @if($order->order_from == 1)
                      {{$order->first_name}} {{$order->last_name}}<BR>
                      {{$address->address}}<BR>
                      {{$address->address2}}<BR>
                      {{$address->zip}} {{$address->city}} {{$address->state}}<BR>
                      {{$address->country}} <BR>
                      @if($order->contact == NULL)
                      {{$address->phone}}
                      @else
                      {{$order->contact}}<BR>
                      @endif
                    @else
                      {{$order->first_name}} {{$order->last_name}}<BR>
                      {{$address->address}}<BR>
                      {{$address->address2}}<BR>
                      {{$address->zip}} {{$address->city}} {{$address->state}}<BR>
                      {{$address->country}} <BR>
                      @if($order->contact == NULL)
                      {{$address->phone}}
                      @else
                      {{$order->contact}}<BR>
                      @endif
                    @endif
                    
                  </div>
                </div>
                 

                <script>
    $('#edit-address').click(function(){
    $('#userAddressModal').modal('show');
  });

    $('#update-add').click(function(e){
    e.preventDefault();
    var base_url = "{{URL::to('/')}}";
    $.ajax({
        url:base_url+'/admin/add-customer1',
        type:'POST',
        data:$("#customer-edit-shipping").serialize(),
        success:function(data){
          if (data.status == 0) {
            toastr.success(data.message, {timeOut: 100000})
            return;
          };
          $('.user-detail-old').hide();
          $('.user-detail').html(data);
          $('#userAddressModal').modal('hide');
          // $('.modal-backdrop.in').css('dispplay','none');
          // $('.modal-backdrop.fade.in').css('position','inherit');
          // toastr.success('Shipping address updated.', {timeOut: 100000});
          e.preventDefault();
        }
      });
  });
  </script>
                