@extends('admin.admin_layout')


@section('content')
	
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Vendor</a>
					</li>
					<li class="active">Profile</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/edit-profile', 'method' => 'post', 'class' => 'form-horizontal']) }}
						
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

							<div class="col-sm-9">
								<input type="text" name="name" value="{{$admin->name}}" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email </label>

							<div class="col-sm-9">
								<input type="text" name="email" value="{{$admin->email}}" id="form-field-1" placeholder="Title" class="col-xs-10 col-sm-5" />
							</div>
						</div>
						
						@if(isStore())

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Customer Email </label>

							<div class="col-sm-9">
								<input type="text" name="customer_email" value="{{$admin->customer_email}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Legal name of business  </label>

							<div class="col-sm-9">
								<input type="text" name="store_name" value="{{$admin->store_name}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Phone </label>

							<div class="col-sm-9">
								<input type="text" name="phone" value="{{$admin->contact}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Address </label>

							<div class="col-sm-9">
								<input type="text" name="address" value="{{$admin->address}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Location </label>

							<div class="col-sm-9">
								<input type="text" name="location" value="{{$admin->location}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">City </label>

							<div class="col-sm-9">
								<input type="text" name="city" value="{{$admin->city}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Postal/ZIP Code </label>

							<div class="col-sm-9">
								<input type="text" name="zip" value="{{$admin->pin_code}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">State </label>

							<div class="col-sm-9">
								<input type="text" name="state" value="{{$admin->state}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Country </label>

							<div class="col-sm-9">
								<input type="text" name="country" value="{{$admin->country}}" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>
						@endif
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Reset
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>


@endsection