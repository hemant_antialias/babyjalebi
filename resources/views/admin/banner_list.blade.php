@extends('admin.admin_layout')


@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <br>
                    <h3 class="header smaller lighter blue">Slider Lists</h3>
                    <!-- <button id="refresh" class="btn btn-default">Refresh</button> -->
                    <div style="float: right;">
                    <button id="refresh" class="btn btn-default">Refresh</button>
                    <a href="{{URL::to('/admin/add-banner')}}"> <button id="createcategory" class="btn btn-default">Create Slider</button></a>
                    </div>
                    <br><br><br>
                    <!-- div.dataTables_borderWrap -->
                    <div class="inventory-data">
                        <table id="blog-dynamic-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Image</th>
                                    <th class="hidden-480 vendor-status">Status</th>
                                    <th>Action</th>
                                </tr>
                                <!-- <tr>
                                    <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="input-filter">Image</th>
                                    <th class="input-filter">Store</th>
                                    <th class="hidden-480 category-status">Status</th>
                                </tr> -->
                            </thead>

                            <tbody class="product-inventory">

                            @foreach($banners as $banner)
                                <tr>
<!--                                     <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace banner-id" value="{{$banner->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td> -->
                                    <td>
	                                    @if(isset($banner->image_name))
		                                    <td class="next-table__image-cell hide-when-printing">
		                                    	<img title="" class="block aspect-ratio__content" src="{{URL::to('/banner_images/'.$banner->id.'/'.$banner->image_name)}}" alt="" style="width: 50px;">
		                                    </td>
		                                @else
		                                    <td class="next-table__image-cell hide-when-printing">
		                                    	<img title="No Image" class="block aspect-ratio__content" src="{{URL::to('/web-assets/images/default_product.jpg')}}" alt="No Image" style="width: 50px;">
		                                    </td>
		                                @endif
                                    </td>
                                   
                                    <td class="hidden-480">
                                        {{ Form::select('status', ['1' => 'Active', '0' => 'Inactive'], $banner->status, ['class' => 'field status']) }}
                                    </td>
                                     <td>
                                        <a href="#">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.col -->
    </div><!-- /.row -->

@stop

@section('scripts')

    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
            $('.input-filter').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('.category-status').each( function () {
                var product_status = '<option value="active">Active</option><option value="inactive">Inactive</option>';
                $(this).html( '<select><option value="">Select</option>'+product_status+'</select>' );
            } );

            var myTable = $('#blog-dynamic-table').DataTable({
                paging: true,
                sort: true,
                searching: true,
                select: {
                    style: 'multi'
                }
            });

                        // Apply the search
            myTable.columns().every( function () {
                var that = this;
                
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


                serachTextBox.on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachSelectBox.on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


            } );
            
            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });
        });

    </script>

@endsection