@extends('admin.admin_layout')


@section('content')
<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Product</a>
					</li>
					<li class="active">Add Product</li>
				</ul><!-- /.breadcrumb -->

			</div>
			<br>
			<div class="row">
				<div class="col-xs-9">
						{{ Form::model($coupon, ['url' => 'admin/coupon/edit/'.$coupon->id, 'method' => 'post', 'class' => 'form-horizontal']) }}
						
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Coupon Code </label>

							<div class="col-sm-9">
								<input type="text" name="coupon_code" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							    @if ($errors->has('coupon_code'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('coupon_code') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Coupon Type </label>

							<div class="col-sm-4">
								{{  Form::select('coupon_type', array('' => 'Select' , MONEY_COUPON => 'Cash', PERCENTAGE_COUPON => 'Percentage', 'FREE_SHIPPING' => 'Free Shipping'),null,['class' => 'form-control']) }}		
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Discount Value </label>

							<div class="col-sm-9">
								<input type="text" name="discount_value" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Discount Type </label>

							<div class="col-sm-4">
								{{  Form::select('discount_type', array(
																'' => 'Select' , 
																'all_orders' => 'all orders', 
																'minimum_order' => 'orders over', 
																'collection' => 'collection',
																'product' => 'product',
																'customer_in_group' => 'customer in group'
																)
																,null,['class' => 'form-control', 'id' => 'discount_type']) }}		
							
							</div>
						</div>

						<div class="form-group order_above" style="display:none">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Order Above </label>

							<div class="col-sm-9">
								<input type="text" name="order_above" id="form-field-1" placeholder="" class="col-xs-10 col-sm-5" />
							    @if ($errors->has('order_above'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('order_above') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>

						<div class="form-group d_collection" style="display:none">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Collection </label>

							<div class="col-sm-9" class="form-control">
							    <select name="collection">
									@foreach($product_categories as $category )
										<option value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							    @if ($errors->has('collection'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('collection') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>

						<div class="form-group d_product" style="display:none">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product </label>

							<div class="col-sm-4">
							    <select name="product" class="form-control">
									@foreach($products as $product )
										<option value="{{$product->id}}">{{$product->product_title}}</option>
									@endforeach
								</select>
							    @if ($errors->has('product'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('product') }}</strong>
	                                </span>
	                            @endif
							</div>
						</div>

						<div class="form-group d_customer_group" style="display:none">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Customer Group </label>

							<div class="col-sm-4">
								{{  Form::select('customer_group', array(
																'accepts' => 'Accepts Marketing', 
																'from_india' => 'From India', 
																'prospects' => 'Prospects',
																'repeat_customer' => 'Repeat Customer',
																)
																,null,['class' => 'form-control']) }}		
							
							@if ($errors->has('customer_group'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_group') }}</strong>
                                </span>
                            @endif
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Date </label>

							<div class="col-sm-4">
								<div class="input-group">
									<input class="form-control date-picker" name="start_date" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" />
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> End Date </label>

							<div class="col-sm-4">
								<div class="input-group">
									<input class="form-control date-picker" name="end_date" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" />
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Total quantity available </label>

							<div class="col-sm-4">
								<div class="input-group">
									Unlimited <input name="coupon_quantity" id="" type="radio" value="unlimited" /><br>
									<input name="coupon_quantity" id="" type="radio" value="limited" />
									<input type="text" name="coupon_quant">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Limit per customer </label>

							<div class="col-sm-4">
								<div class="input-group">
									1 Use Only <input name="limit_per_user" type="checkbox"/><br>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
				
			</div>
		</div>
	</div>


<script>
	$('#discount_type').change(function(){
		var discount_type = $('#discount_type').val();
		if (discount_type == 'all_orders') {
			$('.order_above').css('display','none');
			$('.d_collection').css('display','none');
			$('.d_product').css('display','none');
			$('.d_customer_group').css('display','none');
		};
		if (discount_type == 'minimum_order') {
			$('.order_above').css('display','block');
			$('.d_collection').css('display','none');
			$('.d_product').css('display','none');
			$('.d_customer_group').css('display','none');
		};
		if (discount_type == 'collection') {
			$('.d_collection').css('display','block');
			$('.order_above').css('display','none');
			$('.d_product').css('display','none');
			$('.d_customer_group').css('display','none');
		};
		if (discount_type == 'product') {
			$('.d_product').css('display','block');
			$('.d_collection').css('display','none');
			$('.order_above').css('display','none');
			$('.d_customer_group').css('display','none');
		};
		if (discount_type == 'customer_in_group') {
			$('.d_customer_group').css('display','block');
			$('.d_product').css('display','none');
			$('.d_collection').css('display','none');
			$('.order_above').css('display','none');

		};
		
	})
</script>

@endsection