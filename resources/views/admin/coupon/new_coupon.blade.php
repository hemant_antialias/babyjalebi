	@extends('admin.admin_layout')


	@section('content')
	<style>
	.fordate
	{
	    background-color: #f5f6f7;
	    border-radius: 3px;
	    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
	}
	.select2-container--default .select2-search--inline .select2-search__field
	{
		width: 500px !important;
	}
	.select2-container
	{
		width: 100% !important;
	}
	.ordrabove{
		margin-top: -76px;
	}
	</style>
	<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="{{URL::to('/admin/couponlist')}}">Coupon</a>
						</li>
						<li class="active">Add Coupon</li>
					</ul><!-- /.breadcrumb -->

				</div>
				<br>
				<div class="row">
					
							{{ Form::open(['url' => 'admin/coupon/new', 'method' => 'post', 'class' => 'form-horizontal']) }}
							<div class="col-xs-8">
							<div class="col-md-12" style="padding: 2%;background-color: #ffffff;border-radius: 3px;box-shadow: 0 2px 4px rgba(0,0,0,0.1);margin-right: 3%;">
							<h4> Discount Code </h4>
							
							<div class="form-group">
								

								<div class="col-sm-12">
									<input type="text" name="coupon_code" id="form-field-1" placeholder="e.g SPRINGSALE" class="col-xs-10 col-sm-12" />
									Customers will enter this discount code at checkout.
								    @if ($errors->has('coupon_code'))
		                                <span class="help-block">
		                                    <strong>{{ $errors->first('coupon_code') }}</strong>
		                                </span>
		                            @endif
								</div>
							</div>

							<div class="hr hr32 hr-dotted"></div>

							<h4>Conditions</h4>
							
							<br>
							<div class="form-group">
								<!-- <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Coupon Type </label> -->

								<div class="col-sm-4">
									{{  Form::select('coupon_type', array('' => 'Select' , '1' => 'Cash', '2' => 'Percentage', '3' => 'Free Shipping'),null,['class' => 'form-control coupon_type']) }}		
								</div>
								<div class="col-sm-4">
								<!-- <label for="form-field-1"> Discount Value </label> -->
									<input type="text" name="discount_value" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12 discount_value" />
								</div>
								<div class="col-sm-4">
									{{  Form::select('discount_type', array(
																	'' => 'Select' , 
																	'all_orders' => 'all orders', 
																	'minimum_order' => 'orders over', 
																	'category' => 'category',
																	'product' => 'product',
																	)
																	,null,['class' => 'form-control', 'id' => 'discount_type']) }}		
								
								</div>
							</div>

							
							<div class="row">
							<div class="form-group d_collection col-md-9" style="display:none">
								<label class="col-sm-3" for="form-field-1"> Collection </label>

								<div class="col-sm-12 category-list" style="width:100%">
								    
								</div>
							</div>

							<div class="form-group e_collection col-md-9" style="display:none">
								<label class="col-sm-3" for="form-field-1">Exclude Collections </label>

								<div class="col-sm-12 exclude-category-list" style="width:100%">
								    
								</div>
							</div>

							<div class="form-group d_product col-md-9" style="display:none">
								<label class="col-sm-3" for="form-field-1"> Product </label>

								<div class="col-sm-6 product-list" style="width:100%">
								    
								</div>
							</div>

							<div class="form-group e_product col-md-9" style="display:none">
								<label class="col-sm-3" for="form-field-1"> Exclude Products </label>

								<div class="col-sm-6 exclude-product-list" style="width:100%">
								    
								</div>
							</div>

							<div class="form-group order_above col-md-3" style="display:none">
								

								<div class="ordrabove">
								<label class=" " for="form-field-1"> Order Above </label>
									<input type="text" name="order_above" id="form-field-1" placeholder="" class="col-xs-10 col-sm-12" />
								    @if ($errors->has('order_above'))
		                                <span class="help-block">
		                                    <strong>{{ $errors->first('order_above') }}</strong>
		                                </span>
		                            @endif
								</div>
							</div>
							</div>

							<div class="form-group d_customer_group" style="display:none">
								<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Customer Group </label>

								<div class="col-sm-4">
									{{  Form::select('customer_group', array(
																	'accepts' => 'Accepts Marketing', 
																	'from_india' => 'From India', 
																	'prospects' => 'Prospects',
																	'repeat_customer' => 'Repeat Customer',
																	)
																	,null,['class' => 'form-control']) }}		
								
								@if ($errors->has('customer_group'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('customer_group') }}</strong>
	                                </span>
	                            @endif
								</div>

							</div>
							
							<div class="hr hr32 hr-dotted"></div>
							<h5><b>Usage limits</b></h5>
								<div class="form-group">
								<br>
								<label class="col-sm-12" for="form-field-1"> Total available </label>
								<br>

								<div class="col-sm-4">
									<div class="input-group">
										 <input name="coupon_quantity" id="coupon_quantity" type="radio" value="unlimited" checked="checked" /> Unlimited<br>
										<input name="coupon_quantity" id="coupon_quantity" type="radio" value="limited" /> Limited number of uses <br>
										<input type="text" name="coupon_quant"  id="coupon_quant" style="display:none;">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-12" for="form-field-1"> Limit per customer </label>

								<div class="col-sm-4">
									<div class="input-group">
										<input name="limit_per_user" type="checkbox"/> 1 Use Only <br>
									</div>
								</div>
							</div>
							</div>
							</div>

							<div class="col-xs-4 block-background" >
							<h3>Date Range</h3>
							<div class="form-group">
								<label class="col-sm-12" for="form-field-1"> Start Date </label>

								<div class="col-sm-12">
									<div class="input-group">
										<input class="form-control date-picker" name="start_date" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-12" for="form-field-1"> End Date </label>

								<div class="col-sm-12">
									<div class="input-group">
										<input class="form-control date-picker" name="end_date" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  />
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>

								<div class="col-sm-12">
								<div class="input-group">
								<br>
										<input name="no_end_date " type="checkbox"/> No end date <br>
									</div>
									</div>
							</div>
							<div class="form-group">
				              <div class="col-sm-12" class="form-control">
				              <label for="form-field-1"> Tags </label>
				                <select name="discount_tags[]" id="order-tag" multiple style="width:100%;">
				                  @foreach($discount_tags as $tag )
				                    <option value="{{$tag->id}}">{{$tag->name}}</option>
				                  @endforeach
				                </select>
				              </div>
				            </div>
							</div>
							<div class="hr hr32 hr-dotted"></div>
							<div class="form-group">
								<div class="col-md-offset-9 col-md-3">
									<button class="btn btn-info" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
								</div>
							</div>
						{{ Form::close() }}
					
					
				</div>
			</div>
		</div>


	<script>
	var base_url = "{{URL::to('/')}}";
		$('#discount_type').change(function(){
			// var store_id = $('#store_type').val();
			var result1;
			var result2;
			var result3;
			var result4;


			allProductsByStores(result1);
			allCategoryByStores(result2);
			allExcludeProductsByStore(result3);
			allExcludeCategoryByStores(result4);
		});


		function allProductsByStores(result1){
			$.ajax({
				    url: base_url+'/all-products-by-stores',
				    type: "post",
				    data: 
				    {
			            // "store_id": store_id,
			        },
				    success:function(data)
				    {
				    	result1 = data;
						$('.product-list').html(result1);
				    }
				});
		}

		function allExcludeProductsByStore(result3){
			$.ajax({
				    url: base_url+'/all-exclude-products-by-stores',
				    type: "post",
				    data: 
				    {
			            // "store_id": store_id,
			        },
				    success:function(data)
				    {
				    	result3 = data;
						$('.exclude-product-list').html(result3);
				    }
				});
		}

		function allCategoryByStores(result2){
			$.ajax({
			    url: base_url+'/all-category-by-stores',
			    type: "post",
			    data: 
			    {
		            // "store_id": store_id,
		        },
			    success:function(data)
			    {
				    result2 = data;
					$('.category-list').html(result2);
			    }
			});
		}

		function allExcludeCategoryByStores(result4){
			$.ajax({
			    url: base_url+'/all-exclude-category-by-stores',
			    type: "post",
			    data: 
			    {
		            // "store_id": store_id,
		        },
			    success:function(data)
			    {
				    result4 = data;
					$('.exclude-category-list').html(result4);
			    }
			});
		}


		$('#discount_type').change(function(){
			var discount_type = $('#discount_type').val();
			if (discount_type == 'all_orders') {
				$('.order_above').css('display','none');
				$('.d_collection').css('display','none');
				$('.e_collection').css('display','block');
				$('.d_product').css('display','none');
				$('.e_product').css('display','block');
				$('.d_customer_group').css('display','none');
			};
			if (discount_type == 'minimum_order') {
				$('.order_above').css('display','block');
				$('.d_collection').css('display','none');
				$('.e_collection').css('display','block');
				$('.d_product').css('display','none');
				$('.e_product').css('display','block');
				$('.d_customer_group').css('display','none');
			};
			if (discount_type == 'category') {
				// $('.d_collection').css('display','block');
				$('.e_collection').css('display','block');
				$('.order_above').css('display','block');
				$('.d_product').css('display','none');
				$('.e_product').css('display','block');
				$('.d_customer_group').css('display','none');
			};
			if (discount_type == 'product') {
				$('.d_product').css('display','block');
				$('.d_collection').css('display','none');
				$('.e_collection').css('display','block');
				$('.order_above').css('display','block');
				$('.e_product').css('display','block');
				$('.d_customer_group').css('display','none');
			};
			// if (discount_type == 'customer_in_group') {
			// 	$('.d_customer_group').css('display','block');
			// 	$('.d_product').css('display','none');
			// 	$('.d_collection').css('display','none');
			// 	$('.e_collection').css('display','block');
			// 	$('.e_product').css('display','block');
			// 	$('.order_above').css('display','none');

			// };
			
		});

	$(document).on('change','.coupon_type',function(e){
      var ctype = $('.coupon_type').val();
      if(ctype=="1"){
        $(".discount_value").show();    
        $(".discount_value").attr("placeholder", "Enter Amount").placeholder();  
      }
      if(ctype=="2"){
        $(".discount_value").show();    
        $(".discount_value").attr("placeholder", "Enter % Amount").placeholder();    
      }
      if(ctype=="3"){
        $(".discount_value").hide();    
      }
    });

		$(function(){
		  $('input[type="radio"]').click(function(){
		    if ($(this).is(':checked'))
		    {
		      var coupon_type = $(this).val();

		      if (coupon_type == 'limited') {
				$('#coupon_quant').css('display','block');
		      }else{
				$('#coupon_quant').css('display','none');
		      }
		    }
		  });
		});

	</script>

	@endsection