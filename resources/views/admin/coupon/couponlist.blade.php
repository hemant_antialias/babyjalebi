 @extends('admin.admin_layout')
<style type="text/css">
	.requird
	{
		color:red;
	}
    .dataTables_filter{
        float: right;
    }
</style>
  
@section('content')
<div class='row'>
    <div class="col-lg-12">
     
      	<div class="panel panel-default">
            <div class="panel-body">
            	<h3>Discount</h3><br>
                <form method="get" action="{{URL::to('admin/couponlist')}}">
                        Search by Tags: <input type="text" name="discount_tags">
                        <input type="submit" value="Search" class="btn btn-default">
                    </form>
     			<div style="float: right; margin-top: -14px; ">
                    
                    <button id="refresh" class="btn btn-default">Refresh</button>
                    <a href="{{URL::to('/admin/coupon/new')}}"> <button id="createcoupon" class="btn btn-default">Create Coupon</button></a>
                    <a href="{{URL::to('/exportcoupon')}}"><button id="export1" class="btn btn-default exlport"> Export Coupon List</button></a>
                     </div>
                     <br><br>
     			<div class="order-data">
                        <table id="discount-dynamic-table" class="table table-striped table-bordered table-hover" data-page-length='50'>
                            <thead>
                                <tr>
                                	<th></th>
                                    <th>Coupon Code</th>
                                    <th>Discount Detail</th>
                                    <th>Coupon Status</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>

                                </tr>
                                <tr>
                                    <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace" />
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="input-filter">Coupon Code</th>
                                    <th class="input-filter">Discount Detail</th>
                                    <th class="hidden-480 coupon-status"></th>
                                    <th class="hidden-480"></th>
                                    <th></th>
                                </tr>

                            </thead>
                            
                            <tbody class="product-inventory">

                            @foreach($coupons as $coupon)
                                <tr>
                                <td class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace product-id" value="{{$coupon->id}}" />
                                            <span class="lbl"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <a href="#">{{$coupon->discount_coupon_code}}</a>
                                    </td>
                                    <td>
                                        <a href="#">
                                        @if($coupon->coupon_type == 1)
                                        Rs. {{$coupon->discount_value}} off {{str_replace('_', ' ', $coupon->discount_type)}} @if($coupon->min_order_amount != '') of {{$coupon->min_order_amount}}@endif
                                        @else
                                        {{$coupon->discount_value}}% off {{str_replace('_', ' ', $coupon->discount_type)}} 
                                        @endif
                                    </td>
                                    <td class="hidden-480">
                                        {{ Form::select('coupon_status', ['1' => 'Enable', '0' => 'Disable'], $coupon->is_active, ['class' => 'field coupon_status']) }}
                                    </td>
                                    <td class="hidden-480">
                                        {{$coupon->start_date}}                                    
                                    </td>
                                    <td class="hidden-480">
                                    @if($coupon->valid_till == NULL)
                                        ---- 
                                    @else
                                        {{$coupon->valid_till}}
                                    @endif                                   
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

			</div>
		</div>

    </div>
</div>
@stop
@section('scripts')
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/dataTables.select.min.js')}}"></script>
    <!--  -->
    <script type="text/javascript">
        $(document).ready(function() {
            
            $('.input-filter').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('.coupon-status').each( function () {
                var coupon_status = '<option value="enable">Enable</option><option value="disable">Disable</option>';
                $(this).html( '<select><option value="">Select</option>'+coupon_status+'</select>' );
            } );

            var myTable = $('#discount-dynamic-table').DataTable({
                paging: true,
                sort: true,
                searching: true,
                select: {
                    style: 'multi'
                }
            });

                        // Apply the search
            myTable.columns().every( function () {
                var that = this;
                
                var serachTextBox = $( 'input', this.header() );
                var serachSelectBox = $( 'select', this.header() );


                serachTextBox.on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachSelectBox.on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                serachTextBox.on('click', function(e){
                    e.stopPropagation();
                });
                serachSelectBox.on('click', function(e){
                    e.stopPropagation();
                });


            } );
            
            $('#refresh').click(function(){
                myTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            });
        });

    </script>
@endsection