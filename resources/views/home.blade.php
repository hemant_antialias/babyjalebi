<!DOCTYPE html>
<!--[if lte IE 7 ]>   <html class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>   <html class="no-js ie8"> <![endif]-->
<!--[if (gt IE 8)|!(IE)]><!--> 
<html class="no-js"> 
  <!--<![endif]-->
  <!-- Mirrored from www.lionfresh.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Sep 2016 08:19:26 GMT -->
  <!-- Added by HTTrack -->
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <!-- /Added by HTTrack -->
  <head>
    <!-- Symmetry 1.9.1 -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="https://cdn.shopify.com/s/files/1/1127/1070/files/Lionfresh_logo_2_final-01.png?3290978085961989039" />
    <meta name="twitter:site" content="@">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="LionFresh | Buy Fresh Raw Chicken, Meat, Lamb, Pork Online in Delhi">
    <meta name="twitter:description" content="Online Meat Shop for buying fresh raw Chicken, Mutton, Lamb, Pork and Seafood Online in New Delhi. Get it Delivered. Order Online or call 1800-270-1285.">
    <meta name="twitter:image:alt" content="LionFresh | Buy Fresh Raw Chicken, Meat, Lamb, Pork Online in Delhi">
    <title>
      LionFresh | Buy Fresh Raw Chicken, Meat, Lamb, Pork Online in Delhi
    </title>
    <meta name="description" content="Online Meat Shop for buying fresh raw Chicken, Mutton, Lamb, Pork and Seafood Online in New Delhi. Get it Delivered. Order Online or call 1800-270-1285." />
    <link rel="canonical" href="index.html" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
   <!--  <link rel="shortcut icon" type="image/x-icon" href="../cdn.shopify.com/s/files/1/1127/1070/t/2/assets/favicon9a04.png?14144647724873477262"> -->
    <meta property="og:site_name" content="Lion Fresh Delhi" />
    <meta property="og:type" content="website" />  
    <meta property="og:image" content="http://cdn.shopify.com/s/files/1/1127/1070/t/2/assets/logo.png?14144647724873477262" />
    <meta property="og:url" content="https://www.lionfresh.com" />
    <link href="{{URL::to('web-assets/css/style.css')}}" rel="stylesheet" type="text/css" media="all" >
    <link href="{{URL::to('web-assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all" >
    <link href="{{URL::to('web-assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" >
    <link href="{{URL::to('web-assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all" > 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">   
    <link rel="stylesheet" href="{{URL::to('admin-assets/css/chosen.min.css')}}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            modal: true,
            autoOpen: false,
            title: "jQuery Dialog",
            width: 300,
            height: 150
        });
        $("#btnShow").click(function () {
            $('#dialog').dialog('open');
        });
    });
    </script>
    <script src="{{URL::to('admin-assets/js/jquery-ui.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/chosen.jquery.min.js')}}"></script>
    <script src="{{URL::to('admin-assets/js/select2.min.js')}}"></script>

    <style>
         @media only screen and (max-width: 767px ){
            .collection-listing .product-block {
            width:46% !important;
            }
        }

      body
      {
        font-family: 'Montserrat', sans-serif;
      }
      .heading1
      {
        font-family: 'Montserrat', sans-serif;
        color: #fff;
        /*text-shadow: 3px 3px #505050;*/
            font-size: 20px;
    line-height: 33px;
    margin-top: 3%;
    /*text-shadow: 1px 1px 5px rgba(150, 150, 150, 0.97);*/
    letter-spacing: 4px;
        /*font-size: 30px;*/
      }
      .heading2
      {
        font-family: 'Montserrat', sans-serif;
        color: #fff;
        font-size: 60px;
        font-weight: bold;
      }
      .containgeralignment
      {
        text-align: center;
      }
      .forbutton
      {
        background: #d8920c;
        color: #fff;
        padding: 20px 45px 20px 45px;
        border-radius: 50px;
        margin-top: 10%;
        margin-bottom: 1%;

      }
      .white-section
      {
        color: #fff;
        text-transform: capitalize;
      }
      #clickableAwesomeFont {
     cursor: pointer;

}
#store_id
{
  width: 41% !important;
}
#city {
  font-family: 'Montserrat', sans-serif !important;
}
#city option{
  font-family: 'Montserrat', sans-serif !important;
}

@media screen and (max-width: 810px) {
   

   #store_id
{
      width: 100% !important;
    margin-top: 10px;
    margin-left: 13px;
}
.logoborder{
      border: 2px solid white !important;
}
.icity{
  width: 100% !important ; 
}
.cityleft{ margin-right: 0px !important;  }
.cityright{ margin-left: 0px !important; float: none !important ; }
#logoimg{
  text-align: center;
    width: 34% !important;
    margin-top: 1%;
    margin-bottom: 1%;

}
#submitbtn1{
  margin-left: -45.5% !important;
      background-repeat: no-repeat;
    height: 30px;
    width: 83%;
    position: absolute;
    color: transparent;
    display: none;

}
#submitbtn2{
  /*margin-left: -45.5% !important;*/
      background-repeat: no-repeat;
    height: 30px;
    width: 86%;
    position: absolute;
    color: white !important;
    margin-top: 10px;
    display: block !important;
    background: #d8920c;

}
body {
  background-size:initial!important ; 
  background: black !important;
}
.heading1, .heading2{ margin-left: 0px !important;font-size: 20px; 
  

}
} /* media query ends here */
.pos-topmar{
  margin-top: 6%;
}
#submitbtn1{
  /*margin-left: -45.5% !important;*/
      background-repeat: no-repeat;
    height: 30px;
    /*width: 83%;*/
    position: absolute;
    color: transparent;
}
#submitbtn2{ display: none;  }

.afterselectcity
{
  height: 50px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 0px;
        width: 32%;
}
@media screen and (device-aspect-ratio: 2/3) {
  .heading2 {
    font-family: 'Montserrat', sans-serif;
    color: #fff;
    font-size: 40px;
    font-weight: bold;
    }
}
/*for iphone 5 and 6 */
@media only screen and (max-width: 375px){

.flexsearch--input { width: 20px;}
.tabcontent{ margin-top: 10%; }
#submitbtn2{ width: 84%; }




}
@media only screen and (max-width: 320px){
#submitbtn2{
  width: 81%;
}
}
        @media screen and (max-width: 810px)
{
        .cityleft {
            margin-right: 0px !important;
            margin-bottom: 0px !important;
        }
        .icity {
            width: 100% !important;
        }
    #submitbtn2{
        width: 92% !important;
        margin-top: 20px!important;
    }
 }
       
       
    </style>    
  </head>
  <body class="template-index" style="background-image: url(web-assets/images/background.jpg); background-repeat:no-repeat; background-size:cover;">
   <?php
$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
echo $url;
echo "<br>";
echo $_SERVER['REMOTE_ADDR'];
?>
    <div class="logo-area logo-pos- cf pos-topmar" style="text-align:center;">
      <div class="logo">
        <a href="/" title="Lion Fresh Delhi">
          <img class="logoimage img-circle logoborder" src="{{URL::to('web-assets/images/logo_svg.svg')}}" alt="Lion Fresh Delhi" id="logoimg" style="text-align: center;width: 11%;margin-top: 1%;">
        </a>
      </div>
      <!-- /#logo -->
      <div style="margin-bottom: 3%;">

        <h2 class="heading1" style="margin-left: 24px;"> FRESH MEAT DELIVERED  AT

        
        <p class="heading2" style="margin-left: 24px;"> YOUR LOCATION 
        </p>
      </div>
      
      <div class="col-md-4   col-md-offset-4">
            <form method="get" action="{{URL::to('/home')}}">
            <select class="js-example-basic-multiple form-control icity cityleft" id="city" name="city" style="width: 41%;float: left;margin-right: 14px; margin-bottom:-4%;">
              <option value="">Select City</option>
              @foreach($cities as $city)
                <option value="{{strtolower($city->city_id)}}">{{$city->city}}</option>
              @endforeach
            </select>
           
              <!-- @foreach($cities as $city)
                <option value="{{strtolower($city->city_id)}}">{{$city->city}}</option>
              @endforeach -->
            
             <spam class="store-types" style="
    margin-left: -4%;
    margin-right: 6%;
    min-width: 50% !important;
    padding-right: -13%;
">
              
            </spam>
             <select class="js-example-basic-multiple form-control static icity cityright" id="city" name="city" style="width: 42%;float:left;margin-left: 16px;">
              <option value="">Select Locality</option>
              <spam class="store-types">
              
            </spam>
            </select>

          <!--  <input type="submit" id="submitbtn1" style="background:url({{URL::to('/images/submit_button.png')}}); background-size: 73px 49px;background-repeat: no-repeat;height: 30px;position: absolute; color:transparent;"> -->
           <input type="submit" id="submitbtn1" style="background:url({{URL::to('/images/submit_button.png')}}); background-size: 100% 49px;background-repeat: no-repeat;height: 30px;position: absolute; color:transparent; min-width:30px;">
           <input type="submit" id="submitbtn2"  value = "submit" style=" background-size: 73px 49px;background-repeat: no-repeat;height: 30px;position: absolute; color:transparent;">
          </form>
          <!-- <button><span class="fa fa-angle-right form-control-feedback right" id="clickableAwesomeFont" aria-hidden="true">
          </span></button> -->
       <div>
        <h4 class="white-section" style="margin-top: 25%;font-style: italic;">Already have an account? 
          <a href="{{url('/login')}}"><span style="font-weight: 600;color: #d8920c;text-decoration: underline;text-transform: capitalize;">Login
          </span></a></h4>
          <!-- <a href="{{URL::to('/business/login')}}"><button class="forbutton" >VISIT LIONFRESH BUSINESS
          </button></a>
          <h4 class="white-section">for Bulk / Corporate Orders
          </h4> -->
          </div> 
          </div>
      </div>
      
        <script type="text/javascript">
          var base_url = '{{URL::to('/')}}';

          // Get the all stores by city wise
          $('#city').change(function(){
            var city_id = $('#city').val();
            // alert(city_id);
            $.ajax({
                url: base_url+'/all-stores-by-city',
                type: "post",
                data: 
                {
                  "city_id": city_id,
                }, 
                success:function(data)
                {
                  // alert(data);
                  $('.static').hide();
                  $('.store-types').html(data);
                }
            });
          });

        </script>
      </body>
    </html>