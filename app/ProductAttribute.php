<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attribute';    
    
    public static $rules = array(
        'name' => 'required|max:255',
    );
}
