<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracker extends Model {

   public $attributes = [ 'hits' => 0 ];

   protected $fillable = [ 'ip', 'visit_date' ];
   protected $table = 'visitors';

   public static function hit() {

       $ip = $_SERVER['REMOTE_ADDR'];
       $date = date('Y-m-d');
       $tracker = Tracker::where('ip', $ip)->where('visit_date', date('Y-m-d'))->first();
       if ($tracker) {
           $tracker->hits = $tracker->hits+1;
           $tracker->save();
       }else{
           $tracker = new Tracker;
           $tracker->ip = $ip;
           $tracker->hits = 1;
           $tracker->visit_date = $date;
           $tracker->save();
       }

       return;      
       
   }

}