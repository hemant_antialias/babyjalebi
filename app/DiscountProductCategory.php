<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountProductCategory extends Model
{
    protected $table = "discount_product_categories";

    protected $guarded = [
        'id'
    ];
}
