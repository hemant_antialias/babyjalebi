<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = "product";

    protected $guarded = [
        'id'
    ];

    public function images()
    {
        return $this->hasMany('App\ProductImages', 'product_id');
    }

    public function tags() {
    	return $this->belongsToMany('App\Tags');
    }
}
