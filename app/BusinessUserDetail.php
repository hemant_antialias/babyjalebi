<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessUserDetail extends Model
{
    protected $table = "b2b_user_detail";

    protected $guarded = [
        'id'
    ];
}
