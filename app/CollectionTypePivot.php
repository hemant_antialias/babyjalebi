<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionTypePivot extends Model
{
    protected $table = "collection_type_pivots";
    
}
