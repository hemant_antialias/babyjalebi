<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariationImages extends Model
{
    protected $table = "product_variation_images";

    protected $guarded = [
        'id'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
