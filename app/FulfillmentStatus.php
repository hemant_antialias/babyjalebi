<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FulfillmentStatus extends Model
{
    protected $table = "fulfillment_status";

    protected $guarded = [
        'id'
    ];
}
