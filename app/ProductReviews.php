<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReviews extends Model
{
    protected $table = "product_reviews";

    protected $guarded = [
        'id'
    ];
}