<?php

namespace App\Permissions;
use App\Role;
use App\Permission;
use DB;
trait HasPermissionsTrait
{

	public function hasPermissionTo($permission)
	{
		// has permission through role
	
		return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);		
	}

	public function hasRole(...$roles)
	{
		foreach ($roles as $role) {
			if ($this->roles->contains('name', $role)) {
				return true;
			}
		}

		return false;
	}

	public function hasPermission(...$permissions)
	{
		foreach ($permissions as $permission) {
			if ($this->permissions->contains('name', $permission)) {
				return true;
			}
		}

		return false;
	}

	public function hasPermissionThroughRole($permission)
	{
		$data = DB::table('permission_role')
					->join('roles', 'permission_role.role_id', '=', 'roles.id', 'left')
					->join('permissions', 'permission_role.permission_id', '=', 'permissions.id', 'left')
					->where('permissions.name', '=', $permission)
					->first();
		
		if ($data) {
			return true;
		}

		return false;
	}

	// public function hasPermission($permission)
	// {
	// 	return (bool) $this->permissions->where('name', $permission->name)->count();
	// }


	public function roles()
	{
		return $this->belongsToMany(Role::class, 'role_user');
	}

	public function permissions()
	{
		return $this->belongsToMany(Permission::class, 'user_permissions');
	}
}