<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerImages extends Model
{
    protected $table = "banner_images";

    protected $guarded = [
        'id'
    ];
}
