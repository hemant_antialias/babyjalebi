<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryParent extends Model
{
    protected $table = "category_parent";
    public $timestamps = true;

    protected $guarded = [
        'id'
    ];
}
