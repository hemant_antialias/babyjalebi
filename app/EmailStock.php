<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailStock extends Model
{
    protected $table = "email_stocks";

    protected $guarded = [
        'id'
    ];
}
