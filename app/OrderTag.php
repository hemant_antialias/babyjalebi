<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTag extends Model
{
    protected $table = "order_tags";

    protected $fillable = [
        'order_id', 'tag_id'
    ];
}
