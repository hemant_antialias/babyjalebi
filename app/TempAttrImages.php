<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempAttrImages extends Model
{
    protected $table = "temp_attr_images";

    protected $guarded = [
        'id'
    ];
}
