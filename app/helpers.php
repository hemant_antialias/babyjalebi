<?php

use App\Vendor;

function isStore(){

	if (Session::has('admin')) {
        $admin_id = Session::get('admin');
        $admin = DB::table('admin')->where('id', $admin_id)->where('is_vendor', 1)->first();
        
        return $admin;           
    }
}

function totalOrderCount(){

	 $totalorder_query = DB::table('order_request');
                                
           if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $totalorder_query->where('vendor_id', $vendor->id);

           }
       
            $totalorder = $totalorder_query->where('order_status', 1)->count();

        // $totalorder = DB::table('order_request')->where('order_status', 1)->count();
        
        return $totalorder;           
    
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function send_sms($mobileNumber,$message,$senderId)
{
	//Your authentication key
	$authKey = "127875AD1xSMRi57fb7e74";

	//Define route 
	$route = "default";
	//Prepare you post parameters
	$postData = array(
	    'authkey' => $authKey,
	    'mobiles' => $mobileNumber,
	    'message' => $message,
	    'sender' => $senderId,
	    'route' => 4
	);

	//API URL
	$url="https://control.msg91.com/api/sendhttp.php";

	// init the resource
	$ch = curl_init();
	curl_setopt_array($ch, array(
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $postData
	    //,CURLOPT_FOLLOWLOCATION => true
	));


	//Ignore SSL certificate verification
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


	//get response
	$output = curl_exec($ch);

	//Print error if any
	if(curl_errno($ch))
	{
	    echo 'error:' . curl_error($ch);
	}

	curl_close($ch);

	return $output;
}
	
	function csvToArray($filename = '', $delimiter = ',')
	{
	    if (!file_exists($filename) || !is_readable($filename))
	        return false;

	    $header = null;
	    $data = array();
	    if (($handle = fopen($filename, 'r')) !== false)
	    {
	        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
	        {
	            if (!$header)
	                $header = $row;
	            else
	                $data[] = array_combine($header, $row);
	        }
	        fclose($handle);
	    }

	    return $data;
	}
?>