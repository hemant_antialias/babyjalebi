<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = "tags";

    protected $guarded = [
        'id'
    ];

    public function products() {
    	return $this->belongsToMany('App\Product');
    }
}
