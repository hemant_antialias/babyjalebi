<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = "vendors_details";

    protected $guarded = [
        'id'
    ];
}
