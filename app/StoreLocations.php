<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreLocations extends Model
{
    protected $table = "store_locations";
    public $timestamps = false;

    protected $guarded = [
        'id'
    ];
}
