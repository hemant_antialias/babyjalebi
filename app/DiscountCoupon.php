<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountCoupon extends Model
{
    protected $table = "discount_coupon";
    public $timestamps = false;

    protected $guarded = [
        'id'
    ];

}
