<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategorySliders extends Model
{
    protected $table = "category_slider";

    protected $guarded = [
        'id'
    ];

    
}
