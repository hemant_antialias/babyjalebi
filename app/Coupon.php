<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = "discount_coupon";

    protected $guarded = [
        'id'
    ];
}
