<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountProduct extends Model
{
    protected $table = "discount_products";

    protected $guarded = [
        'id'
    ];
}
