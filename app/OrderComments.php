<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderComments extends Model
{
    protected $table = "order_comments";

    protected $guarded = [
        'id'
    ];
}
