<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopCartProduct extends Model
{
   	protected $table = "top_cart_product";

    protected $guarded = [
        'id'
    ];
}
