<?php

namespace App;
use App\Permissions\HasPermissionsTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
	use HasPermissionsTrait;

    protected $table = "admin";

    protected $guarded = [
        'id'
    ];

    public function getAuthPassword() {
    	return Hash::make($this->password);
	}


}
