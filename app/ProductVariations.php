<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariations extends Model
{
    protected $table = "product_variations";

    protected $guarded = [
        'id'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}