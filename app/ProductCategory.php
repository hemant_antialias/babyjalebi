<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = "product_category";
    public $timestamps = true;

    protected $guarded = [
        'id'
    ];

}
