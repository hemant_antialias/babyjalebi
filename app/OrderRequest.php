<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRequest extends Model
{
    protected $table = "order_request";

    protected $guarded = [
        'id'
    ];
}
