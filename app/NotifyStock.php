<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifyStock extends Model
{
    protected $table = "notify_stocks";

    protected $guarded = [
        'id'
    ];
}
