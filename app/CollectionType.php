<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionType extends Model
{
    protected $table = "collection_types";
}
