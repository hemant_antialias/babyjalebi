<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeatTypes extends Model
{
    protected $table = "meat_types";

    protected $guarded = [
        'id'
    ];
}
