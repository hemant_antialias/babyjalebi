<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use Input;
use DB;
use Hash;
use Auth;
use Validator;
use Session;
use App\Admin;
use App\OrderRequest;
use App\Vendor;
use App\City;
use App\Address;
use App\OrderStatus;
use App\BannerImages;
use App\Http\Requests\AdminVendorRequest;
use App\Http\Requests\AdminChangePasswordRequest;
use Spatie\Analytics\Period;
use Analytics;
use Carbon\Carbon;  

class ReportsController extends Controller
{
    //

    public function reportList(Request $request) {

    // if (isStore()) {
    //         $store_id = isStore()->id;
    //         $product_categories = ProductCategory::where('store_id', $store_id)->where('collection_type_id', '=', 1)->select('name', 'id')->get();
    //     }else{
    //         $product_categories = ProductCategory::where('collection_type_id', '=', 1)->select('name', 'id')->get();
    //     }

    //     $vendors = Vendor::all('store_name', 'id');
    //     $product_tags = Tags::where('tag_type', '=', 1)->get();
    //     $meat_types = ProductCategory::where('collection_type_id', '=', 2)->get();
    //     $brands = ProductCategory::where('collection_type_id', '=', 3)->get(); 

        return view('admin.report.report_lists');
    }   

    public function reportsByMonth(){

        return view('admin.report.report_lists_by_month');
    }
    // reports DETAIL PAGE
    public function reportDetail($report) {


            $top_cart_product = DB::table('top_cart_product')
                                ->join('product', 'top_cart_product.product_id', '=', 'product.id', 'left')
                                ->select('top_cart_product.id', 'top_cart_product.count', 'product.product_title', 'product.price', 'product.product_images', 'product.id')
                                ->get();
            
            // top selling product
            $top_selling_product = DB::table('order_detail')
                                        ->join('product', 'order_detail.product_id', '=', 'product.id', 'left' )
                                        ->select('product.product_title', 'product.product_images','product.price','order_detail.product_id', DB::raw('COUNT(order_detail.product_id) as total_product_record'))
                                        ->orderBy(DB::raw('LENGTH(order_detail.product_id)', 'ASC'))
                                        ->orderBy('order_detail.product_id', 'ASC')
                                        ->groupBy('order_detail.product_id')
                                        ->groupBy('product.price')
                                        ->groupBy('product.product_images')
                                        ->groupBy('product.product_title')
                                        ->get();
            // Low inventory list
            
            $low_inventory = DB::table('product')
                            ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left' )
                            ->select('product.*', 'inventory.quantity')
                            ->groupBy('product.id')
                            ->orderby('inventory.quantity','asc')
                            ->get();
            // total customer with total ammount
            $customer_list = DB::table('order_request')
                                ->select('user_id' , DB::raw('SUM(payble_amount) as total_amount, COUNT(id) as total_order'))
                                ->where('order_status' , '=' , 4)
                                ->groupBy('user_id')
                                ->get();
                                // dd($order);
                                     
             //  this is for products are on discount
            $discountedproduct = DB::table('product')
                        ->select('id','product_title', 'product_images', 'price', 'compare_price')
                        ->where('compare_price', '<>', 'NULL')
                        ->get();  
            //  sales by monthly
            // all orders
            //             $all_records = DB::table('order_request')
            //                      ->select('order_request.*')
            //                     ->get();
           $sales_monthly = DB::select("select DISTINCT MONTH(created_at) as created_at ,order_status  from order_request where order_status = '4'");
            // $sales_day = DB::select("select DISTINCT DAY(created_at) as day, MONTH(created_at) as mnth,order_status  from order_request where order_status = '4' ORDER BY MONTH(created_at) DESC" );
            $sales_day = DB::select("select DISTINCT DAY(created_at) as day, MONTH(created_at) as mnth,order_status , created_at  from order_request where order_status = '4' OR order_status = '5' GROUP BY DAY(created_at),  MONTH(created_at) ORDER BY created_at DESC" );
            // dd($sales_day);
            // filter by month
            // $currentMonth = date('m');
            // $sales_monthly = DB::table("order_request")
            // ->whereRaw('MONTH(created_at) = ?',[$currentMonth])
            // ->get();


            // $atten = OrderRequest::select(  DB::raw('DATE(created_at)')  )->distinct()->get();
            $sales = DB::select('select * from order_request');
            // total amont with total ammount
            $total_gross = DB::table('order_request')
                                ->select('user_id' , DB::raw('SUM(amount) as total_amount'))
                                 ->groupBy('user_id')
                                ->first();
            $total_discount = DB::table('order_request')
                                ->select('user_id' , DB::raw('SUM(discount) as total_discount, SUM(tax_rate_value) as tax_rate, SUM(custom_rate_value) as custom_shipping'))
                                ->first();


            $order_id = DB::table('order_request')
                            ->join('order_detail','order_detail.order_id','=' ,'order_request.id','left')
                            ->select('order_detail.*','order_request.id as orderid','order_request.order_status',DB::raw('SUM(order_request.amount) as gross_amount, SUM(order_request.payble_amount) as net_sale'))
                            ->groupBy('product_id')
                            ->where('order_status', '=' , 4)
                            ->paginate(15);
                            // dd($order_id);
            $discountrecords_qeury = DB::table('order_request')
                             ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                             ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                             ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                             ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                             ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name','users.email', 'order_status.status_title','order_status.id as order_id', 'payment_status.payment_status_name', 'payment_status.id as payment_id')
                             ->where('order_request.order_status','=', 4)
                             ->where('order_request.coupon_code', '<>' , NULL)
                             ->Where('order_request.coupon_code', '<>' , ' ');
                              if (isStore()) {
                $admin = isStore();
                  $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
                     $discountrecords_qeury->where('vendor_id', $vendor->id);

                 }
                   $salesdescounted = $discountrecords_qeury->orderBy('order_request.created_at', 'desc')
                             ->paginate(15);
                // dd($salesdescounted);
               
        return view('admin.report.report_detail', compact('report','top_cart_product','total_sales','todaysvis','total_order','total_order_offline','total_offline_sales','low_inventory','total_today_order','total_online_sales','customer_list','discountedproduct', 'top_selling_product','sales_monthly','sales','total_gross','total_discount','order_id','sales_day','salesdescounted'));


    }

    public function discountReportBydate(Request $request) {
       $start_date = $request->get('startdate');
        
        $end_date = $request->get('enddate');
        // $couponcode = $request->get('couponcode');
        // dd($end_date);
        $discountrecords_qeury = DB::table('order_request')
                             ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                             ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                             ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                             ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                             ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                             ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                             ->where( DB::raw('date(order_request.created_at)'), '>=', $start_date)
                             ->where( DB::raw('date(order_request.created_at)'), '<=', $end_date)
                             ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name','users.email','users.contact','users.notes', 'order_status.status_title','order_status.id as order_id', 'payment_status.payment_status_name', 'payment_status.id as payment_id','address.address', 'address.phone', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.phone as bphone', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
                             ->where('order_request.order_status','=', 4)
                             ->where('order_request.coupon_code', '<>' , NULL);
              if (isStore()) {
                $admin = isStore();
                  $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
                     $discountrecords_qeury->where('vendor_id', $vendor->id);

                 }
                $table = $discountrecords_qeury->orderBy('order_request.created_at', 'desc')
                             ->get();
                        // dd($table);
    $filename = "discountedorderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Order Code', 'Email', 'Financial Status', 'Paid at', 'Subtotal', 'Shipping', 'Taxes', 'Total', 'Discount Code', 'Discount Amount', 'Created at', 'Status', 'Billing Name','Billing Last Name', 'Billing Street','Billing City','Billing Zip','Billing Phone','Shipping Name','Notes','Payment Method'));

    foreach($table as $row) {
        
$address = Address::where('user_id', $row->user_id)->first();
$status = OrderStatus::where('id',$row->order_status)->first();
                // dd($status->status_title);
// echo $status['status_title']; 
// $string = implode(';', $status);
// $status = OrderStatus::where('id', $row->status)->first();
         if ($row->order_from == 0)
    {
         $orderfrom = "Offline";
    }
    else
    {
        $orderfrom = "Online";
    }
    if($row->vendor_id == 1)
    {
        $orderid= "D".$row->id;
    }
      
    else
    {

    $orderid= "G".$row->id;

    }
    if($row->contact == 'NULL')
    {
        $contact = $row->bphone;
    }
    else
    {
        $contact = $row->contact;
    }
        
        fputcsv($handle, array($orderid,$row->email,$row->payment_status_name,$row->created_at,$row->amount,$row->custom_rate_value,$row->tax_rate,$row->payble_amount,$row->coupon_code,$row->discount,$row->created_at,$status['status_title'],$row->first_name,$row->last_name,$row->address,$row->city,$row->zip,$contact,$row->first_name,$row->notes));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'discounted_Order_list.csv', $headers);

    }
// export data from sales by day (by date)
    public function daywiseReportBydate(Request $request) {
       $start_date = $request->get('startdate');
        
        $end_date = $request->get('enddate');

        // $table1 = DB::select(DB::raw("select DISTINCT DAY(created_at) as day, MONTH(created_at) as mnth,order_status  from order_request where Date(created_at) >= ".$start_date." AND Date(created_at) <= ".$end_date." AND order_status = '4' OR order_status = '5'  GROUP BY DAY(created_at),  MONTH(created_at) ORDER BY created_at DESC"));

        $table = DB::table('order_request')
        ->select("created_at","order_status")
        ->where( DB::raw('date(order_request.created_at)'), '<=', $end_date)
        ->where( DB::raw('date(order_request.created_at)'), '>=', $start_date)
        ->where("order_status","=",4)
        ->groupBy(DB::raw('Date(created_at)'))
        ->get();
        
                        // dd($table);
    $filename = "discountedorderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Date','Total Order', 'Gross sales', 'Discount', 'Returns', 'Net Sales', 'Shipping', 'Tax', 'Total Sales'));

    foreach($table as $row) {
                        
                            $str =$row->created_at;
                       

                              // $month1 = $row->created_at;
                              $month = mb_substr($str, 8, 2);
                              $month1 = mb_substr($str, 6, 2);
                            // dd($month); 

                            // $year = date("y",strtotime($row->day));
                           // $year = date("Y");  

                            // echo $month;
        $all_compeletd_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as gross_amount, SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                   ->whereRaw('extract(day from created_at) = ?', [$month])
                   ->whereRaw('extract(month from created_at) = ?', [$month1]);
          if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_compeletd_records_qeury->where('vendor_id', $vendor->id);

           }
            $all_compeletd_records = $all_compeletd_records_qeury->where('order_status','=', 4)->distinct()->distinct()->first();
                    
        $all_return_records_qeury = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount , SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                   ->whereRaw('extract(day from created_at) = ?', [$month])
                   ->whereRaw('extract(month from created_at) = ?', [$month1]);
        if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $all_return_records_qeury->where('vendor_id', $vendor->id);

           }
          $all_return_records = $all_return_records_qeury->where('order_status','=',5)
                    ->distinct()->first();

        $all_records= $all_compeletd_records->gross_amount + $all_return_records->return_amount;
            
        $totalorder = $all_compeletd_records->total_order + $all_return_records->total_order;

        $netsale= $all_records - $all_compeletd_records->discount - $all_return_records->return_amount;
        
        $totalsale = $netsale + $all_compeletd_records->shippingrate + $all_compeletd_records->tax;

        //  $monthNum = $month1;
        //  $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
        // $monthName;
        $y = date("Y");
        $datevalue = $month."-".$month1 .$y;
       

         if($all_compeletd_records->discount == '0' || $all_compeletd_records->discount == NULL )
         {
            $discount = "0.00";
         }
        else
        {
            $discount = $all_compeletd_records->discount;
        }
        if($all_return_records->return_amount == '0' || $all_return_records->return_amount == NULL )
        {
            $returnvalue = "0.00";
        }
        else
        {
            $returnvalue = $all_return_records->return_amount;
        }
        
        $netsale1 = round($netsale, 2);
        if($all_compeletd_records->shippingrate == '0' || $all_compeletd_records->shippingrate == NULL )
        {
            $shippingrate = "0.00";
        }
        else
        {
            $shippingrate = $all_compeletd_records->shippingrate;
        }
        fputcsv($handle, array($datevalue,$totalorder,$all_records,$discount,$returnvalue,$netsale1,$shippingrate,round($all_compeletd_records->tax, 2),round($totalsale, 2)));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'salesbyday_Order_list.csv', $headers);
    }
// end
// All data export in sales by product
    public function productReportAll(Request $request) {
        $order_id = DB::table('order_request')
                            ->join('order_detail','order_detail.id','=' ,'order_request.id','left')
                            ->select('order_detail.*','order_request.id as orderid','order_request.order_status',DB::raw('SUM(order_request.amount) as gross_amount, SUM(order_request.payble_amount) as net_sale'))
                            ->groupBy('product_id')
                            ->where('order_status', '=' , 4)
                            ->get();
        $filename = "orderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Product Title', 'Net Quantity', 'Total Sales'));

        foreach ($order_id as $uid) {
                                # code...
            $total_gross = DB::table('order_detail')
                                ->select('product_id','order_id' , DB::raw('SUM(quantity) as total_quantity'))
                                ->where('product_id','=',$uid->product_id)
                                ->first();
                                // dd($total_gross);
                    // dd($total_gross->total_quantity);

                                if($total_gross->total_quantity == NULL)
                                  {
                                      continue;
                                  }
                                  

               $sales_by_product = DB::table('product')
                            ->select('product_title','price')
                            ->where('id','=', $total_gross->product_id)
                            ->first();

                $return_amount = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount'))
                    ->where('status', '=', 5)
                    ->distinct()->first();

            
        
        $totalsale = $total_gross->total_quantity * $sales_by_product->price; 
        // dd("fdfsdfdfd");
        fputcsv($handle, array($sales_by_product->product_title,$total_gross->total_quantity,$totalsale));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'Product_quantity_list.csv', $headers);
}
    
// export product with quantity by date in sales by product

    public function salesbyproductReportBydate(Request $request) {

        $start_date = $request->get('startdate');
        $end_date = $request->get('enddate');

         $order_id = DB::table('order_request')
                            ->join('order_detail','order_detail.order_id','=' ,'order_request.id','left')
                            ->select('order_detail.*','order_request.id as orderid','order_request.order_status','order_request.created_at',DB::raw('SUM(order_request.amount) as gross_amount, SUM(order_request.payble_amount) as net_sale'))
                            ->groupBy('product_id')
                            ->where('order_status', '=' , 4)
                            ->where( DB::raw('date(order_request.created_at)'), '>=', $start_date)
                            ->where( DB::raw('date(order_request.created_at)'), '<=', $end_date)
                            ->get();
                            // dd($order_id);

        $filename = "orderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Product Title', 'Net Quantity', 'Total Sales'));

        foreach ($order_id as $uid) {
                                # code...
            $total_gross = DB::table('order_detail')
                                ->select('product_id','order_id' , DB::raw('SUM(quantity) as total_quantity'))
                                ->where('product_id','=',$uid->product_id)
                                ->where( DB::raw('date(created_at)'), '>=', $start_date)
                                ->where( DB::raw('date(created_at)'), '<=', $end_date)
                                ->first();
                                // dd($total_gross);
                    // dd($total_gross->total_quantity);

                                if($total_gross->total_quantity == NULL)
                                  {
                                      continue;
                                  }
                                  

               $sales_by_product = DB::table('product')
                            ->select('product_title','price')
                            ->where('id','=', $total_gross->product_id)
                            ->first();
        // dd($sales_by_product);

                $return_amount = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount'))
                    ->where('status', '=', 5)
                    ->where( DB::raw('date(order_request.created_at)'), '>=', $start_date)
                    ->where( DB::raw('date(order_request.created_at)'), '<=', $end_date)
                    ->distinct()->first();

            
        
        $totalsale = $total_gross->total_quantity * $sales_by_product->price; 
        // dd("fdfsdfdfd");
        fputcsv($handle, array($sales_by_product->product_title,$total_gross->total_quantity,$totalsale));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'Product_quantity_list_bydate.csv', $headers);
}


    public function reportBydate(Request $request) {

        $start_date = $request->get('startDate');
        $end_date = $request->get('endDate');

        // dd($start_date);


        // $sales_monthly = DB::select("select DISTINCT MONTH(created_at) as created_at ,order_status  from order_request where order_status = '4'");

        $all_records = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as gross_amount, SUM(payble_amount) as net_sale, COUNT(id) as total_order,SUM(discount) as discount,SUM(custom_rate_value) as shippingrate,SUM(tax_rate) as tax'))
                    ->where( DB::raw('date(created_at)'), '>=', $start_date)
                    ->where( DB::raw('date(created_at)'), '<=', $end_date)
                    ->where('order_status','=', 4)
                    ->first();
                    // dd($all_records);
       $return_amount = DB::table('order_request')
                    ->select('order_request.*', DB::raw('SUM(amount) as return_amount'))
                    ->where( DB::raw('date(created_at)'), '>=', $start_date)
                    ->where( DB::raw('date(created_at)'), '<=', $end_date)
                    ->where('status', '=', 5)
                    ->distinct()->first();
        $netsale= $all_records->gross_amount - $all_records->discount - $return_amount->return_amount;
        
        $totalsale = $netsale + $all_records->shippingrate + $all_records->tax; 

        // $orders = DB::table('order_request')
        //             ->where( DB::raw('date(created_at)'), '>', $start_date)
        //             ->where( DB::raw('date(created_at)'), '<', $end_date)
        //             ->selectRaw('sum(payble_amount) as totalSales')
        //             ->first();

        // $online_orders = DB::table('order_request')
        //             ->where( DB::raw('date(created_at)'), '>', $start_date)
        //             ->where( DB::raw('date(created_at)'), '<', $end_date)
        //             ->where('order_from', '=', 1)
        //             ->selectRaw('sum(payble_amount) as totalSales, count(id) as totalOrders')
        //             ->first();

        // $offline_orders = DB::table('order_request')
        //             ->where( DB::raw('date(created_at)'), '>', $start_date)
        //             ->where( DB::raw('date(created_at)'), '<', $end_date)
        //             ->where('order_from', '=', 0)
        //             ->selectRaw('sum(payble_amount) as totalSales, count(id) as totalOrders')
        //             ->first();

        $data['totalorder'] = "Rs. ".$all_records->total_order;
        $data['totalgrosssales'] = "Rs. ".$all_records->gross_amount;
        $data['discount'] = "Rs. ".$all_records->discount;
        $data['returnamnt'] = "Rs. ".$return_amount->return_amount;
        $data['netsale'] = $netsale;
        $data['shipping'] = "Rs. ".$all_records->shippingrate;
        $data['tax'] = $all_records->tax;

// return view('admin.report.report_detail', compact('sales_monthly'));
        return $data;
    }


}
