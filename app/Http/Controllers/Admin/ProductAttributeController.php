<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator, Redirect; 
use App\ProductAttribute;
use App\AttributeValue;

class ProductAttributeController extends Controller
{
    public function index(){
        $attributes = ProductAttribute::all();
        return view('admin/productattribute/index' , array('attributes'=>$attributes));
    }
    
    /* start add ProductAttribute function  */
    public function add(){
        
        $attributes = ProductAttribute::all();

        return view('admin.productattribute.add', compact('attributes'));
    }

    public function postadd(Request $request){
        $data = $request->all();	

        $check = Validator::make($data, ProductAttribute::$rules);
        // if the validator fails, redirect back to the form
        if ($check->fails()) {
            return Redirect::back()
                ->withErrors($check) // send back all errors to the login form
                ->withInput();
        } else {
            $attribute = new ProductAttribute();
            $attribute->name = $request->input('name');
            $attribute->save();

            if ($request->get('attribue_value')) {
	            foreach ($request->get('attribue_value') as $key => $value) {
	                $attribue_value = new AttributeValue;
	                $attribue_value->attribute_id = $attribute->id;
	                $attribue_value->value = $value;
	                $attribue_value->save();
	            }
            }

            return redirect('admin/attribute');
        }
    }
    /* end add attribute function  */
    
    /* edit attribute function  */
    public function edit($id){
        $attribute = ProductAttribute::find($id);

        return view('admin/productattribute/edit' , compact('attribute'));
    }
    public function postedit(Request $request , $id){
        $data = $request->all();
        $check = Validator::make($data, array(
            'name' => 'required|max:255',
        ));
        // if the validator fails, redirect back to the form
        if ($check->fails()) {
            return Redirect::back()
                ->withErrors($check) // send back all errors to the login form
                ->withInput();
        } else {
            $attribute = ProductAttribute::find($id);
            $attribute->name = $request->input('name');
            $attribute->save();

            if ($request->get('attribue_value')) {
                foreach ($request->get('attribue_value') as $key => $value) {
                    $attribue_value = new AttributeValue;
                    $attribue_value->attribute_id = $attribute->id;
                    $attribue_value->value = $value;
                    $attribue_value->save();
                }
            }
            return redirect('admin/attribute');
        }
    }
    /* end edit attribute function  */
    
    /* delete attribute function*/
    public function delete($id){
        $attribute = ProductAttribute::find($id);
        $attribute->delete();

        DB::table('attribute_value')->where('attribute_id', '=', $attribute->id)->delete();

        return redirect('admin/attribute');
    }
    /* end delete attribute function*/
}
