<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;
use DateTime;
use App\ProductCategory;
use App\Product;
use App\Coupon;
use App\DiscountProduct;
use App\DiscountProductCategory;
use App\Http\Requests\AdminCouponRequest;
use DB;
use App\Tags;
class CouponController extends Controller
{


	public function newCoupon() {

        $product_categories = ProductCategory::all('name', 'id');
        $products = Product::all('product_title', 'id');
        $stores = DB::table('vendors_details')->pluck('store_name','id')->toArray();
        $discount_tags = Tags::where('tag_type', '=', 4)->get();


		return view('admin.coupon.new_coupon', compact('product_categories', 'products', 'stores', 'discount_tags'));
	}

	public function editCoupon($id) {

		$coupon = Coupon::find($id);
        $product_categories = ProductCategory::all('name', 'id');
        $products = Product::all('product_title', 'id');
        $stores = DB::table('vendors_details')->pluck('store_name','id')->toArray();

		return view('admin.coupon.edit_coupon', compact('coupon','product_categories', 'products', 'stores'));
	}

	public function addNewCoupon(AdminCouponRequest $request, $id=null) {


		// dd("fdfdf");
		if ($request->get('exclude_product')) {
			$exclude_product = implode (",", $request->get('exclude_product'));
		}else{
			$exclude_product = 'NULL';
		}

		if ($request->get('exclude_collection')) {
			$exclude_collection = implode (",", $request->get('exclude_collection'));
		}else{
			$exclude_collection = 'NULL';
		}

		

		if ($id) {
			$coupon = Coupon::find($id);
		}else{
			$coupon = new Coupon;
		}

		$coupon->discount_coupon_code = $request->get('coupon_code');
		$coupon->discount_value = $request->get('discount_value');
		$coupon->coupon_type = $request->get('coupon_type');
		$date = DateTime::createFromFormat('d-m-Y H:i:s', $request->get('start_date'));
		$coupon->discount_type = $request->get('discount_type');
		$coupon->min_order_amount = $request->get('order_above');
		// $coupon->collection = $request->get('collection');
		$coupon->customer_type = $request->get('customer_group');
		// $coupon->product_id = $request->get('product');
		$coupon->start_date = $request->get('start_date');
		if ($request->get('end_date')) {
			$coupon->valid_till = $request->get('end_date');
		}else{
			$coupon->valid_till = NULL;
		}
		$coupon->exclude_product = $exclude_product;
		$coupon->exclude_collection = $exclude_collection;
		// $coupon->store_id = $request->get('store_type');
		$coupon->is_active = 1;
		if ($request->get('coupon_quantity') == 'limited') {
			$coupon->coupon_quantity = $request->get('coupon_quant');
		}else{
			$coupon->coupon_quantity = $request->get('coupon_quantity');
		}
		if ($request->get('limit_per_user') == 'on') {
			$coupon->limit_per_user = 1;
		}else{
			$coupon->limit_per_user = 0;
		}
		$coupon->save();

		if ($request->get('discount_tags')) {
            $tags = $request->get('discount_tags');
            $tag_ids = [];
            if ($tags) {
                foreach ($tags as $tag) {
                    $product_tag = Tags::where('id', '=', $tag)->select('id')->first();
                    if ($product_tag) {
                        $product_tag->field_id = $coupon->id;
                        $product_tag->save();
                        array_push($tag_ids, $product_tag->id);
                    } else {
                        $ptag = new Tags;
                        $ptag->name = $tag;
                        $ptag->tag_type = DISCOUNT_TAGS;
                        $ptag->field_id = $coupon->id;
                        $ptag->slug = str_slug($tag);
                        $ptag->save();
                        array_push($tag_ids, $ptag->id);
                    }
                }
            }
        }

		if ($request->get('collection')) {
			foreach ($request->get('collection') as $key => $value) {
				$discount_collection = new DiscountProductCategory;
				$discount_collection->discount_id = $coupon->id;
				$discount_collection->product_category_id = $value;
				$discount_collection->save();
			}
		}

		if ($request->get('product')) {
			foreach ($request->get('product') as $key => $value) {
				$discount_product = new DiscountProduct;
				$discount_product->discount_id = $coupon->id;
				$discount_product->product_id = $value;
				$discount_product->save();
			}
		}

		return redirect()->back()->with('success_msg', 'New Coupon Added Successfully.');
	}

	public function couponList(Request $request){

    	$query = $request->get('discount_tags');
		if ($query) {
			$coupons = DB::table('tags')
    				->join('discount_coupon', 'tags.field_id', '=', 'discount_coupon.id')
                    ->where('tags.name', 'LIKE', "%$query%")
                    ->where('tags.tag_type', DISCOUNT_TAGS)
                    ->select('discount_coupon.*')
                    ->get();
		}else{
        	$coupons = DB::table('discount_coupon')->get();
		}


        return view('admin.coupon.couponlist', compact('coupons'));
    }	

}