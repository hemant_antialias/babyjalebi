<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use App\User;
use Carbon\Carbon;
use App\Admin;
use DB;
use URL;
use Input;
use App\Order;
use App\Vendor;
use App\Inventory;
use App\OrderStatus;
use App\PaymentStatus;
use App\Product;
use App\Address;
use App\Payments;
use App\ProductCart;
use App\OrderDetail;
use App\Tags;
use App\OrderTag;
use App\OrderComments;
use App\DiscountCoupon;
use Redirect;
use File;
use PDF;
use Cart;
use Mail;
use Session;

class OrderController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }

    public function newOrder(Request $request) {
            
        Cart::destroy();

        if ($request->session()->has('pre_product_id')) {
            $request->session()->forget('pre_product_id');
        }
        if ($request->session()->has('product_ids')) {
            $request->session()->forget('product_ids');
        }

        if (isStore()) {
            $store_id = isStore()->id;
            $product_list = DB::table('product')->where('vendor_id', '=', $store_id)->pluck('product_title','id')->toArray();
        }else{
            $product_list = DB::table('product')->pluck('product_title','id')->toArray();
        }

        $select = ['0' => 'Select'];
        $vendor_list = DB::table('vendors_details')->pluck('store_name', 'id')->toArray();
        // dd(array_merge($select, $vendor_list));
        $customer = DB::table('users')->orderBy('first_name', 'asc')->pluck(DB::raw('CONCAT(first_name, " ", last_name) AS full_name'), 'id')->toArray();
        $order_tags = Tags::where('tag_type', '=', 2)->get();

        return view('admin.order.new_order',['order_tags'=> $order_tags, 'product_list' => $product_list,'vendor_list'=>$vendor_list,'customer' => $customer]);
    }

    public function createOrder(Request $request)
    {
        $cart_content = Cart::content();
        // dd($request->all());
        // $productids = Input::get('product_ids');

        // $product_ids = $request->session()->get('product_ids');
        // $productids = array_unique($product_ids);

        $customer_id = Input::get('customer_id');
        $quantitynew = Input::get('quantitynew');

        $address = DB::table('address')->where('user_id', $customer_id)->first();
        if (!$address) {
            
            $data['status'] = 1;
            $data['message'] = "Please Edit Address first.";

            return $data;
        }
        if (isStore()) {
            $store = isStore();
            $vendor = Vendor::where('admin_id', $store->id)->select('id')->first();
            $vendor_id = $vendor->id;

        }else{
            $vendor_id = Input::get('vendor_id');
        }
        
        $amount = Input::get('total_amount');
        $payble_amount = Input::get('final_total_amount');      
        $discount_value = Input::get('discount_value');
        $reason_value = Input::get('reason_value');
        $status = Input::get('status');

        $order = new Order;
        $order->order_code =str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
        $order->user_id = $customer_id;
        $order->vendor_id = $vendor_id;
        $order->order_currency = 'INR';
        $order->order_status = 1;
        $order->order_from = 0;
        $order->amount  = trim($amount);

        if(empty($discount_value)) {
            $order->discount  = 0.00;
        } else {
            $order->discount  = trim($discount_value);
            $order->discount_type = $request->get('discount_type');
            $order->coupon_code = $request->get('discount');
        }

        $order->reason = $reason_value;
        if ($request->get('custom_ship_name') || $request->get('custom_ship_value')) {
            $order->shipping = "custom";
        }else{
            $order->shipping = "free";
        }
        $order->custom_rate_name = $request->get('custom_ship_name');
        $order->custom_rate_value = $request->get('custom_ship_value');
        $order->tax_rate = $request->get('tax');
        $order->tax_rate_value = $amount*$request->get('tax')/100;

        $order->notes = $request->get('notes_value');     

        $order->payble_amount = trim($payble_amount);
        $order->location = '';
        $order->status = $status;
        $order->save();

        foreach ($cart_content as $product) {
            $orderDetail = new OrderDetail;
            $orderDetail->product_id = $product->id;
            $orderDetail->quantity = $product->qty;
            $orderDetail->order_id = $order->id;
            $orderDetail->save();
        
        }

        $order_detail = OrderDetail::where('order_id', $order->id)->get();

        foreach ($order_detail as $detail) {
            $productid = $detail->product_id;
            $quantity = $detail->quantity;
            $storeid = $detail->vendor_id;

            $inventory = Inventory::where('product_id', $productid)->first();
            if ($inventory) {
                $inventory->quantity = $inventory->quantity - $quantity;
                $inventory->save();
            }
        }

        if ($request->get('order_tag')) {
            $tags = $request->get('order_tag');
            $tag_ids = [];
            if ($tags) {
                foreach ($tags as $tag) {
                    $otag = Tags::where('id', '=', $tag)->select('id')->first();
                    if ($otag) {
                        $otag->field_id = $customer_id;
                        $otag->save();
                        array_push($tag_ids, $otag->id);
                    } else {
                        $ptag = new Tags;
                        $ptag->name = $tag;
                        $ptag->tag_type = ORDER_TAGS;
                        $ptag->field_id = $customer_id;
                        $ptag->slug = str_slug($tag);
                        $ptag->save();
                        array_push($tag_ids, $ptag->id);
                    }
                }
            }
        }

        // if (isset($tag_ids)) {
        //     foreach ($tag_ids as $tkey => $tvalue) {
        //         $product_tag = new OrderTag;
        //         $product_tag->order_id = $order->id;
        //         $product_tag->tag_id = $tvalue;
        //         $product_tag->save();
        //     }
        // }
        $user = User::where('id', $order->user_id)->first();
        $user_email = $user->email;
        $order_code = $order->order_code;
        
        if ($order->vendor_id == 1) {
            $oredrid = 'D'.$order->id;
        }
        if ($order->vendor_id == 3) {
            $oredrid = 'G'.$order->id;
        }
        try {
            Mail::send('mail.order.order_success', [], function($message) use($user_email, $oredrid){
                $message->from('admin@Babyjalebi.com');
                $message->to($user_email, "Lionfres Support")->subject("Order #".$oredrid." confirmed");
            });
        } catch (Exception $e) {
            
        }

        $data['status'] = $order->status;
        return $data;


    }

    public function order(Request $request) {
        $order_status = OrderStatus::pluck('status_title', 'id')->toArray();
        $payment_status = PaymentStatus::pluck('payment_status_name', 'id')->toArray();
        $querygetall = $request->get('data_from_table');
        $querygetall1 = $request->get('data_from_table1');
        

         if (isStore()) {
             $admin = isStore();
             $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

             $orders = DB::table('order_request')
                 ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                 ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                 ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                 ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                 ->where('order_request.vendor_id', '=', $vendor->id)
                 ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name','users.email', 'order_status.status_title','order_status.id as order_id', 'payment_status.payment_status_name', 'payment_status.id as payment_id')
                 ->orderBy('order_request.created_at', 'desc')
                 ->whereIn('order_request.order_status', [1,3,4,5])
                 ->paginate(50);
                 if($querygetall1)
                 {
                    // dd($vendor->id);
                    $orders1 = DB::table('order_request')
                            ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                            ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                            ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                            ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                            ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                            ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                            ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name','users.email', 'order_status.status_title','order_status.id as order_id', 'payment_status.payment_status_name', 'payment_status.id as payment_id','baddress.address','baddress.address2','address.address','address.address2')
                            ->orderBy('order_request.created_at', 'desc')
                            ->whereIn('order_request.order_status', [1,3,4,5])
                            ->where(function ($query) use ($querygetall1) {
                                    $query->orWhere('users.first_name', 'LIKE', "%$querygetall1%")
                                            ->orWhere('users.last_name', 'LIKE', "%$querygetall1%")
                                            ->orWhere('baddress.address', 'LIKE', "%$querygetall1%")
                                            ->orWhere('address.address', 'LIKE', "%$querygetall1%")
                                            ->orWhere('address.address2', 'LIKE', "%$querygetall1%")
                                            ->orWhere('users.email', 'LIKE', "%$querygetall1%")
                                            ->orWhere('users.contact', 'LIKE', "%$querygetall1%")
                                            ->orWhere('users.city', 'LIKE', "%$querygetall1%")
                                            ->orWhere('baddress.address2', 'LIKE', "%$querygetall1%");
                                })
                            ->having('order_request.vendor_id','=', $vendor->id)
                            ->distinct()
                            ->simplePaginate (20)->setPath ( '' );
                $orders = $orders1->appends ( array (
                'data_from_table1' => Input::get ( 'data_from_table1' ) 
        ) );
                 }
        }elseif ($querygetall) {
                    // dd($querygetall);
            $orders1 = DB::table('order_request')
                            ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                            ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                            ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                            ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                            ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                            ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                            ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name','users.email', 'order_status.status_title','order_status.id as order_id', 'payment_status.payment_status_name', 'payment_status.id as payment_id','baddress.address','baddress.address2','address.address','address.address2')
                            ->orderBy('order_request.created_at', 'desc')
                            ->whereIn('order_request.order_status', [1,3,4,5])
                            ->where(function ($query) use ($querygetall) {
                                    $query->orWhere('users.first_name', 'LIKE', "%$querygetall%")
                                            ->orWhere('users.last_name', 'LIKE', "%$querygetall%")
                                            ->orWhere('baddress.address', 'LIKE', "%$querygetall%")
                                            ->orWhere('address.address', 'LIKE', "%$querygetall%")
                                            ->orWhere('address.address2', 'LIKE', "%$querygetall%")
                                            ->orWhere('users.email', 'LIKE', "%$querygetall%")
                                            ->orWhere('users.contact', 'LIKE', "%$querygetall%")
                                            ->orWhere('baddress.address2', 'LIKE', "%$querygetall%");
                                })
                            ->distinct()
                            ->paginate (50)->setPath ( '' );
                $orders = $orders1->appends ( array (
                'data_from_table' => Input::get ( 'data_from_table' ) 
        ) );
                // dd($orders1);

         }else{
             $orders = DB::table('order_request')
                             ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                             ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                             ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                             ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                             ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name','users.email', 'order_status.status_title','order_status.id as order_id', 'payment_status.payment_status_name', 'payment_status.id as payment_id')
                             ->whereIn('order_request.order_status', [1,2,3,4,5])
                             ->orderBy('order_request.created_at', 'desc')
                             ->paginate(50);
         }
 
         return view('admin.order.order_list', compact('orders', 'order_status', 'payment_status'));
     }

    public function abondand(Request $request) {

        $order_status = OrderStatus::pluck('status_title', 'id')->toArray();
        
        if (isStore()) {
             $admin = isStore();
             $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

             $orders = DB::table('order_request')
                        ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                        ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                        ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                        ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name', 'order_status.status_title','order_status.id as order_id')
                        ->where('order_request.vendor_id', '=', $vendor->id)
                        ->where('order_request.order_status', 7)
                        ->orderBy('order_request.created_at', 'desc')
                        ->get();
        }
        else{
            $orders = DB::table('order_request')
                        ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                        ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                        ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                        ->select('order_request.*','vendors_details.store_name', 'users.first_name', 'users.last_name', 'order_status.status_title','order_status.id as order_id')
                        ->where('order_request.order_status', 7)
                        ->orderBy('order_request.created_at', 'desc')
                        ->get();
        } 


        return view('admin.order.abondand_list', compact('orders', 'order_status'));

    }

    public function addProductToSession(Request $request) {
        

        $product_ids = [];

        $product_id = array_push($product_ids, $request->get('product_id'));
                

        if ($request->session()->has('product_ids')) {
            $session_product_ids = $request->session()->get('product_ids');
            $product_ids = array_merge($product_ids, $session_product_ids);
        }
        
        $request->session()->put('product_ids', $product_ids);
        
        $data['status'] = 1;
        
        return $data;  
    }

    public function removeProductToSession(Request $request) {
        
        $product_id = $request->get('product_id');

        if($request->session()->get('product_ids')){
            foreach ($request->session()->get('product_ids') as $key => $value) {
                if($value == $product_id){
                    $event_data_display = Session::get('product_ids');
                    unset($event_data_display[$key]);
                    Session::set('product_ids', $event_data_display);
                }
            }
        }
        
        $data['status'] = 1;
        
        return $data;  
    }

    public function addToCart(Request $request){

        if (isStore()) {
            $store = isStore();
            $store_detail = Vendor::where('admin_id', '=', $store->id)->select('id')->first();        
            $store_id = $store_detail->id;
            
        }else{
            $store_id = $request->get('store_id');
        }

        $product_id = $request->get('product_id');
        $product_title = $request->get('product_title');
        $product_price = $request->get('product_price');
        $quantity = $request->get('quantity');
        $image_url = $request->get('image_url');

        $product_detail = Product::where('id', $product_id)->select('tax', 'price')->first();
        $product = Inventory::where('product_id', $product_id)->first();

        $data = Cart::search(function ($cartItem, $rowId) use ($product_id) {
            return $cartItem->id == $product_id;
        });
        // dd($data);

        if (!$data->isEmpty()) {
            // dd('dd');
            $qty = '';
            foreach ($data as $value) {
                $qty = $value->qty;
            }

            if ($product && $qty >= $product->quantity) {
                
                $data['cart_count'] = Cart::count();
                $data['status'] = 0;
                $data['message'] = "You can add only ".$product->quantity." ".$product_title." to the cart.";
                // dd($data);
                return $data;
            }
        }

        if ($product && $quantity > $product->quantity) {

                $data['cart_count'] = Cart::count();
                $data['status'] = 0;
                $data['message'] = "You can add only ".$product->quantity." ".$product_title." to the cart.";
                // dd($data);
                return $data;
        }



        // Add entry to top cart product and update count
        // $top_cart_product = TopCartProduct::where('product_id', $product_id)->first();
        // if ($top_cart_product) {
        //     $top_cart_product->count += 1;
        //     $top_cart_product->save();
        // }else{
        //     $top_cart_product = new TopCartProduct;
        //     $top_cart_product->product_id = $product_id;
        //     $top_cart_product->count = 1;
        //     $top_cart_product->save();
        // }
            
        $tax = $product_detail->tax/100*$product_detail->price;

        $cartItem = Cart::add($product_id, $product_title, $quantity, $product_price, ['image_url' => $image_url,'store_id' => $store_id, 'tax' => $tax, 'discount' => 0]);
        Cart::associate($cartItem->rowId, '\App\Product');

        $data['status'] = 1;
        $data['message'] = "Product Added.";
        // dd($data);
        return $data;
    }


    public function addToOrder(Request $request){

        // if ($request->session()->has('product_ids')) {
        //     $product_ids = $request->session()->get('product_ids'); 
        // }else{
        //     $product_ids = $request->get('product_ids'); 
        // }
        // if (isStore()) {
        //     $store = isStore();
        //     $store_detail = Vendor::where('admin_id', '=', $store->id)->select('id')->first();        
        //     $store_id = $store_detail->id;
            
        // }else{
        //     $store_id = $request->get('store_id');
        // }

        // // array merge will work here
        // if ($request->session()->has('pre_product_id')) {
        //     $session_product_ids = $request->session()->get('pre_product_id');
        //     $product_ids = array_merge($product_ids, $session_product_ids);
        //     // dd($product_ids);
        // }

        // // $request->session()->forget('pre_product_id');
        // $request->session()->put('pre_product_id', $product_ids);
        
        
        // $products = Product::whereIn('id', $product_ids)->select('id','product_title','product_images', 'price','tax', 'i_sku')->get();
        // Cart::destroy();
        // foreach ($products as $product) {
        //     $product_id = $product->id;
        //     $product_title = $product->product_title;
        //     $product_price = $product->price;
        //     $quantity = 1;
        //     $tax = $product->tax/100*$product->price;
        //     $image_url = URL::to('/product_images/'.$product->id.'/featured_images/'.$product->product_images);
            
        //     $cartItem = Cart::add($product_id, $product_title, $quantity, $product_price, ['image_url' => $image_url,'tax' => $tax, 'i_sku' => $product->i_sku, 'discount' => 0, 'store_id' => $store_id, 'is_coupon' => 0, 'is_not_coupon' => 0]);
        //     Cart::associate($cartItem->rowId, '\App\Product');
        // }

        return view('admin.order.order_detail', compact('products', 'pdiscount', 'fdiscount', 'coupon_discount'));   
    }

    public function calcTotalQty(Request $request)
    {
        $id = $request->get('id');
        $qty = $request->get('qty');
        $data = Cart::get($id);
        $product_id = $data->id;
        $store_id = $data->options->store_id;

        $inventory = Inventory::where('product_id', $product_id)
                                ->first();
        if (!$inventory) {
            
            $datas['status'] = 0;
            $datas['message'] = "There is no inventory of this product.";

            return $datas;        
        }                        
        
        if ($inventory && $qty > $inventory->quantity) {

            $datas['status'] = 0;
            $datas['message'] = "You can add only ".$inventory->quantity." quantity to this product.";

            return $datas;
        }

        Cart::update($id, $qty);

        return view('admin.order.order_detail');   
    }

    public function deleteProductFromSession(Request $request){
    
        $id = $request->get('id');
        $product_id = $request->get('pid');

        if($request->session()->get('pre_product_id')){
            foreach ($request->session()->get('pre_product_id') as $key => $value) {
                if($value == $product_id){
                    $event_data_display = Session::get('pre_product_id');
                    unset($event_data_display[$key]);
                    Session::set('pre_product_id', $event_data_display);
                }
            }
        }

        if ($request->session()->has('product_ids')) {
            if($request->session()->get('product_ids')){
                foreach ($request->session()->get('product_ids') as $key => $value) {
                    if($value == $product_id){
                        $event_data_display = Session::get('product_ids');
                        unset($event_data_display[$key]);
                        Session::set('product_ids', $event_data_display);
                    }
                }
            }
        }

        Cart::remove($id);

        return view('admin.order.order_detail');   
    }


    public function bookOrder(Request $request) {
        $product_ids = $request->get('product_ids');
        $products = Product::whereIn('id', $product_ids)->select('product_title', 'price', 'i_sku')->get();

        $data['message'] = "Saved as Draft";
        $data['status'] = 1;

        return $data;
    }

    public function addDiscounts(Request $request) {
        $cart_content = Cart::content();
        $cart_subtotal = Cart::subtotal('2', '.', '');
        $store_id = $request->get('store_id');
        $reason = $request->get('reason');
        if ($request->get('fdiscount')) {
            foreach($cart_content as $detail){

                $product_deatil = Product::where('id', $detail->id)->select('tax')->first();

                // $product_discount = $request->get('pdiscount')/100*$detail->price*$detail->qty;
                $discount = $request->get('fdiscount')*$detail->price*$detail->qty/$cart_subtotal;


                $product_after_discount = $detail->price*$detail->qty-$discount; 
                $tax = $product_deatil->tax;
                $product_tax = $tax/100*$product_after_discount;


                Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $product_tax, 'discount' => $discount, 'store_id' => $store_id, 'is_coupon' => 1]]);

            }
        }

        if ($request->get('pdiscount')) {
            foreach($cart_content as $detail){

                $product_deatil = Product::where('id', $detail->id)->select('tax')->first();

                $product_discount = $request->get('pdiscount')/100*$detail->price*$detail->qty; 

                $product_after_discount = $detail->price*$detail->qty-$product_discount; 
                $tax = $product_deatil->tax;
                $product_tax = $tax/100*$product_after_discount;


                Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $product_tax, 'discount' => $product_discount, 'store_id' => $store_id, 'is_coupon' => 1]]);


            }
        }
        if ($request->get('discount_coupon_code')) {
            $discount_coupon_code = $request->get('discount_coupon_code');

            $coupon_discount = DB::table('discount_coupon')
                            ->leftjoin('discount_products','discount_coupon.id','discount_products.discount_id')
                            ->leftjoin('discount_product_categories','discount_coupon.id','discount_product_categories.discount_id')
                            ->where('discount_coupon.discount_coupon_code', $discount_coupon_code)
                            ->select('discount_coupon.id','discount_coupon.discount_coupon_code','discount_coupon.coupon_type', 'discount_coupon.discount_value','discount_coupon.product_id','discount_coupon.collection','discount_coupon.discount_type','discount_coupon.min_order_amount')
                            ->first();

            if ($coupon_discount) {
                if ($coupon_discount->coupon_type == 1) {
                  foreach($cart_content as $detail){
                
                    $product_deatil = Product::where('id', $detail->id)->select('tax')->first();
                    $discount = $coupon_discount->discount_value*$detail->price*$detail->qty/$cart_subtotal;

                    $product_after_discount = $detail->price*$detail->qty-$discount; 
                    $tax = $product_deatil->tax;
                    $product_tax = $tax/100*$product_after_discount;

                    Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $product_tax, 'discount' => $discount, 'store_id' => $store_id, 'is_coupon' => 1]]);

                    }
                } else {
                    if ($coupon_discount->discount_type == "all_orders" || $coupon_discount->discount_type == null) {
                        foreach($cart_content as $detail){

                            $product_deatil = Product::where('id', $detail->id)->select('tax')->first();

                            $product_discount = $coupon_discount->discount_value/100*$detail->price*$detail->qty; 

                            $product_after_discount = $detail->price*$detail->qty-$product_discount; 
                            $tax = $product_deatil->tax;
                            $product_tax = round($tax/100*$product_after_discount, 2);


                            Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $product_tax, 'discount' => $product_discount, 'store_id' => $store_id, 'is_coupon' => 1]]);

                        }
                    }
                    if ($coupon_discount->discount_type == "collection") {
                        foreach($cart_content as $detail){

                        $discount_product_category = DB::table('discount_product_categories')
                                                            ->where('discount_id', $coupon_discount->id)
                                                            ->select('product_category_id')
                                                            ->get();

                            $product_deatil = Product::where('id', $detail->id)
                                                        ->select('id','tax', 'product_category', 'meat_type', 'brand_type')
                                                        ->first();
                            $check_discount = DB::table('discount_product_categories')
                                                ->where('discount_id', $coupon_discount->id)
                                                ->where('product_category_id', $product_deatil->product_category)
                                                ->select('product_category_id')
                                                ->first();
                                                
                            if ($check_discount) {
                                $product_discount = $coupon_discount->discount_value/100*$detail->price*$detail->qty; 

                                $product_after_discount = $detail->price*$detail->qty-$product_discount; 
                                $tax = $product_deatil->tax;
                                $product_tax = round($tax/100*$product_after_discount, 2);


                                Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $product_tax, 'discount' => $product_discount, 'store_id' => $store_id, 'is_coupon' => 1]]);
                            }else{
                                continue;
                            }

                        }
                    }
                }
                if ($coupon_discount->discount_coupon_code) {
                    $coupon_discount_code = $coupon_discount->discount_coupon_code;
                }else{
                    $coupon_discount_code = '';
                }
            }else{

              $err_message = 1;

              return view('admin.order.order_detail', compact('err_message'));   
            }
        }

        return view('admin.order.order_detail', compact('coupon_discount_code','reason'));   
    }

    public function removeDiscounts(Request $request){
        $cart_content = Cart::content();
        $store_id = $request->get('store_id');
        foreach($cart_content as $detail){

            $product_id = $detail->id;
            $product = Product::find($product_id);
            $tax = $product->tax/100*$product->price;
            $tax_price = $tax*$detail->qty;
            
            Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $tax_price, 'discount' => 0.00, 'store_id' => $store_id,'is_coupon' => 0, 'is_not_coupon' => 1]]);
        }

        return view('admin.order.order_detail');   
    }

    public function addShipping(Request $request){
        $cart_total = $request->get('totalValue');
        $shipping = $request->get('shipping');
        $total_amount = $cart_total + $shipping;

        $data['total'] = $total_amount;
        $data['shipping_name'] = $request->get('shipping_name');
        $data['shipping'] = $request->get('shipping');

        return $data;
    }

    // public function removeShipping(Request $request){

    //     $cart_total = Cart::subtotal('2', '.', '');
    //     $shipping = 100;
    //     $total_amount = $cart_total - $shipping;

    //     return view('admin.order.order_detail', compact('total_amount'));   
    // }


    public function orderDetail($ordercode){

        $order = DB::table('order_request')
                        ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                        ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                        ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                        ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                        ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                        ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                        ->join('payments', 'order_request.id', '=', 'payments.order_id', 'left')
                        ->where('order_request.order_code', $ordercode)
                        ->select('order_request.*','vendors_details.store_name','order_status.status_title','payment_status.payment_status_name', 'users.id as userid','users.first_name', 'users.last_name', 'users.email', 'users.contact','users.notes as note', 'address.address', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default','payments.payment_gateway_id')
                        ->first();

        if ($order) {
            $order_detail = DB::table('order_detail')
                                ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                                ->where('order_detail.order_id', '=', $order->id)
                                ->select('order_detail.*','product.product_title', 'product.price','product.product_images')
                                ->get();
            
        // dd($order_detail);
            $address = Address::where('user_id', $order->user_id)->orderBy('created_at', 'desc')->first();
            // dd($address);

            // $baddress = Address::where('user_id', $order->user_id)->where('is_billing', 1)->first();

            // $order = DB::table('order_request')
            // ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
            // ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
            // ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
            // ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
            // ->where('order_request.order_code', $ordercode)
            // ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email','users.contact', 'address.address', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
            // ->first();
            // dd($order);
            $tags = DB::table('order_request')
                        ->join('order_tags', 'order_request.id', '=', 'order_tags.order_id', 'left')
                        ->join('tags', 'order_tags.tag_id', '=', 'tags.id')
                        ->select('tags.name', 'tags.slug')
                        ->where('order_request.id', '=', $order->id)
                        ->get();

            $ordercomment = OrderComments::where('order_code', $order->order_code)->orderBy('created_at', 'desc')->get();

            return view('admin.order.order_detail_by_id', compact('order', 'order_detail', 'tags', 'ordercomment', 'address', 'baddress'));
        }else{
            return redirect('/admin/order')->with('message', 'Order Not Found.');
        }               
    }

    public function showProductByKeyword(Request $request){
    
        $pName = $request->get("pName");

        // if (isStore()) {
        //     $store = isStore()->id;
        //     $vendor = Vendor::where('admin_id', $store)->select('id')->first();
        //     $store_id = $vendor->id;
        // }else{
        //     $store_id = $request->get("store_id");
        // }

        
        // $products = DB::table('product')->where('vendor_id', '=', $store_id)->where('product_title', 'LIKE', '%'.$pName.'%')->orderBy('product_title', 'asc')->get();
         $products = DB::table('product')
            ->join('inventory' , 'product.id','=','inventory.product_id')->select('product.*', 'inventory.quantity')->where('product.product_title', 'LIKE', '%'.$pName.'%')->orderBy('product.product_title', 'asc')->get();


       
       return view('admin.product.all_products', compact('products'));
    }

    public function changeOrderStatus(Request $request){

        $id = $request->get('id');
        $status = $request->get('status');
        $order = Order::find($id);
        $order->order_status = $status;
        $order->save();
        // dd($order);
        if ($request->get('status') === '5') {
            $order_detail = OrderDetail::where('order_id', $id)->get();
            foreach ($order_detail as $detail) {
                $store_id = $detail->vendor_id;
                $product_id = $detail->product_id;
                $quantity = $detail->quantity;
                $inventory = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();
                if ($inventory) {
                    $inventory->quantity = $inventory->quantity + $quantity;
                    $inventory->save();
                }
            }
        }

        if ($request->get('status') === '1') {
            $order->status = 5;
            $order->created_at = Carbon::now();
            $order->updated_at = Carbon::now();
            $order->save();
        }

        // if ($request->get('status') === '4') {
        //     $user_id = $order->user_id;
        //     $user = User::where('id', $user_id)->select('email', 'first_name')->first();
        //     $user_email = $user->email;
        //     $order_count = Order::where('user_id', $user_id)->count();
        //     $oredrid = $order->id;

        //     $detail = DB::table('order_request')
        //                 ->join('order_detail', 'order_request.id', '=', 'order_detail.order_id', 'left')
        //                 ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
        //                 ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
        //                 ->select('users.email','users.first_name', 'product.product_title', 'product.price', 'order_detail.quantity', 'order_request.payble_amount', 'order_request.discount','order_request.custom_rate_value', 'order_request.order_code','order_request.tax_rate')
        //                 ->where('order_detail.order_id', '=', $order->id)
        //                 ->get();

        //     $first = $detail->first();

        //     $similar_products = DB::table('product')
        //                         ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
        //                         ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
        //                         ->orderBy(DB::raw('ABS(`price` - '.$first->price.')'))
        //                         ->where('product.status', 1)
        //                         ->select('product.*', 'product_category.slug as collection_slug', 'inventory.quantity as totalquantity')
        //                         ->take(2)
        //                         ->get();

        //     $data = [
        //         'detail' => $detail,
        //         'name' => $user->first_name,
        //         'discount' => $order->discount,
        //         'total_amount' => $order->payble_amount,
        //         'tax' => $order->tax_rate,
        //         'shipping' => $order->custom_rate_value,
        //         'similar_products' => $similar_products
        //     ];

        //     if ($order_count == 1) {
                
        //         try {
        //             Mail::send('mail.discount.fifteenpercent', $data, function($message) use($user_email, $oredrid){
        //                 $message->from('admin@Babyjalebi.com');
        //                 $message->to($user_email, "Lionfres Support")->subject("Order Invoice #".$oredrid." confirmed");
        //             });
        //         } catch (Exception $e) {
                        
        //         }
        //     }

        //     if ($order_count > 1) {
                
        //         try {
        //             Mail::send('mail.discount.tenpercent', $data, function($message) use($user_email, $oredrid){
        //                 $message->from('admin@Babyjalebi.com');
        //                 $message->to($user_email, "Lionfres Support")->subject("Order Invoice #".$oredrid." confirmed");
        //             });
        //         } catch (Exception $e) {
                        
        //         }
        //     }
        // }

        $data['status'] = 1;
        return $data;
    }

    public function changePaymentStatus(Request $request){

        $id = $request->get('id');
        $status = $request->get('status');
        $order = Order::find($id);
        $order->status = $status;
        $order->save();

        $data['status'] = 1;
        return $data;
    }

    public function discountByCoupon(Request $request){

        $discount_coupon_code = $request->get('discount_coupon_code');
        $product_ids = $request->get('product_ids');
        $totalValue = $request->get('totalValue');
        $customer_id = $request->get('customer_id');
        
        
    //    $discountQuery = DiscountCoupon::where('discount_coupon_code', $discount_coupon_code)->select('id','coupon_type', 'discount_value','product_id','collection','discount_type','min_order_amount')->first();
//dd($discount_coupon_code);
        $discountQuery = DB::table('discount_coupon')
                            ->leftjoin('discount_products','discount_coupon.id','discount_products.discount_id')
                            ->leftjoin('discount_product_categories','discount_coupon.id','discount_product_categories.discount_id')
                            ->where('discount_coupon.discount_coupon_code', $discount_coupon_code)
                            ->select('discount_coupon.id','discount_coupon.discount_coupon_code','discount_coupon.coupon_type', 'discount_coupon.discount_value','discount_coupon.product_id','discount_coupon.collection','discount_coupon.discount_type','discount_coupon.min_order_amount')
                            ->first();
                            //dd($discountQuery);

        if(count($discountQuery)>0) {
            //dd("ddd");
            //dd($discountQuery->min_order_amount);
            if ($discountQuery->discount_type=="all_orders" && $discountQuery->discount_type!="customer_in_group")
            {
                //dd("aa");
                $discountValue = DiscountCoupon::where('discount_coupon_code', $discount_coupon_code)
                                                  ->select('id','discount_coupon_code', 'coupon_type', 'discount_value','product_id','collection')
                                                  ->first();
                $data['discount_value']=$discountValue->discount_value;
                $data['discount_code']=$discountValue->discount_coupon_code;
                $data['coupon_type']=$discountValue->coupon_type;
                $data['product_id']=$discountValue->product_id;
                $data['message']="Coupon applied successfully.";
                $data['status']="1";
            }
             elseif ($discountQuery->discount_type=="minimum_order"  && $discountQuery->discount_type!="customer_in_group") 
            {  
                //dd("ddd");
                    if(trim($totalValue) > $discountQuery->min_order_amount) 
                    {
//                      dd("qqqqqqqqqq");
                    $discountForMinOrder = DiscountCoupon::where('discount_coupon_code', $discount_coupon_code)
                                                        ->select('id','discount_coupon_code', 'coupon_type','discount_value','product_id','collection')
                                                        ->first();
    
                    $data['discount_value']=$discountForMinOrder->discount_value;
                    $data['discount_code']=$discountForMinOrder->discount_coupon_code;
                    $data['coupon_type']=$discountForMinOrder->coupon_type;
                    $data['product_id']=$discountForMinOrder->product_id;
                    $data['message']="Coupon applied successfully";
                    $data['status']="1";
                    } else {
                $data['discount_value']=0;
                $data['coupon_type']=0;
                $data['message']="Minimum order should be ".$discountQuery->min_order_amount.""; 
                    }
                } 
            else if ((in_array($discountQuery->product_id, $product_ids) || in_array($discountQuery->collection, $product_ids)) && $discountQuery->discount_type!="customer_in_group")
            {
                $discountValue = DiscountCoupon::where('discount_coupon_code', $discount_coupon_code)->select('id','discount_coupon_code','coupon_type', 'discount_value','product_id','collection')->first();
                $data['discount_value']=$discountValue->discount_value;
                $data['discount_code']=$discountValue->discount_coupon_code;
                $data['coupon_type']=$discountValue->coupon_type;
                $data['product_id']=$discountValue->product_id;
                $data['message']="Coupon applied successfuly";
                $data['status']="1";

            }
            else if ($discountQuery->discount_type=="customer_in_group")
            {
                    if($discountQuery->min_order_amount!='') {
                        if($totalValue > $discountQuery->min_order_amount) {
                            //dd("test");
                        $discountValue = DiscountCoupon::where('discount_coupon_code', $discount_coupon_code)->select('id','discount_coupon_code', 'coupon_type', 'discount_value','product_id','collection','discount_type')->first();
                        
                        $data['discount_type']=$discountValue->discount_type;
                        //dd($customer_id);
                        $checkUser = DB::table('order_request')
                                    ->where('user_id', $customer_id)
                                    ->first();
                            if(count($checkUser)>0) {
                                //dd("ddfgg");
                            $data['discount_value']=$discountValue->discount_value;
                            $data['discount_code']=$discountValue->discount_coupon_code;
                            $data['coupon_type']=$discountValue->coupon_type;
                            $data['product_id']=$discountValue->product_id;
                            $data['message']="Coupon applied successfully";
                            $data['status']="1";
                            }
                        } else {

                            $data['message']="Minimum amount should be ".$discountQuery->min_order_amount."--";
                        }

                } else {
                    if($discountQuery->min_order_amount=='') {

                        $discountValue = DiscountCoupon::where('discount_coupon_code', $discount_coupon_code)->select('id','discount_coupon_code','coupon_type', 'discount_value','product_id','collection','discount_type')->first();
                        
                        $data['discount_type']=$discountValue->discount_type;
                        //dd($customer_id);
                        $checkUser = DB::table('order_request')
                                    ->where('user_id', $customer_id)
                                    ->first();
                        if(count($checkUser)>0) {
                            //dd("ddfgg");
                        $data['discount_value']=$discountValue->discount_value;
                        $data['discount_code']=$discountValue->discount_coupon_code;
                        $data['coupon_type']=$discountValue->coupon_type;
                        $data['product_id']=$discountValue->product_id;
                        $data['message']="Coupon applied successfuly";
                        $data['status']="1";
                        }
                    }
                }
                
            }
            else
              {
                $data['discount_value']=0;
                $data['coupon_type']=0;
                $data['message']="This coupon code is not valid";
              }
        } else {
                $data['discount_value']=0;
                $data['coupon_type']=0;
                $data['message']="This coupon code is not valid"; 
        }

       return $data;
    }
    public function exportList(Request $request){

        $tweets = OrderDetail::all();

    // the csv file with the first row
    $output = implode(",", array('tweet text', 'screen name', 'name', 'created at'));

    foreach ($tweets as $row) {
        // iterate over each tweet and add it to the csv
        $output .=  implode(",", array($row['tweet_text'], $row['screen_name'], $row['name'], $row['created_at'])); // append each row
    }

    // headers used to make the file "downloadable", we set them manually
    // since we can't use Laravel's Response::download() function
    $headers = array(
        'Content-Type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename="tweets.csv"',
        );

    // our response, this will be equivalent to your download() but
    // without using a local file
    return Response::make(rtrim($output, "\n"), 200, $headers);
}

     public function printInvoice(Request $request){
        $ordercode = $request->get('order_code');
        $logo = $request->get('logoimg');
        // dd($ordercode);

        $order = DB::table('order_request')
                        ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                        ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                        ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                        ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                        ->join('payments', 'order_request.id', '=', 'payments.order_id', 'left')
                        ->where('order_request.order_code', $ordercode)
                        ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email', 'users.contact', 'address.address', 'address.address2', 'address.country', 'address.phone', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.phone as bphone', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default','payments.payment_gateway_id')
                        ->first(); 
        
            $order_detail = DB::table('order_detail')
                                ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                                ->where('order_detail.order_id', '=', $order->id)
                                ->select('order_detail.*','product.product_title', 'product.price','product.product_images')
                                ->get();
            
            $address = Address::where('user_id', $order->user_id)->first();
            $baddress = Address::where('user_id', $order->user_id)->where('is_billing', 1)->first();


            $tags = DB::table('order_request')
                        ->join('order_tags', 'order_request.id', '=', 'order_tags.order_id', 'left')
                        ->join('tags', 'order_tags.tag_id', '=', 'tags.id')
                        ->select('tags.name', 'tags.slug')
                        ->where('order_request.id', '=', $order->id)
                        ->get();

            $ordercomment = OrderComments::where('order_code', $order->order_code)->orderBy('created_at', 'desc')->get();

            return view('mail.order.invoice', compact('order', 'order_detail', 'tags', 'ordercomment','logo', 'ordercomment', 'address', 'baddress'));


     }
    public function sendInvoice(Request $request){
        $ids = $request->get('product_ids');


        return view('mail.order.invoice' , compact('ids'));

        // $user_id = $request->get('customer_id');
        // $user = User::find($user_id);

        // $pdf_arr = [];
        // $outputName = str_random(4).time();
        // $pdf = PDF::loadView('admin.pdf.invoice',$pdf_arr);
        // $pdfPath = PDF_TMP_DIR.'/'.$outputName.'.pdf';
        // File::put($pdfPath, $pdf->output());

        // $mail_data['pdf_path'] = $pdfPath;
        // $mail_data['user_email'] = $user->email;
        // $mail_data['sub_total'] = $request->get('total_amount');
        // if ($request->get('discount_type')) {
        //     $mail_data['discount'] = $request->get('discount');
        //     $mail_data['discount_type'] = $request->get('discount_type');
        //     $mail_data['discount_value'] = $request->get('discount_value');
        // }
        // if ($request->get('reason_value')) {
        //     $mail_data['discount_reason_value'] = $request->get('reason_value');
        // }
        // if ($request->get('shipping')) {
        //     $mail_data['shipping'] = $request->get('shipping');
        //     $mail_data['custom_ship_name'] = $request->get('custom_ship_name');
        //     $mail_data['custom_ship_value'] = $request->get('custom_ship_value');
        // }
        // if ($request->get('tax')) {
        //     $mail_data['tax'] = $request->get('tax');
        // }
        // if ($request->get('notes_value')) {
        //     $mail_data['notes_value'] = $request->get('notes_value');
        // }
        // $mail_data['payable_amount'] = $request->get('final_total_amount');

        // try {   
        //     Mail::send('mail.order.invoice', $mail_data, function($message) use($mail_data){
        //         $message->to('abhishekgautam76@gmail.com', "Babyjalebi Support")->subject("Order Invoice");
        //         $message->attach($mail_data['pdf_path'], array('as' => 'Invoice.pdf'));
        //     });
        
        // } catch (Exception $e) {
            
        // }

        // $mail_data['pdf_path'] = $pdfPath;

        // $data['status'] = 1;

        // return $data;
    }

    public function postOrderComment(Request $request)
    {
        $ordercode = $request->get('order_code');
        $comment = $request->get('comment');
        $name = "Admin";

        $ordercomment = new OrderComments;
        $ordercomment->order_code = $ordercode;
        $ordercomment->comment = $comment;
        $ordercomment->name = $name;
        $ordercomment->save();

        $order_comments = OrderComments::where('order_code', '=', $ordercode)->orderBy('created_at', 'desc')->get();

        return view('admin.order.ordercomment', compact('order_comments'));

    }

    // public function calcTotalQty(Request $request)
    // {
    //     $product_id = $request->get('productid');
    //     $qty = $request->get('qty');
    //     $tax = $request->get('tax');
    //     $checktype = $request->get('checktype');

    //     $product = Product::find($product_id);
    //     if ($checktype == "increase") {
    //         $old_qty = $qty - 1;
    //     }
    //     if ($checktype == "decrease") {
    //         $old_qty = $qty + 1;
    //     }

    //     if (isset($pdiscount)) {
    //         $total_discount = [];
    //         $total_tax = [];
    //         foreach($products as $detail){
    //           $product_discount = $pdiscount/100*$detail->price;
    //           $product_after_discount = $detail->price-$product_discount;
    //           $tax = $detail->tax/100*$product_after_discount;
    //           array_push($total_tax, $tax);
    //           array_push($total_discount, $product_discount);
    //         }
    //         $final_tax = array_sum($total_tax);
    //         $ttax = number_format((float)$final_tax, 2, '.', '');
    //         $final_discount = array_sum($total_discount);
    //         $final_discount = number_format((float)$final_discount, 2, '.', '');
    //       } elseif (isset($fdiscount)) {

    //         $discount = $fdiscount/count($products);

    //         $total_tax = [];
    //         foreach($products as $detail){
    //           $product_after_discount = $detail->price-$discount;
    //           $tax = $detail->tax/100*$product_after_discount;
    //           array_push($total_tax, $tax);
              
    //         }
    //         $final_tax = array_sum($total_tax);
    //         $ttax = number_format((float)$final_tax, 2, '.', '');
    //         $final_discount = number_format((float)$fdiscount, 2, '.', '');

    //       }elseif(isset($coupon_discount)){

    //         if ($coupon_discount->coupon_type == 1) {
    //           $discount = $coupon_discount->discount_value/count($products);

    //           $total_tax = [];
    //           foreach($products as $detail){
    //             $product_after_discount = $detail->price-$discount;
    //             $tax = $detail->tax/100*$product_after_discount;
    //             array_push($total_tax, $tax);
    //           }
    //           $final_tax = array_sum($total_tax);
    //           $ttax = number_format((float)$final_tax, 2, '.', '');
    //           $final_discount = number_format((float)$coupon_discount->discount_value, 2, '.', '');
    //         } else {
    //           $total_discount = [];
    //           $total_tax = [];
    //           foreach($products as $detail){
    //             $product_discount = $coupon_discount->discount_value/100*$detail->price;
    //             $product_after_discount = $detail->price-$product_discount;
    //             $tax = $detail->tax/100*$product_after_discount;
    //             array_push($total_tax, $tax);
    //             array_push($total_discount, $product_discount);
                
    //           }
    //           $final_tax = array_sum($total_tax);
    //           $ttax = number_format((float)$final_tax, 2, '.', '');
    //           $final_discount = array_sum($total_discount);
    //           $final_discount = number_format((float)$final_discount, 2, '.', '');
    //         }

    //       }else{
    //         $final_discount = 0.00;
    //       }
    //       dd($ttax);
    //       dd($final_discount);
    //     // $old_total_amount = $product->price*$old_qty;
    //     // $old_total_tax = $product->tax/100*$old_total_amount;
    //     // $oldttax = $tax - $old_total_tax;

    //     // $total_amount = $product->price*$qty;
    //     // $total_tax = $product->tax/100*$total_amount;
    //     // $final_total_amount = $total_amount+$total_tax;

    //     $data['product_total'] = $total_amount;
    //     $data['product_tax'] = $total_tax;
    //     $data['tax'] = $oldttax + $total_tax;

    //     return $data;
    // }
    // 

    public function exportByday(Request $request) {

        $start_date = $request->get('startdate');
        
        $end_date = $request->get('enddate');

        // echo $end_date;
        // dd("test");
        $query = DB::table('order_request')
                        ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                        ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                        ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                        ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                        ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                        ->where( DB::raw('date(order_request.created_at)'), '>=', $start_date)
                        ->where( DB::raw('date(order_request.created_at)'), '<=', $end_date)
                        ->select('order_request.*','order_status.status_title','payment_status.payment_status_name', 'users.id as userid','users.first_name', 'users.last_name', 'users.email', 'users.contact','users.notes', 'address.address', 'address.phone', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.phone as bphone', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default');
                             if (isStore()) {
                                    $admin = isStore();
                                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
                                if($vendor->id) {
                                         $query->where('order_request.vendor_id', $vendor->id);
                                    }
                                   // $all_compeletd_records_qeury->where('vendor_id', $vendor->id);
                                // dd($vendor);
                               }

        
                         
                        $table = $query->get();
                        // dd($table);
    $filename = "orderlist.csv";
   
    
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Order Code', 'Email', 'Financial Status', 'Paid at', 'Subtotal', 'Shipping', 'Taxes', 'Total', 'Discount Code', 'Discount Amount', 'Created at', 'Status', 'Billing Name','Billing Last Name', 'Billing Street','Billing City','Billing Zip','Billing Phone','Shipping Name','Notes','Payment Method'));

    foreach($table as $row) {
        
$address = Address::where('user_id', $row->user_id)->first();
$status = OrderStatus::where('id',$row->order_status)->first();
                // dd($status->status_title);
// echo $status['status_title']; 
// $string = implode(';', $status);
// $status = OrderStatus::where('id', $row->status)->first();
         if ($row->order_from == 0)
    {
         $orderfrom = "Offline";
    }
    else
    {
        $orderfrom = "Online";
    }
    if($row->vendor_id == 1)
    {
        $orderid= "D".$row->id;
    }
      
    else
    {

    $orderid= "G".$row->id;

    }
    if($row->contact == 'NULL')
    {
        $contact = $row->bphone;
    }
    else
    {
        $contact = $row->contact;
    }
        
        fputcsv($handle, array($orderid,$row->email,$row->payment_status_name,$row->created_at,$row->amount,$row->custom_rate_value,$row->tax_rate_value,$row->payble_amount,$row->coupon_code,$row->discount,$row->created_at,$status['status_title'],$row->first_name,$row->last_name,$row->address,$row->city,$row->zip,$contact,$row->first_name,$row->notes));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return $data =  Response::download($filename, 'Order_list.csv', $headers);
    // return Response::make(rtrim($filename, "\n"), 200, $headers);


        // return $data=$end_date ;
        // dd($end_date);


    }
    public function totalSales(Request $request) {

        $start_date = $request->get('startDate');
        $end_date = $request->get('endDate');
        
        // getting total sales basis of date and store id
        $total_sales_query = DB::table('order_request')
                  ->selectRaw('sum(payble_amount) as totalSales')
                    ->where( DB::raw('date(created_at)'), '>=', $start_date)
                    ->where( DB::raw('date(created_at)'), '<=', $end_date);

           if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $total_sales_query->where('vendor_id', $vendor->id);

           }
       
            $orders = $total_sales_query->where('order_status','=', 4)->first();
        // dd($orders);
        // getting total_online_sales_quary basis of date and store id

         $total_online_sales_quary = DB::table('order_request')
                       ->select(DB::raw('SUM(payble_amount) as totalSales, count(id) as totalOrders'))
                       ->where( DB::raw('date(created_at)'), '>=', $start_date)
                       ->where( DB::raw('date(created_at)'), '<=', $end_date)
                       ->where('order_from', '=' ,1);

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_online_sales_quary->where('order_request.vendor_id', $vendor->id);

               }
           
           $online_orders = $total_online_sales_quary->where('order_status','=', 4)->first();

        // $online_orders = DB::table('order_request')
        //             ->where( DB::raw('date(created_at)'), '>=', $start_date)
        //             ->where( DB::raw('date(created_at)'), '<=', $end_date)
        //             ->where('order_from', '=', 1)
        //             ->selectRaw('sum(payble_amount) as totalSales, count(id) as totalOrders')
        //             ->first();


        $total_offline_sales_quary = DB::table('order_request')
                       ->select(DB::raw('SUM(payble_amount) as totalSales, count(id) as totalOrders'))
                       ->where( DB::raw('date(created_at)'), '>=', $start_date)
                       ->where( DB::raw('date(created_at)'), '<=', $end_date)
                       ->where('order_from', '=' ,0);

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_offline_sales_quary->where('order_request.vendor_id', $vendor->id);

               }
           
           $offline_orders = $total_offline_sales_quary->where('order_status','=', 4)->first();

        // $offline_orders = DB::table('order_request')
        //             ->where( DB::raw('date(created_at)'), '>=', $start_date)
        //             ->where( DB::raw('date(created_at)'), '<=', $end_date)
        //             ->where('order_from', '=', 0)
        //             ->selectRaw('sum(payble_amount) as totalSales, count(id) as totalOrders')
        //             ->whereIn('order_status',[1,3,4])
        //             ->first(); 

        $data['totalSales'] = "Rs. ".$orders->totalSales;
        $data['totalOnlineSales'] = "Rs. ".$online_orders->totalSales;
        $data['totalOnlineOrders'] = $online_orders->totalOrders;
        $data['totalOfflineSales'] = "Rs. ".$offline_orders->totalSales;
        $data['totalOfflineOrders'] = $offline_orders->totalOrders;


        return $data;
    }
    // public function allSearch(Request $request)
    // {
    //     $querygetall = $request->get('data_from_table');
    //     $orders = DB::table('users')
    //                         ->join('tags', 'users.id', '=', 'tags.field_id', 'left')
    //                         ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
    //                         ->select('order_request.*','users.id', 'users.first_name', 'users.last_name', 'users.address', 'users.email', 'users.contact','users.address2')
    //                         ->orderBy('users.created_at', 'desc')
    //                         ->where('tags.name', 'LIKE', "%$querygetall%")
    //                         ->orWhere('users.first_name', 'LIKE', "%$querygetall%")
    //                         ->orWhere('users.last_name', 'LIKE', "%$querygetall%")
    //                         ->orWhere('users.address', 'LIKE', "%$querygetall%")
    //                         ->orWhere('users.email', 'LIKE', "%$querygetall%")
    //                         ->orWhere('users.contact', 'LIKE', "%$querygetall%")
    //                         ->orWhere('users.address2', 'LIKE', "%$querygetall%")
    //                         ->distinct()
    //                         ->get();
    //                         return view('admin.order.order_list', compact('orders'));
    // }

    public function addNotes(Request $request){

        $notes = $request->get('notes');
        $order_id = $request->get('order_id');

        if ($notes) {
            $order = Order::find($order_id);
            $order->notes = $notes;
            $order->save();

            $data['status'] = 1;
            $data['message'] = "Notes Saved.";
            
            return $data;;
        }else{
            
            $data['status'] = 0;
            $data['message'] = "Order not found.";

            return $data;
        }
    }
}