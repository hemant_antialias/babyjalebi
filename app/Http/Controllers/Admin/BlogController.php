<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Input;
use DB;
use App\Admin;
use App\Vendor;
use App\Blog;
use App\BlogCategory;


class BlogController extends Controller
{
    public function newBlog() {
        
        $stores = DB::table('vendors_details')->pluck('store_name','id')->toArray();

        $category = DB::table('blog_category')->get();

    	return view('admin.blog.new_blog', compact('category', 'stores'));
    }
     public function blogList($slug) {
        
        $blogs = DB::table('blog')
                    ->join('blog_category', 'blog.blog_category_id', '=', 'blog_category.id', 'left')
                    ->select('blog.*')
                    ->where('blog_category.slug', '=', $slug)
                    ->orderBy('blog.created_at', 'desc')
                    ->get();

        return view('web.blog.blog_list', compact('blogs'));
    }

    public function blog(){

        $blogs = DB::table('blog')
                    ->join('blog_category', 'blog.blog_category_id', '=', 'blog.id', 'left')
                    ->select('blog.*', 'blog_category.category')
                    ->orderBy('blog.created_at', 'desc')
                    ->get();

        return view('admin.blog.bloglist', compact('blogs'));
    }

    public function editBlog($id) {
        
        $stores = DB::table('vendors_details')->pluck('store_name','id')->toArray();

        $blog = Blog::find($id);
        if ($blog) {
            $category = DB::table('blog_category')->get();
            $blog = DB::table('blog')->where('id', $id)->first();

            $blog_categories = BlogCategory::pluck('category', 'id')->toArray();
            return view('admin.blog.edit_blog', compact('category', 'blog','blog_categories', 'stores'));
        }else{
            abort('404');
        }
    }

    public function addNewBlog(Request $request, $id=null){
        
        if (isStore()) {
            $store = isStore();
            $store_id = $store->id;            
        }else{
            $store_id = $request->get('store_type');
        }

        if ($id) {
            $blog = Blog::find($id);
            $success_msg = "Blog updated Successfully.";
        }else{
    	    $blog = new Blog;
            $success_msg = "Blog created Successfully.";
        }

        $blog->store_id = $store_id;
    	$blog->author_name = $request->get('author_name');
    	$blog->title = $request->get('title');
        $blog->blog_category_id = $request->get('category');
    	$blog->excerpt = $request->get('excerpt');
    	$blog->description = $request->get('description');
    	$blog->slug = str_replace(" ","-",strtolower($blog->title));
        $blog->seo_title = $request->get('seo_title');
    	$blog->meta_keyword = $request->get('meta_keyword');
    	$blog->meta_description = $request->get('meta_description');
    	$blog->status = 1;
    	$blog->featured = 0;
        $blog->save();
         if ($request->file('blog_image')) {
            $image_name = time()."_".$request->file('blog_image')->getClientOriginalName();
            $blog->image = $image_name;
            $request->file('blog_image')->move('blog/'.$blog->id.'/', $image_name);
        }
		$blog->save();
		return redirect()->back()->with('success_msg', $success_msg);
    }

    public function blogDetail($slug){

        $blog = Blog::where('slug', $slug)->first();

        return view('web.blog.blog_detail', compact('blog'));
    }

    public function blogListing(){

        if (isStore()) {
            $store = isStore();
            $store_detail = Vendor::where('admin_id', '=', $store->id)->select('id')->first();

            $blogs = DB::table('blog')
                        ->leftJoin('blog_category', 'blog.blog_category_id', '=', 'blog_category.id')
                        ->select('blog.*', 'blog_category.category')
                        ->whereIn('store_id', [0,$store_detail->id])
                        ->orderBy('blog.created_at', 'desc')
                        ->get();
        }else{
            $blogs = DB::table('blog')
                        ->leftJoin('blog_category', 'blog.blog_category_id', '=', 'blog_category.id')
                        ->select('blog.*', 'blog_category.category')
                        ->orderBy('blog.created_at', 'desc')
                        ->get();   
        }



        return view('admin.blog.bloglist', compact('blogs'));
    }

}