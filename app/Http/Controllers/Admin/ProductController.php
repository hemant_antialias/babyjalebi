<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AdminProductRequest;
use App\Http\Requests\ProductCategoryRequest;
use App\User;
use App\Product;
use App\Vendor;
use App\Tags;
use App\Admin;
use DB;
use Input;
use App\ProductCategory;
use File;
use Storage;
use App\ProductImages;
use App\CategorySliders;
use App\ProductTag;
use App\MeatTypes;
use App\Brands;
use App\CollectionType;
use App\CollectionTypePivot;
use App\ProductReviews;
use App\OrderDetail;
use App\Inventory;
use App\RelatedProduct;
use App\ProductAttribute;
use App\ProductVariationImages;
use App\ProductVariations;
use App\ProductCategories;
use App\CategoryParent;
use App\TempAttrImages;


class ProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function newProduct(){

        TempAttrImages::truncate();
        File::deleteDirectory(public_path('temp_attr_images'));

        $product_categories = ProductCategory::select('name', 'id')->get(); 

       $vendors = Vendor::all('store_name', 'id');
       $product_tags = Tags::where('tag_type', '=', 1)->get();
       $meat_types = ProductCategory::where('collection_type_id', '=', 2)->get();
       $brands = ProductCategory::where('collection_type_id', '=', 3)->get();
       $products = Product::pluck('product_title', 'id')->toArray();
       $productattribute = ProductAttribute::get();

       
       return view('admin.product.new_product', compact('product_categories', 'vendors', 'product_tags', 'meat_types', 'brands','products','productattribute'));
    }

    public function editProduct($id) {
        

        // $product = Product::find($id);
        $product = DB::table('product')
                        ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                        ->where('product.id', '=', $id)
                        ->select('product.*', 'inventory.quantity as pquantity')
                        ->first();

        if ($product) {
            // $product_categories = ProductCategory::pluck('name', 'id')->toArray();
            
                $product_categories = ProductCategory::pluck('name', 'id')->toArray();
            
            $vendors = Vendor::pluck('store_name', 'id')->toArray();
            $product_tags = Tags::where('tag_type', '=', 1)->pluck('name', 'id')->toArray();
            $ptags = ProductTag::pluck('tag_id')->where('product_id', $id)->toArray();
            $meat_types = ProductCategory::where('collection_type_id', '=', 2)->pluck('name', 'id')->toArray();
            $brands = ProductCategory::where('collection_type_id', '=', 3)->pluck('name', 'id')->toArray();
            $products = Product::pluck('product_title', 'id')->toArray();
            $productvariation_count = DB::table('product_variations')
                        ->join('product_attribute', 'product_variations.attribute_id', '=', 'product_attribute.id', 'left')
                        ->join('attribute_value', 'product_variations.attribute_value_id', '=', 'attribute_value.id', 'left')
                        ->join('product_variation_images', 'product_variation_images.variation_id', '=', 'product_variations.id', 'left')
                        ->where('product_variations.product_id', '=', $id)
                        ->select('attribute_value.value','product_variations.id as varid','product_variations.product_id as pid','attribute_value.id','product_variation_images.images',DB::raw('COUNT(product_variations.attribute_value_id) as total_count'))
                        ->groupBy('product_variations.attribute_value_id')
                        ->get();

                        // dd($productvariation_count);
        // for getting variable images
        $productvariation = DB::table('product_variations')
                        ->join('product_attribute', 'product_variations.attribute_id', '=', 'product_attribute.id', 'left')
                        ->join('attribute_value', 'product_variations.attribute_value_id', '=', 'attribute_value.id', 'left')
                        ->join('product_variation_images', 'product_variation_images.variation_id', '=', 'product_variations.id', 'left')
                        ->where('product_variations.product_id', '=', $id)
                        ->select('attribute_value.value','product_variations.id as varid','product_variations.product_id as pid','attribute_value.id','product_variation_images.images',DB::raw('COUNT(product_variations.attribute_value_id) as total_count'))
                        ->groupBy('product_variation_images.images')
                        ->get();

                        // dd($productvariation);

                        // echo $count2;

            $attributs = DB::table('product_variations')
                            ->join('attribute_value', 'product_variations.attribute_id', '=', 'attribute_value.attribute_id', 'left')
                            ->where('product_variations.product_id', '=', $id)
                            ->pluck('attribute_value.value', 'attribute_value.id')
                            ->toArray();
                // dd($attributs);


            $productattribute = ProductAttribute::get();
            $relatedp = RelatedProduct::where('product_id','=',$product->id)->get();

            // dd($ptags);
            return view('admin.product.edit_product', compact('product', 'product_categories', 'vendors', 'product_tags', 'meat_types', 'brands','productattribute','products','productvariation','attributs','productvariation_count','relatedp'));
        }else{
            return redirect('admin/product');
        }
    }

    public function addNewProduct(AdminProductRequest $request, $id=null){
        // dd($request->all());

        // dd($request->get('product_category'));
        if ($request->get('online_status') == null) {
            $product_status = 0;
        }else{
            $product_status = 1;
        }

        if ($request->get('product_tag')) {
            $tags = $request->get('product_tag');
            $tag_ids = [];
            if ($tags) {
                foreach ($tags as $tag) {
                    $product_tag = Tags::where('id', '=', $tag)->select('id')->first();
                    if ($product_tag) {
                        array_push($tag_ids, $product_tag->id);
                    } else {
                        $ptag = new Tags;
                        $ptag->name = $tag;
                        $ptag->tag_type = PRODUCT_TAGS;
                        $ptag->slug = str_slug($tag);
                        $ptag->save();
                        array_push($tag_ids, $ptag->id);
                    }
                }
            }
        }
        if ($id) {
            $product = Product::find($id);
            $success_msg = "Product Updated Successfully.";
        }else{
            $product = new Product;
            $success_msg = "New Product Added Successfully.";
        }
        if ($request->get('product_category')) {
            $category = DB::table('product_category')->where('id', $request->get('product_category'))->first();
            $category_slug = $category->slug;
        }else{
            $category_slug = '';
        }
        $product->product_title = $request->get('product_title');
        $product->product_description = $request->get('product_description');
        $product->price = $request->get('price');
        if ($request->get('tax')) {
            $product->tax = $request->get('tax');
        }else{
            $product->tax = 0.00;
        }
        $product->quantity = $request->get('quantity');
        $product->compare_price = $request->get('compare_price');
        $product->video_link = $request->get('video_link');
        $product->weight = $request->get('weight');
        $product->unit = $request->get('unit');
        $product->status = $product_status;
        // $product->product_category = $request->get('product_category');
        $product->meat_type = $request->get('meat_type');
        $product->brand_type = $request->get('brand_type');
        $product->overview = $request->get('overview');
        $product->details_that_matter = $request->get('details_that_matter');
        $product->safety_information = $request->get('safety_information');
        $product->dimensions = $request->get('dimensions');
        $product->care = $request->get('care');
        $product->our_commitment = $request->get('our_commitment');
        $product->in_house_design = $request->get('in_house_design');

        if (isStore()) {
            $store = isStore();
            $store_detail = Vendor::where('admin_id', '=', $store->id)->select('id')->first();        
            $vendor_id = $store_detail->id;

        }else{
            $vendor_id = Input::get('vendor_id');
        }
        if ($request->get('personalised') == null) {
             $personalised = 0;
        }else{
            $personalised = 1;
        }
        if ($request->get('dohar') == null) {
             $dohar = 0;
        }else{
            $dohar = 1;
        }
        if ($request->get('quilt') == null) {
             $quilt = 0;
        }else{
            $quilt = 1;
        }
        $product->slug = str_slug($request->get('product_title'));
        $product->i_sku = $request->get('i_sku');
        $product->personalised = $personalised;
        $product->dohar = $dohar;
        $product->quilt = $quilt;
        $product->i_barcode = $request->get('i_barcode');
        $product->product_page_title = $request->get('product_page_title');
        $product->meta_description = $request->get('meta_description');
        $product->url = str_slug($request->get('url'));
        $product->save();

        if ($request->get('product_category')) {
            foreach ($request->get('product_category') as $value) {
                $product_categories = new ProductCategories;
                $product_categories->product_id = $product->id;
                $product_categories->category_id = $value;
                $product_categories->save();
            }
        }


        if (isset($tag_ids)) {
            foreach ($tag_ids as $tkey => $tvalue) {
                $product_tag = new ProductTag;
                $product_tag->product_id = $product->id;
                $product_tag->tag_id = $tvalue;
                $product_tag->save();
            }
        }

        $images = $request->file('product_images');
        if ($images) {
            foreach ($images as $value) {

                $image_name = time()."_".$value->getClientOriginalName();
                $product_image = new ProductImages;
                $product_image->product_id = $product->id;
                $product_image->images = $image_name;
                $product_image->save();

                $value->move('product_images/'.$product->id.'/', $image_name);
            }
        }

        $featured_images = $request->file('featured_images');
        if ($featured_images) {

            $featured_image = time()."_".$featured_images->getClientOriginalName();
            $product_featured_image = Product::find($product->id);
            // $product_featured_image->product_id = $product->id;
            $product_featured_image->product_images = $featured_image;
            $product_featured_image->save();

            $featured_images->move('product_images/'.$product->id.'/featured_images'.'/', $featured_image);
        }
        $product_ids = $request->get('product');
        if ($product_ids) {
            foreach ($product_ids as $pid) {

                $p_ids = new RelatedProduct;
                $p_ids->product_id = $product->id;
                $p_ids->related_pid = $pid;
                $p_ids->save();

            }
        }

        $attributevalues = TempAttrImages::all();
        if ($attributevalues) {
            foreach ($attributevalues as $attributevalue) {
                $product_attribute = new ProductVariations;
                $product_attribute->product_id = $product->id;
                $product_attribute->attribute_id = 'NULL';
                $product_attribute->attribute_value_id = $attributevalue->attr_id;
                $product_attribute->save();

                $variationimg = new ProductVariationImages;
                $variationimg->variation_id = $product_attribute->id;
                $variationimg->images = $attributevalue->image;
                $variationimg->save();


                $dest = File::makeDirectory(public_path().'/product_images/'.$product->id.'/'.$attributevalue->attr_id.'/', 0777, true, true);
                $file = public_path('temp_attr_images/'.$attributevalue->image);
                $new_file = public_path('product_images/'.$product->id.'/'.$attributevalue->attr_id.'/'.$attributevalue->image);

                if (copy($file, $new_file))
                {
                    echo "saved";
                }else{
                    echo "not saved";
                }
            }
        }


        return redirect()->back()->with('success_msg', $success_msg);
    }
// delete product variation by id 

    public function deleteProductvariation(Request $request){

        $varid = $request->get('varid');
        $product_image = ProductVariations::where('id', $varid)->delete();
        $product_image1 = ProductVariationImages::where('variation_id', $varid)->delete();
        // $product_image->delete();

        $data['message'] = "Product Image Deleted Successfully.";
        $data['status'] = 1;

        return $data;
    }
    // delete pattern 
    // public function deletePattern(Request $request ){
    //     dd($request->all());
    //     $pid = $request->get('pid');
    //     $attribute = $request->get('varid');

    //     $attribute_values = DB::table('product_variation')
    //                         ->where('product_id', $pid)
    //                         ->where('size', $attribute)
    //                         ->delete();
    // }

// end

    // delete ptrrn 
    // delete pattern 
    public function deletePattern(Request $request ){
        // dd($request->all());
        $pid = $request->get('pid');
        $attribute = $request->get('varid');

        $attribute_values = DB::table('product_variation')
                            ->where('product_id', $pid)
                            ->where('attribute_id', $attribute)
                            ->delete();
    }

// delete related product
     public function deleteRelatedProduct(Request $request){

        $varid = $request->get('varid');
        // dd($varid);
        // $product_image->delete();
        $product_image = RelatedProduct::where('related_pid', $varid)->delete();

        $data['message'] = "Related product remove Successfully.";
        $data['status'] = 1;

        return $data;
    }

    public function deleteProduct(Request $request){
        
        $imageid = $request->get('key');
        $product_image = ProductImages::find($imageid);
        $product_image->delete();

        $data['message'] = "Product Image Deleted Successfully.";
        $data['status'] = 1;

        return $data;
    }
    public function deleteCategoryslider(Request $request){
        
        $imageid = $request->get('key');
        $category_slider = CategorySliders::find($imageid);
        $category_slider->delete();

        $data['message'] = "Category slider Image Deleted Successfully.";
        $data['status'] = 1;

        return $data;
    }


    /**
     * Display New Category Page
     * @return [type] [new product category blade]
     */
    public function newProductCategory() {

            $products = Product::pluck('product_title', 'id')->toArray();
            $stores = Vendor::pluck('store_name', 'id')->toArray();
            // $stores = array_merge(['0' => 'All Store'], $stores);
        
        $collection_types = ProductCategory::where('is_parent',1)->get();

        return view('admin.product.new_product_category', compact('products', 'collection_types'));
    }

    /**
     * Display Edit Category Page
     * @param  [type] $id [catgory id]
     * @return [type]     [edit product category blade]
     */
    public function editProductCategory($id) {
        
        $category = ProductCategory::find($id);
        $category_products = $category->product_ids;

        if ($category) {
            $pid = explode(',',$category_products);
            if (isStore()) {
                $store = isStore();
                $store_id = $store->id;
                $products = Product::where('vendor_id', '=', $store_id)->pluck('product_title', 'id')->toArray();
            }else{
                $products = Product::pluck('product_title', 'id')->toArray();
                 $stores = Vendor::pluck('store_name', 'id')->toArray();
            $stores = array_merge(['0' => 'All Store'], $stores);

            }

            $productByCategory = DB::table('product')
                                    ->leftjoin('product_images', 'product.id', '=', 'product_images.product_id')
                                    ->select('product.*', 'product_images.images')
                                    ->whereIn('product.id', $pid)->get();   

            $collection_types = ProductCategory::all();
        
            return view('admin.product.edit_product_category', compact('category','stores', 'products','productByCategory', 'collection_types'));
        }else{
            return redirect('/dashboard');
        }
    }

    public function addNewProductCategory(ProductCategoryRequest $request, $id=null) {


        
        if ($request->get('online_status') == null) {
            $category_status = 0;
        }else{
            $category_status = 1;
        }
        if ($request->get('special') == null) {
            $special = 0;
        }else{
            $special = 1;
        }
        if ($request->get('homepage') == null) {
            $inhomepage = 0;
        }else{
            $inhomepage = 1;
        }
        if ($request->get('is_parent') == null) {
            $isparent = 0;
        }else{
            $isparent = 1;
        }

        if ($id) {
            $category = ProductCategory::find($id);
            $success_msg = 'Collection Updated Successfully.';
        }else{
        // dd("categxory");
            $category = new ProductCategory;
            $success_msg = ' Collection Added Successfully.';
        }

        $category->is_parent = $isparent;
        $category->is_special = $special;
        $category->in_homepage = $inhomepage;
        $category->name = $request->get('name');
        // $category->parent_id = $request->get('collection_type');
        $category->slug = strtolower(str_replace(' ', '-', $request->get('name')));
        $category->description = $request->get('description');
        if ($request->file('category_image')) {
            $image_name = time()."_".$request->file('category_image')->getClientOriginalName();
            $category->image = $image_name;
            $request->file('category_image')->move('categories/featured_images', $image_name);
        }
        $category->status = $category_status;
        $category->save();

        if ($request->get('collection_type')) {
            foreach ($request->get('collection_type') as $value) {
                $parent_categories = new CategoryParent;
                $parent_categories->category_id = $category->id;
                $parent_categories->parent_category_id = $value;
                $parent_categories->save();
            }
        }

        $images = $request->file('category_slider');
        // dd($images);
        if ($images) {
            foreach ($images as $value) {

        // dd($value);
                $image_name = time()."_".$value->getClientOriginalName();
                $category_slider = new CategorySliders;
                $category_slider->category_id = $category->id;
                $category_slider->image = $image_name;
                $category_slider->save();
                

                $value->move('categories/category_sliders/'.$category->id.'/', $image_name);
            }
        }



        return redirect()->back()->with('success_msg', $success_msg);
    }

    public function categoryList(){
        if (isStore()) {
            $store = isStore();

            $store_id = $store->id;
            $vendor = Vendor::where('admin_id', $store->id)->select('id')->first();
            $product_categories = ProductCategory::where('store_id', '=', $vendor->id)->get();
            
        }else{
            $product_categories = DB::table('product_category')
                                        ->join('vendors_details', 'product_category.store_id', '=', 'vendors_details.id', 'left')
                                        ->join('collection_types', 'product_category.collection_type_id', '=', 'collection_types.id', 'left')
                                        ->select('product_category.*', 'vendors_details.store_name', 'collection_types.name as collection_type_name')
                                        ->orderBy('product_category.created_at', 'desc')
                                        ->get();
        }

        return view('admin.product.categorylist', compact('product_categories'));
    }

    public function product(Request $request)
    {
        $vendors = Vendor::all();

        if (isStore()) {
            $store_id = isStore()->id;
            $store = Vendor::where('admin_id', '=', $store_id)->first();
            $products = DB::table('product')
                            ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                            ->select('product.*', 'product_category.name as category_name')
                            ->whereIn('product.vendor_id', [$store->id, 0])
                            ->orderBy('product.product_title', 'asc')
                            ->get();

        }else{
            
            if ($request->get('store')) {
                $store = $request->get('store');
                if ($store == "all") {
                    return redirect('admin/product');
                }
                $stores = Vendor::where('slug', $store)->first();

                $products = DB::table('product')
                                ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                                ->join('vendors_details', 'product.vendor_id', '=', 'vendors_details.id', 'left')
                                ->select('product.*', 'product_category.name as category_name', 'vendors_details.store_name as storename')
                                ->whereIn('product.vendor_id', [$stores->id, 0])
                                ->orderBy('product.product_title', 'asc')
                                ->get();

            }else{
                $products = DB::table('product')
                                ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                                ->join('vendors_details', 'product.vendor_id', '=', 'vendors_details.id', 'left')
                                ->select('product.*', 'product_category.name as category_name', 'vendors_details.store_name as storename')
                                ->orderBy('product.product_title', 'asc')
                                ->get();
            }
            // dd($request->all());

        }

        if ($request->ajax()) {
            return view('admin.product.productlist', compact('products', 'vendors', 'stores', 'inventory'));
        }
        return view('admin.product.productlist', compact('products', 'vendors', 'stores', 'inventory'));
    }


    public function inventory(Request $request)
    {
        $vendors = Vendor::all();

        if (isStore()) {
            $store_id = isStore()->id;
            $store = Vendor::where('admin_id', '=', $store_id)->first();
            $products = DB::table('product')
                            ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                            ->select('product.*', 'product_category.name as category_name')
                            ->whereIn('product.vendor_id', [$store->id, 0])
                            ->orderBy('product.product_title', 'asc')
                            ->get();

        }else{
            
            if ($request->get('store')) {
                $store = $request->get('store');
                if ($store == "all") {
                    return redirect('admin/products/inventory');
                }
                $stores = Vendor::where('slug', $store)->first();

                $products = DB::table('product')
                                ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                                ->join('vendors_details', 'product.vendor_id', '=', 'vendors_details.id', 'left')
                                ->select('product.*', 'product_category.name as category_name', 'vendors_details.store_name as storename')
                                ->whereIn('product.vendor_id', [$stores->id, 0])
                                ->orderBy('product.product_title', 'asc')
                                ->get();

            }else{
                $products = DB::table('product')
                                ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                                ->join('vendors_details', 'product.vendor_id', '=', 'vendors_details.id', 'left')
                                ->select('product.*', 'product_category.name as category_name', 'vendors_details.store_name as storename')
                                ->orderBy('product.product_title', 'asc')
                                ->get();
            }
            // dd($request->all());

        }

        if ($request->ajax()) {
            return view('admin.product.inventorylist', compact('products', 'vendors', 'stores', 'inventory'));
        }
        return view('admin.product.inventorylist', compact('products', 'vendors', 'stores', 'inventory'));
    }

    // public function changeStore(Request $request){
    //     $store_id = $request->get('store_id');
    //     $request->session()->put('store_id', $store_id);
    //     $store = $request->session()->get('store_id');

    //     $vendors = Vendor::all();

    //     $products = DB::table('product')
    //                     ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
    //                     ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
    //                     ->join('vendors_details', 'product.vendor_id', '=', 'vendors_details.id', 'left')
    //                     ->whereIn('product.vendor_id', [$store, 0])
    //                     ->select('product.*', 'product_category.name as category_name', 'vendors_details.store_name as storename',  'inventory.quantity as iquantity')
    //                     ->orderBy('product.product_title', 'asc')
    //                     ->get();

    // }

    // Review list
     public function reviewlist(Request $request)
    {
       
            // $reviews = DB::table('product_reviews')->get();
        $reviews = DB::table('product_reviews')
                            ->join('product', 'product_reviews.product_id', '=', 'product.id', 'left')
                            ->select('product_reviews.*', 'product.product_title')
                            ->orderBy('product.created_at', 'desc')
                            ->get();
                            // dd($reviews);
        
        return view('admin.product.reviewlist', compact('reviews'));
    }

    // public function filterInventory(Request $request)
    // {
    //     if ($request->ajax()) {
    //         $products = Product::all();
    //         return view('admin.product.inventory_filter', compact('products'));
    //     }else{
    //         return "No data found";
    //     }
    // }

    public function updateProductQuantity(Request $request){

        $product_id = $request->get('id');
        $quantity = $request->get('quantity');
       


        $product = Product::where('id', '=', $product_id)->first();
        
        if ($product) {
            $product->id = $product_id;
            $product->quantity = $product->quantity+$quantity;
            $product->save();
        }else{
            $product = new Product;
            $product->id = $product_id;
            $product->quantity = $quantity;
            $product->save();
        }

        $data['status'] = 1;
        $data['message'] = 'Quantity Updated.';

        return $data;
    }

    public function allProducts(Request $request){

        
            // $store_detail = Vendor::where('admin_id', '=', $store_id)->select('id')->first();
        $products = DB::table('product')
                        ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                        ->select('product.id','product.product_title', 'product.price','inventory.quantity')
                        ->orderBy('product.product_title', 'asc')
                        ->get();


            // $products = Product::select('id','product_title', 'price')->orderBy('product_title', 'asc')->get();
        // dd("new edit");

        
        
        return view('admin.product.all_products', compact('products'));
    }

    public function allCategory(){
        if (isStore()) {
            $store_id = isStore()->id;
            $store_detail = Vendor::where('admin_id', '=', $store_id)->select('id')->first();

            $product_categories = ProductCategory::where('store_id', [0,$store_detail->id])->select('id', 'name')->get();
        }else{
            $product_categories = ProductCategory::select('id', 'name')->get();
        }
        
        return view('admin.product.all_product_categories', compact('product_categories'));
    }

    public function allProductsByCategory(Request $request){
        $category_id = $request->get('category_id');
        $products = Product::where('product_category', '=', $category_id)
                            ->select('id','product_title', 'price')
                            ->get();
        return view('admin.product.all_products_by_cid', compact('products'));
    }

    public function allVendor(){

        $vendors = Vendor::select('id', 'store_name')->get();
        
        return view('admin.product.all_vendors', compact('vendors'));
    }

    public function allProductsByVendor(Request $request){

        $vendor_id = $request->get('vendor_id');
        $products = Product::where('vendor_id', '=', $vendor_id)
                            ->select('id','product_title', 'price')
                            ->get();

        return view('admin.product.all_products_by_vid', compact('products'));

    }

    public function allTags(){
        
        $tags = Tags::where('tag_type', '=', PRODUCT_TAGS)->get();

        return view('admin.product.all_tags', compact('tags'));
    }

    public function allProductsByTags(Request $request){

        $tag_id = $request->get('tag_id');
        $products = DB::table('product_tag')
                        ->join('product', 'product_tag.product_id', '=', 'product.id', 'left')
                        ->where('product_tag.tag_id', '=', $tag_id)
                        ->select('product.id','product.product_title', 'product.price')
                        // ->groupBy('product_tag.tag_id')
                        ->get();

        return view('admin.product.all_products_by_tag', compact('products'));
    }

    public function addProductsToCategory(Request $request) {
        // if ($request->get('id') && $request->get('cid')) {
        //     $cid = $request->get('cid');
        //     // dd($request->get('id'));
        //     $product = ProductCategory::find($cid);
        //     $product->product_ids = implode(',', $request->get('id'));
        //     $product->save();
            
        //     $category = ProductCategory::find($cid);
        //     $products = $category->product_ids;

        
        //     $pid = explode(',',$products);
        //     $productByCategory = DB::table('product')
        //                             ->whereIn('id', $pid)
        //                             ->get(); 
        //     return view('admin.product.show_products_by_category', compact('productByCategory'));
        // }
        // if ($request->get('id')) {
            // dd('fdfdf');
            $product_ids = $request->get('id');
            $productByCategory = DB::table('product')
                                        ->whereIn('id', $product_ids)
                                        ->get();   
            return view('admin.product.show_products_by_category', compact('productByCategory'));
        
    }

    public function allCategoryByStores(Request $request){
        

            $categories = ProductCategory::whereIn('store_id', [$store_id,0])
                                            ->select('name', 'id')
                                            ->get();


            return view('admin.product.all_category_by_stores', compact('categories'));
        
    }

    public function allExcludeCategoryByStores(Request $request){
        // $store_id = $request->get('store_id');
        

            $categories = ProductCategory::all();

            return view('admin.product.all_exclude_category_by_stores', compact('categories'));
        
    }

    public function allProductsByStores(Request $request){
        // $store_id = $request->get('store_id');
        
            $products = Product::select('product_title', 'id')->get();

            return view('admin.product.all_products_by_stores', compact('products'));

       
    }
    
    public function allExcludeProductsByStores(Request $request){
        // $store_id = $request->get('store_id');
        
            $products = Product::select('product_title', 'id')->get();

            return view('admin.product.all_exclude_products_by_stores', compact('products'));

       
    }
    public function allProductsByStoresCategory(Request $request){
        
            $products = Product::select('product_title', 'id')->get();

            return view('admin.product.all_products_by_stores_category', compact('products'));

        
    }

    public function changeStatus(Request $request){
        $id = $request->get('id');
        $status = $request->get('status');
        $category = ProductCategory::find($id);
        $category->status = $status;
        $category->save();

        $data['status'] = 1;
        return $data;
    }

    // 
    public function menuStatus(Request $request){
        $id = $request->get('id');
        $status = $request->get('status');
        // dd($status);
        $category = ProductCategory::find($id);
        $category->is_menu = $status;
        $category->save();

        $data['status'] = 1;
        return $data;
    }

    public function changeProductStatus(Request $request){
        $id = $request->get('id');
        $status = $request->get('status');
        $product = Product::find($id);
        $product->status = $status;
        $product->save();

        $data['status'] = 1;
        return $data;
    }

    public function changeReviewStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('review_status');
        $product = ProductReviews::find($id);
        $product->status = $status;
        $product->save();

        $data['status'] = 1;
        return $data;
    }

    public function addNewProductType(Request $request){
        
        $product_type = $request->get('product_type');
        $product_types = new MeatTypes;
        $product_types->meat_type = $product_type;
        $product_types->slug = str_replace(' ', '-', $request->get('product_type'));
        $product_types->save();

        $data['type_id'] = $product_types->id;
        $data['product_type'] = $product_types->meat_type;
        $data['status'] = 1;
        $data['message'] = "Product Type Added Successfully.";

        return $data;
    }

    public function addProductCategory(Request $request, $id=null){
        $id = $request->get('cid');
        $product = ProductCategory::find($id);
        $product->product_ids = $request->get('pid');
        $product->save();

 //       $data['status'] = 1;
  //      $data['message'] = "Product Added Successfully.";
 
        $category = ProductCategory::find($id);
        $products = $category->product_ids;

        
            $pid = explode(',',$products);
            $productByCategory = DB::table('product')
            ->leftjoin('product_images', 'product.id', '=', 'product_images.product_id')
            ->select('product.*', 'product_images.images')
            ->whereIn('product.id', $pid)->get();   

        return view('admin.product.show_product_by_category', compact('productByCategory'));
        //return $data;
    }

    /**
     * [deleteProductFromSession description]
     * @return [type] boolean
     */
    public function deleteProductFromSession(Request $request){

        $product_id = $request->get('productId');
        if($request->session()->has('pre_product_id')){
            foreach ($request->session()->get('pre_product_id') as $key => $value) {
                if($value == $product_id){
                    $request->session()->pull('pre_product_id.'.$key);
                }
            }
        }

        $data['message'] = "Deleted Successfully";
        $data['status'] = 1;

        return $data;
    }

    public function deleteTag(Request $request){
        $tag_id = $request->get('tag_id');
        if ($tag_id) {
    
            DB::table('product_tag')->where('tag_id', '=', $tag_id)->delete();
    
            $data['message'] = "Tag Deleted Successfully";
            $data['status'] = 1;
        }else{
            $data['message'] = "Tag Not Found";
            $data['status'] = 0;
        }        

        return $data;
    }

    public function restock(Request $request){

        $order_id = $request->get('order_id');
        $order_detail = OrderDetail::where('order_id', $order_id)->get();

        foreach ($order_detail as $detail) {
            $product = Product::find($detail->product_id);
            $product->quantity = $detail->quantity;
            $product->save();
        }

        $data['status'] = 1;
        $data['message'] = "Product inventory updated";
        return $data;
    }

    // delete variation images
    public function delvarimg(Request $request)
    {
        $varid = $request->get('varid');
         $products = DB::table('product_variation_images')
                            ->join('product_variations', 'product_variation_images.variation_id', '=', 'product_variations.id', 'left')
                            ->select('product_variations.product_id')
                            ->where('product_variation_images.id', $varid)
                            ->first();
        // dd($products->product_id);
        File::delete('product_images/'.$products->product_id.'/' . $varid);
        $item = ProductVariationImages::find($varid);
        $item->delete();
        // dd($varid);
        if($item)
        {
            $data['status'] = 1;
            $data['message'] = "image remove Successfully";
        }
        else
        {
            $data['status'] = 0;
            $data['message'] = "Opss something wrong";
        }
        return $data;
    }

    public function addVariationImage(Request $request)
    {
        $data = $request->all();
        $attrid = $data['attrid'];
        $images_count = (count($data)-1);

        for ($i=0; $i < $images_count ; $i++) { 
            $attr_images = new TempAttrImages;
            $image = $data['file-'.$i];
            $attr_images->attr_id = $attrid;
            $attr_images->save();

            if ($image) {
                $attr_image = time()."_".$image->getClientOriginalName();
                $attr_images->image = $attr_image;
                $attr_images->save();

                $image->move('temp_attr_images/', $attr_image);
            }
        }

        $data['status'] = 1;
        return $data;
    }
}