<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AdminCustomerRequest;
use Input;
use Validator;
use DB;
use Mail;
use Hash;
use App\Admin;
use App\Order;
use App\Vendor;
use App\User;
use App\Tags;
use App\CustomerTag;
use App\Address;


class CustomerController extends Controller
{
    
    public function newCustomer() {
        $customer_tags = Tags::where('tag_type', '=', 3)->get();
        $stores = DB::table('vendors_details')->pluck('store_name','id')->toArray();

    	return view('admin.customer.new_customer',compact('customer_tags', 'stores'));
    }
     public function customerDetail($customerid) {

        // $user = User::find($customerid);
        
        $user = DB::table('users')
                    ->join('address', 'users.id', '=', 'address.user_id', 'left')
                    ->where('users.id', $customerid)
                    ->select('users.*', 'address.address as caddress', 'address.address2 as caddress2', 'address.city as ccity', 'address.state as cstate', 'address.zip as czip')
                    ->first();

        $customer_tags = Tags::where('tag_type', '=', 3)->pluck('name', 'id')->toArray();

        $order = DB::table('order_request')
                        ->select(DB::raw('SUM(payble_amount) as total_amount, COUNT(id) as total_order, AVG(payble_amount) as avg_amount'))
                        ->where('user_id', '=', $user->id)
                        ->first();

        $last_order = DB::table('order_request')
                            ->select('order_request.*')
                            ->orderBy('id', 'desc')
                            ->where('user_id', '=', $user->id)
                            ->first();
        

        if ($last_order) {
            $last_order_detail = DB::table('order_detail')
                                    ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                                    ->select('product.id','product.product_title', 'product.product_images')
                                    ->where('order_detail.order_id', $last_order->id)
                                    ->get();
        }

        $ctags = DB::table('tags')
                    ->join('customer_tags', 'tags.id', '=', 'customer_tags.tag_id', 'left')
                    // ->where('customer_tags.customer_id', '=', $customerid)
                    ->where('tags.tag_type', '=', 2)
                    ->pluck('tags.name', 'tags.id')
                    ->toArray();
        // dd($ctags);
        if ($user) {
            return view('admin.customer.customer_detail_by_id', compact('ctags', 'user', 'order', 'last_order', 'customer_tags', 'last_order_detail','userdetail'));
        }else{
            return redirect('admin/customers')->with('message', 'Customer Not Found.');
        }

    }

    public function customerAllOrder($id) {

        $all_orders = DB::table('order_request')
                    ->select('order_request.*')
                    ->orderBy('id', 'desc')
                    ->where('user_id', '=', $id)
                    ->get();
        
        return view('admin.customer.customer_all_orders', compact('all_orders'));
    }

    public function addNewCustomer(AdminCustomerRequest $request, $id=null){

        

        if (isStore()) {
            $store = isStore();
            $store_id = $store->id;            
        }else{
            $store_id = $request->get('store_type');
        }

        if ($id) {
            $user = User::find($id);
            $success_msg = "Customer updated Successfully.";
        }else{
    	    $user = new User;
            $success_msg = "Customer created Successfully.";
        }
        if ($request->get('is_accept_marketing') == 'on') {
            $is_accept_marketing = 1;
        }else{
            $is_accept_marketing = 0;
        }
        if ($request->get('is_tax_exempt') == 'on') {
            $is_tax_exempt = 1;
        }else{
            $is_tax_exempt = 0;
        }
        $user->store_id = $store_id;
    	$user->first_name = $request->get('first_name');
    	$user->last_name = $request->get('last_name');
    	$user->email = $request->get('email');
        $user->password = randomPassword();
    	$user->is_accept_marketing = $is_accept_marketing;
    	$user->is_tax_exempt = $is_tax_exempt;
    	$user->contact = $request->get('contact');
        $user->company = $request->get('company');
    	$user->address = $request->get('address');
    	$user->city = $request->get('city');
    	$user->state = $request->get('state');
    	$user->country = $request->get('country');
        $user->zip = $request->get('zip');
    	$user->notes = $request->get('notes');
		$user->save();

        if ($request->get('customer_tags')) {
            $tags = $request->get('customer_tags');
            $tag_ids = [];
            if ($tags) {
                foreach ($tags as $tag) {
                    $ctag = Tags::where('id', '=', $tag)->select('id')->first();
                    if ($ctag) {
                        $ctag->field_id = $user->id;
                        $ctag->save();
                        array_push($tag_ids, $ctag->id);
                    } else {
                        $ptag = new Tags;
                        $ptag->name = $tag;
                        $ptag->tag_type = CUSTOMER_TAGS;
                        $ptag->slug = str_slug($tag);
                        $ptag->field_id = $user->id;
                        $ptag->save();
                        array_push($tag_ids, $ptag->id);
                    }
                }
            }
        }

        //  if (isset($tag_ids)) {
        //     foreach ($tag_ids as $tkey => $tvalue) {
        //         $customer_tag = new CustomerTag;
        //         $customer_tag->customer_id = $user->id;
        //         $customer_tag->tag_id = $tvalue;
        //         $customer_tag->save();
        //     }
        // }

		return redirect()->back()->with('success_msg', $success_msg);
    }

    public function editCustomer($id) {

        // $customer_tags = Tags::where('tag_type', '=', 3)->get();
        $customer_tags = DB::table('tags')->where('field_id', '=', $id)->pluck('name')->toArray();

        $customer = User::find($id);
        return view('admin.customer.edit_customer', compact('customer', 'customer_tags'));
    }

    public function getCustomerById(Request $request){


        $userid = $request->get('customerid');
        // $user = User::find($userid);
        $user = DB::table('users')
                        ->join('address', 'users.id', '=', 'address.user_id', 'left')
                        ->select('users.id as userid', 'users.first_name', 'users.last_name','users.email','users.contact', 'users.notes as note', 'address.*')
                        ->where('users.id', $userid)
                        ->first();   
                        // dd($user);  

//        $last_user = User::where('id','=','5')->fist();
        return view('admin.customer.customer_detail', compact('user', 'customer_tags'));
    }


// 

    public function customers(Request $request){

        $query = $request->get('customer_tags');
        $querygetall = $request->get('data_from_table');

        if (isStore()) {
            $store = isStore();

            // $store_id = $store->id;
            // $vendor = Vendor::where('admin_id', $store->id)->select('id')->first();

            if ($query) {
                
                $users1 = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'right')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact')
                            // ->where('order_request.vendor_id', $vendor->id)
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$query%")
                            ->where('tags.tag_type', ORDER_TAGS)
                            ->orWhere('tags.tag_type', CUSTOMER_TAGS)
                            ->distinct()
                            ->paginate (20)->setPath ( '' );
                $users = $users1->appends ( array (
                'data_from_table' => Input::get ( 'data_from_table' ) 
        ) );
            }else{
                $users = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact')
                            // ->where('order_request.vendor_id', $vendor->id)
                            ->orderBy('users.created_at', 'desc')
                            ->distinct()
                            ->paginate(50);            
            }
        //  for all
        }elseif ($querygetall) {
            // dd($querygetall);
            $users1 = DB::table('users')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'left')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.address', 'users.email', 'users.contact','users.address2','users.contact')
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.first_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.last_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address', 'LIKE', "%$querygetall%")
                            ->orWhere('users.email', 'LIKE', "%$querygetall%")
                            ->orWhere('users.contact', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address2', 'LIKE', "%$querygetall%")
                            ->distinct()
                            ->paginate (20)->setPath ( '' );
                $users = $users1->appends ( array (
                'data_from_table' => Input::get ( 'data_from_table' ) 
        ) );
                            // dd($users);
        }else{

            if ($query) {
                $users1 = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'right')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact')
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$query%")
                            ->where('tags.tag_type', ORDER_TAGS)
                            ->orWhere('tags.tag_type', CUSTOMER_TAGS)
                            ->distinct()
                            ->paginate (20)->setPath ( '' );
                $users = $users1->appends ( array (
                'data_from_table' => Input::get ( 'data_from_table' ) 
        ) );
            }elseif ($querygetall) {

                $users1 = DB::table('users')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'right')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.address', 'users.email', 'users.contact','users.address2')
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.first_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.last_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address', 'LIKE', "%$querygetall%")
                            ->orWhere('users.email', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address2', 'LIKE', "%$querygetall%")
                            ->distinct()
                            ->paginate (20)->setPath ( '' );
                $users = $users1->appends ( array (
                'data_from_table' => Input::get ( 'data_from_table' ) 
        ) );
                # code...
            }else{
                $users = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact')
                            ->orderBy('users.created_at', 'desc')
                            ->distinct()
                            ->paginate(50);
            }
        }

        return view('admin.customer.customerlist', compact('users'));

    }

    public function businessCustomers(Request $request)
    {
        $query = $request->get('customer_tags');
        $querygetall = $request->get('data_from_table');

        if (isStore()) {
            $store = isStore();

            // $store_id = $store->id;
            // $vendor = Vendor::where('admin_id', $store->id)->select('id')->first();

            if ($query) {
                
                $users = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'right')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact')
                            // ->where('order_request.vendor_id', $vendor->id)
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$query%")
                            ->where('tags.tag_type', ORDER_TAGS)
                            ->where('users.is_buser', 1)
                            ->orWhere('tags.tag_type', CUSTOMER_TAGS)
                            ->distinct()
                            ->get();
            }else{
                $users = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact')
                            // ->where('order_request.vendor_id', $vendor->id)
                            ->where('users.is_buser', 1)
                            ->orderBy('users.created_at', 'desc')
                            ->distinct()
                            ->get();            
            }
        //  for all
        }elseif ($querygetall) {
            // dd($querygetall);
            $users = DB::table('users')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'left')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.address', 'users.email', 'users.contact','users.address2','users.contact')
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$querygetall%")
                            ->where('users.is_buser', 1)
                            ->orWhere('users.first_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.last_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address', 'LIKE', "%$querygetall%")
                            ->orWhere('users.email', 'LIKE', "%$querygetall%")
                            ->orWhere('users.contact', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address2', 'LIKE', "%$querygetall%")
                            ->distinct()
                            ->get();
                            // dd($users);
        }else{

            if ($query) {
                $users = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'right')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact')
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$query%")
                            ->where('users.is_buser', 1)
                            ->where('tags.tag_type', ORDER_TAGS)
                            ->orWhere('tags.tag_type', CUSTOMER_TAGS)
                            ->distinct()
                            ->get();
            }elseif ($querygetall) {

                $users = DB::table('users')
                            ->join('tags', 'users.id', '=', 'tags.field_id', 'right')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.address', 'users.email', 'users.contact','users.address2')
                            ->orderBy('users.created_at', 'desc')
                            ->where('tags.name', 'LIKE', "%$querygetall%")
                            ->where('users.is_buser', 1)
                            ->orWhere('users.first_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.last_name', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address', 'LIKE', "%$querygetall%")
                            ->orWhere('users.email', 'LIKE', "%$querygetall%")
                            ->orWhere('users.address2', 'LIKE', "%$querygetall%")
                            ->distinct()
                            ->get();
                # code...
            }else{
                $users = DB::table('users')
                            ->join('order_request', 'users.id', '=', 'order_request.user_id', 'left')
                            ->select('users.id', 'users.first_name', 'users.last_name', 'users.location', 'users.email', 'users.contact', 'users.credits')
                            ->where('users.is_buser', 1)
                            ->orderBy('users.created_at', 'desc')
                            ->distinct()
                            ->get();
            }
        }

        return view('admin.customer.business_customerlist', compact('users'));
    }

    public function updateBusinessUserCredits(Request $request)
    {   
        $userid = $request->get('customer_id');
        $credits = $request->get('credits');
        $user = User::find($userid);
        if ($user) {
            
            $user->credits = $credits;
            $user->save();

            $data['status'] = 1;
            $data['message'] = "Credits updated successfully.";

            return $data;
        }else{
            $data['status'] = 0;
            $data['message'] = "User not found.";

            return $data;
        }
    }

    public function addCustomer(AdminCustomerRequest $request, $id=null){
        
        $email_exists = User::where('email', '=', $request->get('email'))->first();
        if ($email_exists) {
            $data['status'] = 0;
            $data['message'] = "Email already exists";
            return $data;
        }
        $validator = Validator::make($request->all(), [
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            $data['status'] = 0;
            $data['message'] = "Address field is required.";
            return $data;
        }
        if ($request->get('id')) {
            $id = $request->get('id');
        }
        if ($id) {
            $user = User::find($id);
            $success_msg = "Customer updated Successfully.";
        }else{
            $user = new User;
            $success_msg = "Customer created Successfully.";
        }
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        if($request->get('email')){
            $user->email = $request->get('email');
        }
        $user->password = randomPassword(1000,10000);
        $user->save();
        
       if ($request->get('is_accept_marketing') == 'on') {
            $is_accept_marketing = 1;
        }else{
            $is_accept_marketing = 0;
        }
        if ($request->get('is_tax_exempt') == 'on') {
            $is_tax_exempt = 1;
        }else{
            $is_tax_exempt = 0;
        }
        $address = Address::where('user_id', '=', $user->id)
                   ->orderBy('created_at', 'desc')
                   ->first();
   if ($address) {
       $address->user_id = $user->id;
       $address->phone = $request->get('contact');
       $address->company = $request->get('company');
       $address->address = $request->get('address');
       $address->address2 = $request->get('address2');
       $address->city = $request->get('city');
       $address->state = $request->get('state');
       $address->country = $request->get('country');
       $address->zip = $request->get('zip');
       $address->notes = $request->get('notes');
       $address->save();
   }else {
        $address = new Address;
        $address->user_id = $user->id;
        $address->phone = $request->get('contact');
        $address->company = $request->get('company');
        $address->address = $request->get('address');
        $address->address2 = $request->get('address2');
        $address->city = $request->get('city');
        $address->state = $request->get('state');
        $address->country = $request->get('country');
        $address->zip = $request->get('zip');
        $address->notes = $request->get('notes');
        $address->save();
    }

        $data = array(
                'email'=>$user->email,
                'password'=>$user->password
                ); 
                
        $user_email = $user->email;
        try {
            Mail::send('mail.user.new_user', $data, function($message) use($user_email){
                $message->from('admin@Babyjalebi.com');
                $message->to($user_email, "Lionfres Support")->subject("Welcome to Babyjalebi");
            });
        } catch (Exception $e) {
            
        }

        // $user = User::where('id','=',$user->id)->first();
        $user = DB::table('users')
                    ->join('address', 'users.id', '=', 'address.user_id', 'left')
                    ->select('address.*', 'users.first_name','users.last_name', 'users.email', 'users.id as userid')
                    ->orderBy('address.created_at', 'desc')
                    ->first();

        
        // $user->password = Hash::make($user->password);
        // $user->save();

        return view('admin.customer.customer_detail', compact('user'));        
    }
// this is for change user address from order detail by id
    public function editCustomerinorderpage(AdminCustomerRequest $request, $id=null,$order_code=null){

         $email_exists = User::where('email', '=', $request->get('email'))->first();
        if ($email_exists) {
            $data['status'] = 0;
            $data['message'] = "Email already exists";
            return $data;
        }
        $validator = Validator::make($request->all(), [
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            $data['status'] = 0;
            $data['message'] = "Address field is required.";
            return $data;
        }
        if ($request->get('id')) {
            $id = $request->get('id');
        }
        if ($id) {
            $user = User::find($id);
            $success_msg = "Customer updated Successfully.";
        }else{
            $user = new User;
            $success_msg = "Customer created Successfully.";
        }
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        if($request->get('email')){
            $user->email = $request->get('email');
        }
        $user->password = randomPassword(1000,10000);
        $user->save();
        
       if ($request->get('is_accept_marketing') == 'on') {
            $is_accept_marketing = 1;
        }else{
            $is_accept_marketing = 0;
        }
        if ($request->get('is_tax_exempt') == 'on') {
            $is_tax_exempt = 1;
        }else{
            $is_tax_exempt = 0;
        }
        $address = Address::where('user_id', '=', $user->id)
                   ->orderBy('created_at', 'desc')
                   ->first();
   if ($address) {
       $address->user_id = $user->id;
       $address->phone = $request->get('contact');
       $address->company = $request->get('company');
       $address->address = $request->get('address');
       $address->address2 = $request->get('address2');
       $address->city = $request->get('city');
       $address->state = $request->get('state');
       $address->country = $request->get('country');
       $address->zip = $request->get('zip');
       $address->notes = $request->get('notes');
       $address->save();
   }else {
       $address = new Address;
       $address->user_id = $user->id;
       $address->phone = $request->get('contact');
       $address->company = $request->get('company');
       $address->address = $request->get('address');
       $address->address2 = $request->get('address2');
       $address->city = $request->get('city');
       $address->state = $request->get('state');
       $address->country = $request->get('country');
       $address->zip = $request->get('zip');
       $address->notes = $request->get('notes');
       $address->save();
   }

        $data = array(
                'email'=>$user->email,
                'password'=>$user->password
                ); 
                
        $user_email = $user->email;
        
        $ordercode = $request->get('order_id');

        // $user = User::where('id','=',$user->id)->first();
        $order = DB::table('order_request')
                        ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                        ->join('vendors_details', 'order_request.vendor_id', '=', 'vendors_details.id', 'left')
                        ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                        ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                        ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                        ->join('payment_status', 'order_request.status', '=', 'payment_status.id', 'left')
                        ->where('order_request.order_code', $ordercode)
                        ->select('order_request.*','vendors_details.store_name','order_status.status_title','payment_status.payment_status_name', 'users.id as userid','users.first_name', 'users.last_name', 'users.email', 'users.contact','users.notes as note', 'address.address', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
                        ->first();

                        $address = Address::where('user_id', $order->user_id)->orderBy('created_at', 'desc')->first();

                        // dd($order);

        
        // $user->password = Hash::make($user->password);
        // $user->save();''
                        // return;

        return view('admin.customer.customer_edit_orderpage', compact('order','address'));        
        

    }
    public function changeCustomerEmail(Request $request){
        $id = $request->get('id');
        $email = $request->get('email');
        $email_exists = User::where('email', $email)->first();

        if ($email_exists) {
            $data['status'] = 0;
            $data['message'] = 'Email already exists';

            return $data;
        }
        $user = User::find($id);
        if ($user) {
            $user->email = $email;
            $user->save();

            $data['status'] = 1;
            $data['email'] = $user->email;
        }else{
            $data['status'] = 0;
            $data['message'] = 'User not found';
        }

        return $data;
    }

    public function addTagsToCustomer(Request $request){
        $tag_ids = $request->get('tag_ids');
        $customer_id = $request->get('cid');
        
        if (isset($tag_ids)) {
            
            DB::table('customer_tags')->where('customer_id', '=', $customer_id)->delete();
            
            foreach ($tag_ids as $tkey => $tvalue) {
                $customer_tag = new CustomerTag;
                $customer_tag->customer_id = $customer_id;
                $customer_tag->tag_id = $tvalue;
                $customer_tag->save();
            }
        
            $customer_tags = DB::table('tags')
                                    ->join('customer_tags', 'tags.id', '=', 'customer_tags.tag_id', 'left')
                                    ->where('customer_tags.customer_id', '=', $customer_id)
                                    ->select('tags.name')
                                    ->get();

            return view('admin.customer.customer_tags', compact('customer_tags'));
        }else{
            return "No Tags Found";
        }
    }

    public function removeCustomer(Request $request){

        $customer_id = $request->get('customer_id');

        if ($customer_id) {
            $customer = User::find($customer_id);
            $customer->delete();

            $orders = Order::where('user_id', '=', $customer_id)->get();

            foreach ($orders as $order) {
                $order->delete();
            }

            $data['status'] = 1;
            $data['message'] = "Customer deleted successfully.";
            
            return $data;;
        }else{
            
            $data['status'] = 0;
            $data['message'] = "Customer not found.";

            return $data;
        }
    }

     public function updateBillingAddressOrderpage(Request $request){
        $user_id = $request->get('user_id');

        $address = Address::where('user_id', '=', $user_id)
                    ->where('is_billing', '=', 1)
                    ->orderBy('created_at', 'desc')
                    ->first();
    if ($address) {
        $address->user_id = $request->get('user_id');
        $address->company = $request->get('company');
        $address->phone = $request->get('contact');
        $address->address = $request->get('address');
        $address->address2 = $request->get('address2');
        $address->country = $request->get('country');
        $address->city = $request->get('city');
        $address->state = $request->get('state');
        $address->zip = $request->get('zip');   
        $address->save();
    }else {
       $address = new Address;
        $address->user_id = $request->get('user_id');
        $address->company = $request->get('company');
        $address->phone = $request->get('contact');
        $address->address = $request->get('address');
        $address->address2 = $request->get('address2');
        $address->country = $request->get('country');
        $address->city = $request->get('city');
        $address->state = $request->get('state');
        $address->zip = $request->get('zip');
        $address->is_billing = 1;
        $address->save();
    }

        

        // $order = User::where('id','=', $user_id)->first();
        $order = DB::table('users')->where('id', '=', $user_id)->first();
        // dd($order);
        // $userid = $request->get('user_id');
        $ordercode = $request->get('order_id');

        // $user = User::where('id','=',$user->id)->first();
        
                        // dd($order);

                        $address2 = Address::where('user_id', $user_id)->where('is_billing','=',1)->first();
                        // dd($address2);

        return view('admin.customer.customer_billingaddchange', compact('address2', 'order'));
    }  

    public function updateBillingAddress(Request $request){

        $address = new Address;
        $address->user_id = $request->get('user_id');
        $address->company = $request->get('company');
        $address->phone = $request->get('contact');
        $address->address = $request->get('address');
        $address->address2 = $request->get('address2');
        $address->country = $request->get('country');
        $address->city = $request->get('city');
        $address->state = $request->get('state');
        $address->zip = $request->get('zip');
        $address->is_billing = 1;
        $address->save();

        $user = User::where('id', $request->get('user_id'))->first();

        return view('admin.customer.billing_address', compact('address', 'user'));
    }

     public function addNotes(Request $request){

        $notes = $request->get('notes');
        $customer_id = $request->get('customer_id');

        if ($notes) {
            $customer = User::find($customer_id);
            $customer->notes = $notes;
            $customer->save();

            $data['status'] = 1;
            $data['message'] = "Notes Saved.";
            
            return $data;;
        }else{
            
            $data['status'] = 0;
            $data['message'] = "Customer not found.";

            return $data;
        }
    }
}