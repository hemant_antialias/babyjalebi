<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
// use Carbon;
use Mail;
use App\AbandonedCart;
use App\NotifyStock;

class CronController extends Controller
{
    

    public function abondandCart()
    {
        
        $cart_abandon_cut_off_time = 60;
        $template_to_send_after_time = 20;
        
        $current_time = \Carbon\Carbon::now();
        $first = $current_time->toDateTimeString();

        $cart_abandon_cut_off_time = $current_time->subMinutes($cart_abandon_cut_off_time);
        $second = $cart_abandon_cut_off_time->toDateTimeString();

        $abondand_order = DB::table('order_request')
                            ->where('created_at', '>', $second)
                            ->where('order_status', '=', 7)
                            ->get();

        // dd($abondand_order);
        foreach ($abondand_order as $order) {

            $detail = DB::table('order_request')
                            ->join('order_detail', 'order_request.id', '=', 'order_detail.order_id', 'left')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                            ->select('users.email', 'product.product_title', 'product.price', 'order_detail.quantity', 'order_request.payble_amount', 'order_request.discount', 'order_request.order_code')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->get();
            
            // dd($detail);

            $user_email = 'abhishekgautam76@gmail.com';
            $order_code = 'YYUIIYU';

            $abandoned_cart = AbandonedCart::where('email', $user_email)->first();

            if (!$abandoned_cart) {
                $data = [
                    'cart_value' => $detail,
                    'order_code' => $order->order_code
                ];

                try {
                    Mail::send('mail.order.abandoned_cart', $data, function($message) use($user_email, $order_code){
                        $message->from('wecare@babyjalebi.com');
                        $message->to($user_email, "Babyjalebi Support")->subject("Order #".$order_code." confirmed");
                    });
                } catch (Exception $e) {
                    
                }

                $abandoned_cart = new AbandonedCart;
                $abandoned_cart->email = $user_email;
                $abandoned_cart->status = 1;
                $abandoned_cart->save();
            }
        }

        return "Done";
    }

    public function notifyStock()
    {

        $product_ids = NotifyStock::where('is_sent', 0)->select('product_id', 'store_id', 'email','location', 'is_sent')->get();

        foreach ($product_ids as $product_id) {
            $product = DB::table('product')
                            ->join('inventory', 'product.id', '=', 'inventory.product_id')
                            ->where('product.id', $product_id->product_id)
                            ->where('inventory.store_id', $product_id->store_id)
                            ->select('product.product_title','product.slug','inventory.quantity')
                            ->first();
            
            if ($product->quantity > 0) {
                $user_email = $product_id->email;
                $data = [
                    'product_title' => $product->product_title,
                    'slug' => $product->slug,
                    'location' => $product_id->location,
                ];

                try {
                    Mail::send('mail.notify_stock', $data, function($message) use($user_email){
                        $message->from('wecare@babyjalebi.com');
                        $message->to($user_email, "Babyjalebi Support")->subject("Product is now available");
                    });
                } catch (Exception $e) {
                    
                } 

                $product_ids = NotifyStock::where('email', $user_email)->where('is_sent', 0)->first();
                $product_ids->is_sent = 1;
                $product_ids->save();

            }

        }

        return "Done";
    }
}



