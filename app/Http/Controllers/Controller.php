<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Session;
use Cart;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){

    	$cart_count = Cart::count();
        // View::share('cart_count', $cart_count);
        // $store_id = session('store_id');
        // echo   $store_id ;
         
        // $brandname = DB::table('product_category')->whereIn('store_id', [$store_id ,0])->where('collection_type_id', '=', 3)->select('id', 'name', 'slug', 'status')->get();
        // // $meats = DB::table('product_category')->whereIn('store_id', [$store_id ,0])->where('collection_type_id', '=', 2)->select('id', 'name', 'slug', 'status')->get();
        // $categories = DB::table('product_category')->whereIn('store_id', [$store_id ,0])->where('collection_type_id', '=', 1)->select('id', 'name', 'slug', 'status')->get();
        // $cities = DB::table('city')->select('city_id', 'city')->where('status', '=', 1)->get();

        View::share(['cart_count' => $cart_count]);
    }
}
