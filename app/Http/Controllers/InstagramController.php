<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App;
use Illuminate\Http\Request;
use Vinkla\Instagram\Instagram;

class InstagramController extends Controller
{
    public function instagramFeed()
    {
    	$instgram = new Instagram();
    	$instagrams = $instgram->get('babyjalebi');
    	// $response = Instagram::users('barberlinkapp')->getMedia();
    	// $instgrams = $response->get();

    	return view('web-files.instagram' , compact('instagrams'));
    }
}
