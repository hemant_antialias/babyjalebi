<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Mail;
use App\Product;
use App\ProductImages;
use App\Brands;
use App\MeatTypes;
use App\ProductCategory;
use App\ProductCategoreis;
use App\ProductReviews;
use DB;
use URL;
use Cart;
use App\Tracker;
use App\Vendor;
use App\User;
use App\Blog;
use App\Visitor;
use App\NotifyStock;
use App\Subscriber;
use App\BannerImages;
use App\RelatedProduct;
use Validator;
use Vinkla\Instagram\Instagram;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $cookie_name = "store";
        $cookie_value = $request->get('store');
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
        // $this->middleware('store');
        parent::__construct();
    }

    public function testCron(){
            
        // try{
        //     Mail::send('mail.order.order_success', [], function($message){
        //         $message->from('founder@theantialias.com');
        //         $message->to('abhishek.gautam@theantialias.com', "Lionfres Support")->subject("Welcome To babyjelabi");
        //     });
        // }catch (Exception $e){
            
        // }
        // 
        $cart = DB::table('top_cart_product')->where('id', 7)->delete();

        return $cart;
    }

    public function query(Request $request)
    {
        $query = $request->get('term');
        // dd($query);
        $res = DB::table('product')
                    // ->join('product_category as pcc', 'product.product_category', '=', 'pcc.id', 'left')
                    ->where('product.status', '=', 1)
                    ->where('product_title', 'LIKE', "%$query%")
                    // ->orWhere('pcc.name', 'LIKE', "%$query%")
                    ->select('product.*')
                    ->get();


        // dd($res);
        if (count($res) > 0) {
            //This will only send the e-mail and avatar to avoid that users can view all the rows from selected user table
            foreach($res as $index=>$user ){
                $usersArray[$index] = [
                    'name' => $user->product_title,
                    'url' => URL::to('/product/search/'.$user->slug),
                    'image_url' => URL::to('/product_images/'.$user->id.'/featured_images/'.$user->product_images)
                ];
            }
            return response()->json($usersArray);
        }else{
            $usersArray[] = [
                    'name' => "No Result Found",
                    'url' => "#",
                    'image_url' => URL::to('/web-assets/images/default_product.jpg')
                ];

            return response()->json($usersArray);

        }
    }

    // search product detail
    public function productserachDetail(Request $request, $productslug)
    // public function productDetail(Request $request)
    {
        // dd("fdfdd");
                $product = DB::table('product')
                            ->join('product_categories', 'product.product_category', '=', 'product_categories.id', 'left')
                            ->where('product.slug', $productslug)
                            ->select('product.*')
                            ->first();

                $subcategory1 = DB::table('product_categories')->where('product_id', $product->id)->first();
                $subcategory2 = DB::table('product_category')->where('id', $subcategory1->category_id)->first();
                $subcategory = $subcategory2->slug;
                            // dd($subcategory); 
            // $product_images = DB::table('product_images')->where('product_id',$product->id)->get();

     $product_images1 = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_variations.product_id', $product->id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id','product_variations.product_id')
                                            ->first();
                                         if($product_images1 == null)
                                         {

     $product = DB::table('product')
                            ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                            ->where('product.slug', $productslug)
                            ->select('product.*', 'product_category.name as category_name')
                            ->first();
                            // dd("fdfdfd");
            $product_images = DB::table('product_images')->where('product_id',$product->id)->get();

            // dd($product_images);
            if ($product) {
                // dd("fdfd");
                $recentproduct = Product::limit(4)->get();
                $relatedp = RelatedProduct::where('product_id','=',$product->id)->get();
                }
                                         dd('NULL value');
                $attributname = "test";

                                        }
                                        else
                                        {
                         $attributname = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->join('attribute_value', 'attribute_value.id', '=', 'product_variations.attribute_value_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('attribute_value.value')
                                            
                                            ->first();
                                         // dd($product_images1);
                                            $product_images = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id')
                                            ->groupby('product_variation_images.images')
                                            ->get();
                                         // dd('NULL value noet');


                                        }
                            
                                         // dd($attributname);
                            
    

                                        
        
                                         // dd($product_images);
                    // if($subcategory)
                    // {

                    // }
                                            
                $recentproduct = Product::limit(4)->get();
                $relatedp = RelatedProduct::where('product_id','=',$product->id)->get();


                return view('web-files.product-detail ', compact('product', 'category', 'subcategory', 'product_images','recentproduct','relatedp','attributname'));
           
    }


    // contact form send
    public function sendContact(Request $request){
        // dd($request->all());
        $user = $request->get('name');
        $email = $request->get('email');
        $contact = $request->get('contact');
        $message = $request->get('message');

        // $user = User::findOrFail($id);

        try{
            Mail::send('mail.contact_us_temp', array('user'=>$user , 'mail' =>$email, 'contact' =>$contact, 'msg'=>$message ), function($message){
                $message->from('info@theantialias.com');
                $message->to('info@theantialias.com', "Babyjalebi Support")->subject("Welcome To Babyjalebi");
            });
        }catch (Exception $e){
            
        }
        
    
        $data = '1';
        return $data;
    }

    public function allStoresByCity(Request $request){

        $city_id = $request->get('city_id');
        $stores = DB::table('store_locations')
                    ->where('city_id', '=', $city_id)
                    ->select('location', 'slug')
                    ->orderBy('location' ,'asc')
                    ->get();
        // dd($stores);        
        $out = '<select classs="js-example-basic-multiple form-control" name="store" id="store_id" style="
    height: 50px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 0px;
        width: 32%;
">';
        $out .= '<option value="">Select</option>';
        foreach($stores as $store){

            if ($store->location == null) {
                continue;
            }
            $out .= '<option value="'.strtolower($store->slug).'">' . $store->location . '</option>';
        }
        $out .= '</select>';

        return $out;
    }

    public function chicken()
    {
        return view('web.chicken');
    }

    public function cityByHomepage(Request $request){

        if ($request->get('store')) {
            $store_slug = str_slug($request->get('store'));
           
            $store = DB::table('vendors_details')
                        ->join('store_locations', 'vendors_details.id', '=', 'store_locations.store_id', 'left')
                        ->where('store_locations.slug', $store_slug)
                        ->select('vendors_details.*', 'store_locations.slug as location_slug', 'store_locations.location')
                        ->first();
                
        } else {
            $store = DB::table('vendors_details')
                        ->join('store_locations', 'vendors_details.id', '=', 'store_locations.store_id', 'left')
                        ->where('store_locations.slug', $request->session()->get('location_slug'))
                        ->select('vendors_details.*', 'store_locations.slug as location_slug')
                        ->first();
        }
        
        // visitor track entry
        Tracker::hit();
        
        if ($store) {
            // Cart::destroy();
            $request->session()->put('store', $store->store_name);
            $request->session()->put('store_id', $store->id);
            $request->session()->put('location_slug', $store->location_slug);

            // dd($store->id);
            $product_categories = ProductCategory::whereIn('store_id', [$store->id,0])
                                                    ->where('collection_type_id', '=', 1)
                                                    ->where('status', '=', 1)
                                                    ->get();
                                                    // dd($store->id);
            
            $lacarne_products = DB::table('product')
                                    ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
                                    ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                                    ->whereIn('product.vendor_id', [$store->id ,0])
                                    ->where('inventory.store_id', $store->id)
                                    ->where('product.brand_type', 5)
                                    ->where('product.status', 1)
                                    ->select('product.id','product.product_title','product.product_images', 'product.price','product.compare_price', 'product.slug', 'product.quantity', 'product_category.slug as collection_slug','inventory.quantity as totalquantity')
                                    ->get();

            $howdyji_products = DB::table('product')
                                        ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
                                        ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                                        ->whereIn('product.vendor_id', [$store->id ,0])
                                        ->where('inventory.store_id', $store->id)
                                        ->where('product.brand_type', 4)
                                        ->where('product.status', 1)
                                        ->select('product.id','product.product_title','product.product_images', 'product.price', 'product.compare_price', 'product.slug', 'product.quantity', 'product_category.slug as collection_slug','inventory.quantity as totalquantity')
                                        ->get();

            $lionfresh_products = DB::table('product')
                                        ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
                                        ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                                        ->whereIn('product.vendor_id', [$store->id ,0])
                                        ->where('inventory.store_id', $store->id)
                                        ->where('product.brand_type', 6)
                                        ->where('product.status', 1)
                                        ->select('product.id','product.product_title','product.product_images', 'product.price','product.compare_price', 'product.slug', 'product.quantity', 'product_category.slug as collection_slug','inventory.quantity as totalquantity')
                                        ->get();

            $banners = BannerImages::whereIn('city_id', [$store->id,0])->where('status', '=' , 1)->get();
            // dd($banners);
            $blogs = DB::table('blog')
                        ->join('blog_category', 'blog.blog_category_id', '=', 'blog_category.id', 'left')
                        ->where('blog.blog_category_id', 3)
                        ->whereIn('blog.store_id', [$store->id,0])
                        ->select('blog.*', 'blog_category.category','blog_category.slug as category_slug')
                        ->take(3)
                        ->get();
                        // dd($blogs);

            return view('web.index', compact('product_categories', 'lacarne_products', 'howdyji_products', 'lionfresh_products', 'banners', 'blogs'));
            
        }else{
            return redirect('/');
        }

    }

    public function index(Request $request)
    {
        
        // $request->session()->put('location', $location);

Tracker::hit();
        $product_categories = ProductCategory::all();
        
        $special_cat = ProductCategory::where('is_special', 1)->select('name','slug','id')->get();

        $homepage_cat = ProductCategory::where('in_homepage', 1)->select('name','slug','id','image')->get();

        // dd($homepage_cat);
        $howdyji_products = Product::where('brand_type', 2)->select('id','product_title', 'price')->get();
        $lionfresh_products = Product::where('brand_type', 3)->select('id','product_title', 'price')->get();
        $sliders = BannerImages::all();
        // instagram feeds
        $instgram = new Instagram();
        $instagrams = $instgram->get('babyjalebi');
        // dd("fdfdf");
        return view('web-files.index', compact('product_categories','sliders', 'special_cat','homepage_cat','instagrams'));
    }

    public function productIndex(Request $request, $categoryslug = null)
    {
        // dd('fdfd');
        // $request->session()->put('location', $location);
        $product_category = ProductCategory::where('slug', $categoryslug)->first();
        $product_category1 = DB::table('category_parent')
                    ->join('product_category', 'product_category.id', '=', 'category_parent.category_id', 'left')
                    ->where('category_parent.parent_category_id', $product_category->id)
                    ->select('category_parent.*', 'product_category.name as category_name','product_category.slug')
                    ->first();
        // dd($product_category1);
        // $product_category1 = ProductCategoryies::where('parent_category_id', $product_category->id)->first();
        // dd($product_category1);

        if($product_category1)
        {
        //dd('fdfd');


            $subcategories = DB::table('category_parent')
                    ->join('product_category', 'product_category.id', '=', 'category_parent.category_id', 'left')
                    ->where('category_parent.parent_category_id', $product_category->id)
                    ->select('product_category.*')
                    ->get();
                // dd($subcategories);
            // $subcategories = ProductCategory::where('parent_id', $product_category->id)->get();
            return view('web-files.sub_category', compact('subcategories','categoryslug'));
        }
        else
        {
        // dd('fdfd');

            // $product_categories = ProductCategory::all();
            $special_cat = ProductCategory::where('is_special', 1)
                                            ->select('name')
                                            ->get();
            $products = DB::table('product_categories')
                    ->join('product', 'product.id', '=', 'product_categories.product_id', 'left')
                    ->where('product_categories.category_id', $product_category->id)
                    ->where("product.status",'=',1)
                    ->select('product.*')
                    ->orderBy('product.price','asc')
                    ->get();
            // $products = Product::where('product_category', $product_category->id)
            //                     ->select('id','product_title', 'price','product_images', 'slug')
            //                     ->get();
                                // dd($products);
            $sliders = BannerImages::all();

            $recentproduct = Product::limit(4)->get();
            // dd($recentproduct);

            return view('web-files.product', compact('product_categories','sliders', 'lacarne_products', 'special_cat', 'products','categoryslug','recentproduct'));
        }
    }

    public function withSubcategory(Request $request, $categoryslug, $subcategoryslug = null)
    {
        
        
        if ($categoryslug && !$subcategoryslug) {

            $category = DB::table('product_category')->where('slug', $categoryslug)->first();

            if ($category) {
        // dd('dsdsd');
                $products = DB::table('product_categories')
                    ->join('product', 'product.id', '=', 'product_categories.product_id', 'left')
                    ->where('product_categories.category_id', $subcategory->id)
                    ->where("product.status",'=',1)
                    ->select('product.*')
                    ->orderBy('product.price','asc')
                    ->get();
                $category = $category->name;
                 $sidecat = ProductCategory::where('is_menu', 1)->get();
                 
                 $recentproduct = Product::limit(4)->get();
                return view('web-files.product', compact('products','category', 'categoryslug','sidecat','recentproduct'));
            }else{
                return abort('404');
            }

        }
        if ($categoryslug && $subcategoryslug) {
        // dd('cdc');


            $subcategory = DB::table('product_category')->where('slug', $subcategoryslug)->first();
            // $parentcat1 = DB::table('product_category')->where('id', $subcategory->parent_id)->first();
            if ($subcategory) {
                // dd($subcategory->id);
                // $products = DB::table('product')->where('product_category', $subcategory->id)->get();
                 $products = DB::table('product_categories')
                    ->join('product', 'product.id', '=', 'product_categories.product_id', 'left')
                    ->where('product_categories.category_id', $subcategory->id)
                    ->where("product.status",'=',1)
                    ->select('product.*')
                    ->orderBy('product.price','asc')
                    ->get();
                    

                    // $categoryslug = $subcategory->name;
                    // $parentcat = $parentcat1->name;
                 // dd($products);

                $recentproduct = Product::limit(4)->get();
                return view('web-files.product', compact('products','categoryslug', 'subcategoryslug','sidecat','recentproduct'));
                // return view('web-files.product-detail ', compact('products', 'category', 'subcategory', 'product_images','recentproduct','relatedp','attributname'));
            }else{
                return redirect('/'.$categoryslug);
            }
        }
    }

    public function productDetail(Request $request, $category, $subcategory = null, $productslug)
    {
        // dd("fdfdd");

        if ($category && $subcategory && $productslug) {

            $product = DB::table('product')
                            ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                            ->where('product.slug', $productslug)
                            ->select('product.*', 'product_category.name as category_name')
                            ->first();
                            // dd("fdfdfd");
            $product_images = DB::table('product_images')->where('product_id',$product->id)->get();

            // dd($product_images);
            if ($product) {
                // dd("fdfd");

                 $product_images1 = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_variations.product_id', $product->id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id','product_variations.product_id')
                                            ->first();
                     $attributname = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->join('attribute_value', 'attribute_value.id', '=', 'product_variations.attribute_value_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('attribute_value.value')
                                            
                                            ->first();
                                         // dd($product_images1);
                                            $product_images = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id')
                                            ->groupby('product_variation_images.images')
                                            ->get();
                $recentproduct = Product::limit(4)->get();
                $relatedp = RelatedProduct::where('product_id','=',$product->id)->get();

                // dd($relatedp);

                return view('web-files.product-detail', compact('product', 'category', 'subcategory', 'product_images','recentproduct','relatedp','relatedp','attributname'));
            }else{
                return abort('404');
            }
        }
    }

    public function variationsimges(Request $request)
    {

        $varid = $request->get('imgvar');
        $pid = $request->get('pid');
         // $productvar = DB::table('product_variations')
         //            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
         //            ->where('product_variation_images.variation_id', $varid)
         //            ->select('product_variation_images.*' ,'product_variations.product_id')
         //            ->get();

        // abhishek code implement
        $product_images = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_id', $pid)
                                            ->where('attribute_value_id', $varid)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id')
                                            ->groupby('product_variation_images.images')
                                            ->get();

        // dd($product_images);
                    // dd($product_images);

        return view('web-files.variation', compact('product_images','pid'));
    }

    // for bed in a bag
    public function productDetailstatic(Request $request, $productslug)
    {
                // dd("pil.low");
                $product = DB::table('product')
                            ->join('product_categories', 'product.product_category', '=', 'product_categories.id', 'left')
                            ->where('product.slug', $productslug)
                            ->where("product.status",'=',1)
                            ->select('product.*')
                            ->first();
                            // dd($product);
                            $subcategory1 = DB::table('product_categories')
                                        ->join('product_category', 'product_category.id', '=', 'product_categories.category_id', 'left')
                                        ->select('product_category.slug')
                                        ->where('product_categories.product_id' , '=' ,$product->id)->first();
                                        // dd($subcategory);
            // $product_images = DB::table('product_images')->where('product_id',$product->id)->get();

     $product_images1 = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_variations.product_id', $product->id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id','product_variations.product_id')
                                            ->first();
    $attributname = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->join('attribute_value', 'attribute_value.id', '=', 'product_variations.attribute_value_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('attribute_value.value')
                                            
                                            ->first();
                            
                                         // dd($attributname);
                            
    $product_images = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id')
                                            ->groupby('product_variation_images.images')

                                            ->get();

                                        
        
                                            
                $recentproduct = Product::limit(4)->orderBy('id','DESC')->get();
                $relatedp = RelatedProduct::where('product_id','=',$product->id)->get();
                
                 $subcategory = $subcategory1->slug;
                 
                                         // dd($subcategory);


                return view('web-files.product-detail ', compact('product', 'category', 'subcategory', 'product_images','recentproduct','relatedp','attributname'));

    }
    // for diaper bags
     public function productDetailstatic1(Request $request, $productslug)
    {
        // dd("fdfdfd");
                $product = DB::table('product')
                            ->join('product_categories', 'product.product_category', '=', 'product_categories.id', 'left')
                            ->where('product.slug', $productslug)
                            ->select('product.*')
                            ->first();
                            // dd($product);
            // $product_images = DB::table('product_images')->where('product_id',$product->id)->get();

     $product_images1 = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_variations.product_id', $product->id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id','product_variations.product_id')
                                            ->first();
    $attributname = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->join('attribute_value', 'attribute_value.id', '=', 'product_variations.attribute_value_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('attribute_value.value')
                                            
                                            ->first();
                            
                                         // dd($attributname);
                            
    $product_images = DB::table('product_variations')
                                            ->join('product_variation_images', 'product_variations.id', '=', 'product_variation_images.variation_id', 'left')
                                            ->where('product_id', $product_images1->product_id)
                                            ->where('attribute_value_id', $product_images1->attribute_value_id)
                                            ->select('product_variation_images.*', 'product_variations.attribute_value_id')
                                            // ->groupby('product_variation_images.images')
                                            ->get();

                                        
        
                                         // dd($product_images);
                                            
                $recentproduct = Product::limit(4)->get();
                $relatedp = RelatedProduct::where('product_id','=',$product->id)->get();


                 $subcategory = "bed-in-a-bag";


                return view('web-files.product-detail ', compact('product', 'category', 'subcategory', 'product_images','recentproduct','relatedp','attributname'));

    }

    public function allProducts()
    {
        $products = Product::all();
        return view('web.allproducts', compact('products'));
    }

    public function ajaxProductList(Request $request){

        if ($request->get('categoryslug') && !$request->get('subcategoryslug')) {
            $collection = ProductCategory::where('slug', $request->get('categoryslug'))->first();
        }
        if ($request->get('categoryslug') && $request->get('subcategoryslug')) {
            $collection = ProductCategory::where('slug', $request->get('subcategoryslug'))->first();
        }

        $query  = DB::table('product')->where('product_category' ,$collection->id);
        if ($request->get('sort_by_filter') == "newest") {
            
            $query->orderBy('product.created_at', 'desc');
        }

        if ($request->get('sort_filter') == "ltoh") {
            $query->orderBy('product.price', 'asc');
        }

        if ($request->get('sort_filter') == "htol") {
            $query->orderBy('product.price', 'desc');
        }
        if ($request->get('sort_filter') == "popularity") {
            $query->orderBy('product.price', 'desc');
        }
        if ($request->get('min_price') || $request->get('max_price')) {

            $min = (int)$request->get('min_price');
            $max = (int)$request->get('max_price');

            $query->where('product.price', '>', $min);
            $query->where('product.price', '<', $max);
        }

        $products = $query->where('status', '=', 1)->get();

        if (count($products) > 0) {
            if ($request->ajax()) {

                return view('web-files.product_list')->with('products', $products)->render();
            }else{
                return view('web-files.product_list')->with('products', $products);
            }
        }else{
            return view('web.no_product_found');
        }
        
    }

    public function shopByCollections(Request $request, $category){

        $store_id = $request->session()->get('store_id');
        if (!$store_id) {
        	return redirect('/');
        }
        $product_category = ProductCategory::where('slug', $category)->where('collection_type_id', '=', 1)->first();
        $meat_types = ProductCategory::where('slug', $category)->where('collection_type_id', '=', 2)->first();
        $brands = ProductCategory::where('slug', $category)->where('collection_type_id', '=', 3)->first();

        if ($product_category) {
            $products = DB::table('product')
                            ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
                            ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                            ->whereIn('product.vendor_id', [$store_id ,0])
                            ->where('inventory.store_id', $store_id)
                            ->where('product.product_category', $product_category->id)
                            ->where('product.status', 1)
                            ->select('product.*', 'product_category.slug as collection_slug', 'inventory.quantity as totalquantity')
                            ->orderBy('product.product_title', 'asc')
                            ->get();

            return view('web.product_category.product_category', compact('products', 'product_category', 'category'));
        }

        if ($meat_types) {
            // $products = Product::whereIn('vendor_id', [$store_id ,0])->where('meat_type', $meat_types->id)->where('status', 1)->get();
            $products = DB::table('product')
                            ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
                            ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                            ->whereIn('product.vendor_id', [$store_id ,0])
                            ->where('inventory.store_id', $store_id)
                            ->where('product.meat_type', $meat_types->id)
                            ->where('product.status', 1)
                            ->select('product.*', 'product_category.slug as collection_slug' , 'inventory.quantity as totalquantity')
                            ->orderBy('product.product_title', 'asc')
                            ->get();
                            // dd($products);





            return view('web.meat_products.meat_products', compact('products', 'meat_types', 'category'));
        }
        if ($brands) {
            // $products = Product::whereIn('vendor_id', [$store_id ,0])->where('brand_type', $brands->id)->where('status', 1)->get();
            $products = DB::table('product')
                        ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
                        ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
                        ->whereIn('product.vendor_id', [$store_id ,0])
                        ->where('inventory.store_id', $store_id)
                        ->where('product.brand_type', $brands->id)
                        ->where('product.status', 1)
                        ->select('product.*', 'product_category.slug as collection_slug', 'inventory.quantity as totalquantity')
                        ->orderBy('product.product_title', 'asc')
                        ->get();

            return view('web.brand_products.brand_products', compact('products', 'brands', 'category'));
        }
        return view('404');
    }

    // public function productDetail(Request $request, $slug){
        
    //     if ($request->get('location')) {
            
    //         $store_slug = str_slug($request->get('location'));

    //         $store = DB::table('vendors_details')
    //                     ->join('store_locations', 'vendors_details.id', '=', 'store_locations.store_id', 'left')
    //                     ->where('store_locations.slug', $store_slug)
    //                     ->select('vendors_details.*', 'store_locations.slug as location_slug', 'store_locations.location')
    //                     ->first();


    //         $request->session()->put('store', $store->store_name);
    //         $request->session()->put('store_id', $store->id);
    //         $request->session()->put('location_slug', $store->location_slug);

    //     }

    //     $store_id = $request->session()->get('store_id');
    //     if (!$store_id) {
    //     	return redirect('/');
    //     }
    //     // $product = Product::where('slug', $slug)->first();
    //     $product = DB::table('product')
    //                     ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
    //                     ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
    //                     ->whereIn('product.vendor_id', [$store_id ,0])
    //                     ->where('inventory.store_id', $store_id)
    //                     ->where('product.slug', '=', $slug)
    //                     // ->where('product_category.slug', '=', $collection)
    //                     ->select('product.*', 'product_category.name as brand_name', 'product_category.slug as brand_slug', 'inventory.quantity as totalquantity')
    //                     ->first();
    //     // dd($product_reviews);
    //     if ($product) {
    //     // dd($product);
    //     $product_images = ProductImages::where('product_id', $product->id)->get();
    //     $similar_products = DB::table('product')
    //                             ->join('product_category', 'product.brand_type', '=', 'product_category.id', 'left')
    //                             ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left')
    //                             ->whereIn('product.vendor_id', [$store_id ,0])
    //                             ->where('inventory.store_id', $store_id)
    //                             ->orderBy(DB::raw('ABS(`price` - '.$product->price.')'))
    //                             ->whereNotIn('product.id', [$product->id])
    //                             ->where('product.status', 1)
    //                             ->where('product.product_category', $product->product_category)
    //                             ->select('product.*', 'product_category.slug as collection_slug', 'inventory.quantity as totalquantity')
    //                             ->take(6)
    //                             ->get();
        
    //     $product_reviews = ProductReviews::where('product_id', $product->id)->where('status', '=', 2)->get();
    //         return view('web.detail', compact('product', 'product_images', 'similar_products','product_reviews'));
    //     }else{
    //         abort('404');
    //     }

    // }

    public function writeReviews(Request $request){
        // dd($request->all());
        // dd("dssds");

        $product_id = $request->get('product_id');
        $rating = $request->get('rating');
        // $name = $request->get('name');
        // $email = $request->get('email');
        $email = $request->get('email');
        $name = $request->get('name');
        $comment = $request->get('message');
        // dd($comment);
        $review = new ProductReviews;
        $review->product_id = $product_id;
        $review->rating = $rating;
        $review->name = $name;
        // $review->title= $title;
        $review->comment = $comment;
        $review->status = REVIEW_PENDING;
        $review->save();


         $data['status'] = 1;
        $data['message'] = "Thanks for writing a review.";
        return $data;
    }

   public function blogList(Request $request, $bcategory){

            $store_id = $request->session()->get('store_id');
            $store_detail = Vendor::where('admin_id', '=', $store_id)->select('id')->first();

            $blogs = DB::table('blog')
                        ->leftJoin('blog_category', 'blog.blog_category_id', '=', 'blog_category.id')
                        ->select('blog.*', 'blog_category.category')
                        ->whereIn('store_id', [$store_id ,0])
                        ->where('blog_category.slug', '=', $bcategory)
                        ->orderBy('blog.created_at', 'desc')
                        ->simplePaginate(10);
            // dd($blogs);
        return view('web.blog.blog_list', compact('blogs'));
    }
    public function blogDetail(Request $request, $bcategory, $slug){

            $store_id = $request->session()->get('store_id');
            $store_detail = Vendor::where('admin_id', '=', $store_id)->select('id')->first();

            $blog_detail = DB::table('blog')
                        ->leftJoin('blog_category', 'blog.blog_category_id', '=', 'blog_category.id')
                        ->select('blog.*', 'blog_category.category')
                        ->whereIn('blog.store_id', [$store_id ,0])
                        ->where('blog.slug', '=', $slug)
                        ->where('blog_category.category', '=', $bcategory)
                        ->first();

        return view('web.blog.blog_detail', compact('blog_detail'));
    }

    public function addSubscriber(Request $request){

        $validator = Validator::make($request->all(), [
            'subscriber_email'    => 'required|email',
        ]);

        if ($validator->fails()) {

            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        $email = $request->get('subscriber_email');
        $subscribe = new Subscriber;
        $subscribe->email = $email;
        $subscribe->save();

        $data['status'] = 1;
        $data['message'] = "You have subscribed successfully.";

        return $data;
    }

    public function emailWhenAvailable(Request $request)
    {
            $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
        ]);

        if ($validator->fails()) {

            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        $store_id = $request->session()->get('store_id');
        $location = $request->session()->get('location_slug');
        $store = $request->session()->get('store');
        $email = $request->get('email');

        $notifystock = new NotifyStock;
        $notifystock->email = $email;
        $notifystock->product_id = $request->get('product_id');
        $notifystock->store_id = $store_id;
        $notifystock->store = $store;
        $notifystock->location = $location;
        $notifystock->is_sent = 0;
        $notifystock->save();

        $data['status'] = 1;
        $data['message'] = "Your notification has been registered";

        return $data;    
    }
}
