<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\Product;
use Softon\Indipay\Facades\Indipay;
use Cart;
use Auth;
use Paytm;
use Mail;
use App\OrderRequest;
use App\User;
use App\Address;
use App\OrderDetail;
use App\Inventory;

use DB;
use App\Payment;

class PaymentController extends Controller
{

	function __construct()
    {
        // $this->vanityUrl = Config::get('indipay.citrus.vanityUrl');
        $this->merchantKey = Config::get('indipay.citrus.merchantKey');
        // $this->testMode = Config::get('indipay.testMode');

        $this->parameters['merchantTxnId'] = $this->generateTransactionID();
        $this->parameters['currency'] = 'INR';
        $this->parameters['returnUrl'] = url(Config::get('indipay.citrus.returnUrl'));
    }

	public function generateTransactionID()
    {
        return substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    }

	public function freechargePayment(Request $request){

		$merchantId = Config::get('services.freecharge.merchantId');
		$merchant_key = Config::get('services.freecharge.merchant_key');
		
	    $order_request = DB::table('order_request')
							->join('users', 'order_request.user_id', '=', 'users.id', 'left')
							->where('order_code', $request->get('ordercode'))
							->select('order_request.*', 'users.contact', 'users.email')
							->first();

		if ($order_request) {
			$amount = round($order_request->payble_amount, 2);

			$data = array('merchantTxnId'=> $order_request->order_code, 'currency' => 'INR' ,'amount'=> (string)$amount, 'surl' => url('/spayment/callback'),'furl' => url('/fpayment/callback'), 'merchantId'=>  $merchantId, 'channel' => 'WEB', 'email' => $order_request->email );

			// $merchant_key = 'bbe7759d-8694-4be1-870d-839e78c1e1df';
		    $json_encoded_data = json_encode($data);
		    $json_decoded_data = json_decode($json_encoded_data, TRUE);

			$hash = Payment::generateChecksumForJson($json_decoded_data,$merchant_key);
		    
		    $request->session()->put('order_code', $order_request->order_code);
		    $request->session()->put('order_id', $order_request->id);


	    	return view('web.freecharge.freecharge', compact('hash', 'data'));
		}else{
			return redirect('/cart');
		}

	}

	public function freechargeSuccessResponse(Request $request){
			
		$ordercode = $request->session()->get('order_code');
    	$order_id = $request->session()->get('order_id');

		$paymentResponse =  $request->all();
		if (strtolower($paymentResponse['status']) == 'completed') {
			$payment = new Payment;
			$payment->order_id = $order_id;
			$payment->status = $paymentResponse['status'];
			$payment->txnid = $paymentResponse['txnId'];
			$payment->payment_gateway_id = 3;
			$payment->paid_amount = $paymentResponse['amount'];
			$payment->currency = "INR";
			$payment->details = json_encode($paymentResponse);
			$payment->save();

			$order_request = OrderRequest::find($order_id);
			$order_request->order_from = ONLINE;
			$order_request->status = 2;
			$order_request->order_status = 1;
			$order_request->save();

	        $order_detail = OrderDetail::where('order_id', $order_id)->get();

	        foreach ($order_detail as $detail) {
		        $product_id = $detail->product_id;
		        $quantity = $detail->quantity;
		        $store_id = $detail->vendor_id;

		        $inventory = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();
		        $inventory->quantity = $inventory->quantity - $quantity;
		        $inventory->save();
		    }

		    $user = User::where('id', $order_request->user_id)->first();
		    $user_email = $user->email;
		    $order_code = $order_request->order_code;

    		Cart::destroy();
    		$request->session()->forget('order_code');
    		$request->session()->forget('order_id');

		    return redirect('/checkout/thankyou/'.$ordercode);
		}else{
			$order_request = OrderRequest::find($order_id);
			$order_request->status = 8;
			$order_request->save();
		    return redirect('/checkout/payment-method/'.$ordercode)->with('err_msg', '');
		}

	}

	public function freechargeFaiureResponse(Request $request)
	{
		$ordercode = $request->session()->get('order_code');
    	$order_id = $request->session()->get('order_id');

		$paymentResponse =  $request->all();

		if ( strtolower($paymentResponse['status']) == 'failed' ) {
			
			$order_request = OrderRequest::find($order_id);
			$order_request->status = 8;
			$order_request->save();

		    return redirect('/checkout/payment-method/'.$ordercode)->with('err_msg', '');
		}else{
			$order_request = OrderRequest::find($order_id);
			$order_request->status = 8;
			$order_request->save();
		    return redirect('/checkout/payment-method/'.$ordercode)->with('err_msg', '');
		}
		
	}

	public function payment(Request $request){

		$order_request = DB::table('order_request')
							->join('users', 'order_request.user_id', '=', 'users.id', 'left')
							->where('order_code', $request->get('ordercode'))
							->select('order_request.*', 'users.contact', 'users.email')
							->first();
		// dd($order_request);
		if ($order_request) {
			$data = array('CUST_ID' => $order_request->user_id ,'TXN_AMOUNT'=> (int)$order_request->payble_amount, 'EMAIL' => $order_request->email, 'MOBILE_NO' => $order_request->contact, 'CALLBACK_URL' => url('/payment/callback') );

			// dd($data);
			$v = Paytm::pay($data);
		    
		    $request->session()->put('order_code', $order_request->order_code);
		    $request->session()->put('order_id', $order_request->id);

			return $v;
		}else{
			return redirect('/cart');
		}

	}

	public function callback(Request $request)
	{

		$ordercode = $request->session()->get('order_code');
    	$order_id = $request->session()->get('order_id');

		$paymentResponse =  $request->all();
		$response = Paytm::verifyPayment($paymentResponse);
		$txStatus = Paytm::transactionStatus($response['data']['ORDERID']);

		// dd($txStatus);
		// $data = array('MID' => $response['data']['MID'],'ORDERID' => $response['data']['ORDERID'],'CHECKSUMHASH' => $response['data']['CHECKSUMHASH'] );
		// $response_data = json_encode($data);
  //       $url = 'https://secure.paytm.com/oltp/HANDLER_INTERNAL/TXNSTATUS?JsonData='.$response_data;

  //       $ch = curl_init($url);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  //       curl_setopt($ch, CURLOPT_HEADER, false);
  //       curl_setopt($ch, CURLOPT_POST, 1);
  //       curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  //       $output = curl_exec($ch);      
  //       curl_close($ch);

        if ($txStatus['STATUS'] == 'TXN_SUCCESS' && $response['status'] == 'success') {
        	$payment = new Payment;
			$payment->order_id = $order_id;
			$payment->status = $response['data']['STATUS'];
			$payment->txnid = $response['data']['TXNID'];
			$payment->payment_gateway_id = 2;
			$payment->mode = $response['data']['PAYMENTMODE'];
			$payment->payment_gateway_name = $response['data']['GATEWAYNAME'];
			$payment->paid_amount = $response['data']['TXNAMOUNT'];
			$payment->currency = $response['data']['CURRENCY'];
			$payment->bank_ref_num = $response['data']['BANKTXNID'];
			$payment->payment_time = $response['data']['TXNDATE'];
			$payment->details = json_encode($response['data']);
			$payment->save();

			$order_request = OrderRequest::find($order_id);
			$order_request->order_from = ONLINE;
			$order_request->status = 2;
			$order_request->order_status = 1;
			$order_request->save();

	        $order_detail = OrderDetail::where('order_id', $order_id)->get();

	        foreach ($order_detail as $detail) {
		        $product_id = $detail->product_id;
		        $quantity = $detail->quantity;
		        $store_id = $detail->vendor_id;

		        $inventory = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();
		        $inventory->quantity = $inventory->quantity - $quantity;
		        $inventory->save();
		    }

		    $user = User::where('id', $order_request->user_id)->first();
		    $user_email = $user->email;
		    $order_code = $order_request->order_code;

    		Cart::destroy();
    		$request->session()->forget('order_code');
    		$request->session()->forget('order_id');

		    return redirect('/checkout/thankyou/'.$ordercode);
        }else{
        	$order_request = OrderRequest::find($order_id);
			$order_request->status = 8;
			$order_request->save();
		    return redirect('/checkout/payment-method/'.$ordercode)->with('err_msg', '');
        }
	}

	public function citruspay(Request $request){
		// dd('fdfdfd');
			
			// $order_request = DB::table('order_request')
			// 					->join('users', 'order_request.user_id', '=', 'users.id', 'left')
			// 					->where('order_code', $request->get('ordercode'))
			// 					->select('order_request.*','users.id as userid','users.first_name','users.last_name','users.email', 'users.contact')
			// 					->first();
					$order_request = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('address', 'order_request.user_id', '=', 'address.user_id', 'left')
                    ->join('address as baddress', 'order_request.user_id', '=', 'baddress.user_id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $request->get('ordercode'))
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email','users.contact', 'address.address', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
                    ->first();

// dd($order_request);

			if ($order_request) {
	
		        // $address = Address::where('user_id', '=', $order_request->userid)
				      //               ->where('address_type', '=', 1)
				      //               ->orderBy('created_at', 'desc')
				      //               ->first();

		        // $address = Address::where('user_id', '=', $order_request->userid)
				      //               ->where('address_type', '=', 1)
				      //               ->orderBy('created_at', 'desc')
				      //               ->first();

				DB::table('order_request')
		            ->where('id', $order_request->id)
		            ->update(['status' => 8, 'order_status' => 7]);

				$parameters = [
				'tid' => $order_request->id,
		        'currency' => 'INR',
		        'order_id' => $order_request->id,
		        'amount' => $order_request->payble_amount,
		       	'billing_name' => $order_request->first_name,
		        'billing_email' => $order_request->email,
		        'billing_tel' => '123456987',
		        'billing_address' => $order_request->baddress,
		        'billing_city' => $order_request->bcity,
		        'billing_state' => $order_request->bstate,
		        'billing_zip' => $order_request->bzip,
		        'billing_country' => 'India',
		        'delivery_name' => $order_request->first_name,
		        'delivery_tel' => '1234567896',
		        'delivery_address' => $order_request->baddress,
		        'delivery_city' => $order_request->bcity,
		        'delivery_state' => $order_request->bstate,
		        'delivery_zip' => $order_request->bzip,
		        'delivery_country' => 'India',


		      ];
		    //         $parameters = [
      
      //   'tid' => '1233221223322',
        
      //   'order_id' => '1232212',
        
      //   'amount' => '1200.00',
        
      // ];

		      $order = Indipay::prepare($parameters);
		      
		      $request->session()->put('order_code', $order_request->order_code);
		      $request->session()->put('order_id', $order_request->id);

		      return Indipay::process($order);
			}else{
				return redirect('/cart');
			}
		

	}

	public function response(Request $request)
    {
    	$ordercode = $request->session()->get('order_code');
    	$order_id = $request->session()->get('order_id');


        // For default Gateway
        $response = Indipay::response($request);
        // dd($response);
        // For Otherthan Default Gateway
        // $response = Indipay::gateway('Citrus')->response($request);

        if ($response['order_status'] == 'SUCCESS' || $response['order_status'] == 'Success') {
			$payment = new Payment;
			$payment->order_id = $order_id;
			if (isset($response['order_status'])) {
				$payment->status = $response['order_status'];
			}
			if (isset($response['TxId'])) {
				$payment->txnid = $response['TxId'];
			}
			// $payment->tx_ref_no = $response['TxRefNo'];
			$payment->payment_gateway_id = 1;
			if (isset($response['card_name'])) {
				$payment->mode = $response['card_name'];
			}
			if (isset($response['payment_mode'])) {
				$payment->payment_gateway_name = $response['payment_mode'];
			}
			// $payment->transaction_id = $response['transactionId'];
			// $payment->mode = $response['paymentMode'];
			
			if (isset($response['Amount'])) {
				$payment->paid_amount = $response['Amount'];
			}
			if (isset($response['currency'])) {
				$payment->currency = $response['currency'];
			}
			if (isset($response['issuerRefNo'])) {
				$payment->bank_ref_num = $response['issuerRefNo'];
			}
			if (isset($response['billing_name'])) {
				$payment->firstname = $response['billing_name'];
			}
			if (isset($response['lastName'])) {
				$payment->last_name = $response['lastName'];
			}
			if (isset($response['billing_email'])) {
				$payment->email = $response['billing_email'];
			}
			if (isset($response['billing_address'])) {
				$payment->address1 = $response['billing_address'];
			}
			if (isset($response['addressStreet2'])) {
				$payment->address2 = $response['addressStreet2'];
			}
			if (isset($response['billing_city'])) {
				$payment->city = $response['billing_city'];
			}
			if (isset($response['billing_state'])) {
				$payment->state = $response['billing_state'];
			}
			if (isset($response['billing_country'])) {
				$payment->country = $response['billing_country'];
			}
			if (isset($response['billing_zip'])) {
				$payment->zip = $response['billing_zip'];
			}
			if (isset($response['billing_tel'])) {
				$payment->phone = $response['billing_tel'];
			}
			if (isset($response['billing_tel'])) {
				$payment->phone = $response['billing_tel'];
			}
			if (isset($response['txnDateTime'])) {
				$payment->payment_time = $response['txnDateTime'];
			}
			$payment->details = json_encode($response);
			$payment->save();

			$order_request = OrderRequest::find($order_id);
			$order_request->order_from = ONLINE;
			$order_request->status = 2;
			$order_request->order_status = 1;
			$order_request->save();

	        $order_detail = OrderDetail::where('order_id', $order_id)->get();

	     //    foreach ($order_detail as $detail) {
		    //     $product_id = $detail->product_id;
		    //     $quantity = $detail->quantity;
		    //     $store_id = $detail->vendor_id;

		    //     $inventory = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();
		    //     $inventory->quantity = $inventory->quantity - $quantity;
		    //     $inventory->save();
		    // }

		    $user = User::where('id', $order_request->user_id)->first();
		    $user_email = $user->email;
		    $order_code = $order_request->order_code;


    		Cart::destroy();
    		$request->session()->forget('order_code');
    		$request->session()->forget('order_id');


		    return redirect('/checkout/thankyou/'.$ordercode);

        }else{
        	$order_request = OrderRequest::find($order_id);
			$order_request->order_from = COD;
			$order_request->status = 8;
			$order_request->order_status = 7;
			$order_request->save();
		    return redirect('/checkout/payment-method/'.$ordercode)->with('err_msg', '');
        }

    } 


// public function cashfree(Request $request)
// 	{

// 	}
}