<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use DB;
use Hash;
use Auth;
use Validator;
use Session;
use App\Admin;
use App\Vendor;
use App\City;
use App\Role;
use App\User;
use App\Permission;
use App\Product;
use App\TopCartProduct;
use App\BannerImages;
use App\ProductCategory;
use App\Http\Requests\AdminVendorRequest;
use App\Http\Requests\AdminChangePasswordRequest;
use Spatie\Analytics\Period;
use Analytics;


class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('admin', ['except' => ['login', 'postLogin']]);
    }
    
    public function login() {
        if (Session::has('admin')) {
            return redirect('admin/dashboard');
        }
        return view('admin.login');
    }

    public function postLogin(Request $request){

        $validator = Validator::make($request->all(), [
                "email" => "required|email",
                "password" => "required",
            ]);

        if ($validator->fails()) {

            return redirect('admin/login')
                        ->withErrors($validator)
                        ->withInput();
        }
        $admin = Admin::where('email', $request->get('email'))
                  ->select('id', 'name', 'email', 'password', 'is_active', 'store_id')
                  ->first();

    if ($admin->store_id && $admin->store_id != 0) {

      $store = DB::table('vendors_details')
                  ->join('admin', 'vendors_details.admin_id', '=', 'admin.id', 'left')
                  ->where('vendors_details.id', $admin->store_id)
                  ->select('admin.id as staff_id', 'vendors_details.id as store_id')
                  ->first();

      Session::put('staff', $admin->id);
      $admin->id = $store->staff_id;

    }

        if($admin){
            if (Hash::check($request->get('password'), $admin->password)) {

                if ($admin->is_active == 1) {
                    Session::put('admin', $admin->id);
                    return redirect('admin/dashboard');
                } else {
                    Session::flash('error_message', 'Your account is pending for activation. contact to Admin');
                    return redirect('admin/login');
                }
            }else{
                Session::flash('error_message', 'Invalid Email/Password.');
                return redirect('admin/login');
            }
        }else{
            Session::flash('error_message', 'User Not Found.');
            return redirect('admin/login');
        }
    }

    public function logout(){
        session()->forget('admin');
        return redirect('admin/login');
    }

    public function changeStore(Request $request)
    {
        $admin = Admin::where('id', $request->get('store_id'))->first();
        
        Session::put('admin', $admin->id);
        
        return redirect('admin/dashboard');

    }

    public function banner(){

        $stores = DB::table('vendors_details')->get();
        return view('admin.add_banner', compact('stores'));
    }

    public function addBanner(Request $request){

        $images = $request->file('banner_images');
        if ($images) {
            foreach ($images as $value) {

                $image_name = time()."_".$value->getClientOriginalName();
                $banner_image = new BannerImages;
                $banner_image->image_name = $image_name;
                $banner_image->link = $request->get('link');
                $banner_image->city_id = $request->get('store');
                $banner_image->status = 1;
                $banner_image->save();

                $value->move('banner_images/'.$banner_image->id.'/', $image_name);
            }
        }
        return redirect()->back()->with('success_msg', 'Banner Images Added Successfully.');
    }

    public function bannerList(){
        $banners = DB::table('banner_images')
                        ->join('vendors_details', 'banner_images.city_id', '=', 'vendors_details.id', 'left')
                        ->select('banner_images.*', 'vendors_details.store_name')
                        ->get();

        return view('admin.banner_list', compact('banners'));
    }

    public function profile() {
        if (Session::has('admin')) {
            $id = Session::get('admin');
            $admin = DB::table('admin')
                            ->join('vendors_details', 'admin.id', '=', 'vendors_details.admin_id', 'left')
                            ->where('admin.id', $id)
                            ->select('vendors_details.*', 'admin.name', 'admin.email', 'admin.contact')
                            ->first();

            return view('admin.profile', compact('admin'));
        }else{
            return('admin/login');
        }
    }

    public function editProfile(Request $request){

        if (Session::has('admin')) {
            $id = Session::get('admin');
            $admin = Admin::where('id', $id)->first();
            $admin->name = $request->get('name');
            if (isStore()) {
                $admin->contact = $request->get('phone');
            }
            $admin->save();
            if (isStore()) {
                $store = Vendor::where('admin_id', $id)->first();
                $store->customer_email = $request->get('customer_email');
                $store->store_name = $request->get('customer_email');
                $store->address = $request->get('address');
                $store->location = $request->get('location');
                $store->country = $request->get('country');
                $store->state = $request->get('state');
                $store->city = $request->get('city');
                $store->pin_code = $request->get('zip');
                $store->save();
            }

            return redirect()->back()->with('success_msg', 'Store Updated Successfully.');
        }else{
            return('admin/login');
        }   
    }

    public function settings(){
        if (Session::has('admin')) {

            $id = Session::get('admin');
            $admin = DB::table('admin')->where('id', $id)->first();
            
            return view('admin.settings', compact('admin'));
        }else{
            return redirect('admin/login');
        }
    }

    public function postChangepassword(AdminChangePasswordRequest $request) {
        
        $admin = Admin::find(Session::get('admin')); 
        $old_password   = $request->get('current_password');
        $password       = $request->get('new_password');
        if (Hash::check($old_password, $admin->password)) {

            $admin->password = Hash::make($password);
            $admin->save();

            return redirect('admin/settings')->with('success_msg', 'Your password has been changed');
        }else{
            return redirect('admin/settings')->with('err_msg', 'Your old password is incorrect');
        }
    }

    public function postChangeEmail(Request $request){

        $this->validate($request, [
            'new_email' => 'required|email',
        ]);

        $admin = Admin::find(Session::get('admin'));
        $admin->email = $request->get('new_email');
        $admin->save(); 

        return redirect('admin/settings')->with('success_msg', 'Your Email has been changed');

    }

    public function postDeactivateAccount(Request $request) {

        $admin = Admin::find(Session::get('admin'));
        $check = Hash::check($request->get('password'), $admin->password);
        // dd($check);
        if (!$check) {
            Session::flash('has_error',1);
            return redirect('admin/settings');
        }else{
            $admin->is_active = 0;
            $admin->save();

            return redirect('admin/login')->with('success_msg', 'Your Account has been Deactivated');
        }

    }

// for menu 
    public function menu() {

         $menu = ProductCategory::get();
        // dd($menu);
         return view('admin.menu', compact('menu'));

    }
    public function dashboard() {
$msgDat = array();
        $top_cart_product = DB::table('top_cart_product')
                                ->join('product', 'top_cart_product.product_id', '=', 'product.id', 'left')
                                ->select('top_cart_product.id', 'top_cart_product.count', 'product.product_title', 'product.price', 'product.product_images', 'product.id')
                                ->limit(3)
                                ->orderby('count','desc')
                                ->get();

                                
            
            
            

            // $analyticsData = Analytics::fetchTotalVisitorsAndPageViews(Period::days(0));
            // $todaysvis = $analyticsData[0]['visitors'];

            $todaysvis = 67;

            $low_inventory_query = DB::table('product')
                                ->join('inventory', 'product.id', '=', 'inventory.product_id', 'left' )
                                ->select('product.*', 'inventory.quantity')
                                ->where('inventory.quantity', '<=' , 3)
                                ->orWhere('inventory.quantity', '=' , 0);


           if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $low_inventory_query->where('inventory.store_id', $vendor->id);

           }
       
            $low_inventory = $low_inventory_query->groupBy('product.id')->orderby('inventory.quantity','asc')->limit(4)->get();
            // dd($low_inventory);
            // $low_inventory = DB::table('product')
            //                     ->select('product.*')
            //                     ->where('quantity', '<=' , 3)
            //                     ->orderby('quantity','asc')
            //                     ->limit(4)
            //                     ->get();

            // total customer with total ammount
            $customer_list = DB::table('order_request')
                                ->select('user_id' , DB::raw('SUM(payble_amount) as total_amount, COUNT(id) as total_order'))
                                ->groupBy('user_id')
                                ->orderby('total_amount','desc')
                                ->limit(4)
                                ->get();


            $total_spent = DB::table('order_request')
                                ->join('users', 'users.id', '=', 'order_request.user_id', 'left' )
                                ->select('users.*' , DB::raw('SUM(order_request.payble_amount) as total_amount, COUNT(order_request.id) as total_order'))
                                ->groupBy('id')
                                ->orderby('total_spent','desc')
                                ->limit(4)
                                ->get();

            // $total_spent = DB::table('order_request')
            //         ->join('users', 'users.id', '=', 'order_request.user_id', 'left' )
            //         ->select('users.*' , DB::raw('SUM(order_request.payble_amount) as total_amount, COUNT(order_request.id) as total_order'))
            //         ->where('order_request.order_status', '=' ,4)
            //         ->toSql();





                                // dd($total_spent);
                                     
             //  this is for products are on discount
            $discountedproduct = DB::table('product')
                        ->select('id','product_title', 'product_images', 'price', 'compare_price')
                        ->where('compare_price', '<>', 'NULL')
                        ->limit(4)
                        ->get();  
             

            $top_selling_product = DB::table('order_detail')
                                        ->join('product', 'order_detail.product_id', '=', 'product.id', 'left' )
                                        ->select('product.product_title', 'product.product_images','product.price','order_detail.product_id', DB::raw('COUNT(order_detail.product_id) as total_product_record'))
                                        ->groupBy('order_detail.product_id')
                                        ->groupBy('product.price')
                                        ->groupBy('product.product_images')
                                        ->groupBy('product.product_title')
                                        ->limit(4)
                                        ->get();

            //  get total sales basis of store id 
            $total_sales_query = DB::table('order_request')
                   ->select(DB::raw('SUM(payble_amount) as payble_amount'))
                   ->whereRaw('Date(created_at) = CURDATE()');

           if (isStore()) {
                $admin = isStore();
            $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();
               $total_sales_query->where('vendor_id', $vendor->id);

           }
       
            $total_sales = $total_sales_query->whereIn('order_status',[1,3,4])->first();

        // $total_sales = DB::table('order_request')
  //                            ->select(DB::raw('SUM(payble_amount) as payble_amount'))
  //                            ->whereRaw('Date(created_at) = CURDATE()')
  //                            ->where('order_status',1)
  //                            ->first();

            // getting total order on the basis of store id 
           $total_order_query = DB::table('order_request')
                       ->select(DB::raw('COUNT(order_code) as totalorder'))
                       ->whereRaw('Date(created_at) = CURDATE()');

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_order_query->where('order_request.vendor_id', $vendor->id);

               }
           
           $total_order = $total_order_query->where('order_from',1)->where('order_status',1)->first();
                // dd($total_order);
    
            // total today's order
        $total_today_order_quary = DB::table('order_request')
                       ->select(DB::raw('COUNT(order_code) as totaltodaysorder'))
                       ->whereRaw('Date(created_at) = CURDATE()');

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_today_order_quary->where('order_request.vendor_id', $vendor->id);

               }
           
           $total_today_order = $total_today_order_quary->whereIn('order_status',[1,3,4])->first();

        
                // total online sales
           $total_online_sales_quary = DB::table('order_request')
                       ->select(DB::raw('SUM(payble_amount) as payble_amount'))
                       ->whereRaw('Date(created_at) = CURDATE()')
                       ->where('order_from', '=' ,0);

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_online_sales_quary->where('order_request.vendor_id', $vendor->id);

               }
           
           $total_online_sales = $total_online_sales_quary->whereIn('order_status',[1,3,4])->first();
           // dd($total_online_sales);

           // offline sales
           $total_offline_sales_quary = DB::table('order_request')
                                ->select(DB::raw('SUM(payble_amount) as payble_amount'))
                                ->whereRaw('Date(created_at) = CURDATE()');

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_offline_sales_quary->where('order_request.vendor_id', $vendor->id);

               }
           
           $total_offline_sales = $total_offline_sales_quary->where('order_from', '=' ,1)->whereIn('order_status',[1,3,4])->first();

        //  total order online
           $total_order_online_quary = DB::table('order_request')
                                ->select(DB::raw('COUNT(order_code) as totalorder'))
                                ->whereRaw('Date(created_at) = CURDATE()');

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_order_online_quary->where('order_request.vendor_id', $vendor->id);

               }
           
           $total_order_online = $total_order_online_quary->where('order_from', '=' ,0)->whereIn('order_status',[1,3,4])->first();

           // total order offline
           //  total order online
           $total_order_offline_quary = DB::table('order_request')
                                ->select(DB::raw('COUNT(order_code) as totalorder'))
                                ->whereRaw('Date(created_at) = CURDATE()');

               if (isStore()) {
                $admin = isStore();
                $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

                   $total_order_offline_quary->where('order_request.vendor_id', $vendor->id);

               }
           
           $total_order_offline = $total_order_offline_quary->where('order_from', '=' ,1)->whereIn('order_status',[1,3,4])->first();
        

                        //      $records = DB::table('order_request')->select(DB::raw('*'))
                           //               ->whereRaw('Date(created_at) = CURDATE()')->get();
                           // dd($records);


           // $total_visitor_quary = DB::table('visitors')
           //                      ->select(DB::raw('SUM(hits) as totalhits'));

           //     if (isStore()) {
           //      $admin = isStore();
           //      $vendor = Vendor::where('admin_id', $admin->id)->select('id')->first();

           //         $total_visitor_quary->where('store_id', $vendor->id);

           //     }
           
           // $total_visitor = $total_visitor_quary->whereRaw('Date(created_at) = CURDATE()')->first();

           // $total_visitor = DB::table('visitors')
           //                      ->select(DB::raw('SUM(hits) as totalhits'))
           //                      ->whereRaw('Date(created_at) = CURDATE()')->first();
                                // dd($total_visitor);


        return view('admin.dashboard', compact('top_cart_product','total_sales','todaysvis','total_order','total_order_offline','total_offline_sales','low_inventory','total_today_order','total_online_sales','customer_list','discountedproduct', 'top_selling_product','all_added_product','total_order_online','total_visitor','total_spent'));
    }
 
    public function product()
    { 
        return view('admin.product');
    }
    
    public function addUser(Request $request){

        $roles = Role::all();
        $permissions = Permission::all();
        $vendors = Vendor::all('store_name', 'id');

        return view('admin.staff.add_user', compact('roles', 'permissions', 'vendors'));
    }

    public function postAddUser(Request $request){
        
        $admin = isStore();
        if ($admin) {
            $store = Vendor::where('admin_id', $admin->id)->select('id')->first();
            $store_id = $store->id;
        }else{
            $store_id = $request->get('vendor_id');
        }
      
      $admin = new Admin();
        $admin->name = $request->get('name');
        $admin->email = $request->get('email');
        $admin->contact = $request->get('phone');
        $admin->password = Hash::make($request->get('email'));
        $admin->store_id = $store_id;
        $admin->is_active = 1;
        $admin->save();

        if ($request->get('roles')) {
            foreach ($request->get('roles') as $role) {
                DB::table('role_user')->insert(
                    ['admin_id' => $admin->id, 'role_id' => $role]
                );
            }
        }

        return redirect()->back();
    }

    public function adminUserList(Request $request)
    {   
        // dd($request->session()->all());
        $admin_id = $request->session()->get('admin');
        $store = Vendor::where('admin_id', $admin_id)->first();
        if ($store) {
            $staffs = DB::table('admin')
                            ->join('role_user', 'admin.id', '=', 'role_user.admin_id', 'left')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id', 'left')
                            ->where('store_id', $store->id)
                            ->select('admin.id','admin.name', 'admin.email', 'admin.contact', 'roles.display_name')
                            ->get();
            // dd($staffs);
            // $staffs = Admin::where('store_id', $store->id)->get();

            return view('admin.staff.user_list', compact('staffs'));
        }else{
            return redirect('/admin/dashboard');
        }
    }

    
    public function vendor()
    {
        $vendors = DB::table('admin')
                        ->join('vendors_details', 'admin.id', '=', 'vendors_details.admin_id', 'left')
                        ->join('city', 'vendors_details.city', '=', 'city.city_id', 'left')
                        ->where('admin.is_active', '=', 1)
                        ->where('admin.is_vendor', '=', 1)
                        ->select('admin.id as vendorid', 'admin.is_active', 'admin.email', 'admin.contact','city.city as cityname', 'vendors_details.*')
                        ->get();

        return view('admin.vendor.vendorlist', compact('vendors'));
    }

    public function editVendor($id) {

        $vendor = DB::table('admin')
                        ->join('vendors_details', 'admin.id', '=', 'vendors_details.admin_id', 'left')
                        ->where('admin.id', '=', $id)
                        ->where('admin.is_active', '=', 1)
                        ->where('admin.is_vendor', '=', 1)
                        ->select('admin.id as vendorid', 'admin.name', 'admin.email', 'admin.contact', 'vendors_details.*')
                        ->first();

        return view('admin.vendor.edit_vendor', compact('vendor'));
    }
    
    public function newVendor() {
        
        $cities = City::where('status', '=', 1)->pluck('city', 'city_id')->toArray();
        
        return view('admin.vendor.new_vendor', compact('cities'));
    }

    public function addNewVendor(AdminVendorRequest $request, $id=null) {
        
        if ($id) {
            $admin = Admin::find($id);
            $success_msg = "Store Updated Successfully.";
        }else{
            $admin = new Admin;
            $admin->password = Hash::make(123456);
            $admin->city_id = $request->get('city');
            $success_msg = "New Store Added Successfully.";
        }

        $admin->name = $request->get('store_name');
        $admin->email = $request->get('email');
        $admin->contact = $request->get('contact');
        $admin->is_vendor = 1;
        $admin->is_active = 1;
        $admin->save();
        if ($request->file('vendor_image')) {
            $image_name = time()."_".$admin->id."_".$request->file('vendor_image')->getClientOriginalName();
            $request->file('vendor_image')->move('vendor_images/'.$admin->id, $image_name);

        }else{
            $image_name = '';
        }
        if ($id) {
            $vendor = Vendor::where('admin_id', $id)->first();
        }else{
            $vendor = new Vendor;
        }
        $vendor->admin_id = $admin->id;
        $vendor->store_name = $request->get('store_name');
        $vendor->customer_email = $request->get('customer_email');
        $vendor->image = $image_name;
        $vendor->business_name = $request->get('business_name');
        $vendor->address = $request->get('address');
        $vendor->location = $request->get('location');
        $vendor->city = $request->get('city');
        $vendor->pin_code = $request->get('pin_code');
        $vendor->state = $request->get('state');
        $vendor->country = $request->get('country');
        $vendor->slug = str_slug($request->get('store_name'));
        $vendor->save();

        return redirect()->back()->with('success_msg', $success_msg);
    }

}
