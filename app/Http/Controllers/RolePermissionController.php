<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Admin;
use App\Role;
use App\Permission;

class RolePermissionController extends Controller
{
    
    public function addRole(Request $request) {

        $permissions = Permission::all();
    	return view('admin.role.addrole', compact('permissions'));
    }

    public function postAddRole(Request $request) {

    	$role = new Role();
		$role->name         = $request->get('name');
		$role->display_name = $request->get('display_name');
		$role->save();
    	
        if ($request->get('permissions')) {
            foreach ($request->get('permissions') as $perm) {
                 DB::table('permission_role')->insert(
                    ['permission_id' => $perm, 'role_id' => $role->id]
                );
            }            
        }

    	return redirect()->back();
    }

    public function addPermission(Request $request) {
    	// $admin_id = $request->session()->get('admin');
     //    $admin = Admin::where('id', $admin_id)->first();
        // $admin = DB::table('admin')->where('id', $admin_id)->first();

        // dd($admin->hasRole('admin'));
        // dd($admin->hasPermission('edit post'));
        // dd($admin->hasPermissionThroughRole('edit post'));


    	return view('admin.role.addpermission');
    }

    public function postAddPermission(Request $request) {

    	$permission = new Permission();
		$permission->name         = $request->get('name');
		$permission->display_name = $request->get('display_name'); // optional
		// Allow a user to...
		$permission->save();
    	
    	return redirect()->back();
    }
}
