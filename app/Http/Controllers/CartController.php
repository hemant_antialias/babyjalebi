<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;
use App\Coupon;
use App\TopCartProduct;
use Cart;
use Auth;
use App\Order;
use App\Address;
use App\OrderDetail;
use App\StoreLocations;
use App\Inventory;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth', ['except' => [
        //     'addToCart', 'cart', 'checkDiscount', 'deleteProduct', 'updateOnQuantity'
        // ]]);

        parent::__construct();
    }

    public function addToCart(Request $request){
        $store_id = $request->session()->get('store_id');
    	$product_id = $request->get('product_id');
    	$product_title = $request->get('product_title');
    	$product_price = $request->get('product_price');
    	$quantity = $request->get('quantity');
        $image_url = $request->get('image_url1');
        $ptext = $request->get('pname');
        $font = $request->get('font');
        $color = $request->get('color');
        $pattern = $request->get('varname');
    	$fullurl = $request->get('fullurl');
        // $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        // dd($fullurl);

        $product_detail = Product::where('id', $product_id)->select('tax')->first();
        $product = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();

        $data = Cart::search(function ($cartItem, $rowId) use ($product_id) {
            return $cartItem->id == $product_id;
        });
        // dd($data);

        if (!$data->isEmpty()) {
            // dd('dd');
            $qty = '';
            foreach ($data as $value) {
                $qty = $value->qty;
            }

            if ($product && $qty >= $product->quantity) {
                
                $data['cart_count'] = Cart::count();
                $data['status'] = 0;
                $data['message'] = "You can add only ".$product->quantity." ".$product_title." to the cart.";
                // dd($data);
                return $data;
            }
        }

        if ($product && $quantity > $product->quantity) {

                $data['cart_count'] = Cart::count();
                $data['status'] = 0;
                $data['message'] = "You can add only ".$product->quantity." ".$product_title." to the cart.";
                // dd($data);
                return $data;
        }



        // Add entry to top cart product and update count
        $top_cart_product = TopCartProduct::where('product_id', $product_id)->first();
        if ($top_cart_product) {
            $top_cart_product->count += 1;
            $top_cart_product->save();
        }else{
            $top_cart_product = new TopCartProduct;
            $top_cart_product->product_id = $product_id;
            $top_cart_product->count = 1;
            $top_cart_product->save();
        }
            

    	$cartItem = Cart::add($product_id, $product_title, $quantity, $product_price, ['image_url' => $image_url, 'tax' => $product_detail->tax, 'discount' => 0,'ptext' =>$ptext, 'font' =>$font,'color'=>$color,'pattern'=>$pattern,'fullurl'=>$fullurl]);
        Cart::associate($cartItem->rowId, '\App\Product');
        $data["count"] = Cart::count();
        return $data;
    }

    public function cart()
    {
        // dd("cart page");
        return view('web-files.cart', compact('products'));
    }
    public function checkout()
    {

        return view('web.checkout');
    }

    public function checkDiscount(Request $request){

        if ($request->get('discout')) {
            $userid = Auth::id();

            $order = DB::table('order_request')
                        ->join('discount_coupon', 'order_request.coupon_code', '=', 'discount_coupon.discount_coupon_code', 'left')
                        ->where('order_request.coupon_code', $request->get('discout'))
                        ->where('order_request.order_status', 4)
                        ->where('order_request.user_id', $userid)
                        ->where('discount_coupon.limit_per_user', 1)
                        ->first();
                        
            if ($order) {

                $data['status'] = 0;
                $data['message'] = "You have already applied this code";

                return $data;
            }
        }

        if ($request->get('ordercode')) {
           $order = Order::where('order_code', $request->get('ordercode'))->first();
           $order_detail = OrderDetail::where('order_id', $order->id)->get();

        }else{
            $data['status'] = 0;
            $data['message'] = "Invalid Order Code";
        }

        $cart_total = $order->payble_amount;
        $discout = $request->get('discout');
        $subtotal = $request->get('subtotal');
        $coupon = Coupon::where('discount_coupon_code', $discout)->select('id', 'discount_coupon_code','min_order_amount', 'discount_value','coupon_type', 'discount_type')->first();

        if ($cart_total > $coupon->min_order_amount) {

    		if($coupon){
    			$data['coupon_type'] = $coupon->coupon_type;
    			if($coupon->coupon_type=='2')
    			{
                    if ($coupon->discount_type == "all_orders" || $coupon->discount_type == null) {
                        $total_tax = [];
                        $cart_content = Cart::content();
                        $subtotal = $order->payble_amount;
                        $discount_value = round($order->payble_amount*$coupon->discount_value/100,2);
                        $subtotal_after_discount = $subtotal - $discount_value;
                        
                        foreach ($cart_content as $detail) {

                            $product_deatil = Product::where('id', $detail->id)->select('tax')->first();

                            $product_discount = $coupon->discount_value/100*$detail->price*$detail->qty; 

                            $product_after_discount = $detail->price*$detail->qty-$product_discount; 
                            $tax = $product_deatil->tax;
                            $product_tax = round($tax/100*$product_after_discount, 2);
                            // $tax = $detail->options->tax/100*$detail->price*$detail->qty;
                            array_push($total_tax, $product_tax);
                        }

                        $final_tax = array_sum($total_tax);

                        $order->discount = $discount_value;
                        $order->coupon_code = $coupon->discount_coupon_code;
                        $order->payble_amount =$order->amount - $discount_value + $final_tax;
                        $order->tax_rate = $final_tax;
                        $order->save();

                        if ($order->payble_amount > 700) {
                            $order->custom_rate_value = 0;
                            $order->payble_amount = $order->payble_amount + $order->custom_rate_value;
                            $order->save();                            
                        }else{
                            $order->custom_rate_value = 0;
                            $order->payble_amount = $order->payble_amount + $order->custom_rate_value;
                            $order->save();
                        }

                        $data['after_discount_price'] = $order->payble_amount;
                        $data['discount_value'] = $order->discount;
                        $data['coupon_code'] = $coupon->discount_coupon_code;
                        $data['shipping'] = $order->custom_rate_value; 
                        $data['final_tax'] = $order->tax_rate;           
                        $data['status'] = 1;
                        $data['message'] = "Coupon Applied Successfully";
                    }

                    if ($coupon->discount_type == "collection") {
                        $total_tax = [];
                        $cart_content = Cart::content();
                        $subtotal = $order->payble_amount;
                        $pdiscounts = [];

                        
                        foreach ($cart_content as $detail) {

                            $discount_product_category = DB::table('discount_product_categories')
                                                            ->where('discount_id', $coupon->id)
                                                            ->select('product_category_id')
                                                            ->get();

                            foreach ($discount_product_category as $category) {
                            
                                $product_deatil = Product::where('id', $detail->id)
                                                    ->select('id','tax', 'product_category', 'meat_type', 'brand_type')
                                                    ->first();
                                
                                if ($product_deatil->product_category == $category->product_category_id) {
                                    $category_id = $category->product_category_id;
                                    $discount = $coupon->discount_value/100*$detail->price*$detail->qty; 
                                    array_push($pdiscounts, $discount);

                                }
                                if ($product_deatil->meat_type == $category->product_category_id) {

                                    $category_id = $category->product_category_id;
                                    $discount = $coupon->discount_value/100*$detail->price*$detail->qty; 
                                    array_push($pdiscounts, $discount);
                                
                                }
                                if ($product_deatil->brand_type == $category->product_category_id){

                                    $category_id = $category->product_category_id;
                                    $discount = $coupon->discount_value/100*$detail->price*$detail->qty; 
                                    array_push($pdiscounts, $discount);

                                }

                            }

                            $product_discounts = array_sum($pdiscounts);

                            $discount_value = round($product_discounts,2);
                            $subtotal_after_discount = $subtotal - $discount_value;

                            $product_discount = $discount_value; 

                            $product_after_discount = $detail->price*$detail->qty-$product_discount; 
                            $tax = $detail->tax;
                            $product_tax = round($tax/100*$product_after_discount, 2);
                            array_push($total_tax, $product_tax);
                        }

                        $final_tax = array_sum($total_tax);

                        $order->discount = $discount_value;
                        $order->coupon_code = $coupon->discount_coupon_code;
                        $order->payble_amount =$order->amount - $discount_value;
                        $order->tax_rate = $final_tax;
                        $order->save();

                        if ($order->payble_amount > 700) {
                            $order->custom_rate_value = 0;
                            $order->payble_amount = $order->payble_amount + $order->custom_rate_value;
                            $order->save();                            
                        }else{
                            $order->custom_rate_value = 0;
                            $order->payble_amount = $order->payble_amount + $order->custom_rate_value;
                            $order->save();
                        }

                        $data['after_discount_price'] = $order->payble_amount;
                        $data['discount_value'] = $order->discount;
                        $data['shipping'] = $order->custom_rate_value; 
                        $data['final_tax'] = $order->tax_rate;           
                        $data['status'] = 1;
                        if ($order->discount > 1) {
                            $data['message'] = "Coupon Applied Successfully";
                            $data['coupon_code'] = $coupon->discount_coupon_code;
                        }else{
                            $data['message'] = "Coupon code is not valid."; 
                            $data['coupon_code'] = "";
                        }
                    }

                    if ($coupon->discount_type == "product") {
                        $total_tax = [];
                        $cart_content = Cart::content();
                        $subtotal = $order->payble_amount;
                        
                        foreach ($cart_content as $detail) {

                            $discount_products = DB::table('discount_product')
                                                            ->where('discount_id', $coupon->id)
                                                            ->select('product_category_id')
                                                            ->get();

                            foreach ($discount_products as $product) {
                            
                                $product_deatil = Product::where('id', $detail->id)
                                                    ->select('id','tax', 'product_category', 'meat_type', 'brand_type')
                                                    ->first();
                                
                                $product_discount = [];
                                if ($product_deatil->product_category == $product->product_category_id) {
                                 
                                    $product_id = $product->product_id;
                                    $discount = $coupon->discount_value/100*$detail->price*$detail->qty; 
                                    array_push($product_discount, $discount);
                                } 

                            }                                

                            $product_discount = array_sum($product_discount);

                            $discount_value = round($product_discount,2);
                            $subtotal_after_discount = $subtotal - $discount_value;

                            
                            $product_discount = $coupon->discount_value/100*$detail->price*$detail->qty; 

                            $product_after_discount = $detail->price*$detail->qty-$product_discount; 
                            $tax = $product_deatil->tax;
                            $product_tax = round($tax/100*$product_after_discount, 2);
                            // $tax = $detail->options->tax/100*$detail->price*$detail->qty;
                            array_push($total_tax, $product_tax);
                        }

                        $final_tax = array_sum($total_tax);

                        $order->discount = $discount_value;
                        $order->coupon_code = $coupon->discount_coupon_code;
                        $order->tax_rate = $final_tax;
                        $order->payble_amount =$order->amount - $discount_value;
                        $order->save();

                        if ($order->payble_amount > 700) {
                            $order->custom_rate_value = 0;
                            $order->save();                            
                        }else{
                            $order->custom_rate_value = 0;
                            $order->save();
                        }


                        $data['after_discount_price'] = $order->payble_amount;
                        $data['discount_value'] = $order->discount;
                        $data['coupon_code'] = $coupon->discount_coupon_code;
                        $data['shipping'] = $order->custom_rate_value; 
                        $data['final_tax'] = $order->tax_rate;           
                        $data['status'] = 1;
                        $data['message'] = "Coupon Applied Successfully";       
                    }
		
    			}

    			if($coupon->coupon_type=='1') {


        			$data['after_discount_price'] = $order->payble_amount - $coupon->discount_value;
                    // dd($order->tax_rate_value);
        			$data['discount_value'] = $coupon->discount_value;
                    $data['coupon_code'] = $coupon->discount_coupon_code;           
        			$data['status'] = 1;
        			$data['message'] = "Coupon Applied Successfully";
    			}
    		}else{
    			$data['status'] = 0;
    			$data['message'] = "Invalid Coupon Code";
    		}
        }else{
            $data['status'] = 0;
            $data['message'] = "Minimum order should be Rs. ".$coupon->min_order_amount;
        }

		
        return $data;   
    }

    public function asyncContent(){
        if (Auth::check()) {
        
            return view('web.cart.asyncdata');
        
        }else{

            $data['status'] = 0;
            // return redirect('/login');
            return $data;
        }

    }

    public function deleteProduct(Request $request){
        
        $id = $request->get('id');
		$cart_id = Cart::get($id);
		$total_price = Cart::subtotal('2', '.', '');
		$price = $cart_id->price * $cart_id->qty;
		$item_price = number_format((float)$price, 2, '.', '');
		$subtotal_price = $total_price - $price;
		
		Cart::remove($id);
		$data['subtotal'] = $subtotal_price;
		$data['status'] = 1;
		$data['message'] = "Item Removed from Cart Successfully.";

		return $data;
    }

    public function updateOnQuantity(Request $request){
        
        $store_id = $request->session()->get('store_id');
        $product_id = $request->get('pid');
        $quantity = (int)$request->get('quantity');
        $id = $request->get('id');
        $cart_id = Cart::get($id);

        $product = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();

        if ($product && $quantity > $product->quantity) {
            
            Cart::update($cart_id->rowId, $product->quantity);
            $total_product_price = $cart_id->price * $product->quantity;
            $data['quantity'] = $product->quantity;
            $data['total_product_price'] = $total_product_price;
            $data['status'] = 0;
            $data['message'] = "You can add only ".$product->quantity." to the cart.";

            return $data;
        }


        Cart::update($cart_id->rowId, $quantity);
        $total_product_price = $cart_id->price * $quantity;
        $subtotal_price = Cart::subtotal('2', '.', '');
        $data['subtotal'] = $subtotal_price;
        $data['total_product_price'] = $total_product_price;
        $data['status'] = 1;
        $data['message'] = "Quantity updated from Cart Successfully.";

        return $data;

    }

    public function newOrder(Request $request){
        $cart_total = Cart::subtotal('2', '.', '');
        $slug = $request->session()->get('location_slug');
        $store_locations = StoreLocations::where('slug', $slug)->first();

        

        if (!Auth::check()) {
            return redirect('/login');
        }

        if ($request->get('discount_amount')) {
            $discout_amount = $request->get('discount_amount');
        }else{
            $discout_amount = 0;
        }


        // $store_id = session('store_id');
        $userid = Auth::id();
        $order = new Order;
        $order->order_code = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
        $order->user_id = $userid;
        // $order->vendor_id = $store_id;
        $order->discount = $discout_amount;
        $order->coupon_code = $request->get('discounted_coupon_code');
        $order->notes = $request->get('notes');
        $order->order_currency = 'INR';
        $order->order_status = 7;
        $order->amount  = $request->get('totalamnt');
        $order->payble_amount = $cart_total-$discout_amount;
        $order->status = 5;
        $order->save();

        $cart_content = Cart::content();

        foreach ($cart_content as $product) {
            $orderDetail = new OrderDetail;
            $orderDetail->product_id = $product->id;
            $orderDetail->quantity = $product->qty;
            $orderDetail->pattern_name = $product->options->pattern;
            $orderDetail->text = $product->options->ptext;
            $orderDetail->font = $product->options->font;
            $orderDetail->color = $product->options->color;
            // $orderDetail->vendor_id = $store_id;
            $orderDetail->order_id = $order->id;
            $orderDetail->save();
        }

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity', 'product.tax')
                            ->get();

        $total_tax = [];
        foreach($order_detail as $detail) {
            $tax = $detail->tax/100*$detail->price;
            $ttax = $tax*$detail->quantity;
            array_push($total_tax, $ttax);
        }
        $final_tax = array_sum($total_tax);

        // check shipping
        if ($order->payble_amount > 700) {
            $shipping = 0;                            
        }else{
            $shipping = 0;                            
        }

        DB::table('order_request')
            ->where('order_code', $order->order_code)
            ->update(['tax_rate' => $final_tax, 'payble_amount' => $order->amount+$shipping+$final_tax, 'custom_rate_value' => $shipping]);

        $address = Address::where('user_id', '=', $userid)->orderBy('created_at', 'desc')->first();
        // if ($address) {
        //     return redirect('checkout/shipping-method/'.$order->order_code);
        // }else{
            return redirect('checkout/customer-info/'.$order->order_code);
        // }
    }

    // for guest user 

    public function guestnewOrder(Request $request){
        // dd("add");
        // $cart_content = Cart::content();
        // dd($cart_content);
        $cart_total = Cart::subtotal('2', '.', '');
        $slug = $request->session()->get('location_slug');
        $store_locations = StoreLocations::where('slug', $slug)->first();


        // $store_id = session('store_id');
        // $userid = Auth::id();
        $order = new Order;
        $order->order_code = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
        // $order->user_id = $userid;
        // $order->vendor_id = $store_id;
        // $order->discount = $discout_amount;
        $order->coupon_code = $request->get('discounted_coupon_code');
        $order->notes = $request->get('notes');
        $order->order_currency = 'INR';
        $order->order_status = 7;
        $order->amount  = $request->get('totalamnt');
        // $order->payble_amount = $cart_total-$discout_amount;
        $order->status = 5;
        $order->save();

        $cart_content = Cart::content();
        foreach ($cart_content as $product) {
            $orderDetail = new OrderDetail;
            $orderDetail->product_id = $product->id;
            $orderDetail->quantity = $product->qty;
            $orderDetail->pattern_name = $product->options->pattern;
            $orderDetail->text = $product->options->ptext;
            $orderDetail->font = $product->options->font;
            $orderDetail->color = $product->options->color;
            // $orderDetail->pattern_color = $product->options->;
            // $orderDetail->vendor_id = $store_id;
            $orderDetail->order_id = $order->id;
            $orderDetail->save();
        }

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity', 'product.tax')
                            ->get();

        $total_tax = [];
        foreach($order_detail as $detail) {
            $tax = $detail->tax/100*$detail->price;
            $ttax = $tax*$detail->quantity;
            array_push($total_tax, $ttax);
        }
        $final_tax = array_sum($total_tax);

        // check shipping
        if ($order->payble_amount > 700) {
            $shipping = 0;                            
        }else{
            $shipping = 0;                            
        }

        DB::table('order_request')
            ->where('order_code', $order->order_code)
            ->update(['tax_rate' => $final_tax, 'payble_amount' => $order->amount+$shipping+$final_tax, 'custom_rate_value' => $shipping]);

        // $address = Address::where('user_id', '=', $userid)->orderBy('created_at', 'desc')->first();
        // if ($address) {
        //     return redirect('checkout/shipping-method/'.$order->order_code);
        // }else{
        // dd($final_tax);
            return redirect('checkout/customer-info/'.$order->order_code);
        // }
    }

    public function removeDiscount(Request $request)
    {
        $ordercode = $request->get('ordercode');
        $order_request = Order::where('order_code', $ordercode)->first();
        if ($order_request) {

            $order_detail = DB::table('order_detail')
                    ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                    ->where('order_detail.order_id', '=', $order_request->id)
                    ->select('product.product_title', 'product.price', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity', 'product.tax')
                    ->get();

            $total_tax = [];
            foreach($order_detail as $detail) {
                $tax = $detail->tax/100*$detail->price;
                $ttax = $tax*$detail->quantity;
                array_push($total_tax, $ttax);
            }
            $final_tax = array_sum($total_tax);

            if ($order_request->amount+$final_tax > 700) {
                $shipping = 0;
            }else{
                $shipping = 0;
            }

            $order_request->custom_rate_value = $shipping;
            $order_request->discount = 0;
            $order_request->coupon_code = " ";
            $order_request->tax_rate = $final_tax;
            $order_request->payble_amount = $order_request->amount+$final_tax+$shipping;
            $order_request->save();

            $total = Cart::subtotal();
            $cart_content = Cart::content();
            // $total_tax = [];
            // foreach($cart_content as $detail){
            //     $tax = $detail->options->tax/100*$detail->price*$detail->qty;
            //     array_push($total_tax, $tax);
            // }
            // $final_tax = array_sum($total_tax);
            
            $data['status'] = 1;
            $data['final_tax'] = $order_request->tax_rate;
            $data['total_amount'] = $order_request->payble_amount;
            $data['shipping'] = $order_request->custom_rate_value; 

        }else{
            $data['status'] = 0;
            $data['message'] = "Order not found!";
        }

        return $data;
    }

      public function checkPincode(Request $request)
    {

        // dd("pincode checked");

         

            $pincodelist = DB::table('pincode')
                        ->where('pincode', $request->get('pincode'))
                        ->count();
                        // dd($order);
                        
            if ($pincodelist > 0) {

                $data['status'] = 0;
                $data['message'] = "COD available in your Pin Code";

                return $data;
            }
            else
            {
                $data['status'] = 1;
                $data['message'] = "COD not available in your Pin Code";
                return $data;
            }
        

    }

}
