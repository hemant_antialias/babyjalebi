<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;
use DB;
use Cart;
use Mail;
use App\User;
use App\Order;
use App\Address;
use App\OrderDetail;
use App\Vendor;
use App\Product;
use App\Inventory;
use App\BusinessUserDetail;

class BusinessController extends Controller
{
    public function __construct()
    {

    }

    public function login(Request $request)
    {
    	return view('auth.b2b.login');
    }

    public function postLogin(Request $request)
    {

    	if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'is_buser' => 1])) {

	    	return redirect('/business/order');
		} else{
	    	return redirect('/business/login')->with('message', 'Invalid Email or Password.');
		}

    }

    public function register(Request $request)
    {
    	return view('auth.b2b.register');
    }

    public function postRegister(Request $request)
    {
		$user = new User;
		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->contact = $request->get('phone');
		$user->is_buser = 1;
		$user->email = $request->get('email');
		$user->password = Hash::make($request->get('password'));
		$user->save();

    	if ($request->get('business_type')) {
    		$business_type = implode(',', $request->get('business_type'));
    	}

    	$b_user_detail = new BusinessUserDetail;
		$b_user_detail->user_id = $user->id;
		$b_user_detail->address1 = $request->get('address');
		$b_user_detail->address2 = $request->get('address2');
		$b_user_detail->city = $request->get('city');
		$b_user_detail->state = $request->get('state');
		$b_user_detail->business_type = $business_type;
		$b_user_detail->business_name = $request->get('business_name');
		$b_user_detail->business_phone = $request->get('phone');
		$b_user_detail->business_mobile = $request->get('mobile');
		$b_user_detail->local_sales_tax = $request->get('lst');
		$b_user_detail->lst_vdate = $request->get('lst_date');
		$b_user_detail->service_tax = $request->get('service_tax');
		$b_user_detail->s_vdate = $request->get('st_date');
		$b_user_detail->central_sales_tax = $request->get('cst');
		$b_user_detail->cst_vdate = $request->get('cst_date');
		$b_user_detail->fda_license = $request->get('fda_license');
		$b_user_detail->fda_vdate = $request->get('fda_date');
		$b_user_detail->tin = $request->get('tin');
		$b_user_detail->tin_vdate = $request->get('tin_date');
		$b_user_detail->pan = $request->get('pan');
		$b_user_detail->pan_vdate = $request->get('pan_date');
		$b_user_detail->other_license = $request->get('other');
		$b_user_detail->other_vdate = $request->get('other_date');
		$b_user_detail->save();

		return redirect('/login');
    }

    public function bulkOrder(Request $request)
    {
    	if (!Auth::check())
		{
		    return redirect('/business/login');
		}

    	$meat_types = DB::table('product_category')
    					->join('product', 'product_category.id', '=', 'product.meat_type', 'right')
    					->select('product_category.id', 'product_category.name')
    					->groupBy('product_category.id')
    					->get();

    	$products = DB::table('product')
    					->join('product_category', 'product.meat_type', '=', 'product_category.id', 'left')
    					->select('product.id','product.product_title','product.price','product.weight','product.unit','product.product_images','product.meat_type', 'product_category.name')
    					->get();
    	// dd($products);
    	// $products = Product::all();

    	return view('b2b.index', compact('products', 'meat_types'));
    }

    public function addToCart(Request $request){

    	$product_id = $request->get('product_id');
    	$product_title = $request->get('product_title');
    	$product_price = $request->get('product_price');
    	$quantity = $request->get('quantity');
    	$image_url = $request->get('image_url');

        $product_detail = Product::where('id', $product_id)->select('tax')->first();
        $product = Inventory::where('product_id', $product_id)->first();

        $data = Cart::search(function ($cartItem, $rowId) use ($product_id) {
            return $cartItem->id == $product_id;
        });
        

    	$cartItem = Cart::add($product_id, $product_title, $quantity, $product_price, ['image_url' => $image_url, 'tax' => $product_detail->tax, 'discount' => 0]);
        Cart::associate($cartItem->rowId, '\App\Product');

        return view('b2b.cart_summary', compact('cartItem'));
    }

    public function updateQuantity(Request $request)
    {
    	$product_id = $request->get('id');
        $qty = $request->get('qty');

        $data = Cart::search(function ($cartItem, $rowId) use ($product_id) {
            return $cartItem->id == $product_id;
        });

        if (!$data->isEmpty()) {

            $rowid = '';
        	foreach ($data as $value) {
                $rowid = $value->rowId;
            }

	        Cart::update($rowid, $qty);
	        
	        return view('b2b.cart_summary');
        
        } else {
	        return view('b2b.cart_summary');
        }

    }

    public function applyCoupon(Request $request)
    {
    	if ($request->get('coupon')) {
            $discount_coupon_code = $request->get('coupon');
			$cart_content = Cart::content();

            $coupon_discount = DB::table('discount_coupon')
                            ->leftjoin('discount_products','discount_coupon.id','discount_products.discount_id')
                            ->leftjoin('discount_product_categories','discount_coupon.id','discount_product_categories.discount_id')
                            ->where('discount_coupon.discount_coupon_code', $discount_coupon_code)
                            ->select('discount_coupon.id','discount_coupon.discount_coupon_code','discount_coupon.coupon_type', 'discount_coupon.discount_value','discount_coupon.product_id','discount_coupon.collection','discount_coupon.discount_type','discount_coupon.min_order_amount')
                            ->first();

            if ($coupon_discount) {
                if ($coupon_discount->coupon_type == 1) {
                  foreach($cart_content as $detail){
                
                    $product_deatil = Product::where('id', $detail->id)->select('tax')->first();
                    $discount = $coupon_discount->discount_value*$detail->price*$detail->qty/$cart_subtotal;

                    $product_after_discount = $detail->price*$detail->qty-$discount; 
                    $tax = $product_deatil->tax;
                    $product_tax = $tax/100*$product_after_discount;

                    Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $product_tax, 'discount' => $discount, 'is_coupon' => 1]]);

                    }
	        	
                } else {
                    foreach($cart_content as $detail){

                        $product_deatil = Product::where('id', $detail->id)->select('tax')->first();

                        $product_discount = $coupon_discount->discount_value/100*$detail->price*$detail->qty; 

                        $product_after_discount = $detail->price*$detail->qty-$product_discount; 
                        $tax = $product_deatil->tax;
                        $product_tax = round($tax/100*$product_after_discount, 2);


                        Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $product_tax, 'discount' => $product_discount, 'is_coupon' => 1]]);

                    }


                }
                if ($coupon_discount->discount_coupon_code) {
                    $coupon_discount_code = $coupon_discount->discount_coupon_code;
                }else{
                    $coupon_discount_code = '';
                }

	        	return view('b2b.cart_summary', compact('coupon_discount_code'));

            }else{

              	$err_message = 1;

              	return view('b2b.cart_summary', compact('err_message'));   
            }
        }
    }

    public function removeDiscount(Request $request)
    {
    	$cart_content = Cart::content();
        foreach($cart_content as $detail){

            $product_id = $detail->id;
            $product = Product::find($product_id);
            $tax = $product->tax/100*$product->price;
            $tax_price = $tax*$detail->qty;
            
            Cart::update($detail->rowId, ['options' => ['image_url' =>$detail->options->image_url, 'i_sku' => $detail->options->i_sku, 'tax' => $tax_price, 'discount' => 0.00,'is_coupon' => 0, 'is_not_coupon' => 1]]);
        }

        return view('b2b.cart_summary');
    }

    public function removeProduct(Request $request)
    {

        $product_id = $request->get('id');

        $data = Cart::search(function ($cartItem, $rowId) use ($product_id) {
            return $cartItem->id == $product_id;
        });
        if (!$data->isEmpty()) {

            $rowId = '';
        	foreach ($data as $value) {
                $rowid = $value->rowId;
            }

	        Cart::remove($rowid);
	        	
	        $data['status'] = 1;
	        $data['message'] = "Product removed.";


	        return view('b2b.cart_summary');
        
        } else {
	        return view('b2b.cart_summary');
        }
    }

    public function inventory(Request $request)
    {

        $products = DB::table('product')
                        ->join('product_category', 'product.product_category', '=', 'product_category.id', 'left')
                        ->join('vendors_details', 'product.vendor_id', '=', 'vendors_details.id', 'left')
                        ->select('product.*', 'product_category.name as category_name', 'vendors_details.store_name as storename')
                        ->orderBy('product.product_title', 'asc')
                        ->get();


        return view('b2b.inventorylist', compact('products', 'vendors', 'stores', 'inventory'));
    }

    public function newOrder(Request $request){
        $cart_total = Cart::subtotal('2', '.', '');

        if (!Auth::check()) {
            return redirect('/business/login');
        }

        if ($request->get('discount_amount')) {
            $discout_amount = $request->get('discount_amount');
        }else{
            $discout_amount = 0;
        }


        $store_id = session('store_id');
        $userid = Auth::id();
        $order = new Order;
        $order->order_code = rand(10000,99999);
        $order->user_id = $userid;
        $order->vendor_id = 0;
        $order->discount = $discout_amount;
        $order->coupon_code = $request->get('discounted_coupon_code');
        $order->notes = $request->get('notes');
        $order->order_currency = 'INR';
        $order->order_status = 7;
        $order->amount  = $cart_total;
        $order->payble_amount = $cart_total-$discout_amount;
        $order->status = 5;
        $order->save();

        $cart_content = Cart::content();

        foreach ($cart_content as $product) {
            $orderDetail = new OrderDetail;
            $orderDetail->product_id = $product->id;
            $orderDetail->quantity = $product->qty;
            $orderDetail->vendor_id = 0;
            $orderDetail->order_id = $order->id;
            $orderDetail->save();
        }

        $address = Address::where('user_id', '=', $userid)->orderBy('created_at', 'desc')->first();
        if ($address) {
            return redirect('/business/checkout/shipping-method/'.$order->order_code);
        }else{
            return redirect('/business/checkout/customer-info/'.$order->order_code);
        }
    }

    public function thankyou(Request $request, $ordercode){

        $name = Auth::user()->first_name;
        $last_name = Auth::user()->last_name;

        $email = Auth::user()->email;
        $phone = Auth::user()->contact;
        $userid = Auth::id();
        // $address = Address::where('user_id', '=', $userid)->where('is_default', 1)->first();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('address', 'order_request.shipping_address_id', '=', 'address.id', 'left')
                    ->join('address as baddress', 'order_request.billing_address_id', '=', 'baddress.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email','users.contact', 'address.address', 'address.address2', 'address.country', 'address.state', 'address.city', 'address.zip', 'address.is_billing', 'address.is_default', 'baddress.address as baddress', 'baddress.address2 as baddress2', 'baddress.country as bcountry', 'baddress.state as bstate', 'baddress.city as bcity', 'baddress.zip as bzip', 'baddress.is_billing as bis_billing', 'baddress.is_default as bis_default')
                    ->first();

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();

        $user_id_detail = User::Select('total_spent' , 'total_orders')->where('id', $userid)->first();
        // dd($user_id_detail->total_spent + $order->payble_amount);
        $total_amnt = $user_id_detail->total_spent + $order->payble_amount;
        $total_order = $user_id_detail->total_orders + 1;
        $user_id_detail->total_spent = $total_amnt;
        $user_id_detail->total_orders = $total_order;
        $user_id_detail->save();
        // dd($total_amnt);

        $user_email = Auth::user()->email;
        $oredrid = $order->id;

        try {
            Mail::send('mail.order.order_success', [], function($message) use($user_email, $oredrid){
                $message->from('admin@babyjalebi.com');
                $message->to($user_email, "Lionfres Support")->subject("Order #".$oredrid." confirmed");
            });
        } catch (Exception $e) {
            
        }

        return view('b2b.thankyou', compact('name','last_name','phone', 'order', 'order_detail', 'ordercode', 'address', 'oredrid'));
    }

    public function customerInfo(Request $request, $ordercode){

        if (!Auth::check()) {
            return redirect('/business/login');
        }

        $name = Auth::user()->first_name;
        $email = Auth::user()->email;
        $userid = Auth::id();
        $address = Address::where('user_id', '=', $userid)
                            ->where('address_type', '=', 1)
                            ->orderBy('created_at', 'desc')
                            ->first();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email', 'users.country','users.state','users.city','users.address','users.contact', 'users.zip')
                    ->first();

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();

        return view('b2b.customerinfo', compact('order', 'order_detail', 'name', 'email', 'address'));
    }

    public function shippingMethod(Request $request, $ordercode){

        if (!Auth::user()) {
            return redirect('/business/login');
        }

        $name = Auth::user()->first_name;
        $email = Auth::user()->email;
        $userid = Auth::id();
        $address = Address::where('user_id', '=', $userid)
                            ->where('address_type', '=', 1)
                            ->orderBy('created_at', 'desc')
                            ->first();

        if (!$address) {
            return redirect('/business/checkout/customer-info/'.$ordercode);
        }

        $order_request = Order::where('order_code', $ordercode)->first();
        $order_request->payble_amount = $order_request->amount;
        $order_request->shipping = null;
        $order_request->custom_rate_value = null;
        $order_request->tax_rate = null;
        $order_request->coupon_code = null;
        $order_request->discount = null;
        $order_request->save();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email', 'users.country','users.state','users.city','users.address','users.contact', 'users.zip')
                    ->first();
        // dd($order);
        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price','product.tax', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();
            
        return view('b2b.shipping_method', compact('order', 'order_detail', 'name', 'email', 'address'));
    }

    public function paymentMethod(Request $request, $ordercode){
            
        $name = Auth::user()->first_name;
        $email = Auth::user()->email;
        $userid = Auth::id();
        $address = Address::where('user_id', '=', $userid)->first();

        $order = DB::table('order_request')
                    ->join('users', 'order_request.user_id', '=', 'users.id', 'left')
                    ->join('order_status', 'order_request.order_status', '=', 'order_status.id', 'left')
                    ->where('order_request.order_code', $ordercode)
                    ->select('order_request.*','order_status.status_title', 'users.first_name', 'users.last_name', 'users.email', 'users.country','users.state','users.city','users.address','users.contact', 'users.zip')
                    ->first();

        $order_detail = DB::table('order_detail')
                            ->join('product', 'order_detail.product_id', '=', 'product.id', 'left')
                            ->where('order_detail.order_id', '=', $order->id)
                            ->select('product.product_title', 'product.price', 'product.tax', 'product.product_images', 'order_detail.product_id', 'order_detail.quantity')
                            ->get();
        // dd($order_detail);

        return view('b2b.payment_method', compact('order', 'order_detail', 'name', 'email', 'address'));
    }

    public function addCheckoutAddress(Request $request){

	    $validator = Validator::make($request->all(), [
	        // 'first_name'    => 'required',
	        'shipping_address' => 'required',
	        'shipping_city' => 'required',
	        'shipping_state' => 'required',
	        'shipping_zip' => 'required',
	        'shipping_contact' => 'required',
	    ]);

	    if ($validator->fails()) {
	        return redirect()
	                    ->back()
	                    ->withErrors($validator)
	                    ->withInput();
	    }

	    $ordercode = $request->get('ordercode');
	    $shipping_address = $request->get('shipping_address');
	    $shipping_city = $request->get('shipping_city');
	    $shipping_state = $request->get('shipping_state');
	    $shipping_country = "India";
	    $shipping_zip = $request->get('shipping_zip');
	    $shipping_contact = $request->get('shipping_contact');
	    
	    $user = User::find(Auth::id());
	    $user->contact = $shipping_contact;
	    $user->save();

	    $user_id = Auth::id();
	    $address = Address::where('user_id', '=', $user_id)
	                    ->orderBy('created_at', 'desc')
	                    ->first();
	    if ($address) {
	        $address->address = $shipping_address;
	        $address->city = $shipping_city;
	        $address->state = $shipping_state;
	        $address->country = $shipping_country;
	        $address->zip = $shipping_zip;
	        $address->address_type = 1;
	        $address->is_default = 1;       
	        $address->save();
	    }else {
	        $address = new Address;
	        $address->user_id = Auth::id();

	        $address->address = $shipping_address;
	        $address->city = $shipping_city;
	        $address->state = $shipping_state;
	        $address->country = $shipping_country;
	        $address->zip = $shipping_zip;
	        $address->address_type = 1;
	        $address->is_default = 1;       
	        $address->save();
	    }

	    return redirect('/business/checkout/shipping-method/'.$ordercode);

  	} 

  	public function addCheckoutShipping(Request $request){
	    // dd($request->all());
	    $shipping = $request->get('shipping');
	    $order_code = $request->get('ordercode');
	    $total_tax = $request->get('total_tax');
	    // dd($request->get('payble_amount'));
	    $order_request = Order::where('order_code', '=', $order_code)->first();
	    // dd($order_request);
	    if ($request->get('coupon_code')) {
	        $order_request->coupon_code = $request->get('coupon_code');
	        $order_request->discount = $request->get('discount_price');
	        $order_request->payble_amount = $request->get('payble_amount');
	        $order_request->save();
	    }
	    if ($request->get('notes')) {
	        $order_request->notes = $request->get('notes');
	    }
	    if ($request->get('total_tax')) {
	        $order_request->tax_rate = $request->get('total_tax');
	    }
	    if ($shipping == 'custom') {
	        $order_request->shipping = 'custom';
	        $order_request->custom_rate_value = 50;
	        if ($request->get('coupon_code')) {
	            $order_request->payble_amount = $request->get('payble_amount') + 50;
	        }else{
	            $order_request->payble_amount = $order_request->payble_amount +$request->get('total_tax')+ 50;
	        }
	    }else{
	        $order_request->payble_amount = $order_request->payble_amount +$request->get('total_tax');
	        $order_request->shipping = 'free';
	    }
	    $order_request->shipping_address_id = $request->get('shipping_address');
	    $order_request->save();
	    // dd($order_request);
	    return redirect('/business/checkout/payment-method/'.$order_code);
  	}

    public function completeOrder(Request $request){

	    if ($request->get('bill_address') == 2) {
	        $validator = Validator::make($request->all(), [
	            'billing_address' => 'required',
	            'city' => 'required',
	            'state' => 'required',
	            'zip' => 'required',
	            'phone' => 'required',
	        ]);

	        if ($validator->fails()) {
	            return redirect()
	                    ->back()
	                    ->withErrors($validator)
	                    ->withInput();
	        }  
	    }
	    $payment_method = $request->get('payment_method');
	    $order_code = $request->get('ordercode');
	    $order_request = Order::where('order_code', '=', $order_code)->first();

	    $order_detail = OrderDetail::where('order_id', $order_request->id)->get();
	    // foreach ($order_detail as $detail) {
	    //     $product_id = $detail->product_id;
	    //     $quantity = $detail->quantity;
	    //     $store_id = $detail->vendor_id;

	    //     // $inventory = Inventory::where('store_id', $store_id)->where('product_id', $product_id)->first();
	    //     $inventory = DB::table('inventory')
	    //                     ->join('product', 'inventory.product_id', '=', 'product.id', 'left')
	    //                     ->where('inventory.store_id', $store_id)
	    //                     ->where('inventory.product_id', $product_id)
	    //                     ->select('inventory.*', 'product.product_title')
	    //                     ->first();
	        
	    //     if ($inventory->quantity == 0) {
	    //         return redirect('/cart')->with('oot_message', 'Sorry '.$inventory->product_title.' is no longer available.We will notify you when the product is back in stock. Sorry for the Inconvenience.');
	    //     }

	    //     if ($inventory->quantity < $quantity) {
	    //         return redirect('/cart')->with('oot_message', 'Sorry '.$inventory->product_title.' is only '.$inventory->quantity.' quantity left in Stock. Please change the quantity in your cart. We will notify you when more stock is available.');
	    //     }
	    // }

	    // Save billing address
	    if ($request->get('bill_address') == 2) {
	        $address = new Address;
	        $address->user_id = $order_request->user_id;
	        $address->address = $request->get('billing_address');
	        $address->country = "India";
	        $address->state = $request->get('state');
	        $address->city = $request->get('city');
	        $address->zip = $request->get('zip');
	        $address->phone = $request->get('phone');
	        $address->address_type = 1;
	        $address->is_billing = 1;
	        $address->save();
	        $order_request->billing_address_id = $address->id;

	    }
	    if ($request->get('bill_address') == 1) {
	        $order_request->billing_address_id = $order_request->shipping_address_id;
	    }

	    if ($payment_method == 'netbanking') {
	        return redirect('/cpayment?ordercode='.$order_code);
	    }

	    if ($payment_method == 'paytm') {
	        return redirect('/payment?ordercode='.$order_code);
	    }

	    if ($payment_method == 'cod') {
	        $order_request->order_status = 1;
	        $order_request->order_from = COD;
	        $order_request->status = 5;


	    }

	    $order_request->save();
	    
	    Cart::destroy();
	    return redirect('/business/checkout/thankyou/'.$order_code);
  }

}
