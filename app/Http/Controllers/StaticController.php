<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticController extends Controller
{
    public function ps(){
        return view ('web-files.pages.personalization-services');
    }

	public function about(){

		return view('web-files.pages.about-us');
	}
	public function tandc(){

		return view('web-files.pages.t&c');
	}
	public function privacyp(){

		return view('web-files.pages.privacy-policy');
	}

	public function pageNotFound(){

		return view('errors.404');
	}
    
    public function contactus(){
        return view('web-files.pages.contact-us');
    }

    public function cancellation(){
        return view ('web-files.pages.cancellation&refund');
    }
    public function shippingreturns(){
    	return view ('web-files.pages.shipping-returns');
    }
    public function press(){
    	return view ('web-files.pages.press');
    }
    public function faqs(){
    	return view ('web-files.pages.faqs');
    }
   
}







